
#.. Compile shared files
#..
gen_of77=$(foreach a,$(gen_ff77),$(SHARE)/$(OBJ)/$(a).o)
gen_of90=$(foreach a,$(gen_ff90),$(SHARE)/$(OBJ)/$(a).o)
$(gen_of77) : $(SHARE)/$(OBJ)/%.o : $(SHARE)/src/%.f
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(SHARE)/$(MOD) $< -o $@
$(gen_of90) : $(SHARE)/$(OBJ)/%.o : $(SHARE)/src/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(SHARE)/$(MOD) $< -o $@


#.. Compile local files
#..
loc_of90=$(foreach a,$(loc_ff90),$(OBJ)/$(a).o)
$(loc_of90) : $(OBJ)/%.o : src/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTION) $(MOD) -I $(SHARE)/$(MOD) $< -o $@

loc_oc  =$(foreach a,$(loc_fc),$(OBJ)/$(a).o)
$(loc_oc) : $(OBJ)/%.o : src/%.c
	$(CC) $(CC_OPTS) $< -o $@

#.. Link
#..
$(BIN)/$(PRG_NAME) : $(loc_of90) $(gen_of77) $(gen_of90) $(loc_oc)
	$(FC) $(LINK_OPTS) -o $(BIN)/$(PRG_NAME) $(loc_of90) $(gen_of77) $(gen_of90) $(loc_oc) 

#.. Clean
#..
clean :
	rm -f $(OBJ)/*.o $(MOD)/*.mod


#.. Dependencies between shared files
#.. 
$(SHARE)/$(OBJ)/ModuleRepresentation.o : $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                 $(SHARE)/$(OBJ)/ModuleMatrix.o 
$(SHARE)/$(OBJ)/ModuleBasis.o : $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                        $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                        $(SHARE)/$(OBJ)/ModuleString.o \
	                        $(SHARE)/$(OBJ)/ModuleGaussianBSpline.o \
	                        $(SHARE)/$(OBJ)/ModuleGaussian.o \
	                        $(SHARE)/$(OBJ)/ModuleParameterList.o \
	                        $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                        $(SHARE)/$(OBJ)/ModuleBSpline.o 
$(SHARE)/$(OBJ)/ModuleBSpline.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                          $(SHARE)/$(OBJ)/ModuleParameterList.o\
	                          $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleSimpleLapack.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                               $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleGaussian.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                           $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                           $(SHARE)/$(OBJ)/ModuleSimpleLapack.o \
	                           $(SHARE)/$(OBJ)/ModuleParameterList.o 
$(SHARE)/$(OBJ)/ModuleGaussianBSpline.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                                  $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                  $(SHARE)/$(OBJ)/ModuleSimpleLapack.o \
	                                  $(SHARE)/$(OBJ)/ModuleGaussian.o \
	                                  $(SHARE)/$(OBJ)/ModuleBSpline.o \
	                                  $(SHARE)/$(OBJ)/ModuleString.o \
	                                  $(SHARE)/$(OBJ)/ModuleParameterList.o 
$(SHARE)/$(OBJ)/ModuleGeneralInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModuleScatteringStatesInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModulePropagationInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o \
                                               $(SHARE)/$(OBJ)/ModuleConstants.o
$(SHARE)/$(OBJ)/ModuleAnalPropResInputFile.o : $(SHARE)/$(OBJ)/ModuleParameterList.o
$(SHARE)/$(OBJ)/ModuleMatrix.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                         $(SHARE)/$(OBJ)/ModuleString.o 
$(SHARE)/$(OBJ)/ModuleParameterList.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o
$(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o : $(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                      $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                                      $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                      $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                                      $(SHARE)/$(OBJ)/ModuleRepresentation.o 

$(SHARE)/$(OBJ)/krylovx.o     : $(SHARE)/$(OBJ)/wave_function_mod.o
$(SHARE)/$(OBJ)/gsorbm.o      : $(SHARE)/$(OBJ)/safeio.o
$(SHARE)/$(OBJ)/parser.o      : $(SHARE)/$(OBJ)/safeio.o
$(SHARE)/$(OBJ)/gifm.o        : $(SHARE)/$(OBJ)/parser.o
$(SHARE)/$(OBJ)/wave_function_mod.o        : $(SHARE)/$(OBJ)/symba.o
$(SHARE)/$(OBJ)/basem.o     : $(SHARE)/$(OBJ)/parser.o
$(SHARE)/$(OBJ)/ModuleBase.o    : $(SHARE)/$(OBJ)/parser.o
$(SHARE)/$(OBJ)/propm.o     : $(SHARE)/$(OBJ)/parser.o
$(SHARE)/$(OBJ)/ModuleAbsorptionPotential.o   : $(SHARE)/$(OBJ)/ModuleBasis.o \
	                                        $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
						$(SHARE)/$(OBJ)/ModuleSystemUtils.o \
	                                        $(SHARE)/$(OBJ)/ModuleMatrix.o \
	                                        $(SHARE)/$(OBJ)/ModuleString.o \
						$(SHARE)/$(OBJ)/ModuleRepresentation.o
$(SHARE)/$(OBJ)/specfun.o : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
	                    $(SHARE)/$(OBJ)/cnormena1.o
$(SHARE)/$(OBJ)/pulsem.o  : $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleErrorHandling.o

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicSpace.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricElectronicChannel.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o \
                            $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o \
                            $(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o

$(SHARE)/$(OBJ)/ModuleSymmetricElectronicChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o \
                            $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/ModuleInteractionBlocks.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o \
                            $(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o

$(SHARE)/$(OBJ)/ModuleGroups.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o 

$(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o 

$(SHARE)/$(OBJ)/ModuleInteractionBlocks.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleShortRangeChannel.o \
                            $(SHARE)/$(OBJ)/ModulePartialWaveChannel.o \
                            $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o \
                            $(SHARE)/$(OBJ)/symba.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o

$(SHARE)/$(OBJ)/ModuleParentIons.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParameterList.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o 

$(SHARE)/$(OBJ)/ModuleSymmetricLocalizedStates.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o

$(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o


$(SHARE)/$(OBJ)/ModuleShortRangeChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o \
                            $(SHARE)/$(OBJ)/symba.o \
	                    $(SHARE)/$(OBJ)/ModuleSimpleLapack.o

$(SHARE)/$(OBJ)/ModulePartialWaveChannel.o  : $(SHARE)/$(OBJ)/ModuleErrorHandling.o \
                            $(SHARE)/$(OBJ)/ModuleString.o \
                            $(SHARE)/$(OBJ)/ModuleConstants.o \
                            $(SHARE)/$(OBJ)/ModuleGroups.o \
                            $(SHARE)/$(OBJ)/ModuleSymmetryAdaptedSphericalHarmonics.o \
	                    $(SHARE)/$(OBJ)/ModuleBasis.o \
                            $(SHARE)/$(OBJ)/ModuleParentIons.o \
                            $(SHARE)/$(OBJ)/ModuleMatrix.o \
                            $(SHARE)/$(OBJ)/ModulePrepareFilesSystem.o \
                            $(SHARE)/$(OBJ)/symba.o \
	                    $(SHARE)/$(OBJ)/ModuleAbsorptionPotential.o \
                            $(SHARE)/$(OBJ)/ModuleBasicMatrixElements.o


