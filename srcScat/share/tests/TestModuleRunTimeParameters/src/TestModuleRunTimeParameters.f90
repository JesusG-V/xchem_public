!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

!> Test functionality of the Module that handles
!! run time parameters 
program TestModuleRunTimeParameters

  use ISO_FORTRAN_ENV
  
  implicit none
  
  integer            :: NEner, NIter
  real(kind(1d0))    :: Emin, Emax
  character(len=512) :: FLabel

  call GetRunTimeParameters( NEner, Emin, Emax, NIter, FLabel )

  write(OUTPUT_UNIT,*) NEner
  write(OUTPUT_UNIT,*) Emin
  write(OUTPUT_UNIT,*) Emax
  write(OUTPUT_UNIT,*) NIter
  write(OUTPUT_UNIT,*) trim(FLabel)
  
  stop
  

contains
  
 
  subroutine GetRunTimeParameters(&
       NumberOfEnergies   , &
       Emin               , &
       Emax               , &
       NumberOfIterations , &
       FileSuffix           &
       )
    !
    use ModuleRunTimeParameterManager
    use ModuleRunTimeParameterList
    !
    implicit none
    !
    integer         , intent(out) :: NumberOfEnergies
    real(kind(1d0)) , intent(out) :: Emin
    real(kind(1d0)) , intent(out) :: Emax
    integer         , intent(out) :: NumberOfIterations
    character(len=*), intent(out) :: FileSuffix
    !
    type( ClassRunTimeParameterList ) :: RunTimeParameterList
    character(len=512) :: Message
    !
    !.. Define and read the Run Time Parameters
    call InitRunTimeParameters( RunTimeParameterList )
    !
    !.. Get the value of the Run Time Parameters
    call RunTimeParameterList%GetValue( rtp_id_ne   , NumberOfEnergies   )
    call RunTimeParameterList%GetValue( rtp_id_emin , Emin               )
    call RunTimeParameterList%GetValue( rtp_id_emax , Emax               )
    call RunTimeParameterList%GetValue( rtp_id_iter , NumberOfIterations )
    call RunTimeParameterList%GetValue( rtp_id_label, FileSuffix         )
!!$    !
!!$    if( NumberOfEnergies <= 0 )then
!!$       !
!!$       Message=trim(RunTimeParameterList%Name( rtp_id_ne ))//" must be positive"
!!$       call ErrorMessage(Message)
!!$       STOP
!!$       !
!!$    endif
    !
    return
    !
  end subroutine GetRunTimeParameters


  !> Write a message to standard error
  subroutine ErrorMessage(Message)
    use ISO_FORTRAN_ENV
    implicit none
    character(len=*), intent(in) :: Message
    write(ERROR_UNIT,"(a)") Message
    return
  end subroutine ErrorMessage


end program TestModuleRunTimeParameters
