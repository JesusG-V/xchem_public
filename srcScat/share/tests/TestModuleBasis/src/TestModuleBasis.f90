!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program TestModuleBasis

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleSystemUtils
  use ModuleErrorHandling
  use ModuleBasis

  implicit none

  procedure(D2DFun), pointer :: FunPtr
  type(ClassBasis) :: Basis
  

  integer        , parameter :: NPTS        = 501
  DoublePrecision, parameter :: MIN_RADIUS  = 0.d0
  DoublePrecision, parameter :: MAX_RADIUS  = 1.d0
  DoublePrecision            :: XGRID(NPTS) = 0.d0
  DoublePrecision            :: YGRID(NPTS) = 0.d0
  integer :: i, iFun, uid
  integer, parameter :: LMAX=2
  DoublePrecision :: sbtvec(LMAX+1)=0.d0

  call Basis%Init("base")
  call Basis%WriteFingerprint(OUTPUT_UNIT)
  write(OUTPUT_UNIT,"(a)")"'"//Basis%BestPattern()//"'"
  write(OUTPUT_UNIT,"(a,i)")"Lower Bandwidth: ",Basis%LowerBandwidth()
  write(OUTPUT_UNIT,"(a,i)")"Upper Bandwidth: ",Basis%UpperBandwidth()
  write(OUTPUT_UNIT,*) Basis%CauchyIntegral(7,8,0.5d0)
  
  open(NewUnit=uid,File="BasisEval",Form="Formatted",Status="Unknown")
  FunPtr => Unity
  XGRID=[(MIN_RADIUS+(MAX_RADIUS-MIN_RADIUS)*dble(i-1)/dble(NPTS-1),i=1,NPTS)]
  do iFun=1,Basis%GetNFun()
     call Basis%SphericalBesselTransform(1.d0,iFun,LMAX,sbtvec)
     write(OUTPUT_UNIT,"(*(x,f24.16))") &
          Basis%Integral(FunPtr,iFun,iFun),&
          1.d0/(Basis%GetNormFactor(iFun))**2,&
          Basis%MonoIntegral(FunPtr,iFun),&
          Basis%Multipole(iFun,iFun,0.5d0,0),&
          Basis%Multipole(iFun,iFun,iFun,iFun,0),&
          (sbtvec(i),i=1,LMAX+1)
     call Basis%Tabulate(NPTS,XGRID,YGRID,iFun)
     do i=1,NPTS
        write(uid,"(*(x,f24.16))")XGRID(i),YGRID(i)
     enddo
     write(uid,*)
  enddo
  close(uid)
  
contains
  
  DoublePrecision function Unity(x)
    DoublePrecision, intent(in) :: x
    Unity=1.d0
  end function Unity
  
end program
