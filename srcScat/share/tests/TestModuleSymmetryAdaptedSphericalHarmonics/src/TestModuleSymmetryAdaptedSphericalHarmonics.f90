!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program testModuleSymmetryAdaptedSphericalHarmonics



  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleGroups


  implicit none



  type( ClassXlmSet )              :: XlmSet
  type( ClassXlmSymmetricSet ), pointer :: XlmSymSet => NULL()
  type(ClassCartesianSymmetricSet) :: CartSet
  type(ClassIrrep), pointer :: Irrep => NULL()
  !
  integer, parameter  :: Lmax = 2
  integer, allocatable :: MonomialExp(:,:)
  !
  complex(kind(1d0)), allocatable :: XlmToCart(:,:)
  complex(kind(1d0)), allocatable :: XlmToSpher(:,:)
  complex(kind(1d0)), allocatable :: SpherToCart(:,:)
  complex(kind(1d0)), allocatable :: array(:,:), array2(:,:)
  complex(kind(1d0)), allocatable :: ArrayExpan(:,:)
  !
  integer, allocatable :: Indexes(:,:), MVector(:) 

  
  call GlobalGroup.init("D2h")
  !
  call XlmSet.Init( GlobalGroup, Lmax )
  !
  XlmSymSet =>  XlmSet.GetSymSet("Ag")
  !
  Irrep => XlmSymSet.GetIrrep()
  !
  call XlmSet.Show()
  

  allocate( MonomialExp(3,7) )
  !
  MonomialExp(1,1) = 0; MonomialExp(2,1) = 0; MonomialExp(3,1) = 0
  !
  MonomialExp(1,2) = 2; MonomialExp(2,2) = 0; MonomialExp(3,2) = 0
  !
  MonomialExp(1,3) = 1; MonomialExp(2,3) = 1; MonomialExp(3,3) = 0
  !
  MonomialExp(1,4) = 1; MonomialExp(2,4) = 0; MonomialExp(3,4) = 1
  !
  MonomialExp(1,5) = 0; MonomialExp(2,5) = 2; MonomialExp(3,5) = 0
  !
  MonomialExp(1,6) = 0; MonomialExp(2,6) = 1; MonomialExp(3,6) = 1
  !
  MonomialExp(1,7) = 0; MonomialExp(2,7) = 0; MonomialExp(3,7) = 2


!!$  call CartSet.Init( Lmax, Irrep, MonomialExp )
  call CartSet.Init( Lmax, Irrep )
  call CartSet.ComputeTransfMat( )

  !
  call CartSet.Show()
  !

!!$  call CartSet.FetchXlmToCartesianMatrix( array )
  call CartSet.FetchCartesianToXlmMatrix( array )


!!$  call PrintsMatrixOnScreen( SpherToCart )
!!$  call PrintsMatrixOnScreen( XlmToSpher )
!!$  call PrintsMatrixOnScreen( XlmToCart )
  call PrintsMatrixOnScreen( array )


  call CartSet.FetchXlmTransfIndexes( Indexes )
  !
  write(*,*) Indexes  

  
  call XlmSet.GetCartesianExpan( CartSet, "Ag", 2, MVector, ArrayExpan )
  !
  write(*,*) "M Vector", MVector
  !
  call PrintsMatrixOnScreen( ArrayExpan )
  !
  write(*,*)


!!$  call CartSet.FetchCartesianToXlmMatrix( array2 )
!!$  !
!!$  call PrintsMatrixOnScreen( array2 )





contains




  subroutine PrintsMatrixOnScreen( Mat )
    !
    complex(kind(1d0)), intent(in) :: Mat(:,:)
    !
    integer :: i, j, unit
    real(kind(1d0)) :: RealPart, ImagPart
    !
    unit = OUTPUT_UNIT
    !
    do j = 1, size(Mat,2)
       do i = 1, size(Mat,1)
          !
          RealPart = dble(Mat(i,j))
          ImagPart = aimag(Mat(i,j))
          write(unit,fmt='(2i2,2f24.16)') i, j, RealPart, ImagPart
          !
       end do
    end do
    !
  end subroutine PrintsMatrixOnScreen




end program testModuleSymmetryAdaptedSphericalHarmonics
