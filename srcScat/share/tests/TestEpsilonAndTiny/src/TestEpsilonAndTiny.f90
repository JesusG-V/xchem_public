!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program ProgramEpsilon
  implicit none
  DoublePrecision :: a, b, eps, tin
  DoublePrecision :: aPlus, aMinus
  read(*,*) a
  eps=epsilon(a)
  tin=tiny(a)*(1.d0+eps)
  aPlus =(a+tin)*(1.d0+eps)
  aMinus=(a-tin)*(1.d0-eps)
  write(*,*) a,eps,tin
  write(*,*) aPlus, aPlus -a
  write(*,*) aMinus,aMinus-a
end program ProgramEpsilon
