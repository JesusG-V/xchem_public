!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program TestModuleBspline

  use ISO_FORTRAN_ENV
  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleBspline
  use spline
  use ModuleString

  implicit none

  integer, parameter :: DEFAULT_NNODES=11
  integer, parameter :: DEFAULT_ORDER =7
  integer            :: NNODES
  integer            :: ORDER
  DoublePrecision, parameter   :: MIN_RADIUS = 0.d0
  DoublePrecision, parameter   :: MAX_RADIUS = 1.d0
  DoublePrecision, allocatable :: GRID(:)
  DoublePrecision :: SPAN

  type(ClassBspline) :: BSplineSet
  type(spline_type)  :: swav

  integer, parameter :: NPTS = 501
  DoublePrecision    :: XGRID(NPTS) = 0.d0
  DoublePrecision    :: YGRID(NPTS) = 0.d0

  integer :: i,Bs,uid

  DoublePrecision :: t1, t2
  character(len=*), parameter :: TimeFormat="(x,f24.16)"
  procedure(D2DFun), pointer  :: funPtr

  integer, parameter :: MAX_DERIVATIVE_ORDER =  2
  integer, parameter :: MIN_RADIUS_EXPONENT  = -2
  integer, parameter :: MAX_RADIUS_EXPONENT  =  4
  integer, parameter :: MAX_LMULTIPOLE  =  30

  integer :: RadiusExponent
  integer :: BraDerivative
  integer :: KetDerivative

  logical, parameter :: TEST_NORM_FACTORS     = .false.
  logical, parameter :: PLOT_BSPLINES         = .TRUE.
  logical, parameter :: TEST_SPLINE_INTEGRAL  = .false.
  logical, parameter :: TEST_BASIC_INTEGRALS  = .false.
  logical, parameter :: TEST_SPLINE_TINTEGRAL = .FALSE.
  logical, parameter :: TEST_SPLINE_PINTEGRAL = .FALSE.
  logical, parameter :: TEST_MONOELECTRONIC_MULTIPOLE   = .FALSE.
  logical, parameter :: PRINT_MATRIX_ELEMENTS = .FALSE.
  logical, parameter :: INIT_BSPLINE_FROM_CONFIGURATION_FILE = .TRUE.

  DoublePrecision :: TrunkRadius, LowerBound, UpperBound
  DoublePrecision :: SingularRadius
  Character, parameter :: uplo = "l"
  integer :: iostat, lmultipole
  DoublePrecision :: NewNormFact, OldNormFact


  !.. Initialize the BSpline set
  t1=GetTime()
  if(INIT_BSPLINE_FROM_CONFIGURATION_FILE)then
     call BSplineSet%Init("BSplineBasis",iostat=iostat)
     !
     !.. The following is used to initialize the spline_type
     !   of the old spline module for comparison
     NNODES=BSplineSet%GetNNodes()
     ORDER =BSplineSet%GetOrder()
     allocate(GRID(NNODES))
     do i=1,NNODES
        GRID(i)=BSplineSet%GetNodePosition(i)
     enddo
     !
  else
     !.. Set the grid of nodes
     NNODES=DEFAULT_NNODES
     ORDER =DEFAULT_ORDER
     SPAN=(MAX_RADIUS-MIN_RADIUS)/dble(NNODES-1)
     allocate(GRID(NNODES))
     GRID(1)=MIN_RADIUS
     do i=2, NNODES
        GRID(i)=GRID(i-1)+SPAN
     enddo
     call BSplineSet%Init(NNODES,ORDER,GRID,iostat)
     !
  endif
  t2=GetTime()
  if(iostat/=0) write(*,*)iostat
  write(OUTPUT_UNIT,"(a,"//TimeFormat//")") "Init ClassBSpline Time:",t2-t1

  !.. Set the plot grid
  XGRID(1)   =BSplineSet%GetNodePosition(1)
  XGRID(NPTS)=BSplineSet%GetNodePosition(BSplineSet%GetNNodes())
  do i=2,NPTS-1
!!$     XGRID(i)=GRID(1)+(GRID(NNODES)-GRID(1))*dble(i-1)/dble(NPTS-1)
     XGRID(i)=XGRID(1)+(XGRID(NPTS)-XGRID(1))*dble(i-1)/dble(NPTS-1)
  enddo
  
  !
  t1=GetTime()
  if(init_spline(NNODES,ORDER,GRID,swav)/=0)call Assert("spline_type initialization failed")
  t2=GetTime()
  write(OUTPUT_UNIT,"(a,"//TimeFormat//")") "Init spline_type  Time:",t2-t1
  !
  ! -- The new implementation takes the same as the old one until the
  !    normalization constants are computed. This latter task takes much
  !    less in the old implementation (spline_type), which is singularly
  !    fast for some special simple integrands, like 1 (for the overlap).
  !


  !.. Check Normalization factors
  if(TEST_NORM_FACTORS)then
     do Bs=1,swav%t
        OldNormFact=swav%f(Bs)
        NewNormFact=BSplineSet%GetNormFactor(Bs)
        write(OUTPUT_UNIT,"(i4,3(x,d24.16))")Bs,OldNormFact,NewNormFact,NewNormFact-OldNormFact
     enddo
  endif


  !.. Plots the B-splines
  if( PLOT_BSPLINES )then
     open(NewUnit=uid,file="tmp_ClassBSpline",form="formatted",status="unknown")
     t1=GetTime()
     do Bs=1,BSplineSet%GetNBSplines()
        call BSplineSet%Tabulate(NPTS,XGRID,YGRID,Bs)
        do i=1,NPTS
           write(uid,"(2(x,f24.16))")XGRID(i),YGRID(i)
        enddo
        write(uid,"(a)")" "
     enddo
     t2=GetTime()
     close(uid)
     write(OUTPUT_UNIT,"(a,"//TimeFormat//")") "Plot ClassBSpline Time:",t2-t1

     open(NewUnit=uid,file="tmp_spline_type",form="formatted",status="unknown")
     t1=GetTime()
     do Bs=1,swav%t
        call Tabulate_Spline(NPTS,XGRID,YGRID,Bs,swav)
        do i=1,NPTS
           write(uid,"(2(x,f24.16))")XGRID(i),YGRID(i)
        enddo
        write(uid,"(a)")" "
     enddo
     t2=GetTime()
     close(uid)
     write(OUTPUT_UNIT,"(a,"//TimeFormat//")") "Plot spline_type  Time:",t2-t1
  endif
  !
  ! -- The two implementations run at pretty much the same speed, and return 
  !    identical results. The old implementation has a bug in the evaluation
  !    of the BSplines outside the grid intervals.




  !.. Compare the matrix elements with polynomial functions
  funPtr => LocalPotential
  if(TEST_SPLINE_INTEGRAL )then
     write(OUTPUT_UNIT,"(a)")"Extensive test elementary integrals"
     write(OUTPUT_UNIT,"(a)")"BraDerivative, KetDerivative, RadiusExponent, Old Time, New Time, NewTime/OldTime"
     do BraDerivative = 0, MAX_DERIVATIVE_ORDER
        do KetDerivative = 0, MAX_DERIVATIVE_ORDER
           do RadiusExponent = MIN_RADIUS_EXPONENT, MAX_RADIUS_EXPONENT
              write(OUTPUT_UNIT,"(3(x,i3))",advance="no")BraDerivative, KetDerivative, RadiusExponent
              call PrintComparison( &
                   BraDerivative, RadiusExponent, KetDerivative, funPtr, &
                   BSplineSet, swav, PRINT_MATRIX_ELEMENTS )
           enddo
        enddo
     enddo
  endif
  !
  if(TEST_BASIC_INTEGRALS )then
     write(OUTPUT_UNIT,"(a)")"Basic Elementary integrals"
     write(OUTPUT_UNIT,"(a)")"BraDerivative, KetDerivative, RadiusExponent, Old Time, New Time, NewTime/OldTime"
     !
     call BasicIntegralComparison(0,0,-2)
     call BasicIntegralComparison(0,0,-1)
     call BasicIntegralComparison(0,0, 0)
     call BasicIntegralComparison(0,0, 1)
     call BasicIntegralComparison(0,1, 0)
     call BasicIntegralComparison(0,2, 0)
     !
  endif
  !-- Overall, the basic integrals are computed seven times faster with
  !   the new method. This is because, even if the integrals with r^0
  !   are much faster in the older implementation, all the others are
  !   waaaay slower. 
  !   Furthermore, the integral <B|d/dr|B> is of the order of 10^{-15}
  !   in the new implementation, while it was of the order of 10^{-12}
  !   in the old one, which represents a significant gain in accuracy.


  if(TEST_SPLINE_TINTEGRAL)then
     TrunkRadius = 0.5d0 * swav%g(swav%n/2) + 0.5d0 * swav%g(swav%n/2+1)
     write(OUTPUT_UNIT,"(a,x,f24.16)")"Truncation Radius :",TrunkRadius
     write(OUTPUT_UNIT,"(a)")"BraDerivative, KetDerivative, RadiusExponent, Old Time, New Time, NewTime/OldTime"
     do BraDerivative = 0, MAX_DERIVATIVE_ORDER
        do KetDerivative = 0, MAX_DERIVATIVE_ORDER
           do RadiusExponent = MIN_RADIUS_EXPONENT, MAX_RADIUS_EXPONENT
              write(OUTPUT_UNIT,"(3(x,i3))",advance="no")BraDerivative, KetDerivative, RadiusExponent
              call PrintComparison( &
                   BraDerivative, RadiusExponent, KetDerivative, funPtr, &
                   BSplineSet, swav, PRINT_MATRIX_ELEMENTS, TrunkRadius, Uplo )
           enddo
        enddo
     enddo
  endif
  !-- Excellent agreement found


  if(TEST_SPLINE_PINTEGRAL)then
     call cp_spline_type(swav,piggys)
     SingularRadius = 0.5d0 * swav%g(swav%n/2) + 0.5d0 * swav%g(swav%n/2+1)
     write(OUTPUT_UNIT,"(a,x,f24.16)")"Singular Radius :",SingularRadius
     call PrintPComparison( BSplineSet, swav, PRINT_MATRIX_ELEMENTS, SingularRadius )
  endif
  !-- More than excellent agreement found. 
  !   Furthermore, the new version is twice as fast as the old one,
  !   the larger the BSpline basis, the better.


  if(TEST_MONOELECTRONIC_MULTIPOLE)then
     SingularRadius = 0.5d0 * swav%g(swav%n/2) + 0.5d0 * swav%g(swav%n/2+1)
     write(OUTPUT_UNIT,"(a,x,f24.16)")"Multipolar Radius :",SingularRadius
     do lmultipole=0,MAX_LMULTIPOLE
        call PrintMonoComparison( BSplineSet, swav, SingularRadius, lmultipole, PRINT_MATRIX_ELEMENTS )
     enddo
  end if
  !-- Excellent agreement. The new version is twice as fast
  !   as the old one.



  stop

contains

  subroutine BasicIntegralComparison(i,j,k)
    integer, intent(in) :: i,j,k
    BraDerivative=i
    KetDerivative=j
    RadiusExponent=k
    write(OUTPUT_UNIT,"(3(x,i3))",advance="no")BraDerivative, KetDerivative, RadiusExponent
    call PrintComparison( &
         BraDerivative, RadiusExponent, KetDerivative, funPtr, &
         BSplineSet, swav, PRINT_MATRIX_ELEMENTS )
  end subroutine BasicIntegralComparison

  subroutine PrintComparison( dol, nr, dor, funPtr, BSplineSet, swav, PrintMatrixElements , TrunkRadius, uplo_ )
    !
    integer                     , intent(in) :: dol, nr, dor
    procedure(D2DFun) , pointer , intent(in) :: funPtr
    type(ClassBSpline)          , intent(in) :: BSplineSet
    type(spline_type)           , intent(in) :: swav
    logical           , optional, intent(in) :: PrintMatrixElements
    DoublePrecision   , optional, intent(in) :: TrunkRadius
    character         , optional, intent(in) :: uplo_
    !
    integer         :: Bs1,Bs2,Bs2mi,Bs2ma
    character       :: uplo
    DoublePrecision, allocatable :: mat1(:,:), mat2(:,:)
    DoublePrecision :: t1, t2, OldTime, NewTime
    DoublePrecision, save :: TotalOldTime = 0.d0
    DoublePrecision, save :: TotalNewTime = 0.d0
    integer         :: NumberOfNonZeroElements
    DoublePrecision :: MaxValue
    DoublePrecision :: MaxDeviation
    DoublePrecision :: MeanDeviation
    DoublePrecision :: MeanSquareDeviation
    DoublePrecision :: MeanRelativeDeviation
    DoublePrecision, parameter :: MATRIX_ELEMENT_THRESHOLD=1.d-14
    logical, parameter :: COMPARE_RESULTS = .TRUE.

    uplo="l"; if(present(uplo_)) uplo = uplo_

    !.. Evaluate some integrals the old way
    allocate(mat1(-swav%k+1:swav%k-1,Swav%t))
    mat1=0.d0
    if(present(TrunkRadius))then
       t1=GetTime()
       do Bs1=1,Swav%t
          Bs2mi=max(Bs1-swav%k+1,1)
          Bs2ma=min(Bs1+swav%k-1,Swav%t)
          do Bs2=Bs2mi,Bs2ma
             mat1(Bs2-Bs1,Bs1)=Spline_TIntegral(Bs1,Bs2,dol,nr,dor,swav,TrunkRadius,uplo)
          enddo
       enddo
       t2=GetTime()
    else
       t1=GetTime()
       do Bs1=1,swav%t
          Bs2mi=max(Bs1-swav%k+1,1)
          Bs2ma=min(Bs1+swav%k-1,swav%t)
          do Bs2=Bs2mi,Bs2ma
             mat1(Bs2-Bs1,Bs1)=Spline_Integral(Bs1,Bs2,dol,nr,dor,swav)
          enddo
       enddo
       t2=GetTime()
    endif
    OldTime=t2-t1
    TotalOldTime=TotalOldTime+OldTime
    write(OUTPUT_UNIT,TimeFormat,advance="no") OldTime


    !.. Evaluate some integrals the new way
    allocate(mat2(-BsplineSet%GetOrder()+1:BsplineSet%GetOrder()-1,BsplineSet%GetNBSplines()))
    mat2=0.d0
    if(present(TrunkRadius))then
       if(uplo=="l")then
          LowerBound=BSplineSet%GetNodePosition(1)
          UpperBound=TrunkRadius
       else
          LowerBound=TrunkRadius
          UpperBound=BSplineSet%GetNodePosition(BsplineSet%GetNNodes())
       endif
       t1=GetTime()
       do Bs1=1,BSplineSet%GetNBSplines()
          Bs2mi=max(Bs1-BSPlineSet%GetOrder()+1,1)
          Bs2ma=min(Bs1+BSplineSet%GetOrder()-1,BSplineSet%GetNBsplines())
          do Bs2=Bs2mi,Bs2ma
             mat2(Bs2-Bs1,Bs1)=BSplineSet%Integral(&
                  funPtr,Bs1,Bs2,dol,dor,LowerBound,UpperBound)
          enddo
       enddo
       t2=GetTime()
    else
       t1=GetTime()
       do Bs1=1,BSplineSet%GetNBSplines()
          Bs2mi=max(Bs1-BSPlineSet%GetOrder()+1,1)
          Bs2ma=min(Bs1+BSplineSet%GetOrder()-1,BSplineSet%GetNBsplines())
          do Bs2=Bs2mi,Bs2ma
             mat2(Bs2-Bs1,Bs1)=BSplineSet%Integral(funPtr,Bs1,Bs2,dol,dor)
          enddo
       enddo
       t2=GetTime()
    endif
    NewTime=t2-t1
    TotalNewTime=TotalNewTime+NewTime
    write(OUTPUT_UNIT,TimeFormat,advance="no") NewTime

    write(OUTPUT_UNIT,TimeFormat,advance="no") NewTime/OldTime

    write(OUTPUT_UNIT,TimeFormat,advance="no") TotalOldTime
    write(OUTPUT_UNIT,TimeFormat,advance="no") TotalNewTime
    write(OUTPUT_UNIT,TimeFormat) TotalOldTime/TotalNewTime

    !.. Compare the results
    if( COMPARE_RESULTS )then
       NumberOfNonZeroElements=&
            (&
            BsplineSet%GetNBsplines()-&
            BsplineSet%GetOrder()+1)*(2*BSplineSet%GetOrder()-1)
       MaxValue=max(maxval(abs(mat1)),maxval(abs(mat2)))
       MaxDeviation=maxval(abs(mat1-mat2))
       MeanDeviation=sum(abs(mat1-mat2))/dble(NumberOfNonZeroElements)
       MeanSquareDeviation=sqrt(sum((mat1-mat2)**2)/dble(NumberOfNonZeroElements))
       MeanRelativeDeviation=&
            sum( abs(mat1-mat2)/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD ) / &
            sum( abs(mat1     )/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD )
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxValue             :", MaxValue
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxDeviation         :", MaxDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanDeviation        :", MeanDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanSquareDeviation  :", MeanSquareDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanRelativeDeviation:", MeanRelativeDeviation
    endif

    if( present(PrintMatrixElements) .and. PrintMatrixElements )then
       do Bs1=1,BSplineSet%GetNBsplines()
          Bs2mi=max(Bs1-BSplineSet%GetOrder()+1,1)
          Bs2ma=min(Bs1+BSplineSet%GetOrder()-1,BSplineSet%GetNBsplines())
          do Bs2=Bs2mi,Bs2ma
             if(max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))<=1.d-13)cycle
             write(OUTPUT_UNIT,"(2(x,i5),4(x,d24.16))")&
                  Bs1,Bs2,&
                  mat1(Bs2-Bs1,Bs1),mat2(Bs2-Bs1,Bs1),&
                  mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1),&
                  (mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1))/max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))
          enddo
       enddo
    end if

    deallocate(mat1,mat2)


  end subroutine PrintComparison



  subroutine PrintMonoComparison( BSplineSet, swav, SingularRadius, lmultipole, PrintMatrixElements )
    !
    type(ClassBSpline)        , intent(in) :: BSplineSet
    type(spline_type)         , intent(in) :: swav
    DoublePrecision           , intent(in) :: SingularRadius
    integer                   , intent(in) :: lmultipole
    logical        , optional , intent(in) :: PrintMatrixElements
    !
    integer :: Bs1,Bs2,Bs2mi,Bs2ma
    character :: uplo
    DoublePrecision, allocatable :: mat1(:,:), mat2(:,:)
    DoublePrecision :: t1, t2, OldTime, NewTime
    integer         :: NumberOfNonZeroElements
    DoublePrecision :: MaxValue
    DoublePrecision :: MaxDeviation
    DoublePrecision :: MeanDeviation
    DoublePrecision :: MeanSquareDeviation
    DoublePrecision :: MeanRelativeDeviation
    DoublePrecision, parameter :: MATRIX_ELEMENT_THRESHOLD=1.d-14
    logical, parameter :: COMPARE_RESULTS = .TRUE.


    !.. Evaluate some integrals the old way
    write(*,*) "Fixed Radius=",SingularRadius
    write(*,*) "multipole   =",lmultipole
    allocate(mat1(-Swav%k+1:Swav%k-1,Swav%t))
    mat1=0.d0
    t1=GetTime()
    do Bs1=1,Swav%t
       Bs2mi=max(Bs1-Swav%k+1,1)
       Bs2ma=min(Bs1+Swav%k-1,Swav%t)
       do Bs2=Bs2mi,Bs2ma
          mat1(Bs2-Bs1,Bs1)=Spline_MonoMultipole(&
               Bs1,Bs2,lmultipole,swav,SingularRadius)
       enddo
    enddo
    t2=GetTime()
    OldTime=t2-t1
    write(OUTPUT_UNIT,TimeFormat,advance="no") OldTime


    !.. Evaluate some integrals the new way
    allocate(mat2(-Swav%k+1:Swav%k-1,Swav%t))
    mat2=0.d0
    t1=GetTime()
    do Bs1=1,Swav%t
       Bs2mi=max(Bs1-Swav%k+1,1)
       Bs2ma=min(Bs1+Swav%k-1,Swav%t)
       do Bs2=Bs2mi,Bs2ma
          mat2(Bs2-Bs1,Bs1)=BSplineSet%Multipole(&
               Bs1,Bs2,SingularRadius,lmultipole)
       enddo
    enddo
    t2=GetTime()
    NewTime=t2-t1
    write(OUTPUT_UNIT,TimeFormat,advance="no") NewTime

    write(OUTPUT_UNIT,TimeFormat) NewTime/OldTime

    !.. Compare the results
    if( COMPARE_RESULTS )then
       NumberOfNonZeroElements=(Swav%t-Swav%k+1)*(2*Swav%k-1)
       MaxValue=max(maxval(abs(mat1)),maxval(abs(mat2)))
       MaxDeviation=maxval(abs(mat1-mat2))
       MeanDeviation=sum(abs(mat1-mat2))/dble(NumberOfNonZeroElements)
       MeanSquareDeviation=sqrt(sum((mat1-mat2)**2)/dble(NumberOfNonZeroElements))
       MeanRelativeDeviation=&
            sum( abs(mat1-mat2)/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD ) / &
            sum( abs(mat1     )/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD )
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxValue             :", MaxValue
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxDeviation         :", MaxDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanDeviation        :", MeanDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanSquareDeviation  :", MeanSquareDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanRelativeDeviation:", MeanRelativeDeviation
    endif

    if( present(PrintMatrixElements) .and. PrintMatrixElements )then
       do Bs1=1,Swav%t
          Bs2mi=max(Bs1-Swav%k+1,1)
          Bs2ma=min(Bs1+Swav%k-1,Swav%t)
          do Bs2=Bs2mi,Bs2ma
             if(max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))<=0.d0)cycle
             write(OUTPUT_UNIT,"(2(x,i5),4(x,d24.16))")&
                  Bs1,Bs2,&
                  mat1(Bs2-Bs1,Bs1),mat2(Bs2-Bs1,Bs1),&
                  mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1),&
                  (mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1))/&
                  max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))
          enddo
       enddo
    end if

    deallocate(mat1,mat2)


  end subroutine PrintMonoComparison


  subroutine PrintPComparison( BSplineSet, swav, PrintMatrixElements , SingularRadius )
    !
    type(ClassBSpline)        , intent(in) :: BSplineSet
    type(spline_type)         , intent(in) :: swav
    logical        , optional , intent(in) :: PrintMatrixElements
    DoublePrecision, optional , intent(in) :: SingularRadius
    !
    integer :: Bs1,Bs2,Bs2mi,Bs2ma
    character :: uplo
    DoublePrecision, allocatable :: mat1(:,:), mat2(:,:)
    DoublePrecision :: t1, t2, OldTime, NewTime
    integer         :: NumberOfNonZeroElements
    DoublePrecision :: MaxValue
    DoublePrecision :: MaxDeviation
    DoublePrecision :: MeanDeviation
    DoublePrecision :: MeanSquareDeviation
    DoublePrecision :: MeanRelativeDeviation
    DoublePrecision, parameter :: MATRIX_ELEMENT_THRESHOLD=1.d-14
    logical, parameter :: COMPARE_RESULTS = .TRUE.


    !.. Evaluate some integrals the old way
    write(*,*) "SingularRadius=",SingularRadius
    allocate(mat1(-Swav%k+1:Swav%k-1,Swav%t))
    mat1=0.d0
    t1=GetTime()
    do Bs1=1,Swav%t
       Bs2mi=max(Bs1-Swav%k+1,1)
       Bs2ma=min(Bs1+Swav%k-1,Swav%t)
       do Bs2=Bs2mi,Bs2ma
          mat1(Bs2-Bs1,Bs1)=Spline_PIntegral(&
               Bs1,Bs2,SingularRadius,swav)
       enddo
    enddo
    t2=GetTime()
    OldTime=t2-t1
    write(OUTPUT_UNIT,TimeFormat,advance="no") OldTime


    !.. Evaluate some integrals the new way
    allocate(mat2(-Swav%k+1:Swav%k-1,Swav%t))
    mat2=0.d0
    t1=GetTime()
    do Bs1=1,Swav%t
       Bs2mi=max(Bs1-Swav%k+1,1)
       Bs2ma=min(Bs1+Swav%k-1,Swav%t)
       do Bs2=Bs2mi,Bs2ma
          mat2(Bs2-Bs1,Bs1)=BSplineSet%CauchyIntegral(&
               Bs1,Bs2,SingularRadius)
       enddo
    enddo
    t2=GetTime()
    NewTime=t2-t1
    write(OUTPUT_UNIT,TimeFormat,advance="no") NewTime

    write(OUTPUT_UNIT,TimeFormat) NewTime/OldTime

    !.. Compare the results
    if( COMPARE_RESULTS )then
       NumberOfNonZeroElements=(Swav%t-Swav%k+1)*(2*Swav%k-1)
       MaxValue=max(maxval(abs(mat1)),maxval(abs(mat2)))
       MaxDeviation=maxval(abs(mat1-mat2))
       MeanDeviation=sum(abs(mat1-mat2))/dble(NumberOfNonZeroElements)
       MeanSquareDeviation=sqrt(sum((mat1-mat2)**2)/dble(NumberOfNonZeroElements))
       MeanRelativeDeviation=&
            sum( abs(mat1-mat2)/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD ) / &
            sum( abs(mat1     )/abs(mat1), abs(mat1) > MATRIX_ELEMENT_THRESHOLD )
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxValue             :", MaxValue
       write(OUTPUT_UNIT,"(a,d24.16)") "MaxDeviation         :", MaxDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanDeviation        :", MeanDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanSquareDeviation  :", MeanSquareDeviation
       write(OUTPUT_UNIT,"(a,d24.16)") "MeanRelativeDeviation:", MeanRelativeDeviation
    endif

    if( present(PrintMatrixElements) .and. PrintMatrixElements )then
       do Bs1=1,Swav%t
          Bs2mi=max(Bs1-Swav%k+1,1)
          Bs2ma=min(Bs1+Swav%k-1,Swav%t)
          do Bs2=Bs2mi,Bs2ma
             if(max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))<=0.d0)cycle
             write(OUTPUT_UNIT,"(2(x,i5),4(x,d24.16))")&
                  Bs1,Bs2,&
                  mat1(Bs2-Bs1,Bs1),mat2(Bs2-Bs1,Bs1),&
                  mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1),&
                  (mat1(Bs2-Bs1,Bs1)-mat2(Bs2-Bs1,Bs1))/&
                  max(abs(mat1(Bs2-Bs1,Bs1)),abs(mat2(Bs2-Bs1,Bs1)))
          enddo
       enddo
    end if

    deallocate(mat1,mat2)


  end subroutine PrintPComparison


  Pure DoublePrecision function LocalPotential(x)
    DoublePrecision, intent(in) :: x
    LocalPotential=Pow(x,RadiusExponent)
  end function LocalPotential


  Pure DoublePrecision function Pow(x,n)
    DoublePrecision, intent(in) :: x
    integer        , intent(in) :: n
    integer :: i
    Pow=1.d0
    if(n==0)return
    Pow=0.d0
    if(abs(x)<tiny(x))return
    Pow=1.d0
    do i=1,n
       Pow=Pow*x
    enddo
    do i=n,-1
       Pow=Pow/x
    enddo
  end function Pow


end program TestModuleBspline
