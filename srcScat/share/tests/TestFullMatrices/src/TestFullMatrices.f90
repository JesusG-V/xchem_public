!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program TestFullMatrices
 
 
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleMatrix
  use ifport

  implicit none



  complex(kind(1d0)), allocatable :: S(:,:), H(:,:), S_A1_A1(:,:), S_A1_A2(:,:), S_A2_A1(:,:), S_A2_A2(:,:), ArrayLU(:,:)
  complex(kind(1d0)), allocatable :: AuxRow(:,:), Row1S(:,:), Row2S(:,:), Row3S(:,:),WholeS(:,:), WholeS2(:,:), CompRow1(:,:), CompRow2(:,:), S_TestRow1(:,:), S_TestRow2(:,:)
  character(len=:), allocatable :: FileS
  character(len=:), allocatable :: FileH
  character(len=:), allocatable :: FileEigValS
  character(len=:), allocatable :: FileEigValS_Whole
  character(len=:), allocatable :: FileEigValS_A1_A1, FileEigValS_A1_A2, FileEigValS_A2_A1, FileEigValS_A2_A2, FileTestSrow1, FileTestSrow2
  character(len=:), allocatable :: FileEigValH
  type(ClassComplexMatrix) :: Overlap, Hamiltonian, Overlap_A1_A1, Overlap_A1_A2, Overlap_A2_A1, Overlap_A2_A2, ScatteringMat, AuxScatteringMat, InvScatteringMat, BMat, BinvMat, CMat, CopyBMat
  type(ClassMatrix) :: ScatteringMatReal
  type(ClassComplexMatrix) :: OverlapWhole, TestMat
  type(ClassComplexSpectralResolution) :: OverSpecRes, HamSpecRes, OverSpecRes_A1_A1, OverSpecRes_A1_A2, OverSpecRes_A2_A1, OverSpecRes_A2_A2
  type(ClassComplexSpectralResolution) :: OverlapSpecResWhole
  !
  character(len=:), allocatable :: root
  !
  !
  character(len=:), allocatable :: root_FileS_A1_A1
  character(len=:), allocatable :: root_FileS_A1_A2
  character(len=:), allocatable :: root_FileS_A2_A1
  character(len=:), allocatable :: root_FileS_A2_A2
  !
  character(len=:), allocatable :: root_FileH_A1_A1
  character(len=:), allocatable :: root_FileH_A1_A2
  character(len=:), allocatable :: root_FileH_A2_A1
  character(len=:), allocatable :: root_FileH_A2_A2
  !
  !
  character(len=:), allocatable :: FileS_A1_A1
  character(len=:), allocatable :: FileS_A1_A2
  character(len=:), allocatable :: FileS_A2_A1
  character(len=:), allocatable :: FileS_A2_A2
  !
  character(len=:), allocatable :: FileH_A1_A1
  character(len=:), allocatable :: FileH_A1_A2
  character(len=:), allocatable :: FileH_A2_A1
  character(len=:), allocatable :: FileH_A2_A2
  !
  !
  character(len=:), allocatable :: File_IndS_A1_A1
  character(len=:), allocatable :: File_IndS_A1_A2
  character(len=:), allocatable :: File_IndS_A2_A1
  character(len=:), allocatable :: File_IndS_A2_A2
  !
  !
  character(len=:), allocatable :: FileRow1S, FileRow2S, FileRow3S, FileRow1H, FileRow2H
  !
  !
  logical :: AreEqual
  !
  !
  integer :: NumEnergies, info
  real(kind(1d0)) :: Emin, Emax
  real(kind(1d0)), allocatable :: Energy(:)
  !
  complex(kind(1d0)), allocatable :: CompEnerg(:)
  complex(kind(1d0)), allocatable :: ScattArray(:,:), SqrScattArray(:,:), CondScattArray(:,:)
  integer, allocatable :: ipiv(:), ipivReal(:)
  complex(kind(1d0)), allocatable :: VectorRHS(:,:), EigenVector(:)
  character(len=:), allocatable :: Method
  logical :: Homogeneous
  integer :: i, uid, iostat, j, NumScattStates, k
  complex(kind(1d0)), parameter :: Z0 = cmplx(0.d0,0.d0)
  complex(kind(1d0)), parameter :: Z1 = cmplx(1.d0,0.d0)
  complex(kind(1d0)), parameter :: Zi = cmplx(0.d0,1.d0)
  complex(kind(1d0)), allocatable :: ToyMat(:,:), P(:,:), C(:,:), Binv(:,:), FinalEigVec(:,:), FinalEigVec2(:,:), Residue(:,:), SqrSubHArray(:,:), SqrSubSArray(:,:), CSubHArray(:,:), CSubSArray(:,:), Sqr(:,:), ResVec(:)
  real(kind(1d0)), allocatable :: ToyMatReal(:,:), VectorRHSreal(:,:), EigenVectorReal(:), AuxArray(:,:), ImagSol(:), RealSol(:), FullEigenVectorReal(:,:), FullImagSol(:,:), FullRealSol(:,:)
  type(ClassMatrix) :: RealScatt, ImagScatt, InvRealScatt, AuxMat
  real(Kind(1d0)) :: MaxReal, MaxImag, MinReal, MinImag
  type(ClassComplexMatrix) :: CopyScattMat, CopyFinalEigVec, TMat, THMat, SqrSubH, SqrSubS, CSubH, CSubS, CopyFinalEigVecAdjoint
  real(kind(1d0)) :: VecNorm
  !
  character(len=:), allocatable :: FileHSpecRes
  integer :: NumPIEnergies
  real(kind(1d0)), allocatable :: PIEnergies(:)
  integer, allocatable :: NumXlmByPI(:), PIindex(:)
  integer :: Count, Counter, NScatt
  type(ClassComplexMatrix) :: EigValCoeffMat, EigValCoeffMatAdjoint, LastColumnMat, SolMat, ProdMat, EnergMat, EnergMatInv, CopyProdMat
  complex(kind(1d0)), allocatable :: EigValCoeff(:,:), LastColumn(:,:), Sol(:,:), EigenEnergies(:)
  !
  integer :: NumPI, NumChannels, NumMolOrb, NumDifOrb, NumBs, TotNumXlm, NumBsDiag
  character(len=:), allocatable :: InputFile
  !
  !
  real(kind(1d0)), parameter :: CondNumber = 1.d-4
  logical, parameter :: MyMethod = .false.
  logical, parameter :: Preconditioning = .true.
  logical, parameter :: PreconditioningVariant = .false.
  logical, parameter :: HomogeneousSystem = .false.


!!$  NumPI       = 2
!!$  NumChannels = 1 !*** NumXlm
!!$  NumMolOrb   = 5
!!$  NumDifOrb   = 159
!!$  NumBs       = 787 !***Same number used in diagonalization.

!!$  allocate( InputFile, source = "InputFile_He_New_Cart" )
  allocate( InputFile, source = "InputFile_2Se" )
!!$  allocate( InputFile, source = "InputFile_He_New" )
!  allocate( InputFile, source = "InputFile_He_New_L3" )
!!$  allocate( InputFile, source = "InputFile_He_New_2px2py2pz_AllXlm" )
!!$  allocate( InputFile, source = "InputFile_He_New_2sBetter_AllXlm" )
!!$  allocate( InputFile, source = "InputFile_He_New_1s_AllXlm" )
!!$  allocate( InputFile, source = "InputFile_He_New_2px2py2pz" )
!!$  allocate( InputFile, source = "InputFile_He_New_700" )
!!$  allocate( InputFile, source = "InputFile_He_1s2s_X00_test" )
  call ParseInputFile( InputFile, NumPI, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
  !
  write(*,*) "NumPI", NumPI
  write(*,*) "NumXlmByPI", NumXlmByPI
  write(*,*) "NumMolOrb", NumMolOrb
  write(*,*) "NumDifOrb", NumDifOrb
  write(*,*) "NumBs", NumBs
  !
  !
  TotNumXlm = 0
  do i = 1, NumPI
     TotNumXlm = TotNumXlm + NumXlmByPI(i)
  end do
  !
  NumBsDiag = NumBs - 1
  !
  NumEnergies = 250
  Emin = -126.9d0
  Emax = -126.3d0
  !
  !
  allocate( Energy(NumEnergies) )
  call BuildGrid( NumEnergies, Emin, Emax, Energy )

  allocate(CompEnerg(NumEnergies) )
  do i = 1, NumEnergies
     CompEnerg(i) = Energy(i) * Z1
  end do

  allocate( Method, source = "LU" )
  !

  allocate( root, source = "../../DataStructure/Storage/R1.000/Structure/1A/" )

!!$  allocate( root_FileS_A1_A1, source = root//"2A.1_2A.1/SRC_SRC/S" )
!!$  allocate( root_FileS_A1_A2, source = root//"2A.1_2A.2/SRC_SRC/S" )
!!$  allocate( root_FileS_A2_A1, source = root//"2A.2_2A.1/SRC_SRC/S" )
!!$  allocate( root_FileS_A2_A2, source = root//"2A.2_2A.2/SRC_SRC/S" )
!!$  !
!!$  allocate( root_FileH_A1_A1, source = root//"2A.1_2A.1/SRC_SRC/H" )
!!$  allocate( root_FileH_A1_A2, source = root//"2A.1_2A.2/SRC_SRC/H" )
!!$  allocate( root_FileH_A2_A1, source = root//"2A.2_2A.1/SRC_SRC/H" )
!!$  allocate( root_FileH_A2_A2, source = root//"2A.2_2A.2/SRC_SRC/H" )


!!$  allocate( FileS_A1_A1, source = "../../PreparePETScMatrices/2A.1_2A.1_SRC_SRC_S" )
!!$  allocate( FileS_A1_A2, source = "../../PreparePETScMatrices/2A.1_2A.2_SRC_SRC_S" )
!!$  allocate( FileS_A2_A1, source = "../../PreparePETScMatrices/2A.2_2A.1_SRC_SRC_S" )
!!$  allocate( FileS_A2_A2, source = "../../PreparePETScMatrices/2A.2_2A.2_SRC_SRC_S" )
!!$  !
!!$  allocate( FileH_A1_A1, source = "../../PreparePETScMatrices/2A.1_2A.1_SRC_SRC_H" )
!!$  allocate( FileH_A1_A2, source = "../../PreparePETScMatrices/2A.1_2A.2_SRC_SRC_H" )
!!$  allocate( FileH_A2_A1, source = "../../PreparePETScMatrices/2A.2_2A.1_SRC_SRC_H" )
!!$  allocate( FileH_A2_A2, source = "../../PreparePETScMatrices/2A.2_2A.2_SRC_SRC_H" )


!!$  !..Compares the subblocks
!!$
!!$  call CompareMatFromFile( root_FileS_A1_A1, FileS_A1_A1 )
!!$  call CompareMatFromFile( root_FileS_A1_A2, FileS_A1_A2 )
!!$  call CompareMatFromFile( root_FileS_A2_A1, FileS_A2_A1 )
!!$  call CompareMatFromFile( root_FileS_A2_A2, FileS_A2_A2 )
!!$  !
!!$  call CompareMatFromFile( root_FileH_A1_A1, FileH_A1_A1 )
!!$  call CompareMatFromFile( root_FileH_A1_A2, FileH_A1_A2 )
!!$  call CompareMatFromFile( root_FileH_A2_A1, FileH_A2_A1 )
!!$  call CompareMatFromFile( root_FileH_A2_A2, FileH_A2_A2 )

!!$
!!$
!!$
!!$  allocate( File_IndS_A1_A1, source = "../../PreparePETScMatrices/2A.1_2A.1_S" )
!!$  allocate( FileEigValS_A1_A1, source = "EigValS_A1_A1" )
!!$  call ReadFullMatrix( S_A1_A1, File_IndS_A1_A1 )
!!$  Overlap_A1_A1 = S_A1_A1
!!$  write(*,*) "Diagonalizing Overlap_A1_A1 ..."
!!$  call Overlap_A1_A1.Diagonalize( OverSpecRes_A1_A1 )
!!$  call OverSpecRes_A1_A1.WriteEigenvalues( FileEigValS_A1_A1, 1 )
!!$  !
!!$  allocate( File_IndS_A1_A2, source = "../../PreparePETScMatrices/2A.1_2A.2_S" )
!!$  allocate( FileEigValS_A1_A2, source = "EigValS_A1_A2" )
!!$  call ReadFullMatrix( S_A1_A2, File_IndS_A1_A2 )
!!$  Overlap_A1_A2 = S_A1_A2
!!$  write(*,*) "Diagonalizing Overlap_A1_A2 ..."
!!$  call Overlap_A1_A2.Diagonalize( OverSpecRes_A1_A2 )
!!$  call OverSpecRes_A1_A2.WriteEigenvalues( FileEigValS_A1_A2, 1 )
!!$  !
!!$  allocate( File_IndS_A2_A1, source = "../../PreparePETScMatrices/2A.2_2A.1_S" )
!!$  allocate( FileEigValS_A2_A1, source = "EigValS_A2_A1" )
!!$  call ReadFullMatrix( S_A2_A1, File_IndS_A2_A1 )
!!$  Overlap_A2_A1 = S_A2_A1
!!$  write(*,*) "Diagonalizing Overlap_A2_A1 ..."
!!$  call Overlap_A2_A1.Diagonalize( OverSpecRes_A2_A1 )
!!$  call OverSpecRes_A2_A1.WriteEigenvalues( FileEigValS_A2_A1, 1 )
!!$  !
!!$  allocate( File_IndS_A2_A2, source = "../../PreparePETScMatrices/2A.2_2A.2_S" )
!!$  allocate( FileEigValS_A2_A2, source = "EigValS_A2_A2" )
!!$  call ReadFullMatrix( S_A2_A2, File_IndS_A2_A2 )
!!$  Overlap_A2_A2 = S_A2_A2
!!$  write(*,*) "Diagonalizing Overlap_A2_A2 ..."
!!$  call Overlap_A2_A2.Diagonalize( OverSpecRes_A2_A2 )
!!$  call OverSpecRes_A2_A2.WriteEigenvalues( FileEigValS_A2_A2, 1 )



!!$  allocate( FileS, source = "../../PreparePETScMatrices/FullS" )
!!$  allocate( FileH, source = "../../PreparePETScMatrices/FullH" )
  allocate( FileS, source = "FullS" )
  allocate( FileH, source = "FullH" )
  allocate( FileEigValS, source = "EigValS" )
  allocate( FileEigValH, source = "EigValH" )
  allocate( FileHSpecRes, source = "H_SpecRes" )



!!$  allocate( FileRow1S, source = "../../PreparePETScMatrices/AuxSpionRow1" )
!!$  allocate( FileRow2S, source = "../../PreparePETScMatrices/AuxSpionRow2" )
!!$  allocate( FileRow3S, source = "../../PreparePETScMatrices/AuxSpionRow3" )
  !allocate( FileRow1S, source = "../../PreparePETScMatrices/2A.1_S" )
  !allocate( FileRow2S, source = "../../PreparePETScMatrices/2A.2_S" )
  !allocate( FileRow3S, source = "../../PreparePETScMatrices/2A.3_S" )
!!$  !
  !allocate( FileRow1H, source = "../../PreparePETScMatrices/2A.1_H" )
  !allocate( FileRow2H, source = "../../PreparePETScMatrices/2A.2_H" )
!!$  allocate( FileEigValS_Whole, source = "EigValS_Whole" )
!!$  !
!!$  call ReadFullMatrix( Row1S, FileRow1S )
!!$  call ReadFullMatrix( Row2S, FileRow2S )
!!$  call ReadFullMatrix( Row3S, FileRow3S )
!!$  !
!!$  call ComposeWholeMat( Row1S, Row2S, "Rows", WholeS )
!!$  call ComposeWholeMat( WholeS, Row3S, "Rows", WholeS2 )
!!$  !
!!$  OverlapWhole = WholeS2
!!$  write(*,*) OverlapWhole.NRows(), OverlapWhole.NColumns()
!!$  !
!!$  call OverlapWhole.Diagonalize( OverlapSpecResWhole )
!!$  call OverlapSpecResWhole.WriteEigenvalues( FileEigValS_Whole, 1 )
!!$
!!$stop



  call ReadFullMatrix( S, FileS )
  write(*,*) "Read S",size(S,1),size(S,2)
!!$  Overlap = S
  call ConvertToScattMat( S, NumPI, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )

  Overlap = S(1:size(S,1)-TotNumXlm,1:size(S,1))
!!$  Overlap = S(1:size(S,1)-TotNumXlm,1:size(S,1)-TotNumXlm)
  write(*,*) "SIZE",Overlap.NRows(), Overlap.NColumns()


!!$  write(*,*) "Diagonalizing Overlap ..."
!!$  call Overlap.Diagonalize( OverSpecRes )
!!$  call OverSpecRes.WriteEigenvalues( FileEigValS, 1 )



!!$  call ComposeWholeMat( S_A1_A1, S_A1_A2, "Columns", CompRow1 )
!!$  call ComposeWholeMat( S_A2_A1, S_A2_A2, "Columns", CompRow2 )
!!$  !
!!$  call CompareMat( Row1S, CompRow1, 1.d-12, AreEqual )
!!$  if ( .not. AreEqual ) then
!!$     write(*,*) "The Row1 S are not equal."
!!$     stop
!!$  end if
!!$  !
!!$  call CompareMat( Row2S, CompRow2, 1.d-12, AreEqual )
!!$  if ( .not. AreEqual ) then
!!$     write(*,*) "The Row2 S are not equal."
!!$     stop
!!$  end if


!!$  allocate( FileTestSrow1, source = "../../PreparePETScMatrices/S_Row1" )
!!$  allocate( FileTestSrow2, source = "../../PreparePETScMatrices/S_Row2" )
!!$  !
!!$  call ReadFullMatrix( S_TestRow1, FileTestSrow1 )
!!$  call ReadFullMatrix( S_TestRow2, FileTestSrow2 )
!!$  !
!!$  call CompareMat( Row1S, S_TestRow1, 1.d-12, AreEqual )
!!$  if ( .not. AreEqual ) then
!!$     write(*,*) "The Row1 S are not equal to test row."
!!$     stop
!!$  end if
!!$  !
!!$  !
!!$  if ( IsZero( S_TestRow2, 1.d-8 ) ) then
!!$     write(*,*) "***** S_TestRow2 is zero. *****"
!!$  else
!!$     write(*,*) "***** S_TestRow2 is not zero. *****"
!!$  end if
!!$  !
!!$  call CompareMat( Row2S, S_TestRow2, 1.d-12, AreEqual )
!!$  if ( .not. AreEqual ) then
!!$     write(*,*) "The Row2 S are not equal to test row."
!!$     stop
!!$  end if




!!$  call CompareMat( S, WholeS, 1.d-12, AreEqual )
!!$  if ( .not. AreEqual ) then
!!$     write(*,*) "The whole S are not equal."
!!$     stop
!!$  end if






  call ReadFullMatrix( H, FileH )
  write(*,*) "Read H",size(H,1),size(H,2)
!!$  Hamiltonian = H
  call ConvertToScattMat( H, NumPI, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
  Hamiltonian = H(1:size(H,1)-TotNumXlm,1:size(H,1))
!!$  Hamiltonian = H(1:size(H,1)-TotNumXlm,1:size(H,1)-TotNumXlm)
  write(*,*) "SIZE",Hamiltonian.NRows(), Hamiltonian.NColumns()


!!$  write(*,*) "Diagonalizing Hamiltonian ..."
!!$  call Hamiltonian.Diagonalize( Overlap, HamSpecRes, CondNumber, TMat )
!!$  call HamSpecRes.WriteEigenvalues( FileEigValH, 1 )
!!$  stop
!!$  !




  !..Builds the Scattering Matrix subblock matrix that is supposed
  !  to be hermitic, to see if it is invertible.
  !
  write(*,*) "Building Scattering Matrix ..."
  !
  

  if ( .not.Preconditioning ) then
     !
     if ( MyMethod ) then
        call OpenFile( "NullSpace_Lapack_Mine", uid, "write", "formatted" )
     elseif ( HomogeneousSystem ) then
        call OpenFile( "NullSpace_Lapack_HomSys", uid, "write", "formatted" )
     else
        call OpenFile( "NullSpace_Lapack", uid, "write", "formatted" )
     end if
     !
  else
     !
     call OpenFile( "NullSpace_Preconditioning", uid, "write", "formatted" )
     !
  end if
  !
  write(uid,fmt=*,IOSTAT=iostat) NumEnergies



  if ( Preconditioning ) then
     !
     call HamSpecRes.Read( FileHSpecRes )
     call HamSpecRes.Fetch( EigValCoeff )
     !
     call Permute( EigValCoeff, &
          NumPI, &
          NumXlmByPI, &
          NumMolOrb, &
          NumDifOrb, &
          NumBsDiag )
     !
     EigValCoeffMat = EigValCoeff
!!$     write(*,*) "EigVec",EigValCoeffMat.NRows(), EigValCoeffMat.NColumns()
     deallocate( EigValCoeff )
     call EigValCoeffMat.TransposeConjugate( EigValCoeffMatAdjoint )
!!$     write(*,*) "EigVecTC",EigValCoeffMatAdjoint.NRows(), EigValCoeffMatAdjoint.NColumns()
     call HamSpecRes.Fetch( EigenEnergies )
!!$     write(*,*) "EigenEnergies", size(EigenEnergies)
!!$stop
     !
  end if


  !
  do i = 1, NumEnergies
     !
     write(*,*) i, "from", NumEnergies, "energies ..."
     !
     ScatteringMat = Overlap
     !
     call ScatteringMat.Multiply(-CompEnerg(i))
     call ScatteringMat.Add( Hamiltonian )

     !
!!$     call Overlap.Free()
!!$     call Hamiltonian.Free()
     !
     !***
!!$     call Overlap.Im( ImagScatt )
!!$     write(*,*) "Im(Overlap) is zero", ImagScatt.IsZero()
!!$     call ImagScatt.Free()
!!$     call Hamiltonian.Im( ImagScatt )
!!$     write(*,*) "Im(Hamiltonian) is zero", ImagScatt.IsZero()
!!$     call ImagScatt.Free()
!!$     write(*,*) Overlap.IsZero(1.d-4), Hamiltonian.IsZero(1.d-4)
!!$     stop
!!$     !***
     !
!!$     write(*,*) "rows, columns", ScatteringMat.NRows(), ScatteringMat.NColumns()
     !


!!$  allocate( ToyMat(3,3) )
!!$  ToyMat(1,1) = 3.d0 * Z1
!!$  ToyMat(1,2) = 1.d0 * Z1
!!$  ToyMat(1,3) = 1.d0 * Zi
!!$  ToyMat(2,1) = 1.d0 * Zi
!!$  ToyMat(2,2) = 2.d0 * Z1
!!$  ToyMat(2,3) = 1.d0 * Z1
!!$  ToyMat(3,1) = 1.d-100 * Z1
!!$  ToyMat(3,2) = 1.d-100 * Z1
!!$  ToyMat(3,3) = 1.d-100 * Z1
!!$  !
!!$  allocate( ToyMatReal(3,3) )
!!$  ToyMatReal(1,1) = 3.d0
!!$  ToyMatReal(1,2) = 1.d0
!!$  ToyMatReal(1,3) = 1.d0
!!$  ToyMatReal(2,1) = 1.d0
!!$  ToyMatReal(2,2) = 2.d0
!!$  ToyMatReal(2,3) = 1.d0
!!$  ToyMatReal(3,1) = 1.d-50
!!$  ToyMatReal(3,2) = 1.d-50
!!$  ToyMatReal(3,3) = 1.d-50
!!$
!!$  BMat = ToyMat(1:2,1:2)
!!$  allocate( C(2,1) )
!!$  C(:,1) = ToyMat(1:2,3)
!!$  call BMat.Inverse( BinvMat )
!!$  call BinvMat.FetchMatrix( Binv )
!!$  allocate( P(2,1) )
!!$  P = matmul( Binv, C )
!!$  do i = 1, BMat.NRows()
!!$     write(*,*) "P", i, P(i,1)
!!$  end do
!!$  !
!!$  ScatteringMatReal = ToyMatReal


     !..Compute the null space
     !
!!$     allocate( VectorRHS(ScatteringMat.NRows(),1) )
!!$     allocate( VectorRHS(3,1) )
!!$     VectorRHS = cmplx(1.d-100,0.d0)
     !
!!$  allocate( VectorRHSreal(ScatteringMatReal.NRows(),1) )
!!$  VectorRHSreal = 1.d-100
!!$     allocate( VectorRHSreal(3,1) )
!!$     VectorRHSreal = 1.d-100


     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! Same procedure implemented in MUMPS.
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     if ( .not. Preconditioning ) then
        !
        if ( .not.MyMethod ) then
           !
           if ( HomogeneousSystem ) then
              !
              write(OUTPUT_UNIT,*) "Solving the homogeneous problem ..."
              !
              NScatt = ScatteringMat.NColumns()-ScatteringMat.NRows()
!!$              call ScatteringMat.SolveHomogScattExpert( ArrayLU )
              call ScatteringMat.AddRows( NScatt, "END" )
              call ScatteringMat.FetchMatrix( ScattArray )
              if ( allocated(ipiv) ) deallocate( ipiv )
              allocate( ipiv(ScatteringMat.NRows()) )
              call zgetrf( ScatteringMat.NRows(), &
                   ScatteringMat.NRows(), &
                   ScattArray, &
                   ScatteringMat.NRows(), &
                   ipiv, info )
              write(*,*) "info", info
              !
              if ( allocated(FinalEigVec) ) deallocate( FinalEigVec )
              call ComputeBackSustSol( NScatt, ScattArray, FinalEigVec )
!!$              call ComputeBackSustSol( ScatteringMat.NColumns()-ScatteringMat.NRows(), ArrayLU, FinalEigVec )
              !
              !***Test result
              CopyScattMat = ScatteringMat
              CopyFinalEigVec = FinalEigVec
              call CopyScattMat.Multiply( CopyFinalEigVec, "Right", "N" ) 
              call CopyFinalEigVec.Free()
              call CopyScattMat.FetchMatrix( Residue )
              call CopyScattMat.Free()
              do j = 1, size(Residue,2)
                 VecNorm = 0.d0
                 do k = 1, size(Residue,1)
                    VecNorm = VecNorm + dble(Residue(k,j))**2 + aimag(Residue(k,j))**2 
                 end do
                 VecNorm = sqrt(VecNorm)
                 write(output_unit,*) "Norm2 residue",j,"=",VecNorm
              end do
              deallocate( Residue )
              !
           else
              !
              write(OUTPUT_UNIT,*) "Same MUMPS method ..."
              !
              call ScatteringMat.FetchMatrix( ScattArray )
              !
              allocate( SqrScattArray(ScatteringMat.NRows(),ScatteringMat.NRows()) )
              SqrScattArray(:,:) = ScattArray(:,:ScatteringMat.NRows())
              !
              call BMat.Free()
              call BinvMat.Free()
              BMat = SqrScattArray
              call BMat.Inverse( BinvMat )
              !
              CopyBMat = BMat
              call CopyBMat.Multiply( BinvMat, "Right", "N" )
!!$        call CopyBMat.Multiply( BinvMat, "Left", "N" )
              if ( .not. CopyBMat.IsIdentity( 1.d-6 ) ) then
                 write(*,*) "Impossible to invert the matrix, program abort."
                 stop
              end if
!!$        write(*,*) CopyBMat.IsIdentity( 1.d-7 )
!!$        stop
              !***
              !
              !
              NumScattStates = ScatteringMat.NColumns() - ScatteringMat.NRows()
              if ( allocated(FinalEigVec) ) deallocate( FinalEigVec )
              !
              allocate( FinalEigVec(ScatteringMat.NColumns(),NumScattStates) )
           !
              if ( allocated(Binv) ) deallocate( Binv )
              call BinvMat.FetchMatrix( Binv )
              !
              do j = 1, NumScattStates
                 !
                 if ( allocated(C) ) deallocate( C )
                 if ( allocated(P) ) deallocate( P )
                 allocate( C(ScatteringMat.NRows(),1) )
                 C(:,1) = ScattArray(:,ScatteringMat.NRows()+j)
                 allocate( P(ScatteringMat.NRows(),1) )
                 P = matmul( Binv, C )
                 !
!!$     do i = 1, ScatteringMat.NRows()
!!$        write(*,*) "P full", i, P(i,1)
!!$     end do
                 !
                 allocate( EigenVector(ScatteringMat.NColumns())  )
                 !
                 EigenVector = Z0
                 !
                 EigenVector(ScatteringMat.NRows()+j) = -Z1  
                 !
                 EigenVector(:ScatteringMat.NRows()) = P(:,1)
                 FinalEigVec(:,j) = EigenVector(:)
                 deallocate( EigenVector )
                 !
              end do
              !
              !***Test result
              CopyScattMat = ScatteringMat
              CopyFinalEigVec = FinalEigVec
              call CopyScattMat.Multiply( CopyFinalEigVec, "Right", "N" ) 
              call CopyFinalEigVec.Free()
              call CopyScattMat.FetchMatrix( Residue )
              call CopyScattMat.Free()
              do j = 1, size(Residue,2)
                 VecNorm = 0.d0
                 do k = 1, size(Residue,1)
                    VecNorm = VecNorm + dble(Residue(k,j))**2 + aimag(Residue(k,j))**2 
                 end do
                 VecNorm = sqrt(VecNorm)
                 write(output_unit,*) "Norm2 residue",j,"=",VecNorm
              end do
              !***
              !
              call BMat.Free()
              call BinvMat.Free()
              deallocate( SqrScattArray )
              deallocate( C, P )
              !
           end if
           !
        end if
     end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
     
     
     
     !
     !  deallocate( ScattArray )
     !  call ScatteringMat.Free()
     !
     !  ScatteringMat = SqrScattArray
     !  deallocate( SqrScattArray )

     !
     if ( Method == "LU" ) then
        Homogeneous = .true.
     else
        Homogeneous = .false.
     end if
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! Procedure implemented in other programs for real matrices.
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
!!$     call ScatteringMat.Re( RealScatt )
!!$     call ScatteringMatReal.Factorize( ipivReal )  
!!$     call RealScatt.Factorize( ipivReal, Homogeneous )  
!!$     !
!!$     call RealScatt.LinEqSolver( ipivReal, VectorRHSreal, EigenVectorReal, Method )
!!$     do j = 1, size(EigenVectorReal)
!!$        write(*,*) j, EigenVectorReal(j)
!!$     end do
!!$     stop
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! Procedure done by me
     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     if ( .not.Preconditioning ) then
        if ( MyMethod ) then
           !
           if ( allocated(EigenVector) ) deallocate( EigenVector )
           if ( allocated(ScattArray) )deallocate( ScattArray )
           if ( allocated(SqrScattArray) ) deallocate( SqrScattArray )
           !
           call ScatteringMat.FetchMatrix( ScattArray )
           allocate( SqrScattArray(ScatteringMat.NColumns(),ScatteringMat.NColumns()) )
           SqrScattArray(1:ScatteringMat.NRows(),:) = ScattArray(:,:)
           SqrScattArray(ScatteringMat.NRows()+1:,:) = 1.d-100*Z1
           if ( allocated(VectorRHSreal) ) deallocate( VectorRHSreal )
           allocate( VectorRHSreal(ScatteringMat.NColumns(),ScatteringMat.NColumns()-ScatteringMat.NRows()) )
           VectorRHSreal = 1.d-100
           !
           call ScatteringMat.Free()
           ScatteringMat = SqrScattArray
           !
!!$     ScatteringMat = ToyMat
           !
           call ScatteringMat.Re( RealScatt )
           call ScatteringMat.Im( ImagScatt )
           call RealScatt.Inverse( InvRealScatt )
           !
!!$  !***
!!$  call RealScatt.Multiply( InvRealScatt, "Right", "N" )
!!$  call RealScatt.Multiply( InvRealScatt, "Left", "N" )
!!$  write(*,*) "Correct inversion", RealScatt.IsIdentity( 1.d-12 )
!!$  stop
!!$  !***
           !
!!$  if ( .false. ) then
           if ( ImagScatt.IsZero(1.d-10) ) then
              !
              write(*,*) "Scattering matrix imaginary part is zero."
              call RealScatt.Factorize( ipivReal )
!!$     call RealScatt.Factorize( ipivReal, Homogeneous )
              call RealScatt.LinEqSolver( ipivReal, VectorRHSreal, Method, FullEigenVectorReal )
              !
              MaxReal = maxval(FullEigenVectorReal)
              MinReal = minval(FullEigenVectorReal)
              MaxReal = max(abs(MaxReal),abs(MinReal))
              FullEigenVectorReal = FullEigenVectorReal/MaxReal
              !
              if ( allocated(FinalEigVec) ) deallocate( FinalEigVec )
              allocate( FinalEigVec(size(FullEigenVectorReal,1),size(FullEigenVectorReal,2)) )
              FinalEigVec = Z1 * FullEigenVectorReal
              !
              !
!!$     do j = 1, size(EigenVectorReal)
!!$        write(*,*) j, EigenVectorReal(j)
!!$     end do
              !
           else
              !
              write(*,*) "Scattering matrix imaginary part is not zero."
              AuxMat = ImagScatt
              call AuxMat.Multiply( InvRealScatt, "Right", "N" )
              call AuxMat.Multiply( ImagScatt, "Right", "N" )
!!$           call AuxMat.Multiply( InvRealScatt, "Left", "N" )
!!$           call AuxMat.Multiply( ImagScatt, "Left", "N" )
!!$     write(*,*) AuxMat.NRows(), AuxMat.NColumns(), RealScatt.NRows(), RealScatt.NColumns()
              call AuxMat.Add( RealScatt, .false. )
              !
              call AuxMat.Factorize( ipivReal, Homogeneous )
              call AuxMat.LinEqSolver( ipivReal, VectorRHSreal, Method, FullImagSol )
              !
              call AuxMat.Free()
              AuxMat = InvRealScatt
              call AuxMat.Multiply( ImagScatt, "Right", "N" )
              call AuxMat.FetchMatrix( AuxArray )
              !
              allocate( FullRealSol(size(FullImagSol,1),size(FullImagSol,2)) )
              allocate( FinalEigVec(size(FullImagSol,1),size(FullImagSol,2)) )
              FullRealSol = matmul(AuxArray,FullImagSol)
              deallocate( AuxArray )
              !
              MaxReal = maxval(FullRealSol)
              MaxImag = maxval(FullImagSol)
              MaxReal = max(MaxReal,MaxImag)
              !
              MinReal = minval(FullRealSol)
              MinImag = minval(FullImagSol)
              MinReal = min(MinReal,MinImag)
              !
              MaxReal = max(abs(MaxReal),abs(MinReal))
              !
              FullRealSol = FullRealSol/MaxReal
              FullImagSol = FullImagSol/MaxReal
              !
              do j = 1, size(FullImagSol,2)
                 do k = 1, size(FullImagSol,1)
                    FinalEigVec(k,j) = cmplx(FullRealSol(k,j),FullImagSol(k,j)) 
                 end do
              end do
              !
!!$     do j = 1, size(EigenVector)
!!$        write(*,*) j, EigenVector(j)
!!$        write(*,*) j, RealSol(j)
!!$        write(*,*) j, ImagSol(j)
!!$     end do
              !
           end if
           !
!!$  call RealScatt.Factorize( ipiv, Homogeneous ) 
!!$  call RealScatt.LinEqSolver( ipiv, VectorRHSreal, EigenVectorreal, Method )
!!$  !
!!$  do j = 1, size(EigenVectorReal)
!!$     write(*,*) j, EigenVectorReal(j)
!!$  end do
!!$  stop
           
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           !
        end if
     end if
  

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Procedure implemented by default in other programs with complex matrices
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
!!$  call ScatteringMat.Factorize( ipiv, Homogeneous ) 
!!$  write(*,*) "ipiv", ipiv
!!$  stop
  !
!!$  call ScatteringMat.LinEqSolver( ipiv, VectorRHS, EigenVector, Method )
!!$     !
!!$  do j = 1, size(EigenVector)
!!$     write(*,*) j, EigenVector(j)
!!$  end do
!!$  stop
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


     
     if ( Preconditioning ) then
        !
        NScatt = ScatteringMat.NColumns()-ScatteringMat.NRows()
        call ScatteringMat.FetchMatrix( ScattArray )
        !
        if ( allocated(FinalEigVec) ) deallocate( FinalEigVec )
        allocate( FinalEigVec(ScatteringMat.NColumns(),NScatt) )
        FinalEigVec = Z0
        !
        call BuildEnergMat( EigenEnergies, CompEnerg(i), EnergMat )
        !
        if ( PreconditioningVariant ) then
           !
           ProdMat = EigValCoeffMat
           call ProdMat.Multiply( EnergMat, "Right", "N" )
           call ProdMat.Multiply( EigValCoeffMatAdjoint, "Right", "N" )
           call ProdMat.FetchMatrix( SqrScattArray )
           !
           allocate( CondScattArray(ScatteringMat.NColumns(),ScatteringMat.NColumns()) )
           CondScattArray = Z0
           CondScattArray(1:ProdMat.NRows(),1:ProdMat.NColumns()) = SqrScattArray(:,:)
           deallocate( SqrScattArray )
           CondScattArray(1:ProdMat.NRows(),ProdMat.NColumns()+1:) = ScattArray(:,ProdMat.NColumns()+1:)
           !
           if ( allocated(ipiv) ) deallocate( ipiv )
           allocate( ipiv(size(CondScattArray,1)) )
           call zgetrf( size(CondScattArray,1), &
                size(CondScattArray,1), &
                CondScattArray, &
                size(CondScattArray,1), &
                ipiv, info )
           write(*,*) "info", info
           !
           if ( allocated(FinalEigVec) ) deallocate( FinalEigVec )
           call ComputeBackSustSol( NScatt, CondScattArray, FinalEigVec )
           deallocate( CondScattArray )
           !
        else
           !
           call EnergMat.Inverse( EnergMatInv )
           !
!!$        call EnergMat.Multiply( EnergMatInv, "Right", "N" )
!!$        write(*,*) "Sucessfull EnergMat invertion:", EnergMat.IsIdentity()
           call EnergMat.Free()
           !
           ProdMat = EigValCoeffMat
           call ProdMat.Multiply( EnergMatInv, "Right", "N" )
           call ProdMat.Multiply( EigValCoeffMatAdjoint, "Right", "N" )
           !
           do j = 1, NScatt
              !
              allocate( LastColumn(ScatteringMat.NRows(),1) )
              !
              LastColumn(:,1) = ScattArray(:,ScatteringMat.NRows()+j)
              LastColumnMat = LastColumn
              FinalEigVec(ScatteringMat.NRows()+j,j) = -Z1
              !
              CopyProdMat = ProdMat
              call CopyProdMat.Multiply( LastColumnMat, "Right", "N" )
              call CopyProdMat.FetchMatrix( Sol )
              !
              FinalEigVec(1:ScatteringMat.NRows(),j) = Sol(:,1)
              !
              deallocate( Sol )
              deallocate( LastColumn )
              call CopyProdMat.Free()
              call LastColumnMat.Free()
              !
           end do
           !
        end if
        !
        !..Test Result
        CopyScattMat = ScatteringMat
        CopyFinalEigVec = FinalEigVec
        call CopyScattMat.Multiply( CopyFinalEigVec, "Right", "N" )
        call CopyFinalEigVec.Free()
        call CopyScattMat.FetchMatrix( Residue )
        call CopyScattMat.Free()
        !
        do j = 1, size(Residue,2)
           VecNorm = 0.d0
           do k = 1, size(Residue,1)
              VecNorm = VecNorm + dble(Residue(k,j))**2 + aimag(Residue(k,j))**2
           end do
           VecNorm = sqrt(VecNorm)
           write(*,*) "Norm2 residue ",j,"=", VecNorm
        end do
        !
        deallocate( Residue )
        !
        deallocate( ScattArray )
        call ProdMat.Free()
        !
     end if


     


     call SaveLapackNullSpace( uid, Energy(i), FinalEigVec ) 
     !
     !
     !
     if ( allocated(VectorRHS) ) deallocate( VectorRHS )
     if ( allocated(ScattArray) ) deallocate( ScattArray )
     if ( allocated(SqrScattArray) ) deallocate( SqrScattArray )
     if ( allocated(Binv) ) deallocate( Binv )
     if ( allocated(C) ) deallocate( C )
     if ( allocated(P) ) deallocate( P )
     if ( allocated(EigenVector) ) deallocate( EigenVector )
     call ScatteringMat.Free()
     call BMat.Free()
     call BinvMat.Free()
     !
  end do
  !
  close( uid )











!!$  !.. Squared sub matrix
!!$  !
!!$  call ScatteringMat.FetchMatrix( ScattArray )
!!$  !
!!$  allocate( SqrScattArray(ScatteringMat.NRows(),ScatteringMat.NRows()) )
!!$  !
!!$  SqrScattArray = ScattArray(:,1:ScatteringMat.NRows())
!!$  !
!!$  deallocate( ScattArray )
!!$  call ScatteringMat.Free()
!!$  !
!!$  ScatteringMat = SqrScattArray
!!$  deallocate( SqrScattArray )
!!$  !
!!$  write(*,*) "rows, columns", ScatteringMat.NRows(), ScatteringMat.NColumns()
!!$  !
!!$  write(*,*) "Computing inversion ..."
!!$  call ScatteringMat.Inverse( InvScatteringMat )
!!$  call ScatteringMat.Multiply( InvScatteringMat, "Right", "N" )
!!$  write(*,*) ScatteringMat.IsIdentity( 1.d-3 )

write(*,*) "Program ended."

contains


  subroutine OpenFile( FileName, uid, Purpose, Form )
    !> The name of the file to be opened.
    character(len=*),           intent(in)    :: FileName
    !> Unit number associated to the opened file.
    integer,                    intent(out)   :: uid
    !> Can be "write" or "read" depending on the action needed.
    character(len=*), optional, intent(in)    :: Purpose
    !> Can be "formatted" or "unformatted".
    character(len=*), optional, intent(in)    :: Form
    !
    integer :: iostat
    character(len=256) :: iomsg
    character(len=:), allocatable :: NewPurpose, NewForm
    !
    if ( .not.present(Purpose) ) then
       allocate( NewPurpose, source = "read" )
    else
       allocate( NewPurpose, source = Purpose )
    end if
    if ( .not.present(Form) ) then
       allocate( NewForm, source = "unformatted" )
    else
       allocate( NewForm, source = Form )
    end if
    !
    OPEN(NewUnit =  uid,          &
         File    =  FileName,     &
         Form    =  NewForm,      &
         Status  = "unknown",     &
         Action  = NewPurpose,    &
         iostat  = iostat ,       &
         iomsg   = iomsg    )
    if( iostat /= 0 ) write(*,*) iomsg
  end subroutine OpenFile



  subroutine ReadFullMatrix( Mat, File )
    !
    complex(kind(1d0)), allocatable, intent(out) :: Mat(:,:)
    character(len=*),                intent(in)  :: File
    !
    integer :: uid, iostat, i, j, NRows, NCols
    !
    call OpenFile( FIle, uid, "read", "formatted" )
    !
    write(*,*) "Reading "//File
    read(uid,fmt=*,IOSTAT=iostat) NRows, NCols
    if( iostat /= 0 ) then
       write(*,*) "Error reading the size of the full matrix"
       stop
    end if
    !
    allocate( Mat(NRows,NCols) )
    !
    do j = 1, size(Mat,2)
       read(uid,fmt=*,IOSTAT=iostat) ( Mat(i,j), i=1,size(Mat,1) )
    end do
    !
    close(uid)
    ! 
  end subroutine ReadFullMatrix



  subroutine CompareMatFromFile( File1, File2 )
    !
    character(len=*), intent(in) :: File1
    character(len=*), intent(in) :: File2
    !
    complex(kind(1d0)), allocatable :: Mat1(:,:), Mat2(:,:)
    real(kind(1d0)), parameter :: Tol = 1.d-12
    logical :: Equal
    !
    call ReadFullMatrix( Mat1, File1)
    call ReadFullMatrix( Mat2, File2)
    !
    call CompareMat( Mat1, Mat2, Tol, Equal )
    !
    if ( Equal ) then
       write(*,*) " Matrices in "//File1//" and "//File2//" are equal."
    else
       write(*,*) " Matrices in "//File1//" and "//File2//" are not equal."
       write(*,*) "************************"
       stop
    end if
    !
  end subroutine CompareMatFromFile



  subroutine CompareMat( Mat1, Mat2, Tol, Equal )
    !
    complex(kind(1d0)), intent(in)  :: Mat1(:,:)
    complex(kind(1d0)), intent(in)  :: Mat2(:,:)
    real(kind(1d0)),    intent(in)  :: Tol
    logical,            intent(out) :: Equal
    !
    integer :: i, j
    real(kind(1d0)) :: Real1, Real2, Imag1, Imag2
    !
    Equal = .false.
    !
    if ( size(Mat1,1) /= size(Mat2,1) ) then
       write(*,*) "The matrices have different rows number."
       return
    end if
    !
    if ( size(Mat1,2) /= size(Mat2,2) ) then
       write(*,*) "The matrices have different columns number."
       return
    end if
    !
    !
    do j = 1, size(Mat1,2)
       do i = 1, size(Mat1,1)
          !
          Real1 = real(Mat1(i,j))
          Real2 = real(Mat2(i,j))
          Imag1 = aimag(Mat1(i,j))
          Imag2 = aimag(Mat2(i,j))
          !
          if ( abs(Real1-Real2) > Tol ) then
             write(*,*) "Real part of elements",i,j," differs:"
             write(*,*) Real1, Real2
             return
          end if
          !
          if ( abs(Imag1-Imag2) > Tol ) then
             write(*,*) "Imaginary part of elements",i,j," differs:"
             write(*,*) Imag1, Imag2, Tol
             return
          end if
          !
       end do
    end do
    !
    Equal = .true.
    !
  end subroutine CompareMat



  !..Only for two rows that makes a complete squared matrix, the first ontop of the second.
  subroutine ComposeWholeMat( Array1, Array2, Through, Mat )
    !
    complex(kind(1d0)),              intent(in)  :: Array1(:,:)
    complex(kind(1d0)),              intent(in)  :: Array2(:,:)
    character(len=*),                intent(in)  :: Through
    complex(kind(1d0)), allocatable, intent(out) :: Mat(:,:)
    !
    integer :: NR1, NR2, NC1, NC2, TotNR, TotNC, i, j
    !
    NR1 = size(Array1,1)
    NC1 = size(Array1,2)
    !
    NR2 = size(Array2,1)
    NC2 = size(Array2,2)
    !
    if ( Through == "Rows" ) then
       !
       TotNR = NR1 + NR2
       TotNC = NC1
       !
       if ( NC1 /= NC2 ) then
          write(*,*) "The matrices do not have the same number of columns, imposible to put the first one on top of the second one."
          stop
       end if
       !
       if ( TotNR /= TotNC ) then
          write(*,*) "The composed matrix is not squared, cannot be diagonalized."
       end if
       !
    elseif ( Through == "Columns" ) then
       !
       TotNR = NR1
       TotNC = NC1 + NC2
       !
       if ( NR1 /= NR2 ) then
          write(*,*) "The matrices do not have the same number of rows, imposible to put the first one on the left side of the second one."
          stop
       end if
       !
       if ( TotNR /= TotNC ) then
          write(*,*) "The composed matrix is not squared, cannot be diagonalized."
       end if
       !
    else
       write(*,*) "Invalid 'Through' matrix composition; it should be either 'Rows' or 'Columns'. Aborting mission..."
       stop
       !
    end if
    !
    write(*,*) NR1,NC1
    write(*,*) NR2,NC2

    !
    !
    allocate( Mat(TotNR,TotNC) )
    !
    if ( Through == "Rows" ) then
       !
       Mat(1:NR1,:)  = Array1
       Mat(NR1+1:,:) = Array2
       !
    elseif ( Through == "Columns" ) then
       !
       Mat(:,1:NC1)  = Array1
       Mat(:,NC1+1:)  = Array2
       !
    end if

    !
  end subroutine ComposeWholeMat



  logical function IsZero( Mat, Tol )
    !
    complex(kind(1d0)), intent(in) :: Mat(:,:)
    real(kind(1d0)),    intent(in) :: Tol
    !
    integer :: i, j
    !
    IsZero = .true.
    !
    do j = 1, size(Mat,2)
       do i = 1, size(Mat,1)
          !
          if ( abs(Mat(i,j)) > Tol ) then
             IsZero = .false.
             write(*,*) Mat(i,j)
             return
          end if
          !
       end do
    end do
    !
  end function IsZero




  subroutine SaveLapackNullSpace( FileUnit, Energy, EigenVector ) 
    !
    integer,            intent(in) :: FileUnit
    real(kind(1d0)),    intent(in) :: Energy
    complex(kind(1d0)), intent(in) :: EigenVector(:,:)
    !
    integer :: i, iostat, NumVec, j
    !
    NumVec = size(EigenVector,2)
    !
    write(FileUnit,fmt=*,IOSTAT=iostat) Energy
    write(FileUnit,fmt=*,IOSTAT=iostat) NumVec
    !
    do j = 1, NumVec
       write(FileUnit,fmt=*,IOSTAT=iostat) size(EigenVector,1)
       do i = 1, size(EigenVector,1)
          write(FileUnit,fmt=*,IOSTAT=iostat) EigenVector(i,j)
       end do
    end do
    !
  end subroutine SaveLapackNullSpace



  subroutine BuildGrid( N, Vmin, Vmax, Vector )
    !
    integer,                      intent(in)  :: N
    real(kind(1d0)),              intent(in)  :: Vmin
    real(kind(1d0)),              intent(in)  :: Vmax
    real(kind(1d0)), allocatable, intent(out) :: Vector(:)
    !
    integer :: i
    !
    allocate( Vector(N) )
    !
    if ( N == 1 ) then
       !
       Vector(1) = Vmin
       !
    else
       !
       do i = 1, N
          Vector(i) = Vmin + (Vmax-Vmin)*dble(i-1)/dble(N-1)
       end do
       !
    end if
    !
  end subroutine BuildGrid




  subroutine BuildEnergMat( EigenEnergies, Energy, EnergMat )
    !
    complex(kind(1d0)),        intent(in)  :: EigenEnergies(:)
    complex(kind(1d0)),        intent(in)  :: Energy
    class(ClassComplexMatrix), intent(out) :: EnergMat
    !
    integer :: i, j, n
    complex(kind(1d0)), allocatable :: Array(:,:)
    !
    n = size(EigenEnergies)
    allocate( Array(n,n) )
    Array = Z0
    !
    do i = 1, n
       Array(i,i) = EigenEnergies(i) - Energy
    end do
    !
    !
    EnergMat = Array
    deallocate( Array )
    !
  end subroutine BuildEnergMat



  !> Permutes the rows to achieve the same permutation carried
  !! out in the scattering matrix.
  subroutine Permute( Coeff, &
       NumPI, &
       NumXlmByPI, &
       NumMolOrb, &
       NumDifOrb, &
       NumBs )
    !
    complex(kind(1d0)), intent(inout) :: Coeff(:,:)
    integer,            intent(in)    :: NumPI
    integer,            intent(in)    :: NumXlmByPI(:)
    integer,            intent(in)    :: NumMolOrb
    integer,            intent(in)    :: NumDifOrb
    integer,            intent(in)    :: NumBs
    !
    integer :: UnpertInitColOrRow, i
    integer, allocatable :: CumNumFunByPI(:)
    !
    UnpertInitColOrRow = NumMolOrb + NumDifOrb
    !
!!$    NumFunByPI = size(Coeff,1)/NumPI
    allocate( CumNumFunByPI(0:NumPI) )
    CumNumFunByPI = 0
    do i = 1, NumPI
       CumNumFunByPI(i) = CumNumFunByPI(i-1) + UnpertInitColOrRow + NumXlmByPI(i)*NumBs
    end do
    !
    do i = 1, NumPI
!!$       call BlockPermutation( Coeff((i-1)*NumFunByPI+1:i*NumFunByPI,:), &
!!$            UnpertInitColOrRow, &
!!$            NumXlmByPI(i), &
!!$            NumBs )
       call BlockPermutation( Coeff(CumNumFunByPI(i-1)+1:CumNumFunByPI(i),:), &
            UnpertInitColOrRow, &
            NumXlmByPI(i), &
            NumBs )
    end do
    !
    call LastPermutation( Coeff, &
         NumPI, &
         UnpertInitColOrRow, &
         NumXlmByPI, &
         NumBs )
    !
  end subroutine Permute


  
  subroutine BlockPermutation( Coeff, &
       UnpertInitColOrRow, &
       NumXlm, &
       NumBs )
    !
    complex(kind(1d0)), intent(inout) :: Coeff(:,:)
    integer,            intent(in)    :: UnpertInitColOrRow
    integer,            intent(in)    :: NumXlm
    integer,            intent(in)    :: NumBs
    !
    integer :: i, j, k
    complex(kind(1d0)), allocatable :: CopyMatOrig(:,:), NewMat(:,:)
    !
    allocate( CopyMatOrig, source = Coeff )
    allocate( NewMat, source = Coeff )
    NewMat = Z0
    !
    do j = 0, NumBs - 1
       do k =1, NumXlm
          do i = 1, size(Coeff,2)
             !
             NewMat(j * NumXlm + k + UnpertInitColOrRow, i) = &
                  CopyMatOrig((k-1) * NumBs + j + 1 + UnpertInitColOrRow, i)
             !
          end do
       end do
    end do
       !
    do j = 1, UnpertInitColOrRow
       do i = 1, size(Coeff,2)
          !
          NewMat(j,i) = CopyMatOrig(j,i)
          !
       end do
    end do
    !
    Coeff = Z0
    Coeff = NewMat
    !
  end subroutine BlockPermutation



  subroutine LastPermutation( Coeff, &
       NumPI, &
       UnpertInitColOrRow, &
       NumXlmByPI, &
       NumBs )
    !
    complex(kind(1d0)), intent(inout) :: Coeff(:,:)
    integer,            intent(in)    :: NumPI
    integer,            intent(in)    :: UnpertInitColOrRow
    integer,            intent(in)    :: NumXlmByPI(:)
    integer,            intent(in)    :: NumBs
    !
    complex(kind(1d0)), allocatable :: NewMat(:,:)
    integer :: j, i, k, NumBlocks, TotalBlocksTimesXlm
    integer, allocatable ::  CumNumColRowPerChann(:), CumNumXlm(:)
    !
!!$    NumBlocks = NumPI
    NumBlocks = size(NumXlmByPI)
    !
    allocate( CumNumColRowPerChann(0:NumBlocks) )
    allocate( CumNumXlm(0:NumBlocks) )
    !
    TotalBlocksTimesXlm = 0
    CumNumColRowPerChann = 0
    CumNumXlm = 0
    do i = 1, NumBlocks
       TotalBlocksTimesXlm = TotalBlocksTimesXlm + NumXlmByPI(i)
       CumNumColRowPerChann(i) = CumNumColRowPerChann(i-1) + UnpertInitColOrRow + NumXlmByPI(i)*NumBs
       CumNumXlm(i) = CumNumXlm(i-1) + NumXlmByPI(i)
    end do
    !
    allocate( NewMat(size(Coeff,1),size(Coeff,2)) )
    NewMat = Z0
    !
    do k = 1, NumBlocks
       do j = 1, UnpertInitColOrRow
          !
!!$          NewMat((k-1)*UnpertInitColOrRow+j,:) = &
!!$               Coeff((k-1)*NumColRowPerChann+j,:)
          NewMat((k-1)*UnpertInitColOrRow+j,:) = &
               Coeff(CumNumColRowPerChann(k-1)+j,:)
          !
       end do
    end do
    !
    !
    do i = 1, NumBs
       do k = 1, NumBlocks
          do j = 1, NumXlmByPI(k)
!!$             NewMat((i-1)*NumXlm*NumBlocks + (k-1)*NumXlm + &
!!$                  NumBlocks*UnpertInitColOrRow + j,:) = &
!!$                  Coeff((i-1)*NumXlm + (k-1)*NumColRowPerChann + &
!!$                  UnpertInitColOrRow + j,:)
             NewMat((i-1)*TotalBlocksTimesXlm + CumNumXlm(k-1) + &
                  NumBlocks*UnpertInitColOrRow + j,:) = &
                  Coeff((i-1)*NumXlmByPI(k) + CumNumColRowPerChann(k-1) + &
                  UnpertInitColOrRow + j,:)
          end do
       end do
    end do
    !
    !
    Coeff = Z0
    Coeff = NewMat
    deallocate( NewMat )
    !
  end subroutine LastPermutation




  subroutine ParseInputFile( File, NumPI, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
    !
    character(len=*),     intent(in)  :: File
    integer,              intent(out) :: NumPI
    integer, allocatable, intent(out) :: NumXlmByPI(:)
    integer,              intent(out) :: NumMolOrb
    integer,              intent(out) :: NumDifOrb
    integer,              intent(out) :: NumBs
    !
    integer :: uid, i, j
    !
    call OpenFile( trim(adjustl(File)), uid, "read", "formatted" )
    !
    read(uid,*) NumPI
    do i = 1, NumPI
       read(uid,*) 
    end do
    !
    allocate( NumXlmByPI(NumPI) )
    NumXlmByPI = 0
    !
    do j = 1, NumPI
       read(uid,*) NumXlmByPI(j)
       do i = 1, NumXlmByPI(j)
          read(uid,*) 
       end do
    end do
    !
    read(uid,*) NumMolOrb
    read(uid,*) NumDifOrb
    read(uid,*) NumBs
    !
    close(uid)
    !
  end subroutine ParseInputFile


  !> Perform either the rows or columns permutations in order set
  !! first the QC subblock and after the B-splines in ascendind
  !! basis index for each Xlm i.e: Bs1X00,Bs2X10...,Bs2X00,Bs2X10...,
  !! BlastX00, BlastX10...
  subroutine PermuteBs( Matrix, Identifier, NumXlm, NumMolOrb, NumDifOrb, NumBs )
    !
    complex(kind(1d0)),    intent(inout)  :: Matrix(:,:)
    character(len=*),      intent(in)     :: Identifier
    integer,               intent(in)     :: NumXlm
    integer,               intent(in)     :: NumMolOrb
    integer,               intent(in)     :: NumDifOrb
    integer,               intent(in)     :: NumBs
    !
    complex(kind(1d0)), allocatable :: CopyMatOrig(:,:), NewMat(:,:)
    integer :: UnpertInitColOrRow, NumChannels, j, i, k
    !
    NumChannels = NumXlm
    !
    allocate( CopyMatOrig(size(Matrix,1),size(Matrix,2)) )
    CopyMatOrig = Matrix
    allocate( NewMat(size(Matrix,1),size(Matrix,2)) )
    NewMat = Z0
    !
    UnpertInitColOrRow = NumMolOrb + NumDifOrb
    !
    if ( Identifier=="Col" ) then
       !
       do j = 0, NumBs - 1
          do k =1, NumChannels
             do i = 1, size(Matrix,1)
                !
                NewMat(i,j*NumChannels + k + UnpertInitColOrRow) = &
                     CopyMatOrig(i,(k-1)*NumBs + j + 1 + UnpertInitColOrRow)
                !
             end do
          end do
       end do
       !
       do j = 1, UnpertInitColOrRow
          do i = 1, size(Matrix,1)
             !
             NewMat(i,j) = CopyMatOrig(i,j)
             !
          end do
       end do
       !
    elseif ( Identifier=="Row" ) then
       !
       do j = 0, NumBs - 1
          do k =1, NumChannels
             do i = 1, size(Matrix,2)
                !
                NewMat(j * NumChannels + k + UnpertInitColOrRow, i) = &
                     CopyMatOrig((k-1) * NumBs + j + 1 + UnpertInitColOrRow, i)
                !
             end do
          end do
       end do
       !
       do j = 1, UnpertInitColOrRow
          do i = 1, size(Matrix,2)
             !
             NewMat(j,i) = CopyMatOrig(j,i)
             !
          end do
       end do
       !
    else
       !
       write(*,*) "Invalid identified passed to the Bs permutation, "//&
            "it must be either 'Row' or 'Col'."
       stop
       !
    end if
    !
    !
    Matrix = Z0
    Matrix = NewMat
    !
  end subroutine PermuteBs



  subroutine ConvertToScattMat( Matrix, NumPI, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
    !
    complex(kind(1d0)), intent(inout) :: Matrix(:,:)
    integer,            intent(in)    :: NumPI
    integer,            intent(in)    :: NumXlmByPI(:)
    integer,            intent(in)    :: NumMolOrb
    integer,            intent(in)    :: NumDifOrb
    integer,            intent(in)    :: NumBs
    !
    complex(kind(1d0)), allocatable :: AuxMat(:,:)
    integer :: i, j, RowMin, RowMax, ColMin, ColMax
    !
    write(*,*) "Size Mat", size(Matrix,1), size(Matrix,2)
    do j = 1, NumPI
       do i = 1, NumPI
          !
          call GetSubBlockLimits( &
               i, j, &
               NumXlmByPI, &
               NumMolOrb, &
               NumDifOrb, &
               NumBs, &
               RowMin, RowMax, ColMin, ColMax )
!!$          write(*,*) i,j,RowMin, RowMax, ColMin, ColMax
          !
          call FetchBlock( &
               Matrix, AuxMat, &
               RowMin, RowMax, ColMin, ColMax )
          !
          call PermuteBs( AuxMat, "Col", NumXlmByPI(j), &
               NumMolOrb, NumDifOrb, NumBs )
          call PermuteBs( AuxMat, "Row", NumXlmByPI(i), &
               NumMolOrb, NumDifOrb, NumBs )
          !
          call SetBlock( &
               Matrix, AuxMat, &
               RowMin, RowMax, ColMin, ColMax )
          !
          deallocate( AuxMat )
          !
       end do
    end do
    !
    call FinalPermutation( Matrix, "Col", NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
    call FinalPermutation( Matrix, "Row", NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
    !
  end subroutine ConvertToScattMat




  subroutine GetSubBlockLimits( &
       RowIndex, ColIndex, &
       NumXlmByPI, &
       NumMolOrb, &
       NumDifOrb, &
       NumBs, &
       RowMin, RowMax, ColMin, ColMax )
    !
    integer, intent(in)  :: RowIndex, ColIndex
    integer, intent(in)  :: NumXlmByPI(:)
    integer, intent(in)  :: NumMolOrb
    integer, intent(in)  :: NumDifOrb
    integer, intent(in)  :: NumBs
    integer, intent(out) :: RowMin, RowMax, ColMin, ColMax
    !
    integer :: i, j, NumQCFun
    !
    NumQCFun = NumMolOrb + NumDifOrb
    !
    RowMin = 0
    RowMax = 0
    do i = 1, RowIndex-1
       RowMin = RowMin + NumQCFun + NumXlmByPI(i)*NumBs
    end do
    RowMax = RowMin + NumQCFun + NumXlmByPI(RowIndex)*NumBs
    RowMin = RowMin + 1
    !
    ColMin = 0
    ColMax = 0
    do i = 1, ColIndex-1
       ColMin = ColMin + NumQCFun + NumXlmByPI(i)*NumBs
    end do
    ColMax = ColMin + NumQCFun + NumXlmByPI(ColIndex)*NumBs
    ColMin = ColMin + 1
    
    !
  end subroutine GetSubBlockLimits



  subroutine FetchBlock( &
       Matrix, Block, &
       RowMin, RowMax, ColMin, ColMax )
    !
    complex(kind(1d0)),              intent(in)  :: Matrix(:,:)
    complex(kind(1d0)), allocatable, intent(out) :: Block(:,:)
    integer,                         intent(in)  :: RowMin, RowMax, ColMin, ColMax
    !
    integer :: NumRows, NumCols, i, j
    !
    NumRows = RowMax - RowMin + 1
    NumCols = ColMax - ColMin + 1
    !
    allocate( Block(NumRows,NumCols) )
    Block = Z0
    !
    do j = ColMin, ColMax
       do i = RowMin, RowMax
          Block(i-RowMin+1,j-ColMin+1) = Matrix(i,j)
       end do
    end do
    !
  end subroutine FetchBlock



  subroutine SetBlock( &
       Matrix, Block, &
       RowMin, RowMax, ColMin, ColMax )
    !
    complex(kind(1d0)),              intent(inout) :: Matrix(:,:)
    complex(kind(1d0)),              intent(in)    :: Block(:,:)
    integer,                         intent(in)    :: RowMin, RowMax, ColMin, ColMax
    !
    integer :: NumRows, NumCols, i, j
    !
    do j = ColMin, ColMax
       do i = RowMin, RowMax
          Matrix(i,j) = Block(i-RowMin+1,j-ColMin+1)
       end do
    end do
    !
  end subroutine SetBlock




  !.. Carries out the permutations ordening the B-splines in ascending order and puting the localized-diffused part in the first columns/rows ascending in parent ion index (as well as the B-splines are ordered).
  subroutine FinalPermutation( Array, Identifier, NumXlmByPI, NumMolOrb, NumDifOrb, NumBs )
    !
    complex(kind(1d0)), intent(inout) :: Array(:,:)
    character(len=*),   intent(in)    :: Identifier
    integer,            intent(in)    :: NumXlmByPI(:)
    integer,            intent(in)    :: NumMolOrb
    integer,            intent(in)    :: NumDifOrb
    integer,            intent(in)    :: NumBs
    !
    complex(kind(1d0)), allocatable :: NewMat(:,:)
    integer :: UnpertInitColOrRow, NumBlocks, NumXlm
    integer, allocatable :: NumColRowPerChann(:), CumNumColRowPerChann(:), CumNumXlm(:)
    integer :: j, i, k, TotalBlocksTimesXlm
    !
    if ( size(Array,1) /= size(Array,2) ) then
       write(*,*) "The matrix for the final permutation must be squared, aborting ..."
       write(*,*) size(Array,1), size(Array,2)
       stop
    end if
    !
    UnpertInitColOrRow = NumMolOrb + NumDifOrb
    NumBlocks = size(NumXlmByPI)
    allocate( NumColRowPerChann(NumBlocks) )
    allocate( CumNumColRowPerChann(0:NumBlocks) )
    allocate( CumNumXlm(0:NumBlocks) )
    !
    TotalBlocksTimesXlm = 0
    CumNumColRowPerChann = 0
    CumNumXlm = 0
    do i = 1, NumBlocks
       TotalBlocksTimesXlm = TotalBlocksTimesXlm + NumXlmByPI(i)
       NumColRowPerChann(i) = UnpertInitColOrRow + NumXlmByPI(i)*NumBs
       CumNumColRowPerChann(i) = CumNumColRowPerChann(i-1) + NumColRowPerChann(i)
       CumNumXlm(i) = CumNumXlm(i-1) + NumXlmByPI(i)
    end do
    !
    allocate( NewMat(size(Array,1),size(Array,2)) )
    NewMat = Z0
    !
    if ( Identifier =="Col" ) then
       do k = 1, NumBlocks
          do j = 1, UnpertInitColOrRow
             !
!!$             NewMat(:,(k-1)*UnpertInitColOrRow+j) = &
!!$                  Array(:,(k-1)*NumColRowPerChann(k)+j)
             NewMat(:,(k-1)*UnpertInitColOrRow+j) = &
                  Array(:,CumNumColRowPerChann(k-1)+j)
             !
          end do
       end do
    elseif ( Identifier == "Row" ) then
       do k = 1, NumBlocks
          do j = 1, UnpertInitColOrRow
             !
!!$             NewMat((k-1)*UnpertInitColOrRow+j,:) = &
!!$                  Array((k-1)*NumColRowPerChann(k)+j,:)
             NewMat((k-1)*UnpertInitColOrRow+j,:) = &
                  Array(CumNumColRowPerChann(k-1)+j,:)
             !
          end do
       end do
    else
       write(*,*) "The identifier passed to the final permutation "//&
               "must be either 'Col' or 'Row', aborting ..."
       stop
    end if
    !
    !
    !
    !
    if ( Identifier =="Col" ) then
       do i = 1, NumBs
          do k = 1, NumBlocks
             do j = 1, NumXlmByPI(k)
!!$                NewMat(:,(i-1)*TotalBlocksTimesXlm + (k-1)*NumXlmByPI(k) + &
!!$                     NumBlocks*UnpertInitColOrRow + j) = &
!!$                     Array(:,(i-1)*NumXlmByPI(k) + (k-1)*NumColRowPerChann(k) + &
!!$                     UnpertInitColOrRow + j)
                NewMat(:,(i-1)*TotalBlocksTimesXlm + CumNumXlm(k-1) + &
                     NumBlocks*UnpertInitColOrRow + j) = &
                     Array(:,(i-1)*NumXlmByPI(k) + CumNumColRowPerChann(k-1) + &
                     UnpertInitColOrRow + j)
             end do
          end do
       end do
    elseif ( Identifier == "Row" ) then
       do i = 1, NumBs
          do k = 1, NumBlocks
             do j = 1, NumXlmByPI(k)
!!$                NewMat((i-1)*TotalBlocksTimesXlm + (k-1)*NumXlmByPI(k) + &
!!$                     NumBlocks*UnpertInitColOrRow + j,:) = &
!!$                     Array((i-1)*NumXlmByPI(k) + (k-1)*NumColRowPerChann(k) + &
!!$                     UnpertInitColOrRow + j,:)
                NewMat((i-1)*TotalBlocksTimesXlm + CumNumXlm(k-1) + &
                     NumBlocks*UnpertInitColOrRow + j,:) = &
                     Array((i-1)*NumXlmByPI(k) + CumNumColRowPerChann(k-1) + &
                     UnpertInitColOrRow + j,:)
             end do
          end do
       end do
    end if
    !
    !
    Array = Z0
    Array = NewMat
    deallocate( NewMat )
    !
  end subroutine FinalPermutation




  subroutine ComputeBackSustSol( NumSol, ArrayLU, Sol )
    !
    integer,                         intent(in)  :: NumSol
    complex(kind(1d0)),              intent(in)  :: ArrayLU(:,:)
    complex(kind(1d0)), allocatable, intent(out) :: Sol(:,:)
    !
    integer :: i, j, n, NumElem
    complex(kind(1d0)) :: Sum
    !
    NumElem = size(ArrayLU,1)
    !
    allocate( Sol(NumElem,NumSol) )
    Sol = Z0
    !
    do j = 1, NumSol
       Sol(NumElem-NumSol+j,j) = -Z1
    end do
    !
    do j = 1, NumSol
       do i = NumElem-NumSol, 1, -1
          Sum = Z0
          do n = i+1, NumElem
             Sum = Sum + ArrayLU(i,n)*Sol(n,j)
          end do
          Sol(i,j) = -Sum/ArrayLU(i,i)
!!$          write(*,*) i, j, Sol(i,j), ArrayLU(i,i), Sum
       end do
    end do
    !
    !***
    write(*,*) "ArrayLU diag :"
    do i = NumElem, NumElem-NumSol, -1
       write(*,*) i, ArrayLU(i,i)
    end do
    write(*,*) HaveInfElement(ArrayLU)
    !***
    !
  end subroutine ComputeBackSustSol



  logical function HaveInfElement( Array )
    !
    complex(kind(1d0)), intent(in) :: Array(:,:)
    integer :: i, j
    real(kind(1d0)), parameter :: Threshold = 1.d100
    !
    HaveInfElement = .false.
    do j = 1, size(Array,2)
       do i = 1, size(Array,1)
          if ( abs(Array(i,j)) > Threshold ) then
             HaveInfElement = .true.
             write(*,*) "Element", i, j, Array(i,j)
             return
          end if
       end do
    end do
    !
  end function HaveInfElement



end program TestFullMatrices
