!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
! {{{ Detailed description

!> \file
!!
!! This module contains all the attributes and procedures neccesary to deal with the absorption potential.

! }}}

module ModuleAbsorptionPotential 

  use ModuleSystemUtils
  use ModuleString
  use ModuleErrorHandling
  use ModuleMatrix
  use ModuleBasis
  use ModuleRepresentation

  implicit none

  character(len=*), parameter :: DBLE_STRN_LENGTH = "14"
  character(len=*), parameter :: DBLE_STRN_FORMAT = "e"//DBLE_STRN_LENGTH//".6"


  !> The absorption potential is defined as
  !! \f[
  !! V(r)=\sum_{i=1}^{n}c_{i}\cdot\theta(r-R_{i})\cdot\left(r-R_{i}\right)^{\alpha_{i}}
  !! \f]
  !! The potential is absorbing if \f$\Im m (c_i)<0\f$.
  !!
  type, public :: ClassAbsorptionPotential
     !
     private
     !> Number of terms in the absorption potential formula summation.
     integer                         :: n
     !> Number of identical absorption potentials to be taken into account. ( Sometimes is better to the program consider the potential as a summation of several identical ones ).
     integer                         :: NumEqualPot
     !> Vector whose elements correspond to the power \f$\alpha_i\f$ in the absorption potential formula.
     Real(kind(1d0))   , allocatable :: a(:)
     !> Vector whose elements correspond to the points \f$R_i\f$ in the absorption potential formula.
     Real(kind(1d0))   , allocatable :: r(:)
     !> Vector whose elements correspond to the complex coefficients \f$c_i\f$ in the absorption potential formula.
     complex(kind(1d0)), allocatable :: c(:)
     !> Vector whose elements are [representation classes](@ref ModuleRepresentation.f90).
     type(ClassRepresentation), allocatable :: Rep(:)
     !
   contains

     !.. Accessors
     !> Retrieves the number of terms in the absorption potential summation.
     procedure :: NumberOfTerms
     !> Retrieves the number of identical potentials.
     procedure :: NumberEqualPot
     !> Retrieves the requested complex coefficient from the absorption potential. 
     procedure :: Coeff => ClassAbsorptionPotentialCoeff

     !> Parses the absorption potential configuration file.
     procedure :: Parse    => ParseConfigurationFile
     !> Initializes the representation classes vector.
     procedure :: Init     => InitAbsorptionPotential
     !> Read the previously saved absorption potential matrix elements.
     procedure :: Read     => ReadAbsorptionPotentialMatrixElem
     !> Computes and saves the absorption potential matrix elements.
     procedure :: ComputeAndSave 
     !> Fetches from ClassAbsorptionPotential the matrix elements.
     generic :: Fetch   => &
          FetchAbsorptionPotentialMatrix,&
          FetchAbsorptionPotentialComplexMatrix,&
          FetchAbsorptionPotentialAll
     !> Builds a name for the file in which the absorption potential matrix elements will be saved.
     procedure :: FileName => AbsorptionPotentialFileName
     !> Write function
     procedure :: Write    => WriteAbsorptionPotential
     !
     procedure, private :: FetchAbsorptionPotentialMatrix
     procedure, private :: FetchAbsorptionPotentialComplexMatrix
     procedure, private :: FetchAbsorptionPotentialAll
     !
  end type ClassAbsorptionPotential


contains
  
  !> Fetches from ClassAbsorptionPotential the matrix elements correspondind to the requested absorption potential among all the possibly existent.
  subroutine FetchAbsorptionPotentialMatrix( self, iPot, Matrix, L, RepLevel )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: self
    !> Absorption potential identification number.
    integer                        , intent(in) :: iPot
    !> Class of matrices.
    type(ClassMatrix)              , intent(out):: Matrix
    !> Angular momentum.
    integer, optional              , intent(in) :: L
    !> Representation Level.
    integer, optional              , intent(in) :: RepLevel 
    !
    character(len=*), parameter :: HERE="ClassAbsorptionPotential::FetchAbsorptionPotentialMatrix : "
    !
    if( ipot < 0 .or. ipot > self%NumberOfTerms() )return
    if(  (       present(RepLevel) .and. .not. present(L) ) .or.&
         ( .not. present(RepLevel) .and.       present(L) ) )&
         call Assert(HERE//"Missing argument")

    if( present( RepLevel ) )then
       call self%Rep(iPot)%GetMatrix( Matrix, L, L, RepLevel, RepLevel )
    else
       Matrix = self%Rep(iPot)%Matrix
       !call self%Rep(iPot)%GetMatrix( Matrix, 0, 0, REP_LEVEL_TOTAL, REP_LEVEL_TOTAL )
    endif
    !
  end subroutine FetchAbsorptionPotentialMatrix


  !> Adds up all the matrices corresponding to the absorption potentials.
  subroutine FetchAbsorptionPotentialAll( self, ZMatrix, L, RepLevel )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: self
    !> Class of complex matrices.
    type(ClassComplexMatrix)       , intent(out):: ZMatrix
    !> Angular momentum.
    integer, optional              , intent(in) :: L
    !> Representation Level.
    integer, optional              , intent(in) :: RepLevel

    character(len=*), parameter :: HERE="ClassAbsorptionPotential::FetchAbsorptionPotentialMatrix : "
    integer :: iPot
    type(ClassComplexMatrix) :: Potential

    do iPot = 1, self%NumberOfTerms()
       !
       call self%Fetch( ipot, Potential, L, RepLevel )
       call Potential%Multiply( self%Coeff(iPot) )
       if(iPot==1)then
          ZMatrix = Potential
       else
          call ZMatrix%Add( Potential )
       endif
       !
    enddo
    !
  end subroutine FetchAbsorptionPotentialAll



  !> Fetches from ClassAbsorptionPotential the matrix elements in complex format, correspondind to the requested absorption potential among all the possibly existent.
  subroutine FetchAbsorptionPotentialComplexMatrix( self, iPot, ZMatrix, L, RepLevel )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: self
    !> 
    !> Absorption potential identification number.
    integer                        , intent(in) :: iPot
    !> Class of complex matrices.
    type(ClassComplexMatrix)       , intent(out):: ZMatrix
    !> Angular momentum.
    integer, optional              , intent(in) :: L
    !> Representation Level.
    integer, optional              , intent(in) :: RepLevel
    !
    type(ClassMatrix) :: Matrix
    !
    call self%fetch(iPot,Matrix,L,RepLevel)
    ZMatrix=Matrix
  end subroutine FetchAbsorptionPotentialComplexMatrix

  
  !> Parses the absorption potential configuration file.
  subroutine ParseConfigurationFile( Potential, FileName )
    !
    !.. Parse the file containing the 
    !   absorption potential parameters
    !..
    implicit none
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(out) :: Potential
    !> Configuration file name.
    character(len=*)               , intent(in)  :: FileName
    !
    character(len=*)  , parameter :: Form = "FORMATTED"
    complex(kind(1d0)), parameter :: Z1 = (1.d0,0.d0)
    complex(kind(1d0)), parameter :: Zi = (0.d0,1.d0)
    !
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=512) :: line
    integer            :: i, iostat, uid, NumIdenPot
    DoublePrecision    :: Alpha, R0, ReC, ImC
    !
    !
    open(NewUnit = uid     , &
         File    = FileName, &
         Form    = Form    , &
         Action  = "read"  , &
         iostat  = iostat  , &
         iomsg   = iomsg   )
    if(iostat/=0)then
       call ErrorMessage("File "//trim(FileName)//" is not readable")
       call ErrorMessage(iomsg)
       STOP
    endif
    !
    !.. Determine the number of potentials
    !..
    rewind(uid)
    Potential%n=0
    do
       !
       read(uid,"(a)",iostat=iostat)line
       if(iostat/=0)exit
       i=index(line,"#")
       if(i>0)line(i:)=" "
       line=adjustl(line)
       if(len_trim(line)==0)cycle
       !
       Potential%n=Potential%n+1
       !
    enddo
    !
    !
    !.. Allocate the memory for parameters
    !..
    if(allocated(Potential%a))deallocate(Potential%a)
    if(allocated(Potential%r))deallocate(Potential%r)
    if(allocated(Potential%c))deallocate(Potential%c)
    allocate(Potential%a(Potential%n))
    allocate(Potential%r(Potential%n))
    allocate(Potential%c(Potential%n))
    !
    !
    !.. Read the parameters
    !..
    rewind(uid)
    Potential%n=0
    do
       !
       read(uid,"(a)",iostat=iostat)line
       !
       if(iostat/=0)exit
       i=index(line,"#")
       if(i>0)line(i:)=" "
       line=adjustl(line)
       if(len_trim(line)==0)cycle
       !
       Potential%n=Potential%n+1
       !
       read(line,*,iostat=iostat) Alpha, R0, ReC, ImC, NumIdenPot
       if(iostat/=0)then
          call ErrorMessage("Invalid Absorption Potential Configuration File")
          STOP
       endif
       !
       if ( NumIdenPot == 1 ) then
          !
          Potential%a(Potential%n) = Alpha
          Potential%r(Potential%n) = R0
          Potential%c(Potential%n) = Z1 * ReC + Zi * ImC
          !
       else
          !
          call EqualPotParse( Potential, Alpha, R0, ReC, ImC, NumIdenPot )
          !
       end if
       !
    enddo
    !
    close(uid)
    !
  end subroutine ParseConfigurationFile

  !> Initializes the representation classes vector.
  subroutine InitAbsorptionPotential( Potential, Basis )
    !
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(inout) :: Potential
    !> Class of radial basis.
    class(ClassBasis)              , intent(in)    :: Basis
    integer :: iPot
    !
    if(allocated(Potential%Rep)) deallocate(Potential%Rep)
    allocate(Potential%Rep(Potential%n))
    !
    if ( (Potential%n == Potential%NumEqualPot) .and. (Potential%NumEqualPot > 1) ) then
       !
       call Potential%Rep(1)%Init( Basis )
       !
       do iPot = 2, Potential%n
          Potential%Rep(iPot) = Potential%Rep(1)
       enddo
       !
    else
       !
       do iPot = 1, Potential%n
          call Potential%Rep(iPot)%Init( Basis )
       enddo
       !
    end if
    !
  end subroutine InitAbsorptionPotential


  !> Builds a name for the file in which the absorption potential matrix elements will be saved.
  function AbsorptionPotentialFileName( Potential, iPot ) result(FileName)
    !> Class of absorption potentials.
    Class(ClassAbsorptionPotential), intent(in) :: Potential
    !> Absorption potential identification number.
    integer                        , intent(in) :: iPot
    !
    character(len=:), allocatable :: FileName
    !
    if(iPot<1.or.iPot>Potential%NumberOfTerms())return
    allocate(FileName,source=PotentialFileName(iPot,Potential%a(iPot),Potential%r(iPot)))
    !
  end function AbsorptionPotentialFileName
  

  !> Builds the identification name for an absorption potential.
  function PotentialFileName(iPot,Alpha,R_0) result(FileName)
    !> Absorption potential identification number.
    integer,         intent(in) :: iPot
    !> Absorption potential relevant parameters to build the identification name.
    DoublePrecision, intent(in) :: Alpha, R_0
    !
    character(len=:), allocatable :: FileName
    character(len=*), parameter :: DoubleFormat="(f12.6)"
    character(len=20) :: nstrn_ipot, nstrn_a, nstrn_r
    character(len=1000) :: strn
    write(nstrn_ipot,*) iPot
    write(nstrn_a,DoubleFormat) Alpha
    write(nstrn_r,DoubleFormat) R_0
    strn="PotNum"//trim(adjustl(nstrn_ipot))//"absp_Exp_"//trim(adjustl(nstrn_a))//"_Ro_"//trim(adjustl(nstrn_r))
    allocate(FileName,source=trim(strn))
    !
  end function PotentialFileName


  !> Computes and saves the absorption potential matrix elements.
  subroutine ComputeAndSave( Potential, Dir )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(inout) :: Potential
    !> Directory in which the matrix elements will be saved.
    character(len=*)               , intent(in)    :: Dir
    !
    integer :: iPot
    character(len=1000) :: FileName
    DoublePrecision :: Alpha, R0
    procedure( D2DFun ), pointer :: funPtr
    !
    if ( (Potential%n == Potential%NumEqualPot) .and. (Potential%NumEqualPot > 1) ) then
       !
       FileName = trim(Dir)//Potential%FileName(1)
       !
       if( .not. Potential%Rep(1)%Saved( FileName ) )then
             !
             R0    = Potential%r( 1 )
             Alpha = Potential%a( 1 )
             funPtr => LocalPotential
             !
             call Potential%Rep(1)%Integral( funPtr, LowerBound = R0 )
             !
             call Potential%Rep(1)%Write( FileName )
             !
          endif
          !
       do iPot = 2, Potential%n
          !
          FileName = trim(Dir)//Potential%FileName(iPot)
          !
          if( .not. Potential%Rep(iPot)%Saved( FileName ) )then
             !
             Potential%Rep(iPot) = Potential%Rep(1)
             !
             call Potential%Rep(iPot)%Write( FileName )
             !
          endif
          !
       enddo
       !
    else
       !
       do iPot = 1, Potential%n
          !
          FileName = trim(Dir)//Potential%FileName(iPot)!PotentialFileName( Potential%a( iPot ), Potential%r( iPot ) )
          !
          if( .not. Potential%Rep(iPot)%Saved( FileName ) )then
             !
             R0    = Potential%r( iPot )
             Alpha = Potential%a( iPot )
             funPtr => LocalPotential
             !
             call Potential%Rep(iPot)%Integral( funPtr, LowerBound = R0 )
             !
             call Potential%Rep(iPot)%Write( FileName )
             !
          endif
          !
       enddo
       !
    end if
    !
  contains 
    !
    Pure DoublePrecision function LocalPotential( r )
      DoublePrecision, intent(in) :: r
      LocalPotential = 0.d0
      if( r <= R0 )return
      LocalPotential = exp( Alpha * log( r - R0 ) )!may be replaced by (r-R0)**Alpha
    end function LocalPotential
    !
  end subroutine ComputeAndSave


  !> Write on unit the whole ClassAbsorptionPotential.
  subroutine WriteAbsorptionPotential( self, uid )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: self
    !> Unit number.
    integer                        , intent(in) :: uid
    !
    integer :: iPot
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=16) :: Writable
    character(len=16) :: Form
    integer           :: iostat
    logical           :: Opened
    !
    INQUIRE(&
         UNIT  = uid     , &
         OPENED= Opened  , &
         WRITE = Writable, &
         FORM  = Form    , &
         IOSTAT= iostat  , &
         IOMSG = iomsg     )
    if(iostat/=0) call Assert(iomsg)
    if( .not. Opened            ) call Assert("Output Unit is closed")
    if( trim(Writable) /= "YES" ) call Assert("Output Unit can't be written")

    select case( Form )
    case( "FORMATTED" )

       write(uid,"(a)")    " Absorption Potential Parameters"
       write(uid,"(a,i4)") " - Number     : ", self%n
       do iPot=1,self%n
          write(uid,"(a4)",advance="no") "  N "
          write(uid,"(x,a"//DBLE_STRN_LENGTH//")",advance="no") "Exponent  "
          write(uid,"(x,a"//DBLE_STRN_LENGTH//")",advance="no") "Radius    "
          write(uid,"(x,a"//DBLE_STRN_LENGTH//")",advance="no") "Re C      "
          write(uid,"(x,a"//DBLE_STRN_LENGTH//")")              "Im C      "
          write(uid,"(i4,*(x,"//DBLE_STRN_FORMAT//"))") &
               iPot, self%a(iPot), self%r(iPot), dble(self%c(iPot)), aimag(self%c(iPot))
          call self%Rep(iPot)%Write(uid)
       enddo
       !
       !
    case( "UNFORMATTED" )
       !
       write(uid) self%n
       do iPot=1,self%n
          write(uid) self%a(iPot), self%r(iPot), dble(self%c(iPot)), aimag(self%c(iPot))
          call self%Rep(iPot)%Write(uid)
       enddo
       !
    case DEFAULT
       call Assert("Invalid unit format")
    end select
    !
  end subroutine WriteAbsorptionPotential

  
  !> Reads the previously saved absorption potential matrix elements.
  subroutine ReadAbsorptionPotentialMatrixElem( Potential, Dir )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential),      intent(inout) :: Potential
    !> Directory in which the file containing the absorption potentials matrix elements is stored.
    character(len=*)               ,      intent(in)    :: Dir
    !
    integer :: iPot, uid, iostat
    character(len=1000) :: FileName
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=*), parameter :: Form = "UNFORMATTED"
    !
    do iPot = 1, Potential%n
       !
       FileName = trim(Dir)//Potential%FileName(iPot)!PotentialFileName( Potential%a( iPot ), Potential%r( iPot ) )
       !
       open(Newunit =  uid     , &
            File    =  FileName, &
            Status  = "old"    , &
            Action  = "read"   , &
            Form    =  Form    , &
            iostat  =  iostat  , &
            iomsg   =  iomsg   )
       !
       if(iostat/=0)call Assert(iomsg)
       call Potential%Rep(iPot)%Read( uid )
       close(uid)
    end do
    !
  end subroutine ReadAbsorptionPotentialMatrixElem


  !> Retrieves the number of terms in the absorption potential summation.
  integer function NumberOfTerms( AbsPot )
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: AbsPot
    NumberOfTerms = AbsPot%n
  end function NumberOfTerms

  
  !> Retrieves the requested complex coefficient from the absorption potential. 
  Complex(kind(1d0)) function ClassAbsorptionPotentialCoeff( AbsPot, iPot ) result(Coeff)
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(in) :: AbsPot
    !> Absorption potential identification number.
    integer                        , intent(in) :: iPot
    !
    Coeff=(0.d0,0.d0)
    if(iPot<0.or.iPot>AbsPot%n)return
    Coeff=AbsPot%c(iPot)
    !
  end function ClassAbsorptionPotentialCoeff


  !> Adequate the ClassAbsorptionPotential to the case in which the same potential is "NumIdenPot" times repeated.
  subroutine EqualPotParse( self, Alpha, R0, ReC, ImC, NumIdenPot )
    !
    !> Class of absorption potentials.
    class(ClassAbsorptionPotential), intent(inout) :: self
    !> Number coresponding to \f$\alpha_i\f$ in the absorption potential formula.
    real(kind(1d0)),                 intent(in)    :: Alpha
    !> Number coresponding to \f$R_i\f$ in the absorption potential formula.
    real(kind(1d0)),                 intent(in)    :: R0
    !> Number coresponding to the real part of \f$c_i\f$ in the absorption potential formula.
    real(kind(1d0)),                 intent(in)    :: ReC
    !> Number coresponding to the imaginary part of \f$c_i\f$ in the absorption potential formula.
    real(kind(1d0)),                 intent(in)    :: ImC
    !> Number of times that the same potential will be repeated.
    integer,                         intent(in)    :: NumIdenPot
    !
    complex(kind(1d0)), parameter :: Z1 = (1.d0,0.d0)
    complex(kind(1d0)), parameter :: Zi = (0.d0,1.d0)
    !
    self%n = NumIdenPot
    self%NumEqualPot = NumIdenPot
    !
    if (allocated( self%a )) deallocate(self%a)
    if (allocated( self%r )) deallocate(self%r)
    if (allocated( self%c )) deallocate(self%c)
    !
    allocate( self%a(self%n), self%r(self%n), self%c(self%n) )
    !
    self%a(:) = Alpha
    self%r(:) = R0
    self%c(:) = Z1 * ReC + Zi * ImC
    !
  end subroutine EqualPotParse


  !> Retrieves the number of times the same absorption potential is repeated.
  integer function NumberEqualPot( self )
    !
    class(ClassAbsorptionPotential), intent(in) :: self
    !
    NumberEqualPot = self%NumEqualPot
    !
  end function NumberEqualPot




end module ModuleAbsorptionPotential
