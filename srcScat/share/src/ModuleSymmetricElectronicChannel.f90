!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module ModuleSymmetricElectronicChannel

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleString
  use ModuleGroups
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleParentIons
  use ModuleShortRangeOrbitals
  use ModuleShortRangeChannel
  use ModulePartialWaveChannel
  use ModuleMatrix
  use ModuleConstants


  implicit none


  private



  !> Set of states corresponding to a given parent ion associated
  !! to either a short-range orbital or to a B-spline, to give a 
  !! defined total symmetry (and, optionally, multiplicity).
  type, public :: ClassSymmetricElectronicChannel
     ! {{{ private attributes

     private
     !> Maximum angular momentum the space can represent
     integer                                    :: Lmax

     !> Total Spin multiplicity (=0 if spin is not well-defined)
     integer                                    :: TotalMultiplicity

     !> Total spatial symmetry
     type(ClassIrrep), pointer, public          :: TotalIrrep

     !> The charge of the parent ion
     integer                                    :: ParentIonCharge
     !> Parent ion
     type(ClassParentIon), public               :: ParentIon
     
     integer                                    :: Nelectrons

     integer                                    :: NPWCs

     type(ClassPartialWaveChannel), allocatable :: PWCv(:)

     type(ClassShortRangeChannel)               :: SRC

     !> Directory in which the results will be stored
     character(len=:)             , allocatable :: StorageDir

     ! }}}

   contains
     generic, public :: ParseConfLine              => ClassSymmetricElectronicChannelParseConfLine
     generic, public :: show                       => ClassSymmetricElectronicChannelShow
     generic, public :: free                       => ClassSymmetricElectronicChannelFree
     generic, public :: SetStorageDir              =>  ClassSymmetricElectronicChannelSetStorageDir
     generic, public :: GetStorageDir              =>  ClassSymmetricElectronicChannelGetStorageDir
     generic, public :: GetPILabel                 => ClassSymmetricElectronicChannelGetPILabel
     generic, public :: GetPILabelFun              => ClassSymmetricElectronicChannelGetPILabelFun
     generic, public :: GetNumQCOrbitals           => ClassSymmetricElectronicChannelGetNumQCOrbitals
     generic, public :: GetNumPWC                  => ClassSymmetricElectronicChannelGetNumPWC
     generic, public :: GetEnergyPI                => ClassSymmetricElectronicChannelGetEnergyPI
     !> Gets the maximum L defined in the Close-Coupling anzat
     !! for the current ClassSymmetricElectronicChannel.
     generic, public :: GetMaxLinCC                => ClassSymmetricElectronicChannelGetMaxLinCC
     generic, public :: GetL                       => ClassSymmetricElectronicChannelGetL
     generic, public :: GetM                       => ClassSymmetricElectronicChannelGetM
     generic, public :: GetSRC                     => ClassSymmetricElectronicChannelGetSRC
     generic, public :: GetPWC                     => ClassSymmetricElectronicChannelGetPWC
     generic, public :: ComputeTransfMat           => ClassSymmetricElectronicChannelComputeTransfMat
     generic, public :: GetCartesianSet            => ClassSymmetricElectronicChannelGetCartesianSet
     procedure, public :: SetLmax 
     procedure, public :: SetNelectrons 
     procedure, public :: SetTotalMultiplicity 
     procedure, public :: SetTotalIrrep 
     procedure, public :: SetParentIonCharge 
     procedure, public :: GetParentIonCharge 
     ! {{{ private procedures

     procedure, private :: ClassSymmetricElectronicChannelParseConfLine
     procedure, private :: ClassSymmetricElectronicChannelShow
     procedure, private :: ClassSymmetricElectronicChannelFree
     procedure, private :: ClassSymmetricElectronicChannelSetStorageDir
     procedure, private :: ClassSymmetricElectronicChannelGetStorageDir
     procedure, private :: ClassSymmetricElectronicChannelGetPILabel
     procedure, private :: ClassSymmetricElectronicChannelGetPILabelFun
     procedure, private :: ClassSymmetricElectronicChannelGetNumQCOrbitals
     procedure, private :: ClassSymmetricElectronicChannelGetNumPWC
     procedure, private :: ClassSymmetricElectronicChannelGetEnergyPI
     procedure, private :: ClassSymmetricElectronicChannelGetMaxLinCC
     procedure, private :: ClassSymmetricElectronicChannelGetL
     procedure, private :: ClassSymmetricElectronicChannelGetM
     procedure, private :: ClassSymmetricElectronicChannelGetSRC
     procedure, private :: ClassSymmetricElectronicChannelGetPWC
     procedure, private :: ClassSymmetricElectronicChannelComputeTransfMat
     procedure, private :: ClassSymmetricElectronicChannelGetCartesianSet

     ! }}}
  end type ClassSymmetricElectronicChannel





  !> Defines a collection of strongly interacting symmetric channels 
  !! whose parent ions are degenerate or almost degenerate. This 
  !! circumstance typically arises as a result of the symmetry group
  !! used being a proper subgroup of the real symmetry group of the
  !! system. As a result, parent ions which nominally belong to different
  !! irreducible representations are really the interconvertible elements
  !! of a basis for a multi-dimensional irreducible representation of the
  !! real global group (stated otherwise, they are the same parent ion,
  !! with "different orientations"). E.g., the Pi states of a diatomic
  !! molecule, or the P states in an atom. If these states are not  
  !! considered all at the same time, their bound and continuum states
  !! will be unphysical geometric mixtures of states with different symmetry
  !! (e.g., in the atomic case, a mixture of S and D states). At the same
  !! time, the spectral resolution of individual channels leaves the 
  !! channels strongly coupled to each other.
!!$  type, public :: ClassSymmetricElectronicSuperChannel
!!$     ! {{{ private attributes
!!$     private
!!$     integer :: NChannels
!!$     type(ClassSymmetricElectronicChannels), pointer, dimension(:) :: Chanv
!!$     ! }}}
!!$   contains
!!$     generic, public :: init => ClassSymmetricElectronicSuperChannelInit
!!$     generic, public :: show => ClassSymmetricElectronicSuperChannelShow
!!$     generic, public :: free => ClassSymmetricElectronicSuperChannelFree
!!$     ! {{{ private procedures
!!$     procedure, private :: ClassSymmetricElectronicSuperChannelInit
!!$     procedure, private :: ClassSymmetricElectronicSuperChannelShow
!!$     procedure, private :: ClassSymmetricElectronicSuperChannelFree
!!$     ! }}}
!!$  end type ClassSymmetricElectronicSuperChannel



contains

  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !      ClassSymmetricElectronicChannel
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=



  subroutine ClassSymmetricElectronicChannelParseConfLine( Channel, Strn, OnlyPrepareData )
    !
    use moduleParameterList
    use moduleString
    class(ClassSymmetricElectronicChannel), intent(inout) :: Channel

    !> String containing the specification of the parent ion and 
    !! of the Xlm waves that must be associated with it
    !!  example 1 :   1Ag.1 (0 0, 2 0, 2 2)
    !!  example 2 :   3B2u.1(1 -1, 3 -1, 3 -3)
    character(len=*)                    , intent(in)    :: Strn
    logical, optional                   , intent(in)    :: OnlyPrepareData
    !
    integer :: ichar, dichar, ichar1, ichar2, iXlm
    character(len=:), allocatable :: PIonLabel, LMvecLabel
    type(ClassGroup), pointer :: Group
    integer :: NumLM
    integer, allocatable :: LMvector(:,:)
    type(ClassXlm) :: Xlm
    character(len=8)  :: PionIrrep
    character(len=32)  :: lString, mString, LmaxString
    integer :: PionMult, PionN
    type(ClassIrrep), pointer :: OrbitalIrrep
    type(ClassXlmSymmetricSet) :: XlmSymSet
    logical :: UseAllXlm = .false.


    !.. 1. Extract parent-ion parameters
    !..
    ichar1 = index(Strn,'(')
    if ( ichar1 == 0 ) call Assert( "Invalid channel string." )
    allocate( PionLabel, source = trim(adjustl(Strn(:ichar1-1))) )
    !
    !.. 1.a Check if the spin of the parent ion is compatible with the
    !       total spin of the space
    !..
    call ParsePionLabel( PionLabel, PionMult, PionIrrep, PionN )
    call CheckSpin( Channel%TotalMultiplicity, PionMult )


    !.. 2. Initialize the parent ion
    !..
    Group => Channel%TotalIrrep%GetGroup()
    call Channel%ParentIon%Init( Group, PionLabel, Channel%ParentIonCharge )
    if ( .not.present(OnlyPrepareData) .or. .not.OnlyPrepareData ) then
       call Channel%ParentIon%SetEnergy( Channel%ParentIon%ReadEnergy( Channel%StorageDir ) )
    end if
    call Channel%ParentIon%SetNelect( Channel%Nelectrons - 1 )

    OrbitalIrrep => Channel%TotalIrrep * Channel%ParentIon%GetIrrep()
    call XlmSymSet%Init( Channel%Lmax, OrbitalIrrep )


    !.. 3. Extract from the Xlm vector of (l,m)
    !..
    ichar2 = index(Strn,')')
    if ( ichar2 == 0 ) call Assert( "Invalind channel string." )
    allocate( LMvecLabel, source = trim(adjustl(Strn(ichar1+1:ichar2-1))) )

    UseAllXlm = ( len_trim(LMvecLabel) == 0 )
    
    !.. Check if the specified Xlm are compatible with the current symmetry
    if ( .not. UseAllXlm ) then

       !.. Count number of specified Xlm channels 
       NumLM = 1
       ichar = 0
       XlmCycle : do

          dichar = index(LMvecLabel(ichar+1:),',')
          if(dichar<1)exit XlmCycle
          ichar = ichar + dichar
          LMvecLabel(ichar:ichar) = " "
          NumLM = NumLM + 1

       end do XlmCycle
       
       !.. Parse list of specified Xlm channels
       allocate( LMVector( 2, NumLM ) )
       read(LMvecLabel,*) (LMVector(1, iXlm), LMVector(2, iXlm),iXlm=1,NumLM)
       
       allocate( Channel%PWCv(NumLM) )

       Channel%NPWCs = NumLM

       !.. Check list of specified Xlm channels
       !..
       do iXlm = 1, NumLM
          call Xlm%Init( LMvector(1,iXlm), LMvector(2,iXlm) )
          if ( .not. XlmSymSet%ValidXlm( Xlm ) ) then
             write(lString,*) Xlm%GetL()
             write(mString,*) Xlm%GetM()
             write(LmaxString,*) Channel%Lmax
             call Assert( "The symmetry adapted spherical harmonic with l: "//trim(adjustl(lString))//" and m: "//&
               trim(adjustl(mString))//" do not belong to the irreducible representation: "//OrbitalIrrep%GetName()//&
               " with maximum angular momentum: "//trim(adjustl(LmaxString)) )
          end if
          call Channel%PWCv(iXlm)%Init(   &
               Channel%ParentIon        , &
               Channel%Lmax             , &
               Channel%TotalMultiplicity, &
               Channel%TotalIrrep       , &
               Xlm                      , &
               Channel%StorageDir     )
       enddo
       
    else
       
       NumLM = XlmSymSet%GetNXlm()
       allocate( Channel%PWCv(NumLM) )
       Channel%NPWCs = NumLM
       
       do iXlm = 1, NumLM
          Xlm = XlmSymSet%GetXlm(iXlm)
          call Channel%PWCv(iXlm)%Init(   &
               Channel%ParentIon        , &
               Channel%Lmax             , &
               Channel%TotalMultiplicity, &
               Channel%TotalIrrep       , &
               Xlm                      , &
               Channel%StorageDir     )
       enddo
       
    end if

    if ( present(OnlyPrepareData) .and. OnlyPrepareData ) return

    call Channel%SRC%init(          &
         Channel%ParentIon        , &
         Channel%Lmax             , &
         Channel%TotalMultiplicity, &
         Channel%TotalIrrep        , &
         Channel%StorageDir  )
    
  contains
    !
    subroutine CheckSpin( TotalMult, PionMult )
      !
      integer, intent(in) :: TotalMult, PionMult
      !
      if ( abs( TotalMult - PionMult ) /= 1 ) call Assert("Total spin and parent-ion spin are incompatible")
      !
    end subroutine CheckSpin
    !
  end subroutine ClassSymmetricElectronicChannelParseConfLine




  subroutine ClassSymmetricElectronicChannelShow( Channel, unit )
    class(ClassSymmetricElectronicChannel), intent(in) :: Channel
    integer, optional             ,         intent(in) :: unit
    !
    integer :: outunit, iChan
    !
    outunit = OUTPUT_UNIT
    !
    if(present(unit)) outunit = unit
    !  
    write(outunit,"(a)") "Symmetric Electronic Channels Info : "
    !
    write(outunit,"(a,i4)"          ) "  Maximum Angular Momentum :",Channel%Lmax
    write(outunit,"(a,i4)"          ) "  Total Multiplicity :",Channel%TotalMultiplicity
    !
    write(outunit,"(a)",advance="no") "  Total Symmetry     :"
    !
    call Channel%TotalIrrep%show( outunit )
    !
    if( .true. ) then !*** It's necessary to avoid catastrophical error at compilation time.
       call Channel%ParentIon%Show()
    end if
    !
    do iChan = 1, Channel%NPWCs
       call Channel%PWCv(iChan)%Show(unit)
    enddo
    call Channel%SRC%Show(unit)
  end subroutine ClassSymmetricElectronicChannelShow



  subroutine ClassSymmetricElectronicChannelFree( Channel )
    class(ClassSymmetricElectronicChannel), intent(inout) :: Channel
    Channel%Lmax = -1
    Channel%TotalMultiplicity = 0
    Channel%ParentIonCharge = 0
    call Channel%ParentIon%Free()
    Channel%TotalIrrep => NULL()
    call Channel%SRC%free()
    if ( allocated(Channel%PWCv) ) deallocate( Channel%PWCv )
    Channel%NPWCs = 0
    if ( allocated(Channel%StorageDir) ) deallocate( Channel%StorageDir )
  end subroutine ClassSymmetricElectronicChannelFree



  subroutine ClassSymmetricElectronicChannelFinal( Channel )
    type(ClassSymmetricElectronicChannel) :: Channel
    call Channel%free()
  end subroutine ClassSymmetricElectronicChannelFinal



  subroutine SetTotalMultiplicity( SymElectChan, TotMultiplicity )
    class(ClassSymmetricElectronicChannel), intent(inout) :: SymElectChan
    integer,                                intent(in)    :: TotMultiplicity
    SymElectChan%TotalMultiplicity = TotMultiplicity
  end subroutine SetTotalMultiplicity




  subroutine SetLmax( SymElectChan, Lmax )
    class(ClassSymmetricElectronicChannel), intent(inout) :: SymElectChan
    integer,                                intent(in)    :: Lmax
    SymElectChan%Lmax = Lmax
  end subroutine SetLmax



  subroutine SetNelectrons( SymElectChan, Ne )
    class(ClassSymmetricElectronicChannel), intent(inout) :: SymElectChan
    integer,                                intent(in)    :: Ne
    SymElectChan%Nelectrons = Ne
  end subroutine SetNelectrons



  subroutine SetTotalIrrep( SymElectChan, TotIrrep )
    class(ClassSymmetricElectronicChannel), intent(inout) :: SymElectChan
    class(ClassIrrep), target,              intent(in)    :: TotIrrep
    SymElectChan%TotalIrrep => TotIrrep
  end subroutine SetTotalIrrep



  subroutine SetParentIonCharge( SymElectChan, Charge )
    class(ClassSymmetricElectronicChannel), intent(inout) :: SymElectChan
    integer,                                intent(in)    :: Charge
    SymElectChan%ParentIonCharge = Charge
  end subroutine SetParentIonCharge



  real(kind(1d0)) function GetParentIonCharge( SymElectChan ) result(Charge)
    class(ClassSymmetricElectronicChannel), intent(in) :: SymElectChan
    Charge = SymElectChan%ParentIonCharge
  end function GetParentIonCharge



  subroutine ClassSymmetricElectronicChannelSetStorageDir( Channel, StorageDir )
    class(ClassSymmetricElectronicChannel), intent(inout) :: Channel
    character(len=*),                       intent(in)    :: StorageDir
    if ( allocated(Channel%StorageDir) ) deallocate(Channel%StorageDir)
    allocate( Channel%StorageDir, source = StorageDir )
  end subroutine ClassSymmetricElectronicChannelSetStorageDir



  function ClassSymmetricElectronicChannelGetStorageDir( Self ) result(Dir)
    class(ClassSymmetricElectronicChannel), intent(in) :: Self
    character(len=:), allocatable :: Dir
    if( .not.allocated(Self%StorageDir) ) call Assert( &
         'The storage dir is not allocated in the channel, impossible to fetch.' )
    allocate( Dir, source = Self%StorageDir )
  end function ClassSymmetricElectronicChannelGetStorageDir




  subroutine ClassSymmetricElectronicChannelGetPILabel( Channel, Label )
    class(ClassSymmetricElectronicChannel), intent(in)  :: Channel
    character(len=:), allocatable,          intent(out) :: Label
    allocate( Label, source = Channel%ParentIon%GetLabel() )
  end subroutine ClassSymmetricElectronicChannelGetPILabel



  character(len=128) function ClassSymmetricElectronicChannelGetPILabelFun( Channel ) result( Label )
    class(ClassSymmetricElectronicChannel), intent(in)  :: Channel
    Label = Channel%ParentIon%GetLabel() 
  end function ClassSymmetricElectronicChannelGetPILabelFun



  integer function ClassSymmetricElectronicChannelGetNumQCOrbitals( Channel ) result(NQCO)
    class(ClassSymmetricElectronicChannel), intent(in)  :: Channel
    NQCO = Channel%SRC%GetTotNumOrbitals()
  end function ClassSymmetricElectronicChannelGetNumQCOrbitals



  !> Gets the maximum L defined in the Close-Coupling anzat
  !! for the current ClassSymmetricElectronicChannel.
  integer function ClassSymmetricElectronicChannelGetMaxLinCC( Channel ) result(L)
    class(ClassSymmetricElectronicChannel), intent(in) :: Channel
    integer :: i
    !
    if ( .not.allocated(Channel%PWCv) ) call Assert( &
         'ClassSymmetricElectronicChannel has not been '//&
         'initialized, impossible to get the maximum L in '//&
         'the Close-Coupling expansion, aborting ...' )
    L = 0
    do i = 1, Channel%NPWCs
       L = max(L,Channel%PWCv(i)%GetL())
    end do
  end function ClassSymmetricElectronicChannelGetMaxLinCC


  integer function ClassSymmetricElectronicChannelGetL( Channel, PWC ) result(L)
    class(ClassSymmetricElectronicChannel), intent(in) :: Channel
    integer,                                intent(in) :: PWC
    L = Channel%PWCv(PWC)%GetL()
  end function ClassSymmetricElectronicChannelGetL



  integer function ClassSymmetricElectronicChannelGetM( Channel, PWC ) result(M)
    class(ClassSymmetricElectronicChannel), intent(in) :: Channel
    integer,                                intent(in) :: PWC
    integer :: i
    do i = 1,1
       M = Channel%PWCv(PWC)%GetM()
    end do
  end function ClassSymmetricElectronicChannelGetM



  integer function ClassSymmetricElectronicChannelGetNumPWC( Channel ) result(NPWC)
    class(ClassSymmetricElectronicChannel), intent(in)  :: Channel
    NPWC = Channel%NPWCs
  end function ClassSymmetricElectronicChannelGetNumPWC



  function ClassSymmetricElectronicChannelGetPWC( Self, iPWC ) result(PWC)
    class(ClassSymmetricElectronicChannel), target, intent(in) :: Self
    integer,                                        intent(in)    :: iPWC
    class(ClassPartialWaveChannel), pointer :: PWC
    if ( .not.allocated(Self%PWCv) ) call Assert( &
         'PWC channels are not allocated, impossible to fetch.' )
    PWC => Self%PWCv(iPWC)
  end function ClassSymmetricElectronicChannelGetPWC


  function ClassSymmetricElectronicChannelGetSRC( Self ) result(SRC)
    class(ClassSymmetricElectronicChannel), target, intent(in) :: Self
    class(ClassShortRangeChannel), pointer :: SRC
    SRC => Self%SRC
  end function ClassSymmetricElectronicChannelGetSRC




  real(kind(1d0)) function ClassSymmetricElectronicChannelGetEnergyPI( Channel ) result(Energy)
    class(ClassSymmetricElectronicChannel), intent(in)  :: Channel
    Energy = Channel%ParentIon%GetEnergy()
  end function ClassSymmetricElectronicChannelGetEnergyPI



  subroutine ClassSymmetricElectronicChannelComputeTransfMat( Channel, Identifier, Array )
    class(ClassSymmetricElectronicChannel), intent(inout) :: Channel
    character(len=*),                       intent(in)    :: Identifier
    complex(kind(1d0)), allocatable,        intent(out)   :: Array(:,:)
    call Channel%SRC%ComputeTransfMat()
    call Channel%SRC%GetTransfMat( Identifier, Array )
  end subroutine ClassSymmetricElectronicChannelComputeTransfMat




  function ClassSymmetricElectronicChannelGetCartesianSet( Channel ) result( CartSet )
    class(ClassSymmetricElectronicChannel), intent(inout) :: Channel
    type(ClassCartesianSymmetricSet), pointer :: CartSet
    CartSet => Channel%SRC%GetCartesianSet()
  end function ClassSymmetricElectronicChannelGetCartesianSet





end module ModuleSymmetricElectronicChannel
