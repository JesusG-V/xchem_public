!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines the variables and methods for the Gaussian/B-splines mixed radial basis.


module ModuleGaussianBSpline

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleParameterList
  use ModuleString
  use ModuleBSpline
  use ModuleGaussian
  use ModuleSimpleLapack

  implicit none

  private

  !> Contains the information to precondition the BSpline
  !! subset of the Mixed basis that overlaps with the 
  !! Gaussian basis, so that the overall basis is linearly 
  !! independent. 
  type, private :: ClassGaussianBSplinePreconditioner

     !> If true, the Preconditioner is available
     logical :: Initialized

     !> Number of regular continuous BSplines overlapping with 
     !! the preconditioned gaussian basis set
     integer :: NOverBSpline

     !> Number of splines which are overlapping with the 
     !! preconditioned Gaussian basis subset, yet are reasonably 
     !! linearly independent from it.
     integer :: NPreconBSpline

     !> Number of splines which are non overlapping with the 
     !! The following relation holds
     !! NNonOverlappingBSpline + NOverBSpline + NBSplineSkip = 
     !! = Total number of BSplines ( first and last included )
     integer :: NNonOverlappingBSpline

     !> Eigenvalues of the projector on the preconditioned Gaussian
     !! basis, evaluated on the basis of the overlapping regular
     !! continuous BSPlines.
     DoublePrecision, allocatable :: E(:)

     !> Transformation matrix for the BSpline set from the regular 
     !! continuous sub set to the preconditioned subset 
     DoublePrecision, allocatable :: A(:,:)

   contains

     procedure :: write => WriteBSplinePreconditionerOnUnit
     procedure :: read  => ReadBSplinePreconditionerOnUnit
     procedure :: free  => FreeGaussianBSplinePreconditioner 
     generic   :: assignment(=) => CopyGaussianBSplinePreconditioner
     procedure, private :: CopyGaussianBSplinePreconditioner
     final     :: FinalizeGaussianBSplinePreconditioner

  end type ClassGaussianBSplinePreconditioner

  character(len=*), parameter :: BSPLINE_PRECON_ROOT_NAME = "BSplinePreconditioner"

  !> Specify the smallest tolerated value for an eigenvalue of the
  !! projector on the preconditioned Gaussian basis in the Overlapping
  !! regular BSpline basis.
  real(kind(1d0)), parameter :: LOWER_PROJECTOR_TOLERANCE = -1.d-10

  !> Specify how much an eigenvalue of the projector on the preconditioned 
  !! Gaussian basis in the Overlapping regular BSpline basis can trenspass
  !! the maximum theoretical value of 1.
  real(kind(1d0)), parameter :: UPPER_PROJECTOR_TOLERANCE =  1.d-10


  !> Description:
  !! Contains the variables and procedures to manage the Gaussian/B-splines mixed basis. 
  type, public :: ClassGaussianBSpline
     private
     !> Indicates whether ClassGaussinaBSpline has been initialized or not.
     logical :: Initialized = .FALSE.
     !> Maximum angular momentum in the Gaussian expansion.
     integer             :: Lmax
     !> Class of B-splines.
     type(ClassBSpline)  :: BSpline
     !> Class of Gaussian functions.
     type(ClassGaussian) :: Gaussian

     !> Number of initial BSpline to eliminate from the
     !! total basis to get the regular basis
     integer         :: NBSplineSkip

     !> Minimum overlap between Preconditioned Gaussian
     !! functions and BSplines
     real(kind(1d0)) :: MatrixElementThreshold

     !> Minimum overlap between Preconditioned Gaussian
     !! functions and BSplines
     real(kind(1d0)) :: GaussianBSplineOverlapThreshold

     !> Minimum deviation from 1 of the eigenvalues of 
     !! the projector on the preconditioned gaussian 
     !! basis expressed in the BSpline basis required
     !! to consider the linear combination of BSpline
     !! independent from the preconditioned Gaussian basis
     real(kind(1d0)) :: GaussianProjectorThreshold

     !> Vector whose components are preconditioner classes.
     type(ClassGaussianBSplinePreconditioner), allocatable :: Preconditioner(:)

   contains

     !> Initialize the set of Gaussian BSplines after reading the parameteres from a file.
     procedure :: Init
     !> Allocates the attributes of the Gaussian part in the mixed Gaussian/B-splines basis.
     procedure :: Allocate => GaussianPartAllocate !check if it is possible to set this to private
     !> Frees the GaussianBSpline basis class.
     procedure :: Free

     !.. Accessors
     !> Retrieves the total number of GaussianBSpline basis functions. 
     procedure :: GetNFun
     !> Retrieves the maximum angular momentum considered by the GaussianBSpline basis.
     procedure :: GetLMax
     !> Retrieves the number of exponents of the Gaussian subset in the GaussianBSpline basis.
     procedure :: GetNumExponents
     !> Retrieves the number of initial BSpline functions to skip.
     procedure :: GetNBSplineSkip
     !> Retrieves the normalization factor corresponding to a basis function.
     procedure :: GetNormFactor
     !> Gets a vector with all the monomials exponents belonging to the Gaussian functions.
     procedure, public :: GetAllMonomials
     !> Gets a vector with all the exponents in the exponential belonging to the Gaussian functions.
     procedure, public :: GetAllExponents
     !> Gets the maximum distance at which the basis functions will be plotted.
     procedure :: GetMaxPlotRadius
     !> Gets a given node position for the B-splines
     procedure :: GetNodePosition
     !
     !
     !.. Accessors to parameters of contained private objects
     !> Gets the number of Gaussian functions in the mixed basis.
     procedure :: GetGaussianNFun
     !> Gets the number of used B-splines functions in the mixed basis.
     procedure :: GetBsplinesNFun
     !> Gets the minimum Gaussian functions index among the regular subset.
     procedure :: GetMinimumGaussianRegularIndex
     !> Gets the maximum Gaussian functions index among the regular subset.
     procedure :: GetMaximumGaussianRegularIndex
     !> Gets the mixed basis regular functions indexes.
     procedure :: GetRegularIndexes
     !> Gets the information of the linear independent gaussian function (preconditioned): monomial exponent, gaussian exponent and normalization factor.
     procedure :: GetLinIndGaussian
     !> Evaluates either a selected normalized radial function belonging to the mixed basis or a linear combination of the radial basis functions:
     !! \f[
     !!    u(r)=\sum_{i=1}^{NumGaussian}c_{i}g_{i}(r)+\sum_{j=1}^{NumBsplines}d_{j}b_{j}(r) 
     !! \f]
     generic   :: Eval      =>  GaussianBSplineEval, GaussianBSplineFunctionEval
     !> Tabulates in a specified domain either a selected normalized radial function belonging to the mixed basis or a linear combination of the radial basis functions.
     generic   :: Tabulate  =>  GaussianBSplineTabulate, GaussianBSplineFunctionTabulate
     !
     !> Computes 1D integrals of arbitrary functions with arbitrary derivatives of elements of the mixed radial basis.
     generic   :: Integral       =>  GaussianBSplineGeneralIntegral
     !> Computes the monoelectronic multipole
     !! \f[
     !!    \int_0^\infty dr f_1(R)\frac{r_<^\ell}{r_>^{\ell+1}}f_2(r)
     !! \f]
     !! where \f$r_<=\min(r,R)\f$ and \f$r_>=\max(r,R)\f$. 
     generic   :: Multipole      =>  MonoelectronicMultipole
     !
     !> Logical function that retrieves whether a previous stored fingerprint matches with the current one or not.
     procedure :: MatchFingerPrint       =>  GaussianBSplineMatchFingerPrint
     !> Logical function that retrieves whether a previous stored fingerprint matches with the current one or not; accesssing the storing file through its unit number.
     procedure :: MatchFingerPrintOnUnit =>  GaussianBSplineMatchFingerPrintOnUnit
     !> Logical function that retrieves whether a previous stored fingerprint matches with the current one or not; accesssing the storing file through its name.
     procedure :: MatchFingerPrintOnFile =>  GaussianBSplineMatchFingerPrintOnFile
     !> Writes the mixed basis fingerprint on a files once provided its unit number.
     procedure :: WriteFingerPrint       =>  GaussianBSplineWriteFingerPrint

     generic, public :: ASSIGNMENT(=) => CopyGaussianBSplineSet

     !> Gets the number of regular functions in the mixed basis.
     procedure :: NRegular        => ClassGaussianBSplineNRegular
     !> Gets the number of preconditioned functions in the mixed basis.
     procedure :: NPreconditioned => ClassGaussianBSplineNPreconditioned
     !> Computes de preconditioner.
     procedure :: ComputePreconditioner
     !> Saves the preconditioner.
     procedure :: SavePreconditioner
     !> Logical function that retrieves whether the preconditioner is available or not.
     procedure :: PreconditionerIsAvailable
     !> Loads the preconditioner.
     procedure :: LoadPreconditioner
     !> Fetches the preconditioner.
     procedure :: FetchPreconditioner
     !
     !> Given, for some operator, the matrix element between 
     !! a pair of row and column indexes, tell whether the 
     !! matrix elements with higher column indexes are 
     !! likely to be smaller in magnitude than the threshold for 
     !! matrix elements specified in the configuration file.
     !! 
     !! This is likely to be the case if the row is 
     !! a gaussian and the column is a BSpline already 
     !! so far away that the Integral is negligibly small.
     !!
     !! Possible risks: 
     !!
     !!  1. non-smooth potentials
     !!  2. Integrals which are accidentally very close to zero.
     procedure :: RemainingKetsGiveZero
     !
     !> Given, for some operator, the matrix element between 
     !! a pair of row and column indexes, tell whether the 
     !! matrix elements with higher row indexes are 
     !! likely to be smaller in magnitude than the threshold for 
     !! matrix elements specified in the configuration file.
     !! 
     !! This is likely to be the case if the column is 
     !! a gaussian and the row is a BSpline already 
     !! so far away that the Integral is negligibly small.
     !!
     !! Possible risks: 
     !!
     !!  1. non-smooth potentials
     !!  2. Integrals which are accidentally very close to zero.
     procedure :: RemainingBrasGiveZero
     !> Creates in a directory all the configuration
     !!    files that define the current basis.
     procedure :: SaveConfig
     !
     !.. Private Interface
     procedure, private :: CheckInit
     procedure, private :: GaussianBSplineGeneralIntegral
     procedure, private :: GaussianBSplineEval
     procedure, private :: GaussianBSplineFunctionEval
     procedure, private :: GaussianBSplineTabulate
     procedure, private :: GaussianBSplineFunctionTabulate
     procedure, private :: MonoElectronicMultipole
     procedure, private :: CopyGaussianBSplineSet
     procedure, private :: IsGaussian
     procedure, private :: IsBSpline
     procedure, private :: TotalToBSplineIndex
     procedure, private :: TotalToGaussianIndex
     procedure, private :: BSplineToTotalIndex
     procedure, private :: GaussianToTotalIndex
  end type ClassGaussianBSpline

  !> Contains the variables and procedures to manage the mixed basis relevant information with the aim to distinguish between the used parameterization and others (fingerprint).
  type, public :: ClassGaussianBSplineFingerPrint
     private
     !
     !> Maximum angular momentum in the Gaussian expansion.
     integer         :: Lmax
     !> Number of initial BSpline to eliminate from the
     !! total basis to get the regular basis
     integer         :: NBSplineSkip
     !> Minimum overlap between Preconditioned Gaussian
     !! functions and BSplines
     real(kind(1d0)) :: MatrixElementThreshold
     !> Minimum overlap between Preconditioned Gaussian
     !! functions and BSplines
     real(kind(1d0)) :: GaussianBSplineOverlapThreshold
     !> Minimum deviation from 1 of the eigenvalues of 
     !! the projector on the preconditioned gaussian 
     !! basis expressed in the BSpline basis required
     !! to consider the linear combination of BSpline
     !! independent from the preconditioned Gaussian basis
     real(kind(1d0)) :: GaussianProjectorThreshold
     !> Class of B-splines' fingerprint.
     type(ClassBSplineFingerPrint)  :: BSplineFingerPrint
     !> Class of Gaussian's fingerprint.
     type(ClassGaussianFingerPrint) :: GaussianFingerPrint
     !
   contains
     !> Deallocates the atributes of the mixed basis fingerprint class.
     procedure :: Free   =>  FingerPrintFree
     !> Writes the fingerprint to a unit.
     procedure :: Write  =>  FingerPrintWrite
     !> Reads the fingerprint from a unit.
     procedure :: Read   =>  FingerPrintRead
     procedure, private :: Init   =>  FingerPrintInit
     !> Makes a copy of the fingerprint.
     generic   :: assignment(=) => ExtractFingerPrint
     procedure, private :: ExtractFingerprint
  end type ClassGaussianBSplineFingerPrint


  interface myexp
     module procedure myexp_DD, myexp_DI
  end interface myexp


contains

  !.. Define prototypes and auxiliary routines

  !> Initialize the set of Gaussian BSplines after reading the parameteres from a file.
  subroutine Init( self, ConfigurationFile, IOSTAT )
    !
    Class(ClassGaussianBSpline), intent(inout)         :: self
    character(len=*)           , intent(in)            :: ConfigurationFile
    integer                    , intent(out), optional :: IOSTAT
    !
    type(ClassParameterList) :: List
    character(len=512) :: BSplineBasisFile
    character(len=512) :: GaussianBasisFile
    integer            :: NBSplineSkip
    real(kind(1d0))    :: MatrixElementThreshold
    real(kind(1d0))    :: GaussianBSplineOverlapThreshold
    real(kind(1d0))    :: GaussianProjectorThreshold
    integer            :: BsplineStatus, GaussianStatus
    !
    !.. Define parameters and parse configuration file
    call List%Add( "BSplineBasisFile"  , "OuterBSplineBasis", "required" )
    call List%Add( "GaussianBasisFile" , "GaussianBasis", "required" )
    call List%Add( "NBSplineSkip", 3, "required" )
    call List%Add( "MatrixElementThreshold", 1.d-22, "optional" )
    call List%Add( "GaussianBSplineOverlapThreshold", 1.d-16, "optional" )
    call List%Add( "GaussianProjectorThreshold"     , 1.d-8 , "optional" )
    !
    call List%Parse( ConfigurationFile )
    !
    call List%Get( "BSplineBasisFile", BSplineBasisFile )
    call List%Get( "GaussianBasisFile", GaussianBasisFile  )
    call List%Get( "NBSplineSkip", NBSplineSkip  )
    call List%Get( "MatrixElementThreshold", MatrixElementThreshold )
    call List%Get( "GaussianBSplineOverlapThreshold", GaussianBSplineOverlapThreshold )
    call List%Get( "GaussianProjectorThreshold", GaussianProjectorThreshold  )
    !
    BSplineBasisFile  = trim(adjustl(BSplineBasisFile))
    GaussianBasisFile = trim(adjustl(GaussianBasisFile))
    !
    call self%BSpline%Init( BSplineBasisFile, BSplineStatus )
    call self%Gaussian%Init( GaussianBasisFile, GaussianStatus )

    if ( BSplineStatus/=0 .or. GaussianStatus/=0 ) then
       call Assert("GaussianBSpline basis initialization failed.")
    end if
    !
    if ( .not.self%Initialized ) then
       !
       ! *** 
       !    - Add parameter check
       !    - Create a separate function for the initialization 
       !      with a set of parameters
       ! ***
       self%Lmax = self%Gaussian%GetLMax()
       self%NBSplineSkip = NBSplineSkip
       self%MatrixElementThreshold = MatrixElementThreshold
       self%GaussianBSplineOverlapThreshold = GaussianBSplineOverlapThreshold
       self%GaussianProjectorThreshold = GaussianProjectorThreshold
       self%Initialized = .TRUE.
       IOSTAT = 0
       allocate(self%Preconditioner(0:self%LMax))

    end if
    !
  end subroutine Init

  !> Allocates the attributes of the Gaussian part in the mixed Gaussian/B-splines basis.
  subroutine GaussianPartAllocate( self )
    !
    Class(ClassGaussianBSpline), intent(inout) :: self
    !
    if ( .not.self%Initialized ) then
       call Assert("ClassGaussianBSpline::Allocate: GaussianBSpline set not initialized")
    else
       if ( self%Lmax == self%Gaussian%GetLmax() ) then
          call self%Gaussian%Allocate( self%Lmax, self%Gaussian%GetNAlpha() )
       else
          call Assert(" The Lmax of the GaussianBSpline basis differs from the Gaussian's basis Lmax." )
       end if
    end if
    !
  end subroutine GaussianPartAllocate


  !> Frees the GaussianBSpline basis class.
  subroutine Free( self )
    !
    Class(ClassGaussianBSpline), intent(inout) :: self
    !
    if ( self%Initialized ) then
       self%Lmax = 0
       self%NBSplineSkip = 0
       self%MatrixElementThreshold = 0.d0
       self%GaussianBSplineOverlapThreshold = 0.d0
       self%GaussianProjectorThreshold = 0.d0
       !
       call self%BSpline%Free()
       call self%Gaussian%Free()
       if(allocated(self%preconditioner)) deallocate(self%preconditioner)
       !
       self%Initialized = .FALSE.
    end if
    !
  end subroutine Free


  !> Retrieves the total number of GaussianBSpline basis functions. 
  integer function GetNFun( self ) result( NFun )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    !
    NFun = self%BSpline%GetNBSplines() + self%Gaussian%GetNFun()
    !
  end function GetNFun


  !> Retrieves the maximum angular momentum considered by the GaussianBSpline basis.
  integer function GetLMax( self ) result ( LMax )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    !
    if ( .not.self%Initialized ) then
       call Assert("ClassGaussianBSpline::GetLMax: GaussianBSpline set not initialized")
    else
       LMax = self%Lmax
    end if
    !
  end function GetLMax



  !> Retrieves the maximum angular momentum considered by the GaussianBSpline basis.
  integer function GetNumExponents( self ) result ( NumExp )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    !
    if ( .not.self%Initialized ) then
       call Assert("ClassGaussianBSpline::GetLMax: GaussianBSpline set not initialized")
    else
       NumExp = self%Gaussian%GetNAlpha()
    end if
    !
  end function GetNumExponents



  !> Retrieves the number of initial BSpline functions to skip.
  integer function GetNBSplineSkip( self ) result ( NSkip )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    !
    if ( .not.self%Initialized ) then
       call Assert("ClassGaussianBSpline::GetBSplineSkip : GaussianBSpline set not initialized")
    else
       NSkip = self%NBSplineSkip
    end if
    !
  end function GetNBSplineSkip


  subroutine CheckInit( self )
    Class(ClassGaussianBSpline), intent(in) :: self
    if ( .not. self%Initialized ) call Assert("GaussianBSpline set not initialized")
  end subroutine CheckInit


  !> Gets the number of Gaussian functions in the mixed basis.
  integer function GetGaussianNFun( self ) result ( res )
    Class(ClassGaussianBSpline), intent(in) :: self
    call self%CheckInit()
    res = self%Gaussian%GetNFun()
  end function GetGaussianNFun


  !> Gets the number of B-splines functions in the mixed basis (including the last, skipping the number specified in the mixed basis configuration file).
  integer function GetBsplinesNFun( self ) result ( res )
    Class(ClassGaussianBSpline), intent(in) :: self
    call self%CheckInit()
    res = self%BSpline%GetNBSplines() - self%GetNBSplineSkip()
  end function GetBsplinesNFun


  !> Gets the information of the linear independent gaussian function (preconditioned): monomial exponent, gaussian exponent and normalization factor.
  subroutine GetLinIndGaussian( self, MonExp, GaussExp, NormFactor )
    !> GABS radial basis class.
    Class(ClassGaussianBSpline), intent(in) :: self
    !> Monomial exponents.
    integer, allocatable, intent(out) :: MonExp(:)
    !> Gaussian exponents.
    real(kind(1d0)), allocatable, intent(out) :: GaussExp(:)
    !> Normalization factors.
    real(kind(1d0)), allocatable, intent(out) :: NormFactor(:)
    !
    call self%Gaussian%GetLIGaussian( MonExp, GaussExp, NormFactor )
  end subroutine GetLinIndGaussian


  !
  integer function GetMinimumGaussianRegularIndex( self, L ) result ( res )
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    call self%CheckInit()
    res = self%Gaussian%GetMinimumRegularIndex( L )
  end function GetMinimumGaussianRegularIndex
  !
  integer function GetMaximumGaussianRegularIndex( self, L ) result ( res )
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    call self%CheckInit()
    res = self%Gaussian%GetMaximumRegularIndex( L )
  end function GetMaximumGaussianRegularIndex


  !> Retrieves the normalization factor corresponding to a basis function.
  real(kind(1d0)) function GetNormFactor( self, iFun ) result( Norm )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iFun
    !
    Norm = 0.d0
    if ( iFun < 1 .or. iFun > self%GetNFun() ) return
    ! A function sorting has been assumed, starting with Gaussians and ending with BSplines.
    if ( iFun <= self%Gaussian%GetNFun() ) then
       Norm = self%Gaussian%GetNormFactor( iFun )
    else
       Norm = self%BSpline%GetNormFactor( self%TotalToBSplineIndex( iFun ) )
    end if
    !
  end function GetNormFactor


  !> Gets the maximum distance at which the basis functions will be plotted.
  real(kind(1d0)) function GetMaxPlotRadius( self ) result(MaxPlotR)
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    !
    MaxPlotR = self%BSpline%GetNodePosition( self%BSpline%GetNNodes() )
    !
  end function GetMaxPlotRadius



  !> Gets a given node position for the B-splines
  real(kind(1d0)) function GetNodePosition( self, Node ) result(R)
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    integer,                     intent(in) :: Node
    !
    R = self%BSpline%GetNodePosition( Node )
    !
  end function GetNodePosition



  integer function ClassGaussianBSplineNRegular( self, L ) result( NRegular )
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    NRegular = &
         self%Gaussian%NRegular( L ) + &
         self%BSpline%GetNBSplines() - &
         self%NBSplineSkip
  end function ClassGaussianBSplineNRegular

  integer function ClassGaussianBSplineNPreconditioned( self, L, Identifier ) result( NPreconditioned )
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    character(len=*), optional,  intent(in) :: Identifier 
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::ClassGaussianBSplineNPreconditioned : "
    NPreconditioned = 0
    if( .not. self%Preconditioner(L)%Initialized ) call Assert(HERE//" Preconditioner is not initialized.")
    if ( (present(Identifier)) .and. (Identifier .is. "G") ) then
       !
       NPreconditioned = self%Gaussian%NPreconditioned(L)
       !
    else
       !
       NPreconditioned = &
            self%Gaussian%NPreconditioned(L) + &
            self%Preconditioner(L)%NPreconBSpline + &
            self%Preconditioner(L)%NNonOverlappingBSpline
       !
    end if
  end function ClassGaussianBSplineNPreconditioned


  subroutine GetRegularIndexes( self, Indexes, NIndexes, L )
    Class(ClassGaussianBSpline), intent(in) :: self
    integer, allocatable       , intent(out):: Indexes(:)
    integer                    , intent(out):: NIndexes
    integer                    , intent(in) :: L
    !
    integer :: imiGa, imaGa, i
    NIndexes = self%NRegular( L )
    if(allocated(Indexes))deallocate(Indexes)
    allocate(Indexes(NIndexes))
    imiGa = self%Gaussian%GetMinimumRegularIndex(L)
    imaGa = self%Gaussian%GetMaximumRegularIndex(L)
    Indexes(1:imaGa-imiGa+1)=[(i,i=imiGa,imaGa)]
    Indexes(imaGa-imiGa+2:NIndexes)=[(i,i=self%Gaussian%GetNFun()+1+self%NBSplineSkip,self%GetNFun())]
  end subroutine GetRegularIndexes


  !> Tells if a function in the Mixed-Basis with absolute index iFun
  !! belongs to the Gaussian subset
  !!
  logical function IsGaussian( self, iFun )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iFun
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::IsGaussian : "
    if( iFun < 1 .or. iFun > self%GetNFun() ) call Assert(HERE//"Invalid total index")
    IsGaussian = iFun <= self%Gaussian%GetNFun()
  end function IsGaussian


  !> Tells if a function in the Mixed-Basis with absolute index iFun
  !! belongs to the BSpline subset
  !!
  logical function IsBSpline( self, iFun )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iFun
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::IsGaussian : "
    if( iFun < 1 .or. iFun > self%GetNFun() ) call Assert(HERE//"Invalid total index")
    IsBSpline = .not. self%IsGaussian( iFun )
  end function IsBSpline


  integer function TotalToGaussianIndex( self, iFun ) result( iRel )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iFun
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::TotalToGaussianIndex : "
    if( .not. self%IsGaussian(iFun) )call Assert(HERE//"Invalid total index")
    iRel = iFun
  end function TotalToGaussianIndex


  integer function TotalToBSplineIndex( self, iFun ) result( iRel )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iFun
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::TotalToBSplineIndex : "
    if( .not. self%IsBSpline(iFun) )call Assert(HERE//"Invalid total index")
    iRel = iFun - self%Gaussian%GetNFun()
  end function TotalToBSplineIndex

  integer function GaussianToTotalIndex( self, iRel ) result( iAbs )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iRel
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::GaussianToTotalIndex : "
    if( iRel < 1 .or. iRel > self%Gaussian%GetNFun() )call Assert(HERE//"Invalid Gaussian index")
    iAbs = iRel
  end function GaussianToTotalIndex


  integer function BSplineToTotalIndex( self, iRel ) result( iAbs )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iRel
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::BSplineToTotalIndex : "
    if( iRel < 1 .or. iRel > self%BSpline%GetNBSplines() )call Assert(HERE//"Invalid BSpline index")
    iAbs = self%Gaussian%GetNFun() + iRel
  end function BSplineToTotalIndex


  !> Copies the Gaussian set sOrigin to sDestination 
  subroutine CopyGaussianBSplineSet(sDestination,sOrigin)
    Class(ClassGaussianBSpline), intent(inout):: sDestination
    Class(ClassGaussianBSpline), intent(in)   :: sOrigin
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::CopyGaussianBSpline : "

    call sDestination%Free()
    if( .not. sOrigin%Initialized ) call Assert(HERE//"Invalid GaussSet Origin")
    !
    sDestination%Lmax     = sOrigin%Lmax
    sDestination%BSpline  = sOrigin%BSpline
    sDestination%Gaussian = sOrigin%Gaussian
    !
    sDestination%NBSplineSkip                    = sOrigin%NBSplineSkip 
    sDestination%MatrixElementThreshold = sOrigin%MatrixElementThreshold
    sDestination%GaussianBSplineOverlapThreshold = sOrigin%GaussianBSplineOverlapThreshold
    sDestination%GaussianProjectorThreshold      = sOrigin%GaussianProjectorThreshold
    !
    sDestination%Preconditioner = sOrigin%Preconditioner
    !
    sDestination%INITIALIZED=.TRUE.
    !
  end subroutine CopyGaussianBSplineSet


  subroutine CopyGaussianBSplinePreconditioner( pOut, pInp )
    class(ClassGaussianBSplinePreconditioner), intent(in) :: pInp
    class(ClassGaussianBSplinePreconditioner), intent(out):: pOut
    !
    character(len=*), parameter :: HERE="ClassGaussianBSpline::CopyGaussianBSplinePreconditioner : "
    call pOut%Free()
    if( .not. pInp%Initialized ) call Assert(HERE//"Input Preconditioner not initialized")
    pOut%NOverBSpline=pInp%NOverBSpline
    pOut%NPreconBSpline=pOut%NPreconBSpline
    pOut%NNonOverlappingBSpline=pOut%NNonOverlappingBSpline
    allocate(pOut%E,source=pInp%E)
    allocate(pOut%A,source=pInp%A)
    pOut%Initialized = .TRUE.
  end subroutine CopyGaussianBSplinePreconditioner


  subroutine FreeGaussianBSplinePreconditioner( self )
    class(ClassGaussianBSplinePreconditioner), intent(inout) :: self
    self%NOverBSpline=0
    self%NPreconBSpline=0
    self%NNonOverlappingBSpline=0
    if(allocated(self%E))deallocate(self%E)
    if(allocated(self%A))deallocate(self%A)
    self%Initialized = .FALSE.
  end subroutine FreeGaussianBSplinePreconditioner
  subroutine FinalizeGaussianBSplinePreconditioner( self )
    type(ClassGaussianBSplinePreconditioner), intent(inout) :: self
    call self%free()
  end subroutine FinalizeGaussianBSplinePreconditioner


  function PreconditionerFileName( L ) result( strn )
    integer         , intent(in)  :: L
    character(len=:), allocatable :: strn
    allocate(strn,source=BSPLINE_PRECON_ROOT_NAME//AlphabeticNumber(L))
  end function PreconditionerFileName


  logical function PreconditionerIsAvailable( self, L, dir ) result( Available )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    character(len=*)           , intent(in) :: dir
    !
    character(len=:), allocatable  :: FileName
    FileName = trim(dir)//PreconditionerFileName( L )
    Available = self%MatchFingerPrintOnFile( FileName )
  end function PreconditionerIsAvailable


  !> Computes the transformation matrix that, among the
  !! splines which overlap with the preconditioned Gaussian
  !! subset, eliminates those which are almost linearly 
  !! dependent on the Gaussian subset.
  subroutine ComputePreconditioner( self, L )
    class(ClassGaussianBSpline)   , intent(inout) :: self
    integer                       , intent(in)    :: L

    character(len=*), parameter :: HERE = "ClassGaussianBSpline::ComputePreconditioner : "

    procedure(D2DFun), pointer  :: fptr
    integer :: MinRegGaussianIndex, MaxRegGaussianIndex, NRegGaussian
    integer :: MinRegContBSplineIndex, MaxRegContBSplineIndex, NRegContBSpline
    integer :: NPreconGaussian, NOverlappingBSpline, NPrecon
    integer :: iRowRel, iColRel, iRowAbs, iColAbs
    integer :: iRowGau, iRowBSp, iColBSp
    logical :: Stop_Execution
    DoublePrecision :: Overlap, Integral
    DoublePrecision, allocatable :: S_GReg_BReg(:,:)
    DoublePrecision, allocatable :: S_GPre_BReg(:,:)
    DoublePrecision, allocatable :: S_BOvr_BOvr(:,:)
    DoublePrecision, allocatable :: PreconGaussian(:,:)
    DoublePrecision, allocatable :: Projector(:,:), Eval(:)
    !

    if( L < 0 .or. L > self%GetLmax() ) call Assert(HERE//"Invalid angular momentum")

    call self%Preconditioner(L)%free()


    !.. Precondition the Gaussian basis
    
    call self%Gaussian%ComputePreconditioner( L )
    
    !.. Determines the number of regular Gaussian and BSpline functions
    !
    MinRegGaussianIndex = self%Gaussian%GetMinimumRegularIndex(L)
    MaxRegGaussianIndex = self%Gaussian%GetMaximumRegularIndex(L)
    NRegGaussian        = self%Gaussian%NRegular(L)
    !
    MinRegContBSplineIndex = self%NBSplineSkip + 1
    MaxRegContBSplineIndex = self%BSpline%GetNBSplines() - 1
    NRegContBSpline = MaxRegContBSplineIndex - MinRegContBSplineIndex + 1

    
    !.. Build the overlap matrix between regular Gaussian functions 
    !   and regular continuous BSplines
    allocate(S_GReg_BReg(NRegGaussian,NRegContBSpline))
    S_GReg_BReg=0.d0
    fptr => Unity
    GaussianLoop : do iRowRel = 1, NRegGaussian
       iRowGau = iRowRel + MinRegGaussianIndex - 1
       iRowAbs = self%GaussianToTotalIndex( iRowGau )
       BSplineLoop : do iColRel = 1, NRegContBSpline
          iColBSp = iColRel + MinRegContBSplineIndex - 1
          iColAbs = self%BSplineToTotalIndex( iColBSp )
          Integral = self%Integral( fptr, iRowAbs , iColAbs )
          S_GReg_BReg(iRowRel,iColRel) = Integral
          if( abs(Integral) < self%MatrixElementThreshold )exit BSplineLoop 
       enddo BSplineLoop
    enddo GaussianLoop


    !.. Transform the overlap matrix Sgrbr 
    !          (Gaussian-Regular,BSpline-Regular)   
    !   to the matrix Sgpbr
    !   (Gaussian-Preconditioned,BSpline-Regular)
    NPreconGaussian = self%Gaussian%NPreconditioned( L )
    allocate( PreconGaussian( NRegGaussian, NPreconGaussian ) )
    PreconGaussian = 0.d0
    call self%Gaussian%FetchPreconditioner( PreconGaussian, L )
    !
    allocate( S_GPre_BReg( NPreconGaussian, NRegContBSpline ) )
    S_GPre_BReg = 0.d0
    call DGEMM( "T", "N", NPreconGaussian, NRegContBSpline, NRegGaussian, &
         1.d0, PreconGaussian, NRegGaussian, S_GReg_BReg, NRegGaussian, &
         0.d0, S_GPre_BReg, NPreconGaussian )
    deallocate(S_GReg_BReg,PreconGaussian)


    !>  Determines the number of BSpline functions overlapping with 
    !!  the Gaussian functions.
    !!
    !!  A BSpline function \f$ B_i \f$ is considered overlapping
    !!  with the preconditioned Gaussian functions if
    !!  \f[
    !!     \sum_j |\langle G_j | B_i \rangle|^2 \leq \epsilon_{BG}
    !!  \f]
    !!  where \f$G_j\f$ are preconditioned Gaussian functions and 
    !!  \f$\epsilon_{BG}\f$ is a threshold imposed on the program.
    !!  As long as the preconditioned Gaussian basis is really
    !!  orthonormal, this condition is equivalent to asking that 
    !!  the probability of a state described by the BSpline \f$ B_i\f$
    !!  is smaller than $\epsilon_{BG}$ after projection on the
    !!  Gaussian space.
    !!  
    !!  The number of overlapping BSpline functions is defined 
    !!  such as all the BSplines with higher index are non-overlapping.
    NOverlappingBSpline = NRegContBSpline
    OverlapCycle : do 
       Overlap = dot_product(S_GPre_BReg(:,NOverlappingBSpline),S_GPre_BReg(:,NOverlappingBSpline))
       if( Overlap > self%GaussianBSplineOverlapThreshold )exit OverlapCycle
       NOverlappingBSpline = NOverlappingBSpline - 1
       if(NOverlappingBspline == 0) exit
    enddo OverlapCycle
    !
    if( NOverlappingBSpline == 0 )then
       call ErrorMessage("All BSplines are orthogonal to the Gaussian basis set.")
       self%Preconditioner(L)%NOverBSpline=0
       self%Preconditioner(L)%NPreconBSpline=0
       self%Preconditioner(L)%NNonOverlappingBSpline=self%BSpline%GetNBsplines()-self%NBsplineSkip
       self%Preconditioner(L)%Initialized=.TRUE.
       return
    endif
    write(OUTPUT_UNIT,"(a,i0)") " Number of overlapping BSplines : ", NOverlappingBSpline
    !
    if( NOverlappingBSpline == NRegContBSpline )&
         call ErrorMessage("All BSplines overlap to the Gaussian basis set")


    !.. Build the projector onto the preconditioned Gaussian basis
    !   in the Regular Continuous BSpline basis 
    allocate(Projector(NOverlappingBSpline,NOverlappingBSpline))
    Projector=0.d0
    call DGEMM("T","N",NOverlappingBSpline,NOverlappingBSpline,NPreconGaussian,&
         1.d0,S_GPre_BReg,NPreconGaussian,S_GPre_BReg,NPreconGaussian,&
         0.d0,Projector,NOverlappingBSpline)
    deallocate(S_GPre_BReg)


    !.. Build the overlap matrix between overlapping BSpline functions
    allocate( S_BOvr_BOvr( NOverlappingBSpline, NOverlappingBSpline ) )
    S_BOvr_BOvr=0.d0
    fptr => Unity
    do iColRel = 1, NOverlappingBSpline
       iColBSp = iColRel + MinRegContBSplineIndex - 1
       iColAbs = self%BSplineToTotalIndex( iColBSp )
       do iRowRel = 1, NOverlappingBSpline
          iRowBSp = iRowRel + MinRegContBSplineIndex - 1
          iRowAbs = self%BSplineToTotalIndex( iRowBSp )
          S_BOvr_BOvr(iRowRel,iColRel) = self%Integral( fptr, iRowAbs , iColAbs )
       enddo
    enddo


    !.. Diagonalize the projector on the Bspline basis
    allocate(Eval(NOverlappingBSpline))
    Eval=0.d0
    call SimpleDSYGV( NOverlappingBSpline, &
         Projector  , NOverlappingBSpline, &
         S_BOvr_BOvr, NOverlappingBSpline, Eval )
    deallocate(S_BOvr_BOvr)


    !.. Determines the number of BSpline functions
    !   which are linearly independent from the 
    !   Gaussian set
    !..
    NPrecon = 0
    stop_execution=.FALSE.
    do iColRel = 1, NOverlappingBSpline

       if( Eval(iColRel) < LOWER_PROJECTOR_TOLERANCE )then
          call ErrorMessage("Eigenvalue below lower projector tolerance")
          stop_execution=.TRUE.
       endif
       if( Eval(iColRel) > 1.d0 + UPPER_PROJECTOR_TOLERANCE )then
          call ErrorMessage("Eigenvalue above upper projector tolerance")
          stop_execution=.TRUE.
       endif

       if( Eval(iColRel) < 1.d0 - self%GaussianProjectorThreshold )then
          NPrecon = NPrecon + 1
          Projector(:,NPrecon)=Projector(:,iColRel)
       endif

       write(OUTPUT_UNIT,"(i0,3(x,e14.6),x,i0)") &
            iColRel, Eval(iColRel), 1.d0 - Eval(iColRel), self%GaussianProjectorThreshold, NPrecon

    enddo
    if(stop_execution) STOP
    write(OUTPUT_UNIT,*) "NRegular    :", NOverlappingBSpline
    write(OUTPUT_UNIT,*) "NPrecon     :", NPrecon
    if(NPrecon == 0)then
       call ErrorMessage(HERE//" Preconditioning failed")
       stop
    endif


    !.. Store the BSpline preconditioner
    allocate(self%Preconditioner(L)%A,source=Projector(:,1:NPrecon))
    allocate(self%Preconditioner(L)%E,source=Eval)
    self%Preconditioner(L)%NOverBSpline=NOverlappingBSpline
    self%Preconditioner(L)%NPreconBSpline=NPrecon
    self%Preconditioner(L)%NNonOverlappingBSpline=&
         self%BSpline%GetNBsplines()-NOverlappingBSpline-self%NBsplineSkip
    self%Preconditioner(L)%Initialized=.TRUE.


  contains

    Pure DoublePrecision function Unity( r )
      DoublePrecision, intent(in) :: r
      Unity = 1.d0
    end function Unity

  end subroutine ComputePreconditioner


  subroutine SavePreconditioner( self, L, dir, iostat )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: L
    character(len=*)           , intent(in) :: dir
    integer, optional          , intent(out):: iostat
    !
    character(len=*), parameter    :: HERE = "ClassGaussianBSpline::SavePreconditioner : "
    character(len=:), allocatable  :: FileName
    integer                        :: uid, iostat1
    character(len=IOMSG_LENGTH)    :: iomsg
    !
    if( .not. self%Preconditioner(L)%Initialized )call Assert(HERE//"Preconditioner is not initialized")
    !
    !.. Save the Gaussian preconditioner
    call self%Gaussian%SavePreconditioner(L,trim(dir))
    !
    !.. Save the BSpline preconditioner
    FileName = trim(dir)//PreconditionerFileName( L )
    open(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "unknown"    , &
         Action  = "write"      , &
         iostat  =  iostat1     , &
         iomsg   =  iomsg       )
    if(iostat1/=0)then
       call ErrorMessage(HERE//trim(iomsg))
       if(present(iostat))iostat=iostat1
       return
    endif
    call self%writeFingerprint( uid, iostat )
    call self%Preconditioner(L)%write( uid, iostat )
    close(uid)
    !
  end subroutine SavePreconditioner


  subroutine LoadPreconditioner( self, L, dir, iostat )
    class(ClassGaussianBSpline), intent(inout) :: self
    integer                    , intent(in)    :: L
    character(len=*)           , intent(in)    :: dir
    integer, optional          , intent(out)   :: iostat

    character(len=*), parameter    :: HERE = "ClassGaussianBSpline::LoadPreconditioner : "
    character(len=:), allocatable  :: FileName
    integer                        :: uid
    integer                        :: iostat1
    character(len=IOMSG_LENGTH)    :: iomsg

    !.. Load the Gaussian preconditioner
    call self%Gaussian%LoadPreconditioner(L,trim(dir))

    !.. Load the BSpline preconditioner
    allocate(FileName,source=trim(dir)//PreconditionerFileName( L ))
    open(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "old"        , &
         Action  = "read"       , &
         iostat  =  iostat1     , &
         iomsg   =  iomsg       )
    if ( iostat1 /= 0) then
       if(present(iostat)) then
          iostat = iostat1
          call ErrorMessage(HERE//trim(iomsg))
          return
       endif
    end if
    if(.not.self%matchFingerprintOnUnit(uid))then
       call ErrorMessage("Invalid Fingerprint in BSpline Preconditioner file "//trim(FileName))
       iostat=-1
       return
    endif
    call self%Preconditioner(L)%read( uid, iostat )
    close(uid)
    !
  end subroutine LoadPreconditioner


  !> Write on unformatted unit the content of a 
  !! GaussianBSplinePreconditioner object
  !
  ! *** Generalize to deal with the case of both formatted and
  !     unformatted units
  !
  subroutine WriteBSplinePreconditionerOnUnit( self, uid, iostat )
    class(ClassGaussianBSplinePreconditioner), intent(in) :: self
    integer                                  , intent(in) :: uid
    integer, optional                        , intent(out):: iostat
    integer :: iOver, iPre
    integer :: iostat1, iostat2, iostat3, iostat4, iostat5
    write(uid,iostat=iostat1) self%NOverBSpline
    write(uid,iostat=iostat2) self%NPreconBSpline
    write(uid,iostat=iostat3) self%NNonOverlappingBSpline
    write(uid,iostat=iostat4)(self%E(iOver), &
         iOver=1,self%NOverBSpline)
    write(uid,iostat=iostat5)((self%A(iOver,iPre), &
         iOver=1,self%NOverBSpline), &
         iPre =1,self%NPreconBSpline )
    if( present( iostat ) )then
       iostat = 0
       if ( iostat1 /= 0 .or. &
            iostat2 /= 0 .or. &
            iostat3 /= 0 .or. &
            iostat4 /= 0 .or. &
            iostat5 /= 0 ) iostat = -1
    endif
  end subroutine WriteBSplinePreconditionerOnUnit


  !> Read from unformatted unit the content of a 
  !! GaussianBSplinePreconditioner object
  subroutine ReadBSplinePreconditionerOnUnit( self, uid, iostat )
    class(ClassGaussianBSplinePreconditioner), intent(out):: self
    integer                                  , intent(in) :: uid
    integer, optional                        , intent(out):: iostat
    integer :: iOver, iPre
    integer :: iostat1, iostat2, iostat3, iostat4, iostat5
    call self%free()
    self%Initialized=.TRUE.
    read(uid,iostat=iostat1) self%NOverBSpline
    read(uid,iostat=iostat2) self%NPreconBSpline
    read(uid,iostat=iostat3) self%NNonOverlappingBSpline
    allocate(self%E(self%NOverBSpline))
    read(uid,iostat=iostat4)(self%E(iOver), &
         iOver=1,self%NOverBSpline)
    allocate(self%A(self%NOverBSpline,self%NPreconBSpline))
    read(uid,iostat=iostat5)((self%A(iOver,iPre), &
         iOver=1,self%NOverBSpline), &
         iPre =1,self%NPreconBSpline )
    if( present(iostat) )then
       iostat = 0
       if ( iostat1 /= 0 .or. &
            iostat2 /= 0 .or. &
            iostat3 /= 0 .or. &
            iostat4 /= 0 .or. &
            iostat5 /= 0 )then
          iostat = -1
          self%Initialized = .FALSE.
       endif
    endif
  end subroutine ReadBSplinePreconditionerOnUnit


  subroutine FetchPreconditioner( self, Precon, L, NRegOver, NPreOver )
    class(ClassGaussianBSpline) , intent(in) :: self
    real(kind(1d0)), allocatable, intent(out):: Precon(:,:)
    integer                     , intent(in) :: L
    !
    !! NRegOver, which stands for, Number of Regular Overlapping functions,
    !! is the number of first functions in the regular basis that 
    !! are affected by the overall preconditioner, which is composed by
    !! two diagonal rectangular block: one for the preconditioning
    !! of the Gaussian Basis, and one for the preconditioning of the
    !! Bspline basis. In this way, even if the transformation could
    !! be carried out separately for the Gaussian and the BSpline
    !! regular sub-basis, both operations can be performed in one shot.
    !! Thus, NRegOver, is the SUM of the number of regular Gaussian functions 
    !! and that of the regular BSpline functions overlapping to them.
    !! NRegOver differs from the total number of regular functions by 
    !! the number of non-overlapping BSpline functions.
    integer, optional, intent(out):: NRegOver
    !
    !! NPreOver is the SUM of the preconditioned Gaussian functions and
    !! of the preconditioned overlapping functions.
    !! NPreOver differs from the total number of preconditioned functions 
    !! by the number of non-overlapping BSpline functions.
    integer, optional, intent(out):: NPreOver

    character(len=*), parameter :: HERE="ClassGaussianBSpline::FetchPreconditioner : "
    integer :: NRegular, NPrecon, iFun
    integer :: NRegularGaussian, NPreconGaussian
    integer :: NOverBspline, NPreconBSpline

    if( .not. self%Preconditioner( L )%Initialized ) call Assert(HERE//"Preconditioner not available")
    if(allocated(Precon))deallocate(Precon)

    NRegularGaussian=self%Gaussian%NRegular(L)
    NPreconGaussian =self%Gaussian%NPreconditioned(L)

    NOverBSpline = self%Preconditioner(L)%NOverBSpline
    NPreconBSpline=self%Preconditioner(L)%NPreconBSpline

    NRegular=self%NRegular(L)
    NPrecon=self%NPreconditioned(L)
    allocate(Precon(NRegular,NPrecon))
    Precon=0.d0

    !.. Gaussian Preconditioner
    call self%Gaussian%FetchPreconditioner(Precon(1:NRegularGaussian,1:NPreconGaussian),L)

    !.. Overlapping BSpline preconditioner
    if(present(NRegOver)) NRegOver=NRegularGaussian+NOverBSpline
    if(present(NPreOver)) NPreOver=NPreconGaussian+NPreconBSpline
    Precon(NRegularGaussian+1:NRegularGaussian+NOverBSpline,&
         NPreconGaussian+1:NPreconGaussian+NPreconBSpline)=&
         self%Preconditioner(L)%A

    !.. Non-overlapping BSpline identity
    do iFun=0,self%Preconditioner(L)%NNonOverlappingBSpline-1
       Precon(NRegular-iFun,NPrecon-iFun)=1.d0
    enddo

  end subroutine FetchPreconditioner


  !> Evaluate the normalized radial function (Total)
  !! \f[
  !!    u_{i}(r) = ...
  !! \]
  DoublePrecision function GaussianBSplineEval( self, x, iFun, iDer ) &
       result( Res )
    class( ClassGaussianBSpline ), intent(in) :: self
    DoublePrecision              , intent(in) :: x
    integer                      , intent(in) :: iFun
    integer, optional            , intent(in) :: iDer
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::Eval : "

    Res = 0.d0
    if( iFun < 1 .or. iFun > self%GetNFun() )return

    if( self%IsGaussian(iFun) )then
       Res = self%Gaussian%eval( x, self%TotalToGaussianIndex(iFun), iDer )
    else
       Res = self%BSpline%eval( x, self%TotalToBSplineIndex(iFun), iDer )
    end if
  end function GaussianBSplineEval


  DoublePrecision function GaussianBSplineFunctionEval( self, x, vec, iDer, Identifier ) &
       result( Res )
    class( ClassGaussianBSpline ), intent(in) :: self
    DoublePrecision       , intent(in) :: x
    DoublePrecision       , intent(in) :: vec(:)
    integer, optional     , intent(in) :: iDer
    character(len=*), optional, intent(in) :: Identifier
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::GaussianBSplineFunctionEval : "
    integer :: iFun, InitFun, MaxFun
    !
    if ( present(Identifier) ) then
       if ( Identifier .is. "G" ) then
          InitFun = 1
          MaxFun  = self%GetGaussianNFun()
       elseif ( Identifier .is. "B" ) then
          InitFun = self%GetGaussianNFun() + 1
          MaxFun  = self%GetNFun()
       else
          call Assert("If present the flag for the evaluation must be either 'G' for Gaussian or 'B' for B-splines.")
       end if
    else
       InitFun = 1
       MaxFun  = self%GetNFun()
    end if
    !
    Res = 0.d0
    if( LBOUND(vec,1) > 1              ) call Assert(HERE//"vec : invalid LBOUND")
    if( UBOUND(vec,1) < self%GetNFun() ) call Assert(HERE//"vec : invalid UBOUND")
    do iFun = InitFun,  MaxFun
       Res = Res + self%eval( x, iFun, iDer ) * vec( iFun )
    enddo
  end function GaussianBSplineFunctionEval


  subroutine GaussianBSplineTabulate( self, ndata, xvec, yvec, iFun, iDer )
    class( ClassGaussianBSpline ), intent(in)  :: self
    integer                      , intent(in)  :: ndata
    DoublePrecision              , intent(in)  :: xvec(:)
    DoublePrecision              , intent(out) :: yvec(:)
    integer                      , intent(in)  :: iFun
    integer, optional            , intent(in)  :: iDer
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::GaussianBSplineTabulate : "
    integer :: iData
    do iData = 1, nData
       yvec(iData) = self%eval( xvec(iData), iFun, iDer )
    enddo
  end subroutine GaussianBSplineTabulate


  subroutine GaussianBSplineFunctionTabulate( self, ndata, xvec, yvec, fvec, iDer )
    class( ClassGaussianBSpline ), intent(in)  :: self
    integer                      , intent(in)  :: ndata
    DoublePrecision              , intent(in)  :: xvec(:)
    DoublePrecision              , intent(out) :: yvec(:)
    DoublePrecision              , intent(in)  :: fvec(:)
    integer, optional            , intent(in)  :: iDer
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::GaussianBSplineFunctionTabulate : "
    integer :: iData
    do iData = 1, nData
       yvec(iData) = self%eval( xvec(iData), fvec, iDer )
    enddo
  end subroutine GaussianBSplineFunctionTabulate


  !> Computes 1D integrals of arbitrary functions with 
  !! arbitrary derivatives of elements of the radial basis
  !!
  DoublePrecision function GaussianBSplineGeneralIntegral( &
       self              , &
       FunPtr            , & 
       iBra              , &
       iKet              , &
       BraDerivativeOrder, &
       KetDerivativeOrder, &
       LowerBound        , &
       UpperBound        , &
       BreakPoint        ) &
       result( Integral )
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    procedure(D2DFun)          , pointer    :: FunPtr
    integer                    , intent(in) :: iBra
    integer                    , intent(in) :: iKet
    integer        , optional  , intent(in) :: BraDerivativeOrder
    integer        , optional  , intent(in) :: KetDerivativeOrder
    DoublePrecision, optional  , intent(in) :: LowerBound
    DoublePrecision, optional  , intent(in) :: UpperBound
    DoublePrecision, optional  , intent(in) :: BreakPoint
    !
    character(len=*), parameter :: HERE = "ClassGaussianBSpline::GaussianBSplineGeneralIntegral : "

    logical :: BraIsGaussian, KetIsGaussian, BraIsBSpline, KetIsBSpline
    logical :: GaussianGaussian, BSplineBSpline, GaussianBSpline, BSplineGaussian
    integer :: iBraG, iKetG, iBraB, iKetB
    integer :: iGaussian, iBSpline, iGaussianDerivative, iBSplineDerivative

    procedure(D2DFun), pointer :: FunGaussianPtr

    Integral = 0.d0

    BraIsGaussian = self%IsGaussian( iBra )
    KetIsGaussian = self%IsGaussian( iKet )

    BraIsBSpline = self%IsBSpline( iBra )
    KetIsBSpline = self%IsBSpline( iKet )

    GaussianGaussian = BraIsGaussian .and. KetIsGaussian
    GaussianBSpline  = BraIsGaussian .and. KetIsBSpline
    BSplineGaussian  = BraIsBSpline  .and. KetIsGaussian
    BSplineBSpline   = BraIsBSpline  .and. KetIsBSpline

    if( GaussianGaussian )then

       iBraG = self%TotalToGaussianIndex( iBra )
       iKetG = self%TotalToGaussianIndex( iKet )
       Integral = self%Gaussian%Integral( FunPtr, iBraG, iKetG, &
            BraDerivativeOrder, KetDerivativeOrder, &
            LowerBound, UpperBound, BreakPoint )

    elseif( BSplineBSpline )then

       iBraB = self%TotalToBSplineIndex( iBra )
       iKetB = self%TotalToBSplineIndex( iKet )
       Integral = self%BSpline%Integral( FunPtr, iBraB, iKetB, &
            BraDerivativeOrder, KetDerivativeOrder, &
            LowerBound, UpperBound, BreakPoint )

    else

       iGaussianDerivative = 0
       iBSplineDerivative = 0

       if( GaussianBSpline )then

          iGaussian = self%TotalToGaussianIndex( iBra )
          if(present(BraDerivativeOrder))iGaussianDerivative = BraDerivativeOrder

          iBSpline = self%TotalToBSplineIndex(  iKet )
          if(present(KetDerivativeOrder)) iBSplineDerivative = KetDerivativeOrder

       elseif( BSplineGaussian )then

          if(present(BraDerivativeOrder)) iBSpline = self%TotalToBSplineIndex(  iBra )
          iBSplineDerivative = BraDerivativeOrder

          iGaussian = self%TotalToGaussianIndex( iKet )
          if(present(KetDerivativeOrder))iGaussianDerivative = KetDerivativeOrder

       endif

       FunGaussianPtr => FunPtrGaussianProduct
       Integral = self%BSpline%MonoIntegral( FunGaussianPtr, iBSpline, &
            iBSplineDerivative, LowerBound, UpperBound, BreakPoint ) 

    endif

  contains

    real(kind(1d0)) function FunPtrGaussianProduct(x) result(y)
      real(kind(1d0)), intent(in) :: x
      y = FunPtr(x) * self%Gaussian%Eval( x, iGaussian, iGaussianDerivative )
    end function FunPtrGaussianProduct

  end function GaussianBSplineGeneralIntegral


  !! RemainingKets(Bras)GiveZero
  !! Given, for some operator, the matrix element between 
  !! a pair of row and column indexes, tell whether the 
  !! matrix elements with higher column (row) indexes are 
  !! likely to be smaller in magnitude than the threshold for 
  !! matrix elements specified in the configuration file.
  !! 
  !! This is likely to be the case if the row (column) is 
  !! a gaussian and the column (row) is a BSpline already 
  !! so far away that the Integral is negligibly small.
  !!
  !! Possible risks: 
  !!
  !!  1. non-smooth potentials
  !!  2. Integrals which are accidentally very close to zero.
  !!
  logical function RemainingKetsGiveZero( self, iBra, iKet, Integral )&
       result( AllLikelyZero )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iBra
    integer                    , intent(in) :: iKet
    DoublePrecision            , intent(in) :: Integral
    AllLikelyZero = &
         self%IsGaussian(iBra) .and. &
         self%IsBSpline(iKet)  .and. &
         abs(Integral) < self%MatrixElementThreshold
  end function RemainingKetsGiveZero

  logical function RemainingBrasGiveZero( self, iBra, iKet, Integral )&
       result( AllLikelyZero )
    class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iBra
    integer                    , intent(in) :: iKet
    DoublePrecision            , intent(in) :: Integral
    AllLikelyZero = &
         self%IsGaussian(iKet) .and. &
         self%IsBSpline(iBra)  .and. &
         abs(Integral) < self%MatrixElementThreshold
  end function RemainingBrasGiveZero



  !> Frees the GaussianBSpline's fingerprint class. 
  subroutine FingerPrintFree( self )
    !
    class( ClassGaussianBSplineFingerPrint ), intent(inout)  :: self
    !
    call self%BSplineFingerPrint%Free()
    call self%GaussianFingerPrint%Free()
    !
    self%Lmax = 0
    self%NBSplineSkip = 0
    self%MatrixElementThreshold = 0.d0
    self%GaussianBSplineOverlapThreshold = 0.d0
    self%GaussianProjectorThreshold = 0.d0
    !
  end subroutine FingerPrintFree


  !> Write the GaussianBSpline basis' fingerprint in a unit.
  subroutine FingerPrintWrite( self, OutputUnit, iostat )
    !
    class( ClassGaussianBSplineFingerPrint ), intent(in) :: self
    integer                                 , intent(in) :: OutputUnit
    integer, optional                       , intent(out):: iostat
    !
    character(len=10)  :: IsUnitFormatted, UnitIsWritable
    logical :: UnitIsOpen
    !
    if(present(iostat))iostat=0
    Inquire(&
         Unit      = OutputUnit      , &
         FORMATTED = IsUnitFormatted , &
         OPENED    = UnitIsOpen      , &
         WRITE     = UnitIsWritable  )
    if( UnitIsWritable == "NO" .or. .not. UnitIsOpen ) &
         call Assert("ClassGaussianBSpline::FingerPrintWrite : Invalid unit")

    if( IsUnitFormatted == "YES" )then
       !
       write(OutputUnit,"(2i23)") self%LMax, self%NBSplineSkip
       write(OutputUnit,"(3(x,d24.16))") &
            self%MatrixElementThreshold, &
            self%GaussianBSplineOverlapThreshold, &
            self%GaussianProjectorThreshold
       !
    elseif( IsUnitFormatted == "NO" )then
       !
       write(OutputUnit) self%LMax, self%NBSplineSkip
       write(OutputUnit) &
            self%MatrixElementThreshold, &
            self%GaussianBSplineOverlapThreshold, &
            self%GaussianProjectorThreshold
       !
    else
       call Assert("ClassGaussianBSpline::FingerPrintWrite : Invalid format")
    endif
    !
    call self%GaussianFingerPrint%Write( OutputUnit )
    call self%BSplineFingerPrint%Write( OutputUnit )
    !
  end subroutine FingerPrintWrite



  !> Reads the GaussianBSpline basis' fingerprint from a unit.
  subroutine FingerPrintRead( self, InputUnit, iostat )
    !
    class( ClassGaussianBSplineFingerPrint ), intent(inout) :: self
    integer,                                  intent(in)    :: InputUnit
    integer, optional,                        intent(inout) :: iostat
    !
    character(len=10) :: IsUnitFormatted, UnitIsReadable
    logical :: UnitIsOpen
    integer :: iostat_
    integer :: Lmax
    integer :: NBSplineSkip
    real(kind(1d0)) :: MatrixElementThreshold
    real(kind(1d0)) :: GaussianBSplineOverlapThreshold
    real(kind(1d0)) :: GaussianProjectorThreshold
    character(len=IOMSG_LENGTH)::iomsg
    !
    if(present(iostat))iostat=0
    !
    Inquire(&
         Unit      = InputUnit       , &
         FORMATTED = IsUnitFormatted , &
         OPENED    = UnitIsOpen      , &
         READ      = UnitIsReadable  )
    !
    if( .not.UnitIsOpen )then
       !
       if(present(iostat))then
          IOSTAT=1
          return
       end if
       !
       call Assert("ClassGaussianBSpline::FingerPrintRead : Unit is not open")
       !
    endif
    !
    if( UnitIsReadable=="NO" )then
       !
       if(present(iostat))then
          IOStat=1
          return
       end if
       !
       call Assert("ClassGaussianBSpline::FingerPrintRead : Unreadable Unit")
       !
    endif

    if( IsUnitFormatted == "YES" )then
       !
       read(InputUnit,*,iostat=iostat_,iomsg=iomsg) Lmax, NBSplineSkip
       !
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
       read(InputUnit,"(3(d24.16))",iostat=iostat_,iomsg=iomsg) &
            MatrixElementThreshold, &
            GaussianBSplineOverlapThreshold, &
            GaussianProjectorThreshold
       !
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
       call self%Init( Lmax, &
            NBSplineSkip, &
            MatrixElementThreshold, &
            GaussianBSplineOverlapThreshold, &
            GaussianProjectorThreshold   )
       !
    elseif( IsUnitFormatted == "NO" )then
       !
       read(InputUnit,iostat=iostat_,iomsg=iomsg) Lmax, NBSplineSkip
       !
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
       read(InputUnit,iostat=iostat_,iomsg=iomsg) &
            MatrixElementThreshold, &
            GaussianBSplineOverlapThreshold, &
            GaussianProjectorThreshold
       !
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
       call self%Init( Lmax, &
            NBSplineSkip, &
            MatrixElementThreshold, &
            GaussianBSplineOverlapThreshold, &
            GaussianProjectorThreshold   )
       !
    else
       !
       if(present(iostat))then
          iostat=2
          return
       end if
       call Assert("ClassGaussianBSpline::FingerPrintRead : Invalid format")
    endif
    !
    ! This reads and propperly initializes the separated set of basis.
    call self%GaussianFingerPrint%Read( InputUnit )
    call self%BSplineFingerPrint%Read( InputUnit )
    !
  end subroutine FingerPrintRead



  subroutine FingerPrintInit( self, &
       Lmax, &
       NBSplineSkip, &
       MatrixElementThreshold, &
       GaussianBSplineOverlapThreshold, &
       GaussianProjectorThreshold )
    !
    class( ClassGaussianBSplineFingerPrint ), intent(inout) :: self
    integer,                                  intent(in)    :: Lmax
    integer,                                  intent(in)    :: NBSplineSkip
    real(kind(1d0)),                          intent(in)    :: MatrixElementThreshold
    real(kind(1d0)),                          intent(in)    :: GaussianBSplineOverlapThreshold
    real(kind(1d0)),                          intent(in)    :: GaussianProjectorThreshold
    !
    self%Lmax = Lmax
    self%NBSplineSkip = NBSplineSkip
    self%MatrixElementThreshold = MatrixElementThreshold
    self%GaussianBSplineOverlapThreshold = GaussianBSplineOverlapThreshold
    self%GaussianProjectorThreshold = GaussianProjectorThreshold
    !
  end subroutine FingerPrintInit



  subroutine ExtractFingerprint( self, GaussianBSplineSet )
    !
    class( ClassGaussianBSplineFingerPrint ), intent(inout) :: self
    class( ClassGaussianBSpline ),            intent(in)    :: GaussianBSplineSet
    !
    call self%Init( GaussianBSplineSet%Lmax, &
         GaussianBSplineSet%NBSplineSkip, &
         GaussianBSplineSet%MatrixElementThreshold, &
         GaussianBSplineSet%GaussianBSplineOverlapThreshold, &
         GaussianBSplineSet%GaussianProjectorThreshold  )
    !
    self%GaussianFingerPrint = GaussianBSplineSet%Gaussian
    self%BSplineFingerPrint = GaussianBSplineSet%BSpline
    !
  end subroutine ExtractFingerprint



  logical function GaussianBSplineMatchFingerPrint( self, FingerPrint ) result( Match )
    !
    class( ClassGaussianBSpline ),            intent(in) :: self    
    class( ClassGaussianBSplineFingerPrint ), intent(in) :: FingerPrint
    !
    real(kind(1d0)), parameter :: TOLERANCE=1.d-12
    !
    Match = .false.
    !
    if( FingerPrint%Lmax   /= self%Lmax   ) return
    if( FingerPrint%NBSplineSkip /= self%NBSplineSkip ) return
    !
    if( abs( &
         FingerPrint%MatrixElementThreshold / &
         self%MatrixElementThreshold-1.d0 ) > TOLERANCE )return
    if( abs( & 
         FingerPrint%GaussianBSplineOverlapThreshold / &
         self%GaussianBSplineOverlapThreshold-1.d0 ) > TOLERANCE )return
    if( abs( & 
         FingerPrint%GaussianProjectorThreshold / &
         self%GaussianProjectorThreshold-1.d0 ) > TOLERANCE )return
    !
    Match = .true.
    !
    Match = Match .and. &
         self%Gaussian%MatchFingerPrint(FingerPrint%GaussianFingerPrint) .and. &
         self%BSpline%MatchFingerPrint(FingerPrint%BSplineFingerPrint)
    !
  end function GaussianBSplineMatchFingerPrint



  logical function GaussianBSplineMatchFingerPrintOnUnit( self, uid ) result( Match )
    !
    class( ClassGaussianBSpline ), intent(in) :: self    
    integer,                       intent(in) :: uid
    !
    type( ClassGaussianBSplineFingerPrint ) :: FingerPrint
    !
    call FingerPrint%Read( uid )
    !
    Match = self%MatchFingerPrint( FingerPrint )
    !
    call FingerPrint%Free()
    !
  end function GaussianBSplineMatchFingerPrintOnUnit



  logical function GaussianBSplineMatchFingerPrintOnFile( self, FileName ) result( Match )
    !
    class( ClassGaussianBSpline ), intent(in) :: self    
    character(len=*),              intent(in) :: FileName
    !
    character(len=*), parameter    :: HERE = "ClassGaussianBSpline::MatchFingerprintOnFile : "
    integer                        :: uid
    integer                        :: iostat
    character(len=IOMSG_LENGTH)    :: iomsg
    !
    open(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "old"        , &
         Action  = "read"       , &
         iostat  =  iostat      , &
         iomsg   =  iomsg       )
    !
    if(iostat/=0)then
       Match=.FALSE.
       return
    endif
    !
    Match=self%MatchFingerPrintOnUnit( uid )
    close(uid)
    !
  end function GaussianBSplineMatchFingerPrintOnFile



  subroutine GaussianBSplineWriteFingerPrint( self, uid, iostat )
    !
    class( ClassGaussianBSpline ), intent(in) :: self    
    integer,                       intent(in) :: uid
    integer, optional            , intent(out):: iostat
    !
    type( ClassGaussianBSplineFingerPrint ) :: FingerPrint
    !
    FingerPrint = self
    !
    call FingerPrint%Write( uid, iostat )
    !
    call FingerPrint%Free()
    !
  end subroutine GaussianBSplineWriteFingerPrint


  !> Computes the monoelectronic multipole
  !! \f[
  !!    \int_0^\infty dr f_1(R)\frac{r_<^\ell}{r_>^{\ell+1}}f_2(r)
  !! \f]
  !! where \f$r_<=\min(r,R)\f$ and \f$r_>=\max(r,R)\f$. 
  !!
  DoublePrecision function MonoElectronicMultipole(self,iBra,iKet,R,l) &
       result(Multipole)
    Class(ClassGaussianBSpline), intent(in) :: self
    integer                    , intent(in) :: iBra, iKet
    DoublePrecision            , intent(in) :: R
    integer                    , intent(in) :: l
    procedure(D2DFun)          , pointer    :: FunPtr
    FunPtr => MultipolarPotential
    Multipole = self%Integral( FunPtr, iBra, iKet, BreakPoint = R )
  contains
    DoublePrecision function MultipolarPotential(x) result(y)
      DoublePrecision, intent(in) :: x
      y = myexp(min(x,R),l)*myexp(max(x,R),-l-1)
    end function MultipolarPotential
  end function MonoElectronicMultipole


  !> return  \f$ a^b \f$
  real(kind(1d0)) function myexp_DD(a,b) result(res)
    real(kind(1d0)), intent(in) :: a,b
    res=1.d0; if(b==0.d0) return
    res=0.d0; if(a<=0.d0) return
    res=exp(b*log(a))
  end function myexp_DD


  !> return  \f$ a^b \f$
  DoublePrecision function myexp_DI(a,ib) result(res)
    DoublePrecision, intent(in) :: a
    integer        , intent(in) :: ib
    integer         :: iExp
    DoublePrecision :: a_
    res=1.d0
    if(ib==0) return
    res=0.d0
    if(a==0.d0.or.a==-0.d0)return
    res=1.d0
    if(ib>0)then
       do iExp = 1, ib
          res=res*a
       enddo
    else
       a_=1.d0/a
       do iExp = ib, -1
          res=res*a_
       enddo
    endif
  end function myexp_DI


  !> Creates in a directory all the configuration
  !!    files that define the current basis.
  subroutine SaveConfig(self, ConfigurationFile, Dir)
    !
    Class(ClassGaussianBSpline), intent(in) :: self
    character(len=*)           , intent(in) :: ConfigurationFile
    character(len=*)           , intent(in) :: Dir
    !
    type(ClassParameterList) :: List
    character(len=512) :: BSplineBasisFile
    character(len=512) :: GaussianBasisFile
    !
    call List%Add( "BSplineBasisFile"  , "OuterBSplineBasis", "required" )
    call List%Add( "GaussianBasisFile" , "GaussianBasis", "required" )
    call List%Parse( ConfigurationFile )
    call List%Get( "BSplineBasisFile", BSplineBasisFile )
    call List%Get( "GaussianBasisFile", GaussianBasisFile  )
    !
    BSplineBasisFile  = trim(adjustl(BSplineBasisFile))
    GaussianBasisFile = trim(adjustl(GaussianBasisFile))
    !
    call SYSTEM("mkdir -p "//trim(Dir))
    call SYSTEM("cp "//trim(ConfigurationFile)//" "//trim(Dir))
    call SYSTEM("cp "//trim(BSplineBasisFile)//" "//trim(Dir))
    call SYSTEM("cp "//trim(GaussianBasisFile)//" "//trim(Dir))
    !
  end subroutine SaveConfig



  !> Gets a vector with all the monomials exponents belonging to the Gaussian functions.
  subroutine GetAllMonomials( self, Vec )
    !
    class(ClassGaussianBSpline), intent(in)  :: self
    integer, allocatable,        intent(out) :: Vec(:)
    !
    call self%Gaussian%GetAllMonomials( Vec )
    !
  end subroutine GetAllMonomials



  !> Gets a vector with all the monomials exponents belonging to the Gaussian functions.
  subroutine GetAllExponents( self, Vec )
    !
    class(ClassGaussianBSpline), intent(in)  :: self
    real(kind(1d0)), allocatable,intent(out) :: Vec(:)
    !
    call self%Gaussian%GetAllExponents( Vec )
    !
  end subroutine GetAllExponents



end module ModuleGaussianBSpline
