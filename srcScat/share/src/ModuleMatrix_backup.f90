!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

!> Defines a general matrix kind and basic operations on it
module ModuleMatrix

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling

  implicit none
  private

  integer         , parameter :: DEFAULT_OUTPUT_UNIT  = OUTPUT_UNIT
  integer         , parameter :: DEFAULT_INPUT_UNIT   = INPUT_UNIT

  integer         , parameter :: FILE_NAME_LENGTH     = 512
  character(len=*), parameter :: DOUBLE_PRINT_FORMAT  = "(d24.16)"

  enum, bind(c)
     enumerator :: MATRIX_PATTERN_FULL
     enumerator :: MATRIX_PATTERN_BANDED
     enumerator :: MATRIX_PATTERN_UNDEFINED
     enumerator :: MATRIX_PATTERN_DIAGONAL
  end enum
  

  !> Defines a general DoublePrecision matrix
  type, public :: ClassMatrix
     !
     private
     !
     !.. Pattern : Filled pattern
     !.. NR      : Number of Rows
     !.. NC      : Number of Columns
     !.. NU      : Number of Upper diagonals in banded matrix.
     !.. NL      : Number of Lower diagonals in banded matrix
     !
     integer :: Pattern = MATRIX_PATTERN_UNDEFINED
     !
     integer :: NR = 0
     integer :: NC = 0
     integer :: NU = 0
     integer :: NL = 0
     !
     integer :: NRMin = 0
     integer :: NRMax = 0
     integer :: NCMin = 0
     integer :: NCMax = 0
     !
     DoublePrecision, public, allocatable :: A(:,:) 
     !
   contains
     !
     procedure :: InitFull       => ClassMatrixInitFull
     procedure :: InitBanded     => ClassMatrixInitBanded
     procedure :: Free           => ClassMatrixFree
     procedure :: IsFull         => ClassMatrixIsFull
     procedure :: IsBanded       => ClassMatrixIsBanded
     procedure :: IsDiagonal     => ClassMatrixIsDiagonal
     procedure :: LowerBandWidth => ClassMatrixLowerBandwidth
     procedure :: UpperBandWidth => ClassMatrixUpperBandwidth
     !
     procedure :: NRows         => ClassMatrixNRows
     procedure :: NColumns      => ClassMatrixNColumns
     procedure :: Element       => ClassMatrixElement
     procedure :: SetElement    => ClassMatrixSetElement
     procedure :: GetSubMatrix
     procedure :: IqualDimensionTo
     !
     procedure :: Write => WriteClassMatrixToUnit
     procedure :: Read  => ReadClassMatrixFromUnit
     procedure :: Multiply => Double_x_Matrix
     procedure :: Add => AddMatrixToMatrix

     generic :: assignment(=) => &
          AssignDoubleToClassMatrix, &
          AssignIntegerToClassMatrix, &
          CopyClassMatrixToClassMatrix
     procedure, private :: AssignDoubleToClassMatrix
     procedure, private :: AssignIntegerToClassMatrix
     procedure, private :: CopyClassMatrixToClassMatrix

     generic            :: Diagonalize => GeneralEigenValueSolver, EigenvalueSolver
     procedure, private :: GeneralEigenValueSolver
     procedure, private :: EigenvalueSolver
     procedure, private :: HasSameShapeAs

!!$     generic   :: operator(+)   =>   ClassMatrixPlusClassMatrix
!!$          ClassMatrixPlusInteger    , &
!!$          ClassMatrixPlusDouble     , &
!!$          ClassMatrixPlusMatrix     , &
!!$          IntegerPlusClassMatrix    , &
!!$          DoublePlusClassMatrix     , &
!!$          MatrixPlusClassMatrix     
!!$     !
!!$     procedure :: ClassMatrixPlusClassMatrix
!!$     generic   :: operator(*)   =>    &
!!$          ClassMatrixTimesDouble, &
!!$          DoubleTimesClassMatrix
!!$          ClassMatrixTimesClassMatrix, &
!!$          ClassMatrixTimesInteger    , &
!!$          ClassMatrixTimesMatrix     , &
!!$          IntegerTimesClassMatrix    , &
!!$          DoubleTimesClassMatrix     , &
!!$          MatrixTimesClassMatrix  
     !
     final :: ClassMatrixFinalize
     !
  end type ClassMatrix
  
  type, public :: ClassSpectralResolution
     !
     private
     integer                      :: NEigenvalues
     integer                      :: Dim
     DoublePrecision, allocatable :: EigenValues(:)
     DoublePrecision, allocatable :: EigenVectors(:,:)
     !
   contains
     !
     procedure :: NEval            =>  NevalSpectralResolution
     generic   :: Init             =>  InitSpectralResolutionFull, InitSpectralResolutionReduced
     procedure :: IsConsistent     =>  SpectralResolutionIsConsistent
     generic   :: Read             =>  ReadSpectralResolutionFromUnit, ReadSpectralResolutionFromFile
     generic   :: Write            =>  WriteSpectralResolutionToUnit, WriteSpectralResolutionToFile
     generic   :: WriteEigenvalues =>  WriteEigenvaluesToUnit, WriteEigenvaluesToFile
     generic   :: Fetch            =>  FetchEigenValues, FetchEigenVectors, FetchSingleEigenvector
     generic   :: Transform        =>  TransformEigenvectors, TransformEigenvectorsWithMetric
     procedure :: SyncFirstSign
     procedure :: PurgeNull => SpectralResolutionPurgeNull
     procedure :: ConvertSpectralResToClassMatrix
     !
     procedure, private :: FetchEigenValues
     procedure, private :: FetchEigenVectors
     procedure, private :: FetchSingleEigenvector
     procedure, private :: InitSpectralResolutionFull
     procedure, private :: InitSpectralResolutionReduced
     procedure, private :: WriteSpectralResolutionToUnit
     procedure, private :: WriteSpectralResolutionToFile
     procedure, private :: ReadSpectralResolutionFromUnit
     procedure, private :: ReadSpectralResolutionFromFile
     procedure, private :: WriteEigenvaluesToUnit
     procedure, private :: WriteEigenvaluesToFile
     procedure, private :: TransformEigenvectors
     procedure, private :: TransformEigenvectorsWithMetric
     !
  end type ClassSpectralResolution


  interface AllocateMatrix
     module procedure AllocateDoubleMatrix, AllocateComplexMatrix
  end interface AllocateMatrix


contains

  logical function ClassMatrixIsFull(Matrix) result(IsFull)
    Class(ClassMatrix), intent(in) :: Matrix
    IsFull = ( Matrix%Pattern == MATRIX_PATTERN_FULL )
  end function ClassMatrixIsFull
  !
  logical function ClassMatrixIsBanded(Matrix) result(IsBanded)
    Class(ClassMatrix), intent(in) :: Matrix
    IsBanded = ( Matrix%Pattern == MATRIX_PATTERN_BANDED )
  end function ClassMatrixIsBanded
  !
  logical function ClassMatrixIsDiagonal(Matrix) result(IsDiagonal)
    Class(ClassMatrix), intent(in) :: Matrix
    IsDiagonal = ( Matrix%Pattern == MATRIX_PATTERN_DIAGONAL )
  end function ClassMatrixIsDiagonal

  integer function ClassMatrixLowerBandwidth(Matrix) result(LowerBandwidth)
    Class(ClassMatrix), intent(in) :: Matrix
    LowerBandwidth = Matrix%NL
  end function ClassMatrixLowerBandwidth

  integer function ClassMatrixUpperBandwidth(Matrix) result(UpperBandwidth)
    Class(ClassMatrix), intent(in) :: Matrix
    UpperBandwidth = Matrix%NU
  end function ClassMatrixUpperBandwidth


  subroutine WriteClassMatrixToUnit(Matrix,OutputUnit)
    Class(ClassMatrix), intent(in) :: Matrix
    integer, optional , intent(in) :: OutputUnit
    !
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=16) :: Writable
    character(len=16) :: Form
    integer           :: iostat
    logical           :: Opened
    integer           :: OutUnit 
    if(present(OutputUnit))then
       OutUnit=OutputUnit
    else
       OutUnit = DEFAULT_OUTPUT_UNIT
    endif
    INQUIRE(&
         UNIT  = OutUnit , &
         OPENED= Opened  , &
         WRITE = Writable, &
         FORM  = Form    , &
         IOSTAT= iostat  , &
         IOMSG = iomsg     )
    if(iostat/=0) call Assert(iomsg)
    if( .not. Opened            ) call Assert("Output Unit is closed")
    if( trim(Writable) /= "YES" ) call Assert("Output Unit can't be written")
    !
    select case (trim(FORM))
    case("FORMATTED")
       call WriteClassMatrixToFormattedUnit(Matrix,OutUnit)
    case("UNFORMATTED")
       call WriteClassMatrixToUnformattedUnit(Matrix,OutUnit)
    case DEFAULT
       call Assert("Invalid Output Unit Format")
    end select
    !
  end subroutine WriteClassMatrixToUnit
  !
  subroutine WriteClassMatrixToFormattedUnit(Matrix,OutUnit)
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: OutUnit
    integer :: iostat, i, j
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=*), parameter :: FORMAT_INTS="(*(x,i))"
    character(len=*), parameter :: FORMAT_MAT="(*"//DOUBLE_PRINT_FORMAT//")"
    !
    !.. Write the Matrix attributes and dimensions
    write(OutUnit      , &
         FORMAT_INTS   , &
         IOSTAT=iostat , &
         IOMSG=iomsg   ) &
         Matrix%Pattern, &
         Matrix%NR     , &
         Matrix%NC     , &
         Matrix%NU     , &
         Matrix%NL     , &
         Matrix%NRmin  , &
         Matrix%NRmax  , &
         Matrix%NCmin  , &
         Matrix%NCmax 
    if(iostat/=0)call Assert(iomsg)
    !
    !.. Write Matrix
    do j=Matrix%NCmin,Matrix%NCmax
       write(OutUnit      , &
            FORMAT_MAT    , &
            IOSTAT=iostat , &
            IOMSG =iomsg  ) &
            (Matrix%A(i,j),&
            i=Matrix%NRmin,Matrix%NRmax)
       if(iostat/=0)call Assert(iomsg)
    enddo
    !
  end subroutine WriteClassMatrixToFormattedUnit
  !
  subroutine WriteClassMatrixToUnformattedUnit(Matrix,OutUnit)
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: OutUnit
    integer :: iostat, i, j
    character(len=IOMSG_LENGTH) :: iomsg
    !
    !.. Write the Matrix attributes and dimensions
    write(OutUnit      , &
         IOSTAT=iostat , &
         IOMSG=iomsg   ) &
         Matrix%Pattern, &
         Matrix%NR     , &
         Matrix%NC     , &
         Matrix%NU     , &
         Matrix%NL     , &
         Matrix%NRmin  , &
         Matrix%NRmax  , &
         Matrix%NCmin  , &
         Matrix%NCmax 
    if(iostat/=0)call Assert(iomsg)
    !
    !.. Write Matrix
    write(OutUnit      , &
         IOSTAT=iostat , &
         IOMSG =iomsg  ) &
         ((Matrix%A(i,j),&
         i=Matrix%NRmin,Matrix%NRmax),&
         j=Matrix%NCmin,Matrix%NCmax)
    if(iostat/=0)call Assert(iomsg)
    !
  end subroutine WriteClassMatrixToUnformattedUnit


  subroutine ReadClassMatrixFromUnit(Matrix,InputUnit)
    Class(ClassMatrix), intent(inout):: Matrix
    integer, optional , intent(in)   :: InputUnit
    !
    integer :: InUnit
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=FILE_NAME_LENGTH)     :: FileName
    character(len=16) :: Readable
    character(len=16) :: Form
    integer           :: iostat
    logical           :: Opened
    if(present(InputUnit))then
       InUnit = InputUnit
    else
       InUnit= DEFAULT_INPUT_UNIT
    end if
    INQUIRE(&
         UNIT  = InUnit  , &
         NAME  = FileName, &
         OPENED= Opened  , &
         READ  = Readable, &
         FORM  = Form    , &
         IOSTAT= iostat  , &
         IOMSG = iomsg     )
    if(iostat/=0) call Assert(iomsg)
    if( .not. Opened            ) call Assert("Input Unit is closed")
    if( trim(Readable) /= "YES" ) call Assert("Input Unit can't be read")
    !
    select case (trim(FORM))
    case("FORMATTED")
       call ReadClassMatrixFromFormattedUnit(Matrix,InUnit)
    case("UNFORMATTED")
       call ReadClassMatrixFromUnformattedUnit(Matrix,InUnit)
    case DEFAULT
       call Assert("Invalid Input Unit Format")
    end select
    !
  end subroutine ReadClassMatrixFromUnit
  !
  subroutine ReadClassMatrixFromFormattedUnit(Matrix,InUnit)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: InUnit
    integer :: iostat, i, j
    character(len=IOMSG_LENGTH) :: iomsg=" "
    !
    !.. Read the Matrix attributes and dimensions
    read(InUnit,FMT=*    , &
         IOSTAT=iostat   , &
         IOMSG=iomsg     ) &
         Matrix%Pattern  , &
         Matrix%NR       , &
         Matrix%NC       , &
         Matrix%NU       , &
         Matrix%NL       , &
         Matrix%NRmin    , &
         Matrix%NRmax    , &
         Matrix%NCmin    , &
         Matrix%NCmax 
    if(iostat/=0)call Assert(iomsg)
    !
    call AllocateMatrix( Matrix%A  , &
         Matrix%NRmin, Matrix%NRmax, &
         Matrix%NCmin, Matrix%NCmax  )
    !
    !.. Read Matrix
    do j=Matrix%NCmin,Matrix%NCmax
       read(InUnit,FMT=*  , &
            IOSTAT=iostat , &
            IOMSG =iomsg  ) &
            (Matrix%A(i,j), &
            i=Matrix%NRmin,Matrix%NRmax)
       if(iostat/=0)call Assert(iomsg)
    enddo
    !
  end subroutine ReadClassMatrixFromFormattedUnit
  !
  subroutine ReadClassMatrixFromUnformattedUnit(Matrix,InUnit)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: InUnit
    integer :: iostat, i, j
    character(len=IOMSG_LENGTH) :: iomsg
    !
    !.. Write the Matrix attributes and dimensions
    read(InUnit        , &
         IOSTAT=iostat , &
         IOMSG=iomsg   ) &
         Matrix%Pattern, &
         Matrix%NR     , &
         Matrix%NC     , &
         Matrix%NU     , &
         Matrix%NL     , &
         Matrix%NRmin  , &
         Matrix%NRmax  , &
         Matrix%NCmin  , &
         Matrix%NCmax 
    if(iostat/=0)call Assert(iomsg)
    !
    call AllocateMatrix( Matrix%A  , &
         Matrix%NRmin, Matrix%NRmax, &
         Matrix%NCmin, Matrix%NCmax  )
    !
    read(InUnit        , &
         IOSTAT=iostat , &
         IOMSG =iomsg  ) &
         ((Matrix%A(i,j),&
         i=Matrix%NRmin,Matrix%NRmax),&
         j=Matrix%NCmin,Matrix%NCmax)
    if(iostat/=0)call Assert(iomsg)
    !
  end subroutine ReadClassMatrixFromUnformattedUnit


  subroutine AddDoubleToClassMatrix(Matrix,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    DoublePrecision   , intent(in)    :: x
    if(.not.allocated(Matrix%A))then
       call Assert("Matrix not initialized")
    else
       Matrix%A=x
    endif
  end subroutine AddDoubleToClassMatrix
  !
  subroutine AssignIntegerToClassMatrix(Matrix,i)
    Class(ClassMatrix), intent(inout) :: Matrix
    Integer           , intent(in)    :: i
    call AssignDoubleToClassMatrix(Matrix,dble(i))
  end subroutine AssignIntegerToClassMatrix

  subroutine Double_x_Matrix(Matrix,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    DoublePrecision   , intent(in)    :: x
    if(.not.allocated(Matrix%A))then
       call Assert("Matrix not initialized")
    else
       Matrix%A=Matrix%A*x
    endif
  end subroutine Double_x_Matrix

  subroutine AssignDoubleToClassMatrix(Matrix,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    DoublePrecision   , intent(in)    :: x
    if(.not.allocated(Matrix%A))then
       call Assert("Matrix not initialized")
    else
       Matrix%A=x
    endif
  end subroutine AssignDoubleToClassMatrix


  subroutine CopyClassMatrixToClassMatrix(MatrixOut,MatrixInp)
    Class(ClassMatrix), intent(inout) :: MatrixOut
    type (ClassMatrix), intent(in)    :: MatrixInp
    integer :: LBR, UBR, LBC, UBC
    !
    call MatrixOut%Free()
    !
    MatrixOut%Pattern = MatrixInp%Pattern
    !
    MatrixOut%NR = MatrixInp%NR
    MatrixOut%NC = MatrixInp%NC
    MatrixOut%NL = MatrixInp%NL
    MatrixOut%NU = MatrixInp%NU
    !
    MatrixOut%NRMin = MatrixInp%NRMin
    MatrixOut%NRMax = MatrixInp%NRMax
    MatrixOut%NCMin = MatrixInp%NCMin
    MatrixOut%NCMax = MatrixInp%NCMax
    !
    LBR=LBOUND(MatrixInp%A,1)
    UBR=UBOUND(MatrixInp%A,1)
    LBC=LBOUND(MatrixInp%A,2)
    UBC=UBOUND(MatrixInp%A,2)
    allocate(MatrixOut%A(LBR:UBR,LBC:UBC))
    MatrixOut%A=MatrixInp%A
    !
  end subroutine CopyClassMatrixToClassMatrix


  !> Create and allocate a ClassMatrix object with Full
  !> pattern, if it is not defined, and free it before, 
  !> if it is.
  subroutine ClassMatrixInitFull( Matrix, NR, NC )
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: NR
    integer           , intent(in)    :: NC
    !
    call Matrix%Free()
    call SetMatrixNominalSize( Matrix, NR, NC )
    call SetMatrixPhysicalSize( Matrix, 1, NR, 1, NC )
    call AllocateMatrix( Matrix%A,   &
         Matrix%NRmin, Matrix%NRmax, &
         Matrix%NCmin, Matrix%NCmax  )
    Matrix%Pattern = MATRIX_PATTERN_FULL
    Matrix%A = 0.d0
    !
  end subroutine ClassMatrixInitFull


  !> Create and allocate a ClassMatrix object with Banded
  !> pattern, if it is not defined, and free it before, 
  !> if it is not.
  subroutine ClassMatrixInitBanded( Matrix, NR, NC, NL, NU )
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: NR
    integer           , intent(in)    :: NC
    integer           , intent(in)    :: NL
    integer           , intent(in)    :: NU
    !
    call Matrix%Free()
    call SetMatrixNominalSize( Matrix, NR, NC )
    Matrix%NL=max(0,min(NL,NR-1))
    Matrix%NU=max(0,min(NU,NC-1))
    call SetMatrixPhysicalSize( Matrix, -Matrix%NU, Matrix%NL, 1, Matrix%NC )
    call AllocateMatrix( Matrix%A,   &
         Matrix%NRmin, Matrix%NRmax, &
         Matrix%NCmin, Matrix%NCmax  )
    Matrix%Pattern = MATRIX_PATTERN_BANDED
    Matrix%A = 0.d0
    !
  end subroutine ClassMatrixInitBanded


  subroutine SetMatrixPhysicalSize( Matrix, NRMin, NRMax, NCMin, NCMax )
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: NRMin, NRMax, NCMin, NCMax
    Matrix%NRmin = NRMin
    Matrix%NRmax = NRMax
    Matrix%NCmin = NCMin
    Matrix%NCmax = NCMax
  end subroutine SetMatrixPhysicalSize


  DoublePrecision function ClassMatrixElement(Matrix,i,j) result(Element) 
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: i,j
    Element=0.d0
    select case( Matrix%Pattern )
    case( MATRIX_PATTERN_FULL )
       Element = ClassMatrixFullElement(Matrix,i,j)
    case( MATRIX_PATTERN_BANDED )
       Element = ClassMatrixBandedElement(Matrix,i,j)
!!$    case( MATRIX_PATTERN_DIAGONAL )
!!$       Element = ClassMatrixDiagonalElement(Matrix,i,j)
    case DEFAULT
    end select
  end function ClassMatrixElement

  subroutine ClassMatrixSetElement(Matrix,i,j,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: i,j
    DoublePrecision   , intent(in)    :: x
    select case( Matrix%Pattern )
    case( MATRIX_PATTERN_FULL )
       call ClassMatrixFullSetElement(Matrix,i,j,x)
    case( MATRIX_PATTERN_BANDED )
       call ClassMatrixBandedSetElement(Matrix,i,j,x)
    case DEFAULT
    end select
  end subroutine ClassMatrixSetElement

  subroutine ClassMatrixFullSetElement(Matrix,i,j,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: i,j
    DoublePrecision   , intent(in)    :: x
    call CheckIndexBounds(Matrix,i,j)
    Matrix%A(i,j)=x
  end subroutine ClassMatrixFullSetElement

  subroutine ClassMatrixBandedSetElement(Matrix,i,j,x)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: i,j
    DoublePrecision   , intent(in)    :: x
    integer :: iPhys
    call CheckIndexBounds(Matrix,i,j)
    iPhys=i-j
    if( iPhys < -Matrix%NU .or. iPhys > Matrix%NL )&
         call Assert("Invalid banded matrix index")
    Matrix%A(iPhys,j)=x
  end subroutine ClassMatrixBandedSetElement


  subroutine CheckIndexBounds(Matrix,i,j)
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: i,j
    if( i < 1 .or. i > Matrix%NR )call Assert("Row index is off bound")
    if( j < 1 .or. j > Matrix%NC )call Assert("Column index is off bound")
  end subroutine CheckIndexBounds


  DoublePrecision function ClassMatrixFullElement(Matrix,i,j) result(Element)
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: i,j
    integer  :: iPhys,jPhys
    call CheckIndexBounds(Matrix,i,j)
    iPhys=Matrix%NRMin-1+i
    jPhys=Matrix%NCMin-1+j
    Element=Matrix%A(iPhys,jPhys)
  end function ClassMatrixFullElement


  DoublePrecision function ClassMatrixBandedElement(Matrix,i,j) result(Element)
    Class(ClassMatrix), intent(in) :: Matrix
    integer           , intent(in) :: i,j
    integer :: iPhys
    call CheckIndexBounds(Matrix,i,j)
    iPhys=i-j
    if( iPhys >= -Matrix%NU .and. iPhys <= Matrix%NL )then
       Element=Matrix%A(iPhys,j)
    else
       Element=0.d0
    end if
  end function ClassMatrixBandedElement


  subroutine SetMatrixNominalSize(Matrix,NR,NC)
    Class(ClassMatrix), intent(inout) :: Matrix
    integer           , intent(in)    :: NR,NC
    if(NR<=0)call Assert("Invalid Number of Rows")
    if(NC<=0)call Assert("Invalid Number of Columns")
    Matrix%NR=NR
    Matrix%NC=NC
  end subroutine SetMatrixNominalSize


  subroutine AllocateDoubleMatrix(A,NRMin,NRMax,NCMin,NCMax)
    DoublePrecision, allocatable, intent(inout) :: A(:,:)
    integer                     , intent(in)    :: NRMin, NRmax
    integer                     , intent(in)    :: NCMin, NCmax
    integer :: Stat=0
    character(len=IOMSG_LENGTH) :: iomsg=" "
    if( NRMax < NRmin .or. NCMax < NCMin )return
    if(allocated(A))deallocate(A)
    allocate(A(NRMin:NRMax,NCMin:NCMax),STAT=Stat,ERRMSG=iomsg)
    if(Stat/=0)call Assert(iomsg)
    A=0.d0
  end subroutine AllocateDoubleMatrix

  subroutine AllocateComplexMatrix(A,NRMin,NRMax,NCMin,NCMax)
    Complex(kind(1d0)), allocatable, intent(inout) :: A(:,:)
    integer                     , intent(in)    :: NRMin, NRmax
    integer                     , intent(in)    :: NCMin, NCmax
    integer :: Stat=0
    character(len=IOMSG_LENGTH) :: iomsg=" "
    if( NRMax < NRmin .or. NCMax < NCMin )return
    if(allocated(A))deallocate(A)
    allocate(A(NRMin:NRMax,NCMin:NCMax),STAT=Stat,ERRMSG=iomsg)
    if(Stat/=0)call Assert(iomsg)
    A=0.d0
  end subroutine AllocateComplexMatrix


  !> Free allocatable space and sets to zero
  !> all the members of a ClassMatrix object
  subroutine ClassMatrixFree( Matrix )
    class(ClassMatrix), intent(inout) :: Matrix
    if(allocated(Matrix%A))deallocate(Matrix%A)
    Matrix%NR=0
    Matrix%NC=0
    Matrix%Pattern=MATRIX_PATTERN_UNDEFINED
    Matrix%NU=0
    Matrix%NL=0
  end subroutine ClassMatrixFree


  !> Free allocatable space and sets to zero
  !> all the members of a ClassMatrix object
  subroutine ClassMatrixFinalize( Matrix )
    type(ClassMatrix), intent(inout) :: Matrix
    call Matrix%Free()
  end subroutine ClassMatrixFinalize


  integer function ClassMatrixNRows( Matrix ) result( NRows )
    class(ClassMatrix), intent(in) :: Matrix
    NRows = Matrix%NR
  end function ClassMatrixNRows
  !
  integer function ClassMatrixNColumns( Matrix ) result( NColumns )
    class(ClassMatrix), intent(in) :: Matrix
    NColumns = Matrix%NC
  end function ClassMatrixNColumns



  !.. Assumes Metric is definite positive
  subroutine GeneralEigenValueSolver( Matrix, Metric, SpecRes )
    class(ClassMatrix),            intent(in)  :: Matrix
    type(ClassMatrix),             intent(in)  :: Metric
    type(ClassSpectralResolution), intent(out) :: SpecRes
    !
    select case( Matrix%Pattern )
    case( MATRIX_PATTERN_FULL )
       call FullGeneralEigenvalueSolver( Matrix, Metric, SpecRes )
    case( MATRIX_PATTERN_BANDED )
       call BandGeneralEigenvalueSolver( Matrix, Metric, SpecRes )
    case DEFAULT 
       call Assert('Error: unrecognized matrix pattern')
    end select
    !
  end subroutine GeneralEigenValueSolver
  
  subroutine FullGeneralEigenvalueSolver ( Matrix, Metric, SpecRes )
    class(ClassMatrix),            intent(in)  :: Matrix
    type(ClassMatrix),             intent(in)  :: Metric
    type(ClassSpectralResolution), intent(out) :: SpecRes 
    !
    integer :: n, lda, ldb, LWORK, INFO
    DoublePrecision, allocatable :: WORK(:)
    !
    n = Matrix%NRows()
    call SpecRes%Init( n )
    !
    SpecRes%EigenVectors = Matrix%A
    !
    lda = n
    ldb = n
    LWORK = 3*n-1
    allocate( WORK( LWORK ) )
    call DSYGV( 1, 'V', 'U', n, &
         SpecRes%EigenVectors, lda, &
         Metric%A, ldb, &
         SpecRes%EigenValues, &
         WORK, LWORK, INFO )
    deallocate( WORK )
    !
  end subroutine FullGeneralEigenvalueSolver
  
  subroutine BandGeneralEigenvalueSolver ( Matrix, Metric, SpecRes )
    class(ClassMatrix),            intent(in)  :: Matrix
    type(ClassMatrix),             intent(in)  :: Metric
    type(ClassSpectralResolution), intent(out) :: SpecRes
    !
    integer :: n, ka, kb, ldab, ldbb, ldz, INFO, liwork, lwork
    DoublePrecision, allocatable :: work(:)
    DoublePrecision, allocatable :: ABand(:,:), BBand(:,:)
    !
    n  = size(Matrix%A,2)
    call SpecRes%Init(n)
    !
    ka = Matrix%NU
    kb = Matrix%NU
    lwork = 2*n**2 + 5*n + 1
    liwork = 5*n+3
    ldab = size( Matrix%A, 1 )
    ldbb = ldab
    ldz  = n
    !
    allocate( WORK( 3*n ) )
    allocate( ABand, source = Matrix%A )
    allocate( BBand, source = Metric%A )
    call DSBGV( 'V', 'U', n, ka, kb, &
         ABand, ldab, &
         BBand, ldbb, &
         SpecRes%EigenValues, SpecRes%EigenVectors, ldz, &
         WORK, INFO )  
    !
    !*** We could save somewhere the Cholesky factor 
    !    BBand, instead of throwing it away.
    deallocate(work,ABand,BBand)
    !
    !
  end subroutine BandGeneralEigenvalueSolver


  subroutine EigenvalueSolver( Matrix, SpecRes )
    class(ClassMatrix),            intent(in)  :: Matrix
    type(ClassSpectralResolution), intent(out) :: SpecRes 
    !
    select case( Matrix%Pattern )
    case( MATRIX_PATTERN_FULL )
       call FullEigenvalueSolver( Matrix, SpecRes )
    case( MATRIX_PATTERN_BANDED )
       call BandEigenvalueSolver( Matrix, SpecRes )
    case DEFAULT 
       call Assert('Error: non-proper matrix pattern')
    end select
    !
  end subroutine EigenvalueSolver
  
  subroutine FullEigenvalueSolver( Matrix, SpecRes ) 
    class(ClassMatrix),            intent(in)    :: Matrix
    type(ClassSpectralResolution), intent(out) :: SpecRes
    !
    integer :: n, lda, lwork, liwork, INFO
    DoublePrecision, allocatable :: work(:)
    integer        , allocatable :: iwork(:)
    !
    n = Matrix%NRows()
    call SpecRes%Init( n )
    SpecRes%EigenVectors = Matrix%A
    lda = n
    lwork  = 2*n**2+6*n+1
    liwork = 5*n+3
    allocate( work(lwork), iwork(liwork) )
    !
    call DSYEVD( 'V', 'U', n, &
         SpecRes%EigenVectors, lda, &
         SpecRes%EigenValues, &
         work, lwork, iwork, liwork, INFO )
    !
    deallocate(work,iwork)
    !
  end subroutine FullEigenvalueSolver
  
  subroutine BandEigenvalueSolver( Matrix, SpecRes )
    class(ClassMatrix),            intent(in)    :: Matrix
    type(ClassSpectralResolution), intent(out) :: SpecRes
    !
    integer :: n, lda, kd, ldz, INFO
    DoublePrecision, allocatable :: work(:)
    !
    n = Matrix%NRows()
    call SpecRes%Init( n )
    SpecRes%EigenVectors = Matrix%A
    kd = Matrix%NU
    lda = size(Matrix%A,1)
    ldz = n
    !
    allocate( work( 3*n - 2 ) )
    !
    call DSBEV( 'V', 'U', n, kd,&
         Matrix%A, lda,&
         SpecRes%EigenValues,&
         SpecRes%EigenVectors, ldz,&
         work, INFO )
    !
    deallocate( work )
    !
  end subroutine BandEigenvalueSolver


  subroutine InitSpectralResolutionFull( SpecRes, n )
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    integer,                        intent(in)    :: n
    !
    SpecRes%NEigenvalues = n
    SpecRes%Dim = n
    !
    if(allocated(SpecRes%EigenValues))then
       if(size(SpecRes%Eigenvalues,1)/=n)then
          deallocate( SpecRes%EigenValues )
          allocate( SpecRes%EigenValues( n ))
       endif
    else
       allocate( SpecRes%EigenValues( n ))
    endif
    SpecRes%EigenValues = 0.d0
    !
    if(allocated(SpecRes%EigenVectors))then
       if(  size(SpecRes%Eigenvectors,1) /= n  .or. &
            size(SpecRes%Eigenvectors,2) /= n  )then
          deallocate(SpecRes%EigenVectors)
          allocate(SpecRes%EigenVectors( n, n ) )
       endif
    else
       allocate(SpecRes%EigenVectors( n, n ) )
    endif
    SpecRes%EigenVectors = 0.d0
    !
  end subroutine InitSpectralResolutionFull

  subroutine InitSpectralResolutionReduced( SpecRes, Dim, NEigenvalues )
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    integer,                        intent(in)    :: Dim, NEigenvalues
    !
    SpecRes%Dim = Dim
    SpecRes%NEigenvalues = NEigenvalues
    !
    if(allocated(SpecRes%EigenValues))then
       if(size(SpecRes%Eigenvalues,1)/=NEigenvalues)then
          deallocate( SpecRes%EigenValues )
          allocate( SpecRes%EigenValues( NEigenvalues ))
       endif
    else
       allocate( SpecRes%EigenValues( NEigenvalues ))
    endif
    SpecRes%EigenValues = 0.d0
    !
    if(allocated(SpecRes%EigenVectors))then
       if(  size(SpecRes%Eigenvectors,1) /= Dim  .or. &
            size(SpecRes%Eigenvectors,2) /= NEigenvalues  )then
          deallocate(SpecRes%EigenVectors)
          allocate(SpecRes%EigenVectors( Dim, NEigenvalues ) )
       endif
    else
       allocate(SpecRes%EigenVectors( Dim, NEigenvalues ) )
    endif
    SpecRes%EigenVectors = 0.d0
    !
  end subroutine InitSpectralResolutionReduced

  integer function NevalSpectralResolution( SpecRes ) &
       result( Neval )
    class(ClassSpectralResolution), intent(in) :: SpecRes
    Neval = SpecRes%NEigenvalues
  end function NevalSpectralResolution

  !> Set to + the sign of the first entry in each eigenvector
  subroutine SyncFirstSign( SpecRes ) 
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    integer :: iEval
    if(.not.allocated(SpecRes%Eigenvectors))return
    do iEval = 1, SpecRes%Neval()
       if(SpecRes%Eigenvectors(1,iEval)<0.d0)then
          SpecRes%Eigenvectors(:,iEval)=-SpecRes%Eigenvectors(:,iEval)
       endif
    enddo
  end subroutine SyncFirstSign

  !> Eliminates the null eigenspace
  subroutine SpectralResolutionPurgeNull( SpecRes, Threshold )
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    DoublePrecision, optional     , intent(in)    :: Threshold
    DoublePrecision, parameter :: DEFAULT_THRESHOLD = 1.d-16
    integer, allocatable :: ivec(:)
    integer :: iEval
    integer :: NValidEval
    DoublePrecision :: ActualThreshold
    ActualThreshold=DEFAULT_THRESHOLD
    if(present(Threshold))ActualThreshold=Threshold
    allocate(ivec(SpecRes%NEigenvalues))
    ivec=0
    NValidEval=0
    do iEval = 1, SpecRes%Neval()
       if( abs(SpecRes%EigenValues(iEval))<=Threshold )cycle
       NValidEval=NValidEval+1
       ivec(NValidEval)=iEval
    enddo
    SpecRes%NEigenvalues=NValidEval
    do iEval = 1, NValidEval
       SpecRes%EigenValues(   iEval) = SpecRes%EigenValues(   ivec(iEval))
       SpecRes%EigenVectors(:,iEval) = SpecRes%EigenVectors(:,ivec(iEval))
    enddo
    deallocate(ivec)
  end subroutine SpectralResolutionPurgeNull

  logical function SpectralResolutionIsConsistent( SpecRes, FileName )&
       result( IsConsistent )
    !
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    character(len=*)              , intent(in)    :: FileName
    !
    integer :: uid, iostat, Dim, Neval
    !
    IsConsistent = .FALSE.
    OPEN(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "old"        , &
         Action  = "read"       , &
         iostat  = iostat       )
    if( iostat /= 0 )return
    !
    read(uid,iostat=iostat) Dim, NEval
    if(iostat==0)then
       IsConsistent = ( Dim == SpecRes%Dim .and. NEval <= Dim )
    endif
    close(uid)
    !
  end function SpectralResolutionIsConsistent


  !> Save the eigenvalues and eigenvectors
  !!
  subroutine WriteSpectralResolutionToUnit( SpecRes, uid )
    !
    class(ClassSpectralResolution), intent(in) :: SpecRes
    integer,                        intent(in) :: uid
    integer :: iostat1, iostat2, iostat3
    integer :: i, j
    !
    write( unit=uid, iostat=iostat1 )     SpecRes%Dim, SpecRes%NEigenvalues
    write( unit=uid, iostat=iostat2 ) (   SpecRes%EigenValues (j)  , j=1, SpecRes%NEigenvalues )
    write( unit=uid, iostat=iostat3 ) ( ( SpecRes%EigenVectors(i,j), i=1, SpecRes%Dim ), j=1, SpecRes%NEigenvalues )
    !
    if ( iostat1 /=0 ) call ErrorMessage( 'Error trying to write the new upper index and the number of regular functions' )
    if ( iostat2 /=0 ) call ErrorMessage( 'Error trying to write the hamiltonian eigenvalues on file'  )
    if ( iostat3 /=0 ) call ErrorMessage( 'Error trying to write the hamiltonian eigenvectors on file' )
    !
  end subroutine WriteSpectralResolutionToUnit
  
  subroutine WriteSpectralResolutionToFile( SpecRes, FileName )
    class(ClassSpectralResolution), intent(in) :: SpecRes
    character(len=*)              , intent(in) :: FileName
    !
    integer :: uid, iostat
    character(len=IOMSG_LENGTH) :: iomsg
    !
    open(newunit =  uid         , &
         file    =  FileName    , &
         form    = "unformatted", &
         status  = "unknown"    , &
         action  = "write"      , &
         iostat  =  iostat      , &
         iomsg   =  iomsg       )
    if(iostat/=0)call Assert(iomsg)
    !
    call SpecRes%Write( uid )
    !
    close( uid )
    !
  end subroutine WriteSpectralResolutionToFile


  subroutine ReadSpectralResolutionFromFile( SpecRes, FileName )
    class(ClassSpectralResolution), intent(out) :: SpecRes
    character(len=*)              , intent(in)  :: FileName
    !
    integer :: uid, iostat
    character(len=IOMSG_LENGTH) :: iomsg
    !
    open(Newunit =  uid         , &
         File    =  FileName    , &
         Status  = "old"        , &
         Action  = "read"       , &
         Form    = "unformatted", &
         iostat  =  iostat      , &
         iomsg   =  iomsg       )
    if ( iostat /= 0 ) then
       call ErrorMessage(iomsg)
       return
    endif
    !
    call SpecRes%Read( uid )
    !
    close( uid )
    !
  end subroutine ReadSpectralResolutionFromFile

  subroutine ReadSpectralResolutionFromUnit( SpecRes, uid )
    class(ClassSpectralResolution), intent(out) :: SpecRes
    integer                       , intent(in)  :: uid
    !
    integer :: i, j, iostat, Dim, Neval
    character(len=IOMSG_LENGTH) :: iomsg
    !
    read( uid, iostat=iostat, iomsg=iomsg ) Dim, Neval
    if (iostat/=0) call ErrorMessage(iomsg )
    call SpecRes%Init( Dim, Neval )
    read( uid, iostat=iostat, iomsg=iomsg )  ( SpecRes%Eigenvalues(j), j=1, Neval )             
    if (iostat/=0) call ErrorMessage(iomsg )
    read( uid, iostat=iostat, iomsg=iomsg ) (( SpecRes%Eigenvectors(i,j), i=1, Dim ), j=1, Neval )
    if (iostat/=0) call ErrorMessage(iomsg )
    !
  end subroutine ReadSpectralResolutionFromUnit


  subroutine WriteEigenvaluesToUnit( SpecRes, uid, NColumns, NumberFormat )!@
    class(ClassSpectralResolution), intent(in) :: SpecRes
    integer                       , intent(in) :: uid
    integer, optional             , intent(in) :: NColumns
    character(len=*), optional    , intent(in) :: NumberFormat
    !
    character(len=*), parameter :: DEFAULT_FORMAT = "f24.17"
    integer, parameter :: DEFAULT_NCOLUMNS = 5
    integer :: NCol, iEval
    integer :: iostat
    character(len=IOMSG_LENGTH) :: iomsg
    character(len=32) :: form
    character(len=:), allocatable :: ActualFormat
    !
    if(present(NumberFormat))then
       allocate(ActualFormat,source=trim(NumberFormat))
    else
       allocate(ActualFormat,source=DEFAULT_FORMAT)
    endif
    NCol=DEFAULT_NCOLUMNS
    if(present(NColumns))then
       if(NColumns<1)then
          call ErrorMessage("WriteEigenvalues: Wrong number of columns")
          return
       endif
       NCol=NColumns
    endif
    !
    INQUIRE( uid, form=form, iostat=iostat, iomsg=iomsg )
    if(iostat/=0)then
       call ErrorMessage("WriteEigenvalues: "//trim(iomsg))
       return
    endif
    if(trim(form)/="FORMATTED")then
       call ErrorMessage("WriteEigenvalues: wrong uid format")
       return
    endif
    !
    write(uid,*) SpecRes%Dim,SpecRes%NEigenvalues
    do iEval = 1, SpecRes%NEigenvalues
       !
       if(mod(iEval-1,NCol)==0)then
          if(iEval/=1) write(uid,*)
          write(uid,"(i5)",advance="no") iEval
       endif
       write(uid,"(x,"//ActualFormat//")",advance="no") SpecRes%EigenValues(iEval) 
       !
    enddo
    write(uid,*)
    !
    deallocate(ActualFormat)
  end subroutine WriteEigenvaluesToUnit

  subroutine WriteEigenvaluesToFile( SpecRes, FileName, NColumns, NumberFormat )
    class(ClassSpectralResolution), intent(in) :: SpecRes
    character(len=*)              , intent(in) :: FileName
    integer, optional             , intent(in) :: NColumns
    character(len=*), optional    , intent(in) :: NumberFormat
    !
    integer :: uid, iostat
    character(len=IOMSG_LENGTH) :: iomsg
    !
    open(newunit =  uid        , &
         file    =  FileName   , &
         form    = "formatted" , &
         status  = "unknown"   , &
         action  = "write"     , &
         iostat  =  iostat     , &
         iomsg   =  iomsg      )
    if(iostat/=0)call assert(iomsg)
    if(present(NColumns))then
       if(present(NumberFormat))then
          call SpecRes%WriteEigenvalues( uid, NColumns = NColumns, NumberFormat = NumberFormat )
       else
          call SpecRes%WriteEigenvalues( uid, NColumns = NColumns )
       endif
    else
       if(present(NumberFormat))then
          call SpecRes%WriteEigenvalues( uid, NumberFormat = NumberFormat )
       else
          call SpecRes%WriteEigenvalues( uid )
       endif
    endif
    close( uid, iostat=iostat, iomsg=iomsg )
    if(iostat/=0)call assert(iomsg)
    !
  end subroutine WriteEigenvaluesToFile


  !.. Creates a square submatrix of Matrix defined by a minimum and a
  !   maximum index which are assumed to be equal for both rows and columns
  subroutine GetSubMatrix( Matrix, MinSubIndex, MaxSubIndex, SubMatrix )!@
    !
    class(ClassMatrix), intent(inout) :: Matrix  
    integer,            intent(in)    :: MinSubIndex
    integer,            intent(in)    :: MaxSubIndex
    type(ClassMatrix),  intent(out)   :: SubMatrix
    !
    integer         :: SubDim, NL, NU
    integer         :: iRow, iCol, iSubRow, iSubCol, iSubRowMin, iSubRowMax
    DoublePrecision :: Element
    !
    if( MinSubIndex <= 0 )call Assert("Invalid MinSubIndex")
    if( MaxSubIndex > min( Matrix%NR, Matrix%NC ) ) &
         call Assert("Invalid MaxSubIndex")
    !
    SubDim = MaxSubIndex - MinSubIndex + 1
    NL = Matrix%LowerBandwidth()
    NU = Matrix%UpperBandwidth()
    !
    if( Matrix%IsFull() )then
       call SubMatrix%InitFull( SubDim, SubDim )
    elseif( Matrix%IsBanded() )then
       call SubMatrix%InitBanded( SubDim, SubDim, NL, NU )
    else
       call Assert("Unrecognized Matrix type")
    endif
    !
    do iSubCol = 1, SubDim
       iCol = ( MinSubIndex - 1 ) + iSubCol
       iSubRowMin = max(1,iSubCol-NU)
       iSubRowMax = min(SubDim,iSubCol+NL)
       do iSubRow = iSubRowMin, iSubRowMax 
          iRow = ( MinSubIndex - 1 ) + iSubRow
          Element = Matrix%Element( iRow, iCol )
          call SubMatrix%SetElement( iSubRow, iSubCol, Element )
       enddo
    enddo
    !
  end subroutine GetSubMatrix


  subroutine ClassMatrixTimesDouble( Matrix, Number )
    class(ClassMatrix), intent(inout) :: Matrix 
    DoublePrecision,    intent(in)    :: Number
    Matrix%A = Number * Matrix%A
  end subroutine ClassMatrixTimesDouble


  subroutine AddMatrixToMatrix( Matrix, DeltaMatrix ) 
    class(ClassMatrix), intent(inout) :: Matrix
    class(ClassMatrix), intent(in)    :: DeltaMatrix
    if( DeltaMatrix%HasSameShapeAs(Matrix) )then
       Matrix%A = Matrix%A + DeltaMatrix%A
    else
       call Assert("Incompatible ClassMatrix shapes")
    endif
  end subroutine AddMatrixToMatrix


  logical function HasSameShapeAs( Mat1, Mat2 ) result(Same)
    class(ClassMatrix), intent(in) :: Mat1, Mat2
    Same=.FALSE.
    if( Mat1%Pattern /= Mat2%Pattern ) return
    if(  Mat1%NR /= Mat2%NR .or. &
         Mat1%NC /= Mat2%NC .or. &
         Mat1%NL /= Mat2%NL .or. &
         Mat1%NU /= Mat2%NU )return
    Same=.TRUE.
    return
  end function HasSameShapeAs


  subroutine FetchEigenValues( SpecRes, Vector )
    class(ClassSpectralResolution), intent(in) :: SpecRes
    DoublePrecision, allocatable,   intent(out) :: Vector(:)
    allocate( Vector, source = SpecRes%EigenValues )
  end subroutine FetchEigenValues


  subroutine FetchEigenVectors( SpecRes, Mat )
    class(ClassSpectralResolution), intent(in) :: SpecRes
    DoublePrecision, allocatable,   intent(out) :: Mat(:,:)
    allocate( Mat, source = SpecRes%EigenVectors )
  end subroutine FetchEigenVectors


  subroutine FetchSingleEigenVector( SpecRes, Vec, n )
    class(ClassSpectralResolution), intent(in)  :: SpecRes
    DoublePrecision, allocatable  , intent(out) :: Vec(:)
    integer                       , intent(in)  :: n
    integer :: Dim
    if(n>SpecRes%Neval())call ErrorMessage("Requested eigenvector doesn't exist")
    Dim = size(SpecRes%Eigenvectors,1)
    if(allocated(Vec))then
       if(size(Vec,1)/=Dim)then
          deallocate(Vec)
          allocate(Vec(Dim))
       endif
    else
       allocate(Vec(Dim))
    endif
    Vec = SpecRes%Eigenvectors(:,n)
  end subroutine FetchSingleEigenVector


  !> transform the eigenvectors C of the spectral resolution
  !! on the basis defined by the columns U of NewBasis:
  !! C' = Ut C
  subroutine TransformEigenvectors( SpecRes, NewBasis )
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    class(ClassSpectralResolution), intent(in)    :: NewBasis
    DoublePrecision, allocatable :: Matrix(:,:)
    integer :: NR,NC,NK
    NR=NewBasis%NEigenvalues
    NC=SpecRes%NEigenvalues
    NK=min(SpecRes%Dim,NewBasis%Dim)
    allocate(Matrix(NR,NC))
    Matrix=0.d0
    call DGEMM( "T", "N", NR, NC, NK, 1.d0,&
         NewBasis%Eigenvectors, NewBasis%Dim, &
         SpecRes%Eigenvectors,  SpecRes%Dim, &
         0.d0, Matrix, NR )
    deallocate(SpecRes%Eigenvectors)
    allocate(SpecRes%Eigenvectors,source=Matrix)
    SpecRes%Dim=NR
    deallocate(Matrix)
  end subroutine TransformEigenvectors


  !> transform the eigenvectors C of the spectral resolution
  !! on the basis defined by the columns U of NewBasis, taking
  !! into account the metric S of the basis:
  !! C' = Ut S C
  subroutine TransformEigenvectorsWithMetric( SpecRes, NewBasis, Metric )
    class(ClassSpectralResolution), intent(inout) :: SpecRes
    class(ClassSpectralResolution), intent(in)    :: NewBasis
    class(ClassMatrix)            , intent(in)    :: Metric
    !
    DoublePrecision, allocatable :: Matrix(:,:)
    DoublePrecision, allocatable :: vec(:)
    integer :: iEval
    !
    !.. Pre-multiplies the eigenvectors by the metric, using
    !   the fact that the metric is symmetric
    select case( Metric%Pattern )
    case( MATRIX_PATTERN_FULL )
       allocate(Matrix(SpecRes%Dim,SpecRes%NEigenvalues))
       Matrix=0.d0
       call DSYMM("L","U",SpecRes%Dim,SpecRes%NEigenvalues,1.d0,&
            Metric%A,Metric%NR,&
            SpecRes%Eigenvectors,SpecRes%Dim,&
            0.d0,Matrix,SpecRes%Dim)
       deallocate(SpecRes%Eigenvectors)
       allocate(SpecRes%Eigenvectors,source=Matrix)
       deallocate(Matrix)
    case( MATRIX_PATTERN_BANDED )
       allocate(Vec(SpecRes%Dim))
       Vec=0.d0
       do iEval=1,SpecRes%NEigenvalues
          call DSBMV("U",SpecRes%Dim,Metric%NU,1.d0,&
               Metric%A,Metric%NL+Metric%NU+1,&
               SpecRes%Eigenvectors(1,iEval),1,&
               0.d0,Vec,1)
          SpecRes%Eigenvectors(:,iEval)=Vec
       enddo
       deallocate(Vec)
    case DEFAULT
       call Assert('Error: unrecognized matrix pattern')
    end select
    call SpecRes%Transform( NewBasis )
  end subroutine TransformEigenvectorsWithMetric



  subroutine ConvertSpectralResToClassMatrix( SpecRes, Matrix )
    class(ClassSpectralResolution), intent(in)    :: SpecRes
    type(ClassMatrix),              intent(inout) :: Matrix
    !
    if ( SpecRes%Dim == SpecRes%NEigenvalues ) then
       call Matrix%InitFull( SpecRes%Dim, SpecRes%NEigenvalues )
    else
       call Assert( 'The matrix of the spectral resolution is not squared' )
    end if
    Matrix%A = SpecRes%EigenVectors
  end subroutine ConvertSpectralResToClassMatrix


  logical function IqualDimensionTo( Matrix, Matrix2 )
    class(ClassMatrix), intent(in) :: Matrix
    type(Classmatrix),  intent(in) :: Matrix2
    !
    IqualDimensionTo = .FALSE.
    if( (Matrix%NR == Matrix2%NR) .and. (Matrix%NC == Matrix2%NC) ) then
       IqualDimensionTo = .TRUE.
    end if
  end function IqualDimensionTo


end module ModuleMatrix
