!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> 
!> Defines a number of special functions and symbols of the
!> quantum theory of angular momentum. Unless otherwise
!> stated, this module follow the conventions of 
!>
!> \emph{Quantum Theory of Angular Momentum} by 
!> D. A. Varshalovich, A. N. Moskalev, and V. K. Kersonskii
!> World Scientific, Singapore 
!> \cite{Varshalovich}
!>
module ModuleAngularMomentum

  !implicit none

  private

  logical, private :: NOT_INITIALIZED_YET=.TRUE.

  integer        , private, parameter :: DIMFACT=170
  real(kind(1d0)), private :: fact(0:DIMFACT)
  integer        , private, parameter :: DIMLOGFACT=500
  real(kind(1d0)), private :: logfact(0:DIMLOGFACT)

  private :: initsymba, delta

  public :: ClebschGordanCoefficient
  public :: ThreeJSymbol
  public :: SixJSymbol
  public :: NineJSymbol
  public :: ThreeJCoefficient

  public :: bispharm
  public :: RAssLegFun, RLegPol, LegPolF, LegPolS
  public :: Plm,Ylm,BipolarSphericalHarmonics,SymmetryAdaptedBipolarSphericalHarmonics,WRME
  public :: mtssabsh,smmtssabsh,spsabsh,angcoup2,angcouppm


contains

  subroutine InitThisModule()
    integer :: i
    fact(0)=1.d0
    do i = 1,DIMFACT
       fact(i)=fact(i-1)*dble(i)
    enddo
    logfact(0:DIMFACT)=log(fact(0:DIMFACT))
    do i=DIMFACT+1,DIMLOGFACT
       logfact(i)=logfact(i-1)+log(1.d0*i)
    enddo
    NOT_INITIALIZED_YET=.FALSE.
    return
  end subroutine InitThisModule


  real(kind(1d0)) function ClebschGordanCoefficient(J1,J2,J3,M1,M2) result( cgc )

    IMPLICIT REAL*8(A-H,O-Z)

    if(Not_Initialized_Yet)call InitThisModule

    cgc = 0.d0

    IF(J1<ABS(M1).or.J2<ABS(M2).or.J3<ABS(M1+M2)) return
    IF(J3>(J1+J2).or.J3<ABS(J1-J2))return
    IF((ABS(M1)+ABS(M2))==0.and.BTEST(J1+J2+J3,0)) return 

    IA1=J3+J2-J1
    M3=M1+M2
    IA2=J3+M3
    IA3=J2+M3-J1
    IF(IA3.GT.0) THEN
       NI=IA3
    ELSE
       NI=0
    END IF
    IF(IA2.LE.IA1) THEN
       NM=IA2
    ELSE
       NM=IA1
    END IF
    L1=J3+J1-J2
    L2=J1+J2+J3+1
    L3=J1+J2-J3
    L4=J1-M1
    L5=J2-M2
    L6=J2+M2
    L7=J3-M3
    L8=J1+M1
    L9=2*J3+1
    CC=L9*fact(L1)/fact(L2)*fact(IA1)*fact(L3)/fact(L4)/fact(L5)*&
         fact(IA2)/fact(L6)*fact(L7)/fact(L8)
    CC=DSQRT(CC)
    IP1=J2+J3+M1-NI
    B1=fact(IP1)
    IP2=J1-M1+NI
    B2=fact(IP2)
    IP2=IP2+1
    D1=fact(NI)
    IR1=NI+1
    IR2=J3-J1+J2-NI
    D2=fact(IR2)
    IR3=J3+M3-NI
    D3=fact(IR3)
    IR4=J1-J2-M3+NI
    D4=fact(IR4)
    IR4=IR4+1
    FAC=1.d0
    IF(BTEST(NI+J2+M2,0)) FAC=-FAC
    S1=B1/D2*B2/(D1*D3*D4)*FAC
    N=NM-NI

    IF(N/=0)THEN
       FA=S1
       DO I=1,N
          FA=-FA*IP2*IR2/IP1*IR3/(IR1*IR4)
          S1=S1+FA
          IP1=IP1-1
          IP2=IP2+1
          IR1=IR1+1
          IR2=IR2-1
          IR3=IR3-1
          IR4=IR4+1
       ENDDO
    ENDIF

    CGC=CC*S1
    if(abs(CGC)<1.d-14)CGC=0.d0

    RETURN

  END FUNCTION ClebschGordanCoefficient


  real(kind(1d0)) function ThreeJCoefficient(j1,j2,j3,m1,m2)
    implicit none
    integer, intent(in) :: j1,j2,j3,m1,m2
    if(Not_Initialized_Yet)call InitThisModule
    ThreeJCoefficient = &
         ClebschGordanCoefficient(j1,j2,j3,m1,m2) / &
         sqrt(dble(2*j3+1))*(1-2*mod(abs(j1-j2+m1+m2),2))
    return
  end function ThreeJCoefficient


  real(kind(1d0)) function Racah(ja,jb,je,jd,jc,jf)
    integer, intent(in) :: ja,jb,je,jd,jc,jf
    Racah=SixJSymbol(ja,jb,je,jd,jc,jf)
    if(mod(ja+jb+jc+jd,2)==1)Racah=-Racah
    return
  end function Racah
  

  !> \internal
  !> If the triangular conditions for the 6j symbol
  !> \f[
  !>    \left\{\begin{array}{ccc}
  !>    j_a & j_b & j_c \\
  !>    j_d & j_e & j_f
  !>    \end{array}\right\}
  !> \f], i.e.
  !> \f{eqnarray} 
  !>   |j_a-jb|\leq j_c\leq j_a+j_b, \\
  !>   |j_a-je|\leq j_f\leq j_a+j_e, \\
  !>   |j_d-jb|\leq j_f\leq j_d+j_b, \\
  !>   |j_d-je|\leq j_c\leq j_d+j_e,
  !> \f}
  !> are satisfied, then 
  !>
  !>     lSixJSymbol=.TRUE.
  !>
  !> otherwise
  !>
  !>     lSixJSymbol=.FALSE.
  !>
  logical function lSixJSymbol(ja,jb,jc,jd,je,jf)
    implicit none
    integer, intent(in) :: ja,jb,jc,jd,je,jf
    lSixJSymbol=.false.
    if(ja>jb+jc.or.ja<abs(jb-jc))return
    if(ja>je+jf.or.ja<abs(je-jf))return
    if(jb>jd+jf.or.jb<abs(jd-jf))return
    if(jc>jd+je.or.jc<abs(jd-je))return
    lSixJSymbol=.true.
    return
  end function lSixJSymbol


  !> Returns the 6j symbol
  !> \f[
  !>    \left\{\begin{array}{ccc}
  !>    j_a & j_b & j_c \\
  !>    j_d & j_e & j_f
  !>    \end{array}\right\}
  !> \f].
  real(kind(1d0)) function SixJSymbol(ja,jb,jc,jd,je,jf) 
    !
    implicit real*8(a-h,o-z)
    !
    integer, intent(in) :: ja,jb,jc,jd,je,jf
    !
    if(Not_Initialized_Yet)call InitThisModule
    !
    SixJSymbol = 0.d0
    !
    if((ja+jb-jc)*(ja-jb+jc)*(jb+jc-ja).lt.0) return
    if((jd+jb-jf)*(jd-jb+jf)*(jb+jf-jd).lt.0) return
    if((je+jd-jc)*(je-jd+jc)*(jd+jc-je).lt.0) return
    if((ja+je-jf)*(ja-je+jf)*(je+jf-ja).lt.0) return
    !
    w = 0.d0
    !
    iabe=ja+jb+jc
    icde=je+jd+jc
    iacf=ja+je+jf
    ibdf=jb+jd+jf
    !
    jacdb=ja+je+jb+jd
    jadef=ja+jd+jc+jf
    jbcef=jb+je+jc+jf
    !
    ia=max(iabe,icde,iacf,ibdf)
    ib=min(jacdb,jadef,jbcef)
    !
    iin=ia
    ifin=ib
    !
    sig=1.d0
    sigf=sig
    !
    if(btest(iin,0)) sig=-sig
    !
    aa=0.d0
    do i=iin,ifin
       bb=fact(i-iabe)*fact(i-icde)*fact(i-iacf)*&
            fact(i-ibdf)*fact(jacdb-i)*fact(jadef-i)*fact(jbcef-i)
       aa=aa+sig*fact(i+1)/bb
       sig=-sig
    enddo
    !
    SixJSymbol=aa*delta(ja,jb,jc)*delta(jb,jd,jf)*&
         delta(je,jd,jc)*delta(ja,je,jf)*sigf
    !
    if(abs(SixJSymbol)<1.d-14)SixJSymbol=0.d0
    !
    return
    !
  end function SixJSymbol


  !> Computes the function
  !>\f[
  !>   \Delta = \sqrt{
  !>        \frac{ (j_1+j_2-j_3)! \, (j_1-j_2+j_3)! \, (j_2+j_3-j_1)! }{ 
  !>                                (j_1+j_2+j_3+1)! } }
  !>\f]
  real(kind(1d0)) function Delta(j1,j2,j3)
    integer, intent(in) :: j1, j2, j3
    implicit real*8 (a-h,o-z)
    if(Not_Initialized_Yet)call InitThisModule
    Delta=sqrt(fact(j1+j2-j3)*fact(j1-j2+j3)*fact(j2+j3-j1)/fact(j1+j2+j3+1))
    return
  end function Delta


  logical function TriangularCondition(j1,j2,j3)
    integer :: j1,j2,j3
    TriangularCondition = &
         (  j1 >= abs( j2 - j3 )  ) .and. &
         (  j1 <=      j2 + j3    )
  end function TriangularCondition


  !.. Check triangular conditions for the 9J Symbol
  !    
  !     | j11 j12 j13 |
  !    <  j21 j22 j23  >
  !     | j31 j32 j33 |
  !..
  logical function lNineJSymbol(j11,j12,j13,j21,j22,j23,j31,j32,j33)
    implicit none
    integer, intent(in) :: j11,j12,j13,j21,j22,j23,j31,j32,j33
    lNineJSymbol=.false.
    if(.not.TriangularCondition(j11,j12,j13))return
    if(.not.TriangularCondition(j21,j22,j23))return
    if(.not.TriangularCondition(j31,j32,j33))return
    if(.not.TriangularCondition(j11,j21,j31))return
    if(.not.TriangularCondition(j12,j22,j32))return
    if(.not.TriangularCondition(j13,j23,j33))return
    lNineJSymbol=.true.
  end function lNineJSymbol


  !> Returns the 9j symbol
  !> \f[
  !>    \left\{\begin{array}{ccc}
  !>    j_{11} & j_{12} & j_{13} \\
  !>    j_{21} & j_{22} & j_{23} \\
  !>    j_{31} & j_{32} & j_{33}
  !>    \end{array}\right\}
  !> \f].
  !> 
  !> computed using Eq.5 on pag. 305 of Varshalovich 
  !>
  real(kind(1d0)) function NineJSymbol(j11,j12,j13,j21,j22,j23,j31,j32,j33)
    implicit none
    integer, intent(in) :: j11,j12,j13,j21,j22,j23,j31,j32,j33
    integer :: ell, ellmi, ellma

    if(Not_Initialized_Yet)call InitThisModule

    NineJSymbol=0.d0

    if(.not.TriangularCondition(j11,j12,j13))return
    if(.not.TriangularCondition(j21,j22,j23))return
    if(.not.TriangularCondition(j31,j32,j33))return
    if(.not.TriangularCondition(j11,j21,j31))return
    if(.not.TriangularCondition(j12,j22,j32))return
    if(.not.TriangularCondition(j13,j23,j33))return

    ellmi = max(abs(j11-j33),abs(j32-j21),abs(j23-j12))
    ellma = min(    j11+j33 ,    j32+j21 ,    j23+j12 )
    do ell = ellmi, ellma
       NineJSymbol = NineJSymbol + dble(2*ell+1) * &
            SixJSymbol(j11,j33,ell,j32,j21,j31) * &
            SixJSymbol(j32,j21,ell,j23,j12,j22) * &
            SixJSymbol(j23,j12,ell,j11,j33,j13)
    enddo
  end function NineJSymbol



  logical function delparf(j1,j2,j3,j4,ll)
    !VERIFICA PARITA' DI j1+j2+j3+j4 E LIMITI li e lm
    !DEI POSSIBILI VALORI DI l COMPATIBILI CON LE
    !CONDIZIONI DI TRIANGOLARITA' D(j1,j2,l) D(j3,j4,l)
    !log1=TRUE SE AMBEDUE I TESTS SONO POSITIVI
    integer, intent(in) :: j1,j2,j3,j4,ll
    delparf=.FALSE.
    if(btest(j1+j2+j3+j4,0))return
    if(btest(j1+j2+ll,0))return
    if(ll<max(abs(j1-j2),abs(j3-j4)))return
    if(ll>min(j1+j2,j3+j4))return
    delparf=.TRUE.
    return
  end function delparf

  logical function TRITPARF(j1,j2,j3,j4,j5,j6,ll)
    !CONTROLLA CONDIZIONI TRIANGOLARI D(j1,j2,j) D(j3,j4,j)
    !E D(j5,j6,j). SE LE CONDIZIONI SONO SODDISFATTE TUTTE
    !PER UN INTERVALLO DI VALORI DI j log1=true ed in ji e jm
    !SARANNO POSTI I VALORI MINIMO E MASSIMO DI j
    !ALTRIMENTI log1=false. Si assicura che la parita' di j5+j6 sia la
    !stessa che quella di j1+j2, aggiustando il range relativo.
    implicit none
    integer, intent(in) :: j1,j2,j3,j4,j5,j6,ll
    integer :: j12,l12,j34,l34,j56,l56,ji,jm
    tritparf=.FALSE.
    j12 =j1+j2
    l12=iabs(j1-j2)
    j34=j3+j4
    l34=iabs(j3-j4)
    j56=j5+j6
    l56=iabs(j5-j6)
    j56=j56-mod(j56+j12,2)
    l56=l56+mod(j56+j12,2)
    ji=max(l12,l34,l56)
    jm=min(j12,j34,j56)
    if(ll<ji.or.ll>jm)return
    tritparf=.TRUE.
    return
  end function TRITPARF

  logical function trif(j1,j2,j3)
    !CONTROLLA CONDIZIONE TRANGOLARE D(j1,j2,j3)
    !.TRUE. SE LA CONDIZIONE E' VERIFICATA
    implicit none
    integer, intent(in) :: j1,j2,j3
    trif=(j3>=abs(j1-j2)).and.(j3<=j1+j2)
    return
  end function trif

  logical function tritf(j1,j2,j3,j4,j5,j6,ji,jm)
    !Controlla l'intervallo [ji,jm] di j in cui sono soddisfatte 
    !le condizioni triangolari D(j1,j2,j) D(j3,j4,j) E D(j5,j6,j). 
    !Se l'intervallo esiste (ji<=jm) tritf=.TRUE.
    implicit none
    integer, intent(in) :: j1,j2,j3,j4,j5,j6
    integer, intent(out):: ji,jm
    jm=min(j1+j2,j3+j4,j5+j6)
    ji=max(abs(j1-j2),abs(j3-j4),abs(j5-j6))
    tritf=(jm>=ji)
    return
  end function tritf

  real(kind(1d0)) function sbcsn(j1,j2,j3)
    !CALCOLA IL COEFFICIENTE CS(J1,J2,J3)=C(J1,J2,J3;0,0)*[(2J1+1)*(2J2+1)/(2J3+1)]**1/2
    implicit none
    integer,intent(in) :: j1,j2,j3
    sbcsn=ClebschGordanCoefficient(j1,j2,j3,0,0)*sqrt(dble((2*j1+1)*(2*j2+1))/dble(2*j3+1))
    return
  end function sbcsn

  real(kind(1d0)) function mypar(n)
    implicit none
    integer, intent(in) :: n
    mypar=1.d0-2.d0*mod(n,2)
    return
  end function mypar

complex(kind(1d0)) function BISPHARM(l1,l2,L,M,th1,ph1,th2,ph2)
  implicit none
  integer        , intent(in) :: l1,l2,L,M
  real(kind(1d0)), intent(in) :: th1,ph1,th2,ph2
  !Calcola l'armonica sferica bipolare assegnati i due angoli
  !e tutti i numeri quantici necessari: i momenti angolari
  !delle due armoniche sferiche accoppiate, quello dell'accoppiamento
  !complessivo e la sua proiezione sull'asse di quantizzazione.
  ![Y_l1(theta_1,phi_1)(x)Y_l2(theta_2,phi_2)]_{LM}=
  !sum_m C_{l_1m,l_2 M-m}^{LM}Y_{l_1m}(theta_1,phi_1)Y_{l_2M-m}(theta_2,phi_2)
  !(Varshalovic et al. "Quantum Theory of Angular Momentum" par. 15.16.1
  ! Ed. World Scientific (Singapore)
   
  complex(kind(1d0)) :: cf
  real(kind(1d0)) :: x1,x2,rf,w1,w2,w3,phi
  integer :: mu
   
  BISPHARM=(0.d0,0.d0)
  if(l1<0.or.l2<0.or.L<abs(l1-l2).or.L>l1+l2.or.abs(M)>L)return
  x1=cos(th1)
  x2=cos(th2)
  do mu=max(-l1,M-l2),min(l1,M+l2)
     w1=ClebschGordanCoefficient(l1,l2,L,mu,M-mu)
     w2=NORLEG(l1,mu  ,x1)
     w3=NORLEG(l2,M-mu,x2)
     rf=w1*w2*w3
     phi=dble(mu)*ph1+dble(M-mu)*ph2
     cf=rf*(cos(phi)*(1.d0,0.d0)+sin(phi)*(0.d0,1.d0))
     BISPHARM=BISPHARM+cf
  end do
  return 
end function BISPHARM


real(kind(1d0)) function NORLEG(l,m,c)
  implicit none
  integer        , intent(in) :: l, m
  real(kind(1d0)), intent(in) :: c
  !Funzione naive per il calcolo dei polinomi di 
  !legendre normalizzati, scritta per disperazione:
  !quelle di libreria non funzionano. In seguito 
  !porremo rimedio. In ogni caso questa subroutine
  !puo` essere utilizzata da una piu` generale nei
  !casi piu` semplici dal momento che e` prevedibilmente
  !piu` celere delle altre.
  !History (yymmdd):
  !051219: 1 error found.
  real(kind(1d0)), parameter :: M_PI=3.14159265358979323844d0
  real(kind(1d0)) :: s
  NORLEG=0.d0
  if(abs(c)>1.d0.or.l<0.or.l>5.or.abs(m)>l)return
  s=sqrt(1.d0-c*c)
  select case(l)
  case(0)
     NORLEG=1.d0/(2.d0*sqrt(M_PI))
  case(1)
     select case(m)
     case( 1) 
        NORLEG=-0.5d0*sqrt(1.5d0/M_PI)*s
     case( 0) 
        NORLEG= 0.5d0*sqrt(3.d0/M_PI)*c
     case(-1)
        NORLEG= 0.5d0*sqrt(1.5d0/M_PI)*s
     end select
  case(2)
     select case(m)
     case( 2) 
        NORLEG= 0.25d0*sqrt(7.5d0/M_PI)*s*s
     case( 1) 
        NORLEG=-0.50d0*sqrt(7.5d0/M_PI)*s*c
     case( 0)
        NORLEG= 0.25d0*sqrt(5.d0/M_PI)*(3.d0*c*c-1.d0)
     case(-1) 
        NORLEG= 0.50d0*sqrt(7.5d0/M_PI)*s*c
     case(-2) 
        NORLEG= 0.25d0*sqrt(7.5d0/M_PI)*s*s
     end select
  case( 3)
     select case(m)
     case( 3) 
        NORLEG=-0.125d0*sqrt(35.d0/M_PI)*s*s*s
     case( 2) 
        NORLEG= 0.250d0*sqrt(52.5d0/M_PI)*c*s*s
     case( 1) 
        NORLEG=-0.125d0*sqrt(21.d0/M_PI)*(5.d0*c*c-1.d0)*s
     case( 0) 
        NORLEG= 0.250d0*sqrt( 7.d0/M_PI)*(5.d0*c*c-3.d0)*c
     case(-1) 
        NORLEG= 0.125d0*sqrt(21.d0/M_PI)*(5.d0*c*c-1.d0)*s
     case(-2) 
        NORLEG= 0.250d0*sqrt(52.5d0/M_PI)*c*s*s
     case(-3) 
        NORLEG= 0.125d0*sqrt(35.d0/M_PI)*s*s*s
     end select
  case(4)
     select case(m)
     case( 4) 
        NORLEG= 0.1875d0*sqrt(17.5d0/M_PI)*s*s*s*s
     case( 3) 
        NORLEG=-0.3750d0*sqrt(35.0d0/M_PI)*c*s*s*s
     case( 2) 
        NORLEG= 0.3750d0*sqrt( 2.5d0/M_PI)*(7.d0*c*c-1.d0)*s*s
     case( 1) 
        NORLEG=-0.3750d0*sqrt( 5.0d0/M_PI)*(7.d0*c*c-3.d0)*c*s
     case( 0) 
        NORLEG= 0.1875d0*sqrt( 1.0d0/M_PI)*(35.d0*c*c*c*c-30.d0*c*c+3.d0)
     case(-1) 
        NORLEG= 0.3750d0*sqrt( 5.0d0/M_PI)*(7.d0*c*c-3.d0)*c*s
     case(-2) 
        NORLEG= 0.3750d0*sqrt( 2.5d0/M_PI)*(7.d0*c*c-1.d0)*s*s
     case(-3) 
        NORLEG= 0.3750d0*sqrt(35.0d0/M_PI)*c*s*s*s
     case(-4) 
        NORLEG= 0.1875d0*sqrt(17.5d0/M_PI)*s*s*s*s
     end select
  case(5)
     select case(m)
     case( 5) 
        NORLEG=-0.09375d0*sqrt( 77.d0/M_PI)*s*s*s*s*s
     case( 4) 
        NORLEG= 0.18750d0*sqrt(192.5d0/M_PI)*c*s*s*s*s
     case( 3) 
        NORLEG=-0.03125d0*sqrt(385.d0/M_PI)*(9.d0*c*c-1.d0)*s*s*s
     case( 2) 
        NORLEG= 0.12500d0*sqrt(577.5d0/M_PI)*(3.d0*c*c-1.d0)*c*s*s
     case( 1) 
        NORLEG=-0.06250d0*sqrt( 82.5d0/M_PI)*(21.d0*c*c*c*c-14.d0*c*c+ 1.d0)*s
     case( 0) 
        NORLEG= 0.06250d0*sqrt( 11.d0/M_PI)*(63.d0*c*c*c*c-70.d0*c*c+15.d0)*c
     case(-1) 
        NORLEG= 0.06250d0*sqrt( 82.5d0/M_PI)*(21.d0*c*c*c*c-14.d0*c*c+ 1.d0)*s
     case(-2) 
        NORLEG= 0.12500d0*sqrt(577.5d0/M_PI)*(3.d0*c*c-1.d0)*c*s*s
     case(-3) 
        NORLEG= 0.03125d0*sqrt(385.d0/M_PI)*(9.d0*c*c-1.d0)*s*s*s
     case(-4) 
        NORLEG= 0.18750d0*sqrt(192.5d0/M_PI)*c*s*s*s*s
     case(-5) 
        NORLEG= 0.09375d0*sqrt( 77.d0/M_PI)*s*s*s*s*s
     end select
  end select
  return
end function NORLEG

!!$real(kind(1d0)) function legpolyfun(l,m_,x)
!!$  !This function follows the phase convention
!!$  !of Jackson  (Classical Electrodynamics)
!!$  !Abramovitz  (Handbook of Mathematical Functions)
!!$  !Varshalovic (Quantum Theory of Angular Momentum)
!!$  !while it differs by a factor (-1)^m from the
!!$  !conventions adopted by Messiah (Quantum Mechanics)
!!$  !and Friedrich (Theoretical Atomic Physics).
!!$  implicit none
!!$  integer        , intent(in) :: l,m_
!!$  real(kind(1d0)), intent(in) :: x
!!$  integer         :: m,j
!!$  real(kind(1d0)) :: c1,c2,c3,y
!!$  legpolyfun=0.d0
!!$  m=abs(m_)
!!$  !Check on input parameters
!!$  if(l<0.or.m>l.or.abs(x)>1.d0)return
!!$  !Case l==0
!!$  if(l==0)then
!!$     legpolyfun=1.d0
!!$     return
!!$  endif
!!$  !Case x = +/- 1
!!$  if(1.d0-abs(x)<=epsilon(1.d0))then
!!$     if(m/=0)return
!!$     legpolyfun=1.d0
!!$     if(x<0.and.mod(l,2)==1)legpolyfun=-1.d0
!!$     return
!!$  endif
!!$  !Regular cases
!!$  y=exp(0.5d0*dble(m)*log(1.d0-x*x))
!!$  do j=2*m-1,0,-2
!!$     y=y*dble(j)
!!$  enddo
!!$  if(mod(m,2)==1)y=-y
!!$  c1=y
!!$  c2=0.d0
!!$  do j=m+1,l
!!$     c3=c2
!!$     c2=c1
!!$     c1=dble(2*j-1)/dble(j-m)*x*c2-dble(j+m-1)/dble(j-m)*c3
!!$  enddo
!!$  !Anche se e` asimmetrico rispetto al cambio
!!$  !di segno di m, ce lo teniamo cosi`.
!!$  if(m_<0)then
!!$     y=1.d0
!!$     do j=l-m_+1,l+m_
!!$        y=y*dble(j)
!!$     enddo
!!$     c1=c1/y
!!$  endif
!!$  legpolyfun=c1
!!$  return
!!$end function legpolyfun

recursive real(kind(1d0)) function RAssLegFun(l,m,x) result (res)
  !Associated Legendre Function
  implicit none
  integer        , intent(in) :: l, m
  real(kind(1d0)), intent(in) :: x
  real(kind(1d0))             :: y
  integer :: j
  res=0.d0
  if(l<0.or.abs(m)>l.or.abs(x)>1.d0)return
  if(abs(m)<l)then
     res =dble(2*l-1)/dble(l-m)*x*RAssLegFun(l-1,m,x)-&
          dble(l+m-1)/dble(l-m)*  RAssLegFun(l-2,m,x)
     return
  endif
  if(l==0)then
     res=1.d0
     return
  endif
  if(1.d0-abs(x)<=epsilon(1.d0))return
  res=exp(0.5d0*dble(l)*log(1.d0-x*x))
  if(mod(l,2)==1)res=-res
  if(m==l)then
     do j=2*l-1,1,-2
        res=res*dble(j)
     enddo
  else
     do j=2*l,1,-2
        res=res/dble(j)
     enddo
  endif
  return
end function RAssLegFun

real(kind(1d0)) function LegPolF(l,x) result(res)
  !Legendre Polynomials
  implicit none
  integer        , intent(in) :: l
  real(kind(1d0)), intent(in) :: x
  real(kind(1d0)) :: c_1,c_2
  integer :: ll
  res=0.d0;if(l<0.or.abs(x)>1.d0)return
  res=1.d0;if(l==0)return
  c_1=0
  do ll=1,l
     c_2=c_1
     c_1=res
     res=(dble(2*ll-1)*x*c_1-dble(ll-1)*c_2)/dble(ll)
  enddo
  return
end function LegPolF

subroutine LegPolS(l,x,P,dP,P_1,dP_1)
  !Legendre Polynomials
  !calcola P_l(x),dP_l(x)/dx,P_{l-1}(x),dP_{l-1}(x)/dx
  !L'ho testato e mi pare funzionare correttamente
  implicit none
  integer        , intent(in) :: l
  real(kind(1d0)), intent(in) :: x
  real(kind(1d0)), intent(out):: P,dP,P_1,dP_1
  integer :: ll
  real(kind(1d0)) :: c_1,c_2
  real(kind(1d0)) :: d_1,d_2
  P =0.d0
  dP=0.d0
  P_1=0.d0
  dP_1=0.d0
  if(l<0.or.abs(x)>1.d0)return
  P=1.d0;if(l==0)return
  c_1=0
  d_1=0
  do ll=1,l
     c_2=c_1
     d_2=d_1
     c_1=P
     d_1=dP
     P   =(dble(2*ll-1)*x*c_1-dble(ll-1)*c_2)/dble(ll)
     dP  =(dble(2*ll-1)*c_1+dble(2*ll-1)*x*d_1-dble(ll-1)*d_2)/dble(ll)
     P_1 =c_1
     dP_1=d_1
  enddo
  return
end subroutine LegPolS

real(kind(1d0)) function Plm(x,l,m) result(res)
  !Compute the Associated Legendre functions defined as 
  !Plm(x)=(-1)^m (1-x^2)^{m/2} d^m/dx^m Pl(x)
  !where Pl(x) is the Legendre polynomials
  implicit none
  real(kind(1d0)), intent(in) :: x
  integer        , intent(in) :: l,m
  real(kind(1d0)) :: st,Pmm,c0,c1,c2
  integer         :: j
  res=0.d0
  if(abs(x)>1.d0.or.abs(m)>l.or.l<0)return
  !Compute P|m|m according to the formulas
  !P|m|  |m|(x)  =(-1)^|m| (2|m|)!/ [2^|m| |m|!] sin^|m|(x)
  !P|m|,-|m|(x)  =              1 / [2^|m| |m|!] sin^|m|(x)
  st=sqrt(1-x*x)
  if(m>=0)then
     Pmm=1.d0-2.d0*mod(m,2)
     do j=1,m
        Pmm=Pmm*0.5d0*dble(m+j)*st
     enddo
  else
     Pmm=1.d0
     do j=1,abs(m)
        Pmm=Pmm*0.5d0/dble(j)*st
     enddo
  endif
  if(l==abs(m))then
     res=Pmm
     return
  endif
  !Compute Plm with the recursive formula
  !(l-m)P_{l,m}=(2l-1)xP_{l-1,m}-(l-1+m)P_{l-2,m}
  c0=Pmm
  c1=Pmm*x*dble(2*m+1)
  do j=abs(m)+2,l
     c2=(dble(2*j-1)*x*c1-dble(j-1+m)*c0)/dble(j-m)
     c0=c1
     c1=c2
  enddo
  res=c1
  return
end function Plm

complex(kind(1d0)) function Ylm(theta,phi,l,m) result(res)
  !Computes the spherical harmonics with the conventions of 
  !Varshalovich et al:
  ! Y_{lm}*(theta,phi)=Y_{lm}(theta,-phi)=(-1)^m Y_{l,-m}(theta,phi)
  !Parrebbe funzionare
  implicit none
  real   (kind(1d0)), intent(in) :: theta,phi
  integer           , intent(in) :: l,m
  complex(kind(1d0)), parameter  :: IU=(0.d0,1.d0)
  real   (kind(1d0)), parameter  :: PI=3.14159265358979323844d0
  integer         :: j
  real(kind(1d0)) :: st,norm,x,Plm_
  res=(0.d0,0.d0)
  if(theta<0.d0.or.theta>PI.or.l<0.or.abs(m)>l)return
  x =cos(theta)
  Plm_=Plm(x,l,m)
  !Normalization factor Sqrt[(2l+1)/(4 Pi) * (l-m)!/(l+m)!]
  norm=1.d0
  do j=-abs(m)+1,abs(m)
     norm=norm*dble(l+j)
  enddo
  if(m>0)norm=1.d0/norm
  norm=sqrt(dble(2*l+1)/(4.d0*PI)*norm)
  res = norm * Plm_ * exp( IU * dble(m) * phi )
  return
end function Ylm

!> Compute the bipolar spherical harmonics
!> \f[
!>   \mathcal{Y}_{l_1l_2}^{LM}(\Omega_1,\Omega_2)\,=\,
!>   \sum_{m_1m_2}\,C_{l_1m_1,l_2m_2}^{LM}\,Y_{l_1m_1}(\Omega_1)\,Y_{l_2m_2}(\Omega_2)
!> \f]
complex(kind(1d0)) function BipolarSphericalHarmonics(theta1,phi1,theta2,phi2,l1,l2,L,M) result(res)
  implicit none
  real(kind(1d0)), intent(in) :: theta1,phi1,theta2,phi2
  integer        , intent(in) :: l1,l2,L,M
  real(kind(1d0)), parameter  :: PI=3.14159265358979323844d0
  integer            :: mu
  complex(kind(1d0)) :: z1,z2
  res=(0.d0,0.d0)
  if(abs(M)>L.or.max(theta1,theta2)>PI.or.min(theta1,theta2)<0.d0&
       .or.l1<0.or.l2<0.or.L<abs(l1-l2).or.L>(l1+l2))return
  do mu=max(-l1,M-l2),min(l1,M+l2)
     z1=Ylm(theta1,phi1,l1, mu )
     z2=Ylm(theta2,phi2,l2,M-mu)
     res=res+ClebschGordanCoefficient(l1,l2,L,mu,M-mu)*z1*z2
  enddo
  return
end function BipolarSphericalHarmonics

!> Compute the symmetry adapted bipolar spherical harmonics
!> Yl1l2^LM+/-(Omega1,Omega2)=
!>  =[Y_{l1,l2}^{LM}(Omega1,Omega2)+/-Y_{l1,l2}^{LM}(Omega2,Omega1)]/sqrt(2)
!> If s is even => Y+, if s is odd => Y-
complex(kind(1d0)) function SymmetryAdaptedBipolarSphericalHarmonics(theta1,phi1,theta2,phi2,l1,l2,L,M,s) result(res)
  implicit none
  real(kind(1d0)), intent(in) :: theta1,phi1,theta2,phi2
  integer        , intent(in) :: l1,l2,L,M,s
  complex(kind(1d0)) :: z1,z2
  z1=BipolarSphericalHarmonics(theta1,phi1,theta2,phi2,l1,l2,L,M)
  z2=BipolarSphericalHarmonics(theta2,phi2,theta1,phi1,l1,l2,L,M)*(1.d0-2.d0*mod(s,2))
  res=(z1+z2)/sqrt(2.d0)
  return
end function SymmetryAdaptedBipolarSphericalHarmonics

complex(kind(1d0)) function WRME(alpha,beta,gamma,m1,m2,j) result(res)
  !Wigner Rotation Matrix Elements
  !not checked yet: be carful!
  implicit none
  real(kind(1d0)), intent(in) :: alpha,beta,gamma
  integer        , intent(in) :: j,m1,m2
  complex(kind(1d0)), parameter  :: IU=(0.d0,1.d0)
  real(kind(1d0)) :: cbm,sbm
  integer         :: k
  res=(0.d0,0.d0)
  if(max(abs(m1),abs(m2))>j)return
  !Attenzione: estende gli elementi di matrice anche oltre 
  !il dominio alpha [0:2 Pi], beta [0,Pi], gamma[0:2 Pi]
  res=(1.d0,0.d0)
  cbm=cos(beta/2.d0)
  sbm=sin(beta/2.d0)
  !Calcolo di djmm
  do k=max(0,-m1-m2),min(j-m1,j-m2)
     res=res+(1.d0,0.d0)*(1.d0-2.d0*mod(k,2))*&
          mypow(cbm,m1+m2+2*k)*mypow(sbm,2*j-m1-m2-2*k)/&
          (fact(k)*fact(j-m1-k)*fact(j-m2-k)*fact(m1+m2+k))
  enddo
  res=res*sqrt(fact(j+m1)*fact(j-m1)*fact(j+m2)*fact(j-m2))*(1.d0-2.d0*mod(j-m2,2))
  res=exp(-IU*(m1*alpha+m2*gamma))*res
  return
end function WRME

real(kind(1d0)) function mypow(x,n) result(res)
  real(kind(1d0)), intent(in) :: x
  integer        , intent(in) :: n
  res=1.d0;if(n==0)return
  res=0.d0;if(x<=0)return
  res=exp(dble(n)*log(x))
  return
end function mypow

real(kind(1d0)) function spsabsh(x,J,S,l1,l2,l3,l4) result(res)
  !Compute the
  !Scalar Product between Symmetry Adapted Bipolar Spherical Harmonic
  implicit none
  real(kind(1d0)), intent(in) :: x
  integer        , intent(in) :: J,S,l1,l2,l3,l4
  real(kind(1d0)), parameter  :: PI = 3.14159265358979323844d0
  integer                     :: nat,sym,par
  integer                     :: l,lmi,lma

  res=0.d0
  if(  min(l1,l2,l3,l4)<0          .or.&
       J<max(abs(l1-l2),abs(l3-l4)).or.&
       J>min(l1+l2,l3+l4)          .or.&
        abs(x)>1.d0                .or.&
        mod(l1+l2+l3+l4,2)/=0 ) return

  par=mod(l1+l2,2)
  nat=mod(J+par,2)
  sym=mod(S,2)

  lmi=min(max(abs(l1-l3),abs(l2-l4)),max(abs(l1-l4),abs(l2-l3)))
  lma=max(min(l1+l3,l2+l4),min(l1+l4,l2+l3))
  do l=lmi,lma
     if(mod(l1+l3+l,2)==1.and.mod(l1+l4+l,2)==1)cycle
     res=res+Plm(x,l,0)*(1.d0-2.d0*mod(l,2))*(&
          ClebschGordanCoefficient(l1,l3,l,0,0)*&
          ClebschGordanCoefficient(l2,l4,l,0,0)*&
          SixJSymbol(l1,l2,J,l4,l3,l)+&
          ClebschGordanCoefficient(l1,l4,l,0,0)*&
          ClebschGordanCoefficient(l2,l3,l,0,0)*&
          SixJSymbol(l1,l2,J,l3,l4,l)*&
          (1.d0-2.d0*mod(nat+s,2)))
  enddo

  res=res*sqrt(dble((2*l1+1)*(2*l2+1)*(2*l3+1)*(2*l4+1))) *&
       dble(2*J+1)*(1.d0-2.d0*mod(J,2))/(4.d0*PI)**2

  return
end function spsabsh

subroutine mtssabsh(J,N,S,lv,DIM)
  !Compute the dimension DIM and angular momenta lv(1:DIM) 
  !of the pairs ( l, J + N - l ) with 2*l >= J + N 
  !of the minimal tensorial set of symmetry adapted
  !bipolar spherical harmonic with the assigned
  !quantum numbers 
  ! J : total angular momentum
  ! N : naturality (N odd => unnatural, N even => natural)
  ! S : parity upon interchange of solid angles
  !     (S even => even, S odd => odd)
  ! Checked
  integer, intent(in) :: J,N,S
  integer, intent(out):: lv(*)
  integer, intent(out):: DIM
  integer :: l
  DIM=0
  if(J<0)return
  l=int(dble(J+mod(N,2))/2.d0+0.6d0)
  if(mod(J+N,2)==0.and.(mod(N+S,2)==1))l=l+1
  do while(l<=J)
     DIM=DIM+1
     lv(DIM)=l
     l=l+1
  enddo
  return
end subroutine mtssabsh

subroutine smmtssabsh(x,J,N,S,A,LDA,lv,DIM)
  !Compute the
  !Superposition Matrix of a Minimal Tensorial Set of 
  !Symmetry Adapted Bipolar Spherical Harmonic
  implicit none
  real(kind(1d0)), intent(in) :: x
  integer        , intent(in) :: J,N,S
  real(kind(1d0)), intent(out):: A(LDA,*)
  integer        , intent(in) :: LDA
  integer        , intent(in) :: DIM,lv(*)
  !x     : cos(theta_{12}), -1 <= x <= 1
  ! J    : total angular momentum
  ! N    : naturality
  ! S    : parity upon exchange of the angular 
  !        variables:
  !        SABSA(Omega2,Omega1)=(-1)^s SABSA(Omega1,Omega2)
  !        Where SABSA stands for Symmetry Adapted Bipolar
  !        Spherical Harmonic, and is defined as
  !        SABSA_{l_1l_2}^{JMs}(Omega1,Omega2) =
  !          = ( BSA_{l_1l_2}^{JMs}(Omega1,Omega2) +
  !              BSA_{l_1l_2}^{JMs}(Omega2,Omega1) * (-1)^s )/sqrt(2)
  ! A    : superposition matrix
  ! lda  : leading dimension of A
  ! lv   : vector of highest angular momenta of the minimal basis
  ! dim  : dimension of the minimal tensorial set
  !        with angular momentum J, parity l1+l2 and
  !        simmetry upon exchange s
  integer :: i1,i2,j1,j2,j3,j4,nat
  if(J<0.or.abs(x)>1.d0.or.DIM<=0.or.DIM>LDA)return

  !Build the superposition matrix 
  nat=mod(N,2)
  A(1:LDA,1:dim)=0.d0
  do i1=1,dim
     j1=lv(i1)
     j2=J+nat-j1
     do i2=i1,dim
        j3=lv(i2)
        j4=J+nat-j3
        A(i1,i2)=spsabsh(x,J,S,j1,j2,j3,j4)
        A(i2,i1)=A(i1,i2)
     enddo
  enddo

  return
end subroutine smmtssabsh

subroutine angcoup2(LANG,N,S,LMAX,NAC,l1v,l2v)
  !Compute the allowed couples of angular momenta
  !compatible with the assigned quantum numbers
  implicit none
  integer, intent(in) :: LANG,N,S,LMAX
  integer, intent(out):: NAC,l1v(*),l2v(*)
  integer :: l1,l2
  NAC=0
  do l1=0,LMAX
     do l2=abs(LANG-l1)+mod(N,2),l1-mod(N+S,2),2
        NAC=NAC+1
        l1v(NAC)=l1
        l2v(NAC)=l2
     enddo
  enddo
  return
end subroutine angcoup2


subroutine angcouppm(L12,N12,SGN,LMAX,NCPL,l1v,l2v)
  !Compute the allowed couples of angular momenta
  !compatible with the assigned quantum numbers
  implicit none
  integer, intent(in) :: L12,N12,SGN,LMAX
  integer, intent(out):: NCPL,l1v(*),l2v(*)
  integer :: l1,l2,lambda,P12
  NCPL=0
  P12=mod(L12+N12,2)
  lambda=0
  if(P12==0.and.mod(N12+SGN,2)==1)lambda=1
  !SGN entra solo nel determinare se
  !l'accoppiamento ll e` accettabile o meno
  do l1=0,LMAX
     do l2=abs(L12-l1)+mod(N12,2),l1-lambda,2
        NCPL=NCPL+1
        l1v(NCPL)=l1
        l2v(NCPL)=l2
     enddo
  enddo
  return
end subroutine angcouppm

end module ModuleAngularMomentum
