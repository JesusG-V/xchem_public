!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> Symmetry adapted spherical harmonics (SASH) \f$ X_{\ell m} \f$ are defined as
!!
!! \f{eqnarray}
!!    X_{\ell  0} &=& Y_{\ell 0}, \\
!!    X_{\ell  m} &=& \frac{1}{\sqrt{2}   }\left( Y_{\ell m} + \left( -1 \right)^{m}Y_{\ell -m} \right), m > 0 \\
!!    X_{\ell -m} &=& \frac{1}{\sqrt{2}\,i}\left( Y_{\ell m} - \left( -1 \right)^{m}Y_{\ell -m} \right), m > 0.
!! \f}
!!
!! where $Y_{\ell m}$ are ordinary spherical harmonics, \f$ |m| \leq \ell \f$.
!!
!! Each SASH span a single irreducible representation of the D2h group or any of its subgroup,
!! if defined with respect to the same set of coordinate cartesian axes. 
!!
!!
!!  C1    
!!      A     Xlm Xl-m  l any  m any   N = (2l+1) 
!!
!!  Cs 
!!      A'    Xlm Xl-m  l+m  even      N = l+1
!!      A''    Xlm Xl-m  l+m  odd       N = l
!!
!!  C2 
!!      A     Xlm Xl-m  l any  m even  N = 2[l/2]+1
!!      B     Xlm Xl-m  l any  m odd   N = 2[(l+1)/2]
!!
!!  Ci
!!      Ag    Xlm Xl-m  l even         N = 2l+1
!!      Au    Xlm Xl-m  l odd          N = 2l+1
!!
!!  C2v
!!      A1    Xlm       l any  m even  N = [l/2] + 1
!!      A2    Xl-m      l any  m even  N = [l/2]
!!      B1    Xlm       l any  m odd   N = [(l+1)/2]
!!      B2    Xl-m      l any  m odd   N = [(l+1)/2]
!!
!!  C2h
!!      Ag    Xlm Xl-m  l even m even  N = l+1
!!      Bg    Xlm Xl-m  l even m odd   N = l
!!      Au    Xlm Xl-m  l odd  m even  N = l
!!      Bu    Xlm Xl-m  l odd  m odd   N = l+1
!!
!!  D2
!!      A     Xlm       l even m even  N =  l/2+1  ||  
!!            Xl-m      l odd  m even  N = (l-1)/2
!!      B1    Xlm       l odd  m even  N = (l+1)/2 ||  
!!            Xl-m      l even m even  N =  l/2
!!      B2    Xlm       l even m odd   N =  l/2    ||  
!!            Xl-m      l odd  m odd   N = (l+1)/2
!!      B3    Xlm       l odd  m odd   N = (l+1)/2 ||  
!!            Xl-m      l even m odd   N =  l/2
!!
!!  D2h
!!      Ag    Xlm       l even m even  N =  l/2+1
!!      B1g   Xl-m      l even m even  N =  l/2
!!      B2g   Xlm       l even m odd   N =  l/2
!!      B3g   Xl-m      l even m odd   N =  l/2
!!      Au    Xl-m      l odd  m even  N = (l-1)/2
!!      B1u   Xlm       l odd  m even  N = (l+1)/2
!!      B2u   Xl-m      l odd  m odd   N = (l+1)/2
!!      B3u   Xlm       l odd  m odd   N = (l+1)/2
!!
module ModuleSymmetryAdaptedSphericalHarmonics
  


  use, intrinsic :: ISO_FORTRAN_ENV
  
  use ModuleGroups
  use ModuleErrorHandling
  use ModuleString
  use ModuleConstants
  use ModuleMatrix
  use symba
  ! To manage vectors sorting. (gotten rid of to ensure gfrotran protability)
  !use IFPORT

  implicit none


  !> Defines the class of a definite symmetry adapted spherical harmonic.
  type, public :: ClassXlm
     ! {{{ 

     !> Angular momentum.
     integer :: l
     !> Angular momentum projection.
     integer :: m

     ! }}}
   contains
     !> Initializes the Xlm class.
     procedure :: Init => ClassXlmInit
     generic   :: Getl => ClassXlmGetl
     generic   :: Getm => ClassXlmGetm
     generic   :: Show => ClassXlmShow
     generic   :: Save => ClassXlmSave
     generic   :: Load => ClassXlmLoad
     generic   :: Is   => ClassXlmIs
     generic   :: GetLabel => ClassXlmGetLabel
     generic   :: GetIrrep => ClassXlmGetIrrep
     generic, public :: assignment(=)  => ClassXlmCopyXlm
     ! {{{ 

     procedure, private :: ClassXlmGetl
     procedure, private :: ClassXlmGetm
     procedure, public  :: ClassXlmShow
     procedure, public  :: ClassXlmSave
     procedure, public  :: ClassXlmLoad
     procedure, private :: ClassXlmIs
     procedure, private :: ClassXlmGetLabel
     procedure, private :: ClassXlmGetIrrep
     procedure, private :: ClassXlmCopyXlm

     ! }}}
  end type ClassXlm


  !> Defines the class of the symmetry adapted spherical harmonics set with a definite symmetry.
  type, public :: ClassXlmSymmetricSet
     ! {{{ 

     private
     !> Maximum angular momentum
     integer                     :: Lmax
     !> Number of Symmetric Xlm for each angular momentum
     integer, allocatable        :: dim(:)
     !> Absolute index of the last Xlm with angular momentum 
     !! lower than a given l
     integer, allocatable        :: idim(:)
     !> Irrep class associated to the Xml set.
     type(ClassIrrep), pointer   :: Irrep
     !> Total number of Xlm functions for a given symmetry.
     integer                     :: NumXlm
     !> List of the Xlm functions belonging to the set of definite symmetry.
     type(ClassXlm), allocatable :: XlmList(:)

     ! }}}
   contains
     !
     generic, public :: init      =>  ClassXlmSymmetricSetInit
     generic, public :: GetIrrep  =>  ClassXlmSymmetricSetGetIrrep
     generic, public :: Show      =>  ClassXlmSymmetricSetShow
     generic, public :: GetNXlm   =>  ClassXlmSymmetricSetGetNXlm
     generic, public :: GetXlm    =>  ClassXlmSymmetricSetGetXlm
     generic, public :: GetMList    =>  ClassXlmSymmetricSetGetMList
     generic, public :: ValidXlm  =>  ClassXlmSymmetricSetValidXlm
     generic, public :: GetCartesianExpan => ClassXlmSymmetricSetGetCartesianExpan
     procedure, public :: Free => ClassXlmSymmetricSetFree

     !> Retrieves if the ClassXlmSymmetricSet has been initialized
     procedure         :: Initialized => ClassXlmSymmetricSetInitialized
     ! {{{ 

     procedure, private :: ClassXlmSymmetricSetInit
     procedure, private :: ClassXlmSymmetricSetGetIrrep
     procedure, private :: ClassXlmSymmetricSetShow
     procedure, private :: ClassXlmSymmetricSetGetNXlm
     procedure, private :: ClassXlmSymmetricSetGetXlm
     procedure, private :: ClassXlmSymmetricSetGetMList
     procedure, private :: ClassXlmSymmetricSetValidXlm
     procedure, private :: ClassXlmSymmetricSetGetCartesianExpan
     procedure, private :: Set_DimVectors => ClassXlmSymmetricSetDimension
     !
     procedure, private :: Set_C1_A
     !
     procedure, private :: Set_Cs_Ap
     procedure, private :: Set_Cs_App
     !
     procedure, private :: Set_C2_A
     procedure, private :: Set_C2_B
     !
     procedure, private :: Set_Ci_Ag
     procedure, private :: Set_Ci_Au
     !
     procedure, private :: Set_C2v_A1
     procedure, private :: Set_C2v_A2
     procedure, private :: Set_C2v_B1
     procedure, private :: Set_C2v_B2
     !
     procedure, private :: Set_C2h_Ag
     procedure, private :: Set_C2h_Bg
     procedure, private :: Set_C2h_Au
     procedure, private :: Set_C2h_Bu
     !
     procedure, private :: Set_D2_A
     procedure, private :: Set_D2_B1
     procedure, private :: Set_D2_B2
     procedure, private :: Set_D2_B3
     !
     procedure, private :: Set_D2h_Ag
     procedure, private :: Set_D2h_B1g
     procedure, private :: Set_D2h_B2g
     procedure, private :: Set_D2h_B3g
     procedure, private :: Set_D2h_Au
     procedure, private :: Set_D2h_B1u
     procedure, private :: Set_D2h_B2u
     procedure, private :: Set_D2h_B3u
     !
     final :: ClassXlmSymmetricSetFinal
     ! }}}
  end type ClassXlmSymmetricSet


  type, public :: ClassXlmSet
     ! {{{ 

     private
     logical                             :: Initialized = .false.
     type(ClassGroup)          , pointer :: Group
     !> Maximum angular momentum that can be represented.
     integer                             :: Lmax
     integer                             :: NIrreps
     type(ClassXlmSymmetricSet), pointer :: XlmSymSet(:)

     ! }}}
   contains
     !> Given the global group and the maximum L, initializes 
     !! the Xlm symmetric sets that correspond to all the group's irreps
     procedure, public :: init       => ClassXlmSetInit
     !> Given an irrep of the group, returns a pointer to the
     !! Xlm symmetric set corresponding to that irrep
     procedure, public :: GetSymSet  => ClassXlmSetGetSymSet
     procedure, public :: Show       => ClassXlmSetShow
     !> For a given irreducible representation and an angular momentum retrieve for all te possible Xlm ( r^l*Xlm ), its expansion in terms of the cartessian monomials. 
     procedure, public :: GetCartesianExpan
  end type ClassXlmSet


  !> Defines the class of the monomials associated to the cartesian gaussian functions
  type, private :: ClassMonomial
     ! {{{ 
     !> x exponent for cartesian, n exponent for spherical.
     integer :: ix
     !> y exponent exponent for cartesian, l exponent for spherical.
     integer :: iy
     !> z exponent exponent for cartesian, m exponent for spherical.
     integer :: iz
     ! }}} 
   contains
     !
     !> Initializes the monomial class.
     procedure :: Init => ClassMonomialInit
     !> Fetches the monomial exponents.
     procedure :: FetchMonomialExponents => ClassMonomialFetchMonomialExponents
     generic   :: Matches => ClassMonomialMatches, ClassMonomialClassesMatches
     procedure :: IsInList => ClassMonomialIsInList
     procedure :: Show   =>   ClassMonomialShow
     !
     procedure, private :: ClassMonomialMatches
     procedure, private :: ClassMonomialClassesMatches
     !
  end type ClassMonomial


  !> Defines the class of the cartesian functions set with a definite symmetry. 
  !! It's understood by cartesian functions the pre-exponential factors in the 
  !! cartesian gaussian functions, also known as monomials.
  type, public :: ClassCartesianSymmetricSet
     ! {{{ 

     private
     !
     !> Maximum angular momentum that can be represented.
     integer                      :: Lmax
     !> Irrep class associated to the set of cartesian functions.
     type(ClassIrrep), pointer    :: Irrep
     !> Total number of cartesian functions for a given symmetry.
     integer                      :: NumCart
     !> List of the cartesian functions belonging to the set of definite symmetry.
     type(ClassMonomial), allocatable :: SymMonomialList(:)
     !> List of the cartesian functions used throughout, it can coincide with SymMonomialList or with the values read from the configuration file depending on initialization.
     type(ClassMonomial), allocatable :: MonomialList(:)
     !> Transformation matrix that expresses the cartesian monomials 
     !! in terms of the symmetric adapted spherical harmonics Xlm.
     complex(kind(1d0)), allocatable :: XlmToCartArray(:,:)
     !> Transformation matrix that expresses the symmetric adapted 
     !! spherical harmonics Xlm in terms of the cartesian monomials.
     complex(kind(1d0)), allocatable :: CartToXlmArray(:,:)
     !> Array of indixes, n, l and m in r^n*Xlm stablishing the ordering 
     !! of this function in the matrix transformation from Xlm to cartesian monomials (contains all the possible Xlm functions and not only those allowed by symmetry).this.OperatorLabel
     integer, allocatable :: XlmTransfIndexes(:,:)

     ! }}}
   contains
     !
     !> Retrieves if the class has been initialized
     procedure         :: Initialized => ClassCartesianSymmetricSetInitialized
     !> Initializes the cartesian set with definite symmetry class.
     procedure, public :: Init => ClassCartesianSymmetricSetIndependentInit!, ClassCartesianSymmetricSetInit

     !> Get the index corresponding to a given (ix,iy,iz) set
     generic, public :: GetIndex => ClassCartesianSymmetricSetGetIndex

     !> Get the number of cartesian multipoles
     generic, public :: GetN => ClassCartesianSymmetricSetGetN

     !> Fetches the array of monomial exponents.
     procedure         :: FetchMonomialExponents => ClassCartesianSymmetricSetFetchMonomialExponents

     !> Returns " x  xxx  xyy  xzz " etc.
     procedure         :: FetchMonomialStrn => ClassCartesianSymmetricSetFetchMonomialStrn

     !> Fetches the array of monomial exponents allowed by the symmetry.
     procedure         :: FetchSymMonomialExponents => ClassCartesianSymmetricSetFetchSymMonomialExponents

     !> Shows in a uni the relevant info of the class.
     procedure, public :: Show    =>    ClassCartesianSymmetricSetShow

     !> Fetches the transformation matrix that expresses the cartesian 
     !! monomials in terms of the symmetric adapted spherical harmonics Xlm.
     procedure, public :: FetchXlmToCartesianMatrix

     !> Fetches the transformation matrix that expresses the symmetric 
     !! adapted spherical harmonics Xlm in terms of the cartesian monomials.
     procedure, public :: FetchCartesianToXlmMatrix

     !> Fetches the indexes n, l and m of r^n*Xlm in the order used 
     !! in the transformation to or from cartesian.
     procedure, public :: FetchXlmTransfIndexes
     
     !> Computes the transformation matrices between the cartesians and the Xlm ( in both directions ) belonging to the irrep of the class.
     procedure, public :: ComputeTransfMat
     procedure, public :: Free => ClassCartesianSymmetricSetFree

     ! {{{ 

!!$     procedure, private :: ClassCartesianSymmetricSetInit
     procedure, private :: ClassCartesianSymmetricSetGetIndex
     procedure, private :: ClassCartesianSymmetricSetGetN
     procedure, private :: ClassCartesianSymmetricSetIndependentInit
     procedure, private :: SetXlmToCartArray
     procedure, private :: SetCartToXlmArray
     procedure, private :: ClassCartesianSymmetricSetFree
     final              :: ClassCartesianSymmetricSetFinal

     ! }}} 
  end type ClassCartesianSymmetricSet




  !> Defines the class of the spherical functions that are the
  !!prefactor in the spherical Gaussian set, with a definite 
  !!symmetry: \f$r^{n}Y_{\ell,m}\f$.
  
  type, public :: ClassSpherSymmetricSet
     ! {{{ 

     private
     !
     !> Maximum angular momentum that can be represented.
     integer                      :: Lmax
     !> Irrep class associated to the set of cartesian functions.
     type(ClassIrrep), pointer    :: Irrep
     !> Symmetry adapted spherical harmonics set corresponding
     !! to the provided irreducible representation.
     type(ClassXlmSymmetricSet)   :: XlmSymmetricSet
     !> Total number of spherical functions for a given symmetry.
     integer                      :: NumSpher
     !> List of the spherical functions belonging to the set of definite symmetry.
     type(ClassMonomial), allocatable :: SymMonomialList(:)
     !> List of the spherical functions used through out, it can coincide with SymMonomialList or with the values read from the configuration file depending on initialization.
     type(ClassMonomial),  allocatable :: MonomialList(:)
     !> Transformation matrix that expresses the spherical monomials 
     !! in terms of the symmetric adapted spherical harmonics Xlm.
     complex(kind(1d0)), allocatable :: XlmToSpherArray(:,:)
     !> Transformation matrix that expresses the symmetric adapted 
     !! spherical harmonics Xlm in terms of the spherical monomials.
     complex(kind(1d0)), allocatable :: SpherToXlmArray(:,:)
     !> Array of indixes, n, l and m in r^n*Xlm stablishing the ordering 
     !! of this function in the matrix transformation from Xlm to spherical monomials (contains all the possible Xlm functions and not only those allowed by symmetry).
     integer, allocatable :: XlmTransfIndexes(:,:)

     ! }}}
   contains
     !
     !> Retrieves if the class has been initialized
     procedure         :: Initialized => ClassSpherSymmetricSetInitialized
     !> Initializes the cartesian set with definite symmetry class.
     procedure, public :: Init => ClassSpherSymmetricSetIndependentInit

     !> Get the index corresponding to a given (n,l,m) set
     generic, public :: GetIndex => ClassSpherSymmetricSetGetIndex

     !> Get the number of spherical multipoles
     generic, public :: GetN => ClassSpherSymmetricSetGetN

     !> Fetches the array of monomial exponents.
     procedure         :: FetchMonomialExponents => ClassSpherSymmetricSetFetchMonomialExponents

     !> Fetches the array of monomial exponents allowed by the symmetry.
     procedure         :: FetchSymMonomialExponents => ClassSpherSymmetricSetFetchSymMonomialExponents

     !> Shows in a uni the relevant info of the class.
     procedure, public :: Show    =>    ClassSpherSymmetricSetShow

     !> Fetches the transformation matrix that expresses the spherical 
     !! monomials in terms of the symmetric adapted spherical harmonics Xlm.
     procedure, public :: FetchXlmToSpherMatrix

     !> Fetches the transformation matrix that expresses the symmetric 
     !! adapted spherical harmonics Xlm in terms of the spherical monomials.
     procedure, public :: FetchSpherToXlmMatrix

     !> Fetches the indexes n, l and m of r^n*Xlm in the order used 
     !! in the transformation to or from spherical.
     procedure, public :: SpherFetchXlmTransfIndexes
     
     !> Computes the transformation matrices between the spherical and the Xlm ( in both directions ) belonging to the irrep of the class.
     procedure, public :: SpherComputeTransfMat
     procedure, public :: Free => ClassSpherSymmetricSetFree

     ! {{{ 

!!$     procedure, private :: ClassCartesianSymmetricSetInit
     procedure, private :: ClassSpherSymmetricSetGetIndex
     procedure, private :: ClassSpherSymmetricSetGetN
     procedure, private :: ClassSpherSymmetricSetIndependentInit
     procedure, private :: SetXlmToSpherArray
     procedure, private :: SetSpherToXlmArray
     procedure, private :: ClassSpherSymmetricSetFree
     final              :: ClassSpherSymmetricSetFinal

     ! }}} 
  end type ClassSpherSymmetricSet


  public :: TripleXlmIntegral
  private :: delta
  private :: signm
  private :: Ylm_Xlm_Overlap_Factor1

contains


  !> Initializes the monomial class.
  subroutine ClassMonomialInit( Mon, i, j, k )
    !
    !> Class of monomials.
    class(ClassMonomial), intent(inout) :: Mon
    !> x exponent.
    integer,              intent(in)    :: i
    !> y exponent.
    integer,              intent(in)    :: j
    !> z exponent.
    integer,              intent(in)    :: k
    !
    Mon%ix = i
    Mon%iy = j
    Mon%iz = k
    !
  end subroutine ClassMonomialInit
  


  !> Initializes the Xlm class.
  subroutine ClassXlmInit( Xlm, l, m )
    !
    !> Class of Xlm with definite l and m
    class (ClassXlm),intent(inout) :: Xlm
    !> Angular momentum.
    integer,         intent(in)    :: l
    !> Angular momentum projection.
    integer,         intent(in)    :: m
    !
    Xlm%l = l
    Xlm%m = m
    !
  end subroutine ClassXlmInit


  !> Tell to which irrep the Xlm belong
  function ClassXlmGetIrrep( Xlm, Group ) result( irrep )
    !
    !> Class of Xlm with definite l and m
    class(ClassXlm)  , intent(in) :: Xlm
    !> input group
    class(ClassGroup), intent(in) :: Group
    class(ClassIrrep), pointer    :: irrep
    !
    type(ClassXlmSymmetricSet)   :: XlmSymSet
    type(ClassIrrep), pointer    :: irrepv(:)
    integer :: iIrrep
    !
    irrepv => Group%GetIrrepList()
    do iIrrep = 1, size(irrepv)
       call XlmSymSet%init( Xlm%GetL(), irrepv( iIrrep ))
       if( XlmSymSet%ValidXlm( Xlm ))then
          irrep => irrepv( iIrrep )
          return
       endif
    enddo
    !
  end function ClassXlmGetIrrep


  !> Copies the information of Xlm class to another.
  subroutine ClassXlmCopyXlm( XlmOut, XlmIn )
    !> Class of Xlm to be assigned.
    class(ClassXlm)  , intent(inout) :: XlmOut
    class(ClassXlm)  , intent(in)    :: XlmIn
    !
    XlmOut%l = XlmIn%l
    XlmOut%m = XlmIn%m
    !
  end subroutine ClassXlmCopyXlm


  subroutine ClassXlmSetInit( XlmSet, Group, Lmax )
    !
    class(ClassXlmSet),         intent(out) :: XlmSet
    class(ClassGroup) , target, intent(in)  :: Group
    integer           ,         intent(in)  :: Lmax
    integer :: iIrrep
    type(ClassIrrep), pointer, dimension(:) :: irrepv
    !
    XlmSet%Group => Group
    XlmSet%Lmax  =  Lmax

    irrepv => Group%GetIrrepList()
    XlmSet%NIrreps = size( irrepv )
    allocate( XlmSet%XlmSymSet( XlmSet%NIrreps ) )
    do iIrrep = 1, size( irrepv )
       call XlmSet%XlmSymSet( iIrrep )%init( Lmax, irrepv( iIrrep ) )
    enddo
    XlmSet%Initialized = .true.

  end subroutine ClassXlmSetInit



  subroutine ClassXlmSymmetricSetInit( XlmSym, Lmax, IrrepPtr )
    class(ClassXlmSymmetricSet), intent(out):: XlmSym
    integer                    , intent(in) :: Lmax
    type(ClassIrrep)           , intent(in) :: irrepPtr
    !
    character(len=16) :: IdStrn
    
    IdStrn = IrrepPtr%GetIdStrn()

    if( IdStrn .is. "C1.A"    ) call XlmSym%Set_C1_A   ( LMax, irrepPtr )
    if( IdStrn .is. "Cs.Ap"   ) call XlmSym%Set_Cs_Ap  ( LMax, irrepPtr )
    if( IdStrn .is. "Cs.App"  ) call XlmSym%Set_Cs_App ( LMax, irrepPtr )
    if( IdStrn .is. "C2.A"    ) call XlmSym%Set_C2_A   ( LMax, irrepPtr )
    if( IdStrn .is. "C2.B"    ) call XlmSym%Set_C2_B   ( LMax, irrepPtr )
    if( IdStrn .is. "Ci.Ag"   ) call XlmSym%Set_Ci_Ag  ( LMax, irrepPtr )
    if( IdStrn .is. "Ci.Au"   ) call XlmSym%Set_Ci_Au  ( LMax, irrepPtr )
    if( IdStrn .is. "C2v.A1"  ) call XlmSym%Set_C2v_A1 ( LMax, irrepPtr )
    if( IdStrn .is. "C2v.B1"  ) call XlmSym%Set_C2v_B1 ( LMax, irrepPtr )
    if( IdStrn .is. "C2v.A2"  ) call XlmSym%Set_C2v_A2 ( LMax, irrepPtr )
    if( IdStrn .is. "C2v.B2"  ) call XlmSym%Set_C2v_B2 ( LMax, irrepPtr )
    if( IdStrn .is. "C2h.Ag"  ) call XlmSym%Set_C2h_Ag ( LMax, irrepPtr )
    if( IdStrn .is. "C2h.Bg"  ) call XlmSym%Set_C2h_Bg ( LMax, irrepPtr )
    if( IdStrn .is. "C2h.Au"  ) call XlmSym%Set_C2h_Au ( LMax, irrepPtr )
    if( IdStrn .is. "C2h.Bu"  ) call XlmSym%Set_C2h_Bu ( LMax, irrepPtr )
    if( IdStrn .is. "D2.A"    ) call XlmSym%Set_D2_A   ( LMax, irrepPtr )
    if( IdStrn .is. "D2.B1"   ) call XlmSym%Set_D2_B1  ( LMax, irrepPtr )
    if( IdStrn .is. "D2.B2"   ) call XlmSym%Set_D2_B2  ( LMax, irrepPtr )
    if( IdStrn .is. "D2.B3"   ) call XlmSym%Set_D2_B3  ( LMax, irrepPtr )
    if( IdStrn .is. "D2h.Ag"  ) call XlmSym%Set_D2h_Ag ( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B1g" ) call XlmSym%Set_D2h_B1g( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B2g" ) call XlmSym%Set_D2h_B2g( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B3g" ) call XlmSym%Set_D2h_B3g( LMax, irrepPtr )
    if( IdStrn .is. "D2h.Au"  ) call XlmSym%Set_D2h_Au ( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B1u" ) call XlmSym%Set_D2h_B1u( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B2u" ) call XlmSym%Set_D2h_B2u( LMax, irrepPtr )
    if( IdStrn .is. "D2h.B3u" ) call XlmSym%Set_D2h_B3u( LMax, irrepPtr )

    call XlmSym%Set_DimVectors()

  end subroutine ClassXlmSymmetricSetInit



  subroutine Set_C1_A( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + 2*l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m= -l, l
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C1_A


  subroutine Set_Cs_Ap( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    XlmSym%Lmax = LMax
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m= -l, l
          if ( mod(l+m,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_Cs_Ap


  subroutine Set_Cs_App( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + l
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m= -l, l
          if ( mod(l+m+1,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_Cs_App




  subroutine Set_C2_A( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + 2*floor(dble(l/2))+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m= -l, l
          if ( mod(m,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_C2_A



  subroutine Set_C2_B( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + 2*floor(dble((l+1)/2))
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m= -l, l
          if ( mod(m+1,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_C2_B




  subroutine Set_Ci_Ag( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + 2*l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax, 2
       do m = -l, l
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_Ci_Ag



  subroutine Set_Ci_Au( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + 2*l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 1, lmax, 2
       do m = -l, l
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_Ci_Au



  subroutine Set_C2v_A1( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + floor(dble(l/2))+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = 0, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2v_A1



  subroutine Set_C2v_A2( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + floor(dble(l/2))
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, -2
          if ( mod(-m,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_C2v_A2



  subroutine Set_C2v_B1( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + floor(dble((l+1)/2))
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = 1, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2v_B1



  subroutine Set_C2v_B2( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax
       nXlm = nXlm + floor(dble((l+1)/2))
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, -1
          if ( mod(-m+1,2) == 0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_C2v_B2



  subroutine Set_C2h_Ag( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax, 2
       do m = -l, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2h_Ag



  subroutine Set_C2h_Bg( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax, 2
       do m = -l+1, l-1, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2h_Bg



  subroutine Set_C2h_Au( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + l
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 1, lmax, 2
       do m = -l+1, l-1, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2h_Au



  subroutine Set_C2h_Bu( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + l+1
    enddo 
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 1, lmax, 2
       do m = -l, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_C2h_Bu



  subroutine Set_D2_A( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    !
    do l = 0, lmax, 2
       nXlm = nXlm + l/2 + 1
    enddo 
    do l = 1, lmax, 2
       nXlm = nXlm + (l-1)/2
    enddo 
    !
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, l
          if ( mod(l,2)==0 .and. mod(m,2)==0 .and. m>=0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          elseif ( mod(l+1,2)==0 .and. mod(m,2)==0 .and. m<0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_D2_A



  subroutine Set_D2_B1( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    !
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo 
    do l = 1, lmax, 2
       nXlm = nXlm + (l+1)/2
    enddo 
    !
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, l
          if ( mod(l,2)==0 .and. mod(m,2)==0 .and. m<0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          elseif ( mod(l+1,2)==0 .and. mod(m,2)==0 .and. m>=0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_D2_B1



  subroutine Set_D2_B2( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    !
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo 
    do l = 1, lmax, 2
       nXlm = nXlm + (l+1)/2
    enddo 
    !
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, l
          if ( mod(l,2)==0 .and. mod(m+1,2)==0 .and. m>=0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          elseif ( mod(l+1,2)==0 .and. mod(m+1,2)==0 .and. m<0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_D2_B2



  subroutine Set_D2_B3( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    !
    XlmSym%Irrep => Irrep
    nXlm = 0
    !
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo 
    do l = 1, lmax, 2
       nXlm = nXlm + (l+1)/2
    enddo 
    !
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) ) 
    nXlm = 0
    do l = 0, lmax
       do m = -l, l
          if ( mod(l+1,2)==0 .and. mod(m+1,2)==0 .and. m>=0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          elseif ( mod(l,2)==0 .and. mod(m+1,2)==0 .and. m<0 ) then
             nXlm = nXlm + 1
             call XlmSym%XlmList( nXlm )%init( l, m )
          end if
       enddo
    enddo
  end subroutine Set_D2_B3



  subroutine Set_D2h_Ag( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l/2 + 1
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 0, lmax, 2
       do m = 0, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_Ag



  subroutine Set_D2h_B1g( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 0, lmax, 2
       do m = -l, -2, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B1g



  subroutine Set_D2h_B2g( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 0, lmax, 2
       do m = 1, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B2g



  subroutine Set_D2h_B3g( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 0, lmax, 2
       nXlm = nXlm + l/2
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 0, lmax, 2
       do m = -l+1, -1, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B3g



  subroutine Set_D2h_Au( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + (l-1)/2
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 1, lmax, 2
       do m = -l+1, -2, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_Au



  subroutine Set_D2h_B1u( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + ( l + 1 ) / 2 
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 1, lmax, 2
       do m = 0, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B1u



  subroutine Set_D2h_B2u( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + ( l + 1 ) / 2 
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 1, lmax, 2
       do m = -l, -1, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B2u



  subroutine Set_D2h_B3u( XlmSym, Lmax, Irrep )
    class( ClassXlmSymmetricSet ), intent(inout) :: XlmSym
    integer                      , intent(in)    :: Lmax
    class( ClassIrrep ), target  , intent(in)    :: Irrep
    !
    integer :: l,m,nXlm
    XlmSym%Irrep => Irrep
    nXlm = 0
    do l = 1, lmax, 2
       nXlm = nXlm + ( l + 1 ) / 2 
    enddo
    XlmSym%NumXlm = nXlm
    allocate( XlmSym%XlmList( nXlm ) )
    nXlm = 0
    do l = 1, lmax, 2
       do m = 1, l, 2
          nXlm = nXlm + 1
          call XlmSym%XlmList( nXlm )%init( l, m )
       enddo
    enddo
  end subroutine Set_D2h_B3u


  function ClassXlmGetl( Xlm ) result( l )
    class(ClassXlm), intent(in) :: Xlm
    integer :: l
    l = Xlm%l
  end function ClassXlmGetl


  function ClassXlmGetm( Xlm ) result( m )
    class(ClassXlm), intent(in) :: Xlm
    integer :: m
    m = Xlm%m
  end function ClassXlmGetm


  subroutine ClassXlmSymmetricSetDimension( XlmSym )
    class(ClassXlmSymmetricSet), intent(inout) :: XlmSym
    integer :: iXlm, lmax, il
    !
    if( XlmSym%NumXlm > 0 ) then
       lmax = XlmSym%XlmList( XlmSym%NumXlm )%Getl()
       XlmSym%Lmax = lmax
    else
       call ErrorMessage( "The irreducible representation "//XlmSym%Irrep%GetName()//&
         " does not have Xlm functions for the given maximum angular momentum." )
       return
    end if

    if(allocated(XlmSym%dim))deallocate(XlmSym%dim)
    allocate(XlmSym%dim(0:lmax))
    XlmSym%dim = 0
    do iXlm = 1, XlmSym%NumXlm
       il = XlmSym%XlmList( iXlm )%Getl()
       XlmSym%dim( il ) = XlmSym%dim( il ) + 1
    enddo
    if(allocated(XlmSym%idim))deallocate(XlmSym%idim)
    allocate(XlmSym%idim(0:lmax+1))
    XlmSym%idim = 0
    do il = 0, lmax
       XlmSym%idim(il+1)=XlmSym%idim(il)+XlmSym%dim(il)
    enddo
  end subroutine ClassXlmSymmetricSetDimension


!!$  !> Initializes the cartesian set with definite symmetry class, although all the cartesian monomial indexes belonging to the point group are passed in increasing semi-monotonic way.
!!$  subroutine ClassCartesianSymmetricSetInit( CartSet, Lmax, Irrep, MonomialExp )
!!$    !
!!$    !> Class of the cartesian set with a definite symmetry.
!!$    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
!!$    !> Maximum angular momentum that can be represented.
!!$    integer,                     intent(in)    :: Lmax
!!$    !> Irrep class associated to the cartesian set.
!!$    class(ClassIrrep), target,   intent(in)    :: Irrep
!!$    !> Possible monomial exponents fetched from the interface info: Rows for ix, iy, iz, columns for the cartesian functions.
!!$    integer,                  intent(in)    :: MonomialExp(:,:)
!!$    !
!!$    integer :: i
!!$    integer, allocatable :: FinalMonExp(:,:)
!!$    complex(kind(1d0)), allocatable :: XlmToCartArray(:,:), CartToXlmArray(:,:)
!!$    !
!!$    CartSet%Lmax = Lmax
!!$    !
!!$    CartSet%Irrep => Irrep
!!$    !
!!$    allocate( FinalMonExp, source = MonomialExp )
!!$    !
!!$    CartSet%NumCart = size(FinalMonExp,2)
!!$    !
!!$    allocate( CartSet%MonomialList(CartSet%NumCart) )
!!$    !
!!$    do i = 1, CartSet%NumCart
!!$       !
!!$       call CartSet%MonomialList(i)%Init( &
!!$            FinalMonExp(1,i), &
!!$            FinalMonExp(2,i), &
!!$            FinalMonExp(3,i) )
!!$       !
!!$    end do
!!$    !
!!$    call BuildTransMatfromXlmToCart( MonomialExp, CartSet%XlmTransfIndexes, XlmToCartArray )
!!$    !
!!$    !
!!$    call BuildTransMatfromCartToXlm( XlmToCartArray, CartToXlmArray)
!!$    !
!!$  end subroutine ClassCartesianSymmetricSetInit



  !> Get the index corresponding to a specified x^ix y^iy z^iz monomial.
  !! If the monomial does not belong to the Cartesian Symmetric group,
  !! returns 0.
  function ClassCartesianSymmetricSetGetIndex( CartSet, ix, iy, iz ) result( iMult )
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    integer,                           intent(in)    :: ix, iy, iz
    integer                                          :: iMult
    iMult=CartSet%NumCart
    do 
       if(iMult==0)exit
       if(CartSet%MonomialList(iMult)%matches(ix,iy,iz))exit
       iMult=iMult-1
    enddo
  end function ClassCartesianSymmetricSetGetIndex


  !> Get the index corresponding to a specified r^nYlm  spherical monomial.
  !! If the monomial does not belong to the Spherical Symmetric group,
  !! returns 0.
  function ClassSpherSymmetricSetGetIndex( SpherSet, n, l, m ) result( iMult )
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    integer,                       intent(in)    :: n, l, m
    integer                                      :: iMult
    iMult=SpherSet%NumSpher
    do 
       if(iMult==0)exit
       if(SpherSet%MonomialList(iMult)%matches(n,l,m))exit
       iMult=iMult-1
    enddo
  end function ClassSpherSymmetricSetGetIndex

  

  !> Return the number of current cartesian symmetric monomials
  function ClassCartesianSymmetricSetGetN( CartSet ) result( NCart )
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    integer                                          :: NCart
    NCart=CartSet%NumCart
  end function ClassCartesianSymmetricSetGetN


  !> Return the number of current spherical symmetric monomials
  function ClassSpherSymmetricSetGetN( SpherSet ) result( NSpher )
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    integer                                      :: NSpher
    NSpher=SpherSet%NumSpher
  end function ClassSpherSymmetricSetGetN
  


  !> Initializes the cartesian set with definite symmetry class.
  subroutine ClassCartesianSymmetricSetIndependentInit( CartSet, Lmax, Irrep, ExternalMonExp )
    !
    !> Class of the cartesian set with a definite symmetry.
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    !> Maximum angular momentum that can be represented.
    integer,                     intent(in)    :: Lmax
    !> Irrep class associated to the cartesian set.
    class(ClassIrrep), target,   intent(in)    :: Irrep
    !> External array of monomial exponents.
    integer, optional,           intent(in)    :: ExternalMonExp(:,:)
    !
    integer :: ix, iy, iz, il, NSymCart, i
    !
    type(ClassGroup), pointer :: Group
    type(ClassIrrep), pointer :: IrrepPtr
    !
    CartSet%Lmax = Lmax
    CartSet%Irrep => Irrep
    Group => Irrep%GetGroup()
    !
    !.. Count the number of monomial exponents which are compatible
    !   with the current irrep
    !..
    NSymCart = 0
    do il = 0, lmax
       do ix=il,0,-1
          do iy=il-ix,0,-1
             iz=il-ix-iy
             !
             IrrepPtr => Group%GetMonomialIrrep(ix,iy,iz)
             if( irrepPtr%GetName() .isnt. irrep%GetName() )cycle
             NSymCart = NSymCart + 1
             !
          enddo
       enddo
    enddo
    !
    !.. Store the monomial exponents which are compatible with the current irrep
    CartSet%NumCart = NSymCart
    if ( allocated(CartSet%SymMonomialList) ) deallocate( CartSet%SymMonomialList )
    if ( allocated(CartSet%MonomialList) ) deallocate( CartSet%MonomialList )
    !
    allocate( CartSet%SymMonomialList( CartSet%NumCart ) )
    !
    if ( .not. present(ExternalMonExp) ) then
       allocate( CartSet%MonomialList( CartSet%NumCart ) )
    end if
    !
    NSymCart = 0
    do il = 0, lmax
       do ix=il,0,-1
          do iy=il-ix,0,-1
             iz=il-ix-iy
             !
             IrrepPtr => Group%GetMonomialIrrep(ix,iy,iz)
             if( irrepPtr%GetName() .isnt. irrep%GetName() )cycle
             NSymCart = NSymCart + 1
             call CartSet%SymMonomialList( NSymCart )%init( ix, iy, iz )
             if ( .not. present(ExternalMonExp) ) then
                call CartSet%MonomialList( NSymCart )%init( ix, iy, iz )
             end if
             !
          enddo
       enddo
    enddo
    !
    !
    if ( present(ExternalMonExp) ) then
       !
       CartSet%NumCart = size(ExternalMonExp,2)
       allocate( CartSet%MonomialList( CartSet%NumCart ) )
       do i = 1, CartSet%NumCart
          call CartSet%MonomialList(i)%init( ExternalMonExp(1,i), &
               ExternalMonExp(2,i), &
               ExternalMonExp(3,i) )
       end do
       !
       call CheckMonomialList( CartSet%MonomialList, CartSet%SymMonomialList )
       !
    end if
    !
  end subroutine ClassCartesianSymmetricSetIndependentInit




  !> Initializes the spherical set with definite symmetry class.
  subroutine ClassSpherSymmetricSetIndependentInit( SpherSet, Lmax, Irrep, ExternalMonExp )
    !
    !> Class of the cartesian set with a definite symmetry.
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    !> Maximum angular momentum that can be represented.
    integer,                     intent(in)    :: Lmax
    !> Irrep class associated to the cartesian set.
    class(ClassIrrep), target,   intent(in)    :: Irrep
    !> External array of monomial exponents.
    integer, optional,           intent(in)    :: ExternalMonExp(:,:)
    !
    integer :: n, l, m, NSymSpher, i
    !
    type(ClassGroup), pointer :: Group
    type(ClassIrrep), pointer :: IrrepPtr
    type(ClassXlm) :: Xlm1, Xlm2
    !
    SpherSet%Lmax = Lmax
    SpherSet%Irrep => Irrep
    Group => Irrep%GetGroup()
    !
    call SpherSet%XlmSymmetricSet%Init( Lmax, Irrep )
    !
    !.. Count the number of monomial exponents which are compatible
    !   with the current irrep
    !..
    NSymSpher = 0
    do n = 0, Lmax
       do l = 0, n
          if (mod(n-l,2) == 0 ) then
             do m = -l, l
                !
                call Xlm1%Init( l, m )
                if ( SpherSet%XlmSymmetricSet%ValidXlm( Xlm1 ) ) then

                   NSymSpher = NSymSpher + 1
                end if
                !
             enddo
          end if
       enddo
    enddo
    !
    !.. Store the monomial exponents which are compatible with the current irrep
    SpherSet%NumSpher = NSymSpher
    if ( allocated(SpherSet%SymMonomialList) ) deallocate( SpherSet%SymMonomialList )
    if ( allocated(SpherSet%MonomialList) ) deallocate( SpherSet%MonomialList )
    !
    allocate( SpherSet%SymMonomialList( SpherSet%NumSpher ) )
    !
    if ( .not. present(ExternalMonExp) ) then
       allocate( SpherSet%MonomialList( SpherSet%NumSpher ) )
    end if
    !
    NSymSpher = 0
    do n = 0, Lmax
       do l = 0, n
          if (mod(n-l,2) == 0 ) then
             do m  = -l, l
                !
                call Xlm1%Init( l, m )
                if ( SpherSet%XlmSymmetricSet%ValidXlm( Xlm1 ) ) then
                   !
                   NSymSpher = NSymSpher + 1
                   !
                   call SpherSet%SymMonomialList( NSymSpher )%init( n, l, m )
                   if ( .not. present(ExternalMonExp) ) then
                      call SpherSet%MonomialList( NSymSpher )%init( n, l, m )
                   end if
                end if
                !
             enddo
          end if
       enddo
    enddo
    !
    !
    if ( present(ExternalMonExp) ) then
       !
       SpherSet%NumSpher = size(ExternalMonExp,2)
       allocate( SpherSet%MonomialList( SpherSet%NumSpher ) )
       do i = 1, SpherSet%NumSpher
          call SpherSet%MonomialList(i)%init( ExternalMonExp(1,i), &
               ExternalMonExp(2,i), &
               ExternalMonExp(3,i) )
       end do
       !
       call CheckMonomialList( SpherSet%MonomialList, SpherSet%SymMonomialList )
       !
    end if
    !
  end subroutine ClassSpherSymmetricSetIndependentInit


  

  !> Retrieves if the ClassCartesianSymmetricSet has been initialized.
  logical function  ClassCartesianSymmetricSetInitialized( CartSet ) result( Initialized )
    !
    !> Class of the cartesian set with a definite symmetry.
    class(ClassCartesianSymmetricSet), intent(in) :: CartSet
    !
    if ( allocated( CartSet%MonomialList ) ) then
       !
       Initialized = .true.
       !
    else
       !
       Initialized = .false.
       !
    end if
    !
  end function ClassCartesianSymmetricSetInitialized




  !> Retrieves if the ClassSpherSymmetricSet has been initialized.
  logical function  ClassSpherSymmetricSetInitialized( SpherSet ) result( Initialized )
    !
    !> Class of the spherical set with a definite symmetry.
    class(ClassSpherSymmetricSet), intent(in) :: SpherSet
    !
    if ( allocated( SpherSet%MonomialList ) ) then
       !
       Initialized = .true.
       !
    else
       !
       Initialized = .false.
       !
    end if
    !
  end function ClassSpherSymmetricSetInitialized




     !> Retrieves if the ClassXlmSymmetricSet has been initialized
  logical function  ClassXlmSymmetricSetInitialized( XlmSet ) result( Initialized )
    !
    !> Class of the Xlm set with a definite symmetry.
    class(ClassXlmSymmetricSet), intent(in) :: XlmSet
    !
    if ( allocated( XlmSet%XlmList ) ) then
       !
       Initialized = .true.
       !
    else
       !
       Initialized = .false.
       !
    end if
    !
  end function ClassXlmSymmetricSetInitialized



  !> Computes the angular integral 
  !! \f[
  !!     \langle X_{\ell_{Bra}m_{Bra}}|X_{\ell_{Op}m_{Op}}|X_{\ell_{Ket}m_{Ket}}\rangle
  !! \f]
  real(kind(1d0)) function TripleXlmIntegral(LBra,MBra,LOp,MOp,LKet,MKet) result( Res )
    !
    integer, intent(in) :: LBra,MBra,LOp,MOp,LKet,MKet
    !
    complex(kind(1d0)) :: FactMult
    real(kind(1d0))    :: SignFactorMBra, SignFactorMOp, SignFactorMKet, SignFactor, FactMult2
    !
    Res = 0.d0
    !
!!$    if( LOp == 0 )then
!!$       if( LBra==LKet .and. MBra==MKet .and. MOp == 0 ) Res = 1.d0 / sqrt(4*PI)
!!$       return
!!$    endif
!!$
!!$    if( abs(Lop-LBra) <= LKet .and. (Lop+LBra) >= LKet .and. mod(Lop+LBra+LKet,2)== 0 .and. &
!!$         ( &
!!$         ( MBra ==  MKet + MOp ) .or. &
!!$         ( MBra ==  MKet - MOp ) .or. &
!!$         ( MBra == -MKet + MOp ) .or. &
!!$         ( MBra == -MKet - MOp ) ) ) Res=1/sqrt(4*PI)
         
    FactMult = Ylm_Xlm_Overlap_Factor1( MKet ) * &
         Ylm_Xlm_Overlap_Factor1( MOp ) * &
         conjg(Ylm_Xlm_Overlap_Factor1( MBra ))
    !
    FactMult2 = sqrt(dble((2*LKet+1)*(2*LOp+1))/(4.d0*PI*dble(2*LBra+1))) * CGC(LKet,LOp,LBra,0,0)
    !
    SignFactorMBra = signm(MBra)*dble((-1)**MBra)
    SignFactorMOp  = signm(MOp) *dble((-1)**MOp)
    SignFactorMKet = signm(MKet)*dble((-1)**MKet)
    !
    SignFactor = dble((-1)**(LKet+LOp-LBra))
    !
    Res = ( CGC(LKet,LOp,LBra,abs(MKet),abs(MOp))*delta(abs(MBra),abs(MKet)+abs(MOp)) * &
         (1.d0 + SignFactor*SignFactorMBra*SignFactorMOp*SignFactorMKet) + &
         CGC(LKet,LOp,LBra,abs(MKet),abs(MOp))*delta(-abs(MBra),abs(MKet)+abs(MOp)) * &
         (SignFactorMBra + SignFactor*SignFactorMKet*SignFactorMOp) + &
         CGC(LKet,LOp,LBra,abs(MKet),-abs(MOp))*delta(abs(MBra),abs(MKet)-abs(MOp)) * &
         (SignFactorMOp + SignFactor*SignFactorMBra*SignFactorMKet) + &
         CGC(LKet,LOp,LBra,-abs(MKet),abs(MOp))*delta(abs(MBra),-abs(MKet)+abs(MOp)) * &
         (SignFactorMKet + SignFactor*SignFactorMBra*SignFactorMOp) )
    !
    if ( abs(aimag(FactMult))>1.d-12 .and. abs(Res)>1.d-12 ) then
       write(output_unit,*) "Complex factor: ", FactMult
       write(output_unit,*) "Clebsh Gordan part: ", Res
       call Assert( "The triple integral of Xlm has to be real." )
    else
       Res = Res * dble(FactMult) * FactMult2
!!$write(output_unit,*) "triple",lbra,mbra,lket,mket,lop,mop,FactMult2,Res
    end if
    !
  end function TripleXlmIntegral



!!$  real(kind(1d0)) function TripleXlmYlmXlmIntegral(LBra,MBra,LOp,MOp,LKet,MKet) result( Res )
!!$
!!$
!!$  end function TripleXlmYlmXlmIntegral


  complex(kind(1d0)) function Ylm_Xlm_Overlap_Factor1( m ) result( zres )
    integer, intent(in) :: m
    if( m < 0 )then
       zres = -Zi / sqrt(2.d0)
    else if( m == 0 )then
       zres = Z1
    else !(m>0)
       zres = Z1 / sqrt(2.d0)
    endif
  end function Ylm_Xlm_Overlap_Factor1


  real(kind(1d0)) function Inv_YXlm_Overlap_Factor1( m ) result( res )
    integer, intent(in) :: m
    if( m < 0 )then
       res = dble((-1)**m) / sqrt(2.d0)
    else if( m == 0 )then
       res = 1.d0
    else !(m>0)
       res = 1.d0 / sqrt(2.d0)
    endif
  end function Inv_YXlm_Overlap_Factor1


  real(kind(1d0)) function delta( x, y ) result(d)
    !
    integer, intent(in) :: x, y
    !
    if ( x == y ) then
       d = 1.d0
    else
       d = 0.d0
    end if
    !
  end function delta



  real(kind(1d0)) function signm( m )
    integer, intent(in) :: m
    if ( m == 0 ) then
       signm = 0.d0
    elseif ( m > 0 ) then
       signm = 1.d0
    else
       signm = -1.d0
    end if
  end function signm


  !> Builds the transformtaion matrix M, such that a vector of cartesian monomials C is expressed in terms of a vector of Xlm symmetry adapted spherical harmonics X through: C = M x X.
  subroutine BuildTransMatfromXlmToCart( MonExponents, SpherHarmIndexes, TransMat, FromSpherToCart, FromXlmToSpher )
    !
    !> All the  monomials indexes coresponding to a definite group.
    integer,                                    intent(in)  :: MonExponents(:,:)
    integer,            allocatable,            intent(out) :: SpherHarmIndexes(:,:)
    complex(kind(1d0)), allocatable,            intent(out) :: TransMat(:,:)
    complex(kind(1d0)), allocatable,  optional, intent(out) :: FromSpherToCart(:,:)
    complex(kind(1d0)), allocatable,  optional, intent(out) :: FromXlmToSpher(:,:)
    !> If this array is a matrix A, and the vectors C and S represent the cartesian and spherical harmonics functions respectively then C = A x S
    complex(kind(1d0)), allocatable :: TrasMatSpherHarmToCart(:,:)
    !> If this array is a matrix B, and de vectors S and X represent the spherical harmonics and Xlm functions respectively then S = B x X. So considering the comment and the others above: M = A x B. 
    complex(kind(1d0)), allocatable :: TrasMatXlmToSpherHarm(:,:)
    !> Array for the spherical harmonics indexes n,l,m in r^{n} Y_{lm} in the rows, and in the columns the different functions.
    type(ClassComplexMatrix) :: MatTransMat, MatTrasMatXlmToSpherHarm
    !
    call CartSpherConversionMatrix( MonExponents, TrasMatSpherHarmToCart, SpherHarmIndexes )
    !
    if ( present(FromSpherToCart) ) then
       allocate( FromSpherToCart, source = TrasMatSpherHarmToCart )
    end if
    !
    call SpherXlmConversionMatrix( SpherHarmIndexes, TrasMatXlmToSpherHarm )
    !
    if ( present(FromXlmToSpher) ) then
       allocate( FromXlmToSpher, source = TrasMatXlmToSpherHarm )
    end if
    !
    MatTransMat = TrasMatSpherHarmToCart
    MatTrasMatXlmToSpherHarm = TrasMatXlmToSpherHarm
    call MatTransMat%Multiply( MatTrasMatXlmToSpherHarm, 'Right', 'N' )
    call MatTransMat%FetchMatrix( TransMat )
    !
    !.. The multiplication in this way is dependent on the virtual memory assigned to matmul.
!!$    allocate( TransMat(size(TrasMatSpherHarmToCart,1), size(TrasMatXlmToSpherHarm,2)) )
!!$    TransMat = matmul( TrasMatSpherHarmToCart, TrasMatXlmToSpherHarm )
    !
  end subroutine BuildTransMatfromXlmToCart



  !> Builds the transformation matrix M, such that a vector of spherical monomials C is expressed in terms of a vector of Xlm symmetry adapted spherical harmonics X through: C = M x X.
  subroutine BuildTransMatfromXlmToSpher( MonExponents, SpherHarmIndexes, TransMat )
    !
    !> All the  monomials indexes coresponding to a definite group.
    integer,                                    intent(in)  :: MonExponents(:,:)
    integer,            allocatable,            intent(out) :: SpherHarmIndexes(:,:)
    complex(kind(1d0)), allocatable,            intent(out) :: TransMat(:,:)
    !> If this array is a matrix B, and de vectors S and X represent the spherical harmonics and Xlm functions respectively then S = B x X.
    complex(kind(1d0)), allocatable :: TrasMatXlmToSpherHarm(:,:)
    !> Array for the spherical harmonics indexes n,l,m in r^{n} Y_{lm} in the rows, and in the columns the different functions.
    !
    !
    allocate( SpherHarmIndexes, source = MonExponents )
    !
    call SpherXlmConversionMatrix( SpherHarmIndexes, TrasMatXlmToSpherHarm )
    !
    allocate( TransMat, source = TrasMatXlmToSpherHarm )
    deallocate( TrasMatXlmToSpherHarm )
    !
  end subroutine BuildTransMatfromXlmToSpher



  !> Builds the transformation matrix M, such that a vector of Xlm C is expressed in terms of a vector of Xlm symmetry adapted spherical harmonics X through: C = M x X. Essentially in this case the transformation matrix is the identity.
  subroutine BuildTransMatfromXlmToXlm( MonExponents, SpherHarmIndexes, TransMat )
    !
    !> All the  monomials indexes coresponding to a definite group.
    integer,                                    intent(in)  :: MonExponents(:,:)
    integer,            allocatable,            intent(out) :: SpherHarmIndexes(:,:)
    complex(kind(1d0)), allocatable,            intent(out) :: TransMat(:,:)
    !> If this array is a matrix B, and de vectors S and X represent the Xlm and Xlm functions respectively then S = B x X.
    complex(kind(1d0)), allocatable :: TrasMatXlmToXlm(:,:)
    integer :: i
    !> Array for the Xlm indexes n,l,m in r^{n} X_{lm} in the rows, and in the columns the different functions.
    !
    !
    allocate( SpherHarmIndexes, source = MonExponents )
    !
    allocate( TrasMatXlmToXlm(size(MonExponents,2),size(MonExponents,2)) )
    !
    TrasMatXlmToXlm = Z0
    do i = 1, size(MonExponents,2)
       TrasMatXlmToXlm(i,i) = Z1 
    end do
    !
    allocate( TransMat, source = TrasMatXlmToXlm )
    deallocate( TrasMatXlmToXlm )
    !
  end subroutine BuildTransMatfromXlmToXlm




  !> Fetches the array of monomial exponents.
  subroutine ClassCartesianSymmetricSetFetchMonomialExponents( CartSet, MonExponents )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    integer, allocatable,             intent(out) :: MonExponents(:,:)
    !
    integer :: i
    integer, allocatable :: OneMon(:)
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The cartesian monomial set has not been initialized, the monomial exponents cannot be fetched." )
    end if
    !
    allocate( MonExponents(3,CartSet%NumCart) )
    !
    do i = 1, CartSet%NumCart
       !
       call CartSet%MonomialList(i)%FetchMonomialExponents( OneMon )
       !
       MonExponents(:,i) = OneMon(:) 
       !
       deallocate( OneMon )
       !
    end do
    !
  end subroutine ClassCartesianSymmetricSetFetchMonomialExponents


  !> Fetches the array of monomial exponents.
  subroutine ClassSpherSymmetricSetFetchMonomialExponents( SpherSet, MonExponents )
    !
    class(ClassSpherSymmetricSet), intent(in)  :: SpherSet
    integer, allocatable,          intent(out) :: MonExponents(:,:)
    !
    integer :: i
    integer, allocatable :: OneMon(:)
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The spherical monomial set has not been initialized, the monomial exponents cannot be fetched." )
    end if
    !
    allocate( MonExponents(3,SpherSet%NumSpher) )
    !
    do i = 1, SpherSet%NumSpher
       !
       call SpherSet%MonomialList(i)%FetchMonomialExponents( OneMon )
       !
       MonExponents(:,i) = OneMon(:) 
       !
       deallocate( OneMon )
       !
    end do
    !
  end subroutine ClassSpherSymmetricSetFetchMonomialExponents



  !> Fetches the array of monomial exponents allowed by the symmetry.
  subroutine ClassCartesianSymmetricSetFetchSymMonomialExponents( CartSet, SymMonExponents )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    integer, allocatable,             intent(out) :: SymMonExponents(:,:)
    !
    integer :: i
    integer, allocatable :: OneMon(:)
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The cartesian monomial set has not been initialized, the monomial exponents cannot be fetched." )
    end if
    !
    allocate( SymMonExponents(3,size(CartSet%SymMonomialList)) )
    !
    do i = 1, CartSet%NumCart
       !
       call CartSet%SymMonomialList(i)%FetchMonomialExponents( OneMon )
       !
       SymMonExponents(:,i) = OneMon(:) 
       !
       deallocate( OneMon )
       !
    end do
    !
  end subroutine ClassCartesianSymmetricSetFetchSymMonomialExponents


  !> Returns " x  xxx  xyy  xzz " etc.
  subroutine ClassCartesianSymmetricSetFetchMonomialStrn( CartSet, Strn )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    character(len=:), allocatable,     intent(out) :: Strn
    !
    integer :: i, j, k
    integer, allocatable :: OneMon(:)
    character(len=:), allocatable :: MonStrn
    character(len=10000) :: AuxStrn
    character(len=*), parameter :: xstrn = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    character(len=*), parameter :: ystrn = "yyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
    character(len=*), parameter :: zstrn = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzz"

    if ( .not. CartSet%Initialized() ) then
       call Assert( "The cartesian monomial set has not been initialized, the monomial exponents cannot be fetched." )
    end if
    !
    AuxStrn = " "
    !
    do i = 1, CartSet%NumCart
       !
       call CartSet%SymMonomialList(i)%FetchMonomialExponents( OneMon )

       allocate( MonStrn, source = xstrn(1:OneMon(1))//ystrn(1:OneMon(2))//zstrn(1:OneMon(3)) )
       deallocate(OneMon)
       AuxStrn = trim(adjustl(AuxStrn))//" "//MonStrn 
       deallocate(MonStrn)
       !
    end do
    !
    allocate( Strn, source = trim(adjustl(AuxStrn)) )
    !
  end subroutine ClassCartesianSymmetricSetFetchMonomialStrn



  !> Fetches the array of monomial exponents allowed by the symmetry.
  subroutine ClassSpherSymmetricSetFetchSymMonomialExponents( SpherSet, SymMonExponents )
    !
    class(ClassSpherSymmetricSet), intent(in)  :: SpherSet
    integer, allocatable,          intent(out) :: SymMonExponents(:,:)
    !
    integer :: i
    integer, allocatable :: OneMon(:)
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The spherical monomial set has not been initialized, the monomial exponents cannot be fetched." )
    end if
    !
    allocate( SymMonExponents(3,size(SpherSet%SymMonomialList)) )
    !
    do i = 1, SpherSet%NumSpher
       !
       call SpherSet%SymMonomialList(i)%FetchMonomialExponents( OneMon )
       !
       SymMonExponents(:,i) = OneMon(:) 
       !
       deallocate( OneMon )
       !
    end do
    !
  end subroutine ClassSpherSymmetricSetFetchSymMonomialExponents



  !> Fetches the monomial exponents.
  subroutine ClassMonomialFetchMonomialExponents( Mon, MonVec )
    !
    !> Class of monomials.
    class(ClassMonomial), intent(in)  :: Mon
    !> Vector with the x,y and z exponents
    integer, allocatable, intent(out) :: MonVec(:)
    !
    allocate( MonVec(3) )
    !
    MonVec(1) = Mon%ix
    MonVec(2) = Mon%iy
    MonVec(3) = Mon%iz
    !
  end subroutine ClassMonomialFetchMonomialExponents


  !> Check if a triplet of integers matches the object
  function ClassMonomialMatches( Mon, ix,iy,iz ) result( matches )
    class(ClassMonomial), intent(in) :: Mon
    integer             , intent(in) :: ix, iy, iz
    logical                          :: matches
    matches = &
         ( ix == Mon%ix ) .and. &
         ( iy == Mon%iy ) .and. &
         ( iz == Mon%iz ) 
  end function ClassMonomialMatches


  !> Check if a monomial class matches other.
  function ClassMonomialClassesMatches( Mon1, Mon2 ) result( matches )
    class(ClassMonomial), intent(in) :: Mon1
    class(ClassMonomial), intent(in) :: Mon2
    logical                          :: matches
    integer, allocatable :: MonVec2(:)
    matches = .false.
    call Mon2%FetchMonomialExponents( MonVec2 )
    if ( Mon1%Matches(MonVec2(1),MonVec2(2),MonVec2(3)) ) matches = .true.
  end function ClassMonomialClassesMatches


  !> Checks if a monomial class lies in a monomial class list.
  function ClassMonomialIsInList( Mon, MonList ) result( InList )
    class(ClassMonomial), intent(in) :: Mon
    class(ClassMonomial), intent(in) :: MonList(:)
    logical                          :: InList
    integer :: i
    InList = .false.
    do i = 1, size(MonList)
       if ( Mon%Matches(MonList(i)) ) InList = .true.
    end do
  end function ClassMonomialIsInList


  !> Checks the compatibility of ClassCartesianSymmetricSet and ClassXlmSymmetricSet.
  subroutine CheckXlmCartesianCompatibility( CartSet, XlmSet )
    !
    type(ClassCartesianSymmetricSet), intent(in) :: CartSet
    type(ClassXlmSymmetricSet),       intent(in) :: XlmSet
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The cartesian monomial set has not been initialized." )
    end if
    !
    if ( .not. XlmSet%Initialized() ) then
       call Assert( "The Xlm set has not been initialized." )
    end if
    !
    if ( .not.( CartSet%Irrep%NameIs(XlmSet%Irrep%GetName()) ) ) then
       call Assert( "The Xlm and cartesian sets symmetries do not coincide." )
    end if
    !
  end subroutine CheckXlmCartesianCompatibility




  !> Builds the transformation matrix that expresses the cartesian gaussians in terms of spherical gaussians.
  subroutine CartSpherConversionMatrix( MonomialIndexes, TransMat, SpherHarmIndexes )
    !
    !> Cartesian Monomial exponents array. Vector of (i,j,k): exponents of (x,y,z).
    integer,                         intent(in)  :: MonomialIndexes(:,:)
    !> Transformation matrix
    complex(kind(1d0)), allocatable, intent(out) :: TransMat(:,:)
    !> Array for the spherical harmonics indexes n,l,m in r^{n} Y_{lm} in the rows, and in the columns the different functions.
    integer, allocatable, optional, intent(out) :: SpherHarmIndexes(:,:)
    !
    integer :: p, q, k, n, j, R_AngMom, AngMom, ProjAngMom, info, lwork
    integer :: iRowTF, iColTF, NRS, i, ii, ExtI, ExtJ, ExtraRexpRed, ExtraRexp
    real(kind(1d0)) :: Clm, Dl
    complex(kind(1d0)) :: InnerCoeff, IntermediateCoeff
    complex(kind(1d0)), allocatable :: SpherParam(:,:)
    integer, allocatable :: RequestedSymm(:), NumberOfReqSymm(:), ipiv(:), IndexSpher(:,:)
    complex(kind(1d0)), allocatable :: work(:)
    integer :: NumberOfMonomials
    real(kind(1d0)), parameter :: Threshold = 1.d-12
    !
    NumberOfMonomials = size(MonomialIndexes,2)
    allocate( RequestedSymm( NumberOfMonomials ) )
    !
    allocate( NumberOfReqSymm( NumberOfMonomials ) )
    NumberOfReqSymm = 0
    !
    NRS = 0
    do j = 1, NumberOfMonomials
       !
       RequestedSymm(j) = sum( MonomialIndexes(1:3,j) )
       !
       if ( j == 1 ) then
          NRS = NRS+1
          NumberOfReqSymm(NRS) =  RequestedSymm(j)
       elseif ( RequestedSymm(j) /= RequestedSymm(j-1) ) then
          NRS = NRS+1
          NumberOfReqSymm(NRS) = RequestedSymm(j)
       end if
       !
    end do
    !
    allocate(SpherParam(NumberOfMonomials,NumberOfMonomials))
    SpherParam = Z0
    !
    allocate( IndexSpher(3,NumberOfMonomials) )
    ! Let 
    !
    ! 1. iRowTF be the absolute index of the spherical functions 
    ! in the set r^{n} Y_{lm} with n >= l,  mod(n-l,2) = 0 
    ! sorted in 
    ! 1st: ascending order of n;
    ! 2nd: ascending order of l;
    ! 3rd: ascending order of m.
    !
    ! 2. iColTF be the absolute index of the cartesian functions x^i y^j z^k
    ! sorted as in the input file which specifies the QC basis, under the
    ! assumptions that (i+j+k) is a semi-monotonic function.
    !
    ! Then, the matrix SpherParam contains the expansion of the spherical
    ! harmonics in terms of cartesian monomials
    !
    ! Ylm r^n = \sum_{iColTF} x^(MonomialIndexes(1,iColTF)) 
    !                         y^(MonomialIndexes(2,iColTF)) 
    !                         z^(MonomialIndexes(3,iColTF)) 
    !                         SpherParam( iRowTF, iColTF )
    iRowTF = 0
    iColTF = 0
    !
    do i = 1, NRS
       !
       R_AngMom = NumberOfReqSymm(i)
       !
       do AngMom = mod(R_AngMom,2), R_AngMom, 2
          !
          Dl = dble(AngMom)
          !
          ExtraRexp = R_AngMom - AngMom ! even number
          ExtraRexpRed = ExtraRexp/2
          !
          do ProjAngMom = -AngMom, AngMom
             !
             iRowTF = iRowTF + 1
             
             IndexSpher(1,iRowTF) = R_AngMom   ! l + 2 k
             IndexSpher(2,iRowTF) = AngMom     ! l
             IndexSpher(3,iRowTF) = ProjAngMom ! m
             !
             Clm = sqrt( (2.d0*Dl+1.d0)/(4.d0*PI)*fact(AngMom+ProjAngMom)*fact(AngMom-ProjAngMom) )
             !
             ! 0) p, q, k integers |
             ! 1) p + q + k = AngMom
             ! 2) p - q     = ProjAngMom
             ! 3) p, q, k  >= 0
             do k = mod(abs(AngMom+ProjAngMom),2), AngMom-abs(ProjAngMom), 2 
                !
                p = (AngMom+ProjAngMom-k)/2
                q = p - ProjAngMom
                !
                do j = 0, q
                   !
                   do n = 0, min( p + q - j, p )
                      !
                      IntermediateCoeff = &
                           (-1.d0)**(p+q-j) * Comb(n,p) * Comb(j,q) * &
                           (Zi)**(p-n+q-j)/(2.d0**(p+q)*fact(p)*fact(q)*fact(k))
                      !
                      do ExtI = 0, ExtraRexpRed
                         !
                         do ExtJ = 0, ExtI
                            !
                            !.. Find the absolute index of the current monomial in the 
                            !   expansion of r^{l+2k}Y_{lm} in the cartesian basis
                            do ii = 1, NumberOfMonomials
                               !
                               if ( ( MonomialIndexes(1,ii) == n+j+2*ExtJ              ) .and.&
                                    ( MonomialIndexes(2,ii) == p+q-n-j+2*(ExtI-ExtJ)   ) .and.&
                                    ( MonomialIndexes(3,ii) == k+2*(ExtraRexpRed-ExtI) ) ) then
                                  !
                                  iColTF = ii
                                  exit
                                  !
                               end if
                               !
                            end do
                            !
                            InnerCoeff = IntermediateCoeff * Comb(ExtJ,ExtI)*Comb(ExtI,ExtraRexpRed)
                            !
                            SpherParam(iRowTF,iColTF) = SpherParam(iRowTF,iColTF) + Clm*InnerCoeff
                            !
                         end do
                         !
                      end do
                      !
                   end do
                   !
                end do
                !
             end do
             !
          end do
          !
       end do
       !
    end do
    !
    if ( present(SpherHarmIndexes) ) then
       !
       allocate( SpherHarmIndexes, source = IndexSpher )
       !
    end if
    !
    ! Inversion of the Matrix
    !
    allocate( ipiv(NumberOfMonomials) )
    !
    call zgetrf(NumberOfMonomials,NumberOfMonomials,SpherParam,NumberOfMonomials,ipiv,info)
    !
    lwork = NumberOfMonomials
    !
    allocate( work(lwork) )
    !
    call zgetri(NumberOfMonomials,SpherParam,NumberOfMonomials,ipiv,work,lwork,info)
    !
    allocate( TransMat, source = SpherParam )
    !
    ! Removes posibly very close to zero elements arising from inversion proces.
    do j = 1, size(TransMat,2)
       do i = 1, size(TransMat,1)
          if ( abs(TransMat(i,j)) < Threshold ) then
             TransMat(i,j) = Z0
          end if
       end do
    end do
    !
    deallocate( SpherParam )
    !
  end subroutine CartSpherConversionMatrix




  !> Builds the transformation matrix that expresses the spherical gaussians in terms of the Xlm symmetry adapted functions. Supposes that the Xlm functions have the same ordening than the harmonics, according to l and m.
  subroutine SpherXlmConversionMatrix( SpherHarmIndexes, TransMat )
    !
    !> Array of the spherical harmonics indexes n,l,m in r^{n} Y_{lm} in the rows, and in the columns the different functions.
    integer,                         intent(in)  :: SpherHarmIndexes(:,:)
    !> If this array is a matrix B, and de vectors S and X represent the spherical harmonics and Xlm functions respectively then S = B x X. So considering the comment and the others above: M = A x B. 
    complex(kind(1d0)), allocatable, intent(out) :: TransMat(:,:)
    !
    complex(kind(1d0)), allocatable :: ArrayTransMat(:,:)
    integer :: NumSpherHarm, i, n, l, m, j
    !
    NumSpherHarm = size(SpherHarmIndexes,2)
    !
    allocate( ArrayTransMat(NumSpherHarm,NumSpherHarm) )
    ArrayTransMat = Z0
    !
    do i = 1, NumSpherHarm
       !
       n = SpherHarmIndexes(1,i)
       l = SpherHarmIndexes(2,i)
       m = SpherHarmIndexes(3,i)
       !
       do j = 1, NumSpherHarm
          !
          if ( (SpherHarmIndexes(1,j)==n) .and. (SpherHarmIndexes(2,j)==l) ) then
             !
             if ( m == 0 ) then
                !
                if ( SpherHarmIndexes(3,j) == 0 ) then
                   ArrayTransMat(i,j) = Z1
                end if
                !
             elseif ( m > 0 ) then
                !
                if ( SpherHarmIndexes(3,j) == m ) then
                   ArrayTransMat(i,j) = Z1/sqrt(2.d0)
                end if
                if ( SpherHarmIndexes(3,j) == -m ) then
                   ArrayTransMat(i,j) = Zi/sqrt(2.d0)
                end if
                !
             elseif( m < 0 ) then
                !
                if ( SpherHarmIndexes(3,j) == m ) then
!!$                   ArrayTransMat(i,j) = -Zi/sqrt(2.d0)
                   ArrayTransMat(i,j) = -Zi/sqrt(2.d0)*(-1.d0)**(-m)
                end if
                if ( SpherHarmIndexes(3,j) == -m ) then
!!$                   ArrayTransMat(i,j) = Z1/sqrt(2.d0)
                   ArrayTransMat(i,j) = Z1/sqrt(2.d0)*(-1.d0)**(-m)
                end if
                !
             end if
             !
          end if
          !
       end do
       !
    end do
    !
    allocate( TransMat, source = ArrayTransMat )
    deallocate( ArrayTransMat )
    !
  end subroutine SpherXlmConversionMatrix



  !> Factorial function
  real(kind(1d0)) function fact(n) 
    !
    implicit none
    !> Factorial function argument.
    integer, intent(in) :: n
    !
    real(kind(1d0)) :: factorial
    integer :: i
    !
    factorial = 1.d0
    if (n==0) then
       fact = factorial
    else
       do i = 1, n
          factorial = factorial*dble(i) 
       end do
       fact = factorial
    end if
    !
  end function fact


  !> Combinatorics.
  real(kind(1d0)) function Comb(a,b)
    !
    !> a <= b
    integer, intent(in) :: a
    !> a <= b
    integer, intent(in) :: b
    !
    Comb = fact(b)/(fact(a)*fact(b-a))
    !
  end function Comb




  !> Prints on a unit the relevant info of ClassXlmSet.
  subroutine ClassXlmSetShow( XlmSet, unit )
    !
    !> Class ClassXlmSet
    class(ClassXlmSet), intent(in) :: XlmSet
    !> Unit in which the info will be printed.
    integer, optional,  intent(in) :: unit
    !
    integer :: OutputUnit, i
    !
    if ( .not.  XlmSet%Initialized ) then
       call Assert( "The class has not been initialized, so cannot be shown." )
    end if
    !
    if ( present(unit) ) then
       OutputUnit = unit
    else
       OutputUnit = OUTPUT_UNIT
    end if
    !
    write(OutputUnit,fmt='(a)') "ClassXlmSet info:"
    write(OutputUnit,fmt='(a)') 
    write(OutputUnit,fmt='(a,i2)') 'Lmax =', XlmSet%Lmax
    write(OutputUnit,fmt='(a,i2)') 'Num Irreps =', XlmSet%NIrreps
    write(OutputUnit,fmt='(a,2x,a)') 'Group =>', XlmSet%Group%GetName()
    !
    do i = 1, XlmSet%NIrreps
       call XlmSet%XlmSymSet(i)%Show( OutputUnit )
    end do
    !
    write(OutputUnit,fmt='(a)') 
    !
  end subroutine ClassXlmSetShow





  !> Prints on a unit the relevant info of ClassXlmSymmetricSet.
  subroutine ClassXlmSymmetricSetShow( XlmSymSet, unit )
    !
    class(ClassXlmSymmetricSet), intent(in) :: XlmSymSet
    integer, optional,           intent(in) :: unit
    !
    integer :: i, outunit
    !
    if ( present(unit) ) then
       outunit = unit
    else
       outunit = OUTPUT_UNIT
    end if
    !
    call XlmSymSet%Irrep%Show( unit )
    !
    write(outunit,fmt='(a,2x,i5)') "Number of Xlm", XlmSymSet%NumXlm
    !
    do i = 1, XlmSymSet%NumXlm
       write(outunit,fmt='(2(a,x),x,2i2)') "l", "m", XlmSymSet%XlmList(i)%l, XlmSymSet%XlmList(i)%m
    end do
    !
  end subroutine ClassXlmSymmetricSetShow



  !> Shows in a uni the relevant info of the class.
  subroutine ClassCartesianSymmetricSetShow( CartSet, unit )
    !
    class(ClassCartesianSymmetricSet), intent(in) :: CartSet
    integer, optional,                 intent(in) :: unit
    !
    integer :: OutputUnit, i
    !
    if ( present(unit) ) then
       OutputUnit = unit
    else
       OutputUnit = OUTPUT_UNIT
    end if
    !
    write(OutputUnit,fmt='(a)') "ClassCartesianSymmetricSet info:"
    write(OutputUnit,fmt='(a)') 
    write(OutputUnit,fmt='(a,i2)') 'Lmax =', CartSet%Lmax
    write(OutputUnit,fmt='(a,2x,a)') 'Irrep =>', CartSet%Irrep%GetName()
    write(OutputUnit,fmt='(a,i2)') 'Number Cartesians =', CartSet%NumCart
    !
    write(OutputUnit,fmt='(a)') "Cartesian exponents:" 
    !
    do i = 1, size(CartSet%MonomialList)
       call CartSet%MonomialList(i)%Show( OutputUnit )
    end do
    !
    write(OutputUnit,fmt='(a)') "Cartesian exponents allowed by symmetry:" 
    !
    do i = 1, size(CartSet%SymMonomialList)
       call CartSet%SymMonomialList(i)%Show( OutputUnit )
    end do
    !
    write(OutputUnit,fmt='(a)') 
    !
  end subroutine ClassCartesianSymmetricSetShow



  !> Shows in a uni the relevant info of the class.
  subroutine ClassSpherSymmetricSetShow( SpherSet, unit )
    !
    class(ClassSpherSymmetricSet), intent(in) :: SpherSet
    integer, optional,             intent(in) :: unit
    !
    integer :: OutputUnit, i
    !
    if ( present(unit) ) then
       OutputUnit = unit
    else
       OutputUnit = OUTPUT_UNIT
    end if
    !
    write(OutputUnit,fmt='(a)') "ClassSpherSymmetricSet info:"
    write(OutputUnit,fmt='(a)') 
    write(OutputUnit,fmt='(a,i2)') 'Lmax =', SpherSet%Lmax
    write(OutputUnit,fmt='(a,2x,a)') 'Irrep =>', SpherSet%Irrep%GetName()
    write(OutputUnit,fmt='(a,i2)') 'Number Spherical =', SpherSet%NumSpher
    !
    write(OutputUnit,fmt='(a)') "Spherical exponents:" 
    !
    do i = 1, size(SpherSet%MonomialList)
       call SpherSet%MonomialList(i)%Show( OutputUnit )
    end do
    !
    write(OutputUnit,fmt='(a)') "Spherical exponents allowed by symmetry:" 
    !
    do i = 1, size(SpherSet%SymMonomialList)
       call SpherSet%SymMonomialList(i)%Show( OutputUnit )
    end do
    !
    write(OutputUnit,fmt='(a)') 
    !
  end subroutine ClassSpherSymmetricSetShow



  subroutine ClassMonomialShow( Mon, unit )
    !
    class(ClassMonomial), intent(in) :: Mon
    integer,              intent(in) :: unit
    !
    write(unit,fmt='(3i2)') Mon%ix, Mon%iy, Mon%iz
    !
  end subroutine ClassMonomialShow




  !> Given an irrep of the group, returns a pointer to the
  !! Xlm symmetric set corresponding to that irrep
  function ClassXlmSetGetSymSet( XlmSet, IrrepName ) result( XlmSymSetPtr )
    !
    class(ClassXlmSet), target, intent(in) :: XlmSet
    character(len=*)          , intent(in) :: IrrepName
    type(ClassXlmSymmetricSet), pointer    :: XlmSymSetPtr
    !
    integer :: i
    logical :: Found
    !
    Found = .false.
    !
    XlmSymSetPtr => NULL()
    !
    do i = 1, XlmSet%NIrreps
       if ( XlmSet%XlmSymSet(i)%Irrep%NameIs(IrrepName) ) then
          XlmSymSetPtr => XlmSet%XlmSymSet(i)
          Found = .true.
       end if
    end do
    !
    if ( .not. Found ) then
       call Assert( "The requested irreducible representation "//trim(IrrepName)//" is not present in the Xlm set." )
    end if
    !
  end function ClassXlmSetGetSymSet


  function ClassXlmSymmetricSetGetIrrep( XlmSymSet ) result( IrrepPtr )
    !
    class(ClassXlmSymmetricSet), intent(in) :: XlmSymSet
    type(ClassIrrep), pointer :: IrrepPtr
    !
    IrrepPtr => XlmSymSet%Irrep
    !
  end function ClassXlmSymmetricSetGetIrrep


  function ClassXlmSymmetricSetGetNXlm( XlmSymSet ) result( NXlm )
    class(ClassXlmSymmetricSet), intent(in) :: XlmSymSet
    integer :: NXlm
    NXlm = XlmSymSet%NumXlm
  end function ClassXlmSymmetricSetGetNXlm


  function ClassXlmSymmetricSetGetXlm( XlmSymSet, iXlm ) result( Xlm )
    class(ClassXlmSymmetricSet), intent(in) :: XlmSymSet
    integer                    , intent(in) :: iXlm
    type(ClassXlm) :: Xlm
    Xlm = XlmSymSet%XlmList(iXlm)
  end function ClassXlmSymmetricSetGetXlm


  subroutine ClassXlmSymmetricSetGetMList( XlmSymSet, l, Mlist ) 
    class(ClassXlmSymmetricSet), intent(in)  :: XlmSymSet
    integer                    , intent(in)  :: l
    integer, allocatable       , intent(out) :: Mlist(:)
    integer :: i, icount
    !
    if(l>XlmSymSet%Lmax)call Assert( &
         " l "//AlphabeticNumber(l)//&
         " > Lmax "//AlphabeticNumber(XlmSymSet%Lmax)//&
         "in ClassXlmSymmetricSetGetMList")
    !
    if(allocated(MList))deallocate(Mlist)
    allocate(Mlist(XlmSymSet%dim(l)))
    icount=0
    do i = XlmSymSet%idim(l)+1, XlmSymSet%idim(l)+XlmSymSet%dim(l)
       icount=icount+1
       Mlist(icount) = XlmSymSet%XlmList(i)%Getm()
    enddo
    !
  end subroutine ClassXlmSymmetricSetGetMList


  !> Fetches the transformation matrix that expresses the cartesian monomials in terms of the symmetric adapted spherical harmonics Xlm.
  subroutine FetchXlmToCartesianMatrix( CartSet, Matrix )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    complex(kind(1d0)), allocatable,   intent(out) :: Matrix(:,:)
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The class of cartesians monomial has not been initialized, imposible to fetch transformation matrix.")
    else
       allocate( Matrix, source = CartSet%XlmToCartArray )
    end if
    !
  end subroutine FetchXlmToCartesianMatrix



  !> Fetches the transformation matrix that expresses the spherical monomials in terms of the symmetric adapted spherical harmonics Xlm.
  subroutine FetchXlmToSpherMatrix( SpherSet, Matrix )
    !
    class(ClassSpherSymmetricSet),   intent(in)  :: SpherSet
    complex(kind(1d0)), allocatable, intent(out) :: Matrix(:,:)
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The class of spherical monomial has not been initialized, imposible to fetch transformation matrix.")
    else
       allocate( Matrix, source = SpherSet%XlmToSpherArray )
    end if
    !
  end subroutine FetchXlmToSpherMatrix

  


  !> For a given irreducible representation and an angular momentum retrieve for all te possible Xlm( r^l*Xlm, its expansion in terms of the cartessian monomials. 
  subroutine GetCartesianExpan( XlmSet, CartSet, IrrepName, l, VectorOfM, ExpansionMat )
    !
    class(ClassXlmSet),                intent(in)  :: XlmSet
    class(ClassCartesianSymmetricSet), intent(inout)  :: CartSet
    !> Name of the requested irreducible representation.
    character(len=*),                  intent(in)  :: IrrepName
    !> Angular momentum
    integer,                           intent(in)  :: l
    !> Vector that stores the allowed m indices in the requested Xlm functions
    integer, allocatable,              intent(out) :: VectorOfM(:)
    !> Matrix that stores in each row the cartesian monomials expansion of the Xlm functions keeping the same ordering appearing in the VectorOfM vector.
    complex(kind(1d0)), allocatable,   intent(out) :: ExpansionMat(:,:)
    type( ClassXlmSymmetricSet ), pointer :: XlmSymSet => NULL()
    integer :: NumXlm, i, Counter, j
    !
    if ( .not. XlmSet%Initialized ) then
       call Assert( "The Xlm set has not been initialized." )
    end if
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The Cartesian set has not been initialized." )
    end if
    !
    if ( l > CartSet%Lmax ) then
       call Assert( "The angular momentum is higher than the maximum representable value." )
    end if
    !
    call CartSet%ComputeTransfMat()
    !
    XlmSymSet =>  XlmSet%GetSymSet(IrrepName)
    !
    NumXlm = XlmSymSet%NumXlm
    !
    if ( NumXlm == 0 ) then
       call Assert( "The irrep "//trim(IrrepName)//" does not have Xlm functions for the maximum angular momentum used." )
    end if
    !
    Counter = 0
    !
    do i = 1, NumXlm
       !
       if( l == XlmSymSet%XlmList(i)%l ) then
          Counter = Counter + 1
       end if
       !
    end do
    !
    if ( Counter == 0 ) then
       call Assert( "The irrep "//trim(IrrepName)//" does not have Xlm functions for the angular momentum specified." )
    end if
    !
    allocate( VectorOfM(Counter) )
    allocate( ExpansionMat(Counter,CartSet%NumCart) )
    !
    Counter = 0
    !
    do i = 1, NumXlm
       !
       if( l == XlmSymSet%XlmList(i)%l ) then
          Counter = Counter + 1
          VectorOfM(Counter) = XlmSymSet%XlmList(i)%m
       end if
       !
    end do
    !
    Counter = 0
    !
    do i = 1, size(VectorOfM)
       do j = 1, size(CartSet%CartToXlmArray,1)
          !
          if( (l == CartSet%XlmTransfIndexes(1,j)) &
               .and. (l == CartSet%XlmTransfIndexes(2,j)) &
               .and. (VectorOfM(i) == CartSet%XlmTransfIndexes(3,j))) then 
             !
             Counter = Counter + 1
             !
             ExpansionMat(Counter,:) = CartSet%CartToXlmArray(j,:)
             !
          end if
          !
       end do
    end do
    !
  end subroutine GetCartesianExpan




  !> For a given irreducible representation and an angular momentum retrieve for all te possible Xlm( r^l*Xlm, its expansion in terms of the cartessian monomials. 
  subroutine ClassXlmSymmetricSetGetCartesianExpan( XlmSymSet, CartSet, l, VectorOfM, ExpansionMat )
    !
    class(ClassXlmSymmetricSet),       intent(in)  :: XlmSymSet
    class(ClassCartesianSymmetricSet), intent(inout)  :: CartSet
    !> Angular momentum
    integer,                           intent(in)  :: l
    !> Vector that stores the allowed m indices in the requested Xlm functions
    integer, allocatable,              intent(out) :: VectorOfM(:)
    !> Matrix that stores in each row the cartesian monomials expansion of the Xlm functions keeping the same ordering appearing in the VectorOfM vector.
    complex(kind(1d0)), allocatable,   intent(out) :: ExpansionMat(:,:)
    integer :: NumXlm, i, Counter, j
    !
    if ( .not. XlmSymSet%Initialized() ) then
       call Assert( "The symmetric Xlm set has not been initialized." )
    end if
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The Cartesian set has not been initialized." )
    end if
    !
    if ( l > CartSet%Lmax ) then
       call Assert( "The angular momentum is higher than the maximum representable value." )
    end if
    !
    
!    call CartSet%ComputeTransfMat()
    !
    NumXlm = XlmSymSet%NumXlm
    !
    if ( NumXlm == 0 ) then
       call Assert( "The current irrep does not have Xlm functions for the maximum angular momentum used." )
    end if
    !
    Counter = 0
    !
    do i = 1, NumXlm
       !
       if( l == XlmSymSet%XlmList(i)%l ) then
          Counter = Counter + 1
       end if
       !
    end do
    !
    if ( Counter == 0 ) then
!!$       call ErrorMessage( "The current irrep "//XlmSymSet%Irrep%GetName()//&
!!$            " does not have Xlm functions for the angular momentum "//&
!!$            "specified: "//AlphabeticNumber(l)//"." )
       return
    end if
    !
    allocate( VectorOfM(Counter) )
    allocate( ExpansionMat(Counter,CartSet%NumCart) )
    !
    Counter = 0
    !
    do i = 1, NumXlm
       !
       if( l == XlmSymSet%XlmList(i)%l ) then
          Counter = Counter + 1
          VectorOfM(Counter) = XlmSymSet%XlmList(i)%m
       end if
       !
    end do
    !
    Counter = 0
    !
    do i = 1, size(VectorOfM)
       do j = 1, size(CartSet%CartToXlmArray,1)
          !
          if( (l == CartSet%XlmTransfIndexes(1,j)) &
               .and. (l == CartSet%XlmTransfIndexes(2,j)) &
               .and. (VectorOfM(i) == CartSet%XlmTransfIndexes(3,j))) then 
             !
             Counter = Counter + 1
             !
             ExpansionMat(Counter,:) = CartSet%CartToXlmArray(j,:)
             !
          end if
          !
       end do
    end do
    !
  end subroutine ClassXlmSymmetricSetGetCartesianExpan




  !> Builds the transformation matrix M, such that a vector of  of Xlm symmetry adapted spherical harmonics X is expressed in terms of a vector C of cartesian monomials through: X = M x C.
  subroutine BuildTransMatfromCartToXlm( Mat, MatInv )
    !
    complex(kind(1d0)),              intent(in)  :: Mat(:,:)
    complex(kind(1d0)), allocatable, intent(out) :: MatInv(:,:)
    !
    complex(kind(1d0)), allocatable :: work(:), MatCopy(:,:)
    integer, allocatable :: ipiv(:)
    integer :: lwork, info, NumRows, i, j
    real(kind(1d0)), parameter :: Threshold = 1.d-12
    !
    allocate( MatCopy, source = Mat )
    !
    NumRows = size(MatCopy,1)
    !
    allocate( ipiv(NumRows) )
    !
    call zgetrf( NumRows, NumRows, MatCopy, NumRows, ipiv, info )
    !
    lwork = NumRows
    allocate( work(lwork) )
    !
    call zgetri( NumRows, MatCopy, NumRows, ipiv, work, lwork, info )
    !
    !
    ! Removes posibly very close to zero elements arising from inversion proces.
    do j = 1, size(MatCopy,2)
       do i = 1, size(MatCopy,1)
          if ( abs(MatCopy(i,j)) < Threshold ) then
             MatCopy(i,j) = Z0
          end if
       end do
    end do
    !
    allocate( MatInv, source = MatCopy )
    !
  end subroutine BuildTransMatfromCartToXlm




  !> Builds the transformation matrix M, such that a vector of  of Xlm symmetry adapted spherical harmonics X is expressed in terms of a vector C of spherical monomials through: X = M x C.
  subroutine BuildTransMatfromSpherToXlm( Mat, MatInv )
    !
    complex(kind(1d0)),              intent(in)  :: Mat(:,:)
    complex(kind(1d0)), allocatable, intent(out) :: MatInv(:,:)
    !
    complex(kind(1d0)), allocatable :: work(:), MatCopy(:,:)
    integer, allocatable :: ipiv(:)
    integer :: lwork, info, NumRows, i, j
    real(kind(1d0)), parameter :: Threshold = 1.d-12
    !
    allocate( MatCopy, source = Mat )
    !
    NumRows = size(MatCopy,1)
    !
    allocate( ipiv(NumRows) )
    !
    call zgetrf( NumRows, NumRows, MatCopy, NumRows, ipiv, info )
    !
    lwork = NumRows
    allocate( work(lwork) )
    !
    call zgetri( NumRows, MatCopy, NumRows, ipiv, work, lwork, info )
    !
    !
    ! Removes posibly very close to zero elements arising from inversion proces.
    do j = 1, size(MatCopy,2)
       do i = 1, size(MatCopy,1)
          if ( abs(MatCopy(i,j)) < Threshold ) then
             MatCopy(i,j) = Z0
          end if
       end do
    end do
    !
    allocate( MatInv, source = MatCopy )
    !
  end subroutine BuildTransMatfromSpherToXlm




  !> Fetches the indexes n, l and m of r^n*Xlm in the order used in the transformation to or from cartesian.
  subroutine FetchXlmTransfIndexes( CartSet, Mat )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    integer, allocatable,              intent(out) :: Mat(:,:)
    !
    allocate( Mat, source = CartSet%XlmTransfIndexes )
    !
  end subroutine FetchXlmTransfIndexes



  !> Fetches the indexes n, l and m of r^n*Xlm in the order used in the transformation to or from spherical.
  subroutine SpherFetchXlmTransfIndexes( SpherSet, Mat )
    !
    class(ClassSpherSymmetricSet), intent(in)  :: SpherSet
    integer, allocatable,          intent(out) :: Mat(:,:)
    !
    allocate( Mat, source = SpherSet%XlmTransfIndexes )
    !
  end subroutine SpherFetchXlmTransfIndexes



  !> Fetches the transformation matrix that expresses the symmetric adapted spherical harmonics Xlm in terms of the cartesian monomials.
  subroutine FetchCartesianToXlmMatrix( CartSet, Mat )
    !
    class(ClassCartesianSymmetricSet), intent(in)  :: CartSet
    complex(kind(1d0)), allocatable,   intent(out) :: Mat(:,:)
    !
    allocate( Mat, source = CartSet%CartToXlmArray )
    !
  end subroutine FetchCartesianToXlmMatrix



  !> Fetches the transformation matrix that expresses the symmetric adapted spherical harmonics Xlm in terms of the spherical monomials.
  subroutine FetchSpherToXlmMatrix( SpherSet, Mat )
    !
    class(ClassSpherSymmetricSet),   intent(in)  :: SpherSet
    complex(kind(1d0)), allocatable, intent(out) :: Mat(:,:)
    !
    allocate( Mat, source = SpherSet%SpherToXlmArray )
    !
  end subroutine FetchSpherToXlmMatrix




  !> Computes the transformation matrices between the cartesians and the Xlm ( in both directions ) belonging to the irrep of the class.
  subroutine ComputeTransfMat( CartSet )
    !
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    !
    type(ClassGroup), pointer :: Group
    type( ClassIrrep ), pointer, dimension(:) :: IrrepV
    integer :: i, TotalNumCartesians, FirstInd, LastInd
    type(ClassCartesianSymmetricSet), allocatable :: AllCartSet(:)
    integer, allocatable :: TotalMonomialIndexes(:,:), PartialMonInd(:,:), TotalXlmIndexes(:,:), Veeec(:,:)
    complex(kind(1d0)), allocatable :: XlmToCartArray(:,:), CartToXlmArray(:,:)
    integer :: ii, jj
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The symmetric cartesian set has not been initialized and the transformation matrices cannot be computed." )
    end if
    !
    ! If equal then the two list of classes are the same, appart maybe the ordering, because previously in initialization was assured that CartSet%MonomialList is a subset of CartSet%SymMonomialList.
    if ( size(CartSet%MonomialList) == size(CartSet%SymMonomialList) ) then
       !
       ! Gets all the cartesian monomials of the point group, taking into account all the irreducible representations.
       !
       Group => CartSet%Irrep%GetGroup()
       !
       IrrepV => Group%GetIrrepList()
       !
       allocate( AllCartSet(size(IrrepV)) )
       !
       TotalNumCartesians = 0
       !
       do i = 1, size(IrrepV)
          !
          call AllCartSet(i)%Init( CartSet%Lmax, IrrepV(i) )
          !
          TotalNumCartesians = TotalNumCartesians + AllCartSet(i)%GetN()
          !
       end do
       !
       allocate( TotalMonomialIndexes(3,TotalNumCartesians) )
       !
       FirstInd = 1
       !
       do i = 1, size(IrrepV)
          !
          if ( AllCartSet(i)%GetN() > 0 ) then
             !
             call AllCartSet(i)%FetchMonomialExponents( PartialMonInd )
             !
             LastInd = FirstInd + AllCartSet(i)%GetN() - 1
             !
             TotalMonomialIndexes(:,FirstInd:LastInd) = PartialMonInd(:,:)
             !
             FirstInd = LastInd + 1
             !
             deallocate( PartialMonInd )
             !
          end if
          !
       end do
       !
    else
       !
       call CartSet%FetchMonomialExponents( TotalMonomialIndexes )
       !
    end if
    !

    call Reorder( TotalMonomialIndexes )

    !
    call BuildTransMatfromXlmToCart( TotalMonomialIndexes, TotalXlmIndexes, XlmToCartArray )
    !
    if ( allocated(CartSet%XlmTransfIndexes) ) deallocate( CartSet%XlmTransfIndexes )
    allocate( CartSet%XlmTransfIndexes, source = TotalXlmIndexes )
    !
    !***
!!$    do ii = 1, size(TotalXlmIndexes,2)
  !     write(output_unit,*)  (TotalXlmIndexes(jj,ii),jj=1,3)
      ! if ( ii/=1 .and. ii/=5 ) then
!!$       if ( ii==1 .or. ii==5 ) then
!!$          XlmToCartArray(:,ii) = Z0
!!$       end if
!!$    end do
    ! XlmToCartArray = Z0
    !***
    !
    call BuildTransMatfromCartToXlm( XlmToCartArray, CartToXlmArray)
    !
    call CartSet%SetXlmToCartArray( TotalMonomialIndexes, XlmToCartArray )
    !
    call CartSet%SetCartToXlmArray( TotalMonomialIndexes, CartToXlmArray )
    !
  end subroutine ComputeTransfMat



  !> Computes the transformation matrices between the spherical and the Xlm ( in both directions ) belonging to the irrep of the class.
  subroutine SpherComputeTransfMat( SpherSet )
    !
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    !
    type(ClassGroup), pointer :: Group
    type( ClassIrrep ), pointer, dimension(:) :: IrrepV
    integer :: i, TotalNumSpher, FirstInd, LastInd
    type(ClassSpherSymmetricSet), allocatable :: AllSpherSet(:)
    integer, allocatable :: TotalMonomialIndexes(:,:), PartialMonInd(:,:), TotalXlmIndexes(:,:), Veeec(:,:)
    complex(kind(1d0)), allocatable :: XlmToSpherArray(:,:), SpherToXlmArray(:,:)
    integer :: ii, jj
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The symmetric spherical set has not been initialized and the transformation matrices cannot be computed." )
    end if
    !
!!$    do ii = 1, size(SpherSet%MonomialList)
!!$       write(*,*) "+",ii, SpherSet%MonomialList(ii)%ix,SpherSet%MonomialList(ii)%iy,SpherSet%MonomialList(ii)%iz
!!$    end do
!!$    do ii = 1, size(SpherSet%SymMonomialList)
!!$       write(*,*) "*",ii, SpherSet%SymMonomialList(ii)%ix,SpherSet%SymMonomialList(ii)%iy,SpherSet%SymMonomialList(ii)%iz
!!$    end do
!!$stop
    ! If equal then the two list of classes are the same, appart maybe the ordering, because previously in initialization was assured that SpherSet%MonomialList is a subset of SpherSet%SymMonomialList.
    if ( size(SpherSet%MonomialList) == size(SpherSet%SymMonomialList) ) then
       !
       ! Gets all the spherical monomials of the point group, taking into account all the irreducible representations.
       !
       Group => SpherSet%Irrep%GetGroup()
       !
       IrrepV => Group%GetIrrepList()
       !
       allocate( AllSpherSet(size(IrrepV)) )
       !
       TotalNumSpher = 0
       !
       do i = 1, size(IrrepV)
          !
          call AllSpherSet(i)%Init( SpherSet%Lmax, IrrepV(i) )
          !
          TotalNumSpher = TotalNumSpher + AllSpherSet(i)%GetN()
          !
       end do
       !
       allocate( TotalMonomialIndexes(3,TotalNumSpher) )
       !
       FirstInd = 1
       !
       do i = 1, size(IrrepV)
          !
          if ( AllSpherSet(i)%GetN() > 0 ) then
             !
             call AllSpherSet(i)%FetchMonomialExponents( PartialMonInd )
             !
             LastInd = FirstInd + AllSpherSet(i)%GetN() - 1
             !
             TotalMonomialIndexes(:,FirstInd:LastInd) = PartialMonInd(:,:)
             !
             FirstInd = LastInd + 1
             !
             deallocate( PartialMonInd )
             !
          end if
          !
       end do
       !
    else
       !
       call SpherSet%FetchMonomialExponents( TotalMonomialIndexes )
       !
    end if
    !
    call SpherReorder( TotalMonomialIndexes )
    !
!!$  !  call BuildTransMatfromXlmToSpher( TotalMonomialIndexes, TotalXlmIndexes, XlmToSpherArray )
    call BuildTransMatfromXlmToXlm( TotalMonomialIndexes, TotalXlmIndexes, XlmToSpherArray )
    !
    if ( allocated(SpherSet%XlmTransfIndexes) ) deallocate ( SpherSet%XlmTransfIndexes )
    allocate( SpherSet%XlmTransfIndexes, source = TotalXlmIndexes )
    !
    !
!!$ !   call BuildTransMatfromSpherToXlm( XlmToSpherArray, SpherToXlmArray)
    allocate( SpherToXlmArray, source = XlmToSpherArray )
    !
    call SpherSet%SetXlmToSpherArray( TotalMonomialIndexes, XlmToSpherArray )
    !
    call SpherSet%SetSpherToXlmArray( TotalMonomialIndexes, SpherToXlmArray )
    !
  end subroutine SpherComputeTransfMat




  subroutine SetXlmToCartArray( CartSet, AllMonInd, XlmToCartArray )
    !
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    !> The array of indexes of all the cartesians monomials accesible to the point group.
    integer,                           intent(in)    :: AllMonInd(:,:)
    complex(kind(1d0)),                intent(in)    :: XlmToCartArray(:,:)
    !
    integer, allocatable :: MonExp(:,:)
    integer :: i, NumCart, j
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The symmetric cartesian class has not been initialized, imposible to set transformation matrix." )
    end if
    !
    NumCart = CartSet%GetN()
    !
    if ( allocated(CartSet%XlmToCartArray) ) deallocate( CartSet%XlmToCartArray )
    !
    allocate( CartSet%XlmToCartArray(NumCart,size(XlmToCartArray,2)) )
    !
    call CartSet%FetchMonomialExponents( MonExp )
    !
    do i = 1, NumCart
       do j = 1, size(AllMonInd,2)
       !
          if ( (MonExp(1,i)==AllMonInd(1,j)) .and.&
               (MonExp(2,i)==AllMonInd(2,j)) .and.&
               (MonExp(3,i)==AllMonInd(3,j)) ) then
             !
             CartSet%XlmToCartArray(i,:) = XlmToCartArray(j,:)
             !
          end if
       !
       end do
    end do
    !
  end subroutine SetXlmToCartArray



  subroutine SetXlmToSpherArray( SpherSet, AllMonInd, XlmToSpherArray )
    !
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    !> The array of indexes of all the spherical monomials accesible to the point group.
    integer,                       intent(in)    :: AllMonInd(:,:)
    complex(kind(1d0)),            intent(in)    :: XlmToSpherArray(:,:)
    !
    integer, allocatable :: MonExp(:,:)
    integer :: i, NumSpher, j, ii
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The symmetric spherical class has not been initialized, imposible to set transformation matrix." )
    end if
    !
    NumSpher = SpherSet%GetN()
    !
    if ( allocated(SpherSet%XlmToSpherArray) ) deallocate( SpherSet%XlmToSpherArray )
    !
    allocate( SpherSet%XlmToSpherArray(NumSpher,size(XlmToSpherArray,2)) )
    !
    call SpherSet%FetchMonomialExponents( MonExp )
    !
    do i = 1, NumSPher
       do j = 1, size(AllMonInd,2)
       !
          if ( (MonExp(1,i)==AllMonInd(1,j)) .and.&
               (MonExp(2,i)==AllMonInd(2,j)) .and.&
               (MonExp(3,i)==AllMonInd(3,j)) ) then
             !
             SpherSet%XlmToSpherArray(i,:) = XlmToSpherArray(j,:)
             !
          end if
          !
       end do
    end do
    !
    !
  end subroutine SetXlmToSpherArray




  subroutine SetCartToXlmArray( CartSet, AllMonInd, CartToXlmArray )
    !
    class(ClassCartesianSymmetricSet), intent(inout) :: CartSet
    !> The array of indexes of all the cartesians monomials accesible to the point group.
    integer,                           intent(in)    :: AllMonInd(:,:)
    complex(kind(1d0)),                intent(in)    :: CartToXlmArray(:,:)
    !
    integer, allocatable :: MonExp(:,:)
    integer :: i, NumCart, j
    !
    if ( .not. CartSet%Initialized() ) then
       call Assert( "The symmetric cartesian class has not been initialized, imposible to set transformation matrix." )
    end if
    !
    NumCart = CartSet%GetN()
    !
    if( allocated(CartSet%CartToXlmArray) ) deallocate( CartSet%CartToXlmArray )
    !
    allocate( CartSet%CartToXlmArray(size(CartToXlmArray,1),NumCart) )
    !
    call CartSet%FetchMonomialExponents( MonExp )
    !
    do i = 1, NumCart
       do j = 1, size(AllMonInd,2)
       !
          if ( (MonExp(1,i)==AllMonInd(1,j)) .and.&
               (MonExp(2,i)==AllMonInd(2,j)) .and.&
               (MonExp(3,i)==AllMonInd(3,j)) ) then
             !
             CartSet%CartToXlmArray(:,i) = CartToXlmArray(:,j)
             !
          end if
       !
       end do
    end do
    !
  end subroutine SetCartToXlmArray



  subroutine SetSpherToXlmArray( SpherSet, AllMonInd, SpherToXlmArray )
    !
    class(ClassSpherSymmetricSet), intent(inout) :: SpherSet
    !> The array of indexes of all the spherical monomials accesible to the point group.
    integer,                       intent(in)    :: AllMonInd(:,:)
    complex(kind(1d0)),            intent(in)    :: SpherToXlmArray(:,:)
    !
    integer, allocatable :: MonExp(:,:)
    integer :: i, NumSpher, j
    !
    if ( .not. SpherSet%Initialized() ) then
       call Assert( "The symmetric spherical class has not been initialized, imposible to set transformation matrix." )
    end if
    !
    NumSpher = SpherSet%GetN()
    !
    if( allocated(SpherSet%SpherToXlmArray) ) deallocate( SpherSet%SpherToXlmArray )
    !
    allocate( SpherSet%SpherToXlmArray(size(SpherToXlmArray,1),NumSpher) )
    !
    call SpherSet%FetchMonomialExponents( MonExp )
    !
    do i = 1, NumSpher
       do j = 1, size(AllMonInd,2)
          !
          if ( (MonExp(1,i)==AllMonInd(1,j)) .and.&
               (MonExp(2,i)==AllMonInd(2,j)) .and.&
               (MonExp(3,i)==AllMonInd(3,j)) ) then
             !
             SpherSet%SpherToXlmArray(:,i) = SpherToXlmArray(:,j)
             !
          end if
          !
       end do
    end do
    !
  end subroutine SetSpherToXlmArray


  
  
  !> Given a monomial indexes matrix, containing in the first, the second and the third rows the x, y and z exponents respectively ( ix, iy, iz ), and in each column storing a diferent monomial; this subroutines reorders the columns in a way that the columns (monomials) are sorted in an increasing semi-monotonic fashion of ix+iy+iz = l. 
  subroutine Reorder( MonInd )
    !
    integer, intent(inout) :: MonInd(:,:)
    !
    integer :: i, j, k, Counter
    integer :: NumCart
    integer, allocatable :: Sum(:), OldSum(:), ForbiddenIndexes(:)
    integer, allocatable ::  NewMonInd(:,:)
    logical :: NewMon
    !
    NumCart = size(MonInd,2)
    !
    allocate( Sum(NumCart) )
    !
    do i = 1, NumCart
       !
       Sum(i) = MonInd(1,i) + MonInd(2,i) + MonInd(3,i)
       !
    end do
    !
    allocate( OldSum, source = Sum )
    !
    allocate( ForbiddenIndexes(NumCart) )
    ForbiddenIndexes = 0
    !
    allocate( NewMonInd(3,NumCart) )
    !
    !> Intel subroutine to sort a vector of integers in ascending order.
    ! replaced with isort so that gfrotran can be used
    !call SORTQQ( LOC(Sum), NumCart, SRT$INTEGER8 )
    call ISORT( Sum, Sum, NumCart, 1 )
    !
    Counter = 0
    !
    first : do i = 1, NumCart
       do j = 1, NumCart
          !
          if ( Sum(i) == OldSum(j) ) then
             !
             NewMon = .false.
             !
             do k = 1, NumCart
                if ( j == ForbiddenIndexes(k) ) exit
                if ( k == NumCart ) NewMon = .true.
             end do
             !
             if ( NewMon ) then
                NewMonInd(:,i) = MonInd(:,j) 
                Counter = Counter + 1
                ForbiddenIndexes(Counter) = j
                cycle first
             end if
             !
          end if
          !
       end do
    end do first
    !
    MonInd = NewMonInd
    !
  end subroutine Reorder



  !> Given a monomial indexes matrix, containing in the first, the second and the third rows the n,l and m values of r^nYlm  respectively, and in each column storing a diferent monomial; this subroutines reorders the columns in a way that the columns (monomials) are sorted in an increasing order of n, l and m with that priority (i.e.: 000,11-1,110,111,200,22-2,...). If l is even n has to be even too, the same occurs when it are odd. 
  subroutine SpherReorder( MonInd )
    !
    integer, intent(inout) :: MonInd(:,:)
    !
    integer :: n, l, m, MaxN, Counter, i
    integer :: NumSpher, PosN, PosL, PosM
    integer, allocatable ::  CopyMonInd(:,:), NewMonInd(:,:)
    character(len=32) :: Cstrn, Nstrn
    !
    NumSpher = size(MonInd,2)
    !
    allocate( CopyMonInd, source = MonInd )
    allocate( NewMonInd(3,NumSpher) )
    !
    MaxN = maxval( CopyMonInd(1,:) )
    !
    Counter = 0
    !
    ! Assumes the monomial are not repeated.
    do n = 0, MaxN
       do l = 0, n
          if (mod(n-l,2) == 0 ) then
             do m = -l, l
                !
                do i = 1, NumSpher
                   if ( (n == CopyMonInd(1,i)) .and. &
                        (l == CopyMonInd(2,i)) .and. &
                        (m == CopyMonInd(3,i)) )  then
                      Counter = Counter + 1
                      NewMonInd (1,Counter) = n
                      NewMonInd (2,Counter) = l
                      NewMonInd (3,Counter) = m
                      exit
                   end if
                end do
                !
             end do
          end if
       end do
    end do
    !
    write(Cstrn,*) Counter
    write(Nstrn,*) NumSpher
    if ( Counter /= NumSpher ) then
       call Assert( "The number of spherical monomials after the ordering "//trim(adjustl(Cstrn))//&
         " is not the same than before "//trim(adjustl(Nstrn))//"." )
    end if
    !
    !
    MonInd = NewMonInd
    !
  end subroutine SpherReorder




  subroutine PrintsComplexMatrixOnScreen( Mat )
    !
    complex(kind(1d0)), intent(in) :: Mat(:,:)
    !
    integer :: i, j, unit
    real(kind(1d0)) :: RealPart, ImagPart
    !
    unit = OUTPUT_UNIT
    !
    do j = 1, size(Mat,2)
       do i = 1, size(Mat,1)
          !
          RealPart = dble(Mat(i,j))
          ImagPart = aimag(Mat(i,j))
          write(unit,fmt='(2i2,2f24.16)') i, j, RealPart, ImagPart
          !
       end do
    end do
    !
  end subroutine PrintsComplexMatrixOnScreen



  subroutine PrintsIntegerMatrixOnScreen( Mat )
    !
    integer, intent(in) :: Mat(:,:)
    !
    integer :: i, j, unit
    !
    unit = OUTPUT_UNIT
    !
    do j = 1, size(Mat,2)
       do i = 1, size(Mat,1)
          !
          write(unit,fmt='(2i2,i6)') i, j, Mat(i,j)
          !
       end do
    end do
    !
  end subroutine PrintsIntegerMatrixOnScreen




  subroutine ClassXlmShow( Xlm, unit )
    !
    class(ClassXlm),   intent(in) :: Xlm
    integer, optional, intent(in) :: unit
    !
    integer :: outunit
    !
    if ( present(unit) ) then
       outunit = unit
    else
       outunit = OUTPUT_UNIT
    end if
    !
    write(outunit,fmt="(a,a,i4,4x,a,i4)") "Xlm info : ", "l", Xlm%Getl(), "m", Xlm%Getm()
    !
  end subroutine ClassXlmShow



  subroutine ClassXlmSave( Xlm, unit )
    !
    class(ClassXlm),   intent(in) :: Xlm
    integer,           intent(in) :: unit
    !
    write(unit,*) Xlm%Getl(), Xlm%Getm()
    !
  end subroutine ClassXlmSave


  subroutine ClassXlmLoad( Xlm, unit )
    !
    class(ClassXlm),   intent(inout) :: Xlm
    integer,           intent(in) :: unit
    !
    read(unit,*) Xlm%l, Xlm%m
    !
  end subroutine ClassXlmLoad



  !> Provided an Xlm Class retrieves whether it appears in the Xlm symmetric Set (true) or not (false). 
  logical function ClassXlmSymmetricSetValidXlm( XlmSymSet, Xlm ) result( Valid )
    !
    class(ClassXlmSymmetricSet), intent(in) :: XlmSymSet
    class(ClassXlm),             intent(in) :: Xlm
    !
    integer :: i
    type(ClassXlm) :: XlmTest
    Valid = .false.
    do i = 1, XlmSymSet%GetNXlm()
       XlmTest = XlmSymSet%GetXlm(i)
       if ( Xlm%Is(XlmTest) ) then
          Valid = .true.
          return
       end if
    end do
    !
  end function ClassXlmSymmetricSetValidXlm


  !> Retrieves True if the two Xlm classes coincide and False if do not.
  logical function ClassXlmIs( Xlm, XlmTest ) result( Same )
    !
    class(ClassXlm), intent(in) :: Xlm
    class(ClassXlm), intent(in) :: XlmTest
    !
    Same = .false.
    if ( (Xlm%GetL() == XlmTest%GetL()) .and. &
         (Xlm%GetM() == XlmTest%GetM()) ) Same = .true. 
    !
  end function ClassXlmIs


  !> Gets the label of the Xlm functions in the form "Xl.m".
  function ClassXlmGetLabel( Xlm ) result( Label )
    !
    class(ClassXlm), intent(inout) :: Xlm
    character(len=:), allocatable :: Label
    !
    character(len=32) :: lstrn, mstrn
    !
    write(lstrn,*) Xlm%GetL()
    write(mstrn,*) Xlm%GetM()
    !
    allocate( Label, source = "X"//trim(adjustl(lstrn))//"."//trim(adjustl(mstrn)) )
    !
  end function ClassXlmGetLabel



  subroutine ClassXlmSymmetricSetFree( Self )
    class(ClassXlmSymmetricSet), intent(inout) :: Self
    Self%Lmax = -1
    if ( allocated(Self%dim) ) deallocate(Self%dim)
    if ( allocated(Self%idim) ) deallocate(Self%idim)
    Self%Irrep => NULL()
    Self%NumXlm = -1
    if ( allocated(Self%XlmList) ) deallocate(Self%XlmList)
  end subroutine ClassXlmSymmetricSetFree

  subroutine ClassXlmSymmetricSetFinal( Self )
    type(ClassXlmSymmetricSet) :: Self
    call Self%Free()
  end subroutine ClassXlmSymmetricSetFinal

  subroutine ClassCartesianSymmetricSetFree( Cart )
    !
    class(ClassCartesianSymmetricSet), intent(inout) :: Cart
    !
    Cart%Lmax = -1
    Cart%Irrep => NULL()
    Cart%NumCart = 0
    if ( allocated(Cart%MonomialList) ) deallocate( Cart%MonomialList )
    if ( allocated(Cart%SymMonomialList) ) deallocate( Cart%SymMonomialList )
    if ( allocated(Cart%XlmToCartArray) ) deallocate( Cart%XlmToCartArray )
    if ( allocated(Cart%CartToXlmArray) ) deallocate( Cart%CartToXlmArray )
    if ( allocated(Cart%XlmTransfIndexes) ) deallocate( Cart%XlmTransfIndexes )
    !
  end subroutine ClassCartesianSymmetricSetFree




  subroutine ClassSpherSymmetricSetFree( Spher )
    !
    class(ClassSpherSymmetricSet), intent(inout) :: Spher
    !
    Spher%Lmax = -1
    Spher%Irrep => NULL()
    Spher%NumSpher = 0
    if ( allocated(Spher%MonomialList) ) deallocate( Spher%MonomialList )
    if ( allocated(Spher%SymMonomialList) ) deallocate( Spher%SymMonomialList )
    if ( allocated(Spher%XlmToSpherArray) ) deallocate( Spher%XlmToSpherArray )
    if ( allocated(Spher%SpherToXlmArray) ) deallocate( Spher%SpherToXlmArray )
    if ( allocated(Spher%XlmTransfIndexes) ) deallocate( Spher%XlmTransfIndexes )
    !
  end subroutine ClassSpherSymmetricSetFree



  subroutine ClassCartesianSymmetricSetFinal( Cart ) 
    type(ClassCartesianSymmetricSet) :: Cart
    call Cart%Free()
  end subroutine ClassCartesianSymmetricSetFinal


  subroutine ClassSpherSymmetricSetFinal( Spher ) 
    type(ClassSpherSymmetricSet) :: Spher
    call Spher%Free()
  end subroutine ClassSpherSymmetricSetFinal




  subroutine CheckMonomialList( ExtMonList, SymMonList )
    !> Corresponds to the external list of monomials.
    type(ClassMonomial), intent(in) :: ExtMonList(:)
    !> Corresponds to the symmetry allowed list of monomials.
    type(ClassMonomial), intent(in) :: SymMonList(:)
    !
    integer :: NumExt, NumSym, i
    integer, allocatable :: MonVec(:)
    character(len=32) :: xstrn, ystrn, zstrn
    !
    NumExt = size(ExtMonList)
    NumSym = size(SymMonList)
    !
    !*** It should be an assertion, for testing porposes is passed as error message.
    if ( NumExt > NumSym ) then
       call ErrorMessage( "The number of external cartesian monomials cannot be "//&
            "higher than the number of monomials allowed by the symmetry, "//&
            "Num read: "//AlphabeticNumber(NumExt)//&
            " Num init: "//AlphabeticNumber(NumSym))
    end if
    !
    do i = 1, NumExt
       if ( .not. ExtMonList(i)%IsInList( SymMonList ) ) then
          call ExtMonList(i)%FetchMonomialExponents( MonVec )
          write(xstrn,*) MonVec(1)
          write(ystrn,*) MonVec(2)
          write(zstrn,*) MonVec(3)
          !*** It should be an assertion, for testing porposes is passed as error message.
          call ErrorMessage( "The Xlm with with l and m : "//trim(adjustl(ystrn))//" and "//trim(adjustl(zstrn))//&
            " respectively do not belongs to those allowed by the current symmetry." )
       end if
    end do
    !
  end subroutine CheckMonomialList



  integer function FindValIndex( IntArray, IntVal )
    !
    integer, intent(in) :: IntArray(:)
    integer, intent(in) :: IntVal
    !
    integer :: i
    !
    ! Meaning it did not find the value
    FindValIndex = -1
    !
    do i = 1, size(IntArray)
       if ( IntVal == IntArray(i) ) then
          FindValIndex = i
          return
       end if
    end do
    !
  end function FindValIndex


  
end module ModuleSymmetryAdaptedSphericalHarmonics
