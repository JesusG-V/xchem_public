!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> Declares some utility routines based on system calls.
module ModuleSystemUtils

  implicit none

  private

  public :: GetTime


  interface 
     DoublePrecision function D2DFun( x ) 
       DoublePrecision, intent(in) :: x
     end function D2DFun
  end interface
  public D2DFun
  
  interface realloc
     module procedure realloc_integer_vec
     module procedure realloc_dble_vec
     module procedure realloc_complex_vec
     module procedure realloc_integer_mat2
     module procedure realloc_dble_mat2
     module procedure realloc_complex_mat2
  end interface realloc
  public realloc

contains

  subroutine realloc_integer_vec(vec,n)
    integer, allocatable, intent(inout) :: vec(:)
    integer             , intent(in) :: n
    if(allocated(vec))then
       if(size(vec,1)/=n)then
          deallocate(vec)
          allocate(vec(n))
       endif
    else
       allocate(vec(n))
    endif
    vec=0
  end subroutine realloc_integer_vec

  subroutine realloc_dble_vec(vec,n)
    real(kind(1d0)), allocatable, intent(inout) :: vec(:)
    integer             , intent(in) :: n
    if(allocated(vec))then
       if(size(vec,1)/=n)then
          deallocate(vec)
          allocate(vec(n))
       endif
    else
       allocate(vec(n))
    endif
    vec=0.d0
  end subroutine realloc_dble_vec
  
  subroutine realloc_complex_vec(vec,n)
    complex(kind(1d0)), allocatable, intent(inout) :: vec(:)
    integer             , intent(in) :: n
    if(allocated(vec))then
       if(size(vec,1)/=n)then
          deallocate(vec)
          allocate(vec(n))
       endif
    else
       allocate(vec(n))
    endif
    vec=(0.d0,0.d0)
  end subroutine realloc_complex_vec

  subroutine realloc_integer_mat2(mat,n1,n2)
    integer, allocatable, intent(inout) :: mat(:,:)
    integer             , intent(in) :: n1,n2
    if(allocated(mat))then
       if(size(mat,1)/=n1.or.size(mat,2)/=n2)then
          deallocate(mat)
          allocate(mat(n1,n2))
       endif
    else
       allocate(mat(n1,n2))
    endif
    mat=0
  end subroutine realloc_integer_mat2

  subroutine realloc_dble_mat2(mat,n1,n2)
    real(kind(1d0)), allocatable, intent(inout) :: mat(:,:)
    integer             , intent(in) :: n1,n2
    if(allocated(mat))then
       if(size(mat,1)/=n1.or.size(mat,2)/=n2)then
          deallocate(mat)
          allocate(mat(n1,n2))
       endif
    else
       allocate(mat(n1,n2))
    endif
    mat=0.d0
  end subroutine realloc_dble_mat2

  subroutine realloc_complex_mat2(mat,n1,n2)
    complex(kind(1d0)), allocatable, intent(inout) :: mat(:,:)
    integer             , intent(in) :: n1,n2
    if(allocated(mat))then
       if(size(mat,1)/=n1.or.size(mat,2)/=n2)then
          deallocate(mat)
          allocate(mat(n1,n2))
       endif
    else
       allocate(mat(n1,n2))
    endif
    mat=(0.d0,0.d0)
  end subroutine realloc_complex_mat2

  !> Returns the absolute CPU clock time in 
  !> seconds with a precision of (allegedly)
  !> one microsecond.
  DoublePrecision function GetTime()
    integer(8) :: count, count_rate
    call system_clock(count,count_rate)
    GetTime=dble(count)/dble(count_rate)
  end function GetTime


end module ModuleSystemUtils
