!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Parses the configuration file that contains the parameters to perform the analysis of the wave packet propagation results ([AnalPropResInputFile](@ref AnalPropResInputFile)).


module ModuleAnalPropResInputFile
    

  use ModuleParameterList

  implicit none

  private

  !> Number of Iterations per frame.
  integer , public :: Num_Iter_frame

  !> Number of energy sample points.
  integer , public :: Energy_resolution

  !> Number of radial sample points.
  integer , public :: Radial_resolution

  !> Indicates whether the eigenstates will be plotted in radial coordinates or not.
  logical , public :: Plot_Radial_Orbitals

  !> Number of momentum sample points.
  integer , public :: Moment_resolution

  !> Indicates whether the eigenstates will be plotted in momentum coordinates or not.
  logical , public :: Plot_Moment_Orbitals

  !> Number of zenithal angle sample points.
  integer , public :: Theta_resolution

  !> Number of azimuthal angle sample points.
  integer , public :: Phi_resolution

  !> Number of bound states.
  integer , public :: Nbound_states

  !> Indicates whether the negative energy eigenstates  will be plotted in energy coordinates or not.
  logical , public :: Plot_Negative_Energy_Orbitals

  !> Indicates whether the positive energy eigenstates  will be plotted in energy coordinates or not.
  logical , public :: Plot_Positive_Energy_Orbitals

  !> Format of bound states prob.
  character(len=128) , public :: Format_Bound_Prob

  !> Format of scattering states prob.
  character(len=128) , public :: Format_Cont_Prob

  !> Degree of Padé Rational Interpolation.
  integer , public :: Deg_Pade_Rat_Int

  !> Minimum plot energy.
  real(kind(1d0)) , public :: Emi_Plot

  !> Maximum plot energy.
  real(kind(1d0)) , public :: Ema_Plot

  !> Type of interpolation: RATIONAL, POLYNOMIAL or NONE.
  character(len=128) , public :: Interpolation_kind

  !> Minimum excess energy.
  real(kind(1d0)) , public :: Min_excess_energy 

  !> rho_oo(px,py)
  logical , public :: Plot_rhoa_xy

  !> ||<nl|psi>||²
  logical , public :: Plot_rhoeb

  !> ||<El|psi>||²
  logical , public :: Plot_rhoec

  !> rho(cos(theta),E)
  logical , public :: Plot_rhoebc_te

  !> rho_l(r)
  logical , public :: Plot_rhorr

  !> rho(cos(theta),r)
  logical , public :: Plot_rhor_tr

  !> rho(x,y)
  logical , public :: Plot_rhor_xy

  !> rho(px,py)
  logical , public :: Plot_rhop_xy

  !> rho_c(px,py)
  logical , public :: Plot_rhopc_xy

  !> rho_c(cos(theta),p^2/2)
  logical , public :: Plot_rhopc_te


  public ParseAnalPropResInputFile


contains

  
  !> Parses the [PropagationInputFile](@ref PropagationInputFile).
  subroutine ParseAnalPropResInputFile( ConfigurationFile )
    !
    use, intrinsic :: ISO_FORTRAN_ENV
    !
    character(len=*) , intent(in) :: ConfigurationFile
    !
    type(ClassParameterList) :: List
    integer :: i
    !
    call List%Add( "Num_Iter_frame"      ,1     , "required" )
    call List%Add( "Energy_resolution"   ,1     , "required" )
    call List%Add( "Radial_resolution"   ,1     , "required" )
    call List%Add( "Plot_Radial_Orbitals",.true., "required" )
    call List%Add( "Moment_resolution"   ,1     , "required" )
    call List%Add( "Plot_Moment_Orbitals",.true., "optional" )
    call List%Add( "Theta_resolution"    ,1     , "required" )
    call List%Add( "Phi_resolution"      ,1     , "required" )
    call List%Add( "Nbound_states"       ,1     , "required" )
    call List%Add( "Plot_Negative_Energy_Orbitals",.true., "required" )
    call List%Add( "Plot_Positive_Energy_Orbitals",.true., "required" )
    call List%Add( "Format_Bound_Prob"   ,"A"   , "optional" )
    call List%Add( "Format_Cont_Prob"    ,"A"   , "optional" )
    call List%Add( "Deg_Pade_Rat_Int"    ,1     , "required" )
    call List%Add( "Emi_Plot"            ,1d0   , "required" )
    call List%Add( "Ema_Plot"            ,1d0   , "required" )
    call List%Add( "Interpolation_kind"  ,"A"   , "required" )
    call List%Add( "Min_excess_energy"   ,1d0   , "optional" )
    call List%Add( "Plot_rhoeb"          ,.true., "required" )
    call List%Add( "Plot_rhoa_xy"        ,.true., "required" )
    call List%Add( "Plot_rhoec"          ,.true., "required" )
    call List%Add( "Plot_rhoebc_te"      ,.true., "required" )
    call List%Add( "Plot_rhorr"          ,.true., "required" )
    call List%Add( "Plot_rhor_tr"        ,.true., "required" )
    call List%Add( "Plot_rhor_xy"        ,.true., "required" )
    call List%Add( "Plot_rhop_xy"        ,.true., "optional" )
    call List%Add( "Plot_rhopc_xy"       ,.true., "optional" )
    call List%Add( "Plot_rhopc_te"       ,.true., "optional" )
    !
    call List%Parse( ConfigurationFile )
    !
    call List%Get( "Num_Iter_frame"         , Num_Iter_frame  )
    call List%Get( "Energy_resolution"      , Energy_resolution )
    call List%Get( "Radial_resolution"      , Radial_resolution  )
    call List%Get( "Plot_Radial_Orbitals"   , Plot_Radial_Orbitals  )
    call List%Get( "Moment_resolution"      , Moment_resolution  )
    call List%Get( "Plot_Moment_Orbitals"   , Plot_Moment_Orbitals  )
    call List%Get( "Theta_resolution"       , Theta_resolution  )
    call List%Get( "Phi_resolution"         , Phi_resolution  )
    call List%Get( "Nbound_states"          , Nbound_states  )
    call List%Get( "Plot_Negative_Energy_Orbitals", Plot_Negative_Energy_Orbitals  )
    call List%Get( "Plot_Positive_Energy_Orbitals", Plot_Positive_Energy_Orbitals  )
    call List%Get( "Format_Bound_Prob"      , Format_Bound_Prob )
    call List%Get( "Format_Cont_Prob"       , Format_Cont_Prob  )
    call List%Get( "Deg_Pade_Rat_Int"       , Deg_Pade_Rat_Int  )
    call List%Get( "Emi_Plot"               , Emi_Plot  )
    call List%Get( "Ema_Plot"               , Ema_Plot  )
    call List%Get( "Interpolation_kind"     , Interpolation_kind  )
    call List%Get( "Min_excess_energy"      , Min_excess_energy  )
    call List%Get( "Plot_rhoa_xy"           , Plot_rhoa_xy  )
    call List%Get( "Plot_rhoeb"             , Plot_rhoeb  )
    call List%Get( "Plot_rhoec"             , Plot_rhoec  )
    call List%Get( "Plot_rhoebc_te"         , Plot_rhoebc_te  )
    call List%Get( "Plot_rhorr"             , Plot_rhorr  )
    call List%Get( "Plot_rhor_tr"           , Plot_rhor_tr   )
    call List%Get( "Plot_rhor_xy"           , Plot_rhor_xy  )
    call List%Get( "Plot_rhop_xy"           , Plot_rhop_xy  )
    call List%Get( "Plot_rhopc_xy"          , Plot_rhopc_xy  )
    call List%Get( "Plot_rhopc_te"          , Plot_rhopc_te  )
    !
  end subroutine ParseAnalPropResInputFile






end module ModuleAnalPropResInputFile
