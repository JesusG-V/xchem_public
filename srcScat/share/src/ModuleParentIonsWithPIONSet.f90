!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> Define the classes for the individual parent ions, for the set of
!! parent ions and for the multipoles between them.
!! Furthermore, it defines a global variable for the complete set of
!! parent ions as well as
!! - a configuration file for the parent ions
!! - the files that contain information on individual parent ions (from interface)
!! - the files with the mutipoles (from interface)
module ModuleParentIons

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleString
  use ModuleGroups
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModulePrepareFilesSystem

  implicit none

  private

  character(len=*), public, parameter :: PARENTION_DIR_NAME = "ParentIons/"
  character(len=*), parameter :: PARENTION_ROOT_NAME = "ParentIon_"
  character(len=*), parameter :: MULTIPOLE_ROOT_NAME = "CartesianMultipoles_"


  !> The parent ions are identified by 
  !! - point group (which must be set once and for all at the very beginning)
  !! - multiplicity (if the spin is well defined, multiplicity >=1, &
  !!                 otherwise multiplicity =0)
  !! - irreducible representation, in the order provided by the list of the 
  !!   group class
  !! - number, which is assigned externally by the quantum chemistry program
  !!   and which does not necessarily reflect the energy order of the states
  !!   (due to the fact that the group used here may be just a sub-group of
  !!   the real molecular group, states with the same symmetry could actually
  !!   cross, thus altering the order of their energies).
  type, public :: ClassParentIon
     ! {{{ private attributes
     private
     real(kind(1d0))           :: Energy
     integer                   :: Charge
     integer                   :: Multiplicity
     type(ClassIrrep), pointer :: Irrep
     integer                   :: N
     !> Number of electrons in the parent ion.
     integer, public                  :: NePI
     ! }}}
   contains
     generic, public :: init       => ClassParentIonInit
     generic, public :: free       => ClassParentIonFree
     generic, public :: show       => ClassParentIonShow
     generic, public :: GetEnergy  => ClassParentIonGetEnergy
     generic, public :: GetCharge  => ClassParentIonGetCharge
     generic, public :: GetMultiplicity => ClassParentIonGetMultiplicity
     generic, public :: GetIrrep   => ClassParentIonGetIrrep
     generic, public :: GetNumber  => ClassParentIonGetNumber
     generic, public :: GetLabel   => ClassParentIonGetLabel
     generic, public :: LoadData   => ClassParentIonLoadData
!!$     procedure, public :: SetNePI    => ClassParentIonSetNePI
!!$     generic, public :: write      => ClassParentIonWrite
!!$     generic, public :: read       => ClassParentIonRead
     ! {{{ private procedures

     procedure, private :: ClassParentIonInit
     procedure, private :: ClassParentIonFree
     procedure, private :: ClassParentIonShow
!!$     procedure, private :: ClassParentIonWrite
!!$     procedure, private :: ClassParentIonRead
     procedure, private :: ClassParentIonGetEnergy
     procedure, private :: ClassParentIonGetCharge
     procedure, private :: ClassParentIonGetMultiplicity
     procedure, private :: ClassParentIonGetIrrep
     procedure, private :: ClassParentIonGetNumber
     procedure, private :: ClassParentIonGetLabel
     procedure, private :: ClassParentIonLoadData
!!$     procedure, private :: ClassParentIonSetNePI
     final :: ClassParentIonFinal

     ! }}}
  end type ClassParentIon


  !> Value of the multipoles with a given angular momentum
  !! and symmetry, between two fixed parent ions.
  type, public :: ClassMultipole
     ! {{{ private attributes

     private
     integer                          :: Lmax
     type(ClassIrrep), pointer        :: Irrep
     character(len=:), allocatable    :: Dir
     character(len=:), allocatable    :: File
     character(len=:), allocatable    :: CartFile
     type(ClassCartesianSymmetricSet) :: CartesianSet
     integer                          :: Nmultipoles
     real(kind(1d0)), allocatable     :: CartesianMultipole(:)
     type(ClassXlmSymmetricSet)       :: XlmSymSet
     complex(kind(1d0)), allocatable  :: XlmMultipole(:)
     !
     !.. Comment: the use of objects for seemingly simple variables
     !   like the Xlm multipoles or the cartesian multipoles is 
     !   justified on the basis that the conversion between them
     !   ought to be well defined and managed by a separate module.
     !

     ! }}}
   contains
     generic, public :: setLMax  => ClassMultipoleSetLMax
     generic, public :: setIrrep => ClassMultipoleSetIrrep
     generic, public :: setDir  => ClassMultipoleSetDir
     generic, public :: setFile  => ClassMultipoleSetFile
     generic, public :: setCartFile  => ClassMultipoleSetCartFile
     generic, public :: setEmpty => ClassMultipoleSetEmpty
     generic, public :: Init     => ClassMultipoleInit
     generic, public :: LoadMultipole => ClassMultipoleLoadMultipole
     generic, public :: LoadCartesianData => ClassMultipoleLoadCartesianData
     generic, public :: GetN     => ClassMultipoleGetN
     procedure, public :: GetMultipole     =>  ClassMultipoleGetMultipole
     generic, public :: setCartesianValue => ClassMultipoleSetCartesianValue
     generic, public :: show         => ClassMultipoleShow
     procedure, public :: ComputeAndSaveXlmMultipoles => ClassMultipoleComputeAndSaveXlmMultipoles
!!$     generic, public :: free         => ClassMultipoleFree
!!$     generic, public :: write        => ClassMultipoleWrite
!!$     generic, public :: read         => ClassMultipoleRead
!!$     generic, public :: GetIrrep     => ClassMultipoleGetIrrep
!!$     generic, public :: GetXlmValue  => ClassMultipoleGetXlmValue
     ! {{{ private procedures

     procedure, private :: ClassMultipoleSetLMax
     procedure, private :: ClassMultipoleSetIrrep
     procedure, private :: ClassMultipoleSetDir
     procedure, private :: ClassMultipoleSetFile
     procedure, private :: ClassMultipoleSetCartFile
     procedure, private :: ClassMultipoleSetEmpty
     procedure, private :: ClassMultipoleInit
     procedure, private :: ClassMultipoleLoadMultipole
     procedure, private :: ClassMultipoleLoadCartesianData
     procedure, private :: ClassMultipoleGetN
     procedure, private :: ClassMultipoleGetMultipole
     procedure, private :: ClassMultipoleSetCartesianValue
     procedure, private :: ClassMultipoleShow
!!$     procedure, private :: ClassMultipoleFree
!!$     procedure, private :: ClassMultipoleWrite
!!$     procedure, private :: ClassMultipoleRead
!!$     procedure, private :: ClassMultipoleGetIrrep
!!$     procedure, private :: ClassMultipoleGetXlmValue
!!$     final :: ClassMultipoleFinal

     ! }}}
  end type ClassMultipole


  type, public :: ClassParentIonSet
     ! {{{ private attributes

     private
     !> Total number of parent ions.
     integer                           :: NParentIons
     !> list of parent ions, order according to 
     !! - spin
     !! - irrep
     !! - number
     type(ClassParentIon), allocatable :: ParentIonList(:)
     !> Maximum multipolar angular momentum between parent ions
     integer                           :: LMaxMultipole
     !> List of multipoles between parent ions
     !! Multipole( l, iBraParentIon, iKetParentIon )
     type(ClassMultipole), allocatable :: Multipole(:,:)
     !> Directory where the parent-ion data are preserved
     character(len=:), allocatable     :: DataDir
     !> Pointer to the current group
     type(ClassGroup), pointer :: Group => NULL()

     ! }}}
   contains
     generic, public :: setGroup        =>  ClassParentIonSetGroup
     generic, public :: parseFile       =>  ClassParentIonSetParseFile
     generic, public :: LoadData        =>  ClassParentIonSetLoadData
     generic, public :: show            =>  ClassParentIonSetShow
     generic, public :: GetNumberIons   =>  ClassParentIonSetGetNumberIons
     generic, public :: GetEnergy       =>  ClassParentIonSetGetEnergyFromIndex, ClassParentIonSetGetEnergyFromLabel
     generic, public :: GetLabel        =>  ClassParentIonSetGetLabelFromIndex
     generic, public :: GetIrrep        =>  ClassParentIonSetGetIrrepFromIndex, ClassParentIonSetGetIrrepFromLabel
     generic, public :: GetCharge       =>  ClassParentIonSetGetChargeFromIndex, ClassParentIonSetGetChargeFromLabel
     generic, public :: GetMultiplicity =>  ClassParentIonSetGetmultfromIndex, ClassParentIonSetGetmultfromLabel
     generic, public :: init            =>  ClassParentIonSetInit
     generic, public :: free            =>  ClassParentIonSetFree
     generic, public :: GetIndex        =>  ClassParentIonSetGetIndexFromLabel
!!$     generic, public :: write        =>  ClassParentIonSetWrite
!!$     generic, public :: read         =>  ClassParentIonSetRead
!!$     generic, public :: GetMultipole =>  ClassParentIonSetGetMultipole
     ! {{{ private procedures

     procedure, private :: ClassParentIonSetGroup
     procedure, private :: ClassParentIonSetParseFile
     procedure, private :: ClassParentIonSetInit
     procedure, private :: ClassParentIonSetFree
     procedure, private :: ClassParentIonSetShow
     procedure, private :: ClassParentIonSetGetNumberIons
     procedure, private :: ClassParentIonSetGetEnergyFromIndex
     procedure, private :: ClassParentIonSetGetEnergyFromLabel
     procedure, private :: ClassParentIonSetGetIndexFromLabel
     procedure, private :: ClassParentIonSetGetLabelFromIndex
     procedure, private :: ClassParentIonSetGetIrrepFromIndex
     procedure, private :: ClassParentIonSetGetIrrepFromLabel
     procedure, private :: ClassParentIonSetGetChargeFromIndex
     procedure, private :: ClassParentIonSetGetChargeFromLabel
     procedure, private :: ClassParentIonSetGetmultfromIndex
     procedure, private :: ClassParentIonSetGetmultfromLabel
     procedure, private :: ClassParentIonSetLoadData
!!$     procedure, private :: ClassParentIonSetWrite
!!$     procedure, private :: ClassParentIonSetRead
!!$     procedure, private :: ClassParentIonSetGetMultipole
     final :: ClassParentIonSetFinal

     ! }}}
  end type ClassParentIonSet

  public :: ParsePionLabel


contains


  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !         Procedures   ClassParentIon
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  subroutine ClassParentIonInit( ParentIon, Group, Label, iCharge )
    class(ClassParentIon), intent(inout) :: ParentIon
    type(ClassGroup)     , intent(in)    :: Group
    character(len=*)     , intent(in)    :: Label
    integer              , intent(in)    :: iCharge
    !
    integer :: Multiplicity
    character(len=8) :: IrrepLabel
    integer :: PionN
    integer, save :: ncalls = 0
    ncalls=ncalls+1
    ParentIon%Energy=dble(ncalls)
    ParentIon%Charge=iCharge
    call ParsePionLabel(Label,Multiplicity,IrrepLabel,PionN)
    if( Group%definesIrrep( trim(IrrepLabel) ) )then
       ParentIon%Multiplicity =  Multiplicity
       ParentIon%Irrep        => Group%GetIrrep( trim(IrrepLabel) )
       ParentIon%N            =  PionN
    else
       call ErrorMessage("Invalid irrep "//trim(IrrepLabel)//" for group "//Group%GetName())
       stop
    endif
  end subroutine ClassParentIonInit


  subroutine ParsePionLabel( Label, Multiplicity, Irrep, N )
    character(len=*) , intent(in)  :: Label
    integer          , intent(out) :: Multiplicity
    character(len=*) , intent(out) :: Irrep
    integer          , intent(out) :: N
    !
    integer :: iChar, iPos
    character(len=16) :: strn
    character(len=:),allocatable :: tmpLabel
    !
    allocate(tmpLabel,source=trim(adjustl(Label)))
    strn=" "
    iChar=1
    do 
       if(iChar>len_trim(tmpLabel))exit
       iPos=index("1234567890",tmpLabel(iChar:iChar))
       if(iPos<=0)exit
       iChar=iChar+1
    enddo
    iChar=iChar-1
    if(iChar<=0)call Assert("Missing multiplicity of Parent ion")

    strn=tmpLabel(1:iChar)
    read(strn,*)Multiplicity
    tmpLabel=tmpLabel(iChar+1:)
    !
    iPos=index(tmpLabel,".")
    Irrep=tmpLabel(1:iPos-1)
    strn=tmpLabel(iPos+1:)
    read(strn,*)N
    !
  end subroutine ParsePionLabel


  subroutine ClassParentIonShow( ParentIon, unit )
    class(ClassParentIon), intent(in) :: ParentIon
    integer, optional    , intent(in) :: unit
    integer :: outunit
    character(len=8) :: strn
    outunit=OUTPUT_UNIT
    if(present(unit))outunit=unit
    write(outunit,"(a)") "Parent Ion info: "
    strn=ParentIon%GetLabel()
    write(outunit,"(a)"      ,advance="no")"  "//strn
    write(outunit,"(a,1x,i2)",advance="no")"  Ne Parent Ion =",ParentIon%NePI 
    write(outunit,"(a,1x,i2)",advance="no")"  Mult =",ParentIon%GetMultiplicity() 
    write(outunit,"(a,1x,i2)",advance="no")"  Q =",ParentIon%GetCharge() 
    write(outunit,"(a,1x,d14.6)")          "  E =",ParentIon%GetEnergy()
  end subroutine ClassParentIonShow

  function ClassParentIonGetLabel( ParentIon ) result( label )
    class(ClassParentIon), intent(in) :: ParentIon
    character(len=:)    , allocatable :: label
    character(len=16) :: mstrn,nstrn,istrn
    write(mstrn,*)ParentIon%GetMultiplicity()
    mstrn=adjustl(mstrn)
    istrn=adjustl(ParentIon%irrep%GetName())
    write(nstrn,*)ParentIon%GetNumber()
    nstrn=adjustl(nstrn)
    allocate(label,source=trim(mstrn)//trim(istrn)//"."//trim(nstrn))
  end function ClassParentIonGetLabel
    
  function ClassParentIonGetMultiplicity( ParentIon ) result( multiplicity )
    class(ClassParentIon), intent(in) :: ParentIon
    integer                           :: multiplicity
    multiplicity = ParentIon%multiplicity
  end function ClassParentIonGetMultiplicity

  function ClassParentIonGetIrrep( ParentIon ) result( irrep )
    class(ClassParentIon), intent(in) :: ParentIon
    type(ClassIrrep), pointer         :: irrep
    irrep => ParentIon%irrep
  end function ClassParentIonGetIrrep

  function ClassParentIonGetNumber( ParentIon ) result( number )
    class(ClassParentIon), intent(in) :: ParentIon
    integer                           :: number
    number = ParentIon%N
  end function ClassParentIonGetNumber

  function ClassParentIonGetEnergy( ParentIon ) result( energy )
    class(ClassParentIon), intent(in) :: ParentIon
    real(kind(1d0))                   :: energy
    energy = ParentIon%Energy
  end function ClassParentIonGetEnergy

  function ClassParentIonGetCharge( ParentIon ) result( charge )
    class(ClassParentIon), intent(in) :: ParentIon
    integer                           :: charge
    charge = ParentIon%Charge
  end function ClassParentIonGetCharge

  subroutine ClassParentIonFree( ParentIon )
    class(ClassParentIon) :: ParentIon
    parentIon%Energy = 0.d0
    parentIon%Charge = 0
    parentIon%Multiplicity = 0
    parentIon%Irrep  => NULL()
    parentIon%N      = 0
    parentIon%NePI   = -1
  end subroutine ClassParentIonFree

  subroutine ClassParentIonLoadData( Pion, dir, EnergyId ) 
    class(ClassParentIon),      intent(inout) :: Pion
    character(len=*)     ,      intent(in)    :: dir
    character(len=*), intent(in)    :: EnergyId
    character(len=:), allocatable :: FileName
    character(len=IOMSG_LENGTH)::iomsg
    character(len=512) :: line
    integer :: iostat, iChar
    integer :: uid
    logical :: DataReadCorrectly
    character(len=:), allocatable :: OldPIEnergFile
    real(kind(1d0)) :: Energy
    !
    call SetFileName( FileName )
    !
    allocate( OldPIEnergFile, source = trim(dir)//"/"//PIEnergyLabel//EnergyId//OutExtension )
    open(newunit = uid, &
         file    = OldPIEnergFile, &
         form    ="formatted", &
         status  ="old",&
         action  ="read",&
         iostat  = iostat,&
         iomsg   = iomsg )
    if(iostat/=0)then
       call ErrorMessage("Cannot Find File "//OldPIEnergFile)
       call Assert(iomsg)
    endif
    !
    read(uid,fmt=*,iostat=iostat) Energy
    close(uid)
    !
    PIon%Energy = Energy
       !
!!$       open(newunit = uid, &
!!$            file    = FileName, &
!!$            form    ="formatted", &
!!$            status  ="old",&
!!$            action  ="write",&
!!$            iostat  = iostat,&
!!$            iomsg   = iomsg )
!!$       if(iostat/=0)then
!!$          call ErrorMessage("Cannot Find File "//FileName)
!!$          call Assert(iomsg)
!!$       endif
!!$       !
!!$       write(uid,fmt=*,iostat=iostat) Energy
!!$       close(uid)
       !
    !
!!$    open(newunit = uid, &
!!$         file    = FileName, &
!!$         form    ="formatted", &
!!$         status  ="old",&
!!$         action  ="read",&
!!$         iostat  = iostat,&
!!$         iomsg   = iomsg )
!!$    if(iostat/=0)then
!!$       call ErrorMessage("Cannot Find File "//FileName)
!!$       call Assert(iomsg)
!!$    endif
!!$    DataReadCorrectly = .FALSE.
!!$    do
!!$       read(uid,"(a)",iostat=iostat)line
!!$       if(iostat/=0)exit
!!$       iChar=index(line,"#")
!!$       if(iChar>=1)line=line(:iChar-1)
!!$       if(len_trim(line)==0)cycle
!!$       read(line,*,iostat=iostat)PIon%Energy
!!$       if(iostat/=0)call Assert("Error in parent-ion energy in "//FileName)
!!$       DataReadCorrectly = .TRUE.
!!$       exit
!!$    enddo
!!$    close(uid)
!!$    if( .not. DataReadCorrectly ) call Assert("Couldn't read energy in "//FileName)
    !
    !
  contains
    !
    !
    subroutine SetFileName( FileName )
      character(len=:), allocatable, intent(out) :: FileName
      integer :: iChar
      character(len=16) :: PionLabel
      iChar=len_trim(dir)
      PionLabel=Pion%GetLabel()
      if(dir(iChar:iChar)=="/")then
         allocate(FileName,source=trim(dir)//PARENTION_ROOT_NAME//trim(PionLabel)//"_Data")
      else
         allocate(FileName,source=trim(dir)//"/"//PARENTION_ROOT_NAME//trim(PionLabel)//"_Data")
      endif
    end subroutine SetFileName
    !
    !
  end subroutine ClassParentIonLoadData


  subroutine ClassParentIonFinal( ParentIon )
    type(ClassParentIon) :: ParentIon
    call ParentIon%free()
  end subroutine ClassParentIonFinal

  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !         Procedures   ClassMultipoles
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  
  subroutine ClassMultipoleSetLMax( Multipole, LMax ) 
    class(ClassMultipole), intent(inout) :: Multipole
    integer              , intent(in)    :: LMax
    Multipole%LMax = LMax
  end subroutine ClassMultipoleSetLMax


  subroutine ClassMultipoleSetIrrep( Multipole, IrrepPtr ) 
    class(ClassMultipole)    , intent(inout) :: Multipole
    class(ClassIrrep), target, intent(in)    :: IrrepPtr
    Multipole%Irrep => IrrepPtr
  end subroutine ClassMultipoleSetIrrep


  subroutine ClassMultipoleSetDir( Multipole, DirName ) 
    class(ClassMultipole), intent(inout) :: Multipole
    character(len=*)     , intent(in)    :: DirName
    if(allocated(Multipole%Dir))deallocate(Multipole%Dir)
    allocate(Multipole%Dir,source=DirName)
  end subroutine ClassMultipoleSetDir


  subroutine ClassMultipoleSetFile( Multipole, FileName ) 
    class(ClassMultipole), intent(inout) :: Multipole
    character(len=*)     , intent(in)    :: FileName
    if(allocated(Multipole%File))deallocate(Multipole%File)
    allocate(Multipole%File,source=FileName)
  end subroutine ClassMultipoleSetFile


  subroutine ClassMultipoleSetCartFile( Multipole, CartFileName ) 
    class(ClassMultipole), intent(inout) :: Multipole
    character(len=*)     , intent(in)    :: CartFileName
    if(allocated(Multipole%CartFile))deallocate(Multipole%CartFile)
    allocate(Multipole%CartFile,source=CartFileName)
  end subroutine ClassMultipoleSetCartFile


  subroutine ClassMultipoleSetEmpty( Multipole ) 
    class(ClassMultipole), intent(inout) :: Multipole
    Multipole%NMultipoles = 0
  end subroutine ClassMultipoleSetEmpty


  subroutine ClassMultipoleInit( Multipole, LMax, IrrepPtr ) 
    class(ClassMultipole)    , intent(inout) :: Multipole
    integer                  , intent(in)    :: LMax
    class(ClassIrrep), target, intent(in)    :: IrrepPtr
    call Multipole%SetLMax( LMax )
    call Multipole%SetIrrep( IrrepPtr )
    !
    call Multipole%CartesianSet%Init( LMax, IrrepPtr )
    call Multipole%CartesianSet%ComputeTransfMat()
    !
    call Multipole%XlmSymSet%Init( LMax, IrrepPtr )
    !
    Multipole%NMultipoles = Multipole%CartesianSet%GetN()
    if(allocated(Multipole%CartesianMultipole))deallocate(Multipole%CartesianMultipole)
    allocate(Multipole%CartesianMultipole(Multipole%NMultipoles))
    Multipole%CartesianMultipole = 0.d0
  end subroutine ClassMultipoleInit
  


  !> Loads the multipole of Xlm, specifying m ( l projection ), l has been already specified from the filename. The value is put in XlmMultipole, which will have only this element.
  subroutine ClassMultipoleLoadMultipole( Multipole, LProj )
    class(ClassMultipole), intent(inout) :: Multipole
    integer,               intent(in)    :: LProj
    !
    character(len=:), allocatable :: FileName
    character(len=IOMSG_LENGTH)::iomsg
    character(len=512) :: line
    integer :: iostat, iChar
    integer :: uid, m
    logical :: DataReadCorrectly
    complex(kind(1d0)) :: cMult
    ! Maximum angular momentum of 50.
    integer, parameter :: MaxAngMom = 50
    integer, allocatable :: VectorOfM(:)
    complex(kind(1d0)), allocatable :: XlmMultipolesMat(:,:)
    !
    if ( Multipole%GetN() <= 0 ) call Assert( "The multipoles have not been initialized" )
    !
    if ( allocated(Multipole%XlmMultipole) ) deallocate( Multipole%XlmMultipole )
    !
    allocate( Multipole%XlmMultipole(1) )
    !
    allocate( VectorOfM(2*MaxAngMom+1) )
    allocate( XlmMultipolesMat(2*MaxAngMom+1,Multipole%GetN()) )
    !
    call SetFileName()
    open(newunit = uid, &
         file    = FileName, &
         form    ="formatted", &
         status  ="old",&
         action  ="read",&
         iostat  = iostat,&
         iomsg   = iomsg )
    if(iostat/=0)then
       call ErrorMessage("Cannot Find File "//FileName)
       call Assert(iomsg)
    endif
    DataReadCorrectly = .FALSE.
    do
       read(uid,"(a)",iostat=iostat)line
       if(iostat/=0)exit
       iChar=index(line,"#")
       if(iChar>=1)line=line(:iChar-1)
       if(len_trim(line)==0)cycle
       read(line,*,iostat=iostat) m ,cMult
       if ( m == LProj ) then
          Multipole%XlmMultipole(1) = cMult
          DataReadCorrectly = .TRUE.
          exit
       end if
       if(iostat>0)call Assert("Error in multipole value in "//FileName)
       if ( iostat < 0 ) exit
    enddo
    close(uid)
    if( .not. DataReadCorrectly ) call Assert("Couldn't read the multipole with angular moment projection "//AlphabeticNumber(LProj)//" in "//FileName )
    !
  contains
    !
    subroutine SetFileName()
      integer :: iChar
      character(len=1024) :: tmpDir
      tmpDir=adjustl(Multipole%dir)
      call FormatDirectoryName( tmpDir )
      allocate( FileName, source = trim(tmpDir)//Multipole%File )
    end subroutine SetFileName
    !
  end subroutine ClassMultipoleLoadMultipole



  subroutine ClassMultipoleGetMultipole( Multipole, Val )
    !
    class(ClassMultipole), intent(inout) :: Multipole
    complex(kind(1d0)),    intent(out)   :: Val
    !
    if ( .not. allocated(Multipole%XlmMultipole) ) then
       call Assert( "Imposible to get the Multipole because is not allocated" )
    end if
    !
    if ( 1 /= size(Multipole%XlmMultipole) ) then
       call Assert( "It is supposed to be only one dipole for the requested Xlm function." )
    end if
    !
    Val = Multipole%XlmMultipole(1)
    !
    deallocate( Multipole%XlmMultipole )
    !
  end subroutine ClassMultipoleGetMultipole



  subroutine ClassMultipoleLoadCartesianData( Multipole, dir ) 
    class(ClassMultipole), intent(inout) :: Multipole
    character(len=*)     , intent(in)    :: dir
    character(len=:), allocatable :: CartFileName
    character(len=IOMSG_LENGTH)::iomsg
    character(len=512) :: line
    integer :: iostat, iChar
    integer :: uid, ix,iy,iz
    logical :: DataReadCorrectly
    real(kind(1d0)) :: dMult
    !
    call SetCartFileName()
    !
    open(newunit = uid, &
         file    = CartFileName, &
         form    ="formatted", &
         status  ="old",&
         action  ="read",&
         iostat  = iostat,&
         iomsg   = iomsg )
    if(iostat/=0)then
       call ErrorMessage("Cannot Find File "//CartFileName)
       call Assert(iomsg)
    endif
    !
    DataReadCorrectly = .FALSE.
    !
    do
       read(uid,"(a)",iostat=iostat)line
       if(iostat/=0)exit
       iChar=index(line,"#")
       if(iChar>=1)line=line(:iChar-1)
       if(len_trim(line)==0)cycle
       read(line,*,iostat=iostat)ix,iy,iz,dMult
       if(iostat>0)call Assert("Error in multipole value in "//CartFileName)
       if ( iostat < 0 ) exit
       call Multipole%SetCartesianValue( ix, iy, iz, dMult )
       DataReadCorrectly = .TRUE.
    enddo
    !
    close(uid)
    !
    if( .not. DataReadCorrectly ) call Assert("Couldn't read multipoles in "//CartFileName)
    !
  contains
    !
    subroutine SetCartFileName()
      integer :: iChar
      character(len=1024) :: tmpDir
      tmpDir=adjustl(dir)
      call FormatDirectoryName( tmpDir )
      allocate( CartFileName, source = trim(tmpDir)//Multipole%CartFile )
    end subroutine SetCartFileName
    !
  end subroutine ClassMultipoleLoadCartesianData




  subroutine ClassMultipoleComputeAndSaveXlmMultipoles( Multipole, L )
    !
    class(ClassMultipole), intent(inout) :: Multipole
    integer,               intent(in)    :: L
    !
    character(len=:), allocatable :: FileName
    integer :: i,j, uid, iostat
    integer, allocatable :: VectorOfM(:)
    complex(kind(1d0)), allocatable :: CartExpansionMat(:,:)
    character(len=IOMSG_LENGTH)::iomsg
    complex(kind(1d0)) :: Sum
    real(kind(1d0)) :: RealPart, ImagPart
    integer, allocatable :: MonExponents(:,:)
    integer :: ii
    !
    call SetFileName()
    !
    call Multipole%LoadCartesianData( Multipole%Dir )
    !
!!$    !***
!!$    if ( L == 4 ) then
!!$       do i = 1, Multipole%Nmultipoles
!!$          write(*,*) i, Multipole%CartesianMultipole(i)
!!$       end do
!!$       write(*,*)
!!$       stop
!!$    end if
!!$    !***
    !
    call Multipole%XlmSymSet%GetCartesianExpan( &
         Multipole%CartesianSet, &
         L, &
         VectorOfM, &
         CartExpansionMat )
    !
    !
!!$    !***
!!$    if ( L == 4 ) then
!!$       call Multipole%CartesianSet%FetchMonomialExponents( MonExponents )
!!$       do j = 1, size(CartExpansionMat,2)
!!$          do i = 1, size(VectorOfM)
!!$             write(*,*) i, j
!!$             write(*,*) MonExponents(1,j), MonExponents(2,j), MonExponents(3,j), CartExpansionMat(i,j)
!!$          end do
!!$       end do
!!$       stop
!!$    end if
!!$    !***
    !
    allocate( Multipole%XlmMultipole(size(VectorOfM)) )
    Multipole%XlmMultipole = cmplx(0.d0,0.d0)
    !
    !
    do j = 1, size(VectorOfM)
       !
       Sum = cmplx(0.d0,0.d0)
       !
       do i = 1, Multipole%Nmultipoles
          Sum = Sum + CartExpansionMat(j,i)*Multipole%CartesianMultipole(i)
       end do
       !
       Multipole%XlmMultipole(j) = Sum
       !
    end do
    !
    !
    open(newunit = uid, &
         file    = FileName, &
         form    ="formatted", &
         status  ="unknown",&
         action  ="write",&
         iostat  = iostat,&
         iomsg   = iomsg )
    if(iostat/=0)then
       call Assert(iomsg)
    endif
    !
    write(uid,*) "#", "  m", "  Multipole"
    do i = 1, size(VectorOfM)
!!$       RealPart = 0.5d0 * ( Multipole%XlmMultipole(i) + conjg(Multipole%XlmMultipole(i)) )
!!$       ImagPart = 1.d0!aimag(Multipole%XlmMultipole(i))
       write(uid,*) VectorOfM(i), Multipole%XlmMultipole(i)
    end do
    !
    close(uid)
    !
    deallocate(Multipole%XlmMultipole)
    !
  contains
    !
    subroutine SetFileName()
      integer :: iChar
      character(len=1024) :: tmpDir
      tmpDir=adjustl(Multipole%Dir)
      call FormatDirectoryName( tmpDir )
      allocate( FileName, source = trim(tmpDir)//Multipole%File )
    end subroutine SetFileName
    !
  end subroutine ClassMultipoleComputeAndSaveXlmMultipoles



  

  !> Returns the number of non-vanishing symmetric multipoles
  function ClassMultipoleGetN( Multipole ) result( N )
    class(ClassMultipole), intent(in) :: Multipole
    integer :: N
    N = Multipole%NMultipoles
  end function ClassMultipoleGetN


  !> Returns the number of non-vanishing symmetric multipoles
  subroutine ClassMultipoleSetCartesianValue( Multipole, ix, iy, iz, dValue ) 
    class(ClassMultipole), intent(inout) :: Multipole
    integer              , intent(in)    :: ix, iy, iz
    real(kind(1d0))      , intent(in)    :: dValue
    integer :: iMult
    iMult = Multipole%CartesianSet%GetIndex(ix,iy,iz)
    if(iMult==0)call ErrorMessage("unmatching triplet")
    Multipole%CartesianMultipole(iMult)=dValue
  end subroutine ClassMultipoleSetCartesianValue


  !> Returns the number of non-vanishing symmetric multipoles
  subroutine ClassMultipoleShow( Multipole, unit ) 
    class(ClassMultipole), intent(in) :: Multipole
    integer, optional    , intent(in) :: unit
    integer :: outunit, iMult
    outunit=OUTPUT_UNIT
    if(present(unit))outunit=unit
    write(outunit,"(a,i2)") "  Lmax =",Multipole%LMax
    write(outunit,"(a,i3)") "  NMult=",Multipole%NMultipoles
    if(Multipole%NMultipoles<=0)return
    write(outunit,"(a)")    "  File ="//Multipole%File
    write(outunit,"(a)")    "  CartFile ="//Multipole%CartFile
    call Multipole%Irrep%Show(unit)
    call Multipole%CartesianSet%show(unit)
    call Multipole%XlmSymSet%Show()
    if(allocated(Multipole%CartesianMultipole))then
       do iMult=1,Multipole%NMultipoles
          write(outunit,"(a,i3,a,d14.6)")&
               " Multipole(",iMult,") =",Multipole%CartesianMultipole(iMult)
       enddo
    endif
  end subroutine ClassMultipoleShow


  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !         Procedures   ClassParentIonSet
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


  subroutine ClassParentIonSetGroup( ParentIonSet, Group )
    class(ClassParentIonSet), intent(inout) :: ParentIonSet
    type(ClassGroup), target, intent(in)    :: Group
    ParentIonSet%Group => Group
  end subroutine ClassParentIonSetGroup



  subroutine ClassParentIonSetParseFile( ParentIonSet, FileName )
    use moduleParameterList
    use moduleString
    class(ClassParentIonSet), intent(inout) :: ParentIonSet
    character(len=*)        , intent(in)    :: FileName

    type(ClassParameterList) :: List
    character(len=8)    :: GroupLabel
    character(len=1024) :: ParentIonDir
    integer             :: iCharge
    integer             :: LMaxMultipole
    character(len=2048) :: ParentIonsStrn

    call List%add( "Group"  , "D2h"       , "required" )
    call List%add( "DataDir", "ParentIons", "required" )
    call List%add( "Charge" , 1           , "required" )
    call List%add( "LMaxMultipole", 0     , "required" )
    call List%add( "ParentIons", "1Ag.1"  , "required" )

    call List%Parse( FileName )

    call List%Get( "Group"     , GroupLabel     )
    call List%Get( "DataDir"   , ParentIonDir   )
    call List%Get( "Charge"    , iCharge        )
    call List%Get( "LMaxMultipole", LMaxMultipole )
    call List%Get( "ParentIons", ParentIonsStrn )

    !.. Check if the group is compatible with the global group,
    !..
    if( .not. associated( ParentIonSet%Group ) ) call Assert(&
         "Trying to parse config file for ClassParentIonSet item"//&
         "without having previously wired the item to the global"//&
         "symmetry group. Call <YourParentIonSet>.SetGroup(<YourGroup>)"//&
         "before calling <YourParentIonSet>.parseFile(<YourParentIonSetConfigFile>")
    if( .not. ParentIonSet%Group%IsInitialized() )call Assert("Uninitialized group")
    if( .not. ( ParentIonSet%Group%GetName() .is. trim(GroupLabel) ) )then
       call ErrorMessage("Parent-ion group does not match the global symmetry group")
       stop
    endif
    
    call ParentIonSet%init(      &
         trim( ParentIonDir )  , &
         iCharge               , &
         LMaxMultipole         , &
         trim( ParentIonsStrn )   )
    
  end subroutine ClassParentIonSetParseFile
  

  subroutine ClassParentIonSetInit( &
       ParentIonSet , &
       Dir          , &
       iCharge      , &
       LMaxMultipole, &
       ParentIonsStrn )
    !
    class(ClassParentIonSet), intent(inout) :: ParentIonSet
    character(len=*)        , intent(in)    :: Dir
    integer                 , intent(in)    :: iCharge
    integer                 , intent(in)    :: LMaxMultipole
    character(len=*)        , intent(in)    :: ParentIonsStrn
    
    integer :: NPions, iPion, iBraPion, iKetPion
    character(len=8), allocatable :: vPions(:)
    type(ClassIrrep), pointer :: IrrepPtr
    character(len=1024) :: MFileName

    call ParseParentIonsString( NPions, vPions, ParentIonsStrn )
    if( NPions <= 0 )then
       call ErrorMessage("Warning: No parent ions found")
    endif

    !.. Initialize the individual parent ions
    ParentIonSet%NParentIons   = NPions
    allocate(ParentIonSet%ParentIonList(NPions))
    do iPion = 1, NPions
       call ParentIonSet%ParentIonList(iPion)%init(ParentIonSet%Group,vPions(iPion),iCharge)
    end do

    ParentIonSet%LMaxMultipole = LMaxMultipole
    allocate(ParentIonSet%Multipole(NPions,NPions))
    !
    do iBraPion = 1, NPions
       !
       do iKetPion = iBraPion, NPions

          if(  ParentIonSet%GetMultiplicity( iBraPion ) ==  &
               ParentIonSet%GetMultiplicity( iKetPion ) )then

             MFileName=MULTIPOLE_ROOT_NAME//&
                  ParentIonSet%GetLabel(iBraPion)//"_"//&
                  ParentIonSet%GetLabel(iKetPion)
             call ParentIonSet%Multipole(iBraPion,iKetPion)%SetFile(trim(MFileName))
             
             IrrepPtr => ParentIonSet%GetIrrep(iBraPion) * ParentIonSet%GetIrrep(iKetPion)
             call ParentIonSet%Multipole(iBraPion,iKetPion)%init(&
                  LMaxMultipole, IrrepPtr )
          else

             call ParentIonSet%Multipole(iBraPion,iKetPion)%SetEmpty()

          endif

       enddo
    enddo

    allocate(ParentIonSet%DataDir,source=trim(adjustl(Dir)))
          
  end subroutine ClassParentIonSetInit

  subroutine ClassParentIonSetFree( PionSet )
    class(ClassParentIonSet) :: PionSet
    PionSet%NParentIons = 0
    if(allocated(PionSet%ParentIonList)) deallocate(PionSet%ParentIonList)
    PionSet%LMaxMultipole = 0
    if(allocated(PionSet%Multipole)) deallocate(PionSet%Multipole)
    if(allocated(PionSet%DataDir)) deallocate(PionSet%DataDir)
    PionSet%Group => NULL()
  end subroutine ClassParentIonSetFree

  subroutine ClassParentIonSetFinal( PionSet )
    type(ClassParentIonSet) :: PionSet
    call PionSet%free()
  end subroutine ClassParentIonSetFinal

  subroutine ParseParentIonsString( NPions, vPions, PIonsStrn )
    integer                      , intent(out) :: NPions
    character(len=*), allocatable, intent(out) :: VPions(:)
    character(len=*)             , intent(in)  :: PionsStrn
    character(len=:), allocatable :: Strn
    character(len=8) :: PionLabel
    integer :: i

    allocate(strn,source=trim(adjustl(PionsStrn))//" ")

    !.. Determines the number of parent ions
    i=1
    NPions=0
    do
       if(len_trim(strn)<=0)exit
       read(strn,*) PionLabel
       NPions=NPions+1
       i=index(strn," ")
       strn=adjustl(strn(i:))
    enddo

    allocate(vPions(NPions))
    !.. Read the parent ions
    strn=trim(adjustl(PionsStrn))//" "
    i=1
    NPions=0
    do
       if(len_trim(strn)<=0)exit
       read(strn,*) PionLabel
       NPions=NPions+1
       VPions(NPions)=trim(adjustl(PionLabel))
       i=index(strn," ")
       strn=adjustl(strn(i:))
    enddo
    
  end subroutine ParseParentIonsString

  
  subroutine ClassParentIonSetShow( PISet, unit ) 
    class(ClassParentIonSet), intent(in) :: PISet
    integer, optional       , intent(in) :: unit
    integer :: outunit, iPion, iBraPion, iKetPion
    outunit = OUTPUT_UNIT
    if(present(unit))outunit = unit
    write(outunit,"(a)"   ) " === Parent Ion Set ==="
    write(outunit,"(a)"   ) " Data Directory     = "//trim(PISet%DataDir)
    write(outunit,"(a,i4)") " Number Parent Ions = ",PISet%NParentIons
    write(outunit,"(a,i4)") " Maximum Multipole L= ",PISet%LMaxMultipole
    !.. Show individual parent ions
    do iPion = 1, PISet%NParentIons
       call PISet%ParentIonList(iPion)%show( unit )
    enddo
    !.. Show multipoles between individual parent ions
    do iBraPion = 1, PISet%NParentIons
       do iKetPion = iBraPion, PISet%NParentIons
          call PISet%Multipole(iBraPion,IKetPion)%show(unit)
       enddo
    enddo
  end subroutine ClassParentIonSetShow

  function ClassParentIonSetGetLabelFromIndex( PISet, iPion ) result( Label )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                 , intent(in) :: iPion
    character(len=:), allocatable        :: Label
    character(len=16) :: strn
    if(iPion<0.or.iPion>PISet%GetNumberIons()) call Assert("Invalid parent-ion index")
    strn=PISet%ParentIonList(iPion)%GetLabel()
    strn=adjustl(strn)
    allocate(Label,source=trim(strn))
  end function ClassParentIonSetGetLabelFromIndex

  function ClassParentIonSetGetIndexFromLabel( PISet, Label ) result( iPion )
    class(ClassParentIonSet), intent(in) :: PISet
    character(len=*)        , intent(in) :: Label
    integer                              :: iPion
    iPion=PISet%GetNumberIons()
    do 
       if(iPion<=0)exit
       if( PISet%ParentIonList(iPion)%GetLabel() .is. Label )exit
       iPion = iPion - 1
    enddo
  end function ClassParentIonSetGetIndexFromLabel

  function ClassParentIonSetGetEnergyFromIndex( PISet, iPion ) result( Energy )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                 , intent(in) :: iPion
    real(kind(1d0))                      :: Energy
    if(iPion<0.or.iPion>PISet%GetNumberIons()) call Assert("Invalid parent-ion index")
    Energy = PISet%ParentIonList(iPion)%GetEnergy()
  end function ClassParentIonSetGetEnergyFromIndex

  function ClassParentIonSetGetEnergyFromLabel( PISet, Label ) result( Energy )
    class(ClassParentIonSet), intent(in) :: PISet
    character(len=*)        , intent(in) :: Label
    real(kind(1d0))                      :: Energy
    integer :: iPion
    iPion = PISet%GetIndex( Label )
    if(iPion<=0) call Assert("Invalid Label "//trim(Label))
    Energy = PISet%ParentIonList(iPion)%GetEnergy()
  end function ClassParentIonSetGetEnergyFromLabel

  function ClassParentIonSetGetChargeFromIndex( PISet, iPion ) result( Charge )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                 , intent(in) :: iPion
    integer                              :: Charge
    if(iPion<0.or.iPion>PISet%GetNumberIons()) call Assert("Invalid parent-ion index")
    Charge = PISet%ParentIonList(iPion)%GetCharge()
  end function ClassParentIonSetGetChargeFromIndex

  function ClassParentIonSetGetChargeFromLabel( PISet, Label ) result( Charge )
    class(ClassParentIonSet), intent(in) :: PISet
    character(len=*)        , intent(in) :: Label
    integer                              :: Charge
    integer :: iPion
    iPion = PISet%GetIndex( Label )
    if(iPion<0) call Assert("Invalid Label "//trim(Label))
    Charge = PISet%ParentIonList(iPion)%GetCharge()
  end function ClassParentIonSetGetChargeFromLabel

  function ClassParentIonSetGetmultfromIndex( PISet, iPion ) result( Multiplicity )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                 , intent(in) :: iPion
    integer                              :: Multiplicity
    if(iPion<0.or.iPion>PISet%GetNumberIons()) call Assert("Invalid parent-ion index")
    Multiplicity = PISet%ParentIonList(iPion)%GetMultiplicity()
  end function ClassParentIonSetGetmultfromIndex

  function ClassParentIonSetGetmultfromLabel( PISet, Label ) result( Multiplicity )
    class(ClassParentIonSet), intent(in) :: PISet
    character(len=*)        , intent(in) :: Label
    integer                              :: Multiplicity
    integer :: iPion
    iPion = PISet%GetIndex( Label )
    if(iPion<0) call Assert("Invalid Label "//trim(Label))
    Multiplicity = PISet%ParentIonList(iPion)%GetMultiplicity()
  end function ClassParentIonSetGetmultfromLabel

  function ClassParentIonSetGetIrrepFromIndex( PISet, iPion ) result( Irrep )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                 , intent(in) :: iPion
    type(ClassIrrep)        , pointer    :: Irrep
    if(iPion<0.or.iPion>PISet%GetNumberIons()) call Assert("Invalid parent-ion index")
    Irrep => PISet%ParentIonList(iPion)%GetIrrep()
  end function ClassParentIonSetGetIrrepFromIndex
  
  function ClassParentIonSetGetIrrepFromLabel( PISet, Label ) result( Irrep )
    class(ClassParentIonSet), intent(in) :: PISet
    character(len=*)        , intent(in) :: Label
    type(ClassIrrep)        , pointer    :: Irrep
    integer :: iPion
    iPion = PISet%GetIndex( Label )
    if(iPion<0) call Assert("Invalid Label "//trim(Label))
    Irrep => PISet%ParentIonList(iPion)%GetIrrep()
  end function ClassParentIonSetGetIrrepFromLabel

  function ClassParentIonSetGetNumberIons( PISet ) result( Number )
    class(ClassParentIonSet), intent(in) :: PISet
    integer                              :: Number
    Number = PISet%NParentIons
  end function ClassParentIonSetGetNumberIons

  subroutine ClassParentIonSetLoadData( PISet, StorageDir, EnergyId ) 
    class(ClassParentIonSet), intent(inout) :: PISet
    character(len=*)        , intent(in)    :: StorageDir
    character(len=*)        , intent(in)    :: EnergyId
    character(len=:), allocatable :: strnDir
    integer :: iPion, iBraPion, iKetPion
    
    call FormatDirectoryName()
    
    !.. Load the energies of the parent ions
    do iPion = 1, PISet%GetNumberIons()
       call PISet%ParentIonList(iPion)%LoadData( strnDir, EnergyId )
    enddo

    !.. Load the multipoles between parent ions
    do iBraPion = 1, PISet%GetNumberIons()
       do iKetPion = iBraPion, PISet%GetNumberIons()
          call PISet%Multipole( iBraPion, iKetPion )%LoadCartesianData( strnDir )
       enddo
    enddo
    
  contains
    
    subroutine FormatDirectoryName()
      integer :: iChar
      iChar=len_trim(StorageDir)
      if(iChar==0)then
         allocate(strnDir,source=trim(PISet%DataDir))
      else
         if(StorageDir(iChar:iChar)=="/")then
            allocate(strnDir,source=trim(StorageDir)//trim(PISet%DataDir))
         else
            allocate(strnDir,source=trim(StorageDir)//"/"//trim(PISet%DataDir))
         endif
      endif
    end subroutine FormatDirectoryName
    
  end subroutine ClassParentIonSetLoadData



  subroutine ClassParentIonSetNePI( PI, NePI )
    !
    class(ClassParentIon), intent(inout) :: PI
    integer,               intent(in)    :: NePI
    !
    PI%NePI = NePI
    !
  end subroutine ClassParentIonSetNePI



end module ModuleParentIons
