!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Parses the configuration file that contains the parameters to compute the "exact" states, both bound and continuum ([ScatteringStatesInputFile](@ref ScatteringStatesInputFile)).


module ModuleScatteringStatesInputFile
    

  use ModuleParameterList

  implicit none

  !> Minimum continuum energy.
  real(kind(1d0)) :: EminCont

  !> Maximum continuum energy.
  real(kind(1d0)) :: EmaxCont

  !> Number of points to scale the continuum states to the analytical asymptotic part.
  integer         :: NumPointsPLotCont

  !> Number of bound states to be computed, always starting from the first available.
  Integer         :: NumBound

  !> Number of continuum states to be computed.
  Integer         :: NumCont

  !> Method by which the system of linear equations will be solved:
  !!
  !! "II" for inverse iteration, 
  !!
  !! "LU" for direct homogeneous system of linear equations solving
  !!      making the last row of the squared matrix equal to zero.
  character(len=:), allocatable :: Method


contains


    !> Parses [ScatteringStatesInputFile](@ref ScatteringStatesInputFile). 
  subroutine ParseScatteringStatesInputFile( ConfigurationFile )
    !
    !> File to be parsed.
    Character(len=*), intent(in) :: ConfigurationFile
    !
    type(ClassParameterList) :: List
    character(len=10) :: MethodStrn
    !
    call List%Add( "EminCont"         , 1.d-4,"optional" )
    call List%Add( "EmaxCont"         , 5.d0, "required" )
    call List%Add( "NumPointsPLotCont", 100 , "required" )
    call List%Add( "NumBound"         , 10  , "required" )
    call List%Add( "NumCont"          , 50  , "required" )
    call List%Add( "Method"           , "LU", "required" )
    !
    call List%Parse( ConfigurationFile )
    !
    call List%Get( "EminCont"         , EminCont )
    call List%Get( "EmaxCont"         , EmaxCont )
    call List%Get( "NumPointsPLotCont", NumPointsPLotCont )
    call List%Get( "NumBound"         , NumBound )
    call List%Get( "NumCont"          , NumCont  )
    call List%Get( "Method"           , MethodStrn )
    !
    allocate( Method, source = trim( MethodStrn ) )
    !
  end subroutine ParseScatteringStatesInputFile



end module ModuleScatteringStatesInputFile
