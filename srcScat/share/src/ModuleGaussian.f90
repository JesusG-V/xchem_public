!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines the variables and methods for the Gaussian radial basis.


module ModuleGaussian
  
  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleParameterList
  use ModuleString
  use ModuleSimpleLapack
  
  implicit none
  
  private
  
  Real(Kind(1d0)), parameter :: PI = 4.d0 * atan( 1.d0 )
    

  !> The preconditioner is a matrix that expresses a reasonably linearly independent 
  !! set of radial functions which are regular for a given angular momentum in terms 
  !! of all the regular functions for that given angular momentum.
  !! 
  !!    N_ORTHOGONAL     =      N_PRECONDITIONED        <=     N_REGULAR        <=     N_FUNCTIONS
  !! {ORTHONORMAL SET}_L ~ {PRECONDITIONED FUNCTIONS}_L c {REGULAR FUNCTIONS}_L c { ALL RADIAL FUNCTIONS }
  !! 
  !! In the case of the Gaussian Preconditioner, the preconditioned functions are 
  !! also orthonormal, as a result of the preconditioning procedure. The regular 
  !! functions, in turn, are a subset of all the radial functions. In the case of 
  !! the Gaussian basis, the regular functions correspond to a contiguous subset of 
  !! the complete set of radial functions.
  !! All Gaussian functions are already continuous; no further restrictions
  !! are required to enforce such property.
  !!
  type, private :: ClassGaussianPreconditioner
     logical :: INITIALIZED = .FALSE.
     integer :: MinIndex = 0
     integer :: MaxIndex = 0
     integer :: NRegular
     integer :: NPrecon
     Real(Kind(1d0)) :: MinRCond = 0
     Real(Kind(1d0)), allocatable :: E(:)
     Real(Kind(1d0)), allocatable :: A(:,:)
   contains
     procedure :: Init => InitPreconditioner
     procedure :: write => WritePreconditionerOnUnit
     procedure :: read  => ReadPreconditionerOnUnit
     procedure :: free => FreePreconditioner
     final :: FinalizePreconditioner
  end type ClassGaussianPreconditioner



  !> The gaussian spatial basis functions with angular momentum \f$L\f$ and projection \f$M\f$
  !! are
  !! \f[
  !!     \psi_{L\alpha\ell}(\vec{r}) = N_{\alpha\ell} \frac{g_{\alpha,\ell}(r)}{r} Y_{LM}(\hat{r}),
  !! \f]
  !! where
  !! \f[ 
  !!     g_{\alpha,\ell}(r)= r^{\ell+1} e^{-\alpha r^2},
  !! \f]
  !! \f[
  !!       \ell = L, L+2, L+4, \ldots, L_{max} - \mod(L_{max}+L,2)
  !! \f]
  !! The latter constraint on $\ell$
  !! - ensures the regularity condition $g_{\alpha\ell}(r)\sim r^{L+1}$;
  !! - takes into account the fact that the basis with even and odd polynomial 
  !!   exponents are separately complete;
  !! - limits the maximum cartesian exponents of the functions.
  !!
  !! Since some normalization factors are very different from 1, better numerical
  !! stability is achieved by using as a reduced radial basis the normalized set
  !! \f[
  !!      u_{\alpha\ell}(r) = N_{\alpha\ell} g_{\alpha\ell}
  !! \f]
  !!
  !! The number of radial functions, therefore, depends on the angular momentum
  !! and on the maximum value of \f$L_{max}\f$. In particular, it is not possible 
  !! to represent valid angular momenta larger than \f$L_{max}\f$.
  !!
  type, public :: ClassGaussian
     !
     private
     logical                      :: Initialized = .FALSE.

     !> Maximum value of the "angular momentum" in the Gaussian expansion, 
     !! i.e., \ell = 0, 1, ..., LMax
     integer                      :: LMax   = 0

     !> Number of Exponents in the Gaussian expansion.
     integer                      :: NAlpha = 0

     !> List of exponents.
     real(kind(1d0)), allocatable :: Alpha(:)

     !> NFun: Total number of functions,
     !! NFun = ( LMax + 1 ) * NAlpha
     integer                      :: NFun 

     !> \f$f(i_{\alpha},i_{ell}) \,\,\, i_{\alpha}=1,2,...,N_{\alpha} \,\,\, i_{ell}=0,1,...,L_{Max}\f$.
     !! Normalization factors
     !! \f[
     !!     f = \|g_{\alpha,\ell}\|_{L^2}^{-1}.
     !! \f]
     real(kind(1d0)), allocatable :: F(:,:)

     !> Minimum value of the reciprocal condition number required
     !! for the Preconditioned basis for any orbital angular momentum
     Real(Kind(1d0)) :: MinRCond
     !
     !> Vectorial storage of the basis parameters. To ensure that
     !! the set of functions which are valid for any given angular momentum
     !! corresponds to a contiguous set of indexes, the functions which are
     !! appropriate for even angular momenta (l=0,2,4,...) are located first
     !! and those appropriate for odd angular momenta are located second.
     !! 
     !> ltab(1:NFun): vector of monomial exponents \f$\ell\f$
     integer        , allocatable :: ltab(:)
     !> atab(1:NFun): vector of gauss exponents
     Real(Kind(1d0)), allocatable :: atab(:)
     !> ftab(1:NFun): vector of normalization factors
     Real(Kind(1d0)), allocatable :: ftab(:)
     !
     !> Vector with the minimum valid index for a given angular momentum
     integer        , allocatable :: MinIndex(:)
     !> Vector with the maximum valid index for a given angular momentum
     integer        , allocatable :: MaxIndex(:)
     !
     !> Precondition factors that cast the matrix representation of
     !! an operator on a Gaussian basis to the conditioned regular 
     !! basis for any given orbital angular momentum.
     type(ClassGaussianPreconditioner), allocatable :: Preconditioner(:)
     !
   contains
     !
     !.. Basics
     !> Initializes ClassGaussian.
     generic   :: Init =>  &
          GaussianInit,  &
          GaussInitReadExponentFile, &
          GaussInitReadConfigurationFile
     !> Allocates the atributes of ClassGaussian.
     procedure :: Allocate => ClassGaussianAllocate
     !> Deallocates the atributes of ClassGaussian.
     procedure :: Free =>  GaussianFree
     !
     !.. Accessors
     !> Gets the number of Gaussian functions.
     procedure :: GetNFun        =>  GaussianGetNFun
     !> Gets the maximum angular momentum in the Gaussian expansion.
     procedure :: GetLMax        =>  GaussianGetLMax
     !> Gets the number of exponents in the Gaussian expansion.
     procedure :: GetNAlpha      =>  GaussianGetNAlpha
     !> Gets the normalization factor of a selected Gaussian function.
     procedure :: GetNormFactor  =>  GaussianNormalizationFactor
     !> Gets a vector with all the monomials exponents.
     procedure, public :: GetAllMonomials
     !> Gets a vector with all the exponents in the exponential.
     procedure, public :: GetAllExponents
     !> Gets the maximum distance at which the Gaussian functions could be plotted.
     procedure :: GetMaxPlotRadius  =>  GaussianMaxPlotRadius
     !> Gets the minimum regular Gaussian functions index.
     procedure :: GetMinimumRegularIndex => GaussianGetMinimumRegularIndex
     !> Gets the maximum regular Gaussian functions index.
     procedure :: GetMaximumRegularIndex => GaussianGetMaximumRegularIndex
     !> Gets the minimum value of the reciprocal condition number.
     procedure :: GetMinRCond
     !> Gets the information of the linear independent gaussian function (preconditioned): monomial exponent, gaussian exponent and normalization factor.
     procedure :: GetLIGaussian
     !
     !> Evaluates either a selected normalized Gaussian function or a linear combination of Gaussian radial basis functions.
     generic   :: Eval           =>  GaussianEval, GaussianFunctionEval
     !> Tabulates in a specified domain either a selected normalized Gaussian function or a linear combination of Gaussian radial basis functions.
     generic   :: Tabulate       =>  GaussianTabulate, GaussianFunctionTabulate
     !
     !> Computes the either the integral between reduced Gaussian radial functions,
     !! \f[
     !! \int_0^\infty dr g_{\alpha_1,n_1}(r) r^{n} \frac{d^m}{dr^m}g_{\alpha_2,n_2}(r),
     !! \f]
     !!
     !! or the general integral:
     !! \f[
     !!    \int_{a}^{BP} r^{2}dr
     !!       \frac{d^{n_{1}}g_{i}(r)}{dr^{n_{1}}} f(r)
     !!       \frac{d^{n_{2}}g_{j}(r)}{dr^{n_{2}}} + 
     !!    \int_{BP}^{b} r^{2}dr
     !!       \frac{d^{n_{1}}g_{i}(r)}{dr^{n_{1}}} f(r)
     !!       \frac{d^{n_{2}}g_{j}(r)}{dr^{n_{2}}}
     !! \f]
     !! Where \f$f(r)\f$ is a local operator and \f$BP\f$ an optional break point. 
     generic   :: Integral       =>  GaussianMonomialIntegral, GaussianGeneralIntegral
     !> Computes the monoelectronic multipole
     !! \f[
     !!    \int_0^\infty dr G_1(R)\frac{r_<^\ell}{r_>^{\ell+1}}G_2(r)
     !! \f]
     !! where \f$r_<=\min(r,R)\f$ and \f$r_>=\max(r,R)\f$. 
     generic   :: Multipole      =>  MonoelectronicMultipole
     !
     !> Checks whether the Gaussian characteristics (fingerprint) saved 
     !! in form of a class, coincides or not with the current Gaussian in used.
     procedure :: MatchFingerPrint       =>  GaussianMatchFingerPrint
     !> Checks whether the Gaussian characteristics (fingerprint) saved 
     !! in a unit, coincides or not with the current Gaussian in used.
     procedure :: MatchFingerPrintOnUnit =>  GaussianMatchFingerPrintOnUnit
     !> Checks whether the Gaussian characteristics (fingerprint) saved 
     !! in a file, coincides or not with the current Gaussian in used.
     procedure :: MatchFingerPrintOnFile =>  GaussianMatchFingerPrintOnFile
     !> Writes the Gaussian fingerprint to a file.
     procedure :: WriteFingerPrint       =>  GaussianWriteFingerPrint
     !
     !> Makes a copy of the ClassGaussian.
     generic, public :: ASSIGNMENT(=) => CopyGaussSet
     !
     !> Gets the number of Gaussian regular functions.
     procedure :: NRegular        => ClassGaussianNRegular
     !> Gets the number of Gaussian preconditioned functions.
     procedure :: NPreconditioned => ClassGaussianNPreconditioned
     !> Computes the preconditioner.
     procedure :: ComputePreconditioner
     !> Saves the preconditioner.
     procedure :: SavePreconditioner
     !> Logical function that retrieves whether the preconditoner is available or not.
     procedure :: PreconditionerIsAvailable
     !> Loads the preconditioner.
     procedure :: LoadPreconditioner
     !> Fetches the preconditioner.
     procedure :: FetchPreconditioner
     !> Creates in a directory all the configuration
     !!    files that define the current basis.
     procedure :: SaveConfig
     
     !
     !.. Private Interface
     procedure, private :: GaussianInit
     procedure, private :: GaussInitReadExponentFile
     procedure, private :: GaussInitReadConfigurationFile
     procedure, private :: GaussianMonomialIntegral
     procedure, private :: GaussianGeneralIntegral
     procedure, private :: GaussianFunctionEval
     procedure, private :: GaussianTabulate
     procedure, private :: GaussianFunctionTabulate
     procedure, private :: MonoElectronicMultipole
     procedure, private :: GaussianEval
     procedure, private :: CopyGaussSet
     !
  end type ClassGaussian
  
  
  !> Contains the variables and procedures to manage the Gaussian radial basis relevant information with the aim to distinguish between the used parameterization and others (fingerprint).
  type, public :: ClassGaussianFingerPrint
     private
     !> LMax: Maximum value of the "angular momentum" in the Gaussian expansion. 
     integer                      :: LMax   = 0
     !> Number of Exponents in the Gaussian expansion.
     integer                      :: NAlpha = 0
     !> List of exponents.
     Real(Kind(1d0)), allocatable :: Alpha(:)
   contains
     !> Deallocates the atributes of the Gaussian basis fingerprint class.
     procedure :: Free   =>  FingerPrintFree
     !> Writes the fingerprint to a unit.
     procedure :: Write  =>  FingerPrintWrite
     !> Reads the fingerprint from a unit.
     procedure :: Read   =>  FingerPrintRead
     procedure :: Init   =>  FingerPrintInit
     !> Makes a copy of the fingerprint.
     generic   :: assignment(=) => GaussianExtractFingerPrint
     procedure, private :: GaussianExtractFingerprint
  end type ClassGaussianFingerPrint


  interface myexp
     module procedure myexp_DD, myexp_DI
  end interface myexp
  
contains
  
  integer function GaussianGetNFun(GaussSet) result(NFun)
    class(ClassGaussian), intent(in) :: GaussSet
    if(.not.GaussSet%Initialized)call Assert("ClassGaussian::GetNFun: Gaussian set not initialized")
    NFun=GaussSet%NFun
  end function GaussianGetNFun
  
  integer function GaussianGetLMax(GaussSet) result(LMax)
    class(ClassGaussian), intent(in) :: GaussSet
    if(.not.GaussSet%Initialized)call Assert("ClassGaussian::GetLMax: Gaussian set not initialized")
    LMax=GaussSet%LMax
  end function GaussianGetLMax

  integer function GaussianGetNAlpha(GaussSet) result(NAlpha)
    class(ClassGaussian), intent(in) :: GaussSet
    if(.not.GaussSet%Initialized)call Assert("ClassGaussian::GetNAlpha: Gaussian set not initialized")
    NAlpha=GaussSet%NAlpha
  end function GaussianGetNAlpha

  
  subroutine GaussianFree(GaussSet)
    !
    class(ClassGaussian), intent(inout) :: GaussSet
    !
    GaussSet%LMax   = 0
    GaussSet%NAlpha = 0
    !
    if( allocated( GaussSet%Alpha    ) ) deallocate( GaussSet%Alpha    )
    if( allocated( GaussSet%F        ) ) deallocate( GaussSet%F        )
    if( allocated( GaussSet%ltab     ) ) deallocate( GaussSet%ltab     )
    if( allocated( GaussSet%atab     ) ) deallocate( GaussSet%atab     )
    if( allocated( GaussSet%ftab     ) ) deallocate( GaussSet%ftab     )
    if( allocated( GaussSet%MinIndex ) ) deallocate( GaussSet%MinIndex )
    if( allocated( GaussSet%MaxIndex ) ) deallocate( GaussSet%MaxIndex )
    if( allocated( GaussSet%Preconditioner ) )deallocate( GaussSet%Preconditioner )
    !
    GaussSet%Initialized = .FALSE.
    !
  end subroutine GaussianFree


  subroutine GaussianInit(&
       GaussSet , &
       LMax     , &
       NAlpha   , &
       Alpha    , &
       MaxConThr, &
       Status   )
    !
    class(ClassGaussian), intent(out) :: GaussSet
    integer             , intent(in)  :: LMax
    integer             , intent(in)  :: NAlpha
    Real(Kind(1d0))     , intent(in)  :: Alpha(:)
    Real(Kind(1d0))     , intent(in)  :: MaxConThr
    integer, optional   , intent(out) :: Status

    logical, parameter :: VERBOUS = .FALSE.

    integer :: iAlpha, iFun, iAng, AngularParity
    integer :: MaxEvenL, MaxOddL

    call CheckInputParameters(LMax,NAlpha,Alpha,Status)
    if(present(Status).and.Status/=0)return

    call GaussSet%Allocate( LMax, NAlpha )

    GaussSet%LMax  = LMax
    GaussSet%Alpha = Alpha(1:NAlpha)
    GaussSet%MinRCond = MaxConThr

    MaxEvenL = LMax - mod( LMax  , 2 )
    GaussSet%MinIndex(0:MaxEvenL:2)=[(1+iAng*NAlpha,iAng=0,MaxEvenL/2)]
    GaussSet%MaxIndex(0:MaxEvenL:2)=(MaxEvenL/2+1)*NAlpha
    if(VERBOUS)then
       write(*,*)
       write(*,"(*(x,i4))") Lmax, MaxEvenL
       write(*,"(*(x,i4))") GaussSet%MinIndex(0:MaxEvenL:2)
       write(*,"(*(x,i4))") GaussSet%MaxIndex(0:MaxEvenL:2)
    endif

    MaxOddL  = LMax - mod( LMax+1, 2 )
    GaussSet%MinIndex(1:MaxOddL:2)=GaussSet%MaxIndex(0)+[(1+iAng*NAlpha,iAng=0,(MaxOddL-1)/2 )]
    GaussSet%MaxIndex(1:MaxOddL:2)=GaussSet%MaxIndex(0)+(MaxOddL+1)/2 *NAlpha
    if(VERBOUS)then
       write(*,*) 
       write(*,"(*(x,i4))") Lmax, MaxOddL
       write(*,"(*(x,i4))") GaussSet%MinIndex(1:MaxOddL:2)
       write(*,"(*(x,i4))") GaussSet%MaxIndex(1:MaxOddL:2)
    endif

    iFun=0
    do AngularParity = 0, 1
       do iAng = AngularParity, LMax, 2
          do iAlpha = 1, NAlpha
             iFun = iFun + 1
             GaussSet%ltab( iFun ) = iAng
             GaussSet%atab( iFun ) = Alpha( iAlpha )
             GaussSet%ftab( iFun ) = 1.d0 / MonomialGaussianNorm( Alpha( iAlpha ), iAng )
             GaussSet%F( iAlpha, iAng ) = GaussSet%ftab( iFun ) 
          enddo
       enddo
    enddo

    allocate(GaussSet%Preconditioner(0:GaussSet%LMax))

    GaussSet%Initialized = .TRUE.

  contains

    subroutine CheckInputParameters(&
         LMax     , &
         NAlpha   , &
         Alpha    , &
         Status   )
      integer             , intent(in)  :: LMax, NAlpha
      Real(Kind(1d0))     , intent(in)  :: Alpha(:)
      integer, optional   , intent(out) :: Status
      !
      integer :: iAlpha
      !
      if(present(Status))Status=0
      if( LMax   <  0 )then
         if(present(Status))then
            Status = -1
            return
         else
            call Assert("ClassGaussian::Invalid maximum monomial exponent")
         endif
      endif
      if( NAlpha <= 0 )then
         if(present(Status))then
            Status = -2
            return
         else
            call Assert("ClassGaussian::Invalid Number of exponents")
         endif
      endif
      do iAlpha = 1, NAlpha
         if(Alpha(iAlpha)<=0.d0)then
            if(present(Status))then
               Status=-3
               return
            else
               call Assert("ClassGaussian::Invalid Exponent")
            endif
         endif
      end do
      !
    end subroutine CheckInputParameters
    !
  end subroutine GaussianInit

  integer function GaussianGetMinimumRegularIndex(GaussSet,AngularMomentum) &
       result( MinimumRegularIndex )
    class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: AngularMomentum
    if(  AngularMomentum < 0 .or. &
         AngularMomentum > GaussSet%GetLmax() ) &
         call Assert("ClassGaussian::GetMinimumRegularIndex : Invalid Angular Momentum")
    MinimumRegularIndex = GaussSet%MinIndex( AngularMomentum )
  end function GaussianGetMinimumRegularIndex

  integer function GaussianGetMaximumRegularIndex(GaussSet,AngularMomentum) &
       result( MaximumRegularIndex )
    class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: AngularMomentum
    if(  AngularMomentum < 0 .or. &
         AngularMomentum > GaussSet%GetLmax() ) &
         call Assert("ClassGaussian::GetMaximumRegularIndex : Invalid Angular Momentum")
    MaximumRegularIndex = GaussSet%MaxIndex( AngularMomentum )
  end function GaussianGetMaximumRegularIndex

  real(kind(1d0)) function GetMinRCond(GaussSet) &
       result( MinRCond )
    class(ClassGaussian), intent(in) :: GaussSet
    MinRCond = GaussSet%MinRCond
  end function GetMinRCond

  Real(Kind(1d0)) function GaussianNormalizationFactor(GaussSet,iFun) &
       result(NormalizationFactor)
    class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: iFun
    NormalizationFactor = 0.d0
    if(iFun<1.or.iFun>GaussSet%GetNFun())return
    NormalizationFactor = GaussSet%ftab( iFun )
  end function GaussianNormalizationFactor
  

  Real(Kind(1d0)) function GaussianMaxPlotRadius(GaussSet) &
       result(MaxPlotRadius)
    class(ClassGaussian), intent(in) :: GaussSet
    Real(Kind(1d0)), parameter :: MIN_PLOT_THRESHOLD = 1.d-16
    Real(Kind(1d0)) :: MinAlpha
    MinAlpha = minval(GaussSet%atab)
    MaxPlotRadius = sqrt(-log(MIN_PLOT_THRESHOLD)/MinAlpha)
  end function GaussianMaxPlotRadius

  
  subroutine FingerPrintWrite(FingerPrint,OutputUnit)
    Class(ClassGaussianFingerPrint), intent(in) :: FingerPrint
    integer                        , intent(in) :: OutputUnit
    integer :: i
    character(len=10)  :: IsUnitFormatted, UnitIsWritable
    logical :: UnitIsOpen
    !.. Check if unit is open for writing and in which format
    Inquire(&
         Unit      = OutputUnit      , &
         FORMATTED = IsUnitFormatted , &
         OPENED    = UnitIsOpen      , &
         WRITE     = UnitIsWritable  )
    if( UnitIsWritable == "NO" .or. .not. UnitIsOpen ) &
         call Assert("ClassGaussian::FingerPrintWrite : Invalid unit")

    if( IsUnitFormatted == "YES" )then
       !
       write(OutputUnit,"(2i23)") FingerPrint%LMax,FingerPrint%NAlpha
       write(OutputUnit,"(*(x,d24.16))") (FingerPrint%Alpha(i),i=1,FingerPrint%NAlpha)
       !
    elseif( IsUnitFormatted == "NO" )then
       !
       write(OutputUnit) FingerPrint%LMax,FingerPrint%NAlpha
       write(OutputUnit)(FingerPrint%Alpha(i),i=1,FingerPrint%NAlpha)
       !
    else
       call Assert("ClassGaussian::FingerPrintWrite : Invalid format")
    endif
    return
  end subroutine FingerPrintWrite


  subroutine FingerPrintRead(FingerPrint,InputUnit,iostat)
    Class(ClassGaussianFingerPrint), intent(inout) :: FingerPrint
    integer                        , intent(in)    :: InputUnit
    integer, optional              , intent(inout) :: iostat
    integer :: i
    character(len=10) :: IsUnitFormatted, UnitIsReadable
    logical :: UnitIsOpen
    integer :: iostat_, LMax, NAlpha
    character(len=IOMSG_LENGTH)::iomsg
    !.. Check if unit is open for reading and in which format
    if(present(iostat))iostat=0
    Inquire(&
         Unit      = InputUnit       , &
         FORMATTED = IsUnitFormatted , &
         OPENED    = UnitIsOpen      , &
         READ      = UnitIsReadable  )
    if( .not.UnitIsOpen )then
       if(present(iostat))then
          IOSTAT=1
          return
       end if
       call Assert("ClassGaussian::FingerPrintRead : Unit is not open")
    endif
    if( UnitIsReadable=="NO" )then
       if(present(iostat))then
          IOStat=1
          return
       end if
       call Assert("ClassGaussian::FingerPrintRead : Unreadable Unit")
    endif

    if( IsUnitFormatted == "YES" )then
       !
       read(InputUnit,*,iostat=iostat_,iomsg=iomsg) LMax, NAlpha
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       call FingerPrint%Init( LMax, NAlpha )
       read(InputUnit,"(*(d24.16))",iostat=iostat_,iomsg=iomsg) (FingerPrint%Alpha(i),i=1,FingerPrint%NAlpha)
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
    elseif( IsUnitFormatted == "NO" )then
       !
       read(InputUnit,iostat=iostat_,iomsg=iomsg) LMax,NAlpha
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       call FingerPrint%Init( LMax, NAlpha )
       read(InputUnit,iostat=iostat_,iomsg=iomsg)(FingerPrint%Alpha(i),i=1,FingerPrint%NAlpha)
       if(iostat_/=0)then
          if(present(iostat))then
             iostat=iostat_
             return
          end if
          call Assert(iomsg)
       endif
       !
    else
       if(present(iostat))then
          iostat=2
          return
       end if
       call Assert("ClassGaussian::FingerPrintRead : Invalid format")
    endif

    return
  end subroutine FingerPrintRead

  subroutine FingerPrintInit( FingerPrint, LMax, Nalpha )
    Class(ClassGaussianFingerPrint), intent(inout) :: FingerPrint
    integer                        , intent(in)    :: LMax
    integer                        , intent(in)    :: NAlpha
    FingerPrint%LMax   = LMax
    FingerPrint%NAlpha = NAlpha
    if(allocated(Fingerprint%Alpha))deallocate(FingerPrint%Alpha)
    allocate(FingerPrint%Alpha(NAlpha))
    FingerPrint%Alpha=0.d0
  end subroutine FingerPrintInit


  subroutine FingerPrintFree( FingerPrint )
    Class(ClassGaussianFingerPrint), intent(inout) :: FingerPrint
    FingerPrint%LMax   = 0
    FingerPrint%NAlpha = 0
    if(allocated(Fingerprint%Alpha))deallocate(FingerPrint%Alpha)
  end subroutine FingerPrintFree
  

  logical function GaussianMatchFingerPrint(Set,FingerPrint) result(Match)
    Class( ClassGaussian )          , intent(in) :: Set
    type( ClassGaussianFingerPrint ), intent(in) :: FingerPrint
    integer :: iAlpha
    Real(Kind(1d0)), parameter :: TOLERANCE=1.d-12
    Match=.false.
    if( FingerPrint%LMax   /= Set%LMax   ) return
    if( FingerPrint%NAlpha /= Set%NAlpha ) return
    do iAlpha=1,Set%NAlpha
       if(abs(FingerPrint%Alpha(iAlpha)/Set%Alpha(iAlpha)-1.d0) > TOLERANCE )return
    enddo
    Match=.TRUE.
  end function GaussianMatchFingerPrint


  logical function GaussianMatchFingerPrintOnUnit(Set,uid) result(Match)
    Class(ClassGaussian), intent(in) :: Set
    integer             , intent(in) :: uid
    type(ClassGaussianFingerPrint)   :: FingerPrint
    call FingerPrint%Read(uid)
    Match=Set%MatchFingerprint(FingerPrint)
    call FingerPrint%Free()
  end function GaussianMatchFingerPrintOnUnit
  

  logical function GaussianMatchFingerPrintOnFile( self, fileName ) result(Match)
    class(ClassGaussian), intent(in) :: self
    character(len=*)    , intent(in) :: fileName
    !
    character(len=*), parameter    :: HERE = "ClassGaussian::MatchFingerprintOnFile : "
    integer                        :: uid
    integer                        :: iostat
    character(len=IOMSG_LENGTH)    :: iomsg
    !
    open(NewUnit =  uid         , &
         File    =  fileName    , &
         Form    = "unformatted", &
         Status  = "old"        , &
         Action  = "read"       , &
         iostat  =  iostat      , &
         iomsg   =  iomsg       )
    if(iostat/=0)then
       Match=.FALSE.
       return
    endif
    Match=self%MatchFingerPrintOnUnit( uid )
    close(uid)
  end function GaussianMatchFingerPrintOnFile


  subroutine GaussianExtractFingerPrint( FingerPrint, GaussSet )
    Class( ClassGaussianFingerPrint ), intent(inout) :: FingerPrint
    Class( ClassGaussian )           , intent(in)    :: GaussSet
    call FingerPrint%Init( GaussSet%LMax, GaussSet%NAlpha )
    FingerPrint%Alpha = GaussSet%Alpha
  end subroutine GaussianExtractFingerPrint


  subroutine GaussianWriteFingerprint( GaussSet, uid )
    Class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: uid
    type(ClassGaussianFingerPrint)   :: fingerprint
    fingerprint = GaussSet
    call fingerprint%write(uid)
    call fingerprint%free
  end subroutine GaussianWriteFingerprint


  !> Copies the Gaussian set sOrigin to sDestination 
  subroutine CopyGaussSet(sDestination,sOrigin)
    Class(ClassGaussian), intent(inout):: sDestination
    Class(ClassGaussian), intent(in)   :: sOrigin
    !
    call sDestination%Free()
    if(.not.sOrigin%Initialized)call Assert("ClassGaussian::Invalid GaussSet Origin")
    !
    call sDestination%Allocate( &
         sDestination%LMax    , &
         sDestination%NAlpha  ) 
    !
    sDestination%Alpha = sOrigin%Alpha
    sDestination%F     = sOrigin%F
    sDestination%ltab  = sOrigin%ltab
    sDestination%atab  = sOrigin%atab
    sDestination%ftab  = sOrigin%ftab
    ! *** 
    ! *** Copy of the preconditioner is missing
    ! *** 
    sDestination%INITIALIZED=.TRUE.
    !
  end subroutine CopyGaussSet

  
  subroutine ClassGaussianAllocate( GaussSet, LMax, NAlpha )
    Class(ClassGaussian), intent(inout) :: GaussSet
    integer             , intent(in)    :: LMax
    integer             , intent(in)    :: NAlpha
    !
    integer :: NFun
    !
    if(LMax   < 0) call Assert("ClassGaussian::Allocate : Invalid LMax  ")
    if(NAlpha < 1) call Assert("ClassGaussian::Allocate : Invalid NAlpha")
    !
    call GaussSet%Free()
    !
    GaussSet%LMax   = LMax
    GaussSet%NAlpha = NAlpha
    NFun = (LMax+1)*NAlpha
    GaussSet%NFun   = NFun
    !
    allocate(GaussSet%F(NAlpha,0:LMax))
    allocate(GaussSet%Alpha(NAlpha))
    allocate(GaussSet%ltab(NFun))
    allocate(GaussSet%atab(NFun))
    allocate(GaussSet%ftab(NFun))
    allocate(GaussSet%MinIndex( 0:LMax ))
    allocate(GaussSet%MaxIndex( 0:LMax ))
    !
    GaussSet%F    = 0.d0
    GaussSet%Alpha= 0.d0
    GaussSet%ltab = 0
    GaussSet%ftab = 0.d0
    GaussSet%atab = 0.d0
    GaussSet%MinIndex = 0
    GaussSet%MaxIndex = 0
    !
  end subroutine ClassGaussianAllocate
  
  
  !> Initialize the set of Gaussian after reading the parameteres from a file
  subroutine GaussInitReadConfigurationFile( GaussSet, ConfigurationFile, IOSTAT )
    !
    Class(ClassGaussian), intent(inout)         :: GaussSet
    character(len=*)    , intent(in)            :: ConfigurationFile
    integer             , intent(out), optional :: IOSTAT
    !
    type(ClassParameterList) :: List
    integer :: LMax, NAlpha, status
    character(len=500) :: ExpFile, Generation
    real(kind(1d0)) :: MinRCond
    real(kind(1d0)) :: Alpha0, Beta
    real(kind(1d0)), allocatable :: ExpVec(:)
    !
    character(len=*), parameter :: READ_FROM_FILE= "File"
    character(len=*), parameter :: EVEN_TEMPERED = "EvenTempered"
    character(len=*), parameter :: DEFAULT_GENERATION = EVEN_TEMPERED
    !
    !.. Define parameters and parse configuration file
    call List%Add( "NumberExponents"   ,      30         , "required" )
    call List%Add( "MaxAngularMomentum",      15         , "required" )
    call List%Add( "Generation"        , DEFAULT_GENERATION, "required" )
    call List%Add( "Alpha0"            ,     1.d-3       , "optional" )
    call List%Add( "Beta"              ,     2.d0        , "optional" )
    call List%Add( "ExponentsFile"     , "GaussExponents", "optional" )
    call List%Add( "MinReciprocalConditionNumber", 1.d-8 , "required" )
    call List%Parse( ConfigurationFile )
    call List%Get( "NumberExponents"   , NAlpha  )
    call List%Get( "MaxAngularMomentum", LMax    )
    call List%Get( "Generation"        , Generation )
    call List%Get( "Alpha0"            , Alpha0  )
    call List%Get( "Beta"              , Beta    )
    call List%Get( "ExponentsFile"     , ExpFile )
    call List%Get( "MinReciprocalConditionNumber", MinRCond )
    !
    !.. Initialize the Gaussian set
    if( Generation .is. READ_FROM_FILE )then
       if( .not. List%Present("ExponentsFile") )then
          call ErrorMessage("ExponentsFile parameter missing in "//trim(adjustl(ConfigurationFile)))
          STOP
       endif
       call GaussSet%Init( LMax, NAlpha, ExpFile, MinRCond, Status = status )
    elseif( Generation .is. EVEN_TEMPERED )then
       if( .not. List%Present("Alpha0") )then
          call ErrorMessage("Alpha0 parameter missing in "//trim(adjustl(ConfigurationFile)))
          STOP
       endif
       if( .not. List%Present("Beta") )then
          call ErrorMessage("Beta parameter missing in "//trim(adjustl(ConfigurationFile)))
          STOP
       endif
       allocate(ExpVec(NAlpha))
       call GenerateEvenTemperedSeries( NAlpha, Alpha0, Beta, ExpVec )
       call GaussSet%Init( LMax, NAlpha, ExpVec, MinRCond, Status = status )
    else
       call ErrorMessage("Unrecognized Generation method '"//&
            trim(Generation)//"' in "//trim(adjustl(ConfigurationFile)))
       call ErrorMessage(" Valid options are: ")
       call ErrorMessage(" - "//READ_FROM_FILE )
       call ErrorMessage(" - "//EVEN_TEMPERED  )
       STOP
    endif
    !
    if(present(IOSTAT))then
       IOSTAT=status
    elseif(status/=0)then
       call ErrorMessage("Gaussian Initialization failed")
    end if
    !
  end subroutine GaussInitReadConfigurationFile


  !> Generate a series of Even-tempered exponents
  subroutine GenerateEvenTemperedSeries( nAlpha, alpha0, beta, expVec )
    integer        , intent(in) :: nAlpha
    real(kind(1d0)), intent(in) :: alpha0
    real(kind(1d0)), intent(in) :: beta
    real(kind(1d0)), intent(out):: expVec(:)
    integer :: iAlpha
    expVec(1) = alpha0
    do iAlpha = 2, nAlpha
       expVec( iAlpha ) = expVec( iAlpha - 1 ) * beta
    enddo
  end subroutine GenerateEvenTemperedSeries


  !> Initialize the set of Gaussian after reading the parameteres from a file
  subroutine GaussInitReadExponentFile( GaussSet, LMax, NAlpha, ExponentFile, MinRCond, Status )
    !
    Class(ClassGaussian), intent(inout) :: GaussSet
    integer             , intent(in)    :: LMax
    integer             , intent(in)    :: NAlpha
    character(len=*)    , intent(in)    :: ExponentFile
    real(kind(1d0))     , intent(in)    :: MinRCond
    integer, optional   , intent(out)   :: Status
    !
    integer                     :: uid, iAlpha
    integer                     :: iostat
    character(len=IOMSG_LENGTH) :: iomsg
    Real(Kind(1d0))             :: ExpVec(NAlpha)
    !
    call CheckInputParameters( LMax, NAlpha, ExponentFile, uid, Status )
    if(present(Status).and.Status/=0)return
    !
    do iAlpha = 1, NAlpha
       read(uid,*,iostat=iostat,iomsg=iomsg) ExpVec( iAlpha )
       if(iostat/=0)then
          if(present(Status))then
             Status=-3
             return
          else
             call Assert("ClassGaussian::GaussInitReadExponentFile : "//trim(iomsg))
          end if
       endif
    enddo
    close( uid )
    !
    call GaussSet%Init( LMax, NAlpha, ExpVec, MinRCond, Status )
    !
  contains
    !
    subroutine CheckInputParameters(&
         LMax        , &
         NAlpha      , &
         ExponentFile, &
         uid         , &
         Status      )
      integer             , intent(in)  :: LMax, NAlpha
      character(len=*)    , intent(in)  :: ExponentFile
      integer             , intent(out) :: uid
      integer, optional   , intent(out) :: Status
      !
      integer                     :: iostat
      character(len=IOMSG_LENGTH) :: iomsg
      !
      if(present(Status))Status=0
      if( LMax <  0 )then
         if(present(Status))then
            Status = -1
            return
         else
            call Assert("ClassGaussian::GaussInitReadExponentFile::CheckInputParameters : Invalid maximum monomial exponent")
         endif
      endif
      if( NAlpha <= 0 )then
         if(present(Status))then
            Status = -2
            return
         else
            call Assert("ClassGaussian::GaussInitReadExponentFile::CheckInputParameters : Invalid Number of exponents")
         endif
      endif
      open(NewUnit = uid         , &
           File    = ExponentFile, &
           Form    = "Formatted" , &
           Status  = "Old"       , &
           Action  = "Read"      , &
           iostat  = iostat      , &
           iomsg   = iomsg       )
      if(iostat/=0)then
         if(present(Status))then
            Status = -3
            return
         else
            call Assert("ClassGaussian::GaussInitReadExponentFile::CheckInputParameters : "//trim(iomsg))
         endif
      endif
      !
    end subroutine CheckInputParameters
    !
  end subroutine GaussInitReadExponentFile


  !> Evaluate the reduced normalized radial Gaussian basis functions
  !! \f$ u_{\alpha\ell}(r) = N_{\alpha\ell} r^{\ell+1}\exp(\alpha*x^2)
  Real(Kind(1d0)) function GaussianEval( GaussSet, x, iFun, iDer ) &
       result( Gaussian )
    class( ClassGaussian ), intent(in) :: GaussSet
    Real(Kind(1d0))       , intent(in) :: x
    integer               , intent(in) :: iFun
    integer, optional     , intent(in) :: iDer
    integer         :: iDerivative, ell
    Real(Kind(1d0)) :: dell,Alpha
    Gaussian=0.d0
    if( iFun < 1 .or. iFun > GaussSet%NFun )return
    iDerivative = 0
    if( present( iDer ) )then
       if( iDer < 0 ) call Assert("ClassGaussian::Eval : Negative derivatives are not available.")
       if( iDer > 2 ) call Assert("ClassGaussian::Eval : Derivatives beyond second are not available.")
       iDerivative=iDer
    endif
    !
    ell   = GaussSet%ltab( iFun )
    alpha = GaussSet%atab( iFun )
    dell  = dble(ell)
    select case( iDerivative )
    case( 0 )
       Gaussian = myexp( x, ell + 1)
    case( 1 )
       Gaussian = Gaussian + ( dell + 1 ) * myexp( x, ell ) 
       Gaussian = Gaussian - 2.d0 * Alpha * myexp( x, ell+2 )
    case( 2 )
       if(ell>0) Gaussian = Gaussian + dell * (dell+1) * myexp(x,ell-1)
       Gaussian = Gaussian - 2.d0 * Alpha * ( 2 * dell + 3 ) * myexp(x,ell+1)
       Gaussian = Gaussian + 4.d0 * Alpha**2 * myexp(x,ell+3)
    end select
    Gaussian = Gaussian * exp( - Alpha * x * x ) * GaussSet%GetNormFactor( iFun )
  end function GaussianEval
  
  
  Real(Kind(1d0)) function GaussianFunctionEval( GaussSet, x, vec, iDer ) &
       result( Gaussian )
    class( ClassGaussian ), intent(in) :: GaussSet
    Real(Kind(1d0))       , intent(in) :: x
    Real(Kind(1d0))       , intent(in) :: vec(:)
    integer, optional     , intent(in) :: iDer
    !
    integer  :: iDerivative, iFun,NFun
    !
    NFun = GaussSet%GetNFun()
    if(size(vec,1)<NFun)call Assert("ClassGaussian::Eval : Invalid vector of coefficients.")
    iDerivative=0
    if(present(iDer))iDerivative=iDer
    Gaussian = 0.d0
    do iFun = 1, NFun
       Gaussian = Gaussian + vec(iFun) * GaussSet%Eval(x,iFun,iDerivative)
    enddo
  end function GaussianFunctionEval
  
  
  subroutine GaussianTabulate( GaussSet, ndata, xvec, yvec, iFun, iDer )
    class( ClassGaussian ), intent(in)  :: GaussSet
    integer               , intent(in)  :: ndata
    Real(Kind(1d0))       , intent(in)  :: xvec(:)
    Real(Kind(1d0))       , intent(out) :: yvec(:)
    integer               , intent(in)  :: iFun
    integer, optional     , intent(in)  :: iDer
    !
    integer  :: iDerivative, ix
    !
    iDerivative=0
    if(present(iDer))iDerivative=iDer
    yvec = 0.d0
    do ix = 1, ndata
       yvec(ix) = GaussSet%Eval(xvec(ix),iFun,iDerivative)
    enddo
  end subroutine GaussianTabulate
  

  subroutine GaussianFunctionTabulate( GaussSet, ndata, xvec, yvec, fvec, iDer )
    class( ClassGaussian ), intent(in)  :: GaussSet
    integer               , intent(in)  :: ndata
    Real(Kind(1d0))       , intent(in)  :: xvec(:)
    Real(Kind(1d0))       , intent(out) :: yvec(:)
    Real(Kind(1d0))       , intent(in)  :: fvec(:)
    integer, optional     , intent(in)  :: iDer
    !
    integer  :: iDerivative, ix
    !
    iDerivative=0
    if(present(iDer))iDerivative=iDer
    yvec = 0.d0
    do ix = 1, ndata
       yvec(ix) = GaussSet%Eval(xvec(ix),fvec,iDerivative)
    enddo
  end subroutine GaussianFunctionTabulate
  
  
  !> Computes the norm \f$\|g_{\alpha,n}\|_{L^2}\f$ of the radial gaussian
  !! function \f$g_{\alpha,n}(r) = r^{n+1} e^{-\alpha r^2}\f$:
  !! \f{eqnarray}
  !!     N_{\alpha,n} &=& \sqrt{\int_0^\infty dr |g_{alpha,n}(r)|^2} \f$ =\\
  !!                  &=& \left(\frac{\Gamma(n+3/2)}{2(2\alpha)^{n+3/2}}\right)^{1/2}.
  !! \f{eqnarray}
  Real(Kind(1d0)) function MonomialGaussianNorm(Alpha,n) result( Norm )
    Real(Kind(1d0)), intent(in) :: Alpha
    integer        , intent(in) :: n
    Norm = sqrt( 0.5d0 * Gamma( n + 1.5d0 ) * myexp( 2.d0 * Alpha, -n -1.5d0 ) )
    ! Avoids explosion in Gaussian's norm.
    if ( Norm < sqrt(tiny(1d0)) ) then
       !
       Norm = sqrt(tiny(1d0))
       !
    end if
  end function MonomialGaussianNorm


  !> Computes the integral between reduced Gaussian radial functions,
  !! \f[
  !! \int_0^\infty dr g_{\alpha_1,n_1}(r) r^{n} \frac{d^m}{dr^m}g_{\alpha_2,n_2}(r).
  !! \f]
  !! Restrictions:
  !! 
  Real(Kind(1d0)) function GaussianMonomialIntegral(&
       GaussSet           , &
       nExp               , & 
       iBra               , &
       iKet               , &
       KetDerivativeOrder ) &
       result( Integral )
    Class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: nExp
    integer             , intent(in) :: iBra
    integer             , intent(in) :: iKet
    integer, optional   , intent(in) :: KetDerivativeOrder
    !
    Real(Kind(1d0)) :: Alpha, Beta
    integer         :: nAlpha, nBeta
    !
    if(  .not. GaussSet%Initialized )     call Assert("ClassGaussian::Uninitialized Gaussian set")
    if(  iBra<1 .or. iBra>GaussSet%NFun ) call Assert("ClassGaussian::Invalid Gaussian Bra index")
    if(  iKet<1 .or. iKet>GaussSet%NFun ) call Assert("ClassGaussian::Invalid Gaussian Ket index")
    if(  nExp<-2 )                        call Assert("ClassGaussian::Invalid monomial exponent")
    if(  KetDerivativeOrder < 0 .or. &
         KetDerivativeOrder > 2 )         call Assert("ClassGaussian::Invalid derivative order")
    !
    Alpha = GaussSet%atab( iBra )
    nAlpha= GaussSet%ltab( iBra )
    !
    Beta  = GaussSet%atab( iKet )
    nBeta = GaussSet%ltab( iKet )
    !
    Integral = 0.d0
    select case( KetDerivativeOrder )
    case( 0 )
       !>\f[
       !!   Integral = \frac{            \Gamma[ ( 3 + n_\alpha + n_\beta + n_{exp} ) / 2 ] }
       !!                   { 2 (\alpha+\beta)^{ ( 3 + n_\alpha + n_\beta + n_{exp} ) / 2 } }
       !!\f]
       Integral = &
            Gamma( ( 3.d0 + nAlpha + nBeta + nExp ) / 2.d0 ) / &
            ( 2.d0 * ( Alpha + Beta )**( ( 3.d0 + nAlpha + nBeta + nExp ) / 2.d0 ) )
       !
    case( 1 )
       !>\f[
       !!   Integral = \frac{  ( \alpha ( n_\beta + 1 ) - \beta ( 1 + n_\alpha + n_{exp} ) )   
       !!                      \Gamma[ ( 2 + n_\alpha + n_\beta + n_{exp} ) / 2 ] 
       !!                   }
       !!                   { 2 (\alpha+\beta)^{ ( 4 + n_\alpha + n_\beta + n_{exp} ) / 2 } }
       !!\f]
       Integral = &
            ( Alpha * ( nBeta + 1.d0 ) - Beta * (1.d0 + nAlpha + nExp ) ) * &
            Gamma( ( 2.d0 + nAlpha + nBeta + nExp ) / 2.d0 )   / &
            ( 2.d0 * ( Alpha + Beta )**( ( 4.d0 + nAlpha + nBeta + nExp ) / 2.d0 ) )
       !
    case( 2 ) 
       !>\f[
       !!   Integral = \frac{ ( 
       !!                         \alpha^2 n_\beta ( n_\beta + 1 )  +
       !!                         \beta^2  ( n_\alpha + n_{exp} ) ( n_\alpha + n_{exp} + 1 ) - 
       !!                         \alpha\beta [
       !!                                        n_\alpha ( 3 + 2 n_\beta ) + 
       !!                                        2 n_\beta n_{exp} +
       !!                                        3 ( 1 + n_\beta + n_{exp} )
       !!                                     ] 
       !!                     ) \Gamma[ ( 1 + n_\alpha + n_\beta + n_{exp} ) / 2 ] 
       !!                   }
       !!                   { 2 (\alpha+\beta)^{ ( 5 + n_\alpha + n_\beta + n_{exp} ) / 2 } }
       !!\f]
       Integral = &
            ( &
            Alpha**2 * nBeta * ( nBeta + 1.d0 ) + &
            Beta**2 * ( nAlpha + nExp ) * ( nAlpha + nExp + 1.d0 ) - &
            Alpha * Beta * ( &
               nAlpha * ( 3.d0 + 2.d0 * nBeta ) + &
               2.d0 * nBeta * nExp + &
               3.d0 * ( 1.d0 + nBeta + nExp ) &
            ) &
            ) * &
            Gamma( ( 1.d0 + nAlpha + nBeta + nExp ) / 2.d0 ) / &
            ( 2.d0 * ( Alpha + Beta )**( ( 5.d0 + nAlpha + nBeta + nExp ) / 2.d0 ) )
       !
    case DEFAULT
       !
       call Assert("ClassGaussian::Internal Routine Error: GaussianMonomialIntegral")
       !
    end select
    !
  end function GaussianMonomialIntegral


  Real(Kind(1d0)) function GaussianGeneralIntegral( &
       GaussSet          , &
       FunPtr            , & 
       iBra              , &
       iKet              , &
       BraDerivativeOrder, &
       KetDerivativeOrder, &
       LowerBound        , &
       UpperBound        , &
       BreakPoint        ) &
       result( Integral )
    !
    Class(ClassGaussian)     , intent(in) :: GaussSet
    procedure(D2DFun)        , pointer    :: FunPtr
    integer                  , intent(in) :: iBra
    integer                  , intent(in) :: iKet
    integer        , optional, intent(in) :: BraDerivativeOrder
    integer        , optional, intent(in) :: KetDerivativeOrder
    Real(Kind(1d0)), optional, intent(in) :: LowerBound
    Real(Kind(1d0)), optional, intent(in) :: UpperBound
    Real(Kind(1d0)), optional, intent(in) :: BreakPoint
    !
    Real(Kind(1d0)), parameter :: FUNCTION_THRESHOLD = 1.d-16
    !
    integer         :: BraDer, KetDer
    Real(Kind(1d0)) :: a,b,c
    Real(Kind(1d0)) :: ErrorEstimate, Err1, Err2, Int1, Int2
    Real(Kind(1d0)) :: UpperBoundEstimate
    !
    if(  .not. GaussSet%Initialized )     call Assert("ClassGaussian::Uninitialized Gaussian set")
    if(  iBra<1 .or. iBra>GaussSet%NFun ) call Assert("ClassGaussian::Invalid Gaussian Bra index")
    if(  iKet<1 .or. iKet>GaussSet%NFun ) call Assert("ClassGaussian::Invalid Gaussian Ket index")
    BraDer = 0
    if( present( BraDerivativeOrder ) )then
       if( BraDerivativeOrder < 0 ) call Assert("ClassGaussian::Invalid bra derivative order")
       BraDer = BraDerivativeOrder
    endif
    KetDer = 0
    if( present( KetDerivativeOrder ) )then
       if( KetDerivativeOrder < 0 ) call Assert("ClassGaussian::Invalid ket derivative order")
       KetDer = KetDerivativeOrder
    endif
    !
    UpperBoundEstimate = Estimate( &
         GaussSet%atab( iBra ) + GaussSet%atab( iKet ), &
         GaussSet%ltab( iBra ) + GaussSet%ltab( iKet ) + 2, &
         FUNCTION_THRESHOLD )
    !
    a = 0.d0
    if(present(LowerBound)) a = max( LowerBound, 0.d0 )
    b = UpperBoundEstimate
    if(present(UpperBound)) b = min( UpperBound, UpperBoundEstimate )
    c = a 
    if(present(BreakPoint)) c = max( a , min( BreakPoint, b ) )

    Int1=0.d0
    Int2=0.d0
    Int1=0.d0
    Err1=0.d0
    if(c>a) call LocalIntegrationRoutine( a, c, Int1, Err1 )
    Int2=0.d0
    Err2=0.d0
    !   
    ! **** WARNING ****    b = 8.0
    !
    if(b>c) call LocalIntegrationRoutine( c, b, Int2, Err2 )
    ErrorEstimate = dsqrt( Err1**2 + Err2**2 )
    Integral = Int1 + Int2
    !
  contains
    !
     Real(Kind(1d0)) function Estimate( Alpha, n, Thr ) result( R )
      Real(Kind(1d0)), intent(in) :: Alpha
      integer        , intent(in) :: n
      Real(Kind(1d0)), intent(in) :: Thr
      !
      Real(Kind(1d0)), parameter :: RELATIVE_CONVERGENCE = 1.d-2
      Real(Kind(1d0)) :: Nexp, R2
      !
      Nexp = - log( Thr )
      R = sqrt( Nexp / Alpha )
      !** to be improved
      R2 = sqrt( ( Nexp + log(sqrt(Nexp/0.001)) * n ) / Alpha )
      do
         R2 = sqrt( ( Nexp + log(sqrt(Nexp/0.001)) * n ) / Alpha )
         if( (R2-R)/R < RELATIVE_CONVERGENCE )exit
         R=R2
         exit
      enddo
!!$       end if
!!$       if ( log(R) < 0 ) then
!!$!***          write(*,*) "alpha1 + alpha2 = ", Alpha, " and must be < ", -log( Thr )
!!$!***          call Assert( "Too high exponent coefficients for the gaussian integrals upperbound determination routine.")
!!$          R2 = sqrt(  Nexp / Alpha )
!!$          do
!!$             R2 = sqrt(  Nexp / Alpha )
!!$             if( (R2-R)/R < RELATIVE_CONVERGENCE )exit
!!$             R=R2
!!$             exit
!!$          enddo
!!$       else
!!$          R2 = sqrt( ( Nexp + log(R) * n ) / Alpha )
!!$          do
!!$             R2 = sqrt( ( Nexp + log(R) * n ) / Alpha )
!!$             if( (R2-R)/R < RELATIVE_CONVERGENCE )exit
!!$             R=R2
!!$             exit
!!$          enddo
!!$       end if
      !
    end function Estimate
    !
    Real(Kind(1d0)) function Integrand( x )
      Real(Kind(1d0)), intent(in) :: x
      Integrand = funPtr(x) * &
           GaussSet%Eval( x, iBra, BraDer ) * &
           GaussSet%Eval( x, iKet, KetDer )
    end function Integrand
    !
    subroutine LocalIntegrationRoutine(&
         LowerBound    , &
         UpperBound    , &
         Integral      , &
         ErrorEstimate )
      !
      implicit none
      !
      Real(Kind(1d0))           , intent(in)  :: LowerBound
      Real(Kind(1d0))           , intent(in)  :: UpperBound
      Real(Kind(1d0))           , intent(out) :: Integral
      Real(Kind(1d0))           , intent(out) :: ErrorEstimate
      !
      !.. Error Codes
      integer, parameter :: NORMAL_AND_RELIABLE_TERMINATION_ACHIEVED = 0
      integer, parameter :: MAXIMUM_NUMBER_OF_SUBDIVISIONS_ACHIEVED  = 1
      integer, parameter :: OCCURRENCE_OF_ROUNDOFF_ERROR_DETECTED    = 2
      integer, parameter :: EXTREMELY_BAD_INTEGRAND_BEHAVIOR_OCCURS  = 3
      integer, parameter :: THE_INPUT_IS_INVALID                     = 6
      !
      !.. Accuracy Parameters
      Real(Kind(1d0)), parameter :: ABSOLUTE_ACCURACY_REQUESTED     = 1.d-13
      Real(Kind(1d0)), parameter :: RELATIVE_ACCURACY_REQUESTED     = 1.d-10
      integer        , parameter :: INITIAL_NUMBER_OF_SUBINTERVALS  = 7
      integer        , parameter :: INCREASE_NUMBER_OF_SUBINTERVALS = 4
      integer        , parameter :: MAXIMUM_NUMBER_OF_SUBINTERVALS  = 31
      integer        , parameter :: LENGTH_WORK = 4 * MAXIMUM_NUMBER_OF_SUBINTERVALS 
      integer        , parameter :: KEY_NPTS_07_15 = 1
      integer        , parameter :: KEY_NPTS_10_21 = 2
      integer        , parameter :: KEY_NPTS_15_31 = 3
      integer        , parameter :: KEY_NPTS_20_41 = 4
      integer        , parameter :: KEY_NPTS_25_51 = 5
      integer        , parameter :: KEY_NPTS_30_61 = 6
      integer        , parameter :: LOCAL_INTEGRATION_RULE = KEY_NPTS_30_61
      !
      integer :: ierror
      integer :: ActualNumberOfSubintervals
      integer :: NumberOfIntegralEvaluations
      integer :: CurrentMaxNumSubintervals
      !
      integer        , save :: iWork( MAXIMUM_NUMBER_OF_SUBINTERVALS ) = 0
      Real(Kind(1d0)), save :: Work( LENGTH_WORK ) = 0.d0
      !
      interface
         subroutine dqag_noerror(f,a,b,epsabs,epsrel,key,result,abserr,neval,ier,&
              limit,lenw,last,iwork,work)
           double precision a,abserr,b,epsabs,epsrel,f,result,work
           integer ier,iwork,key,last,lenw,limit,lvl,l1,l2,l3,neval
           dimension iwork(limit),work(lenw)
           external f
         end subroutine dqag_noerror
      end interface
      !
      CurrentMaxNumSubintervals = INITIAL_NUMBER_OF_SUBINTERVALS
      do
         ierror=0
         call dqag_noerror(                & 
              Integrand                  , &
              LowerBound                 , &
              UpperBound                 , &
              ABSOLUTE_ACCURACY_REQUESTED, &
              RELATIVE_ACCURACY_REQUESTED, &
              LOCAL_INTEGRATION_RULE     , &
              Integral                   , &
              ErrorEstimate              , &
              NumberOfIntegralEvaluations, &
              ierror                     , &
              CurrentMaxNumSubintervals  , &
              LENGTH_WORK                , &
              ActualNumberOfSubintervals , &
              iWork                      , &
              Work                       )
         select case( ierror )
         case( NORMAL_AND_RELIABLE_TERMINATION_ACHIEVED )
            exit
         case( MAXIMUM_NUMBER_OF_SUBDIVISIONS_ACHIEVED )
            CurrentMaxNumSubintervals = &
                 CurrentMaxNumSubintervals + &
                 INCREASE_NUMBER_OF_SUBINTERVALS
         case( OCCURRENCE_OF_ROUNDOFF_ERROR_DETECTED )
            CurrentMaxNumSubintervals = &
                 CurrentMaxNumSubintervals + &
                 INCREASE_NUMBER_OF_SUBINTERVALS
         case( EXTREMELY_BAD_INTEGRAND_BEHAVIOR_OCCURS )
            exit
         case( THE_INPUT_IS_INVALID )
            exit
         end select
         if( CurrentMaxNumSubintervals > MAXIMUM_NUMBER_OF_SUBINTERVALS ) exit
      enddo
      return
    end subroutine LocalIntegrationRoutine
    !
  end function GaussianGeneralIntegral


  !> Computes the monoelectronic multipole
  !! \f[
  !!    \int_0^\infty dr G_1(R)\frac{r_<^\ell}{r_>^{\ell+1}}G_2(r)
  !! \f]
  !! where \f$r_<=\min(r,R)\f$ and \f$r_>=\max(r,R)\f$. 
  !!
  Real(Kind(1d0)) function MonoElectronicMultipole(GaussSet,iBra,iKet,R,l) result(Multipole)
    Class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: iBra, iKet
    Real(Kind(1d0))     , intent(in) :: R
    integer             , intent(in) :: l
    procedure(D2DFun)   , pointer    :: FunPtr
    FunPtr => MultipolarPotential
    Multipole = GaussSet%Integral( FunPtr, iBra, iKet, BreakPoint = R )
  contains
    DoublePrecision function MultipolarPotential(x) result(y)
      Real(Kind(1d0)), intent(in) :: x
      y = myexp(min(x,R),l)*myexp(max(x,R),-l-1)
    end function MultipolarPotential
  end function MonoElectronicMultipole

  

  !> return  \f$ a^b \f$
  real(kind(1d0)) function myexp_DD(a,b) result(res)
    real(kind(1d0)), intent(in) :: a,b
    res=1.d0; if(b==0.d0) return
    res=0.d0; if(a<=0.d0) return
    res=exp(b*log(a))
  end function myexp_DD


  !> return  \f$ a^b \f$
  Real(Kind(1d0)) function myexp_DI(a,ib) result(res)
    Real(Kind(1d0)), intent(in) :: a
    integer        , intent(in) :: ib
    integer         :: iExp
    Real(Kind(1d0)) :: a_
    res=1.d0
    if(ib==0) return
    res=0.d0
    if(a==0.d0.or.a==-0.d0)return
    res=1.d0
    if(ib>0)then
       do iExp = 1, ib
          res=res*a
       enddo
    else
       a_=1.d0/a
       do iExp = ib, -1
          res=res*a_
       enddo
    endif
  end function myexp_DI


  Real(Kind(1d0)) function gamma(x) result(res)
    Real(Kind(1d0)), intent(in) :: x
    !
    Real(Kind(1d0)) :: y
    Real(Kind(1d0)), external :: DGAMMA
    y=x
    res=DGAMMA(y)
  end function gamma


  subroutine FinalizePreconditioner( self )
    type(ClassGaussianPreconditioner), intent(inout) :: self
    self%MinIndex = 0
    self%MaxIndex = 0
    self%MinRCond =0.d0
    if(allocated(self%A))deallocate(self%A)
    if(allocated(self%E))deallocate(self%E)
    self%INITIALIZED=.FALSE.
  end subroutine FinalizePreconditioner


  subroutine FreePreconditioner( self )
    class(ClassGaussianPreconditioner), intent(inout) :: self
    self%MinIndex = 0
    self%MaxIndex = 0
    self%MinRCond =0.d0
    if(allocated(self%A))deallocate(self%A)
    if(allocated(self%E))deallocate(self%E)
    self%INITIALIZED=.FALSE.
  end subroutine FreePreconditioner
  

  function PreconditionerFileName( L ) result( strn )
    integer         , intent(in)  :: L
    character(len=:), allocatable :: strn
    allocate(strn,source="GaussianPreconditioner"//AlphabeticNumber(L))
  end function PreconditionerFileName

  
  logical function PreconditionerIsAvailable( GaussSet, L, dir ) result( Available )
    class(ClassGaussian), intent(in) :: GaussSet
    integer             , intent(in) :: L
    character(len=*)    , intent(in) :: dir
    !
    character(len=:), allocatable  :: FileName
    FileName = trim(dir)//PreconditionerFileName( L )
    Available = GaussSet%MatchFingerPrintOnFile( FileName )
  end function PreconditionerIsAvailable


  subroutine ComputePreconditioner( GaussSet, L)
    class(ClassGaussian)          , intent(inout) :: GaussSet
    integer                       , intent(in)    :: L

    integer :: MinRegIndex
    integer :: MaxRegIndex
    Real(Kind(1d0)), allocatable :: A(:,:), Eval(:)
    integer :: NRegular
    integer :: NPrecon
    integer :: iRow, iCol, iFun
    Real(Kind(1d0)) :: MinRCond
    procedure(D2DFun), pointer :: fptr
    character(len=*), parameter :: HERE="ClassGaussian::ComputePreconditioner : "

    if( L < 0 .or. L > GaussSet%GetLmax() ) call Assert(HERE//"Invalid angular momentum")

    !.. Build the overlap matrix between regular functions
    MinRegIndex = GaussSet%GetMinimumRegularIndex(L)
    MaxRegIndex = GaussSet%GetMaximumRegularIndex(L)
    NRegular=MaxRegIndex-MinRegIndex+1
    allocate(A(NRegular,NRegular),Eval(NRegular))
    fptr => Unity
    do iCol = 1, NRegular
       do iRow = 1, NRegular
          A(iRow,iCol) = GaussSet%Integral( fptr, iRow + MinRegIndex - 1, iCol + MinRegIndex - 1 )
       enddo
    enddo

    !.. Diagonalize the overlap matrix
    call SimpleDSYEV(NRegular,A,NRegular,Eval)
    

    !.. Determines the number of linearly independent functions
    NPrecon = 0
    MinRCond = GaussSet%GetMinRCond()
    do iFun = 1, NRegular
       if( Eval(iFun) / Eval(NRegular) >= MinRCond )then
          NPrecon = NPrecon + 1
          A(:,NPrecon)=A(:,iFun)/sqrt(Eval(iFun))
       endif
       write(*,"(i0,3(x,e14.6),x,i0)") iFun, Eval(iFun),Eval(iFun)/Eval(NRegular), MinRCond, NPrecon
    enddo
    write(*,*) "MinRegIndex :", MinRegIndex
    write(*,*) "MaxRegIndex :", MaxRegIndex
    write(*,*) "NRegular    :", NRegular
    write(*,*) "NPrecon     :", NPrecon
    
    if(NPrecon == 0)then
       call ErrorMessage(HERE//" preconditioning failed")
       stop
    endif
    call GaussSet%Preconditioner(L)%init( &
         MinRegIndex, MaxRegIndex, NPrecon, &
         MinRCond, Eval, A )
    deallocate(A,Eval)

  contains
    Real(Kind(1d0)) function Unity( r )
      Real(Kind(1d0)), intent(in) :: r
      Unity = 1.d0
    end function Unity
  end subroutine ComputePreconditioner

  
  subroutine InitPreconditioner( self, &
       MinRegIndex, MaxRegIndex, NPrecon, &
       MinRCond, E, A )
    class(ClassGaussianPreconditioner), intent(inout) :: self
    integer, intent(in)         :: MinRegIndex
    integer, intent(in)         :: MaxRegIndex
    Real(Kind(1d0)), intent(in) :: A(:,:)
    Real(Kind(1d0)), intent(in) :: E(:)
    Real(Kind(1d0)), intent(in) :: MinRCond
    integer, intent(in)         :: NPrecon
    integer :: NRegular
    call self%free()
    self%MinIndex=MinRegIndex
    self%MaxIndex=MaxRegIndex
    self%NRegular=MaxRegIndex-MinRegIndex+1
    self%NPrecon =NPrecon
    self%MinRCond=MinRCond
    allocate(self%E,source=E(1:NPrecon))
    NRegular=self%NRegular
    allocate(self%A,source=A(1:NRegular,1:NPrecon))
    self%INITIALIZED = .TRUE.
  end subroutine InitPreconditioner


  subroutine SavePreconditioner( self, L, dir, iostat )
    class(ClassGaussian), intent(in) :: self
    integer             , intent(in) :: L
    character(len=*)    , intent(in) :: dir
    integer, optional   , intent(out):: iostat
    !
    character(len=*), parameter    :: HERE = "ClassGaussian::SavePreconditioner : "
    character(len=:), allocatable  :: FileName
    integer                        :: uid, iostat1
    character(len=IOMSG_LENGTH)    :: iomsg
    !
    FileName = trim(dir)//PreconditionerFileName( L )
    open(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "unknown"    , &
         Action  = "write"      , &
         iostat  =  iostat1     , &
         iomsg   =  iomsg       )
    if(iostat1/=0)then
       if(present(iostat))then
          call ErrorMessage(HERE//trim(iomsg))
          iostat=-1
          return
       else
          call Assert(HERE//trim(iomsg))
       end if
    endif
    call self%writeFingerprint(uid)
    call self%Preconditioner(L)%write(uid,iostat)
    close(uid)
    !
  end subroutine SavePreconditioner


  subroutine LoadPreconditioner( self, L, dir, iostat )
    class(ClassGaussian), intent(inout) :: self
    integer             , intent(in)    :: L
    character(len=*)    , intent(in)    :: dir
    integer, optional   , intent(out)   :: iostat
    !
    character(len=*), parameter    :: HERE = "ClassGaussian::LoadPreconditioner : "
    character(len=:), allocatable  :: FileName
    integer                        :: uid
    integer                        :: iostat1
    character(len=IOMSG_LENGTH)    :: iomsg
    !
    FileName = trim(dir)//PreconditionerFileName( L )
    open(NewUnit =  uid         , &
         File    =  FileName    , &
         Form    = "unformatted", &
         Status  = "old"        , &
         Action  = "read"       , &
         iostat  =  iostat1      , &
         iomsg   =  iomsg       )
    if(iostat1/=0)then
       if(present(iostat))then
          iostat=iostat1
          call ErrorMessage(HERE//trim(iomsg))
          return
       else
          call Assert(HERE//trim(iomsg))
       endif
    endif
    if(.not.self%matchFingerprintOnUnit(uid))then
       call ErrorMessage(HERE//"Invalid Fingerprint")
       if(present(iostat))then
          iostat=-1
          return
       else
          stop
       endif
    endif
    call self%Preconditioner(L)%read(uid,iostat)
    close(uid)
  end subroutine LoadPreconditioner


  !> Write on unformatted unit the content of a GaussianPreconditioner
  !
  ! *** Generalize to deal with the case of both formatted and
  !     unformatted units
  !
  subroutine WritePreconditionerOnUnit( self, uid, iostat )
    class(ClassGaussianPreconditioner), intent(in) :: self
    integer                           , intent(in) :: uid
    integer, optional                 , intent(out):: iostat
    integer :: iReg, iPre
    integer :: iostat1, iostat2, iostat3, iostat4, iostat5, iostat6, iostat7
    write(uid,iostat=iostat1) self%MinIndex
    write(uid,iostat=iostat2) self%MaxIndex
    write(uid,iostat=iostat3) self%NRegular
    write(uid,iostat=iostat4) self%NPrecon
    write(uid,iostat=iostat5) self%MinRCond
    write(uid,iostat=iostat6)(self%E(iPre),iPre=1,self%NPrecon)
    write(uid,iostat=iostat7)((self%A(iReg,iPre), &
         iReg=1,self%NRegular), &
         iPre=1,self%NPrecon )
    if( present( iostat ) )then
       iostat = 0
       if ( iostat1 /= 0 .or. &
            iostat2 /= 0 .or. &
            iostat3 /= 0 .or. &
            iostat4 /= 0 .or. &
            iostat5 /= 0 .or. &
            iostat6 /= 0 .or. &
            iostat7 /= 0 ) iostat = -1
    endif
  end subroutine WritePreconditionerOnUnit


  !> Read from unformatted unit the content of a 
  !! GaussianPreconditioner object
  subroutine ReadPreconditionerOnUnit( self, uid, iostat )
    class(ClassGaussianPreconditioner), intent(out):: self
    integer                           , intent(in) :: uid
    integer, optional                 , intent(out):: iostat
    integer :: iReg, iPre
    integer :: iostat1, iostat2, iostat3, iostat4, iostat5, iostat6, iostat7
    self%Initialized=.TRUE.
    read(uid,iostat=iostat1) self%MinIndex
    read(uid,iostat=iostat2) self%MaxIndex
    read(uid,iostat=iostat3) self%NRegular
    read(uid,iostat=iostat4) self%NPrecon
    read(uid,iostat=iostat5) self%MinRCond
    if(allocated(self%E))deallocate(self%E)
    allocate(self%E(self%NPrecon))
    read(uid,iostat=iostat6)(self%E(iPre),iPre=1,self%NPrecon)
    if(allocated(self%A))deallocate(self%A)
    allocate(self%A(self%NRegular,self%NPrecon))
    read(uid,iostat=iostat7)((self%A(iReg,iPre), &
         iReg=1,self%NRegular), &
         iPre=1,self%NPrecon )
    if( present(iostat) )then
       iostat = 0
       if ( iostat1 /= 0 .or. &
            iostat2 /= 0 .or. &
            iostat3 /= 0 .or. &
            iostat4 /= 0 .or. &
            iostat5 /= 0 .or. &
            iostat6 /= 0 .or. &
            iostat7 /= 0 )then
          iostat = -1
          self%Initialized = .FALSE.
       endif
    endif
  end subroutine ReadPreconditionerOnUnit


  integer function ClassGaussianNRegular( self, L ) result(NRegular)
    class(ClassGaussian), intent(in) :: self
    integer             , intent(in) :: L
    character(len=*), parameter :: HERE="ClassGaussian::NRegular : "
    NRegular = self%GetMaximumRegularIndex(L) - self%GetMinimumRegularIndex(L)+1
  end function ClassGaussianNRegular


  integer function ClassGaussianNPreconditioned( self, L ) result(NPreconditioned)
    class(ClassGaussian), intent(in) :: self
    integer             , intent(in) :: L
    character(len=*), parameter :: HERE="ClassGaussian::NPreconditioned : "
    if(.not.self%Preconditioner(L)%INITIALIZED)call Assert(HERE//"Preconditioner not initialized")
    NPreconditioned = self%Preconditioner(L)%NPrecon
  end function ClassGaussianNPreconditioned


  subroutine FetchPreconditioner( self, Precon, L )
    class(ClassGaussian), intent(in) :: self
    real(kind(1d0)),      intent(out):: Precon(:,:)
    integer             , intent(in) :: L
    character(len=*), parameter :: HERE="ClassGaussian::FetchPreconditioner : "
    integer :: NReg, NPre
    if( .not. self%Preconditioner( L )%INITIALIZED ) call Assert(HERE//"Preconditioner not available")
    NReg = self%NRegular(L)
    NPre = self%NPreconditioned(L)
    Precon(1:NReg,1:NPre)=self%Preconditioner(L)%A
  end subroutine FetchPreconditioner


  !> Creates in a directory all the configuration
  !!    files that define the current basis.
  subroutine SaveConfig(self, ConfigurationFile, Dir)
    !
    Class(ClassGaussian), intent(in) :: self
    character(len=*)   , intent(in)  :: ConfigurationFile
    character(len=*)   , intent(in)  :: Dir
    !
    call SYSTEM("mkdir -p "//trim(Dir))
    call SYSTEM("cp "//trim(ConfigurationFile)//" "//trim(Dir))
    !
  end subroutine SaveConfig


  !> Gets the information of the linear independent gaussian function (preconditioned): monomial exponent, gaussian exponent and normalization factor.
  subroutine GetLIGaussian( self, MonExp, GaussExp, NormFactor )
    !
    !> Class of Gaussian functions.
    Class(ClassGaussian), intent(in) :: self
    !> Monomial exponents.
    integer, allocatable, intent(out) :: MonExp(:)
    !> Gaussian exponents.
    real(kind(1d0)), allocatable, intent(out) :: GaussExp(:)
    !> Normalization factors.
    real(kind(1d0)), allocatable, intent(out) :: NormFactor(:)
    !
    integer :: NumFunctions, i
    !
    NumFunctions = self%GetNFun()
    !
    allocate( MonExp(NumFunctions), GaussExp(NumFunctions), NormFactor(NumFunctions) )
    MonExp = 0
    GaussExp = 0.d0
    NormFactor = 0.d0
    !
    do i = 1, NumFunctions
       !
       MonExp(i) = self%ltab(i)
       GaussExp(i) = self%atab(i)
       NormFactor(i) = self%ftab(i)
       !
    end do
    
    !
  end subroutine GetLIGaussian



  !> Gets a vector with all the monomials exponents.
  subroutine GetAllMonomials( Gauss, Vec )
    !> Class of Gaussian functions.
    class(ClassGaussian), intent(in)  :: Gauss
    !> Vector that will contain all the Gaussian monomials.
    integer, allocatable, intent(out) :: Vec(:)
    !
    if ( .not. allocated(Gauss%ltab) ) then
       call Assert( "Imposible to get the monomials's vector when it does not exist yet." )
    else
       allocate( Vec, source = Gauss%ltab )
    end if
    !
  end subroutine GetAllMonomials



  !> Gets a vector with all the monomials exponents.
  subroutine GetAllExponents( Gauss, Vec )
    !> Class of Gaussian functions.
    class(ClassGaussian), intent(in)  :: Gauss
    !> Vector that will contain all the Gaussian monomials.
    real(kind(1d0)), allocatable,    intent(out) :: Vec(:)
    !
    if ( .not. allocated(Gauss%atab) ) then
       call Assert( "Imposible to get the exponents's vector when it does not exist yet." )
    else
       allocate( Vec, source = Gauss%atab )
    end if
    !
  end subroutine GetAllExponents



end module ModuleGaussian
