!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> \file
!!
!! Defines some subroutines that serve as an interface with those contained in LAPACK package.


module MyLapackModule

  implicit none

  private
  public :: mydsyev

contains


  !> Simplified interface to the DSYEV diagonalization routine.
  subroutine mydsyev(N,A,LDA,W)
    !
    !> N : Dimension of the matrix to diagonalize.
    integer        , intent(in)    :: N
    !
    !> A(i,j) : input  : Matrix to diagonalize.
    !!          Output : A(:,i) is the i-th eigenvector of A.
    real(kind(1d0)), intent(inout) :: A(:,:)
    !
    !> Leading dimension of A.
    integer        , intent(in)    :: LDA
    !
    !> W(i): i-th eigenvalue of A.
    real(kind(1d0)), intent(out)   :: W(:)
    !
    !.. Variables for matrix diagonalization
    !..
    integer                      :: INFO
    integer                      :: LWORK
    real(kind(1d0)), allocatable :: WORK(:)
    !
    !.. Initializes the service vector 
    !   for the diagonalization
    !..
    LWORK = 16 * N
    allocate(WORK(LWORK))
    WORK=0.d0
    !
    W=0.d0
    call DSYEV("V","U",N,A,LDA,W,WORK,LWORK,INFO)
    !
    if(INFO/=0)then
       !
       write(*,"(a,i4)")"(EEE) - DSYEV failed : ",INFO
       write(*,"(a)"   )"        STOP FORCED"
       stop
       !
    endif
    !
    deallocate(WORK)
    !
    return
    !
  end subroutine mydsyev


end module MyLapackModule
