!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
module ModuleShortRangeChannel

  use, intrinsic :: ISO_C_BINDING
  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleErrorHandling
  use ModuleString
  use ModuleIO
  use ModuleConstants
  use ModuleGroups
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleParentIons
  use ModuleMatrix
  use ModuleShortRangeOrbitals
  use symba
  
  implicit none
  

  private

  type, public :: ClassShortRangeChannel
     ! {{{ private attributes

     private

     !> Interface Dir
     character(len=:), allocatable    :: InterfaceDir

     !> Storage directory
     character(len=:), allocatable    :: StorageDir

     !> State of the (N-1)-electron state to which
     !! an N-th electron is added to define the Channel
     type(ClassParentIon), pointer, public :: ParentIon

     !> Maximum angular momentum the space can represent.
     integer                          :: Lmax

     !> Total Multiplicity
     integer                          :: TotalMultiplicity
     
     !> Total Irrep of the Channel, given by the 
     !! irrep of the parent ion times that of the
     !! outer electron, restricted to a short-range orbital
     !! (either a molecular or a diffuse orbital)
     type(ClassIrrep), pointer, public  :: TotalIrrep
     
     type(ClassShortRangeSymOrbitals) :: SROrb

     ! }}}
   contains
     generic, public :: SetInterfaceDir            => ClassShortRangeChannelSetInterfaceDir
     generic, public :: GetInterfaceDir            => ClassShortRangeChannelGetInterfaceDir
     generic, public :: GetStorageDir              => ClassShortRangeChannelGetStorageDir
     generic, public :: GetTotIrrepName            => ClassShortRangeChannelGetTotIrrepName
     generic, public :: GetTotIrrep                => ClassShortRangeChannelGetTotIrrep
     generic, public :: GetDiffuseIrrep            => ClassShortRangeChannelGetDiffuseIrrep
     generic, public :: GetDiffuseIrrepName        => ClassShortRangeChannelGetDiffuseIrrepName
     generic, public :: GetSymLabel                => ClassShortRangeChannelGetSymLabel
     generic, public :: init                       => ClassShortRangeChannelInit
     generic, public :: show                       => ClassShortRangeChannelShow
     generic, public :: GetTotNumOrbitals          => ClassShortRangeChannelGetTotNumOrbitals
     generic, public :: GetNumDiffuseOrbitals      => ClassShortRangeChannelGetNumDiffuseOrbitals
     generic, public :: GetNumMolOrbitals          => ClassShortRangeChannelGetNumMolOrbitals
     procedure, public :: GetTotMultiplicity       => ClassShortRangeChannelGetTotMultiplicity
     procedure, public :: GetPIMultiplicity        => ClassShortRangeChannelGetPIMultiplicity
     procedure, public :: GetPICharge              => ClassShortRangeChannelGetPICharge
     procedure, public :: GetPI                    => ClassShortRangeChannelGetPI
     generic, public :: GetPIEnergy                => ClassShortRangeChannelGetPIEnergy, ClassShortRangeChannelReadPIEnergy
     procedure, public :: GetLmax                  => ClassShortRangeChannelGetLmax
     generic, public :: ComputeTransfMat           => ClassShortRangeChannelComputeTransfMat
     generic, public :: GetTransfMat               => ClassShortRangeChannelGetTransfMat
     generic, public :: GetCartesianSet            => ClassShortRangeChannelGetCartesianSet
     generic, public :: ComputeFullTransfMat       => ClassShortRangeChannelComputeFullTransfMat
     generic, public :: FetchFullTransfMat         => ClassShortRangeChannelFetchFullTransfMat
     generic, public :: FetchXlmToCartesianMatrix  => ClassShortRangeChannelFetchXlmToCartesianMatrix
     generic, public :: FetchCartMonInterface      => ClassShortRangeChannelFetchCartMonInterface
     generic, public :: FetchCartMonInitialized    => ClassShortRangeChannelFetchCartMonInitialized
     generic, public :: FetchXlmTransfIndexes      => ClassShortRangeChannelFetchXlmTransfIndexes
     generic, public :: FetchCartesianCoeff        => ClassShortRangeChannelFetchCartesianCoeff
     generic, public :: GetPILabel                 => ClassShortRangeChannelGetPILabel
     generic, public :: GetPINelect                => ClassShortRangeChannelGetPINelect
     generic, public :: free                       => ClassShortRangeChannelFree
     ! {{{ private procedures

     procedure, private :: ClassShortRangeChannelInit
     procedure, private :: ClassShortRangeChannelShow
     procedure, private :: ClassShortRangeChannelGetTotNumOrbitals
     procedure, private :: ClassShortRangeChannelGetTotIrrepName
     procedure, private :: ClassShortRangeChannelGetTotIrrep
     procedure, private :: ClassShortRangeChannelGetDiffuseIrrep
     procedure, private :: ClassShortRangeChannelGetDiffuseIrrepName
     procedure, private :: ClassShortRangeChannelGetSymLabel
     procedure, private :: ClassShortRangeChannelGetNumDiffuseOrbitals
     procedure, private :: ClassShortRangeChannelGetNumMolOrbitals
     procedure, private :: ClassShortRangeChannelFree
     procedure, private :: ClassShortRangeChannelSetInterfaceDir
     procedure, private :: ClassShortRangeChannelGetInterfaceDir
     procedure, private :: ClassShortRangeChannelGetStorageDir
     procedure, private :: ClassShortRangeChannelComputeTransfMat
     procedure, private :: ClassShortRangeChannelGetTransfMat
     procedure, private :: ClassShortRangeChannelGetCartesianSet
     procedure, private :: ClassShortRangeChannelComputeFullTransfMat
     procedure, private :: ClassShortRangeChannelFetchFullTransfMat
     procedure, private :: ClassShortRangeChannelFetchXlmToCartesianMatrix
     procedure, private :: ClassShortRangeChannelFetchCartMonInterface
     procedure, private :: ClassShortRangeChannelFetchCartMonInitialized
     procedure, private :: ClassShortRangeChannelFetchXlmTransfIndexes
     procedure, private :: ClassShortRangeChannelFetchCartesianCoeff
     procedure, private :: ClassShortRangeChannelGetPILabel
     procedure, private :: ClassShortRangeChannelGetPI
     procedure, private :: ClassShortRangeChannelGetPINelect
     procedure, private :: ClassShortRangeChannelGetLmax
     procedure, private :: ClassShortRangeChannelGetPIEnergy
     procedure, private :: ClassShortRangeChannelReadPIEnergy
     final              :: ClassShortRangeChannelFinal

     ! }}}
  end type ClassShortRangeChannel




contains



  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  !      ClassShortRangeChannel
  !-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  subroutine ClassShortRangeChannelInit( &
       Channel          , &
       ParentIon        , &
       Lmax             , &
       TotalMultiplicity, &
       TotalIrrep       , &
       StorageDir )
    !
    class(ClassShortRangeChannel) , intent(inout) :: Channel
    class(ClassParentIon), target , intent(in)    :: ParentIon
    integer                       , intent(in)    :: Lmax
    integer                       , intent(in)    :: TotalMultiplicity
    class(ClassIrrep),     target , intent(in)    :: TotalIrrep
    character(len=*)              , intent(in)    :: StorageDir
    !
    class(ClassIrrep), pointer :: OrbitalIrrep
    !
    Channel%ParentIon => ParentIon
    !
    Channel%Lmax = Lmax
    Channel%TotalMultiplicity = TotalMultiplicity
    !
    Channel%TotalIrrep => TotalIrrep
    !
    if ( allocated(Channel%StorageDir) ) deallocate( Channel%StorageDir )
    allocate( Channel%StorageDir, source = StorageDir )
    !
    OrbitalIrrep => TotalIrrep * ParentIon%GetIrrep()
    !
    call Channel%SROrb%Init( OrbitalIrrep, StorageDir, Lmax )
    !
  end subroutine ClassShortRangeChannelInit



  function ClassShortRangeChannelGetDiffuseIrrep( Self ) result(Dirrep)
    class(ClassShortRangeChannel) , intent(in) :: Self
    class(ClassIrrep), pointer :: Dirrep
    Dirrep => Self%TotalIrrep * Self%ParentIon%GetIrrep()
  end function ClassShortRangeChannelGetDiffuseIrrep


  function ClassShortRangeChannelGetDiffuseIrrepName( Self ) result(IrrepName)
    class(ClassShortRangeChannel) , intent(in) :: Self
    character(len=:), allocatable :: IrrepName
    type(ClassIrrep), pointer :: Dirrep
    Dirrep => Self%GetDiffuseIrrep()
    allocate( IrrepName, source = Dirrep%GetName() )
  end function ClassShortRangeChannelGetDiffuseIrrepName




  subroutine ClassShortRangeChannelShow( Channel, unit )
    class(ClassShortRangeChannel) , intent(in) :: Channel
    integer, optional             , intent(in) :: unit
    integer :: outunit
    outunit = OUTPUT_UNIT
    if(present(unit)) outunit = unit
    write(outunit,"(a)") "Short Range Channel Info : "
    call Channel%ParentIon%Show(unit)
    write(outunit,"(a,i4)"          ) "  Maximum Angular Momentum :",Channel%Lmax
    write(outunit,"(a,i4)"          ) "  Channel Multiplicity :",Channel%TotalMultiplicity
    write(outunit,"(a)",advance="no") "  Channel Symmetry     :"
    call Channel%TotalIrrep%show(unit)
    call Channel%SROrb%show(unit)
  end subroutine ClassShortRangeChannelShow


  subroutine ClassShortRangeChannelFree( Channel )
    class(ClassShortRangeChannel), intent(inout) :: Channel
    Channel%ParentIon  => NULL()
    Channel%TotalIrrep => NULL()
    call Channel%SROrb%Free()
    Channel%Lmax = -1
    Channel%TotalMultiplicity = 0
  end subroutine ClassShortRangeChannelFree

  
  subroutine ClassShortRangeChannelFinal( Channel )
    type(ClassShortRangeChannel) :: Channel
    call Channel%free()
  end subroutine ClassShortRangeChannelFinal


  integer function ClassShortRangeChannelGetTotNumOrbitals( SRC ) result( NumOrb )
    !
    class(ClassShortRangeChannel), intent(in) :: SRC
    !
    NumOrb = SRC%SROrb%GetTotNumOrbitals()
    !
  end function ClassShortRangeChannelGetTotNumOrbitals


  function ClassShortRangeChannelGetTotIrrepName( Self ) result(IrrepName)
    class(ClassShortRangeChannel), intent(in) :: Self
    character(len=:), allocatable :: IrrepName
    allocate( IrrepName, source = Self%TotalIrrep%GetName() )
  end function ClassShortRangeChannelGetTotIrrepName


  function ClassShortRangeChannelGetTotIrrep( Self ) result(Irrep)
    class(ClassShortRangeChannel), target, intent(in) :: Self
    type(ClassIrrep), pointer :: Irrep
    Irrep => Self%TotalIrrep
  end function ClassShortRangeChannelGetTotIrrep


  !> Gets the number of molecular orbitals (those which do not overlap with B-splines).
  integer function ClassShortRangeChannelGetNumMolOrbitals( SRC ) result( NumOrb )
    !
    class(ClassShortRangeChannel), intent(in) :: SRC
    !
    NumOrb = SRC%SROrb%GetNumMolOrbitals()
    !
  end function ClassShortRangeChannelGetNumMolOrbitals



  integer function ClassShortRangeChannelGetNumDiffuseOrbitals( SRC ) result( Num )
    !
    class(ClassShortRangeChannel), intent(in) :: SRC
    !
    Num = SRC%SROrb%GetNumDiffuseOrbitals()
    !
  end function ClassShortRangeChannelGetNumDiffuseOrbitals



  integer function ClassShortRangeChannelGetTotMultiplicity( SRC ) result( TotMult )
    !
    class(ClassShortRangeChannel), intent(in) :: SRC
    !
    TotMult = SRC%TotalMultiplicity
    !
  end function ClassShortRangeChannelGetTotMultiplicity



  function ClassShortRangeChannelGetSymLabel( Self ) result(Label)
    class(ClassShortRangeChannel), intent(in) :: Self
    character(len=:), allocatable :: Label
    allocate( Label, source = AlphabeticNumber(Self%GetTotMultiplicity())//Self%GetTotIrrepName() )
  end function ClassShortRangeChannelGetSymLabel



  integer function ClassShortRangeChannelGetPIMultiplicity( SRC ) result( PIMult )
    class(ClassShortRangeChannel), intent(in) :: SRC
    PIMult = SRC%ParentIon%GetMultiplicity()
  end function ClassShortRangeChannelGetPIMultiplicity




  integer function ClassShortRangeChannelGetLmax( SRC ) result( LMax )
    class(ClassShortRangeChannel), intent(in) :: SRC
    LMax = SRC%Lmax
  end function ClassShortRangeChannelGetLmax



  integer function ClassShortRangeChannelGetPICharge( SRC ) result( PICharge )
    class(ClassShortRangeChannel), intent(in) :: SRC
    PICharge = SRC%ParentIon%GetCharge()
  end function ClassShortRangeChannelGetPICharge



  real(kind(1d0)) function ClassShortRangeChannelGetPIEnergy( SRC ) result( PIE )
    class(ClassShortRangeChannel), intent(in) :: SRC
    PIE = SRC%ParentIon%GetEnergy()
  end function ClassShortRangeChannelGetPIEnergy


  real(kind(1d0)) function ClassShortRangeChannelReadPIEnergy( SRC, StorageDir ) result( PIE )
    class(ClassShortRangeChannel), intent(in) :: SRC
    character(len=*)             , intent(in) :: StorageDir
    PIE = SRC%ParentIon%ReadEnergy(StorageDir)
  end function ClassShortRangeChannelReadPIEnergy




  !> Computes the transformation matrices between cartesian Gaussians and symmetry adapted spherical harmonics. 
  subroutine ClassShortRangeChannelComputeTransfMat( SRC )
    !
    class(ClassShortRangeChannel), intent(inout) :: SRC
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The transformation matrices cannot be computed &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%ComputeTransfMat()
    !
  end subroutine ClassShortRangeChannelComputeTransfMat




  subroutine ClassShortRangeChannelGetTransfMat( SRC, Identifier, Array )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    character(len=*),                intent(in)    :: Identifier
    complex(kind(1d0)), allocatable, intent(out)   :: Array(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The transformation matrices cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%GetTransfMat( Identifier, Array )
    !
  end subroutine ClassShortRangeChannelGetTransfMat



  function ClassShortRangeChannelGetCartesianSet( SRC ) result( CartSet )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    type(ClassCartesianSymmetricSet), pointer :: CartSet
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The cartesian symmetric set  cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    CartSet => SRC%SROrb%GetCartesianSet( )
    !
  end function ClassShortRangeChannelGetCartesianSet



  !> Computes the full transformation matrix between cartesian Gaussians multiplied by all the used exponentials and symmetry adapted spherical harmonics also multiplied by the exponentials, to pass from Xlm to cartesian representation. 
  subroutine ClassShortRangeChannelComputeFullTransfMat( SRC, NumExp )
    !
    class(ClassShortRangeChannel), intent(inout) :: SRC
    integer,                       intent(in)    :: NumExp
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The transformation matrices cannot be computed if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%ComputeFullTransfMat( NumExp )
    !
  end subroutine ClassShortRangeChannelComputeFullTransfMat



  !> Fetches the transformation matrix that expresses the cartesian monomials in terms of the symmetric adapted spherical harmonics Xlm.
  subroutine ClassShortRangeChannelFetchXlmToCartesianMatrix( SRC, Mat )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    complex(kind(1d0)), allocatable, intent(out)   :: Mat(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The transformation matrices cannot be fetched if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchXlmToCartesianMatrix( Mat )
    !
  end subroutine ClassShortRangeChannelFetchXlmToCartesianMatrix



  !> Fetches the full transformation matrix that expresses the cartesian monomials in terms of the symmetric adapted spherical harmonics Xlm, where both sets are multiplied by the exponentials functions.
  subroutine ClassShortRangeChannelFetchFullTransfMat( SRC, Mat )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    complex(kind(1d0)), allocatable, intent(out)   :: Mat(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The full transformation matrix cannot be fetched if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchFullTransfMat( Mat )
    !
  end subroutine ClassShortRangeChannelFetchFullTransfMat




  !> Fetches the array of cartesian monomials as appear in the interface file.
  subroutine ClassShortRangeChannelFetchCartMonInterface( SRC, MonArray )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    integer, allocatable,            intent(out)   :: MonArray(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The cartesian monomials as appear in the interface file cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchCartMonInterface( MonArray )
    !
  end subroutine ClassShortRangeChannelFetchCartMonInterface


  !> Fetches the array of cartesian monomials as it was initialized in ClassCartesianSymSet.
  subroutine ClassShortRangeChannelFetchCartMonInitialized( SRC, MonArray )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    integer, allocatable,            intent(out)   :: MonArray(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The cartesian monomials as it were initialized cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchCartMonInitialized( MonArray )
    !
  end subroutine ClassShortRangeChannelFetchCartMonInitialized


  !> Fetches the array of symmetry adapted spherical harmonics's indexes.
  subroutine ClassShortRangeChannelFetchXlmTransfIndexes( SRC, IndArray )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    integer, allocatable,            intent(out)   :: IndArray(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The symmetry adapted spherical harmonics's indexes cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchXlmTransfIndexes( IndArray )
    !
  end subroutine ClassShortRangeChannelFetchXlmTransfIndexes


  !> Fetches the diffuse orbitals expansion in terms of the cartesian Gaussians.
  subroutine ClassShortRangeChannelFetchCartesianCoeff( SRC, Array )
    !
    class(ClassShortRangeChannel),   intent(inout) :: SRC
    real(kind(1d0)), allocatable,    intent(out)   :: Array(:,:)
    !
    if( .not. associated(SRC%TotalIrrep) ) then
       call Assert( "The diffuse orbitals expansion in terms of the cartesian Gaussians cannot be fetched &
         if the short range channel has not been initialize." )
    end if
    !
    call SRC%SROrb%FetchCartesianCoeff( Array )
    !
  end subroutine ClassShortRangeChannelFetchCartesianCoeff



  function ClassShortRangeChannelGetPILabel( SRC ) result( Label )
    !
    class(ClassShortRangeChannel), intent(inout) :: SRC
    character(len=:), allocatable :: Label
    allocate( Label, source = SRC%Parention%GetLabel() )
    !
  end function ClassShortRangeChannelGetPILabel


  function ClassShortRangeChannelGetPI( SRC ) result( PI )
    !
    class(ClassShortRangeChannel), target, intent(in) :: SRC
    class(ClassParentIon), pointer :: PI
    allocate( PI, source = SRC%Parention )
    !
  end function ClassShortRangeChannelGetPI


  integer function ClassShortRangeChannelGetPINelect( SRC ) result( PINE )
    !
    class(ClassShortRangeChannel), intent(in) :: SRC
    PINE = SRC%Parention%GetNelect()
    !
  end function ClassShortRangeChannelGetPINelect



  subroutine ClassShortRangeChannelSetInterfaceDir( SRCS, InterfaceDir )
    !
    class(ClassShortRangeChannel), intent(inout) :: SRCS
    character(len=*),              intent(in)    :: InterfaceDir
    !
    if ( allocated(SRCS%InterfaceDir) ) deallocate( SRCS%InterfaceDir )
    allocate( SRCS%InterfaceDir, source = InterfaceDir )
    !
  end subroutine ClassShortRangeChannelSetInterfaceDir



  subroutine ClassShortRangeChannelGetInterfaceDir( SRCS, InterfaceDir )
    !
    class(ClassShortRangeChannel), intent(inout) :: SRCS
    character(len=:), allocatable, intent(out)    :: InterfaceDir
    !
    allocate( InterfaceDir, source = SRCS%InterfaceDir )
    !
  end subroutine ClassShortRangeChannelGetInterfaceDir


  function ClassShortRangeChannelGetStorageDir( Self )result(Dir)
    class(ClassShortRangeChannel), intent(in) :: Self
    character(len=:), allocatable :: Dir
    if ( .not.allocated(Self%StorageDir) ) call Assert( &
         'The storage directory is not allocated in short channel, impossible to fetch.' )
    allocate( Dir, source = Self%StorageDir )
  end function ClassShortRangeChannelGetStorageDir

    


!!$  subroutine BuildPrecSRCSRCMat( &
!!$       LODiffMat, &
!!$       DiffLOMat, &
!!$       DiffDiffMat, &
!!$       InArray, &
!!$       OutArray ) 
!!$    !
!!$    class(ClassMatrix),           intent(inout) :: LODiffMat
!!$    class(ClassMatrix),           intent(inout) :: DiffLOMat
!!$    class(ClassMatrix),           intent(inout) :: DiffDiffMat
!!$    real(kind(1d0)),              intent(in)    :: InArray(:,:)
!!$    real(kind(1d0)), allocatable, intent(out)   :: OutArray(:,:)
!!$    !
!!$    real(kind(1d0)), allocatable :: AuxMat(:,:), LODiffArray(:,:), DiffLOArray(:,:), DiffDiffArray(:,:)
!!$    integer :: n, NumLO, NumPrecDiff, NumPrecSRC
!!$    !
!!$    NumLO = LODiffMat%NRows()
!!$    NumPrecDiff = LODiffMat%NColumns()
!!$    NumPrecSRC = NumLO + NumPrecDiff
!!$    !
!!$    allocate( AuxMat(NumPrecSRC,NumPrecSRC) )
!!$    AuxMat = 0.d0
!!$    !
!!$    AuxMat(1:NumLO,1:NumLO) = InArray(1:NumLO,1:NumLO)
!!$    !
!!$    call LODiffMat%FetchMatrix( LODiffArray )
!!$    call DiffLOMat%FetchMatrix( DiffLOArray )
!!$    call DiffDiffMat%FetchMatrix( DiffDiffArray )
!!$    !
!!$    AuxMat(1:NumLO,NumLO+1:)  = LODiffArray
!!$    AuxMat(NumLO+1:,1:NumLO)  = DiffLOArray
!!$    AuxMat(NumLO+1:,NumLO+1:) = DiffDiffArray
!!$    !
!!$    allocate( OutArray, source = AuxMat )
!!$    deallocate( AuxMat, LODiffArray, DiffLOArray, DiffDiffArray )
!!$    !
!!$  end subroutine BuildPrecSRCSRCMat


end module ModuleShortRangeChannel
