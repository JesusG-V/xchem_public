!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program test
  use wave_function_mod
  implicit none

  integer, parameter :: LMAX=5
  integer, parameter :: LCURR=3
  integer, parameter :: MCURR=2
  integer, parameter :: NMAX=100

  integer :: vdim(0:LMAX)
  integer :: l,m,mma,n
  type(wave_function) :: psi1,psi2
  real(kind(1d0)) :: dw1,dw2
  complex(kind(1d0)) :: zw1,zw2

  do l=0,LMAX
     vdim(l)=NMAX
  enddo
  
  call wf_init(psi1,LMAX,vdim,LCURR,MCURR)
  call wf_init(psi2,LMAX,vdim,LCURR,MCURR)

  do l=0,LCURR
     mma=min(MCURR,l)
     do m=-mma,mma
        do n=1,vdim(l)
           psi1%vec(l)%A(n,m)=zrandf()
           psi2%vec(l)%A(n,m)=zrandf()
        enddo
     enddo
  enddo

  dw1=wf_nrm2(psi1)
  dw2=wf_nrm2(psi2)
  zw1=wf_zdotc(psi1,psi2)
  
  write(*,*) "||psi1||=",dw1,dw1*dw1
  write(*,*) "||psi2||=",dw2,dw2*dw2
  write(*,*) "<psi1|psi2>=",zw1

  OPEN(10,FILE="wf",FORM="UNFORMATTED",STATUS="UNKNOWN")
  call wf_write(10,psi1)
  call wf_write(10,psi2)
  REWIND(10)
  call wf_read(10,psi2)
  call wf_read(10,psi1)
  
  dw1=wf_nrm2(psi1)
  dw2=wf_nrm2(psi2)
  zw1=wf_zdotc(psi1,psi2)
  
  write(*,*) "||psi2||=",dw1,dw1*dw1
  write(*,*) "||psi1||=",dw2,dw2*dw2
  write(*,*) "<psi1|psi2>=",zw1

  call wf_free(psi1)
  call wf_free(psi2)

  stop

contains

  complex(kind(1d0)) function zrandf() result(zres)
    real   (kind(1d0)), parameter :: PI=3.14159265358979323844d0
    complex(kind(1d0)), parameter :: Z1=(1.d0,0.d0)
    complex(kind(1d0)), parameter :: Zi=(0.d0,1.d0)
    real(kind(1d0)) :: r,th
    call random_number(r)
    call random_number(th)
    th=2.d0*PI*th
    zres=r*(Z1*cos(th)+Zi*sin(th))
    return
  end function zrandf

end program test
