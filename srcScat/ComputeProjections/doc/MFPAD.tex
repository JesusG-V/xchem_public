\documentclass[aps,pra,10pt,onecolumn,superscriptaddress,showpacs,preprintnumbers,amsmath,amssymb]{revtex4-1}

\usepackage{graphicx}
\usepackage{color}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{verbatim}
\usepackage{mathbbol}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{yfonts}
\usepackage{mathrsfs}

\begin{document}

\title{MFPAD in XCHEM}

\newcommand{\UCFPhys}{Department of Physics, University of Central Florida, Orlando, FL 32816.}
\newcommand{\UCFCREOL}{CREOL, University of Central Florida, Orlando, FL 32816.}

\author{Luca Argenti}\email{luca.argenti@ucf.edu}
\affiliation{\UCFPhys}
\affiliation{\UCFCREOL}

\begin{center}
\Large\bf
MFPAD in XCHEM
\end{center}

The plane-wave scattering state $|\Psi_{aE\hat{k}\sigma}^-\rangle$ for a molecular ion $a$ coupled to an electron with defined energy $E$, spin projection $\sigma$, and direction $\hat{k}$, fulfilling incoming boundary conditions, is asymptotically
\begin{equation}
|\Psi_{aE\hat{k}\sigma}^-\rangle = \sum_{\ell m} i^{\ell-1}Y_{\ell m}^*(\hat{k})e^{-i\sigma_{\ell}(k)}\sum_{S\Sigma} C_{S_a\Sigma_a,\frac{1}{2}\sigma}^{S\Sigma} \sqrt{N}\,\hat{\mathcal{A}}\Phi_{a}(x_{1}-x_{N-1},\zeta_N)Y_{\ell m}(\hat{r}_N)\mathcal{W}_{\ell,E-E_a}^{+}(r_N) + \mathrm{incoming},
\end{equation}
where $k=\sqrt{2(E-E_a)}$, $N$ is the total number of electrons, $\sigma_\ell(k)$ is the Coulomb phase, $C_{a\alpha,b\beta}^{c\gamma}$ is a Clebsch-Gordan coefficient, $\hat{\mathcal{A}}\equiv (N!)^{-1}\sum_{\mathcal{P}\in\mathcal{S}_{N}}(-1)^p\mathcal{P}$ is the idempotent antisymmetrizer, $\Phi_a(x_1-x_{N-1},\zeta_N)$ is the parent-ion function coupled to the spin function of the photoelectron to give a total spin and spin projection $S$ and $\Sigma$, respectively, $\zeta_N$ is the spin variable of the last electron, $x_i=(\vec{r}_i,\zeta_i)$ are the spatial and spin coordinates of the ion electrons, $Y_{\ell m}(\hat{\Omega})$ is a spherical harmonics, and $\mathcal{W}_{\ell,E}(r)$ is the outgoing component of a regular Coulomb function normalized to $\delta(E-E')$, and "incoming" refers to the residual purely incoming components in all the open channels. Atomic units are assumed unless otherwise stated.
The plane-scattering state $|\Psi_{aE\hat{k}\sigma}^-\rangle$ can be expressed in terms of the spherical scattering states $|\Psi_{a\ell m E}^-\rangle$, in which the outgoing photoelectron has a well defined orbital angular momentum, as 
\begin{equation}
|\Psi_{aE\hat{k}\sigma}^-\rangle = \sum_{\ell m} i^{\ell-1}Y_{\ell m}^*(\hat{k})e^{-i\sigma_{\ell}(k)}\sum_{S\Sigma} C_{S_a\Sigma_a,\frac{1}{2}\sigma}^{S\Sigma} |\Psi_{a\ell m E}^-\rangle.
\end{equation}

The XCHEM code computes the scattering states in which the outgoing channel has the angular distribution of symmetry-adapted spherical harmonics $X_{\ell m}$ instead of the ordinary spherical harmonics $Y_{\ell m}$,
\begin{eqnarray}
X_{\ell 0}&=&Y_{\ell 0}\\
X_{\ell m}&=&\frac{1}{\sqrt{2}}(Y_{\ell m}+(-1)^mY_{\ell -m}),\quad m>0\\
X_{\ell -m}&=&\frac{1}{\sqrt{2}\,i}(Y_{\ell m}-(-1)^mY_{\ell -m}),\quad m>0.
\end{eqnarray}
Conversely, we can express the $Y_{\ell m}$ in terms of the $X_{\ell m}$ as
\begin{equation}
Y_{\ell m} = \frac{a_{m}}{\sqrt{2}}(X_{\ell m } + i b_{m}X_{\ell -m}),
\end{equation}
where $a_m=1$ if $m> 0$, $a_0=\sqrt{2}$, and $a_m = -i(-1)^m$, if $m<0$, and $b_m=1-\delta_{m0}$.

The plane-wave scattering states, therefore, can be written in terms of the symmetry-adapted spherical waves $|\Psi_{a X\ell m E}^-\rangle$ as
\begin{equation}
|\Psi_{aE\hat{k}\sigma}^-\rangle = \sum_{\ell m} i^{\ell-1}Y_{\ell m}^*(\hat{k})e^{-i\sigma_{\ell}(k)}\sum_{S\Sigma} C_{S_a\Sigma_a,\frac{1}{2}\sigma}^{S\Sigma} \frac{a_{m}}{\sqrt{2}}(|\Psi_{aX\ell m E}^-\rangle + i b_{m}|\Psi_{aX\ell -m E}^-\rangle).
\end{equation}

Then the partial molecular-frame photoelectron angular distribution (MFPAD) $\frac{dP_a}{dEd\Omega}$ is given by
\begin{equation}
\frac{dP_a}{dEd\hat{k}}= \sum_{\sigma\Sigma_a} \left|\langle \Psi_{a E\hat{k}\sigma}|\Psi\rangle\right|^2.
\end{equation}
Let's define the partial photoelectron amplitude of a wavepacket $\Psi$ (e.g., from the solution of the TDSE, or from the action of the dipole operator on an initial bound state, for one-photon ionization) $A_{a\ell m E} = \langle \Psi_{a X \ell m E}^-|\Psi\rangle$. Then
\begin{equation}
\frac{dP_a}{dEd\hat{k}}= (2S_0+1)\sum_{j\mu} Y_{j\mu}(\hat{k}) B_{j\mu}
\end{equation}
where
\begin{equation}
B_{j\mu}=\sum_{\ell\ell'}\frac{\Pi_{j\ell'}}{\sqrt{4\pi}\Pi_{\ell}} i^{\ell'-\ell}e^{i(\sigma_\ell-\sigma_{\ell'})}C_{j0,\ell'0}^{\ell0} \sum_{mm'}C_{j\mu,\ell'm'}^{\ell m} \frac{a_m^*a_{m'}}{2}[A_{a\ell m E}-ib_mA_{a\ell -m E}][A^*_{a\ell' m' E}+ib_{m'}A^*_{a\ell' -m' E}]
\end{equation}
where $\Pi_{ab\cdots}=\sqrt{(2a+1)(2b+1)\cdots}$, and we made use of the well-known integral of the triple product of spherical harmonics.


\end{document}
