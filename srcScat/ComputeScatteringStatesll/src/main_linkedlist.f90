!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************


! {{{ Detailed description
!!  Author : Luca Argenti
!!           University of Central Florida, 2017
!!
!> \mainpage Program ComputeScatteringStatesll.
!! 
!! Synopsis:
!! ---------
!!
!! ___
!! Description (applicable to atoms only):
!! ------------
!!
!! Compute the scattering states using the following procedure. The scattering states
!! at a given energy E satisfy the secular differential equation in the whole box,
!! provided that the function space has the ability to describe finite values for the
!! wavefunction at the boundary.
!! \f[
!!     (E-H)|\Psi_E\rangle=0.
!! \f]
!! This can be ensured numerically if the set of functions used to represent the radial
!! part of $\Psi$ in each of its open channel components includes the last B-spline, B_b,
!! i.e., the one which does not vanish at the box boundary. B_b has an elementary
!! analytical expression:
!! \f[
!! B_b(r) = \left(\frac{r-r_{n-1}}{r_n-r_{n-1}}\right)^{k-1}.
!! \f]
!! The total wavefunction $|\Psi_E\rangle$ can be written, within the box, in terms of
!! the box eigenstates $|\boldsymbol{\psi}\rangle$ and of the last-Bspline channel
!! function, which we schematically indicate as $|\alpha B\rangle$, as
!! \f[
!!     |\psi_E\rangle = |\boldsymbol{\psi}\rangle \mathbf{c} + \sum_{\beta} |\beta B\rangle b_\beta
!! \f]
!! At each energy there are as many linearly independent solutions as there are open
!! channels: $|\psi_{\alpha E}\rangle, corresponding to the values at the boundary
!! $b_\beta^\alpha$. If the energy E is NOT already an eigenvalue of the hamiltonian
!! in the box, we can choose b_\beta^{\alpha}=\delta_{\alpha\beta} \forall \alpha,\beta.
!! \f[
!!      ( E - \mathbf{E} ) \mathbf{c}_{\alpha E} = \langle \boldsymbol{\psi}|(H-E)|\alpha B\rangle
!! \f]
!! and hence
!! \f[
!!     \mathbf{c}_{\alpha E} = ( E - \mathbf{E})^{-1} \langle\boldsymbol{\psi}|(H-E)|\alpha B\rangle,
!!      \quad \forall \alpha
!! \f]
!! If the energy E happens to be close to an eigenvalue E_i, then one can find
!! one solution by setting to 1 the value of (E-E_i)^-1*b_{\alpha_0} for the channel
!! $\alpha_0$ most represented in the |\psi_i\rangle box eigenstate.
!! \f[
!!     \mathbf{c}_{\alpha_0 E} = \frac{E-E_i}{E-\mathbf{E}} \langle\boldsymbol{\psi}|(H-E)|\alpha_0 B\rangle
!! \f]
!! The remaining n-1 solutions are obtained by setting the expansion coefficient c_i to zero
!! and setting to one b_\beta \forall \beta\neq \alpha.
!! \f[
!!     c_{j,\beta E} = \frac{1}{E-E_j} \langle\psi_j|(H-E)|\beta B\rangle, \forall j\neq i,\, \beta\neq \alpha_0
!! \f]
!! In fact, this second procedure can be generalized pretty much to all cases.
!! This procedure extends trivially to the case of degenerate box eigenstates, even if such
!! case is very unlikely to ever manifest itself.
!! To compute the scattering states, therefore, what is needed is the spectrum of
!! the hamiltonian in the box and, for each eigenvector, the value of the integrals
!! \f[
!!     \langle\psi_i|H|\alpha B\rangle, \quad  \langle\psi_i|\alpha B\rangle, \quad \forall i, \alpha.
!! \f]
!! Since the last Bspline is very localized at the end of the box, it is easy to determine the value
!! of these two integrals from the values of \psi_i near the boundary, which is in turn determined by
!! its composition in terms of the PWC states. These integrals can be computed once and forall in the
!! program ComputeSpectrum_H0_CCO, and the function can be tabulated in the last interval to conduct
!! the fitting afterwards.
!!
!! Once the scattering states are computed in this way, the projection on scattering states will be
!! smooth, and we avoid all the problems related to the fast oscillations of the amplitude on
!! the unperturbed continuum.
!!
!! The Psi- states at a given total energy E, which are eventually those committed to disk, are 
!! normalized in such a way to have the following asymptotic expression 
!! \f[
!!       Psi^-_{\alpha E} = W^+_{\alpha E}  + \sum_\beta  W^-_{\beta E} \mathbf{S}_{\beta\alpha}^*(E)
!! \f]
!! where $\mathbf{S}(E)$ is the \emph{on-shell} scattering matrix, 
!! \f[
!!       W^\pm_{\alpha E} = [ 2 \pi k_\alpha ]^{-1/2} e^{ \pm i \theta_\alpha(r) },\\
!!       k_\alpha = k_\alpha(E) \equiv \sqrt{ 2 (E-E_{ion,\alpha}) }$,
!! \f]
!! $\theta_\alpha$ is the phase of a Coulomb wave, i.e., approximately,
!! \f[
!!       \theta_\alpha \approx k_\alpha r + \frac{Z}{k_\alpha} \ln( 2 k_\alpha r ) - \ell_\alpha \pi /2 + \sigma_{\ell_\alpha)(k_\alpha)
!! \f]
!! and $\simga_\ell(k)$ is the Coulomb phase proper.
!!
!! Notice that for time-symmetric systems, such as those we are considering here, the scattering 
!! matrix is not just unitary, but it is also symmetric (in the same way that the reaction matrix
!! is not just Hermitean, it is real and symmetric). This means that $\mathbf{S}^{-1}=\mathbf{S}^\dagger=\mathbf{S}^*$.
!! 
!! In this case, furthermore, the scattering states with incoming boundary conditions, Psi+, are just 
!! the complex conjugates of the Psi- states:
!! \f[
!!       Psi^+_{\alpha E} = W^-_{\alpha E} + \sum_\beta W^+_{\beta E} \mathbf{S}_{\beta\alpha}(E)
!! \f]
!! 
!! With the normalization indicated above, the scattering states are normalized as
!! \f[
!!       \langle \boldsymbol{\Psi}^-_E | \boldsymbol{\Psi}^-_{E'} \rangle = \mathbf{1}    \delta(E-E')
!!       \langle \boldsymbol{\Psi}^-_E | \boldsymbol{\Psi}^+_{E'} \rangle = \mathbf{S}(E) \delta(E-E')
!! \f]
!! etc.
!!
! }}}
program ComputeScatteringStatesll

  use, intrinsic :: ISO_FORTRAN_ENV
  use, intrinsic :: ISO_C_BINDING

  use ModuleErrorHandling
  use ModuleSystemUtils
  use ModuleString
  use ModuleIO

  use ModuleElectronicSpace
  use ModuleHDF5
  use ModuleElectronicSpaceOperators
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleParentIons

  use ModuleConstants
  use ModuleDiagonalize
  use ModuleScatteringStates

  implicit none

  real(kind(1d0)) , parameter :: MINIMUM_PHOTOELECTRON_ENERGY =  1.d-4
  real(kind(1d0)) , parameter :: ENERGY_EPSILON               =  1.d-8
  integer         , parameter :: NUMBER_OF_ENTRIES_PER_LINE   = 10
  enum, bind(c)
     enumerator :: MODE_PRINT_INFO
     enumerator :: MODE_UNIFORM
     enumerator :: MODE_RYDBERG
     enumerator :: MODE_RESOLVE
     enumerator :: MODE_REFINE 
  end enum

  !.. Run-time parameters
  !..
  character(len=:), allocatable :: ProgramInputFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: CloseCouplingConfigFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: SymLabel
  character(len=:), allocatable :: ccset
  integer                       :: Mode
  integer                       :: nEnergies
  real(kind(1d0))               :: Emin
  real(kind(1d0))               :: Emax
  real(kind(1d0))               :: dEmax
  integer                       :: nThr_min
  integer                       :: nThr_max
  real(kind(1d0))               :: pqn_min 
  real(kind(1d0))               :: pqn_max
  real(kind(1d0))               :: dpqn_max
  real(kind(1d0))               :: dPhi_max
  logical                       :: Overwrite
  logical                       :: Verbous


  character(len=:), allocatable :: StorageDir
  character(len=:), allocatable :: scatLabel
  type(ClassElectronicSpace)    :: Space
  character(len=:), allocatable :: SymStorageDir
  type(ClassSymmetricElectronicSpace), pointer :: SymSpace

  !> Total number of partial wave channel. Coincides with the total
  !! number of last B-splines, which will be located contiguously at
  !! the end of the operators matrices.
  integer :: nChannels

  integer :: i, ich, ipwc, lw, iBox
  integer :: uid, iBuf
  character(len=:), allocatable :: FileName, ccset_dir, ccscat_dir
  integer                       :: nBoxStates
  real(kind(1d0))               :: IonCharge
  real(kind(1d0)) , allocatable :: BoxEnergies(:)

  real   (kind(1d0)), allocatable :: SBoundaryIntegral_PWC_BOX(:,:)
  real   (kind(1d0)), allocatable :: HBoundaryIntegral_PWC_BOX(:,:)
  real   (kind(1d0)), allocatable :: BoundaryDeriv(:,:)
  real   (kind(1d0)), allocatable :: TableDer(:,:)
  real   (kind(1d0)), allocatable :: TableVal(:,:)
  real   (kind(1d0)), allocatable :: PWCThr(:)
  integer           , allocatable :: PWClan(:)
  real   (kind(1d0))              :: LastBsVal, LastBsDer

  !> Linked-list with the scattering states at several energies
  type(ClassScatteringStateList)          :: ScatStateList
  type(ClassScatteringState)    , pointer :: ScatState

  integer                      :: iEn
  character(len=:),allocatable :: ChanName
  real(kind(1d0))              :: Energy, Rmax
  real(kind(1d0))              :: IonEnergy, ElectronEnergy
  real(kind(1d0)), allocatable :: EnergyGrid(:)
  real(kind(1d0)), allocatable :: ThresholdList(:)
  integer        , allocatable :: nThresholdChannels(:), ThresholdChannelList(:,:)
  integer                      :: nThresholds,  iThr
  integer                      :: nOpen, iOpen, iOpenPwc
  integer        , allocatable :: listOpen(:)
  real(kind(1d0)), allocatable :: ScatStatesBox(:)
  real(kind(1d0)), allocatable :: AMat(:,:), BMat(:,:)
  real(kind(1d0))              :: NormFactor, PhaseShift

  call GetRunTimeParameters(    &
       ProgramInputFile       , &
       NameDirQCResults       , & 
       CloseCouplingConfigFile, & 
       Multiplicity           , &
       SymLabel               , & 
       mode        , &
       nEnergies   , &
       Emin        , &
       Emax        , &
       dEmax       , &
       nThr_min    , &
       nThr_max    , &
       pqn_min     , &
       pqn_max     , &
       dpqn_max    , &
       dPhi_max    , &
       Overwrite   , &
       scatLabel   , &
       Verbous     )


  call ParseProgramInputFile( ProgramInputFile, StorageDir )
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir


  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )
  
  call GetSymmetricElectronicSpace( Space, SymLabel, SymSpace )

  allocate( SymStorageDir, source = GetBlockStorageDir( SymSpace, SymSpace ) )

  nChannels = SymSpace%GetNumPWC()
  IonCharge = SymSpace%GetPICharge()
  
  !.. Load the BOX energies, eigenvectors, and boundary conditions
  !..
  !.. 1. Load Box H Eigenvalues
  allocate(FileName,source=SymStorageDir//"/BoxStates/H_Eigenvalues")
  open(newunit = uid, &
       file    = FileName, &
       form    ="formatted", &
       status  ="old")
  deallocate(FileName)
  read(uid,*) i, nBoxStates
  write(*,"(a,i0)") "   Number of Box eigenstates:",nBoxStates
  allocate(BoxEnergies(nBoxStates))
  do i=1,nBoxStates
     read(uid,*) iBuf, BoxEnergies(i)
  enddo
  close(uid)
  !
  !.. 2. Load Box Eigenvectors boundary properties
  allocate(FileName,source=SymStorageDir//"/BoxStates/H_EigenVectors_LastBs")
  open(newunit = uid       , &
       file    = FileName  , &
       form    ="formatted", &
       status  ="old"      )
  deallocate(FileName)
  read(uid,*)
  allocate(HBoundaryIntegral_PWC_BOX(nChannels,nBoxStates))
  do i=1,nBoxStates
    read(uid,*) (HBoundaryIntegral_PWC_BOX(ich,i),ich=1,nChannels)
  enddo
  close(uid)
  !
  allocate(FileName,source=SymStorageDir//"/BoxStates/S_EigenVectors_LastBs")
  open(newunit = uid       , &
       file    = FileName  , &
       form    ="formatted", &
       status  ="old"      )
  deallocate(FileName)
  read(uid,*)
  allocate(SBoundaryIntegral_PWC_BOX(nChannels,nBoxStates))
  do i=1,nBoxStates
     read(uid,*) (SBoundaryIntegral_PWC_BOX(ich,i),ich=1,nChannels)
  enddo
  close(uid)
  !
  allocate(FileName,source=SymStorageDir//"/BoxStates/Box_EigenStates_DerRMax")
  open(newunit = uid       , &
       file    = FileName  , &
       form    ="formatted", &
       status  ="old"      )
  deallocate(FileName)
  read(uid,*)
  allocate(BoundaryDeriv(nChannels,nBoxStates))
  do i=1,nBoxStates
     read(uid,*) (BoundaryDeriv(ich,i),ich=1,nChannels)
  enddo
  close(uid)
  !
  allocate(FileName,source=SymStorageDir//"/BoxStates/S_LastBsDer")
  open(newunit = uid       , &
       file    = FileName  , &
       form    ="formatted", &
       status  ="old"      )
  deallocate(FileName)
  read(uid,*) LastBsDer, Rmax
  close(uid)
  !
  allocate(FileName,source=SymStorageDir//"/BoxStates/S_LastBsVal")
  open(newunit = uid       , &
       file    = FileName  , &
       form    ="formatted", &
       status  ="old"      )
  deallocate(FileName)
  read(uid,*) LastBsVal, Rmax
  close(uid)

  
  allocate(ccscat_dir, source = SymStorageDir//"/ScatteringStates/SPEC/")
  call ScatStateList.Init(ccscat_dir)
  
  !.. Reconstruct the list of thresholds and print it on screen
  call SymSpace.GetThresholdSequence( &
       nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList )
  write(*,"(a)") "List of thresholds, and corresponding channel"
  write(*,"(a)") "  Thr      Etn         Etn-Et1    2(Etn-Et1)   Etn-E1(eV)    Nch | cco-list"
  do iThr=1,nThresholds
     write(*,"(xi3,4(x,f12.6),2x,i6,a,*(x,i0))") iThr, ThresholdList(iThr),&
          ThresholdList(iThr)-ThresholdList(1), 2*(ThresholdList(iThr)-ThresholdList(1)), &
          (ThresholdList(iThr)-ThresholdList(1))*Energy_au_to_eV, &
          nThresholdChannels(iThr),&
          " |", ( ThresholdChannelList(ich,iThr), ich = 1, nThresholdChannels(iThr) )
  enddo

  !.. If only basic info are requested, stops here
  !..
  if( Mode == MODE_PRINT_INFO )stop

  !.. Finalize the parameters needed to determine the calculation
  !
  !.. Energy must be above the ionization threshold in any case
  Emin = max( Emin, ThresholdList( 1 ) + MINIMUM_PHOTOELECTRON_ENERGY ) 
  if( nThr_min > 0 )then
     if( nThr_min > nThresholds )then
        write(ERROR_UNIT,*) " starting threshold ",nThr_min," exceeds tot. n. thresholds, ",nThresholds
        stop
     endif
     Emin = max( Emin, ThresholdList( nThr_min ) + MINIMUM_PHOTOELECTRON_ENERGY )
  endif

  if( nThr_max > 0 )then
     if( nThr_max > nThresholds )then
        write(ERROR_UNIT,*) " ending threshold ",nThr_max," exceeds tot. n. thresholds, ",nThresholds
        write(ERROR_UNIT,*) " ending threshold reset to ",nThresholds
        nThr_max = nThresholds
        if( nThr_min >= nThr_max )stop
     endif
     if( pqn_max > 0.d0 )then
        Emax = min( Emax, ThresholdList( nThr_max ) - 0.5d0 * ( IonCharge / pqn_max )**2 )
     else
        Emax = min( Emax, ThresholdList( nThr_max ) - ENERGY_EPSILON )
     endif
     if( pqn_min > 0.d0 )then
        Emin = max( Emin, ThresholdList( nThr_max ) - 0.5d0 * ( IonCharge / pqn_min )**2 )
     endif
  else
     if(Mode==MODE_RYDBERG)then
        write(ERROR_UNIT,*) " In Rydberg mode you must specify the last threshold as Rydberg limit"
        stop
     endif
  endif
  if( Emax < Emin )then
     write(ERROR_UNIT,*) " min energy is smaller than max energy: nothing to compute."
     stop
  endif

  if( dPhi_max <= 0.d0 .and. Mode == MODE_REFINE )then
     write(ERROR_UNIT,*) " In Refine mode you must specify the maximum variation of the total phaseshift"
     stop
  endif

  if( Mode == MODE_RYDBERG )then
     if( IonCharge <=0.d0 )then
        write(ERROR_UNIT,*) " No positively charged parent, no Rydberg series."
        stop
     endif
     if( dpqn_max <= 0.d0 )then
        write(ERROR_UNIT,*) " In Rydberg mode you must specify the maximum variation of the principal quantum number"
        stop
     endif
  endif

  !.. Load pre-existing scattering states, unless there is the
  !   explicit request to overwrite them
  !..
  if( .not. Overwrite ) call ScatStateList.Load( scatLabel )

  call realloc( ScatStatesBox, nBoxStates )

  SweepCycle: do

     select case( Mode )
     case( MODE_UNIFORM )
        if( dEmax < 0     ) dEmax = Emax - Emin
        if( nEnergies > 1 )then
           dEmax = min( dEmax, ( Emax - Emin ) / dble( nEnergies - 1 ) )
        endif
        call ScatStateList.GetBestUniformFilling( Emin, Emax, dEmax, nEnergies, EnergyGrid )
     case( MODE_RYDBERG )
        call ScatStateList.GetBestRydbergFilling( Emin, Emax, dpqn_max, &
             IonCharge, ThresholdList( nThr_max ), nEnergies, EnergyGrid )
     case( MODE_RESOLVE )
        call ScatStateList.GetResolveFilling( Emin, Emax, nEnergies, EnergyGrid )
     case( MODE_REFINE  )
        call ScatStateList.GetRefineFilling( Emin, Emax, dPhi_max, nEnergies, EnergyGrid )
     end select

     if( nEnergies == 0 )exit SweepCycle

     write(*,"(a,i0,a )") "Computing ", nEnergies," points"
     do iEn=1,nEnergies
        write(*,"(x,f11.6)",advance="no") EnergyGrid(iEn)
        if(iEn<nEnergies.and.(mod(iEn,NUMBER_OF_ENTRIES_PER_LINE)==0))write(*,*)
     enddo
     write(*,*)
     

     !.. Print the channels in order of energies and following the 
     !   intra-threshold order (based on the orbital angular momentum)
     !   defined in ModuleSymmetricElectronicSpaces
     !..
     Energy = EnergyGrid( nEnergies )
     call GetOpenChannels(       &
          nThresholds          , &
          ThresholdList        , &
          nThresholdChannels   , &
          ThresholdChannelList , &
          Energy, nOpen, listOpen )
     write(*,"(a)")    "#Chan  Name      Threshold         #CC Index  "
     call realloc( PWCThr, nOpen )
     call realloc( PWClan, nOpen )
     do iOpen = 1, nOpen
        ich       = listOpen( iOpen )
        ChanName  = SymSpace.GetPWCLabel       ( ich )
        IonEnergy = SymSpace.GetChannelPIEnergy( ich )
        PWCThr( iOpen ) = IonEnergy
        PWClan( iOpen ) = SymSpace%GetChannelL ( ich )
        write(*,"(i5,2x,a,2x,f17.9,2x,i5)") iOpen, ChanName, IonEnergy, ich
     enddo
     do iEn = 1, nEnergies

        Energy = EnergyGrid(iEn)
        call GetOpenChannels(       &
             nThresholds          , &
             ThresholdList        , &
             nThresholdChannels   , &
             ThresholdChannelList , &
             Energy, nOpen, listOpen )

        call realloc( AMat    , nOpen, nOpen )
        call realloc( BMat    , nOpen, nOpen )
        call realloc( TableDer, nOpen, nOpen )
        call realloc( TableVal, nOpen, nOpen )
        AMat       = 0.d0
        BMat       = 0.d0
        TableDer   = 0.d0
        TableVal   = 0.d0

        allocate( ScatState )
        call ScatState.Init( Energy, nOpen, nBoxStates )

        !.. Compute a complete set of linearly independent scattering states
        !   at the energy E
        !..
        do iOpen = 1, nOpen

           ich = listOpen( iOpen )
           IonEnergy = SymSpace%GetChannelPIEnergy(ich)
           lw        = SymSpace%GetChannelL       (ich)
           ChanName  = SymSpace%GetPWCLabel       (ich)
           call ScatState.Setlw(iOpen,lw)
           call ScatState.SetThr(iOpen,IonEnergy)
           call ScatState.SetName(iOpen,ChanName)

           do iBox = 1, nBoxStates
              !
              !.. Evaluates the coefficient of the scattering state on the box eigenstates
              ScatStatesBox( iBox ) = 1.d0 / ( Energy - BoxEnergies(iBox) ) * &
                   (        HBoundaryIntegral_PWC_BOX( ich, iBox ) - &
                   Energy * SBoundaryIntegral_PWC_BOX( ich, iBox ) ) 
              !
              !.. Computes the scattering state derivative and value at the border
              do iOpenPWC = 1, nOpen
                 iPWC = listOpen( iOpenPWC )
                 TableDer(iOpenPWC,iOpen) = TableDer(iOpenPWC,iOpen) + &
                      BoundaryDeriv(iPWC,iBox) * ScatStatesBox( iBox ) 
              enddo
              !
           enddo
           TableDer(iOpen,iOpen) = TableDer(iOpen,iOpen) + LastBsDer
           TableVal(iOpen,iOpen) = LastBsVal

           call ScatState.SetCoef(iOpen,ScatStatesBox)

        enddo

        !.. Computes the expansion coefficients of the regular and irregular
        !   Coulomb components of the scattering states in each channel
        !..
        call ComputeRegIrregCoulombCoef( &
             IonCharge, &
             Energy   , &
             Rmax     , &
             nOpen    , &
             PWCThr   , &
             PWClan   , &
             TableVal , &
             TableDer , &
             AMat     , &
             BMat     )
        !.. 
        do iOpen = 1, nOpen
           call ScatState.SetAsymp( iOpen, AMat(:,iOpen), BMat(:,iOpen) )
        enddo

        !.. Compute on-shell matrices

        call ScatStateList.Add(ScatState)
        call ScatState.Solve()

     enddo
     
     call ScatStateList.EnforceContinuity()

     select case( Mode )
     case( MODE_UNIFORM )
        exit SweepCycle
     case( MODE_RYDBERG )
        exit SweepCycle
     end select

  enddo SweepCycle

  call ScatStateList.Save( scatLabel )

  stop

contains

  function UniformGrid(xmi,xma,n) result(grid)
    real(kind(1d0)), intent(in) :: xmi, xma
    integer        , intent(in) :: n
    real(kind(1d0)), allocatable :: grid(:)
    integer :: i
    allocate(grid,source=[(xmi+(xma-xmi)/dble(n-1)*dble(i-1),i=1,n)])
  end function UniformGrid


  !> Reads the run time parameters specified in the command line.
  !! If an optional inherently positive parameters is not specified
  !! on the command line, it is initialized to a negative number
  !! to signal that it should not be used to evaluate the energy grid.
  !<
  subroutine GetRunTimeParameters( &
       ProgramInputFile, &
       NameDirQCResults, &
       ConfigFile, &
       Multiplicity, &
       SymLabel    , &
       mode    , &
       nen     , &
       emin    , &
       emax    , &
       dEmax   , &
       nThr_min, &
       nThr_max, &
       pqn_min , &
       pqn_max , &
       dpqn_max, &
       dPhi_max, &
       Overwrite,&
       ScatLabel,&
       Verbous)
    !
    use ModuleErrorHandling
    use ModuleCommandLineParameterList
    use ModuleString
    !
    implicit none
    !
    real(kind(1d0)) , parameter :: LOWEST_ENERGY  =-1.d16
    real(kind(1d0)) , parameter :: LARGEST_ENERGY = 1.d16
    !
    character(len=:), allocatable, intent(out) :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: NameDirQCResults
    character(len=:), allocatable, intent(out) :: ConfigFile
    integer                      , intent(out) :: Multiplicity
    character(len=:), allocatable, intent(out) :: SymLabel
    character(len=:), allocatable, intent(out) :: scatLabel
    integer         ,              intent(out) :: Mode
    integer         ,              intent(out) :: nen
    real(kind(1d0)) ,              intent(out) :: emin
    real(kind(1d0)) ,              intent(out) :: emax
    real(kind(1d0)) ,              intent(out) :: dEmax
    integer         ,              intent(out) :: nThr_min
    integer         ,              intent(out) :: nThr_max
    real(kind(1d0)) ,              intent(out) :: pqn_min ! Principal Quantum Number
    real(kind(1d0)) ,              intent(out) :: pqn_max
    real(kind(1d0)) ,              intent(out) :: dpqn_max
    real(kind(1d0)) ,              intent(out) :: dPhi_max
    logical         ,              intent(out) :: Overwrite
    logical         ,              intent(out) :: Verbous
    !
    type( ClassCommandLineParameterList ) :: List
    !
    character(len=*), parameter :: PROGRAM_DESCRIPTION=&
         "Compute multichannel scattering states"
    character(len=512) :: strnBuf, pif
    !
    call List.SetDescription(PROGRAM_DESCRIPTION)
    call List.Add( "--help" , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List.Add( "-label" , "subdir label" , ""         , "optional" )
    call List.Add( "-ccfile"  , "Config File"  , "CCFile"       , "optional" )
    call List%Add( "-mult"  , "Total Multiplicity", 1, "required" )
    call List.Add( "-sym"   , "SymLabel"         , "1Se"          , "required" )
    call List.Add( "-mode"  , "info|uniform|rydberg|resolve|refine", "info", "optional" )
    call List.Add( "-ne"    , "n energy pts" ,   0            , "optional" )
    call List.Add( "-emin"  , "min energy"   ,   0.d0         , "optional" )
    call List.Add( "-emax"  , "max energy"   ,   0.d0         , "optional" )
    call List.Add( "-dEmax" , "max Delta E"  ,  10.d0         , "optional" )
    call List.Add( "-scatLabel" , "label of the set of scattering states"  ,  "1", "optional" )
    call List.Add( "-thrmin", "min threshold",   1            , "optional" )
    call List.Add( "-thrmax", "max threshold", 10000          , "optional" )
    call List.Add( "-pqnmin", "min Pr. Q. N.",   1.d-6        , "optional" )
    call List.Add( "-pqnmax", "max Pr. Q. N.",  20.d0         , "optional" )
    call List.Add("-dpqnmax", "max Delta pqn",   1.d0         , "optional" )
    call List.Add( "-dPhmax", "max Delta Phi",   3.15d0       , "optional" )
    call List.Add( "--overwrite", "Recompute from scratch" )
    call List.Add( "-verbous", "Print more info" )
    !
    call List.Parse()
    !
    if(List.Present("--help"))then
       call List.PrintUsage()
       stop
    end if
    call List%Get( "-pif"    , pif  )
    allocate( ProgramInputFile       , source = trim( pif )    )
    call List.Get( "-label",  strnBuf  )
    allocate(NameDirQCResults,source=trim(adjustl(strnBuf)))
    call List.Get( "-scatLabel",  strnBuf  )
    allocate(scatLabel,source=trim(adjustl(strnBuf)))
    call List.Get( "-ccfile",  strnBuf  )
    allocate(ConfigFile,source=trim(adjustl(strnBuf)))
    call List%Get( "-mult"   , Multiplicity )
    call List.Get( "-sym"  ,  strnBuf  )
    allocate( SymLabel, source=trim(adjustl(strnBuf)))
    !
    call List.Get( "-mode",  strnBuf  )
    call SetStringToUppercase( strnBuf )
    select case( strnBuf )
    case( "INFO" )
       Mode = MODE_PRINT_INFO
    case( "UNIFORM" )
       Mode = MODE_UNIFORM
       if(  .not. List.Present( "-ne"   ) .and. &
            .not. List.Present( "-dEmax" ) )then
          write(ERROR_UNIT,*) "In mode uniform, either ne or dEmax (or both) must be specified"
          call List.PrintUsage()
          stop
       endif
    case( "RYDBERG" )
       Mode = MODE_RYDBERG
    case( "RESOLVE" )
       Mode = MODE_RESOLVE
    case( "REFINE"  )
       Mode = MODE_REFINE
    case DEFAULT
       write(ERROR_UNIT,"(a)") "Unrecognized mode "//strnBuf
       call List.PrintUsage()
       stop
    end select

    if ( Mode /= MODE_PRINT_INFO )then !.. check if all the necessary parameters have been specified

       if(List.Present("-ne"))then
          call List.Get( "-ne" , nen  )
          if(nen<1)then
             write(ERROR_UNIT,"(a)") "ne must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          nen=-1
       endif

       if(List.Present("-emin"))then
          call List.Get( "-emin", emin  )
       else
          emin = LOWEST_ENERGY
       endif
       if(List.Present("-emax"))then
          call List.Get( "-emax", emax  )
       else
          emax = LARGEST_ENERGY
       endif

       if(List.Present("-dEmax"))then
          call List.Get("-dEmax"  ,dEmax   )
          if(dEmax<=0.d0)then
             write(ERROR_UNIT,"(a)") "dEmax must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          dEmax=-1.d0
       endif

       if(List.Present("-thrmin"))then
          call List.Get("-thrmin",nThr_min)
          if(nThr_min<1)then
             write(ERROR_UNIT,"(a)") "thrmin must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          nThr_min=-1
       endif

       if(List.Present("-thrmax"))then
          call List.Get("-thrmax",nThr_max)
          if(nThr_max<1)then
             write(ERROR_UNIT,"(a)") "thrmax must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          nThr_max=-1
          if(.not.List.Present("-emax"))then
             write(ERROR_UNIT,"(a)") "either emax or thrmax (or both) must be specified"
             call List.PrintUsage()
             stop
          endif
       endif

       if(List.present("-pqnmin"))then
          call List.Get("-pqnmin" ,pqn_min )
          if(pqn_min<=0.d0)then
             write(ERROR_UNIT,"(a)") "pqnmin must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          pqn_min=-1.d0
       endif

       if(List.present("-pqnmax"))then
          call List.Get("-pqnmax" ,pqn_max )
          if(pqn_max<=0.d0)then
             write(ERROR_UNIT,"(a)") "pqnmax must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          pqn_max=-1.d0
       endif

       if(List.present("-dpqnmax"))then
          call List.Get("-dpqnmax" ,dpqn_max )
          if(dpqn_max<=0.d0)then
             write(ERROR_UNIT,"(a)") "dpqnmax must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          dpqn_max=-1.d0
       endif

       if(List.present("-dPhmax"))then
          call List.Get("-dPhmax" ,dPhi_max )
          if(dPhi_max<=0.d0)then
             write(ERROR_UNIT,"(a)") "dPhmax must be positive"
             call List.PrintUsage()
             stop
          endif
       else
          dPhi_max=-1.d0
       endif

    end if

    Overwrite = List.Present("--overwrite")
    Verbous   = List.Present("-verbous")

    call List.Free()

    !.. Must add check on the input
    !
    write(OUTPUT_UNIT,"(a)" ) "Run time parameters :"
    write(OUTPUT_UNIT,"(a)" ) "Subdir label    : "//NameDirQCResults
    write(OUTPUT_UNIT,"(a)" ) "CC Config File  : "//ConfigFile
    write(OUTPUT_UNIT,"(a)" ) "Spectral SymLabel   : "//SymLabel
    !
  end subroutine GetRunTimeParameters


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    !
    character(len=100) :: StorageDirectory
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"           , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    !
  end subroutine ParseProgramInputFile


   subroutine getopenchannels( nThresholds, ThresholdList, nThresholdChannels, ThresholdChannelList , E, nOpen, listOpen)
     
     integer             , intent(in) :: nThresholds
     real(kind(1d0))     , intent(in) :: ThresholdList(:)
     integer             , intent(in) :: nThresholdChannels(:)
     integer             , intent(in) :: ThresholdChannelList(:,:)
     real(kind(1d0))     , intent(in) :: E
     integer             , intent(out):: nOpen
     integer, allocatable, intent(inout):: listOpen(:)

     integer :: ithr, ich, iOpen
     
     nOpen=0
     do ithr = 1, nThresholds
        if( ThresholdList(ithr) <= E ) nOpen = nOpen + nThresholdChannels(ithr)
     enddo
     call realloc(listOpen,nOpen)

     iOpen=0
     do ithr = 1, nThresholds
        if( ThresholdList(ithr) > E )exit
        do ich = 1, nThresholdChannels(ithr)
           iOpen=iOpen+1
           listOpen(iOpen) = ThresholdChannelList( ich, ithr )
        enddo
     enddo
     
   end subroutine getopenchannels



end program ComputeScatteringStatesll


