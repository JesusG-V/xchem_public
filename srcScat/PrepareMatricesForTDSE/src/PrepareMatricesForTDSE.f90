!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!>  Insert Doxygen comments here
!!  
!!  This program XXX ...
!!
program PrepareMatricesForTDSE 

  use, intrinsic :: ISO_FORTRAN_ENV

  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleHDF5
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleSymmetricElectronicSpaceSpectralMethods
  use ModuleGeneralInputFile
  use ModuleAbsorptionPotential

  implicit none


  logical         , parameter :: BOX_ONLY = .TRUE.
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.
  logical         , parameter :: REMOVE_BIG_EIGENVALUES = .FALSE.


  !.. Run time parameters
  character(len=:), allocatable :: GeneralInputFile
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  logical                       :: OnlyQC
  logical                       :: OnlyLoc
  logical                       :: OnlyPoly
  logical                       :: OnlyDiagQH
  logical                       :: DecoupleChannels
  logical                       :: AnalyzeEvcts

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod


  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: OverlapMat, HamiltonianMat, CAP,DipoleMat
  type(ClassSESSESBlock)        :: QCOverlapMat
  type(ClassMatrix)             :: OvMat, HamMat, CAPMat, DipMat
  type(ClassSpectralResolution) :: OvSpecRes, HamSpecRes, CAPSpecRes
  type(ClassComplexSpectralResolution) :: QHSpecRes
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm
  type(ClassAbsorptionPotential):: AbsPot

  character(len=:), allocatable :: Axis,DipoleOpLabel
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: ConditionLabel
  integer :: i,j
  integer, allocatable :: xyz(:)
  real(kind(1d0)) :: MaxAllowedEigVal
  logical :: MatIsSymmetric,DipVel,DipLen,x,y,z

  !.. Read run-time parameters, among which there is the group and the symmetry

  call GetRunTimeParameters(      &
       GeneralInputFile       ,   &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       BraSymLabel            ,   &
       KetSymLabel            ,   &
       UseConditionedBlocks   ,   &
       UsePerfectProjection   ,   &
       OnlyQC                 ,   &
       OnlyLoc                ,   &
       OnlyPoly               ,   &
       OnlyDiagQH             ,   &
       DecoupleChannels       ,   &
       DipVel                 ,   &
       DipLen                 ,   &
       x                      ,   &
       y                      ,   &
       z                      ,   &
       AnalyzeEvcts)


  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "SAE General Input File  : "//GeneralInputFile
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File : ", ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ", CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 
  write(OUTPUT_UNIT,"(a,L)" )"Only QC, polycentric and monocentric Gaussian :     ", OnlyQC 
  write(OUTPUT_UNIT,"(a,L)" )"Only localized :     ", OnlyLoc 
  write(OUTPUT_UNIT,"(a,L)" )"Only polycentric Gaussian :     ", OnlyPoly
  write(OUTPUT_UNIT,"(a,L)" )"Only digonalize the Quenched Hamiltonian :", OnlyDiagQH



  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod




  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  !..The same symmetries are allowed for the overlap and the 
  !  Hamiltonian, so this subroutine can be called only once.
  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, OverlapLabel, KetSymLabel, &
       Axis                                  , &
       BraSymSpace, KetSymSpace )

  if ( SET_FORMATTED_WRITE) call OverlapMat%SetFormattedWrite()
  if ( BOX_ONLY .and. &
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly) call OverlapMat%SetBoxOnly()
  !

  write(output_unit,"(a)") 'Loading overlap ...'
  !
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly) then
     if ( UseConditionedBlocks ) then
        Group  => Space%GetGroup()
        irrepv => Group%GetIrrepList()
        Xlm = GetOperatorXlm( OverlapLabel, Axis )
        !.. Auxiliar conditioner only with the purpose to get the label.
        call Conditioner%Init(     &
             ConditionBsMethod   , &
             ConditionBsThreshold, &
             NumBsDropBeginning  , &
             NumBsDropEnding     , &
             irrepv(1), Xlm      , &
             UsePerfectProjection )
        allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, Axis, ConditionLabel , LoadLocStates=LoadLocStates )
     else
        call OverlapMat%Load( BraSymSpace, KetSymSpace, OverlapLabel, Axis, LoadLocStates = LoadLocStates )
     end if
     call OverlapMat%Assemble( LoadLocStates, OvMat )
  elseif( OnlyQC ) then
     call OverlapMat%LoadQC( BraSymSpace, KetSymSpace, OverlapLabel, Axis )
     call OverlapMat%AssembleQC( LoadLocStates, OvMat )
  elseif( OnlyLoc ) then
     call OverlapMat%LoadLoc( BraSymSpace, KetSymSpace, OverlapLabel, Axis )
     call OverlapMat%AssembleLoc( LoadLocStates, OvMat )
  elseif( OnlyPoly ) then
     call OverlapMat%LoadPoly( BraSymSpace, KetSymSpace, OverlapLabel, Axis )
     call OverlapMat%AssemblePoly( LoadLocStates, OvMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          OverlapLabel//' operator matrix.' )
  end if
  !
  allocate( SymStorageDir, source = OverlapMat%GetStorageDir() )
  call OvMat%writeColumnLabels( SymStorageDir//'S_basis' )
  !
  call OverlapMat%Free()
  MatIsSymmetric = OvMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'S is symmetric: ', MatIsSymmetric

  call OvMat%Write( trim(SymStorageDir)//'S_basis' )
  call OvMat%Free

  if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
  if ( BOX_ONLY .and.&
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly ) call HamiltonianMat%SetBoxOnly()
  !
  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  if ( DecoupleChannels ) then
    write(6,'(A)') "XCHEMQC decouple channels"
    call HamiltonianMat%SetDecoupleChannels( ) 
  endif 
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly ) then
     if ( UseConditionedBlocks ) then
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, Axis, ConditionLabel,&
          LoadLocStates=LoadLocStates )
     else
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, Axis, LoadLocStates=LoadLocStates )
     end if
     call HamiltonianMat%Assemble( LoadLocStates, HamMat )
  elseif ( OnlyQC ) then
     call HamiltonianMat%LoadQC( BraSymSpace, KetSymSpace, HamiltonianLabel, Axis )
     call HamiltonianMat%AssembleQC( LoadLocStates, HamMat )
  elseif ( OnlyLoc ) then
     call HamiltonianMat%LoadLoc( BraSymSpace, KetSymSpace, HamiltonianLabel, Axis )
     call HamiltonianMat%AssembleLoc( LoadLocStates, HamMat )
  elseif ( OnlyPoly ) then
     call HamiltonianMat%LoadPoly( BraSymSpace, KetSymSpace, HamiltonianLabel, Axis )
     call HamiltonianMat%AssemblePoly( LoadLocStates, HamMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          HamiltonianLabel//' operator matrix.' )
  end if
  !
  call HamiltonianMat%Free()
  MatIsSymmetric = HamMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'H is symmetric: ', MatIsSymmetric
  !
  call HamMat%Write( trim(SymStorageDir)//'H_basis' )
  call HamMat%Free

  !.. Complex absorber part.
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly ) then
     !
     !.. Load the absorption potential parameters
     call SAEGeneralInputData%ParseFile( GeneralInputFile )
     call AbsPot%Parse( AbsorptionPotentialFile )
     !
     do i = 1, AbsPot%NumberOfTerms()
        !
        if ( SET_FORMATTED_WRITE) call CAP%SetFormattedWrite()
        if ( BOX_ONLY  ) call CAP%SetBoxOnly()
        write(output_unit,"(a)") 'Loading CAP ...'
        if ( UseConditionedBlocks ) then
           call CAP%Load( BraSymSpace, KetSymSpace, GetFullCAPLabel(i), Axis, ConditionLabel, LoadLocStates=LoadLocStates )
        else
           call CAP%Load( BraSymSpace, KetSymSpace, GetFullCAPLabel(i), Axis, LoadLocStates=LoadLocStates )
        end if
        call CAP%Assemble( LoadLocStates, CAPMat )
        call CAP%Free()
        !
        call CAPMat%Write( trim(SymStorageDir)//trim(GetFullCAPLabel(i))//'_basis' )
        call CAPMat%Free
     end do

  end if
  
  i = 0
 
  if (x) i = i + 1
  if (y) i = i + 1
  if (z) i = i + 1
  write(*,*) x,y,z,i
  if (i .ne. 0) then
     allocate(xyz(i))
     do j = 1,i
       if (x) then
           xyz(j) = 1
           x = .false.
           cycle
       endif
       if (y) then
           xyz(j) = -1
           y = .false.
           cycle
       endif
       if (z) then
           xyz(j) = 0
           z = .false.
           cycle
       endif
     enddo
  endif

  do i = 1,2
     if ((i .eq. 1) .and. (DipVel)) then
        DipoleOpLabel = "DipoleVel"
     elseif ((i .eq. 2) .and. (DipLen)) then
        DipoleOpLabel = "DipoleLen"
     else
        cycle
     endif
     do j = 1,size(xyz)
        !
        if (xyz(j) .eq.  1) Axis = "X"
        if (xyz(j) .eq.  0) Axis = "Z"
        if (xyz(j) .eq. -1) Axis = "Y"
        call DipoleMat%SetFormattedWrite()
        !
        if ( OnlyQC ) then 
           call DipoleMat%LoadQC( BraSymSpace, KetSymSpace, DipoleOpLabel, Axis )
           call DipoleMat%AssembleQC( LoadLocStates, DipMat )
        else
           call DipoleMat%SetBoxOnly()
           if ( allocated(ConditionLabel) ) then
              call DipoleMat%Load( BraSymSpace, KetSymSpace, DipoleOpLabel, Axis, ConditionLabel )
           else
              call DipoleMat%Load( BraSymSpace, KetSymSpace, DipoleOpLabel, Axis )
           end if
           call DipoleMat%Assemble( LoadLocStates, DipMat )
        end if
        !
        call DipoleMat%Free()
        call DipMat%Write( trim(SymStorageDir)//trim(DipoleOpLabel)//'_'//trim(Axis) )
        call DipMat%Free
     enddo
  enddo
  if (allocated(xyz)) deallocate(xyz)
  write(output_unit,'(a)') "Done"

contains


  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       GeneralInputFile          , &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       BraSymLabel               , &
       KetSymLabel               , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  , &
       OnlyDiagQH                , &
       DecoupleChannels          , &
       DipVel                    , &
       DipLen                    , &
       x                         , &
       y                         , &
       z                         , &
       AnalyzeEvcts)
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: GeneralInputFile
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    logical,                       intent(out)   :: UseConditionedBlocks
    logical,                       intent(out)   :: UsePerfectProjection
    logical,                       intent(out)   :: OnlyQC
    logical,                       intent(out)   :: OnlyLoc
    logical,                       intent(out)   :: OnlyPoly  
    logical,                       intent(out)   :: OnlyDiagQH  
    logical,                       intent(out)   :: DecoupleChannels
    logical,                       intent(out)   :: AnalyzeEvcts
    logical,                       intent(out)   :: DipVel,DipLen,x,y,z
  !
    integer i,j
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: gif, pif, ccfile, qcdir, brasym, ketsym,sBuffer
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Print the overlap, Hamiltonian, CAP and Dipole "//&
         "in the basis "
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-gif"    , "SAE General Input File",  " ", "required" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"      , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-l"    , "Dipole Gauge Length (l) true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-v"    , "Dipole Gauge Length (v) true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-x"    , "Dipole Orientation x: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-y"    , "Dipole Orientation y: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-z"    , "Dipole Orientation x: true (t) or false (f)" ,  "f", "optional" )
    call List%Add( "-pp"  , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-onlyqc"  , "Only diagonalize the quantum chemistry sub-matrices." )
    call List%Add( "-onlyloc"  , "Only diagonalize the localized sub-matrices." )
    call List%Add( "-onlypoly"  , "Only diagonalize the localized sub-matrices." )
    call List%Add( "-onlydiagQH"  , "Only diagonalize the Quenched Hamiltonian." )
    call List%Add( "-DecoupleChannels"  , "Do not load the couplings between different channels in diagonalizing Hamiltonian." )
    call List%Add( "-AnalyzeEvcts"  , &
      "Print to file the most important close coupling basis functions contributing to each eigenstate" )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-gif" , gif  )
    call List%Get( "-pif" , pif  )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-esdir" , qcdir  )
    UseConditionedBlocks = List%Present( "-cond" )
    UsePerfectProjection = List%Present( "-pp" )
    OnlyQC               = List%Present( "-onlyqc" )
    OnlyLoc              = List%Present( "-onlyloc" )
    OnlyPoly             = List%Present( "-onlypoly" )
    OnlyDiagQH           = List%Present( "-onlydiagQH" )
    DecoupleChannels     = List%Present( "-DecoupleChannels" ) 
    AnalyzeEvcts         = List%Present( "-AnalyzeEvcts" ) 
    !
    allocate( GeneralInputFile, source = trim( gif ) )
    allocate( ProgramInputFile, source = trim( pif ) )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults, source = trim( qcdir ) )
    !
    call List%Get( "-v"   , sBuffer  )
    DipVel = .FALSE.
    if (trim(sBuffer) .eq. 't' ) DipVel = .TRUE.
    !
    call List%Get( "-l"   , sBuffer  )
    DipLen = .FALSE.
    if (trim(sBuffer) .eq. 't' ) DipLen = .TRUE.
    !
    call List%Get( "-x"   , sBuffer  )
    x = .FALSE.
    if (trim(sBuffer) .eq. 't' ) x = .TRUE.
    !
    call List%Get( "-y"   , sBuffer  )
    y = .FALSE.
    if (trim(sBuffer) .eq. 't' ) y = .TRUE.
    !
    call List%Get( "-z"   , sBuffer  )
    z = .FALSE.
    if (trim(sBuffer) .eq. 't' ) z = .TRUE.
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    !
    call List%Free()
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    if ( OnlyQC .and. OnlyLoc ) then
       call Assert( '"onlyqc" and "onlyloc" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyQC .and. OnlyPoly ) then
       call Assert( '"onlyqc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyLoc .and. OnlyPoly ) then
       call Assert( '"onlyloc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if

    !
  end subroutine GetRunTimeParameters



  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"     , ConditionNumber  )
    call List%Get( "LoadLocStates"       , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile






end program PrepareMatricesForTDSE
