!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

! fdec-struc fixed
!> This program checks the Virial Theorem for the selected box
!! eigenstates.
program CheckVirialTheorem
  
  
  use, intrinsic :: ISO_FORTRAN_ENV
  
  use ModuleGroups
  use ModuleElectronicSpace
  use ModuleSymmetricElectronicSpace
  use ModuleSymmetricElectronicSpaceOperators
  use ModuleElectronicSpaceOperators
  use ModuleMatrix
  use ModuleString
  use ModuleShortRangeOrbitals
  use ModuleSymmetryAdaptedSphericalHarmonics
  use ModuleSymmetricElectronicSpaceSpectralMethods
  
  implicit none

  logical         , parameter :: BOX_ONLY = .TRUE.
  logical         , parameter :: SET_FORMATTED_WRITE = .TRUE.


  !.. Run time parameters
  character(len=:), allocatable :: ProgramInputFile
  integer                       :: Multiplicity
  character(len=:), allocatable :: CloseCouplingConfigFile
  character(len=:), allocatable :: NameDirQCResults
  character(len=:), allocatable :: BraSymLabel
  character(len=:), allocatable :: KetSymLabel
  logical                       :: UseConditionedBlocks
  logical                       :: UsePerfectProjection
  integer                       :: InitState = 0
  integer                       :: FinalState  = 0
  logical                       :: OnlyQC
  logical                       :: OnlyLoc
  logical                       :: OnlyPoly

  !.. Config file parameters
  character(len=:), allocatable :: StorageDir
  real(kind(1d0))               :: ConditionNumber
  !> If .true. then use the localized state, if .false. then only use the products parent-ion * outer-electron.
  logical                       :: LoadLocStates
  integer                       :: NumBsDropBeginning
  integer                       :: NumBsDropEnding
  real(kind(1d0))               :: ConditionBsThreshold
  character(len=:), allocatable :: ConditionBsMethod


  type(ClassElectronicSpace)    :: Space
  type(ClassSymmetricElectronicSpace), pointer :: BraSymSpace, KetSymSpace
  type(ClassSESSESBlock)        :: KinEnergyMat, HamiltonianMat
  type(ClassMatrix)             :: KinMat, HamMat
  type(ClassSpectralResolution) :: HamSpecRes
  type(ClassConditionerBlock)   :: Conditioner
  type(ClassGroup), pointer     :: Group
  type(ClassIrrep), pointer     :: irrepv(:)
  type(ClassXlm)                :: Xlm

  character(len=:), allocatable :: DipoleAxis
  character(len=:), allocatable :: SymStorageDir
  character(len=:), allocatable :: ConditionLabel
  character(len=:), allocatable :: VirialFileName
  real(kind(1d0)) , allocatable :: VirialCoeffVec(:)
  logical :: MatIsSymmetric


  !.. Read run-time parameters, among which there is the group and the symmetry
  
  call GetRunTimeParameters(      &
       ProgramInputFile       ,   &
       Multiplicity           ,   &
       CloseCouplingConfigFile,   &
       NameDirQCResults       ,   &
       BraSymLabel            ,   &
       KetSymLabel            ,   &
       UseConditionedBlocks   ,   &
       UsePerfectProjection   ,   &
       InitState             ,   &
       FinalState              ,   &
       OnlyQC                 ,   &
       OnlyLoc                ,   &
       OnlyPoly               )


  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(a)" ) "Read run time parameters :"
  write(OUTPUT_UNIT,"(a)" )
  write(OUTPUT_UNIT,"(2a)" ) "Program Input File : ", ProgramInputFile
  write(OUTPUT_UNIT,"(a,i0)" )"Multiplicity :    ", Multiplicity
  write(OUTPUT_UNIT,"(2a)" ) "Close Coupling File : ", CloseCouplingConfigFile
  write(OUTPUT_UNIT,"(a)" ) "Nuclear Configuration Dir Name : "//NameDirQCResults
  if ( allocated(BraSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Bra Symmetry        : "//BraSymLabel
  end if
  if ( allocated(KetSymLabel) ) then
     write(OUTPUT_UNIT,"(a)" ) "Ket Symmetry        : "//KetSymLabel
  end if
  write(OUTPUT_UNIT,"(a,L)" )"Use conditioned blocks :     ", UseConditionedBlocks 
  write(OUTPUT_UNIT,"(a,L)" )"Perfect projector conditioner :     ", UsePerfectProjection 
  write(OUTPUT_UNIT,"(a,i0)" )"First state to check :    ", InitState
  write(OUTPUT_UNIT,"(a,i0)" )"Last state to check :    ", FinalState
  write(OUTPUT_UNIT,"(a,L)" )"Only QC, polycentric and monocentric Gaussian :     ", OnlyQC 
  write(OUTPUT_UNIT,"(a,L)" )"Only localized :     ", OnlyLoc 
  write(OUTPUT_UNIT,"(a,L)" )"Only polycentric Gaussian :     ", OnlyPoly



  call ParseProgramInputFile( & 
       ProgramInputFile     , &
       StorageDir           , &
       ConditionNumber      , &
       LoadLocStates        , &
       NumBsDropBeginning   , &
       NumBsDropEnding      , &
       ConditionBsThreshold , &
       ConditionBsMethod    )
  !
  write(OUTPUT_UNIT,*)
  write(OUTPUT_UNIT,"(a)"  ) "Storage Dir.................."//StorageDir
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for diagonalization :      ", ConditionNumber
  write(OUTPUT_UNIT,"(a,L)" ) "Load Localized States :    ", LoadLocStates
  write(OUTPUT_UNIT,"(a,i0)" )"Number of first B-splines to drop :    ", NumBsDropBeginning
  write(OUTPUT_UNIT,"(a,i0)" )"Number of last B-splines to drop :    ", NumBsDropEnding
  write(OUTPUT_UNIT,"(a,D12.6)" ) "Condition number for conditioning :      ", ConditionBsThreshold
  write(OUTPUT_UNIT,"(a)"  ) "Conditioning method.................."//ConditionBsMethod



  call Space%SetMultiplicity( Multiplicity )
  call Space%SetStorageDir( AddSlash(StorageDir)//AddSlash(NameDirQCResults) )
  call Space%SetNuclearLabel( NameDirQCResults )
  call Space%ParseConfigFile( CloseCouplingConfigFile )


  !..The same symmetries are allowed for the Kinetic Energy Operator and the 
  !  Potential Energy Operator, so this subroutine can be called only once.
  call GetSymmetricElectSpaces( Space         , &
       BraSymLabel, KinEnergyLabel, KetSymLabel, &
       DipoleAxis                             , &
       BraSymSpace, KetSymSpace )


  if ( SET_FORMATTED_WRITE) call KinEnergyMat%SetFormattedWrite()
  if ( BOX_ONLY .and. &
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly) call KinEnergyMat%SetBoxOnly()

  write(output_unit,"(a)") 'Loading kinetic energy ...'


  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly) then
     if ( UseConditionedBlocks ) then
        Group  => Space%GetGroup()
        irrepv => Group%GetIrrepList()
        Xlm = GetOperatorXlm( KinEnergyLabel, DipoleAxis )
        !.. Auxiliar conditioner only with the purpose to get the label.
        call Conditioner%Init(     &
             ConditionBsMethod   , &
             ConditionBsThreshold, &
             NumBsDropBeginning  , &
             NumBsDropEnding     , &
             irrepv(1), Xlm      , &
             UsePerfectProjection )
        allocate( ConditionLabel, source = Conditioner%GetLabel( ) )
        call KinEnergyMat%Load( BraSymSpace, KetSymSpace, KinEnergyLabel, DipoleAxis, ConditionLabel )
     else
        call KinEnergyMat%Load( BraSymSpace, KetSymSpace, KinEnergyLabel, DipoleAxis )
     end if
     call KinEnergyMat%Assemble( LoadLocStates, KinMat )
  elseif( OnlyQC ) then
     call KinEnergyMat%LoadQC( BraSymSpace, KetSymSpace, KinEnergyLabel, DipoleAxis )
     call KinEnergyMat%AssembleQC( LoadLocStates, KinMat )
  elseif( OnlyLoc ) then
     call KinEnergyMat%LoadLoc( BraSymSpace, KetSymSpace, KinEnergyLabel, DipoleAxis )
     call KinEnergyMat%AssembleLoc( LoadLocStates, KinMat )
  elseif( OnlyPoly ) then
     call KinEnergyMat%LoadPoly( BraSymSpace, KetSymSpace, KinEnergyLabel, DipoleAxis )
     call KinEnergyMat%AssemblePoly( LoadLocStates, KinMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          KinEnergyLabel//' operator matrix.' )
  end if
  !
  allocate( SymStorageDir, source = KinEnergyMat%GetStorageDir() )
  !
  call KinEnergyMat%Free()
  MatIsSymmetric = KinMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'K is symmetric: ', MatIsSymmetric



  if ( SET_FORMATTED_WRITE) call HamiltonianMat%SetFormattedWrite()
  if ( BOX_ONLY .and.&
       .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not.OnlyPoly ) call HamiltonianMat%SetBoxOnly()
  !
  write(output_unit,"(a)") 'Loading Hamiltonian ...'
  !
  if ( .not.OnlyQC .and. &
       .not.OnlyLoc .and. &
       .not. OnlyPoly ) then
     if ( UseConditionedBlocks ) then
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis, ConditionLabel )
     else
        call HamiltonianMat%Load( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     end if
     call HamiltonianMat%Assemble( LoadLocStates, HamMat )
  elseif ( OnlyQC ) then
     call HamiltonianMat%LoadQC( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssembleQC( LoadLocStates, HamMat )
  elseif ( OnlyLoc ) then
     call HamiltonianMat%LoadLoc( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssembleLoc( LoadLocStates, HamMat )
  elseif ( OnlyPoly ) then
     call HamiltonianMat%LoadPoly( BraSymSpace, KetSymSpace, HamiltonianLabel, DipoleAxis )
     call HamiltonianMat%AssemblePoly( LoadLocStates, HamMat )
  else
     call Assert( &
          'Invalid flag for loading or assembling '//&
          HamiltonianLabel//' operator matrix.' )
  end if
  !
  call HamiltonianMat%Free()
  MatIsSymmetric = HamMat%IsSymmetric(1.d-10)
  write(output_unit,*) 'H is symmetric: ', MatIsSymmetric

  if ( OnlyQC ) then
     call HamSpecRes%Read( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//QCLabel )
     allocate( VirialFileName, source = GetVirialFileName(SymStorageDir,ConditionLabel)//QCLabel )
  elseif ( OnlyLoc ) then
     call HamSpecRes%Read( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//LocLabel )
     allocate( VirialFileName, source = GetVirialFileName(SymStorageDir,ConditionLabel)//LocLabel )
  elseif ( OnlyPoly ) then
     call HamSpecRes%Read( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel)//PolyLabel )
     allocate( VirialFileName, source = GetVirialFileName(SymStorageDir,ConditionLabel)//PolyLabel )
  else
     call HamSpecRes%Read( GetSpectrumFileName(SymStorageDir,HamiltonianLabel,ConditionLabel) )
     allocate( VirialFileName, source = GetVirialFileName(SymStorageDir,ConditionLabel) )
  end if



  call ApplyVirialTheorem(    &
       KinMat, HamMat       , &
       HamSpecRes           , &
       InitState, FinalState, &
       VirialCoeffVec       )


  call SaveVirialTheorem(     &
       VirialFileName       , &
       HamSpecRes           , &
       InitState            , &
       VirialCoeffVec       )

   write(output_unit,'(a)') "Done"

contains
  
  

  !> Reads the run time parameters from the command line.
  subroutine GetRunTimeParameters( &
       ProgramInputFile          , &
       Multiplicity              , &
       CloseCouplingConfigFile   , &
       NameDirQCResults          , &
       BraSymLabel               , &
       KetSymLabel               , &
       UseConditionedBlocks      , &
       UsePerfectProjection      , &
       InitState                 , &
       FinalState                , &
       OnlyQC                    , &
       OnlyLoc                   , &
       OnlyPoly                  )
    !
    use ModuleCommandLineParameterList
    !
    implicit none
    !
    character(len=:), allocatable, intent(out)   :: ProgramInputFile
    integer,                       intent(out)   :: Multiplicity
    character(len=:), allocatable, intent(out)   :: CloseCouplingConfigFile
    character(len=:), allocatable, intent(out)   :: NameDirQCResults
    character(len=:), allocatable, intent(inout) :: BraSymLabel
    character(len=:), allocatable, intent(inout) :: KetSymLabel
    logical,                       intent(out)   :: UseConditionedBlocks
    logical,                       intent(out)   :: UsePerfectProjection
    integer                      , intent(out)   :: InitState
    integer                      , intent(out)   :: FinalState
    logical,                       intent(out)   :: OnlyQC
    logical,                       intent(out)   :: OnlyLoc
    logical,                       intent(out)   :: OnlyPoly  
  !
    type( ClassCommandLineParameterList ) :: List
    character(len=100) :: pif, ccfile, qcdir, brasym, ketsym
    character(len=*), parameter :: PROGRAM_DESCRIPTION =&
         "Checks the Virial Theorem for the selected eigenstates "//&
         "of the Hamiltonian in the box."
    !
    call List%SetDescription(PROGRAM_DESCRIPTION)
    call List%Add( "--help"  , "Print Command Usage" )
    call List%Add( "-pif"    , "Program Input File",  " ", "required" )
    call List%Add( "-mult"      , "Total Multiplicity", 1, "required" )
    call List%Add( "-ccfile" , "Close Coupling Configuration File",  " ", "required" )
    call List%Add( "-esdir" , "Name of the Folder in wich the QC Result are Stored",  " ", "required" )
    call List%Add( "-brasym" , "Name of the Bra Irreducible Representation",  " ", "optional" )
    call List%Add( "-ketsym" , "Name of the Ket Irreducible Representation",  " ", "optional" )
    call List%Add( "-cond"  , "Use the conditioned blocks" )
    call List%Add( "-pp"  , "The blocks computed using a perfect projector for the conditioning will be loaded" )
    call List%Add( "-initstate" , "First state to check virial theorem", 1, "required" )
    call List%Add( "-finalstate"      , "Last state to check virial theorem", 1, "optional" )
    call List%Add( "-onlyqc"  , "Only diagonalize the quantum chemistry sub-matrices." )
    call List%Add( "-onlyloc"  , "Only diagonalize the localized sub-matrices." )
    call List%Add( "-onlypoly"  , "Only diagonalize the localized sub-matrices." )
    !
    call List%Parse()
    !
    if(List%Present("--help"))then
       call List%PrintUsage()
       stop
    end if
    !
    call List%Get( "-pif" , pif  )
    call List%Get( "-mult"   , Multiplicity )
    call List%Get( "-ccfile" , ccfile  )
    call List%Get( "-esdir" , qcdir  )
    call List%Get( "-initstate"   , InitState )
    if ( List%Present( '-finalstate' ) ) then
       call List%Get( "-finalstate"   , FinalState )
    else
       FinalState = InitState
    end if
    !
    UseConditionedBlocks = List%Present( "-cond" )
    UsePerfectProjection = List%Present( "-pp" )
    OnlyQC               = List%Present( "-onlyqc" )
    OnlyLoc              = List%Present( "-onlyloc" )
    OnlyPoly             = List%Present( "-onlypoly" )
    !
    allocate( ProgramInputFile, source = trim( pif ) )
    allocate( CloseCouplingConfigFile, source = trim( ccfile ) )
    allocate( NameDirQCResults, source = trim( qcdir ) )
    !
    if(List%Present("-brasym"))then
       call List%Get( "-brasym", brasym )
       allocate( BraSymLabel, source = trim(brasym) )
    end if
    if(List%Present("-ketsym"))then
       call List%Get( "-ketsym", ketsym )
       allocate( KetSymLabel, source = trim(ketsym) )
    end if
    !
    call List%Free()
    !
    if ( .not.allocated(BraSymLabel) .and. &
         .not.allocated(KetSymLabel) ) &
         call Assert( 'At least the bra or the ket symmetry has to be present.' )
    !
    if ( OnlyQC .and. OnlyLoc ) then
       call Assert( '"onlyqc" and "onlyloc" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyQC .and. OnlyPoly ) then
       call Assert( '"onlyqc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    if ( OnlyLoc .and. OnlyPoly ) then
       call Assert( '"onlyloc" and "onlypoly" cannot be called '//&
            'at the same time at run time.' )
    end if
    !
    call CheckStateIndex( InitState )
    call CheckStateIndex( FinalState )
    !
  end subroutine GetRunTimeParameters



  subroutine CheckStateIndex( StateIndex )
    integer, intent(in) :: StateIndex
    character(len=32) :: Strn
    write(Strn,*) StateIndex
    if ( StateIndex <= 0 ) call Assert( &
         'The state index '//&
         trim(adjustl(Strn))//&
         ' is not possitive.' )
  end subroutine CheckStateIndex
  


  !> Parses the program input file.
  subroutine ParseProgramInputFile( &
       ProgramInputFile           , &
       StorageDir                 , &
       ConditionNumber            , &
       LoadLocStates              , &
       NumBsDropBeginning         , &
       NumBsDropEnding            , &
       ConditionBsThreshold       , &
       ConditionBsMethod          )
    !
    use ModuleParameterList
    !> Program input file.
    character(len=*)             , intent(in)  :: ProgramInputFile
    character(len=:), allocatable, intent(out) :: StorageDir
    real(kind(1d0))              , intent(out) :: ConditionNumber
    logical                      , intent(out) :: LoadLocStates
    integer                      , intent(out) :: NumBsDropBeginning
    integer                      , intent(out) :: NumBsDropEnding
    real(kind(1d0))              , intent(out) :: ConditionBsThreshold
    character(len=:), allocatable, intent(out) :: ConditionBsMethod
    !
    character(len=100) :: StorageDirectory, Method
    type(ClassParameterList) :: List
    !
    call List%Add( "StorageDir"          , "Outputs/", "optional" )
    call List%Add( "ConditionNumber"     , 1.d0      , "required" )
    call List%Add( "LoadLocStates"       , .false.   , "required" )
    call List%Add( "NumBsDropBeginning"  , 0         , "required" )
    call List%Add( "NumBsDropEnding"     , 0         , "required" )
    call List%Add( "ConditionBsThreshold", 1.d0      , "required" )
    call List%Add( "ConditionBsMethod"   , " "       , "required" )
    !
    call List%Parse( ProgramInputFile )
    !
    call List%Get( "StorageDir"          , StorageDirectory   )
    allocate( StorageDir, source = trim(adjustl(StorageDirectory)) )
    call List%Get( "ConditionNumber"     , ConditionNumber  )
    call List%Get( "LoadLocStates"       , LoadLocStates  )
    call List%Get( "NumBsDropBeginning"  , NumBsDropBeginning  )
    call List%Get( "NumBsDropEnding"     , NumBsDropEnding  )
    call List%Get( "ConditionBsThreshold", ConditionBsThreshold  )
    call List%Get( "ConditionBsMethod"   , Method   )
    allocate( ConditionBsMethod, source = trim(adjustl(Method)) )
    !
    call CheckBsToRemove( NumBsDropBeginning )
    call CheckBsToRemove( NumBsDropEnding )
    call CheckThreshold( ConditionNumber )
    call CheckThreshold( ConditionBsThreshold )
    !
    call SetStringToUppercase( ConditionBsMethod )
    call CheckMethod( ConditionBsMethod )
    !
  end subroutine ParseProgramInputFile


  !> VirialCoeffVec(i) = - <i|V|i>/<i|K|i>
  subroutine ApplyVirialTheorem( &
       KinMat, HamMat          , &
       HamSpecRes              , &
       InitState, FinalState   , &
       VirialCoeffVec          )
    !
    class(ClassMatrix)            , intent(in)    :: KinMat
    class(ClassMatrix)            , intent(inout) :: HamMat
    class(ClassSpectralResolution), intent(in)    :: HamSpecRes
    integer                       , intent(in)    :: InitState
    integer                       , intent(in)    :: FinalState
    real(kind(1d0)), allocatable  , intent(out)   :: VirialCoeffVec(:)
    !
    integer :: i, NumStates, NumReqStates, NumAvailStates, NewFinalState
    type(ClassMatrix) :: StateMat, CopyKmat, CopyVmat
    !
    !..Computes the potential energy matrix only transforming the Hamiltonian.
    call HamMat%Multiply( -1.d0 )
    call HamMat%Add( KinMat )
    call HamMat%Multiply( -1.d0 )
    !
    NumReqStates = FinalState - InitState + 1
    NumAvailStates = HamSpecRes%NEval()
    !
    if ( NumAvailStates < NumReqStates ) call ErrorMessage( &
         'There are less available states than requested.' )
    if ( InitState > NumAvailStates ) call Assert( &
         'The requested initial state is higher than '//&
         'the number of available states.')
    !
    NewFinalState = min(NumAvailStates,FinalState)
    NumStates = NewFinalState - InitState + 1
    !
    !
    allocate( VirialCoeffVec(NumStates) )
    VirialCoeffVec = 0.d0
    !
    !
    do i = InitState, NewFinalState
       !
       CopyKmat = KinMat
       CopyVmat = HamMat
       !
       call HamSpecRes%Fetch( StateMat, i )
       !
       if ( StateMat%NRows() /= CopyKmat%NRows() ) call Assert( &
            'The number of basis functions in the eigenstates '//&
            'and in the kinetic energy matrix is different.')
       !
       call CopyKmat%Multiply( StateMat, 'Left' , 'T' )
       call CopyKmat%Multiply( StateMat, 'Right', 'N' )
       !
       call CopyVmat%Multiply( StateMat, 'Left' , 'T' )
       call CopyVmat%Multiply( StateMat, 'Right', 'N' )
       !
       call StateMat%Free()
       VirialCoeffVec(i) = - CopyVmat%Element(1,1)/CopyKmat%Element(1,1)     
       !
    end do
    !
    call CopyKmat%Free()
    call CopyVmat%Free()
    !
  end subroutine ApplyVirialTheorem



  subroutine SaveVirialTheorem( &
       FileName               , &
       HamSpecRes             , &
       InitState              , &
       VirialCoeffVec         )
    !
    character(len=*)              , intent(in) :: FileName
    class(ClassSpectralResolution), intent(in) :: HamSpecRes
    integer                       , intent(in) :: InitState
    real(kind(1d0))               , intent(in) :: VirialCoeffVec(:)
    !
    integer :: uid, iostat, i, NewFinalState
    real(kind(1d0)), allocatable :: Energies(:)
    !
    call HamSpecRes%Fetch( Energies )
    !
    call OpenFile( FileName, uid, 'write', 'formatted' )
    !
    write(uid,'(2(a,10x))',iostat=iostat) '# Energy', 'Coeff(i) = - <i|V|i>/<i|K|i> ~ 2'
    !
    NewFinalState = InitState + size(VirialCoeffVec) - 1
    !
    do i = InitState, NewFinalState
       write(uid,'(2f24.17)',iostat=iostat) Energies(i), VirialCoeffVec(i-InitState+1)
    end do
    !
    close( uid )
    !
  end subroutine SaveVirialTheorem
    
    
  
end program CheckVirialTheorem
