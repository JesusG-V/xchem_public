DEB_FLAG=
MAKE=/usr/bin/make
HOST=$(shell hostname)
CC=gcc
#FC=ifort
MKL=no
FC=ifort

COM=../Common.mk
IODIR=../basicIO
MISCDIR=../misc
ifeq ($(DEB_FLAG),d)
BIN=../../bin_d
OBJ=$(DEB_FLAG)obj_$(FC)
MOD=$(DEB_FLAG)mod_$(FC)
else 
BIN=../../bin
OBJ=obj_$(FC)
MOD=mod_$(FC)
endif

export CC FC IODIR MISCDIR BIN DEB_FLAG OBJ MOD COM

## Compiler Options
#
ifeq ($(FC),gfortran)
    ifeq ($(DEB_FLAG),d)
        FC_OPTS= -c -C -g -O0 -finteger-4-integer-8 -m64 -fbacktrace #-I${MKLROOT}/include
    else
        FC_OPTS= -c -finteger-4-integer-8 -m64 -fopenmp #-I${MKLROOT}/include
    endif
    ifeq ($(MKL),yes)
        LAPACK= -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_gf_ilp64 -lmkl_gnu_thread -lmkl_core
    else
        LAPACK= -llapack -lblas
    endif
    LINK_OPTS= $(LAPACK) -lgomp -lpthread -lm -ldl
    MOD_OPTS= -J
endif

ifeq ($(FC),ifort)
    ifeq ($(DEB_FLAG),d)
        FC_OPTS=-c -p -i8 -r8 -fpp -qopenmp -parallel -O0 -D_I8_ -DEXT_INT -D_INTEL_ -D_LINUX_  -D_MOLCAS_ -check bounds -traceback
    else
        FC_OPTS=-c -p -i8 -r8 -fpp -qopenmp -parallel -O2  -D_I8_ -DEXT_INT -D_INTEL_ -D_LINUX_  -D_MOLCAS_ -mcmodel=medium 
    endif
    LINK_OPTS=-mkl \
              -qopenmp
    MOD_OPTS=-mod
endif
export FC_OPTS LINK_OPTS MOD_OPTS

## Define the names of the subdirectories
#
SUB_DIRS= \
integralProcessing \
automatization \
linDepKill \
GABS_interface \
augmentation \
basicIO \
misc


## Define the names of all programs
#
INT_PRGS = \
oneIntOrb2OperatorMatrix \
twoIntOrb2OperatorMatrix \
oneIntOrb2OperatorMatrix_new \
twoIntOrb2OperatorMatrix_new \
oneIntBas2Orb \
oneIntBas2Orb_new \
ijkl2ijrs \
ijkl2iqks \
iqks2pqrs \
ijkl2pqrs \
trd1Element \
trd2Element \
trd1Build \
trd2Build \
twoIntConcatenate \
twoIntConcatenatetwoexp \
pqrsConcatenate \
compressTwoElectronIntegrals \
sumOneIntTwoIntNuclear \
subtractDiagonal \
computeEvals \
bin2fmt

AUT_PRGS= \
createInputCas \
createInputInt \
createInputOrb \
pro2cas \
readSewardProCas \
reOrderSym2NoSym

LDK_PRGS= \
linDepKill 

GAB_PRGS= \
GABS_interface \
GABS_interface_sym \
symSplit

AUG_PRGS= \
augment \
CSF2Det \
Det2CSF \
CSFs2CSF \
mix \
detSplit \
eig

MKDIR_P = mkdir -p
%/:
	mkdir -p $@
# Program update rules
##
.PHONY: directories

.PHONY : $(INT_PRGS)
$(INT_PRGS) :
	$(MAKE) $(@) -C integralProcessing

.PHONY : $(LDK_PRGS)
$(LDK_PRGS) :
	$(MAKE) $(@) -C linDepKill

.PHONY : $(GAB_PRGS)
$(GAB_PRGS) :
	$(MAKE) $(@) -C GABS_interface

.PHONY : $(AUG_PRGS)
$(AUG_PRGS) :
	$(MAKE) $(@) -C augmentation

.PHONY : $(AUT_PRGS)
$(AUT_PRGS) :
	$(MAKE) $(@) -C automatization

ALL_PRGS = $(INT_PRGS) $(LDK_PRGS) $(GAB_PRGS) $(AUG_PRGS) $(AUT_PRGS)
DIRS = $(foreach a,$(SUB_DIRS), $(a)/$(MOD) $(a)/$(OBJ))

all : directories $(ALL_PRGS)

directories: ${DIRS}

${DIRS}:
	${MKDIR_P} ${DIRS}

# Clean Rules
##
CLEAN=$(foreach a,$(SUB_DIRS),clean_$(a))

$(CLEAN) : 
	$(MAKE) clean -C $(subst clean_,,$@)
	rm -f ../$(DEB_FLAG)bin_$(FC)/$(subst clean_,,$@)

clean : $(CLEAN)
	$(MAKE) clean -C $(notdir $(IODIR),$(MISCDIR))
