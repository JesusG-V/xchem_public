!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program several_binOrbs
implicit none
integer nsym1
integer nbas0,nbas1,norb0,nbas,norb
real*8, allocatable :: orb(:,:),overlap(:,:)
real*8, allocatable :: orb2(:,:)

integer isym

!!! Several sets of orbitals !!!!
integer nsets

character*100 fich
integer i,j,norb1n,k,l
logical ex
real*8 kk

!! The overlap basis is used to know the number of basis functions
!! and to orthonormalize
write(6,*) "File with the overlap of the basis"
read(5,*) fich
inquire(file=fich,exist=ex)
if (ex) then
 write(6,*) "Reading overlap from ",trim(fich)
else
 write(6,*) "File with overlap is not optional ",trim(fich)
 stop 'Overlap file does not exist '
endif
open(1,file=fich,form="unformatted")
read(1) i
if (i.ne.1) then
 write(6,*) "The program is only working with no symmetry in the localized orbitals"
 stop 'Symmetry is still not implemented'
endif
read(1) nbas
allocate (overlap(nbas,nbas),orb(nbas,nbas),orb2(nbas,nbas))
write(6,*) "Using basis functions ",nbas
do i=1,nbas
 read(1) (overlap(i,j),j=1,i)
enddo
close(1)
do i=1,nbas
 do j=1,i
  overlap(j,i)=overlap(i,j)
 enddo
enddo
write(6,*) "Overlap basis ready"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!! Let's read the ref orbitals
write(6,*) "File with the ref orbitals in binary (usually the augmented ion)"
read(5,*) fich
inquire(file=fich,exist=ex)
if (ex) then
 open(1,file=fich,form="unformatted")
 read(1)
 read(1)
 read(1) ((orb(j,i),j=1,nbas),i=1,nbas)
 close(1)
else
 write(6,*) 'File with ref orbitals does not exist ',trim(fich)
 stop 'File with ref orbitals does not exist '
endif
!! Checking the zero orbitals in ref
norb=0
do i=1,nbas
 kk=0.
 do j=1,nbas
  do k=1,nbas
   kk=kk+orb(j,i)*orb(k,i)*overlap(j,k)
  enddo
 enddo
 if (kk.ge.1e-5) norb=norb+1
enddo
write(6,*) "Total orbitals no zero in ref ",norb
  

!!! Let's take the local orbitals for the different sets
write(6,*) "Number of sets of local orbitals to prepare"
read(5,*) nsets
if (nsets.gt.0) then
!!! Common information
 write(6,*) "Number of local basis functions and local orbitals"
 read(5,*) nbas0,norb0
 write(6,*) "nbas0,norb0=",nbas0,norb0
 if (norb0.gt.nbas0) then
  stop 'The number of orbitals cannot be bigger than the basis'
 endif
 if (nbas0.gt.nbas) then
  stop 'The local basis should be smaller than the total basis'
 endif
else
 stop
endif
do k=1,nsets
 write(6,*) "File for the set ",k
 read(5,*) fich
 inquire(file=fich,exist=ex)
 if (ex) then
  orb2=0.
  open(1,file=fich,form="unformatted")
  read(1) ((orb2(j,i),j=1,nbas0),i=1,norb0)
  close(1)
 else
  write(6,*) 'File with local orbitals does not exist ',trim(fich)
  stop 'File with local orbitals does not exist '
 endif

 write(6,*) "Reading local from ",trim(fich)
 write(6,*) "Local orbitals ready for set ",k
!!! Pasting the ref orbitals
 do i=norb0+1,nbas
  do j=1,nbas
   orb2(j,i)=orb(j,i)
  enddo
 enddo
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 call GramSchmidt(nbas,nbas,orb2,overlap)
!!! Doing zero the linear dependent orbitals
 do i=norb+1,nbas
  do j=1,nbas
   orb2(j,i)=0.
  enddo
 enddo

 write(6,*) "File for the output orbitals (MOLCAS form)"
 read(5,*) fich
 open(1,file=fich)

 do i=1,nbas
  if (i.le.norb0) then
   write(1,"(A,2(x,I4))") "* Local orbital ",i,i
  else if (i.le.norb) then
   write(1,"(A,2(x,I4))") "* Orthonormalized aug orbital ",i,i-norb0
  else
   write(1,"(A,2(x,I4))") "* Zero orbital ",i,i-norb0
  endif
  write(1,"(4(E18.12e2))") (orb2(j,i),j=1,nbas)
 enddo
 close(1)

 write(6,*) "File for the output orbitals (binary form)"
 read(5,*) fich
 open(1,file=fich,form="unformatted")
 write(1) 1
 write(1) nbas
 write(1) ((orb2(j,i),j=1,nbas),i=1,nbas)
 close(1)


!!! How similar is the new set
 do i=norb0+1,norb
  kk=0.
  do j=1,nbas
   do l=1,nbas
    kk=kk+orb2(j,i)*orb(l,i)*overlap(l,j)
   enddo
  enddo
  write(6,*) "Set, orb, overlap ",k,i,kk
 enddo
   

enddo

end
