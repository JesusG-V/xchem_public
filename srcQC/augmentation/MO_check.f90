!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program MO_check
implicit none

interface
 subroutine readover(n,ov,fich,way)
 implicit none
 character*100, intent(in) :: fich,way
 integer, intent(out) :: n
 real*8,intent(out), allocatable :: ov(:,:)
 end subroutine readover

 subroutine readMO(n,orb,fich,way)
 implicit none
 character*100, intent(in) :: fich,way
 integer, intent(in) :: n
 real*8,intent(out) :: orb(n,n)
 end subroutine readMO

 subroutine MOov(n,orb1,orb2,overlap,ov)
 implicit none
 integer, intent(in) :: n
 real*8, intent(in) :: orb1(n),orb2(n),overlap(n,n)
 real*8, intent(out) :: ov
 end subroutine MOov

end interface


integer nbas,nset
real*8 :: thrs=0.5

real*8, allocatable :: overlap(:,:),orb(:,:,:)
real*8, allocatable :: orb1(:),orb2(:)
real*8, allocatable :: norm(:,:)

real*8 kk,kk2,kk3
integer i,j,iset,jset
character*100 fich,way

real*8 maxoff

write(6,*) "Overlap file and type (binary or text)"
read(5,*) fich,way
write(6,*) "Overlap will read from ",trim(fich),trim(way)
call readover(nbas,overlap,fich,way)
write(6,*) "Number of basis ",nbas

write(6,*) "Number of orbital sets"
read(5,*) nset

allocate (orb(nbas,nbas,nset),norm(nbas,nset))
allocate (orb1(nbas),orb2(nbas) )
do iset=1,nset

 write(6,*) "Molecular orbital file and type (binary, free or MOLCAS), set ",iset
 read(5,*) fich,way
 write(6,*) "Orbital will read from ",trim(fich),trim(way)
 call readMO(nbas,orb(:,:,iset),fich,way)
 write(6,*) "Ready set ",iset,trim(fich)
enddo

write(6,*) "File to the output"
read(5,*) fich
open(1,file=fich)

do iset=1,nset
 maxoff=0.d0
 write(1,*) "Set ",iset
 kk2=0.
 do i=1,nbas
  orb1=orb(:,i,iset)
  orb2=orb(:,i,iset)
  call MOov(nbas,orb1,orb2,overlap,norm(i,iset))
  write(1,*) "overlap ",i,norm(i,iset)
  if (norm(i,iset)**2.ge.(1.-thrs) ) then
   kk2=kk2+(1.-norm(i,iset))**2
   do j=1,nbas
    if (i.ne.j) then
     orb1=orb(:,i,iset)
     orb2=orb(:,j,iset)
     call MOov(nbas,orb1,orb2,overlap,kk)
     if (kk**2.ge.maxoff**2) maxoff=kk
     kk2=kk2+kk**2
    endif
   enddo
  endif
 enddo
 write(1,*) "Out of orthonormal (sum,max) ",sqrt(kk2),maxoff

enddo

if (nset.ne.1) then
 write(1,*) "Comparing orbital sets"
 do iset=1,nset-1
  do jset=iset+1,nset
   write(1,*) "Comparing ",iset,jset
   kk3=0.
   do i=1,nbas
    if (norm(i,iset).ge. thrs) then
     kk2=0.
     orb1=orb(:,i,iset)
     do j=1,nbas
      orb2=orb(:,j,jset)
      call MOov(nbas,orb1,orb2,overlap,kk)
      kk2=kk2+kk**2
     enddo
!!!  Comparing with itself
     orb2=orb(:,i,jset)
     call MOov(nbas,orb1,orb2,overlap,kk)
     write(1,"(A,I5,x,I2,A,I2,x,F10.8,A,I5,x,F10.7)") &
     "Orbital ",i,iset," has a total overlap in the set ",jset,kk2, &
     ", specifically orbital ",i,kk
     kk3=kk3+kk2
    endif
   enddo
   write(1,"(A,2(I5,x),F10.2)") "Sum of overlaps ",iset,jset,kk3
   write(1,*) "Comparing ",jset,iset
   kk3=0.
   do j=1,nbas
    if (norm(j,jset).ge. thrs) then
     kk2=0.
     orb1=orb(:,j,jset)
     do i=1,nbas
      orb2=orb(:,i,iset)
      call MOov(nbas,orb1,orb2,overlap,kk)
      kk2=kk2+kk**2
     enddo
!!!  Comparing with itself
     orb2=orb(:,j,iset)
     call MOov(nbas,orb1,orb2,overlap,kk)
     write(1,"(A,I5,x,I2,A,I2,x,F10.8,A,I5,x,F10.7)") &
     "Orbital ",j,jset," has a total overlap in the set ",iset,kk2, &
     ", specifically orbital ",j,kk
     kk3=kk3+kk2
    endif
   enddo
   write(1,"(A,2(I5,x),F10.2)") "Sum of overlaps ",jset,iset,kk3
  enddo
 enddo
endif
    
    


end
