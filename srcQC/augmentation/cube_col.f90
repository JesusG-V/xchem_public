!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program cube_col
implicit none
integer np(3),nat,dir
real*8 x0(3),dx(3,3)
complex*16, allocatable :: wf(:,:,:),wfp(:)
real*8, allocatable :: kk(:)

integer i,j,k,ii,ij,ik,il,l
real*8 norm(2)
real*8 dr(3)
real*8 Epot(2),El(2),x(3)
character*100 fich
logical ex
real*8 ra

integer Lmax,Ltot
integer nmom
real*8 kmom,kmom0,kmomf,ZE
complex*16, allocatable :: proj(:)
real*8, allocatable :: colf(:,:)

integer nr
parameter (nr=1000)
real*8 rmin,rmax,rc(nr)
integer npp(3),ipoint


read(5,*) Epot(:)
read(5,*) El(:)
El=El/27.211

read(5,*) fich

inquire(file=fich, exist=ex)
if (ex) then
 open(1,file=fich)
else
 write(6,*) "Cube file it does not exist ",fich
endif
open(2,file="out_dir.cube")
read(1,*)
write(2,*) 
read(1,*)
write(2,*)
read(1,*) nat,x0(:)
write(2,901) nat,x0(:)

do i=1,3
 read(1,*) np(i),dx(i,:)
 write(2,901) np(i),dx(i,:)
enddo
do i=1,nat
 read(1,"(A)") fich
 write(2,"(A)") trim(fich)
enddo

allocate (wf(np(1),np(2),np(3)))
allocate (kk(np(3)))

write(6,*) "Starting reading cube file"
do i=1,np(1)
 do j=1,np(2)
  read(1,*) (kk(k),k=1,np(3) )
  do k=1,np(3)
   wf(i,j,k)=cmplx(kk(k),0.)
  enddo
 enddo
enddo
write(6,*) "Dyson orbital in memory"
deallocate(kk)

read(5,*) dir
if (dir.eq.1) then
 write(6,*) "Calculating dipole in direction x"
else if (dir.eq.2) then
 write(6,*) "Calculating dipole in direction y"
else if (dir.eq.3) then
 write(6,*) "Calculating dipole in direction z"
else
 stop 'The dipole cannot be calculated'
endif


!! Defining dr for later
do ii=1,3
 dr(ii)=0.
 do j=1,3
  dr(ii)=dr(ii)+dx(ii,j)**2
 enddo
 dr(ii)=sqrt(dr(ii))
enddo

do i=1,3
 write(6,*) "Integrating dir ",dx(i,:)
 allocate (kk(np(i)) )
 call integra(i,np,wf,kk)
 if (i.eq.1) open(1,file="int1.sp")
 if (i.eq.2) open(1,file="int2.sp")
 if (i.eq.3) open(1,file="int3.sp")
 do j=1,np(i)
  ra=float(j-1)*dr(i)
  do k=1,3
   x(k)=x0(k)+float(j-1)*dx(i,k)
  enddo
  write(1,900) ra,kk(j),x(:)
 enddo
 close(1)
 deallocate(kk)
enddo
 
!!!!!
rmin=1000000
rmax=0
!!!!!

 
norm(1)=0.0
norm(2)=0.0
allocate (kk(np(3)))
do i=1,np(1)
 do j=1,np(2)
  do k=1,np(3)
   norm(1)=norm(1)+abs(wf(i,j,k))**2
   ra=0
   do ii=1,3
    x(ii)=x0(ii)
    x(ii)=x(ii)+float(i-1)*dx(1,ii)
    x(ii)=x(ii)+float(j-1)*dx(2,ii)
    x(ii)=x(ii)+float(k-1)*dx(3,ii)
    ra=ra+x(ii)**2
   enddo
   ra=sqrt(ra)
!!!!!
   if (ra.le.rmin) rmin=ra
   if (ra.ge.rmax) rmax=ra
!!!!!

   ra=x(dir)
   wf(i,j,k)=ra*wf(i,j,k)
   kk(k)=dreal(wf(i,j,k))
   norm(2)=norm(2)+abs(wf(i,j,k))**2
  enddo
  write(2,902) (kk(k),k=1,np(3))
 enddo
enddo
deallocate(kk)
do i=1,3
 norm(1)=norm(1)*dr(i)
 norm(2)=norm(2)*dr(i)
enddo
close(2)
   
write(6,*) "Norm in position representation "
write(6,*) "Dys norm ",norm(1)
write(6,*) "dir Dys norm ",dir,norm(2)

!!! IF norm is smaller than a threshold (1e-5) less stop since ionization is negligible
if (norm(1).le.1e-5 .and. norm(2).le.1e-5) then
 write(6,*) 'Dyson norm is negligible'
 write(6,*) "Aborting"
 stop
endif

do i=1,3
 write(6,*) "Integrating dir ",i
 allocate (kk(np(i)) )
 call integra(i,np,wf,kk)
 if (i.eq.1) open(1,file="int1z.sp")
 if (i.eq.2) open(1,file="int2z.sp")
 if (i.eq.3) open(1,file="int3z.sp")
 do j=1,np(i)
  ra=float(j-1)*dr(i)
  do k=1,3
   x(k)=x0(k)+float(j-1)*dx(i,k)
  enddo
  write(1,900) ra,kk(j),x(:)
 enddo
 close(1)
 deallocate(kk)
enddo

!write(6,*) "Writting dir Dys in file space.vtk"
!open(1,file="space.vtk")
!call vtk(1,np,x0,dr,wf,.false.)
!close(1)

read(5,*) Lmax,ZE
read(5,*) nmom,kmom0,kmomf
allocate (kk(0:Lmax) )
Ltot=0
do i=0,Lmax
 Ltot=Ltot+2*i+1
enddo
allocate (colf(nr,0:Lmax), proj(Ltot),wfp(Ltot) )
  

write(6,*) "Calculating projections and saving them in proj.out"
write(6,*) "Using L ",Lmax,Ltot

open(1,file="proj.out")
do ii=1,nmom
 proj=cmplx(0.,0.)
 kmom=kmom0+float(ii-1)*(kmomf-kmom0)/float(nmom-1)
 write(6,*) "Doing ",ii,nmom

!$OMP PARALLEL DO &
!$OMP SHARED(nr,rc,rmin,rmax,kmom,ZE,Lmax,colf) &
!$OMP PRIVATE(i)
 do i=1,nr
  rc(i)=rmin+float(i-1)*(rmax-rmin)/float(nr-1) 
  call Hydrogenic_Continuum_Wave(kmom,rc(i),ZE,Lmax,colf(i,:) )
 !!! Renormalizing to the energy
  if (kmom.gt.0) colf(i,:)=colf(i,:)*sqrt(1./kmom)
 enddo
!$OMP END PARALLEL DO

 write(6,*) "Done ",kmom
 write(fich,"(A4,I5.5,A4)") "col_",ii,".out"
 open(2,file=fich)
 write(2,"(A14,E20.10e3)") "# Coulomb Mom ",kmom
 do i=1,nr
  write(2,900) rc(i),colf(i,:)
 enddo
 close(2)

!$OMP PARALLEL DO &
!$OMP SHARED(np,x0,dx,colf,rc,Lmax,Ltot,proj,wf) &
!$OMP PRIVATE(i,j,k,ra,ik,x,kk,l,il,wfp)
 do k=1,np(3)
  do j=1,np(2)
   do i=1,np(1)

    ra=0.
    do ik=1,3
     x(ik)=x0(ik)
     x(ik)=x(ik)+float(i-1)*dx(1,ik)
     x(ik)=x(ik)+float(j-1)*dx(2,ik)
     x(ik)=x(ik)+float(k-1)*dx(3,ik)
     ra=ra+x(ik)**2
    enddo
    ra=sqrt(ra)

    do ik=1,nr-1
     if (ra.ge.rc(ik) .and. ra.le.rc(ik+1) ) then
      do ij=0,Lmax
       kk(ij)=colf(ik,ij)+(ra-rc(ik))*   &
       (colf(ik+1,ij)-colf(ik,ij))/(rc(ik+1)-rc(ik))
      enddo
     endif
    enddo
     
    call sph_harm(x,Lmax,Ltot,wfp)
    l=0
    do ik=0,Lmax
     do il=-ik,ik
      l=l+1
      proj(l)=proj(l)+conjg(wf(i,j,k))*kk(ik)*wfp(l)
     enddo
    enddo

   enddo
  enddo
 enddo
!$OMP END PARALLEL DO

 l=0
 do i=0,Lmax
  do j=-i,i
   l=l+1
   proj(l)=proj(l)*dr(1)*dr(2)*dr(3)
   write(1,900) kmom,float(i),float(j),kmom*kmom/2.+Epot(2)-Epot(1), &
   abs(proj(l))**2, real(proj(l)),imag(proj(l))
  enddo
 enddo
enddo



   

900 format(10000(x(E20.10e3)) )
!!! cube format
901 format(I5,3(x,F11.6))
902 format(6(x,E12.5e2))
end


subroutine integra(dir,np,wf,integ)
implicit none
integer, intent(in) :: dir,np(3)
complex*16, intent(in) :: wf(np(1),np(2),np(3) )
real*8, intent(out) :: integ(np(dir))

integer i,j,k,ii,ij
 
do k=1,np(dir)
 if (dir.eq.1) then
  ii=2
  ij=3
 endif
 if (dir.eq.2) then
  ii=1
  ij=3
 endif
 if (dir.eq.3) then
  ii=1
  ij=2
 endif
 integ(k)=0.
 do i=1,np(ii)
  do j=1,np(ij)
   if (dir.eq.1) integ(k)=integ(k)+abs(wf(k,i,j))**2
   if (dir.eq.2) integ(k)=integ(k)+abs(wf(i,k,j))**2
   if (dir.eq.3) integ(k)=integ(k)+abs(wf(i,j,k))**2
  enddo
 enddo
enddo


end


subroutine internal(x,ra,theta,phi)
implicit none
real*8, intent(in) :: x(3)
real*8, intent(out) :: ra,theta,phi
integer i

ra=0.
do i=1,3
 ra=ra+x(i)**2
enddo
ra=sqrt(ra)

theta=0.
phi=0.
if (ra.ge.1e-5) then
 theta=acos(x(3)/ra)
 phi=atan2(x(2),x(1))
endif

end

subroutine sph_harm(x,Lmax,Ltot,wf)
implicit none
integer,intent(in) :: Lmax,Ltot
real*8, intent(in) :: x(3)
complex*16, intent(out) :: wf(Ltot)

real*8 ra,theta,phi
integer ml,l,i
complex*16 Ylm

call internal(x,ra,theta,phi)
call Ylm_lmax(Lmax,Ltot,theta,phi,wf)

end
