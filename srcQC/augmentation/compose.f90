!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program compose
implicit none
integer npot0,npot1,npot
integer norb

real*8, allocatable :: Ham(:,:),ov(:,:)

integer reducen
real*8, allocatable :: reducem(:,:)

integer i,j

integer nfich,n
character*100 fich1,fich2
integer,allocatable :: nst(:)

logical ex
logical ex1,ex2
integer readun


write(6,*) "Number of states that are not augmented"
read(5,*) npot0
write(6,*) "Number of states that are augmented"
read(5,*) npot1
write(6,*) "Number of augmented orbitals (RAS + virtual)"
read(5,*) norb

npot=npot0+npot1*norb
write(6,*) "Total number of states ",npot

write(6,*) "Reduced matrix in the rassi"
read(5,*) reducen

allocate (Ham(npot,npot),ov(npot,npot))
allocate (reducem(reducen,reducen),nst(reducen))
Ham=0.d0
ov=0.d0

!!! Temporal solution
write(6,*) "Total number of number of files"
read(5,*) nfich,fich1
inquire(file=fich1,exist=ex) 
if (ex) then
 open(3,file=fich1)
 readun=3
else
 write(6,*) "Reading list of files from input"
 readun=5
endif

do n=1,nfich
 read(readun,*) fich1,fich2,nst(:)
 inquire(file=fich1,exist=ex1)
 inquire(file=fich2,exist=ex2)
 if (ex1 .and. ex2 ) then
  open(1,file=fich1)
  open(2,file=fich2)
!!! Reading Ham
  do i=1,reducen
   read(1,*) (reducem(i,j),j=1,i)
   do j=1,i
    if (i.ne.j) reducem(j,i)=reducem(i,j)
   enddo
  enddo
  do i=1,reducen
   do j=1,reducen
    Ham(nst(i),nst(j))=reducem(i,j)
   enddo
  enddo
!!! Reading ov
  do i=1,reducen
   read(2,*) (reducem(i,j),j=1,i)
   do j=1,i
    if (i.ne.j) reducem(j,i)=-reducem(i,j)
   enddo
  enddo
  do i=1,reducen
   do j=1,reducen
    ov(nst(i),nst(j))=reducem(i,j)
   enddo
  enddo
  
 else
  write(6,*) "Hamiltonian or overlap matrix does not exist"
  write(6,*) fich1,ex1
  write(6,*) fich2,ex2
  stop
 endif

enddo
write(6,*) "Data ready"

open(1,file="Hamiltonian.dat")
open(2,file="overlap.dat")
write(1,*) npot
write(2,*) npot
write(1,*) 
write(2,*) 
do i=1,npot
 write(1,"(10000(x,E30.20e3))") (Ham(i,j),j=1,npot)
 write(2,"(10000(x,E30.20e3))") (ov(i,j),j=1,npot)
enddo
 

end
