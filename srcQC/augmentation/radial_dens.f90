!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program radial_dens
implicit none
integer np(3)
real*8, allocatable :: dens(:,:,:),rden(:)
real*8 x0(3),dr(3,3),x(3),dx(3),dtau
real*8 r,r0,rmax,kk,rgr
integer nr

integer i,j,k,ir,l,m,ip(3)

!READ integration parameters
read(5,*) r0,nr,rmax
allocate(rden(nr))
!READ blank
read(5,*) 
!READ nr of atoms and box size x y z 
read(5,*)  i,x0(:)
!READ basis vectors x y z
dtau=1.
do j=1,3
 dx(j)=0.
 read(5,*) np(j),dr(j,:)
 do k=1,3
  dx(j)=dx(j)+dr(j,k)**2
 enddo
 dx(j)=sqrt(dx(j))
 dtau=dtau*dx(j)
enddo
!READ atomic corrds
do j=1,i
 read(5,*)
enddo

!READ grid
allocate (dens(np(1),np(2),np(3)) )
do i=1,np(1)
 do j=1,np(2)
  read(5,*) (dens(i,j,k),k=1,np(3))
 enddo
enddo

!Integrate
do ir=1,nr
 rden(ir)=0.
 r=r0+float(ir-1)*(rmax-r0)/float(nr-1)
 do i=1,np(1)
  ip(1)=i
  do j=1,np(2)
   ip(2)=j
   do k=1,np(3)
    ip(3)=k
    do l=1,3
     x(l)=x0(l)
     do m=1,3
      x(l)=x(l)+float(ip(m)-1)*dr(m,l)
     enddo
    enddo
    rgr=sqrt(x(1)**2+x(2)**2+x(3)**2)
    !if (rgr.le.r) rden(ir)=rden(ir)+dens(i,j,k)
    if (rgr.le.r) rden(ir)=rden(ir)+dens(i,j,k)**2
   enddo
  enddo
 enddo
enddo
do ir=1,nr
 rgr=(rmax-r0)/float(nr-1)
 r=r0+float(ir-1)*rgr
 if (ir.eq.1) then 
  kk=rden(ir)
 else
  kk=rden(ir)-rden(ir-1)
 endif
 write(6,*) r,rden(ir)*dtau,kk*dtau
enddo

end
