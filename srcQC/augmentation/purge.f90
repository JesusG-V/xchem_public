!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE purge
contains
SUBROUTINE remove(overlap,nbas,x1,x2,orb,norb1,epsilon1,maxEVal,noRMIN)

implicit none
INTEGER, INTENT(in) :: x1,x2,nbas
INTEGER, INTENT(inout) :: norb1
REAL*8, INTENT(in) :: epsilon1
REAL*8, INTENT(in) ,DIMENSION(nbas,nbas) :: overlap
REAL*8, INTENT(inout), DIMENSION(nbas,nbas) :: orb

INTEGER :: i,j,k,l,totorb,nbas1,m
REAL*8, ALLOCATABLE :: temp_overlap(:,:),temp_orb(:,:)
REAL*8, ALLOCATABLE :: w(:)
!REAL*8, ALLOCATABLE :: bla(:,:)

REAL*8, INTENT(inout) :: maxEVal
LOGICAL :: noRM
LOGICAL, OPTIONAL :: noRMIN

noRM=.FALSE.
nbas1=x2-x1+1

IF (PRESENT(noRMIN)) THEN
    noRM=noRMIN
ENDIF

ALLOCATE(temp_overlap(nbas1,nbas1),temp_orb(nbas1,nbas1))
!ALLOCATE(bla(nbas1,nbas1))

!bla=0.d0
temp_overlap=0.d0
temp_orb=0.d0

DO i=x1,x2
    k=i-x1+1
    DO j=x1,x2
        l=j-x1+1
        temp_orb(k,l)=orb(i,j)
    ENDDO
    
    DO j=x1,x2
        l=j-x1+1
        temp_overlap(k,l)=overlap(i,j)
    ENDDO
ENDDO

ALLOCATE(w(nbas1))
w=0.d0
!bla=temp_overlap
!CALL diag(nbas1,bla,temp_overlap,w)
!CALL order(temp_overlap,nbas1,w,.false.)
CALL diag(temp_overlap,nbas1,w)
CALL order(temp_overlap,nbas1,w,.false.)

IF (noRM) THEN
    maxEVal=MAXVAL(w)
    RETURN
ENDIF
norb1=0
DO i=nbas1,1,-1
    IF (w(i)/maxEVal.gt.epsilon1) THEN
        norb1=norb1+1
        DO j=1,nbas1
            temp_orb(j,norb1)=temp_overlap(j,i)/sqrt(w(i))
            IF (temp_orb(j,norb1)**2.le.1e-18) THEN
                temp_orb(j,norb1)=0.d0
            ENDIF
        ENDDO
    ELSE
        IF (w(i+1)/maxEVal.gt.epsilon1) THEN
            WRITE(6,*) "-----Last linear independent",norb1,w(i+1)
            WRITE(6,*) "-----First linear dependent ",norb1+1,w(i)
        ENDIF
    ENDIF
ENDDO
DO i=x1,x2
    k=i-x1+1
    DO j=x1,x2
        l=j-x1+1
        orb(i,j)=temp_orb(k,l)
    ENDDO
ENDDO
DEALLOCATE(temp_overlap,temp_orb,w)

END SUBROUTINE remove

SUBROUTINE remove2(overlap,nbas,x1,x2,y1,y2,orb,norb1,norb2,epsilon1)
implicit none
INTEGER, INTENT(in) :: x1,x2,y1,y2,nbas
INTEGER, INTENT(inout) :: norb1,norb2
REAL*8, INTENT(in) :: epsilon1
REAL*8, INTENT(in) ,DIMENSION(nbas,nbas) :: overlap
REAL*8, INTENT(inout), DIMENSION(nbas,nbas) :: orb

INTEGER :: i,j,k,l,totorb,nbas1,nbas2,nbas1_2,m
REAL*8, ALLOCATABLE :: temp_orb(:,:),tmp(:,:),proj(:,:)
REAL*8, ALLOCATABLE :: w(:)
!REAL*8, ALLOCATABLE :: metric(:,:)

nbas1=0
nbas2=0

nbas1=x2-x1+1
nbas2=y2-y1+1

nbas1_2=nbas1+nbas2

ALLOCATE(temp_orb(nbas,nbas1_2))

temp_orb=0.d0

DO i=x1,x2
    k=i-x1+1
    DO j=x1,x2
        l=j-x1+1
        temp_orb(k,l)=orb(i,j)
    ENDDO
ENDDO
DO i=1,nbas
    DO j=y1,y2
        l=j-y1+1+nbas1
        temp_orb(i,l)=orb(i,j)
    ENDDO
ENDDO

ALLOCATE(proj(norb2,norb2),w(norb2),tmp(norb1,norb2))
!ALLOCATE(metric(norb2,norb2))
proj=0.d0
w=0.d0
tmp=0.d0
!metric=0.d0
DO i=1,norb1
    DO j=nbas1+1,nbas1+norb2
        DO k=1,nbas1
            DO l=1,nbas
                m=j-nbas1
                tmp(i,m)=tmp(i,m)+temp_orb(k,i)*temp_orb(l,j)*overlap(k,l)
            ENDDO
        ENDDO
    ENDDO
ENDDO

!DEBUG
    DO i=1,norb2
        WRITE(6,'(<norb2>(x,F12.6))') (tmp(j,i),j=1,norb1)
    ENDDO
!DEBUG

proj=matmul(transpose(tmp),tmp)
!!DEBUG
!WRITE(6,'(A)') "_________________proj"
!WRITE(6,'(A)') ""
!DO i=1,norb2
!    WRITE(6,'(<norb2>(x,F16.12))') (proj(j,i),j=1,norb2)
!ENDDO
call diag(proj,norb2,w)
!metric=matmul(transpose(tmp),tmp)
!call diag(norb2,metric,proj,w)

IF (MAXVAL(ABS(w)).le.(1.0-epsilon1)) THEN
    print*, "------=>NO overlap! max e'val of projection: ",MAXVAL(w)
    print*, " "
    DEALLOCATE(proj,tmp)
    DEALLOCATE(temp_orb,w)
    RETURN
ENDIF

!!DEBUG
!WRITE(6,'(A)') "_________________proj"
!WRITE(6,'(A)') ""
!DO i=1,norb2
!    WRITE(6,'(<norb2>(x,F16.12))') (proj(j,i),j=1,norb2)
!ENDDO

!!DEBUG
!WRITE(6,'(A)') "_________________w"
!WRITE(6,'(A)') ""
!WRITE(6,'(4(x,F16.12))') w

DEALLOCATE(tmp)
ALLOCATE(tmp(nbas,nbas1_2))

tmp=0.d0
tmp=temp_orb

!!DEBUG
!WRITE(6,'(A)') "_________________tmp(:,nbas1+1)"
!WRITE(6,'(A)') ""
!WRITE(6,'(4(x,F16.12))') tmp(:,nbas1+1)

totorb=nbas1
DO i=1,norb2
    IF (w(i).le.1.0-epsilon1) THEN
        totorb=totorb+1
        DO j=1,nbas
            tmp(j,totorb)=0.d0
            DO k=1,norb2
                tmp(j,totorb)=tmp(j,totorb)+proj(k,i)*temp_orb(j,k+nbas1)  
            ENDDO
        ENDDO
    ELSE
        PRINT*, "-----Too similar to local ones",i,w(i)
    ENDIF                        
ENDDO

!!DEBUG
!WRITE(6,'(A)') "_________________tmp(:,nbas1+1)"
!WRITE(6,'(A)') ""
!WRITE(6,'(4(x,E16.8))') (tmp(i,nbas1+1),i=1,nbas)

DO i=totorb+1,nbas1_2
    DO j=1,nbas
        tmp(j,i)=0.d0
    ENDDO
ENDDO

DO i=1,nbas
    DO j=y1,y2
        l=j-y1+1+nbas1
        orb(i,j)=tmp(i,l)
    ENDDO
ENDDO

norb2=totorb-nbas1
DEALLOCATE(proj,tmp)
DEALLOCATE(temp_orb,w)

END SUBROUTINE remove2
SUBROUTINE collapse (nblock,block_orb,block_bas,nbas,orb,last)
    INTEGER, INTENT(in) :: nblock,nbas,last
    INTEGER, INTENT(in) :: block_orb(0:nblock),block_bas(0:nblock)
    REAL*8, INTENT(inout) :: orb(nbas,nbas)
    
    INTEGER :: i,j,k,shift,shift_start,shifted_block_size
    
    shifted_block_size=0
    shift_start=nbas
   
    DO i=nblock-1,last,-1
        shift=block_bas(i)-block_orb(i)
        shifted_block_size=shifted_block_size+block_orb(i+1)
        shift_start=shift_start-block_bas(i+1)
        IF (shift.ne.0) THEN
            DO j=shift_start+1,shift_start+shifted_block_size
                orb(:,j-shift)=orb(:,j)
                orb(:,j)=0.d0
            ENDDO
        ENDIF
    ENDDO
END SUBROUTINE collapse
END MODULE purge
