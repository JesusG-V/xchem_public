!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module Detfile_

implicit none

type, public :: Detfile
 integer norb,totalDets,npot
 real*8, allocatable :: CIDet(:,:)
 logical, allocatable :: Detlist(:,:)

 contains
  procedure :: readDetfile
end type Detfile

contains

subroutine readDetfile(this, un)
implicit none
class(Detfile) :: this
integer, intent(in) :: un

integer i,j
character*100 tmpchar
integer*1 tmpdet(this%norb)

read(un,*) this%npot,this%totalDets,this%norb
allocate (this%CIDet(this%npot,this%totalDets), this%Detlist(this%totalDets,2*this%norb))

do i=1,this%totalDets
 read(1,*) tmpchar,(this%CIDet(j,i),j=1,this%npot)
 read(tmpchar,"(100I1.1)") (tmpdet(j),j=1,this%norb)
 do j=1,this%norb
  if (tmpdet(j).eq.0) then
   this%Detlist(i,2*j)=.false.
   this%Detlist(i,2*j-1)=.false.
  else if (tmpdet(j).eq.1) then
   this%Detlist(i,2*j)=.false.
   this%Detlist(i,2*j-1)=.true.
  else if (tmpdet(j).eq.2) then
   this%Detlist(i,2*j)=.true.
   this%Detlist(i,2*j-1)=.false.
  else if (tmpdet(j).eq.3) then
   this%Detlist(i,2*j)=.true.
   this%Detlist(i,2*j-1)=.true.
  else
   write(6,*) "Problems orbital ",j
   stop 'Orbital can only be 0, 1, 2 or 3'
  endif
 enddo
enddo

end subroutine readDetfile

end module Detfile_
