!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program CSF2Det

use CSFtools
use Configuration_

implicit none
integer :: norb
type(Configuration) inCSFs

integer npot,nCSF
character*200 fich,tmpDet

integer totalDets
real*8, allocatable :: CIDet(:,:),CICSF(:,:)
integer*1, allocatable :: Detlist(:,:)

integer :: mdet

integer :: iCSF,iroot,i
real*8, allocatable :: innorm(:),outnorm(:)
real*8 Detnorm
real*8 :: redthr=+1.e-15
character*200 way,wayDet


write(6,*) "File with the CI vector and format (ascii or binary)"
read(5,*) fich,way
write(6,*) trim(fich)," ",trim(way)
call readCSF0(fich,way,1,npot,nCSF)
allocate (CICSF(npot,nCSF))
call readCSF(way,1,npot,nCSF,CICSF)
write(6,*) "CI vector in CSFs ready ",nCSF,npot

write(6,*) "File with the guga table"
read(5,*) fich 
open(1,file=fich,status="old",iostat=i)
if (i.ne.0) then
 write(6,*) "File does not exist ",trim(fich)
 stop 'File with guga table is compulsory'
endif
!!! Reading nCSF and norb from guga table
read(1,*) i,norb
if (i.ne.nCSF) then
 write(6,*) "Number of configurations GUGA table ",i
 write(6,*) "Number of configurations in CI vector ",nCSF
 stop 'Number of configurations different in GUGA and CI'
endif
mdet=0
call inCSFs%initConfiguration(norb,nCSF)
call inCSFs%readit(1,mdet,.TRUE.)
close(1)

allocate(innorm(npot),outnorm(npot))
do iroot=1,npot
 innorm(iroot)=0.
 do iCSF=1,nCSF
  innorm(iroot)=innorm(iroot)+CICSF(iroot,iCSF)**2
 enddo
enddo

!!! Allocating supervector for the CI in Slater Determinants
write(6,*) "Allocating vector of Determinants ",mdet
allocate(Detlist(mdet,norb),CIDet(npot,mdet) )
CIDet=0.d0

totalDets=0
call inCSFs%checkDets(mdet,npot,Detlist,totalDets,CIDet, &
 CICSF,npot,0)

write(6,*) "Total different Slater Determinants ",totalDets

write(6,*) "File with the CI vector in Slater Determinants and type"
read(5,*) fich,wayDet
write(6,*) fich,wayDet

do iroot=1,npot
 outnorm(iroot)=0.
 do iCSF=1,totalDets
  outnorm(iroot)=outnorm(iroot)+CIDet(iroot,iCSF)**2
 enddo
 if ((innorm(iroot)-outnorm(iroot))**2.ge.0.001) then
  write(6,*) "Warning root ",iroot
  write(6,*) "Norms (CSF,Det) ",innorm(iroot),outnorm(iroot)
 endif
 write(6,*) "Norms ",iroot,innorm(iroot),outnorm(iroot)
enddo

!!! Removing dets with very small values
i=0
do iCSF=1,totalDets
 Detnorm=0.
 do iroot=1,npot
  Detnorm=Detnorm+CIDet(iroot,iCSF)**2
 enddo
 if (Detnorm.ge.redthr) then
  i=i+1
 endif
enddo

write(6,*) "Reduction of Determinants ",totalDets,i

if (wayDet.eq."ascii") then
 open(1,file=fich)
 write(1,"(3(x,I10.10))") npot,i,norb
 do iCSF=1,totalDets
  !!! Lets use fichDet as a temporal character array to save the Determinant
  Detnorm=0.
  do iroot=1,npot
   Detnorm=Detnorm+CIDet(iroot,iCSF)**2
  enddo
  if (Detnorm.ge.redthr) then
   write(tmpDet,"(50(I1.1))") (DetList(iCSF,iroot),iroot=1,norb)
   write(1,"(A,x,100(x,E40.30e3))") trim(tmpDet),(CIDet(iroot,iCSF),iroot=1,npot)
  endif
 enddo
elseif (wayDet.eq."binary") then
 open(1,file=fich,form="unformatted")
 write(1) npot,i,norb
 do iCSF=1,totalDets
  !!! Lets use fichDet as a temporal character array to save the Determinant
  Detnorm=0.
  do iroot=1,npot
   Detnorm=Detnorm+CIDet(iroot,iCSF)**2
  enddo
  if (Detnorm.ge.redthr) then
   write(tmpDet,"(50(I1.1))") (DetList(iCSF,iroot),iroot=1,norb)
   write(1) trim(tmpDet),(CIDet(iroot,iCSF),iroot=1,npot)
  endif
 enddo
else
 stop "error: unknown format"
endif
close(1)

end program CSF2Det
