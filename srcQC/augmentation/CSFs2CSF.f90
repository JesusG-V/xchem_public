!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program CSFs2CSF
implicit none
integer nfil,nCSF,ntot,npot
real*8, allocatable :: CI(:)
character*200, allocatable :: fich(:)
character*200 fichout

integer ifil,i
integer iCSF,ipot

write(6,*) "Number of different CSF files"
read(5,*) nfil
write(6,*) nfil
allocate(fich(nfil))
ntot=0
do ifil=1,nfil
 read(5,*) fich(ifil)
 open(1,file=fich(ifil),status="old",iostat=i)
 if (i.ne.0) then
  write(6,*) "CSF file does not exist ",fich(ifil)
  stop 'CSF file does not exist'
 endif
 read(1,*) npot,iCSF
 close(1)
 if (ifil.eq.1) nCSF=iCSF
 if (iCSF.ne.nCSF) then
  write(6,*) "All the files must belong to the same active space"
  write(6,*) "CSFs File 1 ",nCSF
  write(6,*) "CSFs File ",ifil,iCSF
  stop "All the files must belong to the same active space"
 endif
 ntot=ntot+npot
enddo

write(6,*) "File with the CI vector for all the states ",ntot
read(5,*) fichout
write(6,*) fichout
open(2,file=fichout)
write(2,"(2(x,I10.10))") ntot,nCSF
allocate(CI(nCSF))
do ifil=1,nfil
 open(1,file=fich(ifil))
 read(1,*) npot,iCSF
 do ipot=1,npot
  read(1,*) (CI(iCSF),iCSF=1,nCSF)
  write(2,"(100000(x,E30.20e3))") (CI(iCSF),iCSF=1,nCSF)
 enddo
 close(1)
enddo

end program
