!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneEl_ham
        implicit none
        integer :: i,j,k,l,nbas,norb,nsets,s
        REAL*8 :: nuclear_repulsion,parity
        REAL*8, ALLOCATABLE :: Homemade(:,:),afterRAS(:,:),oneEl(:,:),H_AR(:,:),H_HM(:,:)
        REAL*8, ALLOCATABLE :: op(:,:)
        REAL*8, ALLOCATABLE :: op_HM(:,:)
        REAL*8, ALLOCATABLE :: op_AR(:,:)
        REAL*8, ALLOCATABLE :: eig_AR(:),eig_HM(:)
        REAL*8, ALLOCATABLE :: temp_AR(:,:),temp_HM(:,:)
        CHARACTER*100 :: fich,fich2,fich3

        PRINT*, "number of orbs and basis function"
        READ(5,*) norb,nbas
        PRINT*, norb,nbas
        
        PRINT*, "file with oneEl.ham"
        READ(5,*) fich
        PRINT*, fich
        OPEN(1,file=fich,STATUS="old")

        PRINT*, "file with Homemade"
        READ(5,*) fich
        PRINT*, fich
        OPEN(2,file=fich,STATUS="old",form="unformatted")
           
        PRINT*, "file with afterRAS"
        READ(5,*) fich
        PRINT*, fich
        OPEN(3,file=fich,STATUS="old",form="unformatted")       
        
        PRINT*, "Extra sets to be considered"
        READ(5,*) nsets
        PRINT*, nsets

        ALLOCATE(Homemade(norb,nbas),afterRAS(norb,nbas),oneEl(nbas,nbas),H_AR(norb,norb),H_HM(norb,norb))
        ALLOCATE(op(nbas,nbas),op_HM(norb,norb),op_AR(norb,norb))
        ALLOCATE(eig_HM(norb),eig_AR(norb))
        ALLOCATE(temp_HM(norb,norb),temp_AR(norb,norb))
        Homemade=0.d0
        afterRAS=0.d0
        oneEl=0.d0
        H_AR=0.d0
        H_HM=0.d0

        READ(1,*) nuclear_repulsion
        print*, "nuclear potential energy=",nuclear_repulsion

        DO i=1,nbas
                READ(1,*) (oneEl(i,j),j=1,i)
        ENDDO
        
        DO i=2,nbas
                DO j=1,i-1
                        oneEl(j,i)=oneEl(i,j)
                ENDDO
        ENDDO                     
        
        READ(2) ((Homemade(i,j),j=1,nbas),i=1,norb)        
        READ(3) ((afterRAS(i,j),j=1,nbas),i=1,norb)

        print *, "express Dipoles and Hamiltonian in Homemade/afterRAS orbitals"
        DO i=1,norb
                DO j=1,norb
                        DO k=1,nbas
                                DO l=1,nbas    
                                        H_HM(i,j)=H_HM(i,j)+Homemade(i,k)*Homemade(j,l)*oneEl(k,l)
                                        H_AR(i,j)=H_AR(i,j)+afterRAS(i,k)*afterRAS(j,l)*oneEl(k,l)
                                ENDDO
                        ENDDO
                ENDDO
        ENDDO

        OPEN(14,file="oneEl.Homemade",status="replace",RECL=60000)
        OPEN(15,file="oneEl.afterRAS",status="replace",RECL=60000)

        DO i=1,norb
                H_HM(i,i)=H_HM(i,i)+nuclear_repulsion
                H_AR(i,i)=H_AR(i,i)+nuclear_repulsion
                WRITE(14,*) (H_HM(i,j),j=1,norb)
                WRITE(15,*) (H_AR(i,j),j=1,norb)
        ENDDO

        CLOSE(14)
        CLOSE(15)

        DO s=1,nsets
                op=0.d0; op_HM=0.d0; op_AR=0.d0
                PRINT*, "input data, Homemade output and afterRAS output files for set ",s
                READ(5,*) fich,fich2,fich3,parity
                PRINT*, fich,fich2,fich3,parity
                OPEN(100,file=fich,STATUS="old")
                DO i=1,nbas
                        READ(100,*) (op(i,j),j=1,i)
                ENDDO
                DO i=2,nbas
                        DO j=1,i-1
                                op(j,i)=op(i,j)*parity
                        ENDDO
                ENDDO                     
                DO i=1,norb
                        DO j=1,norb
                                DO k=1,nbas
                                        DO l=1,nbas    
                                                op_HM(i,j)=op_HM(i,j)+Homemade(i,k)*Homemade(j,l)*op(k,l)
                                                op_AR(i,j)=op_AR(i,j)+afterRAS(i,k)*afterRAS(j,l)*op(k,l)
                                        ENDDO
                                ENDDO
                        ENDDO
                ENDDO
                OPEN(101,file=fich2,status="replace",RECL=60000)
                OPEN(102,file=fich3,status="replace",RECL=60000)
                DO i=1,norb
                        WRITE(101,*) (op_HM(i,j),j=1,norb)                
                        WRITE(102,*) (op_AR(i,j),j=1,norb)                
                ENDDO
                CLOSE(100)
                CLOSE(101)
                CLOSE(102)
        ENDDO

        print*, "Diagonalizing Hamiltonian"
        temp_AR=H_AR
        temp_HM=H_HM
        !call diag(H_AR,norb,eig_AR)
        !call diag(H_HM,norb,eig_HM)
        call diag(norb,H_AR,temp_AR,eig_AR)
        call diag(norb,H_HM,temp_HM,eig_HM)

        print*, "Printing eigen values"
        OPEN(14,file="eig.check.Homemade",status="replace")
        OPEN(15,file="eig.check.afterRAS",status="replace")

        DO i=1,norb
                WRITE(14,*) eig_HM(i)
                WRITE(15,*) eig_AR(i)
        ENDDO

        !temp_HM=MATMUL(TRANSPOSE(H_HM),temp_HM)
        !temp_HM=MATMUL(temp_HM,H_HM)
        
        !temp_AR=MATMUL(TRANSPOSE(H_AR),temp_AR)
        !temp_AR=MATMUL(temp_AR,H_AR)

        !OPEN(16,file="check.Homemade",status="replace",RECL=60000)
        !OPEN(17,file="check.afterRAS",status="replace",RECL=60000)
        !DO i=1,norb
        !        WRITE(16,'(*(f6.2))') (temp_HM(i,j),j=1,norb)
        !        WRITE(17,'(*(f6.2))') (temp_AR(i,j),j=1,norb)
        !ENDDO
        !PRINT*, "DONE"
END PROGRAM
