# Compile shared files
##
gen_ofMisc=$(foreach a,$(gen_Misc),$(MISCDIR)/$(OBJ)/$(a).o)
$(gen_ofMisc) : $(MISCDIR)/$(OBJ)/%.o : $(MISCDIR)/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTS) $(MISCDIR)/$(MOD) -I $(MISCDIR)/$(MOD) $< -o $@

gen_ofIo=$(foreach a,$(gen_Io),$(IODIR)/$(OBJ)/$(a).o)
$(gen_ofIo) : $(IODIR)/$(OBJ)/%.o : $(IODIR)/%.f90
	$(FC) $(FC_OPTS) $(MOD_OPTS) $(IODIR)/$(MOD) -I $(MISCDIR)/$(MOD) -I $(IODIR)/$(MOD) $< -o $@

# Compile local files
##
loc_of=$(foreach a,$(loc),$(OBJ)/$(a).o)
$(loc_of) : $(OBJ)/%.o : %.f90
	$(FC) $(FC_OPTS) $(MOD_OPTS) $(MOD) -I $(IODIR)/$(MOD) -I $(MISCDIR)/$(MOD) -I $(MOD) $< -o $@

# Link
##
$(loc) : % : $(OBJ)/%.o $(gen_ofMisc) $(gen_ofIo)
	$(FC) $^ -o $(BIN)/$(@).exe $(LINK_OPTS)

# Clean
##
clean:
	rm -f $(OBJ)/*.o $(MOD)/*.mod

# Dependencies between shared file
##
$(IODIR)/$(OBJ)/oneIntIO.o : \
	$(MISCDIR)/$(OBJ)/diag-lapack.o

$(MISCDIR)/$(OBJ)/orbitalToolKit.o : \
	$(MISCDIR)/$(OBJ)/diag-lapack.o

$(MISCDIR)/$(OBJ)/createInputUtilities.o : \
	$(MISCDIR)/$(OBJ)/inputProcessing.o

$(IODIR)/$(OBJ)/gatewayTool.o : \
	$(IODIR)/$(OBJ)/lkIO.o \
	$(MISCDIR)/$(OBJ)/inputProcessing.o \
	$(MISCDIR)/$(OBJ)/createInputUtilities.o

$(IODIR)/$(OBJ)/configurationIO.o : \
	$(IODIR)/$(OBJ)/CSFpropIO.o \
	$(MISCDIR)/$(OBJ)/searchInsert.o \
	$(MISCDIR)/$(OBJ)/sortingAlgorithms.o 

