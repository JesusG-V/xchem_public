!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM GABS_interface
!This code reads the basis sets for the ghost atom(s) and a set of 
!molecular orbitals. From this the interface with the XCHEM code is 
!generated containing the monocentric orbitals (expressed in primitives
!rather tha contracted basis function) as well as iformation about the 
!angular momenta.

!if working with one ghost atom for all ang moms specify the name of the file 
!with basis functions with the input basisFile. Otherwise the basis functions
!for the different angular moment code will look for files: l.$l_k.$k.basisFile with
!$l and $k being the angular momenta.
    USE basisIO
    USE orbitalIO
    USE blockIO
    IMPLICIT none
!CONTRAL VAR
    CHARACTER*200 :: inputFile
    INTEGER :: iostat
    LOGICAL :: exist
    CHARACTER*2000 :: fmt
    
!INPUT VARS
    INTEGER :: multiplicity
    INTEGER :: lMax,kMax
    INTEGER :: nLocOrb,nLocBas,nOrb
    INTEGER :: nInactive
    LOGICAL :: multiGhost
    CHARACTER*200 :: basisFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: blockInfoFile

!STURCTURAL VARS
    INTEGER :: bigLMax
    INTEGER :: nXOrb,nXBas,nBas
    INTEGER :: nBlock,nRelevantBlock
    INTEGER,ALLOCATABLE :: kList(:)
    INTEGER,ALLOCATABLE :: lList(:)
    INTEGER,ALLOCATABLE :: mList(:)
    REAL*8,ALLOCATABLE :: exponentVector(:)
    REAL*8,ALLOCATABLE :: orbPrimitiveMatrix(:,:)
    REAL*8,ALLOCATABLE :: orbContractedMatrix(:,:)
    REAL*8,ALLOCATABLE :: contractionMatrix(:,:)
    TYPE(orbital_) :: orb
    TYPE(basis_), ALLOCATABLE :: basis(:,:)
    TYPE(block_), ALLOCATABLE :: block(:)


!TEMP VARS
    CHARACTER*100 :: fich
    INTEGER :: iGhost
    INTEGER :: iBlock,jBlock
    INTEGER :: l,k,m
    INTEGER :: iOrb,iBas
    INTEGER :: nPrimitive,nContraction
    INTEGER :: iPrimitive,iContraction
    INTEGER :: offsetC,offsetP,iSym
    CHARACTER*50 :: label
    LOGICAL :: relevant

    NAMELIST /INPUT/ lMax,&
      kMax,&
      multiGhost,&
      blockInfoFile,&
      basisFile,&
      orbitalFile,&
      nLocOrb,&
      nLocBas,&
      nOrb,&
      multiplicity,&
      nInactive

    multiGhost=.FALSE.
    nXBas=0
    nLocOrb=0
    nLocBas=0
    nOrb=0
    nInactive=0
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exist)
    IF (.NOT.exist) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=iostat)
    IF (iostat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=iostat)
    IF (iostat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    nBas=nLocBas
    nXOrb=nOrb-nLocOrb

!READ ORBITALS
    WRITE(6,'(A)') "Read orbitals"
    CALL orb%readOrbital(TRIM(orbitalFile),"b")
    CALL orb%getOrbital(orbContractedMatrix)

!READ BASIS SETS
    WRITE(6,'(A)') "Read basis sets"
    IF (multiGhost) THEN
        ALLOCATE(basis(0:lMax,0:kMax))
        DO l=0,lMax
            DO k=0,kMax
                WRITE(fich,'(A2,I1,A3,I1,A1,A)') "l.",l,"_k.",k,".",TRIM(basisFile)
                CALL basis(l,k)%create(TRIM(fich))
            ENDDO
        ENDDO
    ELSE
        ALLOCATE(basis(0:0,0:0)) 
        CALL basis(0,0)%create(TRIM(basisFile))
    ENDIF

!READ INFORMATION ABOUT SETS (BLOCKS) OF BASIS FUNCTIONS CORRESPONDING
!TO THE SAME k/l/ghost
    WRITE(6,'(A)') "Read information about l/k-block"
    OPEN(1,FILE=TRIM(blockInfoFile),STATUS="old")
    READ(1,*);READ(1,*);READ(1,*)
    READ(1,*) nBlock
    ALLOCATE(block(nBlock))
    iGhost=0
    nRelevantBlock=0
    DO iBlock=1,nBlock
        IF (multiGhost) THEN
            READ(1,*) iBas,label,l,m,k,iGhost,iSym
        ELSE
            READ(1,*) iBas,label,l,m,k,iSym
        ENDIF
        CALL block(iBlock)%create(k,l,m,iBas,nBas,label,iSym,iGhost)
        nBas=nBas+iBas

        relevant=.TRUE.
        IF (l.gt.lMax.OR.k.gt.kMax) relevant=.FALSE.  
        IF (multiGhost) THEN
            IF (iGhost.ne.l) THEN
                relevant=.FALSE.
            ENDIF
        ENDIF
        IF (relevant) THEN
            IF (multiGhost) THEN
                nPrimitive=basis(l,k)%getNPrimitive()
            ELSE
                nPrimitive=basis(0,0)%getNPrimitive()
            ENDIF
            nXBas=nXBas+nPrimitive
            nRelevantBlock=nRelevantBlock+1
        ELSE
            CALL block(iBlock)%remove()
        ENDIF
        CALL block(iBlock)%printInfo()
    ENDDO
    ALLOCATE(orbPrimitiveMatrix(nXBas,nXOrb))
    orbPrimitiveMatrix=0.d0
 
!CONVERT MONOCENTRIC BASIS FUNCTIONS FROM CONTRACTED TO PRIMITIVE
!PROCEED BLOCK BY BLOCK
    WRITE(6,'(A)') "Convert from Contracted to Primitive"
    ALLOCATE(kList(nRelevantBlock))
    ALLOCATE(lList(nRelevantBlock))
    ALLOCATE(mList(nRelevantBlock))
    offsetP=0
    jBlock=0
    DO iBlock=1,nBlock
        IF (block(iBlock)%removed()) CYCLE            
        k=block(iBlock)%getK()
        l=block(iBlock)%getL()
        m=block(iblock)%getM()
        nBas=block(iBlock)%getNBas()
        offsetC=block(iBlock)%getOffset()
        IF (multiGhost) THEN
            nPrimitive=basis(l,k)%getNPrimitive()
            nContraction=basis(l,k)%getNContraction()
        ELSE
            nPrimitive=basis(0,0)%getNPrimitive()
            nContraction=basis(0,0)%getNContraction()
        ENDIF
        ALLOCATE(contractionMatrix(nPrimitive,nContraction))
        IF (multiGhost) THEN
            CALL basis(l,k)%getContractionMatrix(nPrimitive,nContraction,contractionMatrix)
        ELSE
            CALL basis(0,0)%getContractionMatrix(nPrimitive,nContraction,contractionMatrix)
        ENDIF
        DO iContraction=1+offsetC,nContraction+offsetC
            DO iPrimitive=1+offsetP,nPrimitive+offsetP
                orbPrimitiveMatrix(iPrimitive,:)=orbPrimitiveMatrix(iPrimitive,:)+&
                  orbContractedMatrix(iContraction,nLocOrb+1:nLocOrb+nXOrb)*&
                  contractionMatrix(iPrimitive-offsetP,iContraction-offsetC)
            ENDDO
        ENDDO
        offsetP=offsetP+nPrimitive
        jBlock=jBlock+1
        kList(jBlock)=k
        lList(jBlock)=l
        mList(jBlock)=m
        DEALLOCATE(contractionMatrix)
    ENDDO

!WRITE TO FILE intreface.dat
    bigLMax=lMax+2*kMax
    WRITE(6,'(A)') "WRITE FILE interface.dat"
    OPEN(2,FILE="interface.dat",STATUS="replace")
    WRITE(2,'(I0,x,A)') multiplicity,"multiplicity" 
    WRITE(2,'(I0,x,A)') nLocOrb-nInactive,"active localized Orbitals"
    WRITE(2,'(I0,x,A)') bigLMax,"maximum L"
    WRITE(fmt,'(A,I0,A)') "(I0,x,",nRelevantBlock,"(3(I0,x)))"
    WRITE(2,fmt) nRelevantBlock,(kList(iBlock),lList(iBlock),mList(iBlock),iBlock=1,nRelevantBlock)
    ALLOCATE(exponentVector(nPrimitive))
    CALL basis(0,0)%getExponentVector(nPrimitive,exponentVector)
    WRITE(fmt,'(A,I0,A)') "(I0,x,I0,",nPrimitive,"(x,E25.15e3))"
    DO l=0,bigLMax
       WRITE(2,fmt) l,nPrimitive,(exponentVector(iPrimitive),iPrimitive=1,nPrimitive)
    ENDDO
    WRITE(2,'(I0,x,A)') nXBas,"total number of diffuse basis functions"
    WRITE(2,'(I0,x,A)') nXOrb,"diffused orbitals non zero"
    DO iOrb=1,nXOrb
        WRITE(2,'(500(x,E25.15e3))') (orbPrimitiveMatrix(iBas,iOrb),iBas=1,nXBas)
    ENDDO
END PROGRAM
