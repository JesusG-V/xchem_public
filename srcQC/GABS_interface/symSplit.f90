!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM symSplit
    USE class_irreps
    USE class_inputString
    IMPLICIT none
    !CONTROL VARS
    INTEGER :: stat
    CHARACTER*200 :: inputFile
    LOGICAL :: exists
    !CMD LINE VARS
    CHARACTER*200 :: dataFileCmdLine
    CHARACTER*200 :: createSymmetryFileCmdLine

    !INPUT VARIABLES
    CHARACTER*200 :: orbSymFile
    CHARACTER*200 :: parentSyms
    CHARACTER*200 :: sourceSyms
    CHARACTER*200 :: nParentStatesBySym
    CHARACTER*200 :: nSourceStatesBySym
    CHARACTER*200 :: multiplicityParent
    CHARACTER*200 :: blockInfoFile
    CHARACTER*200 :: dataFile
    CHARACTER*200 :: dataFormat
    LOGICAL :: forceC1

    INTEGER :: irrep1
    INTEGER :: irrep2
    INTEGER :: inactive

    LOGICAL :: createSymmetryFile

    INTEGER :: multiplicityAugmented
    CHARACTER*6 :: irreps(8)
    CHARACTER*6, ALLOCATABLE :: orbSym(:)


    !STRUCTURAL VARIABLES
    CHARACTER*200 :: outFile
    INTEGER :: nSourceStates
    INTEGER :: nParentStates
    INTEGER :: nOrb
    INTEGER :: nIrreps
    CHARACTER*6 :: pntGrp

    TYPE(inputString) :: parentSymsIS
    TYPE(inputString) :: sourceSymsIS
    TYPE(inputString) :: nParentStatesIS
    TYPE(inputString) :: nSourceStatesIS
    TYPE(inputString) :: multiplicityParentIS
    INTEGER, ALLOCATABLE :: parentSymsInt(:)
    INTEGER, ALLOCATABLE :: sourceSymsInt(:)
    INTEGER, ALLOCATABLE :: nParentStatesBySymInt(:)
    INTEGER, ALLOCATABLE :: nSourceStatesBySymInt(:)
    INTEGER, ALLOCATABLE :: multiplicityParentInt(:)
    INTEGER, ALLOCATABLE :: stateID(:)
    INTEGER, ALLOCATABLE :: blockOrder(:,:)

    INTEGER :: nStateBlocks
    INTEGER :: dx1,dx2,dxt1,dxt2
    INTEGER,ALLOCATABLE :: nInd(:,:)
    INTEGER, ALLOCATABLE :: ind(:,:,:)

    REAL*8, ALLOCATABLE :: matrix(:,:)

    TYPE(irrep_) :: s1,s2,s3
    INTEGER :: nState,nStateSym
    !TEMPORARY VARIABLE
    CHARACTER*200 :: fich,fmt
    INTEGER :: iOrb,iIrrep,iInd,iIon,jIon
    INTEGER :: iBlock,jBlock
    INTEGER :: iSource,jSource
    INTEGER :: iState,jState,i,iSym
    INTEGER, ALLOCATABLE :: symTemp(:),multTemp(:)
    CHARACTER*6 :: label
    CHARACTER*4 :: capitalLetters

    !FUNCTIONS
    INTEGER :: label2Index
    CHARACTER*6 :: index2Label

    !DEFINE INPUT NAMELIST
    NAMELIST /INPUT/ orbSymFile,&
      parentSyms,&
      nParentStatesBySym,&
      sourceSyms,&
      nSourceStatesBySym,&
      dataFile,&
      inactive,&
      dataFormat,&
      blockInfoFile,&
      createSymmetryFile,&
      multiplicityAugmented,&
      multiplicityParent,&
      forceC1

    forceC1=.FALSE.
    dataFormat="b"
    createSymmetryFile=.FALSE.
    dataFile=""

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    CALL GETARG(2,dataFileCmdLine)
    IF (LEN(TRIM(dataFileCmdLine)).gt.0) THEN
        dataFile=dataFileCmdLine
        WRITE(6,'(A)') "dataFile command line overwrite. Value = "
        WRITE(6,'(A)') trim(dataFile) 
    ENDIF
    CALL GETARG(3,createSymmetryFileCmdLine)
    IF (LEN(TRIM(createSymmetryFileCmdLine)).gt.0) THEN
        READ(createSymmetryFileCmdLine,*) createSymmetryFile
        WRITE(6,'(A)') "createSymmetryFile command line overwrite. Value = "
        WRITE(6,'(L)') createSymmetryFile
    ENDIF

    CALL parentSymsIS%setString(parentSyms)
    CALL parentSymsIS%process("-")
    CALL parentSymsIS%getArray(symTemp)

    CALL nParentStatesIS%setString(nParentStatesBySym)
    CALL nParentStatesIS%process("-")
    CALL nParentStatesIS%getArray(nParentStatesBySymInt)

    CALL multiplicityParentIS%setString(multiplicityParent)
    CALL multiplicityParentIS%process("-")
    CALL multiplicityParentIS%getArray(multTemp)

    nParentStates=SUM(nParentStatesBySymInt)
    ALLOCATE(parentSymsInt(nParentStates))
    ALLOCATE(multiplicityParentInt(nParentStates))
    ALLOCATE(stateID(nParentStates))
    jState=0
    DO iSym=1,SIZE(symTemp)
        DO iState=1,nParentStatesBySymInt(iSym)
            jState=jState+1
            parentSymsInt(jState)=symTemp(iSym)
            IF (forceC1) parentSymsInt(jState)=1
            multiplicityParentInt(jState)=multTemp(iSym)
            stateID(jState)=iState
            IF (forceC1) stateID(jState)=jState
        ENDDO
    ENDDO
    WRITE(6,'(A,I0)') "Number of parent ion states=",nParentStates
    DEALLOCATE(symTemp,multTemp)

    CALL sourceSymsIS%setString(sourceSyms)
    CALL sourceSymsIS%process("-")
    CALL sourceSymsIS%getArray(symTemp)

    CALL nSourceStatesIS%setString(nSourceStatesBySym)
    CALL nSourceStatesIS%process("-")
    CALL nSourceStatesIS%getArray(nSourceStatesBySymInt)

    nSourceStates=SUM(nSourceStatesBySymInt)
    ALLOCATE(sourceSymsInt(nSourceStates))
    jState=0
    DO iSym=1,SIZE(symTemp)
        DO iState=1,nSourceStatesBySymInt(iSym)
            jState=jState+1
            sourceSymsInt(jState)=symTemp(iSym)
            IF (forceC1) sourceSymsInt(jState)=1
        ENDDO
    ENDDO
    WRITE(6,'(A,I0)') "Number of source states=",nSourceStates
    DEALLOCATE(symTemp)

    IF (forceC1) THEN
        WRITE(6,'(A)') 'Enforcing c1 symmetry. Considering all orbitals to be irrep: "a"'
        nIrreps=1
        pntGrp="c1"
        irreps(1)="a" 
    ELSE
        OPEN(1,file=trim(blockInfoFile),status="old")
        READ(1,*) nIrreps,pntGrp
        READ(1,*) (irreps(iIrrep),iIrrep=1,nIrreps)
        CLOSE(1)
    ENDIF
    WRITE(6,'(3(A),I0,A)') "Working in symmetry ",trim(pntGrp), " with ",nIrreps," irreps"
    WRITE(fmt,'(A,I0,A)') "(",nIrreps,"(A,x))"
    WRITE(6,fmt) (irreps(iIrrep),iIrrep=1,nIrreps)

    OPEN(1,file=trim(orbSymFile),status="old")
    nOrb=0
    DO
        READ(1,*,iostat=stat) iOrb
        IF (IS_IOSTAT_END(stat)) EXIT
        nOrb=nOrb+1
    ENDDO
    REWIND(1)
    WRITE(6,'(A,I0)') "Number of orbitals=",nOrb
    ALLOCATE(orbSym(nOrb))
    IF (forceC1) THEN
        DO iOrb=1,nOrb
            orbSym(iOrb)="a"
        ENDDO
    ELSE
        DO iOrb=1,nOrb
            READ(1,*) orbSym(iOrb) 
        ENDDO
    ENDIF
    CLOSE(1)
    nState=nSourceStates+nParentStates*(nOrb-inactive)
    WRITE(6,'(A,I0)') "Total number of states expected in data files=",nState

    ALLOCATE(matrix(nState,nState))
    WRITE(6,'(A)') "Load Merged Data"
    IF (dataFormat(1:1).eq."b") THEN
        OPEN(1,file=trim(dataFile),status="old",form="unformatted")
        READ(1)
        READ(1) iState
        IF (iState.ne.nState) THEN
            STOP "data file does not contain matrix of expected size"
        ENDIF
        DO iState=1,nState
            READ(1) (matrix(iState,jState),jState=1,nState)
        ENDDO
        CLOSE(1)
    ELSE
        STOP "ERROR Cannot currently handle non-binary data"
    ENDIF

    ALLOCATE(nInd(nIrreps,0:nParentStates))
    nInd=0
    DO irrep1=1,nIrreps
        DO iSource=1,nSourceStates
            IF (sourceSymsInt(iSource).eq.irrep1) nInd(irrep1,0)=nInd(irrep1,0)+1
        ENDDO
        DO iIon=1,nParentStates
            CALL s1%create(index2Label(parentSymsInt(iIon),irreps),pntGrp)
            DO iOrb=inactive+1,nOrb
                CALL s2%create(orbSym(iOrb),pntGrp)
                s3=s1*s2
                IF (label2Index(s3%getLabel(),irreps).eq.irrep1) nInd(irrep1,iIon)=nInd(irrep1,iIon)+1
            ENDDO
        ENDDO
    ENDDO

    ALLOCATE(ind(nIrreps,0:nParentStates,maxval(nInd)))
    ind=0
    nInd=0
    WRITE(fmt,'(A,I0,A)') "(",nParentStates+1,"(I3.3,x))"
    DO irrep1=1,nIrreps
        WRITE(6,'(A,I0)') "Indentify indeces with rows corresponding to irrep ",irrep1
        DO iSource=1,nSourceStates
            IF (sourceSymsInt(iSource).eq.irrep1) THEN
                nInd(irrep1,0)=nInd(irrep1,0)+1
                ind(irrep1,0,nInd(irrep1,0))=iSource
            ENDIF
        ENDDO
        iState=nSourceStates
        DO iIon=1,nParentStates
            CALL s1%create(index2Label(parentSymsInt(iIon),irreps),pntGrp)
            DO iOrb=inactive+1,nOrb
                iState=iState+1
                CALL s2%create(orbSym(iOrb),pntGrp)
                s3=s1*s2
                IF (label2Index(s3%getLabel(),irreps).eq.irrep1) THEN
                    nInd(irrep1,iIon)=nInd(irrep1,iIon)+1
                    ind(irrep1,iIon,nInd(irrep1,iIon))=iState
                ENDIF
            ENDDO
        ENDDO
        WRITE(6,'(A)') "Number of states identified &
            for source states and all possible augmented parent ions:"
        WRITE(6,'(A3,x,A3)') "src","aug"
        WRITE(6,fmt) (nInd(irrep1,iIon),iIon=0,nParentStates)
    ENDDO
   
    nStateBlocks=COUNT(nInd.gt.0)

    capitalLetters="abcd"
    CALL to_lower(pntGrp)
    CALL to_upperSelective(pntGrp,capitalLetters)
    DO irrep1=1,nIrreps
        CALL to_lower(irreps(irrep1))
        CALL to_upperSelective(irreps(irrep1),capitalLetters)
    ENDDO

    IF (createSymmetryFile) THEN
        OPEN(100,file="symmetry",status="replace")
        WRITE(100,'(A)') "# This file indicates how the file labels (*_l1-l2.out)"
        WRITE(100,'(A)') "# correspond to (augmented) states"
        WRITE(100,'(A)') ""
        WRITE(100,'(A)') "# Group"
        WRITE(100,'(A)') trim(pntGrp)     
        WRITE(100,'(A)') ""
        WRITE(100,'(A)') "# Correspondence between parent-ion index and symmetry"
        WRITE(100,'(I3.3)') nParentStates
        DO iIon=1,nParentStates
            WRITE(100,'(I3.3,x,I1,2(A),I0)') &
              iIon,&
              multiplicityParentInt(iIon),&
              trim(index2Label(parentSymsInt(iIon),irreps)),".",&
              stateID(iIon)
        ENDDO
        WRITE(100,'(A)') ""
        WRITE(100,'(A)') "# Correspondence between SRC and total symmetry"
        WRITE(100,'(I3.3)') nStateBlocks
        dx1=-1
        WRITE(100,'(A)') "# Included Source States: "
        WRITE(100,'(A)') "# <file label> <augemnted symmetry> <loc flag> "
        DO irrep1=1,nIrreps 
            IF (nInd(irrep1,0).eq.0) THEN 
                CYCLE
            ELSE    
                dx1=dx1+1      
                WRITE(100,'(I3.3,x,I1.1,A,x,A,x,A,I0)')&
                  dx1,&
                  multiplicityAugmented,&
                  trim(index2label(irrep1,irreps)),&
                  "Loc",&   
                  "# Number of such states: ",& 
                  nInd(irrep1,0)
            ENDIF
        ENDDO
        WRITE(100,'(A)') "# Augmented States: "
        WRITE(100,'(A)') "# <file label> <augemented symmetry> <parent Ion state> "
        DO irrep1=1,nIrreps
            DO iIon=1,nParentStates
                IF (nInd(irrep1,iIon).eq.0) THEN
                    CYCLE
                ELSE
                    dx1=dx1+1
                    WRITE(100,'(I3.3,x,I1.1,A,x,I0,2(A),I1.1,x,A,I0)') dx1,&
                        multiplicityAugmented,&
                        trim(index2label(irrep1,irreps)),&
                        multiplicityParentInt(iIon),&
                        trim(index2label(parentSymsInt(iIon),irreps)),".",&
                        stateID(iIon),&
                        "# Number of such states: ",&
                        nInd(irrep1,iIon)
                ENDIF
            ENDDO
        ENDDO
        CLOSE(100)
    ENDIF

    dataFile=dataFile(1:INDEX(dataFile,".")-1)
    WRITE(6,'(A)') "Establish order of blocks"
    ALLOCATE (blockOrder(nStateBlocks,2))
    dx1=-1
    DO irrep1=1,nIrreps
        IF (nInd(irrep1,0).eq.0) THEN 
            CYCLE
        ELSE    
            dx1=dx1+1      
            blockOrder(dx1+1,1)=irrep1
            blockOrder(dx1+1,2)=0
        ENDIF
    ENDDO
    DO irrep1=1,nIrreps
        DO iIon=1,nParentStates
            IF (nInd(irrep1,iIon).eq.0) THEN
                CYCLE
            ELSE
                dx1=dx1+1
                blockOrder(dx1+1,1)=irrep1
                blockOrder(dx1+1,2)=iIon
            ENDIF
        ENDDO
    ENDDO

    DO iBlock=1,nStateBlocks
        dx1=iBlock-1
        iIon=blockOrder(iBlock,2)
        irrep1=blockOrder(iBlock,1)
        DO jBlock=1,nStateBlocks
            dx2=jBlock-1
            jIon=blockOrder(jBlock,2)
            irrep2=blockOrder(jBlock,1)
            WRITE(outFile,'(A,I3.3,A,I3.3,A)') trim(dataFile)//"_",dx1,"-",dx2,".out"
            OPEN(1,file=trim(outFile),status="replace")
            WRITE(fmt,'(A,I0,A)') "(",nInd(irrep2,jIon),"(E30.20e3))"
            WRITE(1,'(I0,x,I0)') nInd(irrep1,iIon),nInd(irrep2,jIon)
            !WRITE(6,'(A)') ""
            !WRITE(6,'(A,2(x,I0))') "DEBUG dx",dx1,dx2
            !WRITE(6,'(A,2(x,I0))') "DEBUG irrep1,ion1",irrep1,iIon
            !WRITE(6,'(A,2(x,I0))') "DEBUG irrep2,ion2",irrep2,jIon
            !WRITE(6,'(A,2(x,I0))') "DEBUG nInd1,nInd2",nInd(irrep1,iIon),nInd(irrep2,jIon)
            DO iState=1,nInd(irrep1,iIon)
                WRITE(1,fmt) (matrix(ind(irrep2,jIon,jState),&
                  ind(irrep1,iIon,iState)),jState=1,nInd(irrep2,jIon))
            ENDDO
            CLOSE(1)
        ENDDO
    ENDDO

    !DO irrep1=1,nIrreps
    !    DO iIon=0,nParentStates
    !        IF (nInd(irrep1,iIon).eq.0) THEN
    !            CYCLE
    !        ELSE
    !            dx1=dx1+1
    !        ENDIF
    !        dx2=-1
    !        DO irrep2=1,nIrreps
    !            DO jIon=0,nParentStates
    !                IF (nInd(irrep2,jIon).eq.0) THEN
    !                    CYCLE
    !                ELSE
    !                    dx2=dx2+1
    !                ENDIF
    !                IF (dx1.gt.dx2) CYCLE
    !                WRITE(outFile,'(A,I3.3,A,I3.3,A)') trim(dataFile)//"_",dx1,"-",dx2,".out"
    !                OPEN(1,file=trim(outFile),status="replace")
    !                WRITE(fmt,'(A,I0,A)') "(",nInd(irrep2,jIon),"(E30.20e3))"
    !                WRITE(1,'(I0,x,I0)') nInd(irrep1,iIon),nInd(irrep2,jIon)
    !                DO iState=1,nInd(irrep1,iIon)
    !                    WRITE(1,fmt) (matrix(ind(irrep2,jIon,jState),&
    !                      ind(irrep1,iIon,iState)),jState=1,nInd(irrep2,jIon))
    !                ENDDO
    !                CLOSE(1)
    !            ENDDO
    !        ENDDO
    !    ENDDO
    !ENDDO
END PROGRAM

FUNCTION label2Index(label,irreps)
    IMPLICIT none
    CHARACTER*6, INTENT(in) :: label
    CHARACTER*6, INTENT(in) :: irreps(8)
    INTEGER :: label2Index
    INTEGER :: i
    DO i=1,8
        IF (irreps(i).eq.label) label2Index=i
    ENDDO
END FUNCTION

FUNCTION index2Label(ind,irreps)
    IMPLICIT none
    INTEGER, INTENT(in) :: ind
    CHARACTER*6, INTENT(in) :: irreps(8)
    CHARACTER*6 :: index2Label
    index2Label=irreps(ind)
END FUNCTION
