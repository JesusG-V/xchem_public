!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM GABS_interface_sym
!This code reads the basis sets for the ghost atom(s) and a set of 
!molecular orbitals. From this the interface with the XCHEM code is 
!generated containing the monocentric orbitals (expressed in primitives
!rather tha contracted basis function) as well as iformation about the 
!angular momenta.

!if working with one ghost atom for all ang moms specify the name of the file 
!with basis functions with the input basisFile. Otherwise the basis functions
!for the different angular moment code will look for files: l.$l_k.$k.basisFile with
!$l and $k being the angular momenta.
    USE basisIO
    USE orbitalIO
    USE blockIO
    USE class_inputString
    USE lkIO
    IMPLICIT none
!CONTRAL VAR
    CHARACTER*200 :: inputFile
    INTEGER :: iostat
    LOGICAL :: exist
    CHARACTER*2000 :: fmt
    
!CMD LINE VARS
    CHARACTER*200 :: irrepCmdLine
!INPUT VARS
    INTEGER :: multiplicity
    INTEGER :: lMax,kMax
    INTEGER :: nLocOrb,nLocBas,nOrb,nOrbAllSym
    INTEGER :: nInactive
    INTEGER :: irrep
    LOGICAL :: multiGhost,forceC1, twoexp
    CHARACTER*200 :: basisFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: blockInfoFile
    CHARACTER*200 :: orbSymFile
    CHARACTER*200 :: lkMonocentricFile

!STURCTURAL VARS
    INTEGER :: bigLMax
    INTEGER :: nXOrb,nXBas,nBas
    INTEGER :: nBlock,nRelevantBlock
    INTEGER :: irrepI,nIrreps
    INTEGER,ALLOCATABLE :: kList(:)
    INTEGER,ALLOCATABLE :: lList(:)
    INTEGER,ALLOCATABLE :: mList(:)
    REAL*8,ALLOCATABLE :: exponentVector(:)
    REAL*8,ALLOCATABLE :: orbPrimitiveMatrix(:,:)
    REAL*8,ALLOCATABLE :: orbContractedMatrix(:,:)
    REAL*8,ALLOCATABLE :: contractionMatrix(:,:)
    CHARACTER*6 :: pntGrp
    CHARACTER*6 :: irrepC
    CHARACTER*6 :: irreps(8)
    CHARACTER*6, ALLOCATABLE :: orbSym(:)
    TYPE(orbital_) :: orb
    TYPE(basis_), ALLOCATABLE :: basis(:,:)
    TYPE(block_), ALLOCATABLE :: block(:)
    TYPE(block_), ALLOCATABLE :: temp_block(:)
    CHARACTER*6 :: index2Label
    TYPE(lk_) :: lkPairs

!TEMP VARS
    REAL*8, ALLOCATABLE :: tempRealArr(:,:)
    CHARACTER*100 :: fich
    INTEGER :: iGhost
    INTEGER :: iIrrep
    INTEGER :: iBlock,jBlock
    INTEGER :: l,k,m
    INTEGER :: iOrb,iBas,iExp
    INTEGER :: nPrimitive,nContraction
    INTEGER :: iPrimitive,iContraction
    INTEGER :: offsetC,offsetP,iSym
    INTEGER :: nBlockNew
    CHARACTER*50 :: label
    CHARACTER*4 :: capitalLetters
    LOGICAL :: relevant

    NAMELIST /INPUT/ lMax,&
      kMax,&
      multiGhost,&
      blockInfoFile,&
      basisFile,&
      orbitalFile,&
      nLocOrb,&
      nLocBas,&
      multiplicity,&
      nInactive,&
      orbSymFile,&
      irrep,&
      forceC1,&
      twoexp,&
      lkMonocentricFile

    forceC1=.FALSE.
    multiGhost=.FALSE.
    twoexp=.FALSE.
    nXBas=0
    nLocOrb=0
    nLocBas=0
    nInactive=0
    irrep=-1
    lkMonocentricFile=""
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exist)
    IF (.NOT.exist) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=iostat)
    IF (iostat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT)
    !READ(1,NML=INPUT,IOSTAT=iostat)
    !IF (iostat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    CALL GETARG(2,irrepCmdLine) 
    IF (LEN(TRIM(irrepCmdLine)).gt.0) THEN
        READ(irrepCmdLine,*) irrep
        WRITE(6,'(A)') "irrep command line overwrite. Value = "
        WRITE(6,'(I0)') irrep
    ENDIF

    WRITE(6,'(A)') "RESTRICTION INFO"
    IF (len(trim(lkMonocentricFile)).gt.0) THEN 
        WRITE(6,'(A)') "Read from file"
        call lkPairs%create(trim(lkMonocentricFile))
        lMax = lkPairs%getMaxL()
    ELSE
        IF (lMax.lt.0) STOP "negative lMax make no sense"
        IF (kMax.lt.0) STOP "negative kMax make no sense"
        WRITE(6,'(A,I3,2x,A,I3)') "lMax=",lMax,"kMax=",kMax
        call lkPairs%create(lMax,kMax)
    ENDIF
    WRITE(6,'(A)') " "

!READ NUMBER OF ORBITALS AND THEIR SYMMETRY
    OPEN(1,file=trim(orbSymFile),status="old")
    nOrbAllSym=0
    DO
        READ(1,*,iostat=iostat) iOrb
        IF (IS_IOSTAT_END(iostat)) EXIT
        nOrbAllSym=nOrbAllSym+1
    ENDDO
    REWIND(1)
    WRITE(6,'(A,I0)') "Number of orbitals (all symmetries)=",nOrbAllSym
    ALLOCATE(orbSym(nOrbAllSym))
    DO iOrb=1,nOrbAllSym
        READ(1,*) orbSym(iOrb) 
        IF (forceC1) orbSym(iOrb)="a"
    ENDDO
    CLOSE(1)

!READ ORBITALS
    WRITE(6,'(A)') "Read orbitals"
    CALL orb%readOrbital(TRIM(orbitalFile),"b")
    CALL orb%getOrbital(tempRealArr)
    CALL orb%getNOrb(nBas)
    ALLOCATE(orbContractedMatrix(nBas,nBas))
    orbContractedMatrix=0.d0

!READ BASIS SETS
    WRITE(6,'(A)') "Read basis sets"
    IF (multiGhost) THEN
        ALLOCATE(basis(0:lMax,0:lkPairs%getMaxK()))
        DO l=0,lMax
            DO k=0,lkPairs%getK(l)
                WRITE(fich,'(A2,I1,A3,I1,A1,A)') "l.",l,"_k.",k,".",TRIM(basisFile)
                CALL basis(l,k)%create(TRIM(fich))
            ENDDO
        ENDDO
    ELSE
        ALLOCATE(basis(0:0,0:0)) 
        CALL basis(0,0)%create(TRIM(basisFile))
        !DEBUG
        nPrimitive=basis(0,0)%getNPrimitive()
        !write(6,'(A,i0)') 'DEBUG nPrimiteive', nPrimitive
        !END DEBUG
    ENDIF

!READ INFORMATION ABOUT SETS (BLOCKS) OF BASIS FUNCTIONS CORRESPONDING
!TO THE SAME k/l/ghost
    nBas=nLocBas
    WRITE(6,'(A)') "Read information about l/k-block"
    OPEN(1,FILE=TRIM(blockInfoFile),STATUS="old")
    !READ(1,*);READ(1,*);READ(1,*)
    READ(1,*) nIrreps,pntGrp
    READ(1,*) (irreps(iIrrep),iIrrep=1,nIrreps)
    IF (forceC1) THEN
        nIrreps=1
        pntGrp="c1"
        irreps(1)="a"
    ENDIF
    irrepI=irrep
    IF (forceC1) irrepI=1
    irrepC=index2Label(irrepI,irreps)
    READ(1,*)
    READ(1,*) nBlock
    ALLOCATE(block(nBlock))
    iGhost=0
    nRelevantBlock=0
    DO iBlock=1,nBlock
        IF (multiGhost) THEN
            READ(1,*) iBas,label,l,m,k,iGhost,iSym
        ELSE
            READ(1,*) iBas,label,l,m,k,iGhost,iSym
        ENDIF
        CALL block(iBlock)%create(k,l,m,iBas,nBas,label,iSym,iGhost)
        nBas=nBas+iBas

        relevant=.TRUE.
        !IF (l.gt.lMax.OR.k.gt.kMax) relevant=.FALSE.  
        IF (.not.lkPairs%is_lk(l,k)) relevant=.FALSE.
        IF (multiGhost) THEN
            IF (iGhost.ne.l) THEN
                relevant=.FALSE.
            ENDIF
        ENDIF
        IF (.not.forceC1) THEN
            IF (iSym.ne.irrepI) THEN
                relevant=.FALSE.
            ENDIF
        ENDIF
        IF (relevant) THEN
            IF (multiGhost) THEN
                nPrimitive=basis(l,k)%getNPrimitive()
            ELSE
                nPrimitive=basis(0,0)%getNPrimitive()
            ENDIF
            nXBas=nXBas+nPrimitive
            nRelevantBlock=nRelevantBlock+1
        ELSE
            CALL block(iBlock)%remove()
        ENDIF
        CALL block(iBlock)%printInfo()
    ENDDO

!REMOVE ORBITAL OF INCORRECT SYMMETRY
    IF (forceC1) THEN
        nXOrb=0
        DO iOrb=nLocOrb + 1,nOrbAllSym
            nXOrb=nXOrb+1
            orbContractedMatrix(:,nXOrb)=tempRealArr(:,iOrb)
        ENDDO
    ELSE
        nXOrb=0
        DO iOrb=nLocOrb + 1,nOrbAllSym
            IF (trim(orbSym(iOrb)).eq.trim(irrepC)) THEN
                nXOrb=nXOrb+1
                orbContractedMatrix(:,nXOrb)=tempRealArr(:,iOrb)
            ENDIF
        ENDDO
    ENDIF
    DEALLOCATE(tempRealArr)
    WRITE(6,'(3(A),I0)') "Number of orbitals of symmetry (excluding local orbitals)",trim(irrepC)," =",nXOrb
    !nXOrb=nOrb-nLocOrb

!REORDER COEFFICIENTS IF TWOEXP METHODOLOGY IS USED
    IF (twoexp) THEN
        WRITE(6,'(A)') "Two Exp keyword found. Reorder Coefficientes:"
        IF (multiGhost) THEN
            STOP "Both twoexp and multiGhost are set True. This should not happen!"
        ENDIF
        ALLOCATE(temp_block(nBlock))
        ! Create reference copy of block structure
        DO iBlock=1,nBlock
            temp_block(iBlock) = block(iBlock)
        ENDDO
        nBlockNew = 0
        DO iBlock=1,nBlock    
            IF (block(iBlock)%getGhost() .ne. 1) THEN
                EXIT
            ENDIF
            nBlockNew = nBlockNew +1
        ENDDO
        IF (nBlockNew*basis(0,0)%getNPrimitive() .ne. nBlock) THEN
            STOP "Invalid combination of basis and block info file"
        ENDIF
        DEALLOCATE(block)
        ALLOCATE(block(nBlockNew))
        ALLOCATE(tempRealArr(nBas,nBas))
        tempRealArr = orbContractedMatrix
        iBas=0
        WRITE(6,'(A)') "New Block Structure:"
        nRelevantBlock=0
        nXBas = 0
        DO iBlock = 1,nBlockNew
            block(iBlock) = temp_block(iBlock)
            CALL block(iBlock)%setNBas( basis(0,0)%getNPrimitive() )
            IF (.NOT. (block(iBlock)%removed()) ) THEN
                nXBas = nXBas + basis(0,0)%getNPrimitive()
                nRelevantBlock = nRelevantBlock + 1
            ENDIF
            CALL block(iBlock)%setOffset( nLocBas + basis(0,0)%getNPrimitive()*(iBlock-1) ) 
            CALL block(iBlock)%printInfo()
            DO iExp = 1,basis(0,0)%getNPrimitive()
                iBas = iBas+1
!                orbContractedMatrix(nLocBas + iBas,:) = tempRealArr(nLocBas + (iExp-1)*basis(0,0)%getNPrimitive() + iBlock,:)
                orbContractedMatrix(nLocBas + iBas,:) = tempRealArr(nLocBas + (iExp-1)*nBlockNew + iBlock,:)
            ENDDO
        ENDDO
        nBlock = nBlockNew
        DEALLOCATE(tempRealArr)
        DEALLOCATE(temp_block)
    ENDIF
   
    IF (nXOrb.gt.0) THEN
        ALLOCATE(orbPrimitiveMatrix(nXBas,nXOrb))
        orbPrimitiveMatrix=0.d0
 
!CONVERT MONOCENTRIC BASIS FUNCTIONS FROM CONTRACTED TO PRIMITIVE
!PROCEED BLOCK BY BLOCK
        WRITE(6,'(A)') "Convert from Contracted to Primitive"
        ALLOCATE(kList(nRelevantBlock))
        ALLOCATE(lList(nRelevantBlock))
        ALLOCATE(mList(nRelevantBlock))
        offsetP=0
        jBlock=0
        DO iBlock=1,nBlock
            IF (block(iBlock)%removed()) CYCLE            
            k=block(iBlock)%getK()
            l=block(iBlock)%getL()
            m=block(iblock)%getM()
            nBas=block(iBlock)%getNBas()
            offsetC=block(iBlock)%getOffset()
            IF (multiGhost) THEN
                nPrimitive=basis(l,k)%getNPrimitive()
                nContraction=basis(l,k)%getNContraction()
            ELSE
                nPrimitive=basis(0,0)%getNPrimitive()
                nContraction=basis(0,0)%getNContraction()
            ENDIF
            ALLOCATE(contractionMatrix(nPrimitive,nContraction))
            IF (multiGhost) THEN
                CALL basis(l,k)%getContractionMatrix(nPrimitive,nContraction,contractionMatrix)
            ELSE
                CALL basis(0,0)%getContractionMatrix(nPrimitive,nContraction,contractionMatrix)
            ENDIF
            DO iContraction=1+offsetC,nContraction+offsetC
                DO iPrimitive=1+offsetP,nPrimitive+offsetP
                    orbPrimitiveMatrix(iPrimitive,:)=orbPrimitiveMatrix(iPrimitive,:)+&
                      orbContractedMatrix(iContraction,1:nXOrb)*&
                      contractionMatrix(iPrimitive-offsetP,iContraction-offsetC)
                ENDDO
            ENDDO
            offsetP=offsetP+nPrimitive
            jBlock=jBlock+1
            kList(jBlock)=k
            lList(jBlock)=l
            mList(jBlock)=m
            DEALLOCATE(contractionMatrix)
        ENDDO

!WRITE TO FILE intreface.dat
        bigLMax=lkPairs%getBigL()
        CALL to_lower(irrepC)
        capitalLetters="abcd"
        CALL to_upperSelective(irrepC,capitalLetters)
        WRITE(6,'(A)') "WRITE FILE interface"//trim(irrepC)
        OPEN(2,FILE="interface"//trim(irrepC),STATUS="replace")
        WRITE(2,'(I0,x,A)') multiplicity,"multiplicity" 
        WRITE(2,'(I0,x,A)') nLocOrb-nInactive,"active localized Orbitals"
        WRITE(2,'(I0,x,A)') bigLMax,"maximum L"
        WRITE(fmt,'(A,I0,A)') "(I0,x,",nRelevantBlock,"(3(I0,x)))"
        WRITE(2,fmt) nRelevantBlock,(kList(iBlock),lList(iBlock),mList(iBlock),iBlock=1,nRelevantBlock)
        ALLOCATE(exponentVector(nPrimitive))
        CALL basis(0,0)%getExponentVector(nPrimitive,exponentVector)
        WRITE(fmt,'(A,I0,A)') "(I0,x,I0,",nPrimitive,"(x,E25.15e3))"
        DO l=0,bigLMax
           WRITE(2,fmt) l,nPrimitive,(exponentVector(iPrimitive),iPrimitive=1,nPrimitive)
        ENDDO
        WRITE(2,'(I0,x,A)') nXBas,"total number of diffuse basis functions"
        WRITE(2,'(I0,x,A)') nXOrb,"diffuse orbitals non zero"
        DO iOrb=1,nXOrb
            WRITE(2,'(500(x,E25.15e3))') (orbPrimitiveMatrix(iBas,iOrb),iBas=1,nXBas)
        ENDDO
    ELSE
        WRITE(6,'(A)') "No orbitals of this symmety. No interface was produced"
    ENDIF
END PROGRAM
FUNCTION index2Label(ind,irreps)
    IMPLICIT none
    INTEGER, INTENT(in) :: ind
    CHARACTER*6, INTENT(in) :: irreps(8)
    CHARACTER*6 :: index2Label
    index2Label=irreps(ind)
END FUNCTION
