!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM orthoNormalize
    USE orbitalIO
    USE oneIntIO
    USE orbitalToolKit
    IMPLICIT none
    CHARACTER*200 :: inputFile
    LOGICAL :: exists
    INTEGER :: stat
    
    CHARACTER*200 :: orbitalFile       
    CHARACTER*200 :: ovFile
    CHARACTER*200 :: linIndepOrbitalFile   
   
    TYPE(orbital_) :: orbIn
    TYPE(orbital_) :: orbOut
    TYPE(oneInt_) :: ov

    INTEGER :: nOrb       
    INTEGER :: nBas
    INTEGER :: nSym
    INTEGER, ALLOCATABLE :: symSize(:)
    REAL*8, ALLOCATABLE :: orbVec(:)
    REAL*8, ALLOCATABLE :: orbMat(:,:)
    REAL*8, ALLOCATABLE :: ovBasMat(:,:)
    REAL*8, ALLOCATABLE :: ovOrbMat(:,:)

    INTEGER :: iSym
    INTEGER :: iBas
    INTEGER :: iOrb

    NAMELIST /INPUT/ orbitalFile,&
      ovFile,&
      linIndepOrbitalFile
       
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
  
    CALL ov%readOneInt(ovFile,"b")
    CALL ov%getNSym(nSym)
    CALL ov%getSymSize(symSize)

    WRITE(6,'(A,I0)') "Number of symmetries= ",nSym

    CALL orbIn%readOrbital(orbitalFile,"b")
    CALL orbOut%initializeEmpty(symSize)

    DO iSym=1,nSym
        WRITE(6,'(A,I0)') "Symmetry: ",iSym
        nBas=symSize(iSym)
        nOrb=nBas
        ALLOCATE(orbMat(nBas,nOrb))
        ALLOCATE(ovOrbMat(nOrb,nOrb))
        orbMat=0.d0
        ovBasMat=0.d0
        DO iOrb=1,nOrb
            CALL orbIn%getOrbital(orbVec,iOrb,iSym)
            orbMat(:,iOrb)=orbVec
        ENDDO
        CALL ov%getSym(iSym,ovBasMat)
        WRITE(6,'(A)') "Overlap between Orbitals before GramSchmidt"
        CALL overlaps(nOrb,nBas,orbMat,ovOrbMat,ovBasMat)
        CALL gramSchmidt(nOrb,nBas,orbMat,ovBasMat)
        WRITE(6,'(A)') "Overlap between Orbitals after GramSchmidt"
        CALL overlaps(nOrb,nBas,orbMat,ovOrbMat,ovBasMat)
        CALL orbOut%setSym(iSym,orbMat)
        DEALLOCATE(orbMat)
        DEALLOCATE(ovOrbMat)
    ENDDO
    CALL orbOut%writeOrbital(TRIM(linIndepOrbitalFile)//".RasOrb","f")
    CALL orbOut%writeOrbital(TRIM(linIndepOrbitalFile)//".bin","b")
END PROGRAM
