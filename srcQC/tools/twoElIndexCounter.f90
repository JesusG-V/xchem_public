!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoElIndexCounter
    IMPLICIT none
    INTEGER :: i,j,k,l,lstart
    INTEGER :: n
    INTEGER :: counter
    n=10
    counter=0
    DO i=1,n
        DO j=i,n
            DO k=i,n
                IF (i.eq.k) THEN
                    lstart=MAX(j,k)
                ELSE
                    lstart=k
                ENDIF
                DO l=lstart,n
                    counter=counter+1
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    WRITE(6,'(A,x,I0)') "counter=",counter
END PROGRAM
