!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM orbitalSupSym
    USE orbitalIO
    IMPLICIT none

    TYPE orbGroup_
        INTEGER :: sym,orb
        REAL*8, ALLOCATABLE :: coef(:)
    END TYPE

    CHARACTER*200 :: inputFile,token
    LOGICAL :: exists
    INTEGER :: stat

    TYPE (orbital_) :: orb
    CHARACTER*200 :: orbFile
    CHARACTER*200 :: supOrbFile
    INTEGER :: orbGroups
    INTEGER, ALLOCATABLE :: symSize(:)

    INTEGER :: iSym,iOrb,jOrb,iGroup,nOrb,iBas

    INTEGER, ALLOCATABLE :: pos(:)
    REAL*8 :: val,newVal,temp
    REAL*8, ALLOCATABLE :: refOrb(:)

    TYPE(orbGroup_),ALLOCATABLE :: orbSup(:)



    NAMELIST /INPUT/ orbFile,&
        orbGroups,&
        supOrbFile

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(100,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(100,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    REWIND(100)
    DO 
        READ(100,'(A)') token
        print*, TRIM(token)
        IF (INDEX(token,"END").gt.0) EXIT
    ENDDO

    CALL orb%readOrbital(TRIM(orbFile),"f","pro")
    CALL orb%writeOrbital("in.RasOrb","f")
    CALL orb%getSymSize(symSize)
   
    print*, "symSize",symSize

    DO iGroup=1,orbGroups
        READ(100,*)  nOrb
        ALLOCATE(orbSup(nOrb))
        ALLOCATE(pos(nOrb))
        DO iOrb=1,nOrb
            READ(100,*) iSym,jOrb
            orbSup(iOrb)%sym=iSym
            orbSup(iOrb)%orb=jOrb
            CALL orb%getOrbital(orbSup(iOrb)%coef,jOrb,iSym)
        ENDDO
        ALLOCATE(refOrb(SIZE(orbSup(1)%coef)))
        refOrb=orbSup(1)%coef
        DO         
            pos(1)=MAXLOC(ABS(refOrb),1)
            val=refOrb(pos(1))
            refOrb(pos(1))=0.d0
            newVal=val
            IF (ABS(val).lt.1e-20) EXIT
            WRITE(6,'(A,F12.8)') 'val',val
            DO iOrb=2,nOrb
                temp=1e20
                DO iBas=1,SIZE(orbSup(iOrb)%coef)
                    IF (ABS(val-orbSup(iOrb)%coef(iBas)).lt.temp) THEN
                        temp=ABS(val-orbSup(iOrb)%coef(iBas))
                        pos(iOrb)=iBas
                    ENDIF
                ENDDO
                WRITE(6,'(A,F12.8)') 'val',orbSup(iOrb)%coef(pos(iOrb))
                newVal=newVal+orbSup(iOrb)%coef(pos(iOrb))
            ENDDO
            newVal=newVal/nOrb
            WRITE(6,'(A,F12.8)') 'newval',newval
            DO iOrb=1,nOrb
                orbSup(iOrb)%coef(pos(iOrb))=newVal
            ENDDO
        ENDDO
        DO iOrb=1,nOrb
            CALL orb%setOrbital(orbSup(iOrb)%coef,orbSup(iOrb)%orb,orbSup(iOrb)%sym)
        ENDDO
        DEALLOCATE(orbSup,pos,refOrb)
    ENDDO
    CALL orb%writeOrbital(TRIM(supOrbFile),"f","pro")
    CALL orb%writeOrbital("out.RasOrb","f")

END PROGRAM     

    
