!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM orbitalTest
    use oneIntIO
    use orbitalIO
    use diagonalizations
    IMPLICIT none
    LOGICAL :: exists
    INTEGER :: stat

    TYPE(oneInt_) :: basOverlap
    TYPE(oneInt_) :: orbOverlap
    TYPE(orbital_) :: orbital

    CHARACTER*200 :: inputFile
    CHARACTER*200 :: basOverlapFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: orbOverlapFile

    REAL*8,ALLOCATABLE :: orbitalMat(:,:)
    REAL*8,ALLOCATABLE :: basOvMat(:,:)
    REAL*8,ALLOCATABLE :: orbitalVec(:)

    REAL*8,ALLOCATABLE :: orbOv(:,:)
    REAL*8,ALLOCATABLE :: eVal(:)
    
    INTEGER :: nOrb,iOrb,jOrb
    INTEGER :: nBas,iBas,jBas

    NAMELIST /INPUT/ basOverlapFile,&
      orbitalFile,&
      orbOverlapFile,&
      nOrb

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)

    CALL orbital%readOrbital(TRIM(orbitalFile),"b")
    CALL orbital%getSymSize(nBas)
    ALLOCATE(orbitalMat(nBas,nOrb))
    orbitalMat=0.d0

    DO iOrb=1,nOrb
        CALL orbital%getOrbital(orbitalVec,iOrb)
        orbitalMat(:,iOrb)=orbitalVec
    ENDDO

    CALL basOverlap%readOneIntAllData(TRIM(basOverlapFile),"b")
    CALL basOverlap%getSym(basOvMat)

    ALLOCATE(orbOv(nOrb,nOrb)) 

    orbOv=0.d0
    DO iOrb=1,nOrb
        DO jOrb=1,nOrb
            DO iBas=1,nBas
                DO jBas=1,nBas
                    !IF (iOrb.eq.29.and.jOrb.eq.1) THEN
                    !    WRITE(6,'(A,2(x,I5),4(x,E18.12))') "DBEUG ",iOrb,jOrb,&
                    !      orbitalMat(iBas,iOrb),orbitalMat(jBas,jOrb),basOvMat(iBas,jBas),&
                    !      orbOv(iOrb,jOrb)
                    !ENDIF
                    orbOv(iOrb,jOrb)=orbOv(iOrb,jOrb)+orbitalMat(iBas,iOrb)*orbitalMat(jBas,jOrb)*basOvMat(iBas,jBas)
                ENDDO
            ENDDO
        ENDDO
    ENDDO

    CALL orbOverlap%initializeEmpty(nOrb)
    CALL orbOverlap%setSym(orbOv)
    CALL orbOverlap%writeOneInt(TRIM(orbOverlapFile),"f") 
    ALLOCATE(eVal(nOrb))
    CALL diag(orbOv,nOrb,eVal) 
    WRITE(6,'(<nOrb>(x,F18.12))') (eVal(iOrb),iOrb=1,nOrb)
END PROGRAM
