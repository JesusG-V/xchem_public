!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE oneIntIO
!CLASS FOR OBJECTS THAT CONTAIN AND MANIPULATE
!ONE ELECTRON INTEGRALS. CAN PRINT AND READ IN
!binary OR formatted AND list OR matrix FORM
    USE diagonalizations
    IMPLICIT none
    PRIVATE
    TYPE mat
        REAL*8,ALLOCATABLE :: coef(:,:)
        REAL*8,ALLOCATABLE :: eVect(:,:)
        REAL*8,ALLOCATABLE :: eVal(:)
    END TYPE
    PUBLIC oneInt_
    TYPE oneInt_
        PRIVATE
        INTEGER :: nSym
        INTEGER, ALLOCATABLE :: symSize(:)
        TYPE(mat), ALLOCATABLE :: sym(:)
        LOGICAL :: alive=.FALSE.
        LOGICAL :: bin=.FALSE.
        INTEGER :: un=0
        
        CONTAINS
            PROCEDURE :: writeOneInt
            PROCEDURE :: getNSym
            PROCEDURE :: getSymSizeMultiSym
            PROCEDURE :: getSymSizeSingleSym
            PROCEDURE :: getValSingleSym
            PROCEDURE :: getValMultiSym
            PROCEDURE :: getSymSingleSym
            PROCEDURE :: getSymMultiSym
            PROCEDURE :: setSymSingleSym
            PROCEDURE :: setSymMultiSym
            PROCEDURE :: initializeEmptySingleSym
            PROCEDURE :: initializeEmptyMultiSym
            PROCEDURE :: destroy
            PROCEDURE :: diagonalize
            PROCEDURE :: getEVect
            PROCEDURE :: getEVal
            PROCEDURE :: getLinIndep
            PROCEDURE :: getRecord
            PROCEDURE :: readOneIntLine
            PROCEDURE :: readOneIntAllData
            GENERIC :: readOneInt => readOneIntLine,readOneIntAllData
            GENERIC :: setSym => setSymSingleSym,setSymMultiSym
            GENERIC :: getSym => getSymSingleSym,getSymMultiSym
            GENERIC :: getVal => getValSingleSym,getValMultiSym
            GENERIC  :: initializeEmpty => initializeEmptySingleSym,initializeEmptyMultiSym
            GENERIC :: getSymSize => getSymSizeSingleSym,getSymSizeMultiSym
    END TYPE
            
    
    CONTAINS
        SUBROUTINE readOneIntLine(this,fich,sorte,un)
            IMPLICIT none
            CLASS(oneInt_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            INTEGER,INTENT(in) :: un
            LOGICAL :: bin
            INTEGER :: nbas
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            this%un=un
            this%bin=bin
            WRITE(6,'(A)') "WARNING: specified unit will remain open in reading integral by record"
            IF (bin) THEN
                OPEN(un,file=fich,FORM="unformatted",STATUS="old")
                READ(un) this%nSym
                ALLOCATE(this%symSize(this%nSym)) 
                ALLOCATE(this%sym(this%nSym))
                READ(un) this%symSize(1)
                nbas=this%symSize(1)
                ALLOCATE(this%sym(1)%coef(nBas,nBas))
            ELSE
                OPEN(un,file=fich,STATUS="old")
                READ(un,*) this%nSym
                ALLOCATE(this%symSize(this%nSym)) 
                ALLOCATE(this%sym(this%nSym))
                READ(un,*) this%symSize(1)
                nbas=this%symSize(1)
                ALLOCATE(this%sym(1)%coef(nBas,nBas))
            ENDIF
            this%alive=.TRUE.
        END SUBROUTINE
        SUBROUTINE readOneIntAllData(this,fich,sorte,arrayIn,unIn,hermitianIn)
            IMPLICIT none
            CLASS(oneInt_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            LOGICAL,INTENT(in), OPTIONAL :: arrayIn
            INTEGER, INTENT(in), OPTIONAL :: unIn
            CHARACTER*1, INTENT(in), OPTIONAL :: hermitianIn
            CHARACTER*1 :: hermitian 
            LOGICAL :: bin,array
            INTEGER :: iSym,nBas,stat,jBas,iBas,un
            CHARACTER*200 :: token
            REAL*8 :: val
            array=.TRUE.
            un=1
            hermitian="h"
            IF (present(hermitianIn)) hermitian=hermitianIn
            IF (present(arrayIn)) array=arrayIn
            IF (present(unIn)) un=unIn
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
                WRITE(6,'(A,A,A,I3)') "Opening binary one electron integral file: ",TRIM(fich)," in unit ",un
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
                WRITE(6,'(A,A,A,I3)') "Opening formattted one electron integral file: ",TRIM(fich)," in unit ",un
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            IF (bin) THEN
                OPEN(un,file=fich,FORM="unformatted",STATUS="old")
                IF (ALLOCATED(this%symSize)) DEALLOCATE(this%symSize)
                IF (ALLOCATED(this%sym)) DEALLOCATE(this%sym)
                IF (array) THEN
                    IF (hermitian.eq."h") THEN
                        WRITE(6,'(A)') "Reading hermitian matrix"
                    ELSEIF (hermitian.eq."a") THEN
                        WRITE(6,'(A)') "Reading anti hermitian matrix"
                    ELSEIF (hermitian.eq."o") THEN
                        WRITE(6,'(A)') "Reading square matrix"
                    ELSE
                        STOP "ERROR: unknwon keyword for hermitian"
                    ENDIF
                    READ(un) this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    DO iSym=1,this%nSym
                        READ(un) this%symSize(iSym)
                        print*,this%symSize(iSym)
                        nBas=this%symSize(iSym)
                        ALLOCATE(this%sym(iSym)%coef(nBas,nBas))
                        DO iBas=1,this%symSize(iSym)
                            IF (hermitian.eq."o") THEN
                                READ(un) (this%sym(iSym)%coef(jBas,iBas),jBas=1,nBas)
                            ELSE
                                READ(un) (this%sym(iSym)%coef(jBas,iBas),jBas=1,iBas)
                            ENDIF
                            IF (hermitian.eq."h") THEN
                                DO jBas=1,iBas
                                    this%sym(iSym)%coef(iBas,jBas)=this%sym(iSym)%coef(jBas,iBas)
                                ENDDO
                            ELSEIF (hermitian.eq."a") THEN 
                                DO jBas=1,iBas
                                    this%sym(iSym)%coef(iBas,jBas)=-this%sym(iSym)%coef(jBas,iBas)
                                ENDDO
                            ENDIF
                        ENDDO
                    ENDDO
                ELSE
                    READ(un) this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    READ(un) this%symSize(1)
                    nBas=this%symSize(1)
                    ALLOCATE(this%sym(1)%coef(nBas,nBas)) 
                    DO 
                        READ(un,IOSTAT=stat) iBas,jBas,val
                        IF(stat.ne.0) EXIT
                        this%sym(1)%coef(iBas,jBas)=val                                                
                        this%sym(1)%coef(jBas,iBas)=val                                                
                    ENDDO
                ENDIF
            ELSE
                OPEN(un,file=fich,STATUS="old")
                IF (ALLOCATED(this%symSize)) DEALLOCATE(this%symSize)
                IF (ALLOCATED(this%sym)) DEALLOCATE(this%sym)
                IF (array) THEN
                    IF (hermitian.eq."h") THEN
                        WRITE(6,'(A)') "Reading hermitian matrix"
                    ELSEIF (hermitian.eq."a") THEN
                        WRITE(6,'(A)') "Reading anti hermitian matrix"
                    ELSEIF (hermitian.eq."o") THEN
                        WRITE(6,'(A)') "Reading square matrix"
                    ELSE
                        STOP "ERROR: unknwon keyword for hermitian"
                    ENDIF
                    READ(un,*) this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    DO iSym=1,this%nSym
                        READ(un,*) this%symSize(iSym)
                        !print*,"DEBUG",this%symSize(iSym) 
                        nBas=this%symSize(iSym)
                        ALLOCATE(this%sym(iSym)%coef(nBas,nBas))
                        DO iBas=1,nBas
                            IF (hermitian.eq."o") THEN
                                READ(un,*) (this%sym(iSym)%coef(jBas,iBas),jBas=1,nBas)
                            ELSE
                                READ(un,*) (this%sym(iSym)%coef(jBas,iBas),jBas=1,iBas)
                            ENDIF
                            IF (hermitian.eq."h") THEN
                                DO jBas=1,iBas
                                    this%sym(iSym)%coef(iBas,jBas)=this%sym(iSym)%coef(jBas,iBas)
                                ENDDO
                            ELSEIF (hermitian.eq."a") THEN
                                DO jBas=1,iBas
                                    this%sym(iSym)%coef(iBas,jBas)=-this%sym(iSym)%coef(jBas,iBas)
                                ENDDO
                            ENDIF
                        ENDDO
                    ENDDO
                ELSE
                    READ(un,*) this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    READ(un,*) this%symSize(1)
                    nBas=this%symSize(1)
                    ALLOCATE(this%sym(1)%coef(nBas,nBas)) 
                    DO 
                        READ(un,*,IOSTAT=stat) iBas,jBas,val
                        IF(stat.ne.0) EXIT
                        this%sym(1)%coef(iBas,jBas)=val                                                
                        this%sym(1)%coef(jBas,iBas)=val                                                
                    ENDDO
                ENDIF
            ENDIF
            this%alive=.TRUE.
            CLOSE(un)
        END SUBROUTINE
        SUBROUTINE writeOneInt(this,fich,sorte,arrayIn,unIn,printModeIn,skaleIn)
            IMPLICIT none
            CLASS(oneInt_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            LOGICAL, INTENT(in), OPTIONAL :: arrayIn
            INTEGER, INTENT(in), OPTIONAL :: unIn
            CHARACTER*3, INTENT(in), OPTIONAL :: printModeIn
            INTEGER, INTENT(in), OPTIONAL :: skaleIn
            CHARACTER*3 :: printMode
            REAL*8 :: skale
            LOGICAL :: bin,array
            INTEGER :: iSym,nBas,counter,dump,nLines,length,jBas,iBas,un
            array=.TRUE.
            un=1
            printMode="tri"
            skale=1.d0
            IF (present(printModeIn)) printMode=printModeIn
            IF (present(skaleIn)) skale=REAL(skaleIn)
            IF (present(arrayIn)) array=arrayIn
            IF (present(unIn)) un=unIn
            
            IF (.NOT.this%alive) STOP "An oneInt oneInt cannot be written before being read or initialized"
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            IF (bin) THEN
                OPEN(un,file=fich,FORM="unformatted",STATUS="replace")
                IF (array) THEN
                    WRITE(un) this%nSym
                    DO iSym=1,this%nSym     
                        WRITE(un) this%symSize(iSym)
                        nBas=this%symSize(iSym)
                        DO iBas=1,nBas
                            IF (printMode.eq."tri") THEN
                                WRITE(un) (this%sym(iSym)%coef(jBas,iBas)/skale,jBas=1,iBas)
                            ELSE
                                WRITE(un) (this%sym(iSym)%coef(jBas,iBas)/skale,jBas=1,nBas)
                            ENDIF
                        ENDDO
                    ENDDO
                ELSE
                    WRITE(un) 1
                    WRITE(un) this%symSize(1)
                    nBas=this%symSize(1)
                    DO iBas=1,nBas
                        DO jBas=1,nBas
                            WRITE(un) iBas,jBas,this%sym(1)%coef(jBas,iBas)/skale
                        ENDDO
                    ENDDO
                ENDIF
            ELSE
                OPEN(un,file=fich,STATUS="replace")
                IF (array) THEN
                    WRITE(un,*) this%nSym
                    DO iSym=1,this%nSym     
                        WRITE(un,*) this%symSize(iSym)
                        nBas=this%symSize(iSym)
                        DO iBas=1,nBas
                            IF (printMode.eq."tri") THEN
                                WRITE(un,'(10000(X,E30.20e3))') (this%sym(iSym)%coef(jBas,iBas)/skale,jBas=1,iBas)
                            ELSEIF (printMode.eq."rch") THEN
                                DO jBas=iBas+1,nBas
                                    this%sym(iSym)%coef(jBas,iBas)=this%sym(iSym)%coef(iBas,jBas)
                                ENDDO
                                WRITE(un,'(10000(X,E30.20e3))') (this%sym(iSym)%coef(jBas,iBas)/skale,jBas=1,nBas)
                            ELSEIF (printMode.eq."rca") THEN
                                DO jBas=iBas+1,nBas
                                    this%sym(iSym)%coef(jBas,iBas)=-this%sym(iSym)%coef(iBas,jBas)
                                ENDDO
                                WRITE(un,'(10000(X,E30.20e3))') (this%sym(iSym)%coef(jBas,iBas)/skale,jBas=1,nBas)
                            ELSE
                                STOP "ERROR: unknown keyword for printMode. &
                                  Allowed: tri(triangilar),rch(rectangular hermitian),rch (rectangular antihermitian)" 
                            ENDIF
                        
                        ENDDO
                    ENDDO
                ELSE
                    WRITE(un,*) 1
                    WRITE(un,*) this%symSize(1)
                    nBas=this%symSize(1)
                    DO iBas=1,nBas
                        DO jBas=1,nBas
                            WRITE(un,'(2(x,I8),x,E18.12)') iBas,jBas,this%sym(1)%coef(jBas,iBas)/skale
                        ENDDO
                    ENDDO
                ENDIF
            ENDIF
            CLOSE(un)
        END SUBROUTINE
        SUBROUTINE getRecord(this,i,j,val,stat)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER,INTENT(out) :: i,j,stat
            REAL*8,INTENT(out) :: val
            IF (this%bin) THEN
                READ(this%un,IOSTAT=stat) i,j,val
            ELSE
                READ(this%un,*,IOSTAT=stat) i,j,val
            ENDIF
        END SUBROUTINE
        SUBROUTINE getNSym(this,nSym)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(out) :: nSym
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            nSym=this%nSym
        END SUBROUTINE
        SUBROUTINE getSymSizeMultiSym(this,symSize)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER,ALLOCATABLE, INTENT(out) :: symSize(:)
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            IF (ALLOCATED(symSize)) DEALLOCATE(symSize)
            ALLOCATE(symSize(this%nSym))
            symSize=this%symSize
        END SUBROUTINE
        SUBROUTINE getSymSizeSingleSym(this,symSize)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER,INTENT(out) :: symSize
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            symSize=this%symSize(1)
        END SUBROUTINE
        SUBROUTINE getValSingleSym(this,i,j,val)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(in) :: i,j
            REAL*8, INTENT(out) :: val
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            val=this%sym(1)%coef(i,j)
        END SUBROUTINE           
        SUBROUTINE getValMultiSym(this,s,i,j,val)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(in) :: s,i,j
            REAL*8, INTENT(out) :: val
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            val=this%sym(s)%coef(i,j)
        END SUBROUTINE           
        SUBROUTINE getSymMultiSym(this,s,arr)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(in) :: s
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:,:)
            INTEGER :: nBas
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            IF(ALLOCATED(arr)) DEALLOCATE(arr)
            nBas=this%symSize(s)
            ALLOCATE(arr(nBas,nBas))
            arr=this%sym(s)%coef 
        END SUBROUTINE
        SUBROUTINE getSymSingleSym(this,arr)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER :: s
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:,:)
            INTEGER :: nBas
            s=1
            IF (.NOT.this%alive) STOP "Information cannot be read from an oneInt before being read or initialized"
            IF(ALLOCATED(arr)) DEALLOCATE(arr)
            nBas=this%symSize(s)
            ALLOCATE(arr(nBas,nBas))
            arr=this%sym(s)%coef 
        END SUBROUTINE
        SUBROUTINE setSymMultiSym(this,s,arr)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(in) :: s
            REAL*8, ALLOCATABLE, INTENT(in) :: arr(:,:)
            IF (.NOT.this%alive) STOP "An oneInt oneInt cannot be modified before being read or initialized"
            IF (.NOT.ALLOCATED(arr)) STOP "Only an allocated array can be written to a symmetry"
            IF (SIZE(arr,1).NE.this%symSize(s)) STOP "Dimension of Matrix and OneInt symmetry dont match"           
            this%sym(s)%coef=arr 
        END SUBROUTINE 
        SUBROUTINE setSymSingleSym(this,arr)
            IMPLICIT none
            class(oneInt_) :: this
            REAL*8, ALLOCATABLE, INTENT(in) :: arr(:,:)
            IF (.NOT.this%alive) STOP "An oneInt oneInt cannot be modified before being read or initialized"
            IF (.NOT.ALLOCATED(arr)) STOP "Only an allocated array can be written to a symmetry"
            IF (SIZE(arr,1).NE.this%symSize(1)) STOP "Dimension of Matrix and OneInt symmetry dont match"           
            this%sym(1)%coef=arr 
        END SUBROUTINE 
        SUBROUTINE initializeEmptyMultiSym(this,symSize)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, ALLOCATABLE, INTENT(in) :: symSize(:)
            INTEGER :: iSym,nBas
            IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
            IF (.NOT.(ALLOCATED(symSize))) STOP "Cannot use unallocated matrix of symmetry sizes to initialize"
            this%nSym=SIZE(symSize)
            ALLOCATE(this%symSize(this%nSym))
            ALLOCATE(this%sym(this%nSym))
            this%symSize=symSize
            DO iSym=1,this%nSym     
                nBas=symSize(iSym)
                ALLOCATE(this%sym(iSym)%coef(nBas,nBas))
                this%sym(iSym)%coef=0.d0
            ENDDO           
            this%alive=.TRUE.
        END SUBROUTINE 
        SUBROUTINE initializeEmptySingleSym(this,symSize)
            IMPLICIT none
            class(oneInt_) :: this
            INTEGER, INTENT(in) :: symSize
            INTEGER :: iSym,nBas
            IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
            this%nSym=1
            ALLOCATE(this%symSize(this%nSym))
            ALLOCATE(this%sym(this%nSym))
            this%symSize(1)=symSize
            DO iSym=1,this%nSym     
                nBas=this%symSize(iSym)
                ALLOCATE(this%sym(iSym)%coef(nBas,nBas))
                this%sym(iSym)%coef=0.d0
            ENDDO           
            this%alive=.TRUE.
        END SUBROUTINE 
        SUBROUTINE diagonalize(this)
            IMPLICIT none
            CLASS(oneInt_) :: this
            INTEGER*8 :: iSym,nOrb
            DO iSym=1,this%nSym
                nOrb=this%symSize(iSym)
                ALLOCATE(this%sym(iSym)%eVect(nOrb,nOrb))
                ALLOCATE(this%sym(iSym)%eVal(nOrb))
                this%sym(iSym)%eVect=this%sym(iSym)%coef
                CALL diag(this%sym(iSym)%eVect,nOrb,this%sym(iSym)%eVal)
            ENDDO
        END SUBROUTINE
        SUBROUTINE getEVect(this,evect,symIn)
            IMPLICIT none
            CLASS(oneInt_) :: this
            INTEGER,OPTIONAL,INTENT(in) :: symIn
            REAL*8,INTENT(out),ALLOCATABLE :: evect(:,:)
            INTEGER :: iSym,nOrb
            iSym=1
            IF (PRESENT(symIn)) iSym=symIn
            IF (ALLOCATED(evect)) DEALLOCATE(evect)
            nOrb=this%symSize(iSym)
            ALLOCATE(evect(nOrb,nOrb))
            evect=this%sym(iSym)%eVect
        END SUBROUTINE
        SUBROUTINE getEVal(this,eval,symIn)
            IMPLICIT none
            CLASS(oneInt_) :: this
            INTEGER,OPTIONAL,INTENT(in) :: symIn
            REAL*8,INTENT(out),ALLOCATABLE :: eval(:)
            INTEGER :: iSym,nOrb
            iSym=1
            IF (PRESENT(symIn)) iSym=symIn
            IF (ALLOCATED(eval)) DEALLOCATE(eval)
            nOrb=this%symSize(iSym)
            ALLOCATE(eval(nOrb))
            eval=this%sym(iSym)%eVal
        END SUBROUTINE
        SUBROUTINE getLinIndep(this,thr,arr,nLinIndep,symIn)
            IMPLICIT none
            CLASS(oneInt_) :: this
            REAL*8, INTENT(in) :: thr
            INTEGER, INTENT(out) :: nLinIndep
            INTEGER,INTENT(in), OPTIONAL :: symIn
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:,:)
            INTEGER :: iSym,nDep,iOrb,nOrb,nDependent
            iSym=1
            IF (present(symIn)) iSym=symIn
            nOrb=this%symSize(iSym)
            DO iOrb=1,nOrb
                IF (this%sym(iSym)%eVal(iOrb).ge.thr) EXIT
                nDep=nDep+1
            ENDDO
            nLinIndep=nOrb-nDep
            IF (ALLOCATED(arr)) DEALLOCATE(arr)
            ALLOCATE(arr(nOrb,nLinIndep))
            DO iOrb=nOrb,nDependent+1
                arr(:,iOrb)=this%sym(iSym)%eVect(:,iOrb)/sqrt(this%sym(iSym)%eVal(iOrb))
            ENDDO
        END SUBROUTINE
        SUBROUTINE destroy(this)
            IMPLICIT none
            class(oneInt_) :: this
            this%alive=.FALSE.
            IF (this%un.gt.0) THEN
                    CLOSE(this%un)
            ENDIF
            DEALLOCATE(this%symSize,this%sym)
        END SUBROUTINE
END MODULE
