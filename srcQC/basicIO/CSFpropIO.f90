!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module CSFprop_
implicit none

type, public :: CSFprop
 integer :: nDet,norb
 real*8, allocatable :: ampl(:)
 integer*1, allocatable :: Dets(:,:)
 character*1, allocatable :: CSFname(:)
 
 contains
  procedure :: initCSFprop
  procedure :: translation
  procedure :: extractDet
  
end type CSFprop

contains

 subroutine initCSFprop(this,numberofdet,norbitals)
 implicit none
 class(CSFprop) :: this
 integer :: numberofdet,norbitals
 
 this%nDet=numberofdet
 this%norb=norbitals
 allocate(this%ampl(this%nDet))
 allocate(this%Dets(this%nDet,this%norb))
 allocate(this%CSFname(this%norb))
 
 end subroutine initCSFprop


 subroutine translation(this,tmpchar,amplitude)
 implicit none
 class(CSFprop) :: this
 character(len=this%norb) :: tmpchar(this%nDet)
 integer :: amplitude(2,this%nDet)
 character(len=this%norb) :: tmpchar2
 integer i,j
 
 do j=1,this%nDet
  this%ampl(j)=float(amplitude(1,j))/float(amplitude(2,j))
  if (this%ampl(j) .ge.0) then
   this%ampl(j)=sqrt(this%ampl(j))
  else
   this%ampl(j)=-sqrt(-this%ampl(j))
  endif
  tmpchar2=trim(adjustl(tmpchar(j)))
!  write(6,*) j,this%ampl(j),tmpchar2
  do i=1,this%norb
   select case(tmpchar2(i:i))
    case("#")
     this%Dets(j,i)=3
    case(".")
     this%Dets(j,i)=0
    case("+")
     this%Dets(j,i)=1
    case("-")
     this%Dets(j,i)=2
    case default
     write(6,*) "Problems to select the occupation ",tmpchar(i:i)
     stop
   end select
  enddo
 ! write(6,*) j,this%ampl(j),this%Dets(j,:)
 enddo
 
 end subroutine translation
 
 subroutine extractDet(this,n,extDet,extamp)
 implicit none
 class(CSFprop) :: this
 integer :: n
 integer*1 :: extDet(this%norb)
 real*8 :: extamp
 
 integer i
 
 do i=1,this%norb
  extDet(i)=this%Dets(n,i)
  extamp=this%ampl(n)
 enddo
 
 return
 end subroutine extractDet

 
end module CSFprop_
