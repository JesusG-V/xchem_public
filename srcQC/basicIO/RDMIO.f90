!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE RDMIO
!CLASS FOR ONE ELECTRON REDUCED DENSITY MATRICX OBJECTS
    IMPLICIT none
    PRIVATE
    PUBLIC RDM_
    TYPE RDM_
        INTEGER :: un=0
        INTEGER :: states=0
        INTEGER :: occupied=0
        LOGICAL :: bin=.TRUE.
        LOGICAL :: aliveRead=.FALSE.
        LOGICAL :: aliveWrite=.FALSE.

        CONTAINS
            PROCEDURE :: initializeNew
            PROCEDURE :: initializeOld
            PROCEDURE :: getNextRecordInfoOneInt
            PROCEDURE :: writeNextRecordInfoOneInt
            PROCEDURE :: getNextRecordInfoTwoInt
            PROCEDURE :: writeNextRecordInfoTwoInt
            PROCEDURE :: getNextRecordData
            PROCEDURE :: writeNextRecordData
            PROCEDURE :: getOccupied
            PROCEDURE :: getStates
            PROCEDURE :: reset
            PROCEDURE :: back
            PROCEDURE :: destroy
            GENERIC :: getNextRecordInfo => getNextRecordInfoOneInt,getNextRecordInfoTwoInt
            GENERIC :: writeNextRecordInfo => writeNextRecordInfoOneInt,writeNextRecordInfoTwoInt
    END TYPE

    CONTAINS
        SUBROUTINE initializeNew(this,occupied,states,fich,sorte,unIn)
            IMPLICIT none 
            CLASS(RDM_) :: this
            CHARACTER(len=*),INTENT(in) :: fich
            CHARACTER(len=*),INTENT(in) :: sorte
            INTEGER, INTENT(in), OPTIONAL :: unIn
            INTEGER,INTENT(in) :: occupied,states
            this%un=1
            IF (PRESENT(unIn)) this%un=unIn
            IF (LEN(TRIM(fich)).eq.0) STOP "ERROR no file provided"
            IF (sorte(1:1).eq."b") THEN
                this%bin=.TRUE.
                OPEN(this%un,FILE=TRIM(fich),STATUS="replace",FORM="unformatted")
                WRITE(this%un) occupied,states
            ELSEIF(sorte(1:1).eq."f") THEN
                this%bin=.FALSE.
                OPEN(this%un,FILE=TRIM(fich),STATUS="replace")
                WRITE(this%un,*) occupied,states
            ELSE
                STOP "ERROR unknwon format keyword"
            ENDIF
            this%occupied=occupied
            this%states=states
            this%aliveWrite=.TRUE.
        END SUBROUTINE
        
        SUBROUTINE initializeOld(this,fich,sorte,unIn)
            IMPLICIT none 
            CLASS(RDM_) :: this
            CHARACTER(len=*),INTENT(in) :: fich
            CHARACTER(len=*),INTENT(in) :: sorte
            INTEGER*8, INTENT(in), OPTIONAL :: unIn
            INTEGER :: occupied,states
            this%un=1
            IF (PRESENT(unIn)) this%un=unIn
            IF (LEN(TRIM(fich)).eq.0) STOP "ERROR no file provided"
            IF (sorte(1:1).eq."b") THEN
                this%bin=.TRUE.
                OPEN(this%un,FILE=TRIM(fich),STATUS="old",FORM="unformatted")
                READ(this%un) occupied,states
            ELSEIF(sorte(1:1).eq."f") THEN
                this%bin=.FALSE.
                OPEN(this%un,FILE=TRIM(fich),STATUS="old")
                READ(this%un,*) occupied,states
            ELSE
                STOP "ERROR unknwon format keyword"
            ENDIF
            this%occupied=occupied
            this%states=states
            this%aliveRead=.TRUE.
        END SUBROUTINE

        SUBROUTINE getNextRecordInfoOneInt(this,i,j,empty,stat)
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER,INTENT(out) :: i,j,stat
            LOGICAL,INTENT(out) :: empty

            IF (.NOT.this%aliveRead) STOP "ERROR cannot read from the RDM"
            IF (this%bin) THEN
                READ(this%un,IOSTAT=stat) i,j,empty
            ELSE
                READ(this%un,*,IOSTAT=stat) i,j,empty
            ENDIF
        END SUBROUTINE

        SUBROUTINE writeNextRecordInfoOneInt(this,i,j,empty)
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER, INTENT(in) :: i,j
            LOGICAL, INTENT(in) :: empty
            IF (.NOT.this%aliveWrite) STOP "ERROR cannot write to the RDM"
            IF (this%bin) THEN
                WRITE(this%un) i,j,empty
            ELSE
                WRITE(this%un,*) i,j,empty
            ENDIF
        END SUBROUTINE

        SUBROUTINE getNextRecordInfoTwoInt(this,i,j,k,l,empty,stat)
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER,INTENT(out) :: i,j,k,l,stat
            LOGICAL,INTENT(out) :: empty

            IF (.NOT.this%aliveRead) STOP "ERROR cannot read from the RDM"
            IF (this%bin) THEN
                READ(this%un,IOSTAT=stat) i,j,k,l,empty
            ELSE
                READ(this%un,*,IOSTAT=stat) i,j,k,l,empty
            ENDIF
        END SUBROUTINE

        SUBROUTINE writeNextRecordInfoTwoInt(this,i,j,k,l,empty)
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER, INTENT(in) :: i,j,k,l
            LOGICAL, INTENT(in) :: empty
            IF (.NOT.this%aliveWrite) STOP "ERROR cannot write to the RDM"
            IF (this%bin) THEN
                WRITE(this%un) i,j,k,l,empty
            ELSE
                WRITE(this%un,'(4(x,I5),x,L)') i,j,k,l,empty
            ENDIF
        END SUBROUTINE

        SUBROUTINE getNextRecordData(this,states,rdm,lowerTriangularIn)
            !MOD: 2016-10-03 ONLY EVER TRIANGULAR MATRICES ARE READ BUT THE
            !WHOLE THING CAN BE GIVEN AS REQUESTE. ALSO REQUEST PARAMETER CHANGED FROM
            !LGOIAL TO CHARACTER
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER, INTENT(in) :: states
            REAL*8, INTENT(out) :: rdm(states,states)
            CHARACTER*1, INTENT(in), OPTIONAL :: lowerTriangularIn
            CHARACTER*1 :: lowerTriangular
            INTEGER :: iState,jState

            IF (.NOT.this%aliveRead) STOP "ERROR cannot read from the RDM"
            IF (states.ne.this%states) STOP "ERROR mismatch between rdm array sizes"

            lowerTriangular="T"
            IF (present(lowerTriangularIn)) lowerTriangular=lowerTriangularIn
            IF (states.ne.this%states) THEN
                WRITE(6,'(A,I0,A,I0)') "RDM object size: ",this%states," size of RDM passed to oject: ",states
                STOP "ERROR mismatch between rdm array sizes"
            ENDIF
            IF (this%bin) THEN
                READ(this%un) ((rdm(jState,iState),jState=1,iState),iState=1,this%states)
            ELSE
                READ(this%un,*) ((rdm(jState,iState),jState=1,iState),iState=1,this%states)
            ENDIF
            IF (lowerTriangular.eq."R") THEN
                DO iState=1,this%states
                    DO jState=1,iState
                        rdm(iState,jState)=rdm(jState,iState)
                    ENDDO
                ENDDO
            ENDIF
        END SUBROUTINE

        SUBROUTINE writeNextRecordData(this,states,rdm)
            !MOD: 2016-10-03 REMOVED OPTIION TO PRINT WHOLE MATRIX, RATHER ONLY EVER THE LOWER
            !TRIANGLE IS PRINTED
            IMPLICIT none 
            CLASS(RDM_) :: this
            INTEGER, INTENT(in) :: states
            REAL*8, INTENT(in) :: rdm(states,states)
            INTEGER :: iState,jState

            IF (.NOT.this%aliveWrite) STOP "ERROR cannot write to the RDM"
            IF (states.ne.this%states) STOP "ERROR mismatch between rdm array sizes"

            IF (this%bin) THEN
                WRITE(this%un) ((rdm(jState,iState),jState=1,iState),iState=1,this%states)
            ELSE
                WRITE(this%un,*) ((rdm(jState,iState),jState=1,iState),iState=1,this%states)
            ENDIF
        END SUBROUTINE

        SUBROUTINE getOccupied(this,occupied)
            IMPLICIT none
            CLASS(RDM_) :: this
            INTEGER, INTENT(out) :: occupied
            IF (.NOT.(this%aliveRead.OR.this%aliveWrite)) STOP "ERROR rdm not initialized"
            occupied=this%occupied
        END SUBROUTINE 

        SUBROUTINE getStates(this,states)
            IMPLICIT none
            CLASS(RDM_) :: this
            INTEGER, INTENT(out) :: states
            IF (.NOT.(this%aliveRead.OR.this%aliveWrite)) STOP "ERROR rdm not initialized"
            states=this%states
        END SUBROUTINE 

        SUBROUTINE reset(this)
            IMPLICIT none 
            CLASS(RDM_) :: this
            REWIND(this%un)
            IF (this%bin) THEN
                READ(this%un)
            ELSE
                READ(this%un,*)
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE back(this)
            IMPLICIT none
            CLASS(RDM_) :: this
            IF (this%aliveRead.OR.this%aliveWrite) THEN
                BACKSPACE(this%un)
            ELSE
                STOP "ERROR: back cannot be called on uninitialized RDM"
            ENDIF
        END SUBROUTINE

        SUBROUTINE destroy(this)
            IMPLICIT none 
            CLASS(RDM_) :: this
            CLOSE(this%un)
            this%aliveRead=.FALSE.
            this%aliveWrite=.FALSE.
        END SUBROUTINE
END MODULE
