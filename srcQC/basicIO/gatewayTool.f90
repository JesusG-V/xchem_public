!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE class_gateway
    USE createInputUtilities
    USE class_inputString
    USE lkIO
    IMPLICIT none
    PRIVATE
    PUBLIC :: gateway
    TYPE atom
        !gfort 4.8 does not support deferred length types
        !CHARACTER(len=:), ALLOCATABLE :: label
        CHARACTER*30 :: label
        REAL*8 :: cartesian(3)
        CHARACTER*100 :: lengthUnit
    END TYPE
    TYPE basis
        REAL*8 :: nuclearCharge
        !gfort 4.8 does not support deferred length types
        !CHARACTER(len=:), ALLOCATABLE :: label
        CHARACTER*30 :: label
        INTEGER :: lMax,nAtom
        INTEGER, ALLOCATABLE :: primitives(:),contractions(:)
        REAL*8, ALLOCATABLE :: exponents(:,:)
        REAL*8, ALLOCATABLE :: contractionCoefficients(:,:,:)
        TYPE(atom), ALLOCATABLE :: atoms(:)
    END TYPE
    TYPE gateway
        PRIVATE
        INTEGER :: nbasis
        REAL*8 :: nuclearCharge
        CHARACTER*100 :: title
        CHARACTER*100 :: monocentricBasisFile
        CHARACTER*100 :: symmetry
        TYPE(basis), ALLOCATABLE :: bases(:)        
        CONTAINS
            PROCEDURE :: readGateway
            PROCEDURE :: writeMolcasGatewayLoc
            PROCEDURE :: writeMolcasGatewayLocMon
            PROCEDURE :: setProjectName
            PROCEDURE :: setMonocentricBasisFile
            PROCEDURE :: writeMolproGateway
            GENERIC :: writeMolcasGateway => writeMolcasGatewayLoc, writeMolcasGatewayLocMon
    END TYPE
    
    CONTAINS
        SUBROUTINE readGateway(this,fich) 
            IMPLICIT none
            CLASS(gateway) :: this
            CHARACTER*100, INTENT(in) :: fich
            INTEGER :: i,iL,iExp,iCon,iAto,stat,iBas
            INTEGER :: basCount
            CHARACTER*200 :: token
            REAL*8 :: r
            
            OPEN(2,file=fich,STATUS="old")
            OPEN(1,STATUS="scratch")
            DO 
                READ(2,'(A)',IOSTAT=stat) token
                IF (IS_IOSTAT_END(stat)) EXIT
                token=ADJUSTL(token)
                IF (LEN(TRIM(token)).eq.0) CYCLE
                IF (token(1:1).eq."*") CYCLE
                CALL to_lower(token)
                WRITE(1,'(A)') TRIM(token)
            ENDDO      
            REWIND(1)
            CLOSE(2)

            DO
                READ(1,'(A)',IOSTAT=stat) token
                IF (IS_IOSTAT_END(stat)) EXIT
                IF (INDEX(token,"inline").ne.0) THEN
                    this%nBasis=this%nBasis+1
                ENDIF
            ENDDO
            REWIND(1)

            ALLOCATE(this%bases(this%nBasis))
            DO iBas=1,this%nBasis
                DO 
                    READ(1,'(A)',IOSTAT=stat) token
                    IF (INDEX(token,"inline").ne.0) THEN
                        EXIT
                    ENDIF
                ENDDO
                i=LEN(TRIM(token))
                IF (LEN(TRIM(token)).gt.30) THEN
                    STOP "ERROR: Max legth for basis label is 30 (gfort 4.8 :( )"
                ENDIF
                i=SCAN(token,"/")
                IF (i.eq.0) THEN
                    STOP "ERROR: Gateway file containes errors"
                ENDIF
                this%bases(iBas)%label=TRIM(token(1:i-1))
            ENDDO

            DO iBas=1,this%nBasis
                REWIND(1)
                this%bases(ibas)%nAtom=0
                basCount=0
                DO 
                    READ(1,'(A)',IOSTAT=stat) token
                    IF (INDEX(token,"inline").ne.0) THEN
                        basCount=basCount+1
                        IF (basCount.eq.iBas) EXIT
                    ENDIF
                ENDDO
                READ(1,*) this%bases(iBas)%nuclearCharge,this%bases(iBas)%lMax
                ALLOCATE(this%bases(iBas)%primitives(this%bases(iBas)%lMax+1))
                ALLOCATE(this%bases(iBas)%contractions(this%bases(iBas)%lMax+1))
                DO iL=1,this%bases(iBas)%lMax+1
                    READ(1,*) this%bases(iBas)%primitives(iL),this%bases(iBas)%contractions(iL)
                    !DO iExp=1,2*this%bases(iBas)%primitives(iL)
                    !    READ(1,*)
                    !ENDDO
                    !DO iExp=1,this%bases(iBas)%primitives(iL)
                    READ(1,*) (r,iExp=1,this%bases(iBas)%primitives(iL))
                    !ENDDO
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        READ(1,*)
                    ENDDO
                ENDDO
                
                ALLOCATE(this%bases(iBas)%exponents(MAXVAL(this%bases(iBas)%primitives),this%bases(iBas)%lMax+1))
                ALLOCATE(this%bases(iBas)%contractionCoefficients(      MAXVAL(this%bases(iBas)%contractions),&
                                                                        MAXVAL(this%bases(iBas)%primitives),&
                                                                        this%bases(iBas)%lMax+1))
                this%bases(iBas)%exponents=0.d0
                this%bases(iBas)%contractionCoefficients=0.d0
                REWIND(1)
                
                basCount=0
                DO 
                    READ(1,'(A)',IOSTAT=stat) token
                    IF (INDEX(token,"inline").ne.0) THEN
                        basCount=basCount+1
                        IF (basCount.eq.iBas) EXIT
                    ENDIF
                ENDDO
                
                READ(1,*)

                DO iL=1,this%bases(iBas)%lMax+1
                    READ(1,*) 
                    !DO iExp=1,this%bases(iBas)%primitives(iL)
                    READ(1,*) (this%bases(iBas)%exponents(iExp,iL),iExp=1,this%bases(iBas)%primitives(iL))
                    !ENDDO
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        READ(1,*) (this%bases(iBas)%contractionCoefficients(iCon,iExp,iL),iCon=1,this%bases(iBas)%contractions(iL))
                    ENDDO
                ENDDO
                DO
                    READ(1,'(A)',IOSTAT=stat) token
                    IF (INDEX(token,"end").gt.0) EXIT
                    IF (LEN(TRIM(token)).gt.0) THEN
                        this%bases(iBas)%nAtom=this%bases(iBas)%nAtom+1
                    ENDIF
                ENDDO
                ALLOCATE(this%bases(iBas)%atoms(this%bases(ibas)%nAtom))
                DO iAto=1,this%bases(iBas)%nAtom+1
                    BACKSPACE(1)
                ENDDO
                DO  iAto=1,this%bases(iBas)%nAtom
                    READ(1,*) token,(this%bases(iBas)%atoms(iAto)%cartesian(i),i=1,3),this%bases(iBas)%atoms(iAto)%lengthUnit
                    IF (LEN(TRIM(token)).gt.30) THEN
                        STOP "ERROR: Max legth for atom label is 30 (gfort 4.8 :( )"
                    ENDIF
                    this%bases(iBas)%atoms(iAto)%label=TRIM(token)
                ENDDO
            ENDDO
            DO 
                READ(1,'(A)',IOSTAT=stat) token
                IF (stat.ne.0) THEN    
                    this%symmetry=""
                    EXIT
                ENDIF
                IF (INDEX(token,"symmetry").gt.0) THEN
                    READ(1,'(A)') this%symmetry
                    EXIT
                ENDIF
            ENDDO
            CLOSE(1)
        END SUBROUTINE
        
        SUBROUTINE writeMolcasGatewayLocMon(this,un,lkPairs,symmetry,multiGhost)
            IMPLICIT none
            CLASS(gateway) :: this
            LOGICAL :: symmetry
            LOGICAL, INTENT(in) :: multiGhost
            INTEGER, INTENT(in) :: un
            TYPE(lk_), INTENT(in) :: lkPairs
            INTEGER :: i,iL,iK,iExp,iCon,iAto,iSym,iBas,iP,kMax
            CHARACTER*100 :: token,token2,fmt
            REAL*8 :: r

            WRITE(un,'(A)')  "&GATEWAY"
            WRITE(un,'(A,A)')  "Title=",TRIM(this%title)
            WRITE(un,'(A)')  "expert"
            DO ibas=1,this%nBasis
                WRITE(un,'(A)')  "Basis set"
                token=this%bases(iBas)%label
                WRITE(un,'(A,A,4(x),A,x,A)') TRIM(token),"....","/","inline" 
                !WRITE(un,'(A,A,4(x),A,x,A)') token(1:1),"....","/","inline" 
                WRITE(un,'(10(x),F3.0,3(x),I0)') this%bases(iBas)%nuclearCharge,this%bases(iBas)%lMax
                DO iL =1,this%bases(iBas)%lMax+1
                    WRITE(un,'(I5,I5)') this%bases(iBas)%primitives(iL),this%bases(iBas)%contractions(iL) 
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        WRITE(un,'(F18.8)') this%bases(iBas)%exponents(iExp,iL)
                    ENDDO
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        WRITE(fmt,'(A,I0,A)') "(",this%bases(iBas)%contractions(iL),&
                          "(x,F17.8))"
                        WRITE(un,TRIM(fmt)) &
                          (this%bases(iBas)%contractionCoefficients(iCon,iExp,iL),iCon=1,this%bases(iBas)%contractions(iL))
                    ENDDO
                ENDDO
                WRITE(un,*)
                IF (symmetry) THEN
                    i=SCAN(this%bases(iBas)%atoms(1)%label,"1234567890")
                    IF (i.eq.0) THEN
                        IF (this%bases(iBas)%nAtom.gt.1) THEN
                            STOP "ERROR: Incorrect Atom label.&
                             For more then one atom, atoms need to be labeled X1,X2,..."
                        ENDIF
                    ENDIF

                    DO iAto=1,this%bases(ibas)%nAtom
                        IF (i.eq.0) THEN
                            WRITE(un,'(A,A1,x,3(F18.12,x),x,A)') &
                              TRIM(this%bases(iBas)%atoms(iAto)%label),"1",&
                              (this%bases(ibas)%atoms(iAto)%cartesian(i),i=1,3),this%bases(ibas)%atoms(iAto)%lengthUnit
                        ELSE
                            WRITE(un,'(A,x,3(F18.12,x),x,A)') &
                              TRIM(this%bases(iBas)%atoms(iAto)%label),&
                              (this%bases(ibas)%atoms(iAto)%cartesian(i),i=1,3),this%bases(ibas)%atoms(iAto)%lengthUnit
                        ENDIF
                    ENDDO  
                ELSE
                    i=SCAN(this%bases(iBas)%atoms(1)%label,"1234567890")
                    token=this%bases(iBas)%atoms(1)%label
                    IF (i.eq.0) THEN
                        IF (this%bases(iBas)%nAtom.gt.1) THEN
                            STOP "ERROR: Incorrect Atom label.&
                             For more then one atom, atoms need to be labeled X1,X2,..."
                        ENDIF
                    ELSE
                        token=token(1:i-1)
                    ENDIF
                    DO iAto=2,this%bases(iBas)%nAtom
                        i=SCAN(this%bases(iBas)%atoms(iAto)%label,"1234567890")
                        IF (i.eq.0) THEN
                            IF (this%bases(iBas)%nAtom.gt.1) THEN
                                STOP "Incorrect Atom label.&
                                  For more then one atom, atoms need to be labeled X1,X2,..."
                            ENDIF
                        ENDIF
                        token2=this%bases(iBas)%atoms(iAto)%label
                        token2=token2(1:i-1)
                        IF (TRIM(token).ne.TRIM(token2)) THEN
                            STOP "Inconsitency in Atom labels"
                        ENDIF
                    ENDDO
                    !WRITE(un,'(A,A,A)') "##geometry",TRIM(token),"#"
                    WRITE(un,'(A,I3.3,A)') "##geometry",iBas,"#"
                ENDIF
                WRITE(un,'(A)') "End of basis"
            ENDDO
            
            IF (multiGhost) THEN
                DO iP=1,lkPairs%getNrLs()
                    iL = lkPairs%getL(iP)
                    kMax = lkPairs%getK(iL)
                    WRITE(un,'(A)') "Basis set"
                    WRITE(un,'(A)') "X.... / inline"
                    WRITE(un,'(F3.1,x,I2)') 0.0,kMax*2+iL
                    i=0
                    DO iK=0,iL-1
                        WRITE(un,'(A)') ">> INCLUDE $ghostLibrary/zero.bas"
                    ENDDO
                    DO iK=0,kMax*2
                        IF (mod(iK,2).eq.0) THEN
                            IF (LEN(TRIM(this%monocentricBasisFile)).eq.0) THEN
                                WRITE(un,'(A,I1,A,I1,A)') ">> INCLUDE $ghostLibrary/l.",iL,"_k.",iK/2,".bas"
                            ELSE
                                WRITE(un,'(A,I1,A,I1,A,A)') ">> INCLUDE $CurrDir/l.",iL,"_k.",iK/2,".",&
                                  TRIM(this%monocentricBasisFile)
                            ENDIF
                            i=i+1
                        ELSE
                            WRITE(un,'(A)') ">> INCLUDE $ghostLibrary/zero.bas"
                        ENDIF
                    ENDDO
                    WRITE(un,'(A)') "spherical all"
                    WRITE(un,'(A)') "contaminant all"
                    WRITE(un,'(A,I1,A)') "X",iL," 0.0 0.0 0.0 Bohr"
                    WRITE(un,'(A)') "End of Basis"
                ENDDO
            ELSE
                WRITE(un,'(A)') "Basis set"
                WRITE(un,'(A)') "X.... / inline"
                WRITE(un,'(F3.1,x,I2)') 0.0,lkPairs%getBigL()
                DO iL = 0,lkPairs%getBigL()
                    WRITE(un,'(A,A)') ">> INCLUDE $CurrDir/",TRIM(this%monocentricBasisFile)
                ENDDO
                WRITE(un,'(A)') "spherical all"
                WRITE(un,'(A)') "contaminant all"
                WRITE(un,'(A)') "X 0.0 0.0 0.0 Bohr"
                WRITE(un,'(A)') "End of Basis"
            ENDIF
            !IF (lMonocentric.ge.0) THEN
            !    WRITE(un,'(A)') "Basis set"
            !    WRITE(un,'(A)') "X.... / inline"
            !    WRITE(un,'(F3.1,x,I2)') 0.0,lMonocentric
            !    DO il=0,lMonocentric
            !        WRITE(un,'(A,x,A)') ">> INCLUDE",TRIM(this%monocentricBasisFile)
            !    ENDDO
            !    WRITE(un,'(A)') "spherical all"
            !    WRITE(un,'(A)') "contaminant all"
            !    WRITE(un,'(A)') "X 0.0 0.0 0.0 Angstrom"
            !    WRITE(un,'(A)') "End of Basis"
            !ENDIF
            
            IF (symmetry) THEN
                IF (LEN(TRIM(this%symmetry)).ne.0) THEN
                    WRITE(un,'(A)') "symmetry"
                    WRITE(un,'(A)') TRIM(this%symmetry)
                ENDIF
            ENDIF
            WRITE(un,'(A)') "sdip"
        END SUBROUTINE writeMolcasGatewayLocMon

        SUBROUTINE writeMolcasGatewayLoc(this,un,symmetry,multiGhost)
            IMPLICIT none
            CLASS(gateway) :: this
            LOGICAL :: symmetry
            LOGICAL, INTENT(in) :: multiGhost
            INTEGER, INTENT(in) :: un
            INTEGER :: i,iL,iK,iExp,iCon,iAto,iSym,iBas
            CHARACTER*100 :: token,token2,fmt
            REAL*8 :: r

            WRITE(un,'(A)')  "&GATEWAY"
            WRITE(un,'(A,A)')  "Title=",TRIM(this%title)
            WRITE(un,'(A)')  "expert"
            DO ibas=1,this%nBasis
                WRITE(un,'(A)')  "Basis set"
                token=this%bases(iBas)%label
                WRITE(un,'(A,A,4(x),A,x,A)') TRIM(token),"....","/","inline" 
                !WRITE(un,'(A,A,4(x),A,x,A)') token(1:1),"....","/","inline" 
                WRITE(un,'(10(x),F3.0,3(x),I0)') this%bases(iBas)%nuclearCharge,this%bases(iBas)%lMax
                DO iL =1,this%bases(iBas)%lMax+1
                    WRITE(un,'(I5,I5)') this%bases(iBas)%primitives(iL),this%bases(iBas)%contractions(iL) 
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        WRITE(un,'(F18.8)') this%bases(iBas)%exponents(iExp,iL)
                    ENDDO
                    DO iExp=1,this%bases(iBas)%primitives(iL)
                        WRITE(fmt,'(A,I0,A)') "(",this%bases(iBas)%contractions(iL),&
                          "(x,F17.8))"
                        WRITE(un,TRIM(fmt)) &
                          (this%bases(iBas)%contractionCoefficients(iCon,iExp,iL),iCon=1,this%bases(iBas)%contractions(iL))
                    ENDDO
                ENDDO
                WRITE(un,*)
                IF (symmetry) THEN
                    i=SCAN(this%bases(iBas)%atoms(1)%label,"1234567890")
                    IF (i.eq.0) THEN
                        IF (this%bases(iBas)%nAtom.gt.1) THEN
                            STOP "ERROR: Incorrect Atom label.&
                             For more then one atom, atoms need to be labeled X1,X2,..."
                        ENDIF
                    ENDIF

                    DO iAto=1,this%bases(ibas)%nAtom
                        IF (i.eq.0) THEN
                            WRITE(un,'(A,A1,x,3(F18.12,x),x,A)') &
                              TRIM(this%bases(iBas)%atoms(iAto)%label),"1",&
                              (this%bases(ibas)%atoms(iAto)%cartesian(i),i=1,3),this%bases(ibas)%atoms(iAto)%lengthUnit
                        ELSE
                            WRITE(un,'(A,x,3(F18.12,x),x,A)') &
                              TRIM(this%bases(iBas)%atoms(iAto)%label),&
                              (this%bases(ibas)%atoms(iAto)%cartesian(i),i=1,3),this%bases(ibas)%atoms(iAto)%lengthUnit
                        ENDIF
                    ENDDO  
                ELSE
                    i=SCAN(this%bases(iBas)%atoms(1)%label,"1234567890")
                    token=this%bases(iBas)%atoms(1)%label
                    IF (i.eq.0) THEN
                        IF (this%bases(iBas)%nAtom.gt.1) THEN
                            STOP "ERROR: Incorrect Atom label.&
                             For more then one atom, atoms need to be labeled X1,X2,..."
                        ENDIF
                    ELSE
                        token=token(1:i-1)
                    ENDIF
                    DO iAto=2,this%bases(iBas)%nAtom
                        i=SCAN(this%bases(iBas)%atoms(iAto)%label,"1234567890")
                        IF (i.eq.0) THEN
                            IF (this%bases(iBas)%nAtom.gt.1) THEN
                                STOP "Incorrect Atom label.&
                                  For more then one atom, atoms need to be labeled X1,X2,..."
                            ENDIF
                        ENDIF
                        token2=this%bases(iBas)%atoms(iAto)%label
                        token2=token2(1:i-1)
                        IF (TRIM(token).ne.TRIM(token2)) THEN
                            STOP "Inconsitency in Atom labels"
                        ENDIF
                    ENDDO
                    !WRITE(un,'(A,A,A)') "##geometry",TRIM(token),"#"
                    WRITE(un,'(A,I3.3,A)') "##geometry",iBas,"#"
                ENDIF
                WRITE(un,'(A)') "End of basis"
            ENDDO
            IF (symmetry) THEN
                IF (LEN(TRIM(this%symmetry)).ne.0) THEN
                    WRITE(un,'(A)') "symmetry"
                    WRITE(un,'(A)') TRIM(this%symmetry)
                ENDIF
            ENDIF
            WRITE(un,'(A)') "sdip"
        END SUBROUTINE writeMolcasGatewayLoc
        
        SUBROUTINE writeMolproGateway(this,un,inputOrb,multiplicitySource)
            IMPLICIT none
            CLASS(gateway) :: this
            INTEGER, INTENT(in) :: un,multiplicitySource
            CHARACTER(len=*),INTENT(in) :: inputOrb
            CHARACTER*100 :: token,token2,fmt
            CHARACTER*1 :: angMomLetter(10)
            LOGICAL :: readOrbFromFile
            INTEGER :: i,iL,iExp,iCon,iAto,iSym,iBas
            REAL*8 :: r
            angMomLetter(1)="s"
            angMomLetter(2)="p"
            angMomLetter(3)="d"
            angMomLetter(4)="f"
            angMomLetter(5)="g"
            angMomLetter(6)="h"
            angMomLetter(7)="i"
            angMomLetter(8)="k"
            angMomLetter(9)="l"
            angMomLetter(10)="m"
            
            INQUIRE(file=inputOrb,exist=readOrbFromFile)
            IF (readOrbFromFile) THEN
                WRITE(un,'(A,x,A)')  "file 3","in.wf"
                WRITE(un,'(A,x,A,A,x,A)') "file 2",TRIM(this%title),".wf","new"
            ELSE
                WRITE(un,'(A,x,A,A)') "file 2",TRIM(this%title),".wf" 
            ENDIF
            WRITE(un,'(A)') "gprint orbital=1000 civector"
            WRITE(un,'(A)') "memory 1000 M"
            
            WRITE(un,'(A)') "basis={"
            DO iBas=1,this%nBasis
                DO iL=1,this%bases(iBas)%lMax+1
                    i=SCAN(this%bases(iBas)%atoms(1)%label,"1234567890")
                    token2=this%bases(iBas)%atoms(1)%label
                    if (i.gt.0) then
                        token2=token2(1:i-1)
                    endif
                    WRITE(fmt,'(A,I0,A)') "(A1,x,A,",&
                      this%bases(iBas)%primitives(iL),"(x,E18.8))"
                    WRITE(un,TRIM(fmt)) &
                      !token(3:3),TRIM(token2),&
                      angMomLetter(iL),TRIM(token2),&
                      (this%bases(iBas)%exponents(iExp,iL),iExp=1,this%bases(iBas)%primitives(iL))
                    DO iCon=1,this%bases(iBas)%contractions(iL)
                        WRITE(fmt,'(A,I0,A)') "(A,x,I1,A,I2.2,",&
                          this%bases(iBas)%primitives(iL),"(x,F17.8))"
                        WRITE(un,TRIM(fmt)) &
                          "c",1,".",this%bases(iBas)%primitives(iL),&
                          (this%bases(iBas)%contractionCoefficients(iCon,iExp,iL),iExp=1,this%bases(iBas)%primitives(iL))
                    ENDDO
                ENDDO
            ENDDO
            WRITE(un,'(A)') "}"
            
            IF (.NOT.(readOrbFromFile)) THEN
                IF (LEN(TRIM(this%symmetry)).ne.0) THEN
                    WRITE(un,'(A,x,A)') "symmetry",this%symmetry
                ENDIF
                WRITE(un,'(A)') "geom={"
                
                WRITE(un,'(A)') "##geometry"
                WRITE(un,'(A)') "}"
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE setProjectName(this,pName)
            IMPLICIT none
            CLASS(gateway) :: this
            CHARACTER(LEN=*), INTENT(in) :: pName
            this%title=TRIM(pName)
        END SUBROUTINE       
        
        SUBROUTINE setMonocentricBasisFile(this,pName)
            IMPLICIT none
            CLASS(gateway) :: this
            CHARACTER(LEN=*), INTENT(in) :: pName
            this%monocentricBasisFile=TRIM(pName)
        END SUBROUTINE       
        
END MODULE
