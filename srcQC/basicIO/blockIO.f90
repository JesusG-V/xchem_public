!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE blockIO
!BLOCK CLASS: OBKECTS STORE INFORMATION ABOUT A BLOCK OF BASIS FUNCTIONS.
!SIZE, QUANTUM NUMBERS etc.
    IMPLICIT none
    PRIVATE
    PUBLIC block_
    TYPE block_
        PRIVATE
        INTEGER :: sym
        INTEGER :: k,l,m
        INTEGER :: ghost
        INTEGER :: nBas
        INTEGER :: offset
        LOGICAL :: rm
        CHARACTER*50 :: label
        CONTAINS
            !CONSTRUCTOR
            PROCEDURE :: create
            !GENERAL
            PROCEDURE :: printInfo
            PROCEDURE :: remove
            PROCEDURE :: removed
            !GETTERS
            PROCEDURE :: getSym
            PROCEDURE :: getK
            PROCEDURE :: getL
            PROCEDURE :: getM
            PROCEDURE :: getNBas
            PROCEDURE :: getOffset
            PROCEDURE :: getLabel
            PROCEDURE :: getGhost
            !SETTERS
            PROCEDURE :: setSym
            PROCEDURE :: setK
            PROCEDURE :: setL
            PROCEDURE :: setM
            PROCEDURE :: setNBas
            PROCEDURE :: setOffset
            PROCEDURE :: setLabel
            !OVERLOAD
            PROCEDURE :: ass
            GENERIC :: ASSIGNMENT(=) => ass
    END TYPE
    CONTAINS

    !OVERLOAD
    SUBROUTINE ass(a,b)
        CLASS(block_), INTENT(out) :: a
        CLASS(block_), INTENT(in) :: b
        a%sym=b%sym
        a%k=b%k
        a%l=b%l
        a%m=b%m
        a%nBas=b%nBas
        a%offset=b%offset
        a%label=b%label
        a%rm=b%rm
        a%ghost=b%ghost
    END SUBROUTINE

    !CONSTRUCTOR
    SUBROUTINE create(this,k,l,m,nBas,offset,label,sym,ghost)
        IMPLICIT none
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: k,l,m,nBas,offset,sym,ghost
        CHARACTER*50, INTENT(in) :: label
        this%k=k
        this%l=l
        this%m=m
        this%nBas=nBas
        this%offset=offset
        this%label=label
        this%sym=sym
        this%ghost=ghost
        this%rm=.FALSE.
    END SUBROUTINE

    !GENERAL
    SUBROUTINE printInfo(this)
        IMPLICIT none
        CLASS(block_) :: this
        WRITE(6,'(A,A7,A,I4,A,I3,A,I3,A,I3,A,I0,A,L,A,I1,A,I0)') "block ",TRIM(this%label)," nBas= ",this%nBas,&
          " (k,l,m)=(",this%k,",",this%l,",",this%m,") offset= ",this%offset," lkMax violation= ",&
          this%rm," symmetry= ",this%sym," ghost= ",this%ghost
    END SUBROUTINE

    SUBROUTINE remove(this)
        IMPLICIT none
        CLASS(block_) :: this
        this%rm=.TRUE.
    END SUBROUTINE
    
    FUNCTION removed(this) RESULT(del)
        IMPLICIT none
        LOGICAL :: del
        CLASS(block_) :: this
        del=this%rm
    END FUNCTION

    !SETTERS
    SUBROUTINE setK(this,k)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: k
        this%k=k
    END SUBROUTINE
    SUBROUTINE setL(this,l)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: l
        this%l=l
    END SUBROUTINE
    SUBROUTINE setM(this,m)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: m
        this%m=m
    END SUBROUTINE
    SUBROUTINE setNBas(this,nBas)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: nBas
        this%nBas=nBas
    END SUBROUTINE
    SUBROUTINE setOffset(this,offset)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: offset
        this%offset=offset
    END SUBROUTINE
    SUBROUTINE setLabel(this,label)
        IMPLICIT NONE
        CLASS(block_) :: this
        CHARACTER*50, INTENT(in) :: label
        this%label=label
    END SUBROUTINE
    SUBROUTINE setSym(this,sym)
        IMPLICIT none
        CLASS(block_) :: this
        INTEGER, INTENT(in) :: sym
        this%sym=sym
    END SUBROUTINE

    !GETTERS
    FUNCTION getGhost(this) RESULT(ghost)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: ghost
        ghost=this%ghost
    END FUNCTION
    FUNCTION getK(this) RESULT(k)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: k
        k=this%k 
    END FUNCTION
    FUNCTION getL(this) RESULT(l)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: l
        l=this%l 
    END FUNCTION
    FUNCTION getM(this) RESULT(m)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: m
        m=this%m
    END FUNCTION
    FUNCTION getNBas(this) RESULT(nBas)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: nBas
        nBas=this%nBas
    END FUNCTION
    FUNCTION getOffset(this) RESULT(offset)
        IMPLICIT NONE
        CLASS(block_) :: this
        INTEGER :: offset
        offset=this%offset
    END FUNCTION
    FUNCTION getLabel(this) RESULT(label)
        IMPLICIT NONE
        CLASS(block_) :: this
        CHARACTER*50 :: label
        label=this%label
    END FUNCTION
    FUNCTION getSym(this) RESULT(sym)
        IMPLICIT none
        CLASS(block_) :: this
        INTEGER :: sym
        sym=this%sym
    END FUNCTION
END MODULE
