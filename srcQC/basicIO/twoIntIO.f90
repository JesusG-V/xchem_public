!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE twoIntIO
!CLASS FOR OBJECTS THAT CONTAIN AND MANIPULATE
!TWO ELECTRON INTEGRALS. CAN PRINT AND READ IN
!binary OR formatted FORM AND list FORM ONLY
    IMPLICIT none
    PRIVATE
    PUBLIC twoInt_
    TYPE twoInt_
        PRIVATE
        INTEGER :: nBas
        INTEGER :: nIntegrals
        INTEGER :: un=0
        LOGICAL :: bin=.FALSE.
        LOGICAL :: indexed=.FALSE.
        REAL*8,ALLOCATABLE :: integral(:,:,:,:)
        REAL*8,ALLOCATABLE :: indexedIntegral(:)
        INTEGER, ALLOCATABLE :: indexList(:,:)
        LOGICAL :: alive=.FALSE.
        CONTAINS
            PROCEDURE :: readTwoInt
            PROCEDURE :: writeTwoInt
            PROCEDURE :: getNBas
            PROCEDURE :: getNIntegrals
            PROCEDURE :: getValMatrix
            PROCEDURE :: getValLine
            PROCEDURE :: setValArr
            PROCEDURE :: setValList
            PROCEDURE :: initializeEmpty
            PROCEDURE :: destroy
            PROCEDURE :: incValArr
            PROCEDURE :: incValList
            PROCEDURE :: setIndex
            PROCEDURE :: getIndex
            GENERIC :: setVal => setValArr,setValList
            GENERIC :: incVal => incValList,incValArr
            GENERIC :: getVal => getValLine,getValMatrix
            GENERIC :: getRecord => getValLine,getValMatrix
    END TYPE
    
    CONTAINS
        SUBROUTINE readTwoInt(this,fich,sorte,noReadIn)
            IMPLICIT none
            class(twoInt_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            INTEGER, OPTIONAL, INTENT(in) :: noReadIn
            LOGICAL :: bin
            REAL*8 :: val
            INTEGER :: iB,jB,kB,lB,stat,noRead
            noRead=0
            IF (PRESENT(noReadIn)) noRead=noReadIn
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            this%bin=bin
            this%nIntegrals=0
            IF (bin) THEN
                IF (noRead.eq.0) THEN
                    OPEN(1,file=fich,FORM="unformatted",STATUS="old")
                    READ(1) this%nBas
                ELSE
                    OPEN(noRead,file=fich,FORM="unformatted",STATUS="old")
                    READ(noRead) this%nBas
                    this%alive=.TRUE.
                    this%un=noRead
                    WRITE(6,'(x,A)') "Warning: specified unit one will remain open for later reading"
                    RETURN
                ENDIF
                ALLOCATE(this%integral(this%nBas,this%nBas,this%nBas,this%nBas)) 
                this%integral=0.d0
                DO 
                    READ(1,IOSTAT=stat) iB,jB,kB,lB,val
                    IF (stat.ne.0) EXIT
                    this%integral(iB,jB,kB,lB)=val
                    this%integral(iB,jB,lB,kB)=val
                    this%integral(jB,iB,kB,lB)=val
                    this%integral(jB,iB,lB,kB)=val
                    this%integral(kB,lB,iB,jB)=val
                    this%integral(kB,lB,jB,iB)=val
                    this%integral(lB,kB,iB,jB)=val
                    this%integral(lB,kB,jB,iB)=val
                    this%nIntegrals=this%nIntegrals+1
                ENDDO
            ELSE
                IF (noRead.eq.0) THEN
                    OPEN(1,file=fich,STATUS="old")
                    READ(1,*) this%nBas
                ELSE
                    OPEN(noRead,file=fich,STATUS="old")
                    READ(noRead,*) this%nBas
                    this%alive=.TRUE.
                                        this%un=noRead
                    WRITE(6,'(x,A)') "Warning: unit one will remain open for later reading"
                    RETURN
                ENDIF
                ALLOCATE(this%integral(this%nBas,this%nBas,this%nBas,this%nBas)) 
                this%integral=0.d0
                DO 
                    READ(1,*,IOSTAT=stat) iB,jB,kB,lB,val
                    this%integral(iB,jB,kB,lB)=val
                    this%integral(iB,jB,lB,kB)=val
                    this%integral(jB,iB,kB,lB)=val
                    this%integral(jB,iB,lB,kB)=val
                    this%integral(kB,lB,iB,jB)=val
                    this%integral(kB,lB,jB,iB)=val
                    this%integral(lB,kB,iB,jB)=val
                    this%integral(lB,kB,jB,iB)=val
                    IF (stat.ne.0) EXIT
                    this%nIntegrals=this%nIntegrals+1
                ENDDO
            ENDIF
            this%alive=.TRUE.
            CLOSE(1)
            END SUBROUTINE
            SUBROUTINE writeTwoInt(this,fich,sorte)
                IMPLICIT none
                class(twoInt_) :: this
                CHARACTER(len=*), INTENT(in) :: fich,sorte
                LOGICAL :: bin
                REAL*8 :: val
                INTEGER :: iB,jB,kB,lB
                IF (.NOT.this%alive) STOP "Cannot write uninitialized data to file"
                IF (sorte(1:1).eq."b") THEN
                    bin=.TRUE.
                ELSEIF (sorte(1:1).eq."f") THEN
                    bin=.FALSE.
                ELSE
                    STOP "unknown keyword filetype keyword" 
                ENDIF
                IF (bin) THEN
                    OPEN(1,file=TRIM(fich),FORM="unformatted",STATUS="replace")
                    WRITE(1) this%nBas,this%nIntegrals
                    IF (this%indexed) THEN
                        DO iB=1,this%nIntegrals
                            IF (ABS(this%indexedIntegral(iB)).lt.1e-40) CYCLE
                            WRITE(1) (this%indexList(jB,iB),jB=1,4),this%indexedIntegral(iB)  
                        ENDDO
                    ELSE
                        DO iB=1,this%nBas
                            DO jB=1,this%nBas
                                DO kB=1,this%nBas
                                    DO lB=1,this%nBas
                                        IF (ABS(this%integral(iB,jB,kB,lB)).lt.1e-40) CYCLE
                                        WRITE(1) iB,jB,kB,lB,this%integral(iB,jB,kB,lB)
                                    ENDDO
                                ENDDO
                            ENDDO
                        ENDDO
                    ENDIF
                ELSE
                    OPEN(1,file=TRIM(fich),STATUS="replace")
                    WRITE(1,*) this%nBas,this%nIntegrals
                    IF (this%indexed) THEN
                        DO iB=1,this%nIntegrals
                            IF (ABS(this%indexedIntegral(iB)).lt.1e-40) CYCLE
                            WRITE(1,'(4(x,I8),x,E24.16)') (this%indexList(jB,iB),jB=1,4),this%indexedIntegral(iB)  
                        ENDDO
                    ELSE
                        DO iB=1,this%nBas
                            DO jB=1,this%nBas
                                DO kB=1,this%nBas
                                    DO lB=1,this%nBas
                                        IF (ABS(this%integral(iB,jB,kB,lB)).lt.1e-40) CYCLE
                                        WRITE(1,'(4(x,I8),x,E24.16)') iB,jB,kB,lB,this%integral(iB,jB,kB,lB)
                                    ENDDO
                                ENDDO
                            ENDDO
                        ENDDO
                    ENDIF
                ENDIF
                CLOSE(1)
            END SUBROUTINE
            SUBROUTINE initializeEmpty(this,nBas,nIntegrals)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: nBas
                INTEGER, INTENT(in), OPTIONAL :: nIntegrals
                IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
                this%indexed=.FALSE.
                IF (present(nIntegrals)) this%indexed=.TRUE.
                this%nBas=nBas
                WRITE(6,'(A)') "Allocating memory for integral data"
                IF (this%indexed) THEN
                    WRITE(6,'(A)') "Store integrals in one dimensional array with auxilary containg indeces"
                    WRITE(6,'(A,I12,A)') "Memory: ",nIntegrals," real (8 byte)"
                    WRITE(6,'(A,I12,A)') "Memory: ",4*nIntegrals," integer (4 byte)"
                    ALLOCATE(this%indexedIntegral(nIntegrals))
                    ALLOCATE(this%indexList(4,nIntegrals))
                    this%indexedIntegral=0.d0
                    this%indexList=0
                    this%nIntegrals=nIntegrals
                ELSE
                    WRITE(6,'(A)') "Store integrals in four dimensional array with element position in arrray giving indeces"
                    WRITE(6,'(A,I12,A)') "Memory: ",nBas**4," real (8 byte)"
                    ALLOCATE(this%integral(nBas,nBas,nBas,nBas))
                    this%integral=0.d0
                ENDIF
                this%alive=.TRUE.
            END SUBROUTINE 
            SUBROUTINE setIndex(this,n,i,j,k,l)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: n,i,j,k,l
                this%indexList(1,n)=i
                this%indexList(2,n)=j
                this%indexList(3,n)=k
                this%indexList(4,n)=l
            END SUBROUTINE
            SUBROUTINE getIndex(this,n,i,j,k,l)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: n
                INTEGER, INTENT(out) :: i,j,k,l
                i=this%indexList(1,n)
                j=this%indexList(2,n)
                k=this%indexList(3,n)
                l=this%indexList(4,n)
            END SUBROUTINE
            SUBROUTINE getNIntegrals(this,nBas)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(out) :: nBas
                IF (.NOT.this%alive) STOP "cannot retrieve information from uninitialized array"
                nBas=this%nBas
            END SUBROUTINE
            SUBROUTINE getNBas(this,nBas)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(out) :: nBas
                IF (.NOT.this%alive) STOP "cannot retrieve information from uninitialized array"
                nBas=this%nBas
            END SUBROUTINE
            SUBROUTINE getValMatrix(this,i,j,k,l,val)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: i,j,k,l
                REAL*8, INTENT(out) :: val
                IF (.NOT.this%alive) STOP "cannot retrieve information from uninitialized array"
                val=this%integral(i,j,k,l)
            END SUBROUTINE 
            SUBROUTINE getValLine(this,i,j,k,l,val,stat)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(out) :: i,j,k,l
                REAL*8, INTENT(out) :: val
                INTEGER :: stat
                    IF (.NOT.this%alive) STOP "cannot retrieve information from uninitialized data"
                    IF (this%bin) THEN
                        READ(this%un,IOSTAT=stat) i,j,k,l,val
                    ELSE
                        READ(this%un,*,IOSTAT=stat) i,j,k,l,val
                    ENDIF
            END SUBROUTINE
            SUBROUTINE setValArr(this,i,j,k,l,val)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: i,j,k,l
                REAL*8, INTENT(in) :: val
                IF (.NOT.this%alive) STOP "cannot set information in uninitialized array"
                this%integral(i,j,k,l)=val
            END SUBROUTINE 
            SUBROUTINE setValList(this,i,val)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: i
                REAL*8, INTENT(in) :: val
                IF (.NOT.this%alive) STOP "cannot set information in uninitialized array"
                this%indexedIntegral(i)=val
            END SUBROUTINE 
            SUBROUTINE incValArr(this,i,j,k,l,val)
                IMPLICIT none
                class(twoInt_) :: this
                INTEGER, INTENT(in) :: i,j,k,l
                REAL*8, INTENT(in) :: val
                IF (.NOT.this%alive) STOP "cannot set information in uninitialized array"
                this%integral(i,j,k,l)=this%integral(i,j,k,l)+val
            END SUBROUTINE 
            SUBROUTINE incValList(this,n,val)
                IMPLICIT none 
                class(twoInt_) :: this
                INTEGER,INTENT(in) :: n
                REAL*8, INTENT(in) :: val
                this%indexedIntegral(n)=this%indexedIntegral(n)+val
            END SUBROUTINE
            SUBROUTINE destroy(this)
                IMPLICIT none
                class(twoInt_) :: this
                this%alive=.FALSE.
                IF (ALLOCATED(this%indexedIntegral)) DEALLOCATE(this%indexedIntegral)
                IF (ALLOCATED(this%indexList)) DEALLOCATE(this%indexList)
                IF (ALLOCATED(this%integral)) DEALLOCATE(this%integral)
            END SUBROUTINE
END MODULE
