!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE detIO
    IMPLICIT none
    PRIVATE
    PUBLIC det_
    TYPE det_
        INTEGER :: nState
        INTEGER :: nRAS
        INTEGER :: nDet
        LOGICAL*1, ALLOCATABLE :: detConf(:,:,:)
        REAL*8, ALLOCATABLE :: detCoef(:,:)
        CHARACTER*200 :: fich
        CONTAINS
            PROCEDURE :: printDet
            PROCEDURE :: printNonZeroDets
            PROCEDURE :: createFromFile
            PROCEDURE :: createFromFiles
            PROCEDURE :: getConf
            PROCEDURE :: getCoefOne
            PROCEDURE :: getCoefAll
            PROCEDURE :: getNumNRAS
            PROCEDURE :: getNumState
            PROCEDURE :: getNumDet
            PROCEDURE :: split
            GENERIC :: create => createFromFile,createFromFiles
            GENERIC :: getCoef => getCoefOne,getCoefAll
            
    END TYPE

    CONTAINS
        SUBROUTINE createFromFile(this,fich,fileFormatIn)
            IMPLICIT none
            CLASS(det_) :: this
            CHARACTER(len=*), INTENT(in) :: fich
            CHARACTER(len=*), INTENT(in), OPTIONAL :: fileFormatIn
            CHARACTER*100 :: token
            CHARACTER*1 :: fileFormat
            INTEGER :: iDet
            INTEGER :: iState
            fileFormat="b"
            this%fich=TRIM(fich)
            IF (PRESENT(fileFormatIn)) THEN
                fileFormat=fileFormatIn(1:1)
            ENDIF
            WRITE(6,'(4(A))') "Reading determinants from File:",TRIM(fich)," in ",fileFormat 
            IF (fileFormat.eq."b") THEN
                OPEN(1,FILE=TRIM(fich),STATUS="old",FORM="unformatted")
                READ(1) this%nState,this%nDet,this%nRAS
                WRITE(6,'(A,I0,A,I0,A,I0,A)') " -",this%nState," states; ",&
                  this%nRAS," active orbitals; ",&
                  this%nDet," determinants"
                ALLOCATE(this%detConf(this%nRAS,2,this%nDet))
                ALLOCATE(this%detCoef(this%nState,this%nDet))
                DO iDet=1,this%nDet
                    READ(1) token(1:this%nRas), (this%detCoef(iState,iDet),iState=1,this%nState)
                    CALL string2log(this%nRAS,TRIM(token),this%detConf(:,:,iDet))
                ENDDO
            ELSE
                OPEN(1,FILE=TRIM(fich),STATUS="old")
                READ(1,*) this%nState,this%nDet,this%nRAS
                WRITE(6,'(A,I0,A,I0,A,I0,A)') " -",this%nState," states; ",&
                  this%nRAS," active orbitals; ",&
                  this%nDet," determinants"
                ALLOCATE(this%detConf(this%nRAS,2,this%nDet))
                ALLOCATE(this%detCoef(this%nState,this%nDet))
                DO iDet=1,this%nDet
                    READ(1,*) token, (this%detCoef(iState,iDet),iState=1,this%nState)
                    CALL string2log(this%nRAS,TRIM(token),this%detConf(:,:,iDet))
                ENDDO
            ENDIF
            CLOSE(1)
        END SUBROUTINE 

        SUBROUTINE createFromFiles(this,fich,state,fileFormatIn)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER, INTENT(in) :: state
            CHARACTER(len=*), INTENT(in) :: fich
            CHARACTER(len=*), INTENT(in), OPTIONAL :: fileFormatIn
            INTEGER :: stat
            CHARACTER*200 :: fich2
            CHARACTER*100 :: token
            CHARACTER*1 :: fileFormat
            INTEGER :: iDet
            INTEGER :: iState
            INTEGER :: nState,nRAS,nDet,counter
            INTEGER :: temp
            REAL*8 :: coef
            fileFormat="b"
            this%fich=TRIM(fich)
            IF (PRESENT(fileFormatIn)) THEN
                STOP "ERROR I was to lazy to allow for non binary input files"
                fileFormat=fileFormatIn(1:1)
            ENDIF

            IF (.NOT.ALLOCATED(this%detConf)) THEN
                WRITE(fich2,'(A,A,I3.3)') TRIM(fich),"_",0
                WRITE(6,'(A,A)') "read configurations from ",TRIM(fich2)
                OPEN(1,FILE=TRIM(fich2),STATUS="old",FORM="unformatted")
                READ(1) this%nState,this%nDet,this%nRAS
                ALLOCATE(this%detConf(this%nRAS,2,this%nDet))
                ALLOCATE(this%detCoef(1,this%nDet))
                temp=-1
                DO iDet=1,this%nDet
                    IF (temp.ne.INT(REAL(iDet)/REAL(this%nDet)*10)) THEN
                        temp=INT(REAL(iDet)/REAL(this%nDet)*10)
                        WRITE(6,'(I3,A)') temp*10,"% Done"
                    ENDIF
                    READ(1) token(1:this%nRas)
                    CALL string2log(this%nRAS,TRIM(token),this%detConf(:,:,iDet))
                ENDDO
                CLOSE(1)
            ENDIF
            this%detCoef=0.d0
            WRITE(fich2,'(A,A,I3.3)') TRIM(fich),"_",state
            WRITE(6,'(A,I0,A,A)') "read coefficients for state ",state," from ",TRIM(fich2)
            OPEN(1,FILE=TRIM(fich2),STATUS="old",FORM="unformatted")
            READ(1) nState,nDet,nRAS
            IF (nState.ne.this%nState.OR.nRAS.ne.this%nRAS.OR.nDet.ne.this%nDet) THEN
                STOP "ERROR detConf header does not match detCoef header"
            ENDIF
            counter=0
            DO 
                READ(1,IOSTAT=stat) iDet,coef
                IF (stat.ne.0) EXIT
                counter=counter+1
                this%detCoef(1,iDet)=coef
            ENDDO
            WRITE(6,'(I0,A,A)') counter," Determinants read from",TRIM(fich2)
            CLOSE(1) 
        END SUBROUTINE

        SUBROUTINE getConf(this,conf)
            IMPLICIT none
            CLASS(det_) :: this
            LOGICAL*1, INTENT(inout) :: conf(this%nRAS,2,this%nDet)
            conf=this%detConf
        END SUBROUTINE

        SUBROUTINE getCoefAll(this,coef)
            IMPLICIT none
            CLASS(det_) :: this
            REAL*8, INTENT(inout) :: coef(this%nState,this%nDet)
            coef=this%detCoef
        END SUBROUTINE
        SUBROUTINE getCoefOne(this,coef,iStateIN)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER, INTENT(in),OPTIONAL :: iStateIN
            INTEGER :: iState
            REAL*8, INTENT(inout) :: coef(this%nDet)
            iState=1
            IF (PRESENT(iStateIN)) THEN
                iState=iStateIN
            ENDIF
            coef=this%detCoef(iState,:)
        END SUBROUTINE

        FUNCTION getNumNRAS(this) RESULT(nRAS)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER :: nRAS
            nRAS=this%nRAS
        END FUNCTION

        FUNCTION getNumState(this) RESULT(nState)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER :: nState
            nState=this%nState
        END FUNCTION

        FUNCTION getNumDet(this) RESULT(nDet)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER :: nDet
            nDet=this%nDet
        END FUNCTION

        SUBROUTINE printDet(this,iDet)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER, INTENT(in) :: iDet
            INTEGER :: i,iOrb
            CHARACTER*100 :: fmt
            WRITE(fmt,'(A,I0,A)') "(",this%nRAS,"(L))"
            DO i=1,2
                WRITE(6,fmt) (this%detConf(iOrb,i,iDet),iOrb=1,this%nRAS)
            ENDDO
        END SUBROUTINE

        SUBROUTINE printNonZeroDets(this,iStateIn)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER, INTENT(in), OPTIONAL :: iStateIn
            INTEGER :: iState,iDet
            CHARACTER*100 :: token
            LOGICAL*1 :: ras(this%nRas,2)
            iState=1
            IF(PRESENT(iStateIn)) THEN
                iState=iStateIn
            ENDIF
            DO iDet=1,this%nDet
                IF (ABS(this%detCoef(iState,iDet)).gt.1e-14) THEN
                    CALL log2string(this%nRas,this%detConf(:,:,iDet),token)
                    WRITE(6,'(A)') TRIM(token)
                ENDIF
            ENDDO

        END SUBROUTINE

        SUBROUTINE split(this)
            IMPLICIT none
            CLASS(det_) :: this
            INTEGER :: iDet,iState
            CHARACTER*200 :: fich
            CHARACTER*100 :: detString
            INTEGER :: counter

            WRITE(fich,'(A,A,I3.3)') TRIM(this%fich),"_",0
            OPEN(1,FILE=fich,STATUS="replace",FORM="unformatted")
            !OPEN(1,FILE=fich,STATUS="replace")
            WRITE(1) this%nState,this%nDet,this%nRAS
            !WRITE(1,'(3(x,I0))') this%nState,this%nDet,this%nRAS
            DO iDet=1,this%nDet
                CALL log2string(this%nRAS,this%detConf(:,:,iDet),detString)
                WRITE(1) TRIM(detString)
                !WRITE(1,'(A)') TRIM(detString)
            ENDDO
            CLOSE(1)
            DO iState=1,this%nState
                counter=0
                WRITE(fich,'(A,A,I3.3)') TRIM(this%fich),"_",iState
                OPEN(1,FILE=fich,STATUS="replace",FORM="unformatted")
                WRITE(1) this%nState,this%nDet,this%nRAS
                DO iDet=1,this%nDet
                    IF (ABS(this%detCoef(iState,iDet)).ge.1e-14) THEN
                        counter=counter+1
                        WRITE(1) iDet,this%detCoef(iState,iDet)
                    ENDIF
                ENDDO
                CLOSE(1)
                WRITE(6,'(I0,A,A)') counter," Determinants written to ",TRIM(fich)
            ENDDO
        END SUBROUTINE

        SUBROUTINE string2log(nRAS,string,num)
            IMPLICIT none
            INTEGER, INTENT(in) :: nRAS
            LOGICAL*1, INTENT(out) :: num(nRAS,2)
            CHARACTER(len=*),INTENT(in) :: string
            INTEGER :: i,n
            n=LEN(TRIM(string))
            num=.FALSE.
            DO i=1,nRAS
                IF (string(i:i).eq."3") THEN
                    num(i,1)=.TRUE.
                    num(i,2)=.TRUE.
                ELSEIF (string(i:i).eq."1") THEN
                    num(i,1)=.TRUE.
                ELSEIF (string(i:i).eq."2") THEN
                    num(i,2)=.TRUE.
                ENDIF
            ENDDO
        END SUBROUTINE
        SUBROUTINE log2string(nRAS,num,string)
            IMPLICIT none
            INTEGER, INTENT(in) :: nRAS
            LOGICAL*1, INTENT(in) :: num(nRAS,2)
            CHARACTER(len=*),INTENT(out) :: string
            INTEGER :: i,n
            CHARACTER*100 :: fmt
            WRITE(fmt,'(A,I0,A)') "(",nRas,"(A1))"
            WRITE(string,TRIM(fmt)) ("0",i=1,nRAS)
            DO i=1,nRas
                IF (num(i,1).AND.num(i,2)) THEN
                    string(i:i)="3"
                ELSEIF (num(i,1)) THEN
                    string(i:i)="1"
                ELSEIF (num(i,2)) THEN
                    string(i:i)="2"
                ENDIF
            ENDDO
        END SUBROUTINE
END MODULE
