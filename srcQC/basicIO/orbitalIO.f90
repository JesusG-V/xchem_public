!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE orbitalIO
    IMPLICIT none
    PRIVATE
    TYPE mat
        REAL*8,ALLOCATABLE :: coef(:,:)
    END TYPE
    PUBLIC orbital_
    TYPE orbital_
        PRIVATE
        INTEGER :: nSym
        INTEGER :: nOrb,nBas
        INTEGER, ALLOCATABLE :: symSize(:)
        TYPE(mat), ALLOCATABLE :: sym(:)
        LOGICAL :: alive=.FALSE.
        CONTAINS
            PROCEDURE :: readOrbital
            PROCEDURE :: writeOrbital
            PROCEDURE :: getNSym
            PROCEDURE :: getSymSizeMultiSym
            PROCEDURE :: getSymSizeSingleSym
            PROCEDURE :: setSym
            PROCEDURE :: initializeEmptySingleSym
            PROCEDURE :: initializeEmptyMultiSym
            PROCEDURE :: destroy
            PROCEDURE :: scaleOrb
            PROCEDURE :: getOrbitalVal
            PROCEDURE :: getOrbitalVec
            PROCEDURE :: setOrbitalVec
            PROCEDURE :: getOrbitalMat
            PROCEDURE :: getNOrb
            GENERIC :: initializeEmpty => initializeEmptySingleSym,initializeEmptyMultiSym
            GENERIC :: getOrbital => getOrbitalVal,getOrbitalVec,getOrbitalMat
            GENERIC :: setOrbital => setOrbitalVec
            GENERIC :: getSymSize => getSymSizeMultiSym,getSymSizeSingleSym
    END TYPE
                

    CONTAINS
        SUBROUTINE readOrbital(this,fich,sorte,QMpackageIn)
            IMPLICIT none
            class(orbital_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            CHARACTER(len=*), INTENT(in),OPTIONAL :: QMpackageIn
            LOGICAL :: bin,exists
            INTEGER :: iSym,nOrb,stat,jOrb,iOrb
            CHARACTER*100 :: token
            CHARACTER*3 :: QMpackage
            this%nOrb=nOrb
            this%nBas=nOrb
            QMpackage="cas"
            IF (present(QMPackageIn)) THEN
                QMpackage=QMpackageIn(1:3)
            ENDIF
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f") THEN
                bin=.FALSE.
            ELSE
                STOP "ERROR unknown filetype keyword" 
            ENDIF
            INQUIRE(FILE=TRIM(fich),EXIST=exists)
            IF (.NOT.exists) THEN
                WRITE(6,'(A,A,A)') "File: ",TRIM(fich)," does not exist"
                STOP "ERROR: file passed to orbital object does not exist"
            ENDIF
            IF (bin) THEN
                !DEBUG
                OPEN(1,file=fich,FORM="unformatted",STATUS="old")
                IF (ALLOCATED(this%symSize)) DEALLOCATE(this%symSize)
                IF (ALLOCATED(this%sym)) DEALLOCATE(this%sym)
                READ(1) this%nSym
                ALLOCATE(this%symSize(this%nSym)) 
                ALLOCATE(this%sym(this%nSym))
                READ(1) (this%symSize(iSym),iSym=1,this%nSym)
                DO iSym=1,this%nSym
                    nOrb=this%symSize(iSym)
                    ALLOCATE(this%sym(iSym)%coef(nOrb,nOrb))
                    READ(1) ((this%sym(iSym)%coef(jOrb,iOrb),jOrb=1,nOrb),iOrb=1,nOrb)
                ENDDO
                this%nOrb=SUM(this%symSize)
            ELSE
                OPEN(1,file=fich,STATUS="old")
                IF (ALLOCATED(this%symSize)) DEALLOCATE(this%symSize)
                IF (ALLOCATED(this%sym)) DEALLOCATE(this%sym)
                IF (QMpackage.eq."cas") THEN
                    DO 
                        READ(1,'(A)',IOSTAT=stat) token
                        IF (IS_IOSTAT_END(stat)) STOP "Faulty RasOrb file"
                        IF (INDEX(token,"#INFO").gt.0) EXIT
                    ENDDO
                    READ(1,*)
                    READ(1,*) this%nSym,this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    READ(1,*) (this%symSize(iSym),iSym=1,this%nSym)
                    DO 
                        READ(1,'(A)',IOSTAT=stat) token
                        IF (IS_IOSTAT_END(stat)) STOP "Faulty RasOrb file"
                        IF (INDEX(token,"#ORB").gt.0) EXIT
                    ENDDO
                    DO iSym=1,this%nSym
                        nOrb=this%symSize(iSym)
                        ALLOCATE(this%sym(iSym)%coef(nOrb,nOrb))
                        DO iOrb=1,nOrb
                                READ(1,*)
                                READ(1,'(5(1X,ES21.14))') (this%sym(iSym)%coef(jOrb,iOrb),jOrb=1,nOrb)
                        ENDDO
                    ENDDO
                    !WRITE(1,'(A)') "#OCC"
                    !WRITE(1,'(A)') "* OCCUPATION NUMBERS"
                    !DO iSym=1,this%nSym
                    !    nOrb=this%symSize(iSym)
                    !    WRITE(1,'(4(E18.12))') (0.d0,iOrb=1,nOrb)
                    !ENDDO
                    !WRITE(1,'(A)') "#ONE"
                    !WRITE(1,'(A)') "* ONE ELECTRON ENERGIES"
                    !DO iSym=1,this%nSym
                    !    nOrb=this%symSize(iSym)
                    !    WRITE(1,'(4(E18.12))') (0.d0,iOrb=1,nOrb)
                    !ENDDO
                ELSEIF (QMpackage.eq."pro") THEN
                    READ(1,*)
                    READ(1,*)
                    READ(1,*) this%nSym
                    ALLOCATE(this%symSize(this%nSym)) 
                    ALLOCATE(this%sym(this%nSym))
                    READ(1,*) (this%symSize(iSym),iSym=1,this%nSym)
                    DO iSym=1,this%nSym
                        nOrb=this%symSize(iSym)
                        ALLOCATE(this%sym(iSym)%coef(nOrb,nOrb))
                        DO iOrb=1,nOrb
                            READ(1,*) (this%sym(iSym)%coef(iOrb,jOrb),jOrb=1,nOrb)
                        ENDDO
                    ENDDO
                ENDIF
            ENDIF
            this%alive=.TRUE.
            CLOSE(1)
        END SUBROUTINE
        SUBROUTINE writeOrbital(this,fich,sorte,QMpackageIn)
            IMPLICIT none
            class(orbital_) :: this
            CHARACTER(len=*), INTENT(in) :: fich,sorte
            CHARACTER(len=*), INTENT(in),OPTIONAL :: QMpackageIn 
            LOGICAL :: bin
            INTEGER :: iSym,nOrb,counter,dump,nLines,length,jOrb,iOrb
            INTEGER, ALLOCATABLE :: order(:)
            CHARACTER*3 :: QMpackage
            QMpackage="cas"
            IF (present(QMPackageIn)) THEN
                QMpackage=QMpackageIn(1:3)
            ENDIF
            IF (.NOT.this%alive) STOP "An orbital orbital cannot be written before being read or initialized"
            IF (sorte(1:1).eq."b") THEN
                bin=.TRUE.
            ELSEIF (sorte(1:1).eq."f".or.sorte(1:1).eq."m") THEN
                bin=.FALSE.
            ELSE
                STOP "unknown keyword filetype keyword" 
            ENDIF
            IF (sorte(1:1).eq."b") THEN
                OPEN(1,file=fich,FORM="unformatted",STATUS="replace")
                WRITE(1) this%nSym
                WRITE(1) (this%symSize(iSym),iSym=1,this%nSym)
                DO iSym=1,this%nSym     
                    nOrb=this%symSize(iSym)
                    WRITE(1) ((this%sym(iSym)%coef(jOrb,iOrb),jOrb=1,nOrb),iOrb=1,nOrb)
                ENDDO
            ELSEIF(sorte(1:1).eq."f") THEN
                OPEN(1,file=fich,STATUS="replace")
                IF (QMpackage.eq."cas") THEN
                    WRITE(1,'(A)') "#INPORB 2.2"
                    WRITE(1,'(A)') "#INFO"
                    WRITE(1,'(A)') "* Symetry Specfications"
                    WRITE(1,'(3(x,I7))') 0,this%nSym,0
                            
                    WRITE(1,'(*(x,I7))') (this%symSize(iSym),iSym=1,this%nSym)
                    WRITE(1,'(*(x,I7))') (this%symSize(iSym),iSym=1,this%nSym)
                    WRITE(1,'(A,x,A,x,A)') "* Written by Markus' orbitalClass"
                    WRITE(1,'(A)') "#ORB"        
                    counter=0
                    DO iSym=1,this%nSym
                        nOrb=this%symSize(iSym)
                        DO iOrb=1,nOrb
                            counter=counter+1
                            WRITE(1,'(A,3(x,I4))') "* ORBITAL",iSym,iOrb,counter
                            WRITE(1,'(5(1X,ES21.14))') (this%sym(iSym)%coef(jOrb,iOrb),jOrb=1,nOrb)
                        ENDDO
                    ENDDO
                    WRITE(1,'(A)') "#OCC"
                    WRITE(1,'(A)') "* OCCUPATION NUMBERS"
                    DO iSym=1,this%nSym
                        IF (this%symSize(iSym).eq.0) CYCLE
                        nOrb=this%symSize(iSym)
                        WRITE(1,'(5(1X,ES21.14))') (0.d0,iOrb=1,nOrb)
                    ENDDO
                    WRITE(1,'(A)') "#OCHR"
                    WRITE(1,'(A)') "* OCCUPATION NUMBERS"
                    DO iSym=1,this%nSym
                        IF (this%symSize(iSym).eq.0) CYCLE
                        nOrb=this%symSize(iSym)
                        WRITE(1,'(10(1X,F7.4))') (0.d0,iOrb=1,nOrb)
                    ENDDO
                    WRITE(1,'(A)') "#ONE"
                    WRITE(1,'(A)') "* ONE ELECTRON ENERGIES"
                    counter=0
                    DO iSym=1,this%nSym
                        IF (this%symSize(iSym).eq.0) CYCLE
                        nOrb=this%symSize(iSym)
                        WRITE(1,'(10(1X,ES11.4))') (REAL(counter+iOrb),iOrb=1,nOrb)
                        counter=counter+nOrb
                    ENDDO
                    WRITE(1,'(A)') "#INDEX"
                    DO iSym=1,this%nSym
                        dump=0
                        WRITE(1,'(A)') "* 1234567890"
                        IF (this%symSize(iSym).eq.0) CYCLE
                        nLines=INT(REAL(this%symSize(iSym))/REAL(10))+1
                        counter=this%symSize(iSym)
                        DO iOrb=1,nLines
                            length=MIN(10,counter)
                            WRITE(1,'(I1,x,10(A1))') dump,("s",jOrb=1,length)
                            dump=dump+1
                            IF (dump.eq.10) dump=0
                            counter=counter-10
                            IF (counter.le.0) EXIT
                        ENDDO
                    ENDDO
                ELSEIF (QMpackage.eq."pro") THEN
                    WRITE(1,'(A)') "BEGIN_DATA,"
                    WRITE(1,'(A)') "# MATRIX CURRORB            ORBITALS NATURAL SYMMETRY=1"
                    DO iSym=1,this%nSym
                        nOrb=this%symSize(iSym)
                        DO iOrb=1,nOrb
                            WRITE(1,'(5(E30.20E3,A1))') (this%sym(iSym)%coef(iOrb,jOrb),",",jOrb=1,nOrb)
                        ENDDO
                    ENDDO
                    WRITE(1,'(A)') "END_DATA,"
                ENDIF
            ENDIF
            CLOSE(1)
        END SUBROUTINE
        SUBROUTINE getNSym(this,nSym)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(out) :: nSym
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            nSym=this%nSym
        END SUBROUTINE
        SUBROUTINE getSymSizeMultiSym(this,symSize)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER,ALLOCATABLE, INTENT(out) :: symSize(:)
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            IF (ALLOCATED(symSize)) DEALLOCATE(symSize)
            ALLOCATE(symSize(this%nSym))
            symSize=this%symSize
        END SUBROUTINE
        SUBROUTINE getSymSizeSingleSym(this,symSize)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(out) :: symSize
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            symSize=this%symSize(1)
        END SUBROUTINE
        SUBROUTINE getSym(this,s,arr)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: s
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:,:)
            INTEGER :: nOrb
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            IF(ALLOCATED(arr)) DEALLOCATE(arr)
            nOrb=this%symSize(s)
            ALLOCATE(arr(nOrb,nOrb))
            arr=0.d0
            arr=this%sym(s)%coef 
        END SUBROUTINE
        SUBROUTINE getOrbitalMat(this,arr,symIn)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in), OPTIONAL :: symIn
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:,:)
            INTEGER :: sym,nBas
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            IF(ALLOCATED(arr)) DEALLOCATE(arr)
            sym=1
            IF (PRESENT(symIn)) sym=symIn
            nBas=this%symSize(sym)
            ALLOCATE(arr(nBas,nBas))
            arr=this%sym(sym)%coef
        END SUBROUTINE

        SUBROUTINE getOrbitalVec(this,arr,orb,symIn)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: orb
            INTEGER, INTENT(in), OPTIONAL :: symIn
            REAL*8, ALLOCATABLE, INTENT(out) :: arr(:)
            INTEGER :: sym,nBas
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            IF(ALLOCATED(arr)) DEALLOCATE(arr)
            sym=1
            IF (PRESENT(symIn)) sym=symIn
            nBas=this%symSize(sym)
            ALLOCATE(arr(nBas))
            arr=this%sym(sym)%coef(:,orb)
        END SUBROUTINE
        SUBROUTINE setOrbitalVec(this,arr,orb,symIn)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: orb
            INTEGER, INTENT(in), OPTIONAL :: symIn
            REAL*8, ALLOCATABLE, INTENT(in) :: arr(:)
            INTEGER :: sym,nBas
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            sym=1
            IF (PRESENT(symIn)) sym=symIn
            this%sym(sym)%coef(:,orb)=arr
        END SUBROUTINE


        SUBROUTINE getOrbitalVal(this,val,orb,coef,symIn)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: orb,coef
            INTEGER, INTENT(in), OPTIONAL :: symIn
            REAL*8, INTENT(out) :: val
            INTEGER :: sym,nBas
            IF (.NOT.this%alive) STOP "Information cannot be read from an orbital before being read or initialized"
            sym=1
            IF (PRESENT(symIn)) sym=symIn
            nBas=this%symSize(sym)
            val=this%sym(sym)%coef(coef,orb)
        END SUBROUTINE
        SUBROUTINE setSym(this,s,arr)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: s
            REAL*8, ALLOCATABLE, INTENT(in) :: arr(:,:)
            IF (.NOT.this%alive) STOP "An orbital orbital cannot be modified before being read or initialized"
            IF (.NOT.ALLOCATED(arr)) STOP "Only an allocated array can be written to a symmetry"
            IF (SIZE(arr,1).NE.this%symSize(s)) THEN
                WRITE(6,'(A,I0)') "Trying to write array of size ",SIZE(arr,1)
                WRITE(6,'(A,I0)') "to object of size ",this%symSize(s)
                STOP "Dimension of Matrix and Orbital symmetry dont match"           
            ENDIF
            this%sym(s)%coef=arr 
        END SUBROUTINE 
        SUBROUTINE initializeEmptyMultiSym(this,symSize)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, ALLOCATABLE, INTENT(in) :: symSize(:)
            INTEGER :: iSym,nOrb
            IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
            IF (.NOT.(ALLOCATED(symSize))) STOP "Cannot use unallocated matrix of symmetry sizes to initialize"
            this%nSym=SIZE(symSize)
            ALLOCATE(this%symSize(this%nSym))
            ALLOCATE(this%sym(this%nSym))
            this%symSize=symSize
            DO iSym=1,this%nSym     
                nOrb=symSize(iSym)
                ALLOCATE(this%sym(iSym)%coef(nOrb,nOrb))
                this%sym(iSym)%coef=0.d0
            ENDDO           
            this%alive=.TRUE.
        END SUBROUTINE 
        SUBROUTINE initializeEmptySingleSym(this,symSize)
            IMPLICIT none
            class(orbital_) :: this
            INTEGER, INTENT(in) :: symSize
            INTEGER :: iSym,nOrb
            IF (this%alive) STOP "An array that has been read or initialized cannot be initialized"
            this%nSym=1
            ALLOCATE(this%symSize(this%nSym))
            ALLOCATE(this%sym(this%nSym))
            this%symSize(1)=symSize
            DO iSym=1,this%nSym     
                nOrb=this%symSize(iSym)
                ALLOCATE(this%sym(iSym)%coef(nOrb,nOrb))
                this%sym(iSym)%coef=0.d0
            ENDDO           
            this%alive=.TRUE.
        END SUBROUTINE 
        SUBROUTINE scaleOrb(this,scalingFactor)
            IMPLICIT none
            class(orbital_) :: this
            REAL*8, INTENT(in) :: scalingFactor
            INTEGER :: iSym 
            IF (.NOT.this%alive) STOP "Cannot rescale uninitialized Orbital"
            DO iSym=1,this%nSym
                this%sym(iSym)%coef=this%sym(iSym)%coef*scalingFactor       
            ENDDO 
        END SUBROUTINE 
        SUBROUTINE destroy(this)
            IMPLICIT none
            class(orbital_) :: this
            this%alive=.FALSE.
            DEALLOCATE(this%symSize,this%sym)
        END SUBROUTINE 
        SUBROUTINE getNOrb(this,nOrb)
            IMPLICIT none
            CLASS(orbital_) :: this
            INTEGER, INTENT(out) ::nOrb
            nOrb=this%nOrb
        END SUBROUTINE

END MODULE
