!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM subtractDiagonal
    USE oneIntIO
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: oneIntFile
    CHARACTER*200 :: ovFile
    CHARACTER*6 :: outputFormat
    CHARACTER*6 :: inputFormat
    REAL*8 :: diagonal

    TYPE(oneInt_) :: oneInt, ov
    INTEGER :: symSize
    INTEGER :: i
    INTEGER :: j
    REAL*8, ALLOCATABLE :: oneIntMat(:,:)
    REAL*8, ALLOCATABLE :: ovMat(:,:)

    NAMELIST /INPUT/ oneIntFile,&
        diagonal,&
        ovFile,&
        outputFormat,&
        inputFormat

    outputFormat="b"
    inputFormat="b"
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    CALL oneInt%readOneInt(TRIM(oneIntFile),inputFormat(1:1),.TRUE.,1,"o") 
    CALL oneInt%getSymSize(symSize)
    CALL oneInt%getSym(oneIntMat)
    
    CALL ov%readOneInt(TRIM(oneIntFile),inputFormat(1:1),.TRUE.,1,"o") 
    CALL ov%getSym(ovMat)
    
    DO i=1,symSize
        DO j=1,symSize
            IF (i.eq.j) THEN
                oneIntMat(i,j)=diagonal*ovMat(i,j)-oneIntMat(i,j)
            ELSE
                oneIntMat(i,j)=(-1)*oneIntMat(i,j)
            ENDIF
        ENDDO
    ENDDO
    CALL oneInt%destroy
    CALL oneInt%initializeEmpty(symSize)
    CALL oneInt%setSym(oneIntMat)
    CALL oneInt%writeOneInt(TRIM(oneIntFile),outputFormat(1:1),.TRUE.,1,"rch")
END PROGRAM
