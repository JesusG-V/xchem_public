!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntOrb2OperatorMatrix
!PROGRAM THAT FROM A ONE ELECTRON 
!INTEGRAL TABLE (oneInt_ ) AND A REDUCED DENSITY MATRIX (oneIntRDM_)
!CREATES THE MATRIX ELEMENTS
    USE RDMIO
    USE oneIntIO
    USE class_inputString
    USE stopWatch
    IMPLICIT none
    LOGICAL :: debug=.FALSE.
    INTEGER :: stat,stat1,stat2
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(RDM_) :: oneIntRDM
    TYPE(oneInt_) :: oneInt
    TYPE(oneInt_) :: output
    TYPE(inputString) :: inString
    TYPE(inputString) :: outString
    TYPE(inputString) :: orbitalString
    TYPE(inputString) :: printModeString
    TYPE(inputString) :: scaleString
    CHARACTER*200, ALLOCATABLE :: inFiles(:)
    CHARACTER*200, ALLOCATABLE :: outFiles(:)
    CHARACTER*200, ALLOCATABLE :: printModeMat(:)
    INTEGER, ALLOCATABLE :: scaleMat(:)
    CHARACTER*200 :: inputFile,fmt,token
    CHARACTER*200 :: ciVectorFile
    CHARACTER*200 :: oneIntRDMFile
    CHARACTER*200 :: oneIntFiles
    CHARACTER*200 :: outputFiles
    CHARACTER*200 :: activeOrbitals
    CHARACTER*200 :: printMode
    CHARACTER*200 :: scaleFactor
    
    REAL*8, ALLOCATABLE :: operatorMatrix(:,:)        
    REAL*8 :: integral
    
    INTEGER :: nFiles,iFiles
    INTEGER :: nState,iState,jState
   
    REAL*8,ALLOCATABLE :: rdm(:,:)
    LOGICAL :: empty

    INTEGER,ALLOCATABLE :: orbitals(:),ras3Orbitals(:)
    INTEGER :: nOrb,iOrb
    !INTEGER :: symSize
    
    INTEGER :: occupied,local,ras3
           
    INTEGER :: i1,j1
    INTEGER :: i2,j2
    INTEGER :: tempI(1),tempJ(1)
    
    INTEGER :: augtype
    INTEGER :: nLastState
    INTEGER, ALLOCATABLE :: lastStates(:)
    
    NAMELIST /INPUT/ oneIntRDMFile,&
      oneIntFiles,&
      outputFiles,&
      activeOrbitals,&
      printMode,&
      scaleFactor
    
    inputFile=""
    i1=0;i1=0;i2=0;j2=0;tempI=0;tempJ=0

    ras3=2
    activeOrbitals="" 
    augType=0
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    !INQUIRE(FILE="aug.dat",EXIST=exists)
    !IF (exists) THEN
    !    WRITE(6,'(A)') "aug.dat exists. Only compute matrix elements in compliance" 
    !    WRITE(6,'(A)') "with augmentation type and specified states"
    !    OPEN(1,FILE="aug.dat",STATUS="old")
    !    READ(1,*) nLastState,augType
    !    IF (augType.gt.0.AND.nLastState.gt.0) THEN
    !        ALLOCATE(lastStates(nLastState))
    !        READ(1,*) (lastStates(i1),i1=1,nLastState)
    !    ENDIF
    !ELSE
    !    WRITE(6,'(A)') "aug.dat does not exists." 
    !    WRITE(6,'(A)') "Compute all matrix elements."
    !ENDIF
    
    CALL printModeString%setString(TRIM(printMode))
    CALL printModeString%process("-")
    CALL printModeString%getArray(printModeMat)
    
    CALL scaleString%setString(TRIM(scaleFactor))
    CALL scaleString%process("-")
    CALL scaleString%getArray(scaleMat)
    
    CALL oneIntRDM%initializeOld(TRIM(oneIntRDMFile),"b",2)
    CALL oneIntRDM%getOccupied(occupied)
    CALL oneIntRDM%getStates(nState)

    local=occupied-ras3
    IF (LEN(TRIM(activeOrbitals)).eq.0) THEN
        ALLOCATE(orbitals(occupied))
        DO iOrb=1,occupied
            orbitals(iOrb)=iOrb
        ENDDO
        WRITE(6,'(A)') "No active ras3 orbitals were specified:"
        WRITE(6,'(A)') "assumed to be first two nonlocalized orbitals. Thus:"
        WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
        WRITE(fmt,'(A,I0,A)') "(",occupied,"(x,I4))"
        WRITE(6,TRIM(fmt)) (orbitals(iOrb),iOrb=1,occupied)
    ELSE
        CALL orbitalString%setString(activeOrbitals)
        CALL orbitalString%process("-")
        CALL orbitalString%getArray(ras3Orbitals)
        CALL orbitalString%sizeString(nOrb)
        ALLOCATE(orbitals(local+nOrb))
        DO iOrb=1,local
            orbitals(iOrb)=iOrb
        ENDDO
        DO iOrb=local+1,local+nOrb
            orbitals(iOrb)=ras3Orbitals(iOrb-local)
        ENDDO
        WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
        WRITE(fmt,'(A,I0,A)') "(",local+nOrb,"(x,I4))"
        WRITE(6,TRIM(fmt)) (orbitals(iOrb),iOrb=1,local+nOrb)
    ENDIF
    
    
    CALL inString%setString(oneIntFiles)
    CALL outString%setString(outputFiles)
    CALL inString%process("-")
    CALL outString%process("-")
    CALL inString%sizeString(nFiles)
    CALL outString%sizeString(iOrb)
    IF (nFiles.ne.iOrb) THEN        
        STOP "One and only one ouput files must be provided for each input file"
    ENDIF
    
    CALL inString%getArray(inFiles)
    CALL outString%getArray(outFiles)
    
    !WRITE(6,'(A,I1)') "Augmentation Type=",augType
    !IF (augType.gt.0) THEN
    !    WRITE(fmt,'(A,I0,A)') "(A,",local+nOrb,"(x,I4))"
    !    WRITE(6,TRIM(fmt)) "Last states:",(lastStates(i1),i1=1,nLastState)
    !ENDIF
    
    ALLOCATE(operatorMatrix(nState,nState)) 
    ALLOCATE(rdm(nState,nState))
    CALL timer%initialize()
    DO iFiles=1,nFiles
        WRITE(token,'(A,A)') "Working on: ",TRIM(inFiles(iFiles))
        CALL timer%time(TRIM(token))
        CALL oneInt%readOneInt(TRIM(inFiles(iFiles)),"b",3)
        operatorMatrix=0.d0
        rdm=0.0
        DO  
            CALL oneInt%getRecord(i1,j1,integral,stat1)
            IF (stat1.ne.0) EXIT
            IF (.NOT.(ANY(orbitals.eq.i1).AND.ANY(orbitals.eq.j1))) THEN
                CYCLE
            ENDIF
            tempI=MINLOC(ABS(orbitals-i1))
            tempJ=MINLOC(ABS(orbitals-j1))
            i1=tempI(1)
            j1=tempJ(1)
            
            DO 
                CALL oneIntRDM%getNextRecordInfo(i2,j2,empty,stat2)
                IF (stat2.ne.0) THEN
                    WRITE(6,'(A,2(x,I4),A)') "ERROR: could not match integral:",i1,j1," to guga coefs."
                    STOP "ERROR: check output file for info"        
                ENDIF
                IF (i1.ne.i2.OR.j1.ne.j2) THEN
                    CYCLE
                ENDIF
                IF (empty) EXIT
                CALL oneIntRDM%getNextRecordData(nState,rdm,"R")
                operatorMatrix=operatorMatrix+rdm*integral
                EXIT
            ENDDO

        ENDDO
        CALL oneInt%destroy
        CALL output%initializeEmpty(nState)
        CALL output%setSym(operatorMatrix)
        CALL output%writeOneInt(TRIM(outFiles(iFiles)),"f",.TRUE.,1,printModeMat(iFiles),scaleMat(iFiles))
        CALL oneIntRDM%reset()
        CALL output%destroy
    ENDDO
    WRITE(6,'(A)') "Happy"
END PROGRAM 
