!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntOrb2OperatorMatrix
!PROGRAM THAT FROM A LIST OF CI VECTORS (ciVector_), A TWO ELECTRON 
!INTEGRAL TABLE (twoInt_ ) AND A MAP FROM INTEGRALS TO CSF PAIRS (twoInt2CSFMap_)
!CREATES THE MATRIX ELEMENTS FOR THESE CI VECTORS
    USE ciVectorIO
    USE RDMIO
    USE oneIntIO
    USE twoIntIO
    USE stopWatch
    USE class_inputString
    IMPLICIT none
    LOGICAL :: debug=.FALSE.
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(ciVector_) :: ciVector
    TYPE(RDM_) :: twoIntRDM
    TYPE(twoInt_) :: twoInt
    TYPE(oneInt_) :: output
    TYPE(inputString) :: orbitalString
    CHARACTER*200 :: inputFile,fmt,token
    CHARACTER*200 :: ciVectorFile
    CHARACTER*200 :: twoIntRDMFile
    CHARACTER*200 :: twoIntFile
    CHARACTER*200 :: outputFile
    CHARACTER*200 :: activeOrbitals
    
    REAL*8, ALLOCATABLE :: operatorMatrix(:,:)        
    REAL*8 :: integral

    INTEGER :: nState,iState,jState

    REAL*8,ALLOCATABLE :: rdm(:,:)
    LOGICAL :: empty
           
    INTEGER,ALLOCATABLE :: orbitals(:),ras3Orbitals(:)
    INTEGER :: nOrb,iOrb
    
    INTEGER :: occupied,local,ras3
    INTEGER :: nIntegral
    
    INTEGER :: i1,j1,k1,l1
    INTEGER :: i2,j2,k2,l2
    INTEGER :: tempI(1),tempJ(1),tempK(1),tempL(1)
    
    INTEGER :: augtype
    INTEGER :: nLastState
    INTEGER, ALLOCATABLE :: lastStates(:)
    INTEGER :: symFct,iSymFct
    INTEGER :: symmetryFactor

    !INTEGER :: nStatePairs,iSP
    !INTEGER, ALLOCATABLE :: statePairs(:,:)

    NAMELIST /INPUT/ twoIntRDMFile,&
      twoIntFile,&
      outputFile,&
      activeOrbitals
    
    ras3=2
    activeOrbitals="" 
    augType=0
    nLastState=0
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
    
    INQUIRE(FILE="aug.dat",EXIST=exists)
    IF (exists) THEN
        WRITE(6,'(A)') "aug.dat exists. Only compute matrix elements in compliance" 
        WRITE(6,'(A)') "with augmentation type and specified states"
        OPEN(1,FILE="aug.dat",STATUS="old")
        READ(1,*) nLastState,augType
        IF (augType.gt.0.AND.nLastState.gt.0) THEN
            ALLOCATE(lastStates(nLastState))
            READ(1,*) (lastStates(i1),i1=1,nLastState)
        ENDIF
    ELSE
        WRITE(6,'(A)') "aug.dat does not exists." 
        WRITE(6,'(A)') "Compute all matrix elements."
    ENDIF
    
    CALL twoInt%readTwoInt(TRIM(twoIntFile),"b",2)
    CALL twoIntRDM%initializeOld(TRIM(twoIntRDMFile),"b",1)
    CALL twoIntRDM%getOccupied(occupied)
    CALL twoIntRDM%getStates(nState)


    local=occupied-ras3
    
    IF (LEN(TRIM(activeOrbitals)).eq.0) THEN
        ALLOCATE(orbitals(occupied))
        DO iOrb=1,occupied
            orbitals(iOrb)=iOrb
        ENDDO
        WRITE(6,'(A)') "No active ras3 orbitals were specified:"
        WRITE(6,'(A)') "assumed to be first two nonlocalized orbitals"
        WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
        WRITE(fmt,'(A,I0,A)') "(",occupied,"(x,I4))"
        WRITE(6,TRIM(fmt)) (orbitals(iOrb),iOrb=1,occupied)
    ELSE
        CALL orbitalString%setString(activeOrbitals)
        CALL orbitalString%process("-")
        CALL orbitalString%getArray(ras3Orbitals)
        CALL orbitalString%sizeString(nOrb)
        ALLOCATE(orbitals(local+nOrb))
        DO iOrb=1,local
            orbitals(iOrb)=iOrb
        ENDDO
        DO iOrb=local+1,local+nOrb
            orbitals(iOrb)=ras3Orbitals(iOrb-local)
        ENDDO
        WRITE(6,'(A)') "In calculation of states only consider orbtitals:"
        WRITE(fmt,'(A,I0,A)') "(",local+nOrb,"(x,I4))"
        WRITE(6,TRIM(fmt)) (orbitals(iOrb),iOrb=1,local+nOrb)
    ENDIF

    WRITE(6,'(A,I1)') "Augmentation Type=",augType
    IF (augType.gt.0) THEN
        WRITE(fmt,'(A,I0,A)') "(A,",local+nOrb,"(x,I4))"
        WRITE(6,TRIM(fmt)) "Last states:",(lastStates(i1),i1=1,nLastState)
    ENDIF
    
    !nStatePairs=0
    !IF (augType.ne.0) THEN
    !    WRITE(6,'(A)') "Identify mich matrix elements must be calculated,&
    !      in accordance with laststate information found in aug.dat"
    !    DO iState=1,nState
    !        DO jState=1,iState
    !            IF (augType.eq.1) THEN
    !                IF (ANY(lastStates.eq.iState).OR.ANY(lastStates.eq.jState)) THEN
    !                    nStatePairs=nStatePairs+1
    !                ENDIF
    !            ELSEIF (augType.eq.2) THEN
    !                IF (ANY(lastStates.eq.iState).AND.ANY(lastStates.eq.jState)) THEN
    !                    nStatePairs=nStatePairs+1
    !                ENDIF
    !            ENDIF
    !        ENDDO
    !    ENDDO
    !    ALLOCATE(statePairs(2,nStatePairs))
    !    iSP=0
    !    DO iState=1,nState
    !        DO jState=1,iState
    !            IF (augType.eq.1) THEN
    !                IF (ANY(lastStates.eq.iState).OR.ANY(lastStates.eq.jState)) THEN
    !                    iSP=iSP+1
    !                    statePairs(1,iSP)=iState
    !                    statePairs(2,iSP)=jState
    !                ENDIF
    !            ELSEIF (augType.eq.2) THEN
    !                IF (ANY(lastStates.eq.iState).AND.ANY(lastStates.eq.jState)) THEN
    !                    iSP=iSP+1
    !                    statePairs(1,iSP)=iState
    !                    statePairs(2,iSP)=jState
    !                ENDIF
    !            ENDIF
    !        ENDDO
    !    ENDDO
    !    WRITE(6,'(I0,A,I0,A)') nStatePairs," of ",nState*(nState+1)/2," matrix Elements must be calculated"
    !    WRITE(6,'(A)') "DEBUG: which are:"
    !    DO iSP=1,nStatePairs
    !        WRITE(6,'(I0,x,I0)') statePairs(1,iSP),statePairs(2,iSP)
    !    ENDDO
    !ENDIF

    ALLOCATE(operatorMatrix(nState,nState))
    ALLOCATE(rdm(nState,nState))
    operatorMatrix=0.d0
    nIntegral=0
    CALL timer%initialize()
    DO  
        CALL twoInt%getRecord(i1,j1,k1,l1,integral,stat)
        IF (stat.ne.0) THEN
            WRITE(6,'(A,I8,A)') "End of integral file reached after reading ",nIntegral," integrals"
            EXIT
        ENDIF
        nIntegral=nIntegral+1

        IF (.NOT.(ANY(orbitals.eq.i1).AND.ANY(orbitals.eq.j1).AND.&
          ANY(orbitals.eq.k1).AND.ANY(orbitals.eq.l1))) THEN
            CYCLE
        ENDIF
        tempI=MINLOC(ABS(orbitals-i1))
        tempJ=MINLOC(ABS(orbitals-j1))
        tempK=MINLOC(ABS(orbitals-k1))
        tempL=MINLOC(ABS(orbitals-l1))
        i1=tempI(1)
        j1=tempJ(1)
        k1=tempK(1)
        l1=tempL(1)
        DO 
            CALL twoIntRDM%getNextRecordInfo(i2,j2,k2,l2,empty,stat)
            !!DEBUG
            !WRITE(6,'(A,4(x,I0),A,4(x,I0))') "Debug",i2,j2,k2,l2," vs ",i1,j1,k1,l1
            !IF (i1.ne.i2.OR.j1.ne.j2.OR.k1.ne.k2.OR.l1.ne.l2) WRITE(6,'(A)') "not equal"
            !!DEBUG
            IF (stat.ne.0) THEN
                WRITE(6,'(A,4(x,I4),A)') "ERROR: could not match integral:",i1,j1,k1,l1," to guga coefs."
                STOP "ERROR: check output file for info"
            ENDIF
            IF (i1.ne.i2.OR.j1.ne.j2.OR.k1.ne.k2.OR.l1.ne.l2) CYCLE
            symFct=symmetryFactor(i1,j1,k1,l1)
            CALL twoIntRDM%back
            DO iSymFct=1,symFct
                CALL twoIntRDM%getNextRecordInfo(i2,j2,k2,l2,empty,stat)

                !WRITE(6,'(A,I0,A,4(x,I0))') "DEBUG",iSymFct," : ",i2,j2,k2,l2

                IF (.NOT.empty) THEN
                    !CALL timer%time("time check")
                    !BREAK OBJECT MODULIZATION TO SPEED UP CODE
                    READ(1) ((rdm(jState,iState),jState=1,iState),iState=1,nState)
                    !CALL timer%time("record reading done")

                    IF (augType.eq.0) THEN
                        !1
                        !11
                        !111
                        !1111
                        !11111
                        DO iState=1,nState
                            DO jState=1,iState
                                operatorMatrix(jState,iState)=operatorMatrix(jState,iState)+rdm(jState,iState)*integral
                            ENDDO
                        ENDDO
                    ELSEIF(augType.eq.1) THEN
                        !0
                        !00
                        !000
                        !1111
                        !00000
                        DO iState=lastStates(1),lastStates(nLastState)
                            DO jState=1,iState
                                operatorMatrix(jState,iState)=operatorMatrix(jState,iState)+rdm(jState,iState)*integral
                            ENDDO
                        ENDDO
                    ELSE
                        !0
                        !00
                        !000
                        !0001
                        !00011
                        DO iState=lastStates(1),lastStates(nLastState)
                            DO jState=lastStates(1),iState
                                operatorMatrix(jState,iState)=operatorMatrix(jState,iState)+rdm(jState,iState)*integral
                            ENDDO
                        ENDDO
                    ENDIF

                    !CALL timer%time("matrix element evaluation done")
                    !ELSE
                    !    DO iSP=1,nStatePairs
                    !        operatorMatrix(statePairs(1,iSP),statePairs(2,iSP))=&
                    !          operatorMatrix(statePairs(1,iSP),statePairs(2,iSP))+&
                    !          rdm(statePairs(1,iSP),statePairs(2,iSP))*integral
                    !    ENDDO
                    !ENDIF
                ENDIF
            ENDDO
            EXIT
        ENDDO
    ENDDO
    CALL timer%time("Matrix done")
    CALL output%initializeEmpty(nState)
    CALL output%setSym(operatorMatrix)
    CALL output%writeOneInt(TRIM(outputFile),"f")
    WRITE(6,'(A)') "Happy"
END PROGRAM 
FUNCTION symmetryFactor(i,j,k,l) RESULT (symFct)
    IMPLICIT none
    INTEGER,INTENT(in) :: i,j,k,l
    INTEGER :: symFct
    symFct=1
    IF ((i.eq.j).AND.(k.eq.l).AND.(i.ne.k)) THEN
        symFct=2
    ELSEIF ((i.ne.j).AND.(k.eq.l)) THEN
        symFct=4
    ELSEIF ((i.eq.j).AND.(k.ne.l)) THEN
        symFct=4
    ELSEIF ((i.ne.j).AND.(k.ne.l)) THEN
        symFct=4
        IF (.NOT.(((i.eq.k).AND.(j.eq.l)).OR.((i.eq.l).AND.(j.eq.k)))) THEN
            symFct=8
        ENDIF
    ENDIF
END FUNCTION
