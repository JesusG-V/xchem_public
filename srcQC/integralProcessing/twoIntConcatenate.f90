!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntConcatanate
        USE twoIntIO
        IMPLICIT NONE
        LOGICAL :: exists
        LOGICAL :: suff
        INTEGER :: nFiles,stat
        CHARACTER*200 :: stem,nFileString
        CHARACTER*200 :: suffixFile
        CHARACTER*100 :: suffix
        CHARACTER*200 :: suffixInfo

        TYPE (twoInt_) :: g
        
        CHARACTER*200 :: fileName,token
        INTEGER :: iFile,counter,tCounter
        INTEGER :: i,j,k,l,nOrb,key
        REAL*8 :: val
        LOGICAL :: keyIndex
        LOGICAL :: keyIndexRef
        CHARACTER*3 :: keyChar
          
        suff=.FALSE.
        CALL GETARG(1,stem) 
        CALL GETARG(2,suffixFile)
        IF (LEN(TRIM(suffixFile)).gt.0) THEN
            suff=.TRUE.
            OPEN(101,FILE=TRIM(suffixFile),STATUS="old")
        ENDIF
    
        WRITE(6,'(A,A)') "Openin output file: ", TRIM(stem)
        OPEN(100,file=TRIM(stem),form="unformatted",STATUS="replace")
        iFile=0
        tCounter=0
        IF (suff) THEN
            DO
                iFile=iFile+1
                READ(101,*,IOSTAT=stat) suffix
                IF (stat.ne.0) EXIT
                WRITE(fileName,'(A,A,A)') TRIM(stem),".",TRIM(suffix)
                INQUIRE(file=TRIM(fileName),EXIST=exists)
                IF (exists) THEN
                    !CALL g%readTwoInt(TRIM(fileName),"b",1)    
                    !CALL g%getNBas(nOrb)
                    OPEN(1,file=trim(fileName),status="old",form="unformatted")

                    keyChar=""
                    READ(1,iostat=stat) nOrb,keyChar
                    IF (keyChar.eq."key") THEN 
                        keyIndex=.TRUE.
                        IF (stat.ne.0) THEN
                            WRITE(6,'(A,A)') "ERROR: Could not read header of intergral file: ",trim(fileName)
                            STOP "ERROR: Could not read header of intergral file"
                        ENDIF
                    ELSE
                        keyIndex=.FALSE.
                        REWIND(1)
                        READ(1) nOrb
                    ENDIF

                    IF (iFile.eq.1) THEN
                        keyIndexRef=keyIndex
                        IF (keyIndex) THEN
                            WRITE(100) nOrb, "key"
                        ELSE
                            WRITE(100) nOrb
                        ENDIF
                    ELSE
                        IF (keyIndex.neqv.keyIndexRef) THEN
                            WRITE(6,'(A)') "ERROR: problem in integral concatenation"
                            STOP "ERROR: problem in integral concatenation"
                        ENDIF
                    ENDIF
                    counter=0
                    DO
                        !CALL g%getVal(i,j,k,l,val,stat) 
                        IF (keyIndex) THEN
                            READ(1,iostat=stat) key,val
                            IF(isnan(val)) then
                             write(6,"(A,A,I0)") "XCHEM_WARNING a two electron integrals is not valid ",trim(suffix),key
                            endif
                            IF (IS_IOSTAT_END(stat)) EXIT
                            WRITE(100) key,val
                        ELSE
                            READ(1,iostat=stat) i,j,k,l,val
                            IF(isnan(val)) then
                             write(6,"(A,A,4(x,I0))") "XCHEM_WARNING a two electron integrals is not valid ",trim(suffix),i,j,k,l
                            endif
                            IF (IS_IOSTAT_END(stat)) EXIT
                            WRITE(100) i,j,k,l,val
                        ENDIF
                        counter=counter+1
                    ENDDO  
                    tCounter=tCounter+counter
                    !CALL g%destroy()
                    WRITE(6,'(A,I0,A,A)') "Processed ",counter," records, from file: ",TRIM(fileName)
                ELSE
                    WRITE(6,'(A,A,A)') "file with suffix ",TRIM(suffix)," not found"
                    STOP "ERROR: a file couldnt be found"
                ENDIF
            ENDDO
        ELSE
            DO 
                    iFile=iFile+1
                    WRITE(token,'(I0)') iFile
                    WRITE(fileName,'(A,A,A)') TRIM(stem),".",TRIM(ADJUSTL(token))
                    INQUIRE(file=TRIM(fileName),EXIST=exists)
                    IF (exists) THEN
                            CALL g%readTwoInt(TRIM(fileName),"b",1)    
                            CALL g%getNBas(nOrb)
                            IF (iFile.eq.1) WRITE(100) nOrb, "key"
                            counter=0
                            DO
                                    CALL g%getVal(i,j,k,l,val,stat) 
                                    IF (IS_IOSTAT_END(stat)) EXIT
                                    !WRITE(6,'(4(I0,x),E18.12)') i,j,k,l,val
                                    WRITE(100) i,j,k,l,val
                                    counter=counter+1
                            ENDDO  
                            tCounter=tCounter+counter
                            CALL g%destroy()
                            WRITE(6,'(A,I0,A,A)') "Processed ",counter," records, from file: ",TRIM(fileName)
                    ELSE
                            WRITE(6,'(I5,3A)') iFile-1," Files with name ",TRIM(stem),".*" 
                            EXIT
                    ENDIF
            ENDDO
        ENDIF
        WRITE(6,'(I0,A,I0,A)') tCounter," records written to concatenated file from ",iFile," files"
        WRITE(6,'(A)') "Happy"
        CLOSE(100)
END PROGRAM
