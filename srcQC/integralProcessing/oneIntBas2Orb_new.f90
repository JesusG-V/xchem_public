!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntBas2Orb
!CODE THAT TRANSFORMS ONE ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRELS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.
    USE omp_lib
    USE oneIntIO
    USE orbitalIO
    USE class_inputString
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(oneInt_) :: hBas
    TYPE(oneInt_) :: hOrb
    TYPE(orbital_) :: orb
    TYPE(inputString) :: inString
    TYPE(inputString) :: outString
    TYPE(inputString) :: hermitianString
    CHARACTER*200, ALLOCATABLE :: hermitianMat(:)
    CHARACTER*200, ALLOCATABLE :: inFiles(:)
    CHARACTER*200, ALLOCATABLE :: outFiles(:)
    
    INTEGER :: occupied
    INTEGER :: nFiles,iFiles
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: oneElectronOperatorBasisFiles
    CHARACTER*200 :: oneElectronOperatorOrbitalFiles
    CHARACTER*200 :: hermitian
    
    REAL*8, ALLOCATABLE :: orb1(:),oM(:,:)
    REAL*8, ALLOCATABLE :: ij(:,:),iq(:,:),pq(:,:)
    INTEGER :: iBas,jBas,iOrb,jOrb
    INTEGER :: nBas
    INTEGER :: workingUnit
    !PARALLELIZATION VARIABLES        
    INTEGER*4 :: nCPU
    
    NAMELIST /INPUT/        orbitalFile,&
                            occupied,&
                            oneElectronOperatorBasisFiles,&
                            oneElectronOperatorOrbitalFiles,&
                            hermitian
                            
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    
    CALL hermitianString%setString(TRIM(hermitian))
    CALL hermitianString%process("-")
    CALL hermitianString%getArray(hermitianMat)
    
    CALL orb%readOrbital(orbitalFile,"b") 
    CALL inString%setString(oneElectronOperatorBasisFiles)
    CALL outString%setString(oneElectronOperatorOrbitalFiles)
    CALL inString%process("-")
    CALL outString%process("-")
    CALL inString%sizeString(nFiles)
    CALL outString%sizeString(iBas)
    IF (nFiles.ne.iBas) THEN        
        STOP "One and only one ouput files must be provided for each input file"
    ENDIF
    CALL inString%getCharArray(inFiles)
    CALL outString%getCharArray(outFiles)

    CALL orb%getNOrb(nBas)

    ALLOCATE(oM(nBas,occupied))
    ALLOCATE(iq(nBas,occupied))
    ALLOCATE(pq(occupied,occupied))

    WRITE(6,'(A)') "Preparing Oribtals"
    DO jOrb=1,occupied
        CALL orb%getOrbital(orb1,jOrb)
        oM(:,jOrb)=orb1
    ENDDO

    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of available threads:",nCPU        
    IF (occupied.le.nCPU) THEN
            nCPU=occupied
            CALL omp_set_num_threads(nCPU)
    ELSE
            CALL omp_set_num_threads(nCPU)
    ENDIF
    nCPU=OMP_get_max_threads()
    WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU        
    
    DO iFiles=1,nFiles
        workingUnit=500+iFiles
        iq=0.d0
        pq=0.d0
        WRITE(6,'(A,A)') "Processing file: ",TRIM(inFiles(iFiles))
        CALL hBas%readOneInt(TRIM(inFiles(iFiles)),"b",.TRUE.,workingUnit,TRIM(hermitianMat(iFiles)))
        CALL hBas%getSym(ij)
      
        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP SHARED(iq,ij,oM,nBas,occupied) &
        !$OMP PRIVATE(iOrb,iBas,jBas)
        !$OMP DO
        DO iOrb=1,occupied
            DO iBas=1,nBas
                DO jBas=1,nBas
                    iq(iBas,iOrb)=iq(iBas,iOrb)+oM(jBas,iOrb)*ij(iBas,jBas)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL

        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP SHARED(iq,pq,oM,nBas,occupied) &
        !$OMP PRIVATE(iOrb,jOrb,iBas)
        !$OMP DO
        DO iOrb=1,occupied
            DO jOrb=1,occupied
                DO iBas=1,nBas
                    pq(iOrb,jOrb)=pq(iOrb,jOrb)+oM(iBas,iOrb)*iq(iBas,jOrb)
                ENDDO
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL hBas%destroy
        
        CALL hOrb%initializeEmpty(occupied)
        CALL hOrb%setSym(pq)
        CALL hOrb%writeOneInt(TRIM(outFiles(iFiles))//".fmt","f",.TRUE.,workingUnit)
        CALL hOrb%writeOneInt(TRIM(outFiles(iFiles)),"b",.FALSE.,workingUnit)
        CALL hOrb%destroy
    ENDDO
    
    WRITE(6,'(A)') "Happy"
END PROGRAM
