!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM createIntegralCSFMap
!PROGRAM THAT FROM A GUGA TABLE 
!CREATES OBJECTS OF TYPE oneIntCSFMap_ AND
!twoIntCSFMap_ AND WRITES THESE TO FILE
!DEBUG
!    USE omp_lib
!DEBUG
    USE oneIntCSFMapIO
    USE twoIntCSFMapIO
    USE gugaIO
    USE stopWatch
    !USE integralCSFMapIO
    LOGICAL :: exists
    INTEGER :: stat
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: gugaFile
    CHARACTER*200 :: oneIntCSFMapFile
    CHARACTER*200 :: twoIntCSFMapFile
    CHARACTER*200 :: outputFormat
    CHARACTER*200 :: token
    INTEGER :: occupied,nCSF
    INTEGER :: iCSF,jCSF
    INTEGER :: nOrbRas3
    INTEGER :: temp
!DEBUG
!    LOGICAL, ALLOCATABLE :: csfMatrix(:,:,:)
!    INTEGER :: un
!    INTEGER :: nCPU,iCPU
!    INTEGER :: diff
!    INTEGER :: counter
!    INTEGER :: nRelevantTwoInt,nRelevantOneInt
!DEBUG

    LOGICAL :: one,two

    TYPE(stopWatch_) :: timer
    TYPE(guga_) :: guga
    TYPE(oneIntCSFMap_) :: oneIntCSFMap
    TYPE(twoIntCSFMap_) :: twoIntCSFMap
    
    NAMELIST /INPUT/        gugaFile,&
                            oneIntCSFMapFile,&
                            twoIntCSFMapFile,&
                            occupied,&
                            outputFormat,&
                            nOrbRas3

    outputFormat="binary"
    oneIntCSFMapFile=""
    twoIntCSFMapFile=""

    nOrbRas3=0
    one=.FALSE.
    two=.FALSE.
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)       
   
    IF (outputFormat(1:1).eq."b") THEN
        WRITE(6,'(A)') "Write IntegralCSFMapFiles in (b)inary (default)"
    ELSEIF (outputFormat(1:1).eq."f") THEN
        WRITE(6,'(A)') "Write IntegralCSFMapFiles (f)ormatted"
    ELSE
        STOP "Unknown outputFormat specifier"
    ENDIF
    
    IF (LEN(TRIM(oneIntCSFMapFile)).gt.0) one=.TRUE.
    IF (LEN(TRIM(twoIntCSFMapFile)).gt.0) two=.TRUE.
    
    IF (one) WRITE(6,'(A)') "Will create IntegralCSFMap for one electron operators"
    IF (two) WRITE(6,'(A)') "Will create IntegralCSFMap for two electron operators"
    
    IF ((.NOT.(one)).AND.(.NOT.(two))) THEN
        STOP "ERROR: must request output for at least one of oneIntCSFMapFile and twoIntCSFMapFile"
    ENDIF
    
    WRITE(6,'(A)') "Reading Guga Table"
    CALL guga%readGuga(TRIM(gugaFile),nOrbRas3)
    CALL guga%getNumberOfCSF(nCSF)
    WRITE(6,'(A,I0)') "Number of CSFs=",nCSF

    WRITE(6,'(A,I,A)') "Allocate CSF Maps (as requested) with space for ",occupied," orbitals"
    IF (one) CALL oneIntCSFMap%create(occupied)
    IF (two) CALL twoIntCSFMap%create(occupied)

    WRITE(6,'(A)') 'Find number of CSF pairs appearing in each integral'
    CALL timer%initialize()

!DEBUG
!    CALL guga%getCSFMatrix(csfMatrix)
!    nCPU=OMP_get_max_threads() 
!    DO iCPU=0,nCPU-1
!        un=500+iCPU
!        WRITE(token,'(A,I3.3)') "oneIntCSF.indeces",iCPU
!        OPEN(un,FILE=TRIM(token),STATUS="replace")
!        un=600+iCPU
!        WRITE(token,'(A,I3.3)') "twoIntCSF.indeces",iCPU
!        OPEN(un,FILE=TRIM(token),STATUS="replace")
!    ENDDO
!    temp=0
!    counter=0
!    !$OMP PARALLEL &
!    !$OMP DEFAULT (none) &
!    !$OMP SHARED (csfMatrix,nCSF) &
!    !$OMP PRIVATE (jCSF,diff,iCPU)
!    !$OMP DO
!    DO iCSF=1,nCSF
!        DO jCSF=iCSF+1,nCSF
!            !CALL guga%prelimCSF(iCSF,jCSF,diff)
!            diff=SUM(ABS(COUNT(csfMatrix(iCSF,:,:),2)-COUNT(csfMatrix(jCSF,:,:),2)))/2
!            iCPU=OMP_get_thread_num()
!            IF (diff.le.1) THEN 
!                WRITE(500+iCPU,*) iCSF,jCSF
!            ELSEIF (diff.eq.2) THEN
!                WRITE(600+iCPU,*) iCSF,jCSF
!            ENDIF
!            !counter=counter+1
!            !IF (diff.gt.2) nRelevantTwoInt=nRelevantTwoInt+1
!            !IF (diff.gt.1) nRelevantOneInt=nRelevantOneInt+1
!        ENDDO
!        !WRITE(token,'(I)') iCSF
!        !CALL timer%time(token)
!        !WRITE(6,'(A,I,A,I,A,F11.6)') "Total csf checks: ",counter," of which ",nRelevantOneInt,&
!        !  " irrelevant to one integral CSF map ",REAL(nRelevantOneInt)/REAL(counter)
!        !WRITE(6,'(A,I,A,I,A,F11.6)') "Total csf checks: ",counter," of which ",nRelevantTwoInt,&
!        !  " irrelevant to two integral CSF map ",REAL(nRelevantTwoInt)/REAL(counter)
!        !WRITE(6,'(A)') ""
!        !IF (INT(100*REAL(iCSF)/REAL(nCSF)).ne.temp) THEN
!        !    temp=INT(100*REAL(iCSF)/REAL(nCSF))
!        !    WRITE(token,'(A,I0,A)') "CSF Map prep ",temp,"% done"
!        !    CALL timer%time(token)
!        !ENDIF
!    ENDDO
!    !$OMP END DO NOWAIT
!    !$OMP END PARALLEL
!    STOP
!DEBUG

    temp=0
    DO iCSF=1,nCSF
        DO jCSF=1,nCSF
            IF (one) CALL guga%initIntegralCSFMap(iCSF,jCSF,oneIntCSFMap)
            IF (two) CALL guga%initIntegralCSFMap(iCSF,jCSF,twoIntCSFMap)
        ENDDO
        IF (INT(100*REAL(iCSF)/REAL(nCSF)).ne.temp) THEN
            temp=INT(100*REAL(iCSF)/REAL(nCSF))
            WRITE(token,'(A,I0,A)') "CSF Map prep ",temp,"% done"
            CALL timer%time(token)
        ENDIF
    ENDDO
    WRITE(6,'(A)') 'Allocate space for CSF pairs in integral map object'
    IF (one) CALL oneIntCSFMap%allocateCSF()
    IF (two) CALL twoIntCSFMap%allocateCSF()
    WRITE(6,'(A)') 'Store the CSF pairs in integral map object'
    temp=0
    DO iCSF=1,nCSF
        DO jCSF=1,nCSF
            IF (one) CALL guga%createIntegralCSFMap(iCSF,jCSF,oneIntCSFMap)
            IF (two) CALL guga%createIntegralCSFMap(iCSF,jCSF,twoIntCSFMap)
        ENDDO
        IF (INT(100*REAL(iCSF)/REAL(nCSF)).ne.temp) THEN
            temp=INT(100*REAL(iCSF)/REAL(nCSF))
            WRITE(token,'(A,I0,A)') "CSF Map calc ",temp,"% done"
            CALL timer%time(token)
        ENDIF
    ENDDO
    IF (one) THEN
        WRITE(6,'(A)') "Compress one electron CSF Map"
        CALL oneIntCSFMap%compress()
    ENDIF
    IF (two) THEN
        WRITE(6,'(A)') "Compress two electron CSF Map"
        CALL twoIntCSFMap%compress()
    ENDIF
    WRITE(6,'(A)') "Print the whole malarkey"
    IF (one) CALL oneIntCSFMap%writeOneIntCSFMap(oneIntCSFMapFile,outputFormat(1:1))
    IF (two) CALL twoIntCSFMap%writeTwoIntCSFMap(twoIntCSFMapFile,outputFormat(1:1))
    
    WRITE(6,'(A)') "Happy"
END PROGRAM
