!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM compressTwoElectronIntegrals
!PROGRAM THAT READS TWO ELECTRON INTEGRALS FROM MOLCAS OUTPUT
!AND REWRITES THIS USING USEFUL INDECES. FURTHERMORE SKIPS
!ZERO INTEGRALS, INTEGRALS REDUNDANT DUE TO SYMMETRIES IN INDECES
!AND INTEGRALS NOT COMPLYING WITH LMAX AND KMAX WHICH CAN BE SPECIFIED BY USER.
!FURTHERMORE OUTPUTS A FILE THAT CONTAINS INFORMATION
!ABOUT MONOCENTRIC BLOCKS: SIZE,LABEL,QUANTUMNUMBERS L,M,K
    USE searchInsert
    USE lkIO
    USE stopWatch
    IMPLICIT none
    TYPE shellInfo_
        INTEGER :: nBas
        INTEGER :: nKLM
        INTEGER :: ghost
        LOGICAL :: augmented
        INTEGER, ALLOCATABLE :: k(:),l(:),m(:)
    END TYPE
    TYPE integral_   
        REAL*8 :: I
        INTEGER :: ijkl(4)
    END TYPE
    TYPE(stopWatch_) :: timer
    TYPE(lk_) :: lkPairs 
    CHARACTER*200 :: inputFile,fmt,token
    CHARACTER*200 :: petiteBasisList
    CHARACTER*200 :: twoElectronIntegralsMolcas
    CHARACTER*200 :: twoElectronIntegralsCompressed
    CHARACTER*200 :: blockInfoFile
    CHARACTER*200 :: outputFormat
    CHARACTER*200 :: lkMonocentricFile
    INTEGER :: stat,iDump,pos
    LOGICAL :: exists
    
    INTEGER :: nBas,nShell
    INTEGER :: iShell,iTemp
    INTEGER :: center,tempCenter
    INTEGER :: pB,qB,rB,sB
    INTEGER :: iBas,jBas,kBas,lBas
    INTEGER :: A,B,C,D
    INTEGER :: AM,BM,CM,DM
    INTEGER :: iM,jM,kM,lM
    INTEGER :: maxMQ
    INTEGER :: nIntegrals
    INTEGER :: deleted,written
    INTEGER :: lMax,kMax
    INTEGER :: K,L,M
    INTEGER :: insertionPoint,key,nKeys,id
    INTEGER :: multiGhost 
    !INTEGER :: nPm,iPm,pm(8,4)
    INTEGER,ALLOCATABLE :: IDs(:)
    INTEGER,ALLOCATABLE :: bK(:),bL(:),bMl(:)
    INTEGER,ALLOCATABLE :: bID(:,:),shellIndex(:)
    INTEGER,ALLOCATABLE :: bGhost(:)
    CHARACTER*16 :: label,mLabel,cTemp
    CHARACTER*16 :: atom
    CHARACTER*26 :: letters
    
    CHARACTER*16 :: tempLabel
    INTEGER :: iBlock,nBlock
    CHARACTER*16,ALLOCATABLE :: blockLabel(:)
    INTEGER, ALLOCATABLE :: blockSize(:)
    INTEGER, ALLOCATABLE :: blockQuantumNumbers(:,:) 
    INTEGER, ALLOCATABLE :: blockGhost(:)
    CHARACTER*10,ALLOCATABLE :: orb(:)
    
    REAL*8,ALLOCATABLE :: vec(:,:,:,:)
    
    LOGICAL :: dontCompress,printBasInfo
    LOGICAL :: spherical, prev_spherical

    TYPE(shellInfo_), ALLOCATABLE :: SInfo(:)
    LOGICAL, ALLOCATABLE :: augBas(:)
    TYPE(integral_), ALLOCATABLE :: integral(:)
    INTEGER :: iInt,jInt,iInd

    NAMELIST /INPUT/&
      petiteBasisList,& 
      twoElectronIntegralsMolcas,&
      blockInfoFile,&
      twoElectronIntegralsCompressed,&
      lMax,&
      kMax,&
      dontCompress,&
      outputFormat,&
      printBasInfo,&
      lkMonocentricFile

    printBasInfo=.TRUE.
    outputFormat="b"
    twoElectronIntegralsMolcas=""
    twoElectronIntegralsCompressed=""
    lkMonocentricFile=""
    dontCompress=.FALSE.
    lMax=0
    kMax=0
    blockInfoFile="list.blockBasis"
    letters="abcdefghijklmnopqrstuvwxyz"
    
    WRITE(6,'(x,A)') "Start: compressTwoElectronIntegrals"
    WRITE(6,'(x,A)') "Looking for input file to be provided:"
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT)
    !READ(1,NML=INPUT,IOSTAT=stat)
    !IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)
   
    WRITE(6,'(A)') "RESTRICTION INFO"
    IF (len(trim(lkMonocentricFile)).gt.0) THEN 
        WRITE(6,'(A)') "Read from file"
        call lkPairs%create(trim(lkMonocentricFile))
    ELSE
        IF (lMax.lt.0) STOP "negative lMax make no sense"
        IF (kMax.lt.0) STOP "negative kMax make no sense"
        WRITE(6,'(A,I3,2x,A,I3)') "lMax=",lMax,"kMax=",kMax
        call lkPairs%create(lMax,kMax)
    ENDIF
    WRITE(6,'(A)') " "

    OPEN(1,file=TRIM(petiteBasisList),status="old")
    IF (.NOT.dontCompress) THEN
        OPEN(2,file=TRIM(twoElectronIntegralsMolcas),status="old",form="unformatted")
        IF (outputFormat(1:1).eq."b") THEN
            OPEN(3,file=TRIM(twoElectronIntegralsCompressed),status="replace",form="unformatted")
        ELSE
            OPEN(3,file=TRIM(twoElectronIntegralsCompressed),status="replace")
        ENDIF
    ENDIF

    tempLabel=" "
    nBlock=0
    WRITE(6,'(A)') " "
    WRITE(6,'(A)') "Process petite basis list"
    DO 
        READ(1,*,IOSTAT=STAT) nBas,atom,label
        IF (IS_IOSTAT_END(stat)) EXIT
        IF (SCAN(atom,"X").gt.0) THEN
            IF (TRIM(tempLabel).ne.TRIM(label)) THEN
                tempLabel=label
                nBlock=nBlock+1
            ENDIF
        ENDIF
    ENDDO
    REWIND(1)
    ALLOCATE (bID(nBas,2))
    ALLOCATE (shellIndex(nBas))
    ALLOCATE (orb(nBas))
    ALLOCATE (bK(nBas),bL(nBas),bMl(nBas))
    ALLOCATE (augBas(nBas))
    ALLOCATE (bGhost(nBas))
    
    ALLOCATE (blockLabel(nBlock))
    ALLOCATE (blockQuantumNumbers(nBlock,3))
    ALLOCATE (blockGhost(nBlock))
    ALLOCATE (blockSize(nBlock))
    blockSize=0
    augBas=.FALSE.
    cTemp=" "
    iTemp=0
    nShell=0
    tempCenter=0
    tempLabel=" "
    iBlock=0
    bGhost=-1
    multighost = -1
    DO pB=1,nBas
        READ(1,*,IOSTAT=STAT) iDump,atom,label,center
        orb(pB)=TRIM(label)
        CALL isSpherical(label, spherical)
        IF (pB.eq.1) THEN
            prev_spherical = spherical
        ENDIF
        CALL getL(label,L)
        CALL getK(label,K)
        CALL getM(label,M)
        IF (spherical) THEN
            pos=SCAN(label,letters)
            READ(label(1:pos-1),*) iShell
        ELSE
            iShell = L
        ENDIF
        IF ((iShell.ne.iTemp).OR.(center.ne.tempCenter).OR.(prev_spherical.neqv.spherical)) THEN
            nShell=nShell+1
            iTemp=iShell
            prev_spherical = spherical
        ENDIF
       
        IF (spherical) THEN 
            mLabel=label(pos:LEN(TRIM(label)))
        ELSE
            mLabel=label(2:4)
        ENDIF
        IF ((TRIM(mLabel).eq.TRIM(cTemp)).AND.(center.eq.tempCenter)) THEN
            iBas=iBas+1
        ELSE
            iBas=1
            cTemp=mLabel
            tempCenter=center
        ENDIF
        bID(pB,1)=nShell
        bID(pB,2)=iBas
        IF (spherical) THEN
            bMl(pB)=M+1+L
        ELSE
            bMl(pB)=M
        ENDIF
        shellIndex(pB)=iShell
        bK(pB)=K
        bL(pB)=L
        IF (SCAN(atom,"X").gt.0) THEN
            augBas(pB)=.TRUE.
            IF (LEN(TRIM(atom)).eq.1) THEN
                bGhost(pB)=L
                IF (multighost .eq. 1) THEN
                    STOP "Error: inconsistent ghost basis sets" 
                ENDIF
                multighost = 0 
            ELSE
                token=atom(INDEX(atom,"X")+1:INDEX(atom,"X")+1)
                READ(token,*) bGhost(pB)
                IF (multighost .eq. 0) THEN
                    STOP "Error: inconsistent ghost basis sets" 
                ENDIF
                multighost = 1
            ENDIF
            IF (TRIM(tempLabel).ne.TRIM(label)) THEN
                tempLabel=label
                iBlock=iBlock+1
                blockSize(iBlock)=blockSize(iBlock)+1
                blockLabel(iBlock)=TRIM(label)
                blockQuantumNumbers(iBlock,1)=K
                blockQuantumNumbers(iBlock,2)=L
                blockQuantumNumbers(iBlock,3)=M
                blockGhost(iBlock)=bGhost(pB)
            ELSE
                blockSize(iBlock)=blockSize(iBlock)+1
            ENDIF
        ENDIF
    ENDDO
    CLOSE(1)
    
    ALLOCATE(SInfo(nShell))
    DO iShell=1,nShell
        SInfo(iShell)%nBas=0
        SInfo(iShell)%nKLM=0
    ENDDO
    iM=-1
    jM=-1
    kM=-1
    DO pB=1,nBas
            SInfo(bID(pB,1))%nBas=SInfo(bID(pB,1))%nBas+1
            SInfo(bID(pB,1))%augmented=augBas(pB)
            SInfo(bID(pB,1))%ghost=bGhost(pB)
            IF (bK(pB).ne.iM.OR.bL(pB).ne.jM.OR.bMl(pB).ne.kM) THEN
                SInfo(bID(pB,1))%nKLM=SInfo(bID(pB,1))%nKLM+1
                iM=bK(pB)
                jM=bL(pB)
                kM=bMl(pB)
            ENDIF
    ENDDO
    iM=1
    jM=0
    DO iShell=1,nShell
        ALLOCATE(SInfo(iShell)%k(SInfo(iShell)%nKLM))
        ALLOCATE(SInfo(iShell)%l(SInfo(iShell)%nKLM))
        ALLOCATE(SInfo(iShell)%m(SInfo(iShell)%nKLM))
        jm=jm+SInfo(iShell)%nBas
        kM=MAXVAL(bID(iM:jM,2))
        im=im+SInfo(iShell)%nBas
        SInfo(iShell)%nBas=km
    ENDDO
    iM=-1
    jM=-1
    kM=-1
    lM=-1
    aM=0
    DO pB=1,nBas
        IF (bID(pB,1).ne.lM) THEN
            lM=bID(pB,1)
            iM=-1
            jM=-1
            kM=-1
            aM=0
        ENDIF
        IF (bK(pB).ne.iM.OR.bL(pB).ne.jM.OR.bMl(pB).ne.kM) THEN
            aM=aM+1
            iM=bK(pB)
            jM=bL(pB)
            kM=bMl(pB)
        ENDIF
        SInfo(bID(pB,1))%k(aM)=bK(pB)
        SInfo(bID(pB,1))%l(aM)=bL(pB)
        SInfo(bID(pB,1))%m(aM)=bMl(pB)
    ENDDO
    
    IF (multiGhost.eq.0) THEN
        WRITE(6,'(A)') "One ghost atom only. Only apply global l and k limits as specified by user"
    ELSE
        WRITE(6,'(A)') "Multiple ghost atoms were found."
        WRITE(6,'(A)') "These will be assumed to be in order of asscending l quantum number"
    ENDIF

    WRITE(6,'(x,A)') "Completed Processing of Petetis Basis List"
    WRITE(6,'(A)') " "

    WRITE(6,'(A)') "SHELL INFO"
    WRITE(6,'(2x,A,x,I5)') "Number of shells: ",nShell
    WRITE(6,'(A)') "Augmented Shell:"
    WRITE(fmt,'(A,I0,A)') "(",nShell,"(x,L))"
    WRITE(6,TRIM(fmt)) (SInfo(iShell)%augmented,iShell=1,nShell)
    WRITE(6,'(2x,A)') "Basis Functions per klm triplet (block) in each:"
    WRITE(fmt,'(A,I0,A)') "(",nShell,"(x,I5))"
    WRITE(6,TRIM(fmt)) (SInfo(iShell)%nBas,iShell=1,nShell)
    WRITE(6,'(2x,A)') "Number of klm triplets in shell:"
    WRITE(6,TRIM(fmt)) (SInfo(iShell)%nKLM,iShell=1,nShell)
    WRITE(6,'(A)') "klm triplets in each shell:"
    DO iShell=1,nShell
        WRITE(fmt,'(A,I0,A)') "(A,I2,A,",SInfo(iShell)%nKLM,"(x,I3))"
        IF (sInfo(iShell)%augmented) THEN
            WRITE(6,'(A,I0)') "ghost ID ",sInfo(iShell)%ghost
        ELSE
            WRITE(6,'(A,I0)') "real atom"
        ENDIF
        WRITE(6,TRIM(fmt)) "Shell ",iShell," k        = ",(SInfo(iShell)%k(iM),iM=1,SInfo(iShell)%nKLM)
        WRITE(6,TRIM(fmt)) "Shell ",iShell," l        = ",(SInfo(iShell)%l(iM),iM=1,SInfo(iShell)%nKLM)
        WRITE(6,TRIM(fmt)) "Shell ",iShell," m+l+1/xyz= ",(SInfo(iShell)%m(iM),iM=1,SInfo(iShell)%nKLM)
        WRITE(6,'(A)') " "
    ENDDO

    WRITE(6,'(A)') "BASIS FCT INFO"
    WRITE(6,'(2x,A,x,I5)') "Total number of basis functions: ",nBas
    WRITE(6,'(x,A)') "Table to identify each basis function:"
    WRITE(6,'(x,A16,2x,A7,2x,A5,2x,A15,2x,A10,2x,A11,2x,A11,2x,A1,2x,A8)') &
      "petite basis fct","orbital","shell","block basis fct","mL+l+1","quant num K","quant num l","X","ghost ID"
    IF (printBasInfo) THEN
        DO pB=1,nBas
                WRITE(6,'(x,I16,2x,A7,2x,I5,2x,I15,2x,I10,2x,I11,2x,I11,2x,L,2x,I8)') &
                  pB,TRIM(orb(pB)),bID(pB,1),bID(pB,2),bMl(pB),bK(pB),bL(pB),augBas(pB),bGhost(pB)
        ENDDO
    ELSE
        WRITE(6,'(A)') "printing of basis function info disables by user"
    ENDIF
    WRITE(6,'(A)') " "
   
    OPEN(1,file=TRIM(blockInfoFile),STATUS="replace")
    DO iBlock=1,nBlock
            IF (multighost.eq.1) THEN
                WRITE(1,'(I3,x,A7,x,I3,x,I3,x,I3,x,I3)') blockSize(iBlock),TRIM(blockLabel(iBlock)),&
                  blockQuantumNumbers(iBlock,2),blockQuantumNumbers(iBlock,3),blockQuantumNumbers(iBlock,1),blockGhost(iBlock)
            ELSE
                WRITE(1,'(I3,x,A7,x,I3,x,I3,x,I3,x,I3)') blockSize(iBlock),TRIM(blockLabel(iBlock)),&
                  blockQuantumNumbers(iBlock,2),blockQuantumNumbers(iBlock,3),blockQuantumNumbers(iBlock,1),0
            ENDIF 
    ENDDO
    CLOSE(1)
    
    IF (dontCompress) THEN
        STOP
    ENDIF

    WRITE(6,'(A)') "That was the info. Now commence the grinding!"
    maxMQ=0
    DO iShell=1,nShell
        IF (SInfo(iShell)%nKLM.gt.maxMQ) THEN
            maxMQ=SInfo(iShell)%nKLM
        ENDIF
    ENDDO
    WRITE(6,'(A)') "Allocate space input integrals by record"
    WRITE(6,'(A,I0)') "maximum number of mkl triplets: ",maxMQ
    WRITE(6,'(A,I0)') "Array size: ",maxMQ**4
    WRITE(6,'(A)') " "
    IF (outputFormat(1:1).eq."b") THEN
        WRITE(3) nBas,"key"
    ELSE
        WRITE(3,'(x,I6)') nBas
    ENDIF
    
    ALLOCATE(vec(maxMQ,maxMQ,maxMQ,maxMQ))

    A=0
    B=0
    DO iShell=1,nShell
        IF (SInfo(iShell)%augmented) THEN
            C=SInfo(iShell)%nKLM*SInfo(iShell)%nBas
            IF (A.lt.C) A=C
        ELSE
            C=SInfo(iShell)%nKLM*SInfo(iShell)%nBas
            IF (B.lt.C) B=C
        ENDIF
    ENDDO
    IF (A.gt.B) THEN
        C=A**2*B**2
    ELSE
        C=B**4
    ENDIF
    WRITE(6,'(A)') "Allocate space for output integrals by block."
    WRITE(6,'(A,I0,x,I0)') "Largest number of integrals in local / augmented shell: ",B,A
    WRITE(6,'(A,I0)') "Array size: ",C
    WRITE(6,'(A)') " "

    ALLOCATE(integral(C))
    ALLOCATE(IDs(C))
    IDs=0

    deleted=0
    written=0
    CALL timer%initialize()
    DO 
        READ(2,IOSTAT=stat) A,B,C,D
        IF ((stat.ne.0).OR.(A+B+C+D.eq.0)) EXIT
        !READ(2,*,IOSTAT=stat) A,B,C,D
        AM=SInfo(A)%nKLM
        BM=SInfo(B)%nKLM
        CM=SInfo(C)%nKLM
        DM=SInfo(D)%nKLM
        nIntegrals=AM*BM*CM*DM*&
          SInfo(A)%nBas*SInfo(B)%nBas*SInfo(C)%nBas*SInfo(D)%nBas 
        WRITE(6,'(A,4(I0,x))') "Block : A B C D shell ",A,B,C,D
        WRITE(6,'(A,4(I0,x))') "Block : A B C D nKLM  ",AM,BM,CM,DM
        WRITE(6,'(A,4(I0,x),A,I0)') "Block : A B C D nBas  ",SInfo(A)%nBas,SInfo(B)%nBas,SInfo(C)%nBas,SInfo(D)%nBas,&
          " => nIntegrals=",nIntegrals
        !DEALLOCATE(integral,IDs)
        !ALLOCATE(integral(nIntegrals),IDs(nIntegrals))
        nKeys=0
        DO lBas=1,SInfo(D)%nBas
            DO kBas=1,SInfo(C)%nBas
                DO jBas=1,SInfo(B)%nBas
                    DO iBas=1,SInfo(A)%nBas
                        !READ(2) ((((vec(lM,kM,jM,iM),iM=1,AM),jM=1,BM),kM=1,CM),lM=1,DM)    
                        READ(2) ((((vec(iM,jM,kM,lM),iM=1,AM),jM=1,BM),kM=1,CM),lM=1,DM)    
                        
                        DO lM=1,DM
                            DO kM=1,CM
                                DO jM=1,BM 
                                    DO iM=1,AM
                                        IF (ABS(vec(iM,jM,kM,lM)).le.1e-14) THEN
                                            deleted=deleted+1
                                            CYCLE
                                        ENDIF
                                        DO pB=1,nBas
                                            IF (ABS(bID(pB,1)-A)+&
                                              ABS(bID(pB,2)-iBas)+&
                                              ABS(bMl(pB)-SInfo(A)%m(iM))+&
                                              ABS(bK(pB)-SInfo(A)%k(iM))+&
                                              ABS(bL(pB)-SInfo(A)%l(iM)).eq.0) THEN
                                                IF (multighost.eq.1) THEN
                                                    IF (ABS(bGhost(pB)-SInfo(A)%ghost).eq.0) EXIT
                                                ELSE
                                                    EXIT
                                                ENDIF
                                            ENDIF
                                        ENDDO      
                                        DO qB=1,nBas
                                            IF (ABS(bID(qB,1)-B)+&
                                              ABS(bID(qB,2)-jBas)+&
                                              ABS(bMl(qB)-SInfo(B)%m(jM))+&
                                              ABS(bK(qB)-SInfo(B)%k(jM))+&
                                              ABS(bL(qB)-SInfo(B)%l(jM)).eq.0) THEN
                                              IF (multighost.eq.1) THEN
                                                    IF (ABS(bGhost(qB)-SInfo(B)%ghost).eq.0) EXIT
                                                ELSE
                                                    EXIT
                                                ENDIF
                                            ENDIF
                                        ENDDO      
                                        DO rB=1,nBas
                                            IF (ABS(bID(rB,1)-C)+&
                                              ABS(bID(rB,2)-kBas)+&
                                              ABS(bMl(rB)-SInfo(C)%m(kM))+&
                                              ABS(bK(rB)-SInfo(C)%k(kM))+&
                                              ABS(bL(rB)-SInfo(C)%l(kM)).eq.0) THEN
                                                IF (multighost.eq.1) THEN
                                                    IF (ABS(bGhost(rB)-SInfo(C)%ghost).eq.0) EXIT
                                                ELSE
                                                    EXIT
                                                ENDIF
                                            ENDIF
                                        ENDDO      
                                        DO sB=1,nBas
                                            IF (ABS(bID(sB,1)-D)+&
                                              ABS(bID(sB,2)-lBas)+&
                                              ABS(bMl(sB)-SInfo(D)%m(lM))+&
                                              ABS(bK(sB)-SInfo(D)%k(lM))+&
                                              ABS(bL(sB)-SInfo(D)%l(lM)).eq.0) THEN
                                                IF (multighost.eq.1) THEN
                                                    IF (ABS(bGhost(sB)-SInfo(D)%ghost).eq.0) EXIT
                                                ELSE
                                                    EXIT
                                                ENDIF
                                            ENDIF
                                        ENDDO  

                                        IF ((pB.gt.nBas).OR.(qB.gt.nBas).OR.(rB.gt.nBas).OR.(sB.gt.nBas)) THEN
                                            WRITE(6,'(4(x,I6))') A,B,C,D
                                            STOP "Error: could not map to Basis fcts"
                                        ENDIF

                                        IF (augBas(pB)) THEN
                                            !IF (bK(pB).gt.kMax.OR.bL(pB).gt.lMax) THEN
                                            IF (.not.lkPairs%is_lk(bL(pB),bK(pB))) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                            IF (bL(pB).ne.bGhost(pB)) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                        ENDIF
                                        IF (augBas(qB)) THEN
                                            !IF (bK(qB).gt.kMax.OR.bL(qB).gt.lMax) THEN
                                            IF (.not.lkPairs%is_lk(bL(qB),bK(qB))) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                            IF (bL(qB).ne.bGhost(qB)) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                        ENDIF
                                        IF (augBas(rB)) THEN
                                            !IF (bK(rB).gt.kMax.OR.bL(rB).gt.lMax) THEN
                                            IF (.not.lkPairs%is_lk(bL(rB),bK(rB))) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                            IF (bL(rB).ne.bGhost(rB)) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                        ENDIF
                                        IF (augBas(sB)) THEN
                                            !IF (bK(sB).gt.kMax.OR.bL(sB).gt.lMax) THEN
                                            IF (.not.lkPairs%is_lk(bL(sB),bK(sB))) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                            IF (bL(sB).ne.bGhost(sB)) THEN
                                                deleted=deleted+1       
                                                CYCLE
                                            ENDIF
                                        ENDIF


                                        id=(sB-1)*nBas**3+(rB-1)*nBas**2+(qB-1)*nBas+(pB-1) !lkji
                                        key=id
                        
                                        id=(qB-1)*nBas**3+(pB-1)*nBas**2+(rB-1)*nBas+(sB-1) !jikl
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(pB-1)*nBas**3+(qB-1)*nBas**2+(sB-1)*nBas+(rB-1) !ijlk
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(qB-1)*nBas**3+(pB-1)*nBas**2+(sB-1)*nBas+(rB-1) !jilk
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(rB-1)*nBas**3+(sB-1)*nBas**2+(pB-1)*nBas+(qB-1) !klij
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(sB-1)*nBas**3+(rB-1)*nBas**2+(pB-1)*nBas+(qB-1) !lkij
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(rB-1)*nBas**3+(sB-1)*nBas**2+(qB-1)*nBas+(pB-1) !klji
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                        
                                        id=(pB-1)*nBas**3+(qB-1)*nBas**2+(rB-1)*nBas+(sB-1) !ijkl
                                        IF (id.lt.key) THEN
                                            key=id
                                        ENDIF
                                        IF (nKeys.gt.0) THEN
                                            CALL binarySearch(key,nKeys,IDs(1:nKeys),insertionPoint)
                                            IF (insertionPoint.lt.0) THEN      
                                                deleted=deleted+1
                                                CYCLE
                                            ENDIF
                                        ELSE
                                            insertionPoint=0
                                        ENDIF
                                        CALL insert(key,insertionPoint,nKeys,nIntegrals,IDs)
                                        nKeys=nKeys+1
                                        integral(nKeys)%I=vec(iM,jM,kM,lM)
                                        integral(nKeys)%ijkl(1)=pB
                                        integral(nKeys)%ijkl(2)=qB
                                        integral(nKeys)%ijkl(3)=rB
                                        integral(nKeys)%ijkl(4)=sB
                                    ENDDO
                                ENDDO    
                            ENDDO    
                        ENDDO
                    ENDDO
                ENDDO
            ENDDO
        ENDDO
        DO iInt=1,nKeys
            written=written+1
            IF (outputFormat(1:1).eq."b") THEN
                !WRITE(3),(integral(iInt)%ijkl(iInd),iInd=1,4),integral(iInt)%I
                key=integral(iInt)%ijkl(1)*(nBas+1)**3+&
                  integral(iInt)%ijkl(2)*(nBas+1)**2+&
                  integral(iInt)%ijkl(3)*(nBas+1)+&
                  integral(iInt)%ijkl(4)
                WRITE(3) key,integral(iInt)%I
            ELSE
                WRITE(3,'(4(x,I8),x,E18.12)') (integral(iInt)%ijkl(iInd),iInd=1,4),integral(iInt)%I
            ENDIF
        ENDDO
        WRITE(token,'(A,x,I10,x,A,x,I10)') "Progress report: Integrals written:",written,"Integrals deleted:",deleted
        CALL timer%time(TRIM(token))
    ENDDO  
    CLOSE(2)
    CALL timer%time("Integrals Compressed")        
    WRITE(6,'(A,x,I12)') "Total number of integrals written to file:",written
    WRITE(6,'(A)') "Happy"
END PROGRAM

SUBROUTINE getM(label,m)
    IMPLICIT none
    CHARACTER(len=*),INTENT(in) :: label    
    INTEGER, INTENT(out) :: m
    CHARACTER*1 :: labelList(10)
    CHARACTER*26 :: letters
    CHARACTER*1 :: lC
    LOGICAL :: spherical
    INTEGER :: p,q
    
    letters="abcdefghijklmnopqrstuvwxyz"
    
    CALL isSpherical(label,spherical)
    IF (spherical) THEN
        p=SCAN(label,letters)
        lC=label(p:p)
        IF (lc.eq."s") THEN
            m=0
        ELSEIF (label(p:p+1).eq."px") THEN
            m=1
        ELSEIF (label(p:p+1).eq."py") THEN
            m=-1
        ELSEIF (label(p:p+1).eq."pz") THEN
            m=0
        ELSE
            q=SCAN(label," ")
            IF (label(q-1:q).eq."0") THEN
                m=0
            ELSEIF (label(q-1:q).eq."-") THEN
                q=q-1
                READ(label(p+1:q-1),*) m
                m=m*(-1)
            ELSEIF (label(q-1:q).eq."+") THEN
                q=q-1
                READ(label(p+1:q-1),*) m
            ELSE
                q=0
            ENDIF
        ENDIF
    ELSE 
        READ(label(2:4),*) m
    ENDIF
END SUBROUTINE
SUBROUTINE getL(label,l)
    IMPLICIT none
    CHARACTER(len=*),INTENT(in) :: label    
    INTEGER, INTENT(out) :: l
    CHARACTER*1 :: labelList(16)
    CHARACTER*26 :: letters
    CHARACTER*1 :: lC
    LOGICAL :: spherical
    INTEGER :: p,q
    
    letters="abcdefghijklmnopqrstuvwxyz"
    labelList(1)="s"
    labelList(2)="p"
    labelList(3)="d"
    labelList(4)="f"
    labelList(5)="g"
    labelList(6)="h"
    labelList(7)="i"
    labelList(8)="k"
    labelList(9)="l"
    labelList(10)="m"
    labelList(11)="n"
    labelList(12)="o"
    labelList(13)="q"
    labelList(14)="r"
    labelList(15)="t"
    labelList(16)="u"
    
    CALL isSpherical(label,spherical)
    IF (spherical) THEN
        p=SCAN(label,letters)
        lC=label(p:p)
    ELSE
        lC=label(1:1)
    ENDIF
    DO q=1,16
        IF (lC.eq.labelList(q)) THEN
            l=q-1
            EXIT
        ENDIF
    ENDDO
END SUBROUTINE
SUBROUTINE getK(label,k)
    IMPLICIT none
    CHARACTER(len=*),INTENT(in) :: label    
    INTEGER, INTENT(out) :: k
    CHARACTER*1 :: labelList(16)
    CHARACTER*26 :: letters
    CHARACTER*1 :: lC
    CHARACTER*10 :: temp
    LOGICAL :: spherical
    INTEGER :: p,q,l,shell
    
    letters="abcdefghijklmnopqrstuvwxyz"
    labelList(1)="s"
    labelList(2)="p"
    labelList(3)="d"
    labelList(4)="f"
    labelList(5)="g"
    labelList(6)="h"
    labelList(7)="i"
    labelList(8)="k"
    labelList(9)="l"
    labelList(10)="m"
    labelList(11)="n"
    labelList(12)="o"
    labelList(13)="q"
    labelList(14)="r"
    labelList(15)="t"
    labelList(16)="u"
   
    CALL isSpherical(label,spherical)
    IF (spherical) THEN
        p=SCAN(label,letters)
        lC=label(p:p)
        DO q=1,16
            IF (lC.eq.labelList(q)) THEN
                l=q
                EXIT
            ENDIF
        ENDDO
        temp=label(1:p-1)
        READ(temp,*) shell
        k=(shell-l)/2
    ELSE
        k=0
    ENDIF
END SUBROUTINE

SUBROUTINE isSpherical(label, res)
    IMPLICIT none
    CHARACTER(len=*), INTENT(in) :: label
    LOGICAL, INTENT(out) :: res
    CHARACTER*26 :: letters
    letters="abcdefghijklmnopqrstuvwxyz"
    
    IF (SCAN(label(1:1), letters) .gt. 0) THEN
        res = .false.
    ELSE
        res = .true.
    ENDIF
END SUBROUTINE
