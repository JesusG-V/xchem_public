!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE gugaIO
    IMPLICIT none
    PRIVATE
    LOGICAL :: debug=.FALSE.
    PUBLIC guga_
    !INTERNAL TYPES
    TYPE det_
        LOGICAL*1, ALLOCATABLE :: occ(:,:)
        INTEGER :: zahler
        INTEGER :: nenner
    END TYPE
    TYPE csf_
        LOGICAL*1, ALLOCATABLE :: conf(:,:)
        TYPE(det_), ALLOCATABLE :: det(:)
        INTEGER :: nDet
    END TYPE
    !CLASS STRUCTURE
    TYPE guga_
        PRIVATE
        LOGICAL :: alive=.FALSE.
        INTEGER :: nUniqueCSF
        INTEGER :: nUniqueDet 
        INTEGER :: activeSpaceSize
        TYPE(csf_), ALLOCATABLE :: csf(:)
        CONTAINS
            PROCEDURE :: getCSFMatrix
            PROCEDURE :: readGuga
            PROCEDURE :: initOneIntegralCSFMap 
            PROCEDURE :: createOneIntegralCSFMap 
            PROCEDURE :: initTwoIntegralCSFMap 
            PROCEDURE :: createTwoIntegralCSFMap 
            PROCEDURE :: getNumberOfCSF
            PROCEDURE :: CSFDiff
            GENERIC :: initIntegralCSFMap => initOneIntegralCSFMap,initTwoIntegralCSFMap
            GENERIC :: createIntegralCSFMap => createOneIntegralCSFMap,createTwoIntegralCSFMap
    END TYPE

    CONTAINS
        SUBROUTINE printDeterminant(det,nCas)
            IMPLICIT none
            INTEGER, INTENT(in) :: nCas
            LOGICAL*1,INTENT(in) :: det(nCas,2)
            CHARACTER(len=nCas) :: detC
            INTEGER :: i,s
            DO s=1,2
                DO i=1,nCAS
                    IF (det(i,s)) THEN
                        detC(i:i)='1'
                    ELSE
                        detC(i:i)='0'
                    ENDIF
                ENDDO
                WRITE(6,'(A)') TRIM(detC)
            ENDDO
        END SUBROUTINE                
        !INTERNAL FUNCTIONS AND SUBROUTINES
        FUNCTION kronecker(i,j) RESULT(k)
            INTEGER,INTENT(in) :: i,j
            LOGICAL*1 :: k
            IF (i.eq.j) THEN
                k=.TRUE.
            ELSE
                k=.FALSE.
            ENDIF
        END FUNCTION
        FUNCTION compareDeterminants(occ1,occ2,nCAS) RESULT(diff)
            IMPLICIT none
            LOGICAL*1,INTENT(in) :: occ1(nCAS,2),occ2(nCAS,2)
            INTEGER,INTENT(in) :: nCAS
            INTEGER :: iCas,s
            INTEGER :: diff
            !diff=0
            !DO s=1,2
            !    DO iCas=1,nCAS
            !        IF (occ1(iCas,s).eqv.occ2(iCas,s)) CYCLE
            !        diff=diff+1
            !        IF (diff.ge.5) THEN
            !            RETURN
            !        ENDIF
            !    ENDDO
            !ENDDO
            diff=COUNT(occ1.neqv.occ2)/2
            !diff=diff/2
        END FUNCTION
        
        SUBROUTINE locateDeviantSpinOrbitals(occ1,occ2,nCas,diff,orb,spin,phase) 
            IMPLICIT none
            LOGICAL*1,INTENT(in) :: occ1(nCAS,2),occ2(nCAS,2)
            INTEGER,INTENT(in) :: nCAS
            INTEGER,INTENT(in) :: diff
            INTEGER,INTENT(out) :: orb(2,2),spin(2,2)
            INTEGER,INTENT(out),OPTIONAL :: phase
            INTEGER :: iCas,iSpin
            INTEGER :: iDiff,jDiff
            INTEGER :: jump1,jump2
            LOGICAL :: done
            iDiff=0
            jDiff=0
            orb=0
            spin=0
            jump1=0
            jump2=0
            done=.FALSE.
            DO iCas=1,nCAS
                DO iSpin=1,2
                    IF (occ1(iCas,iSpin).eqv.occ2(iCas,iSpin)) CYCLE
                    IF (occ1(iCas,iSpin)) THEN
                        iDiff=iDiff+1
                        orb(1,iDiff)=iCas
                        spin(1,iDiff)=iSpin
                    ELSE
                        jDiff=jDiff+1
                        orb(2,jDiff)=iCas
                        spin(2,jDiff)=iSpin
                    ENDIF 
                    IF (iDiff.eq.diff.AND.jDiff.eq.diff) THEN
                        done=.TRUE.
                        EXIT
                    ENDIF
                ENDDO
                IF (done) EXIT
            ENDDO
            
            IF (.NOT.PRESENT(phase)) RETURN
            
            DO iDiff=1,diff
                done=.FALSE.
                DO iCas=1,nCas
                    DO iSpin=1,2
                        IF (iCAS.eq.orb(1,iDiff).AND.iSpin.eq.spin(1,iDiff)) THEN
                            done=.TRUE.           
                            EXIT   
                        ENDIF
                        IF (occ1(iCas,iSpin)) THEN
                            jump1=jump1+1
                        ENDIF
                    ENDDO
                    IF (done) EXIT
                ENDDO
            ENDDO
            
            DO iDiff=1,diff
                done=.FALSE.
                DO iCas=1,nCas
                    DO iSpin=1,2
                        IF (iCAS.eq.orb(2,iDiff).AND.iSpin.eq.spin(2,iDiff)) THEN
                            done=.TRUE.           
                            EXIT   
                        ENDIF
                        IF (occ2(iCas,iSpin)) THEN
                            jump2=jump2+1
                        ENDIF
                    ENDDO
                    IF (done) EXIT
                ENDDO
            ENDDO
            IF (mod(jump1,2).ne.mod(jump2,2)) THEN
                phase=-1
            ELSE
                phase=1
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE string2num(string,num)
            IMPLICIT none
            LOGICAL*1,ALLOCATABLE, INTENT(out) :: num(:,:)
            CHARACTER(len=*),INTENT(in) :: string
            INTEGER :: i,n
            n=LEN(TRIM(string))
            IF (ALLOCATED(num)) THEN
                IF (size(num,1).ne.n.OR.size(num,2).ne.2) THEN
                    DEALLOCATE(num) 
                    ALLOCATE(num(n,2))
                ENDIF
            ELSE
                ALLOCATE(num(n,2))
            ENDIF
            num=.FALSE.
            DO i=1,n
                IF ((string(i:i).eq."2").OR.(string(i:i).eq."#")) THEN
                    num(i,1)=.TRUE.
                    num(i,2)=.TRUE.
                ELSEIF ((string(i:i).eq."u").OR.(string(i:i).eq."+")) THEN
                    num(i,1)=.TRUE.
                ELSEIF ((string(i:i).eq."d").OR.(string(i:i).eq."-")) THEN
                    num(i,2)=.TRUE.
                ENDIF
            ENDDO
        END SUBROUTINE
        
        !CLASS METHODS
        SUBROUTINE readGuga(this,fich,nOrbRas3In)
            IMPLICIT none
            CLASS(guga_) :: this
            CHARACTER(len=*),INTENT(in) :: fich
            INTEGER,INTENT(in),OPTIONAL :: nOrbRas3In
            INTEGER :: nOrbRas3
            INTEGER :: nELec
            INTEGER :: stat
            LOGICAL :: exists
            INTEGER :: iDet,iCSF,nDet,nCSF,nCAS,counter
            INTEGER,ALLOCATABLE ::zahler(:),nenner(:)
            LOGICAL*1,ALLOCATABLE :: conf(:,:)
            CHARACTER(len=:), ALLOCATABLE :: csf
            CHARACTER(len=:), ALLOCATABLE :: det(:)

            nOrbRas3=0
            IF (PRESENT(nOrbRas3In)) THEN
                nOrbRas3=nOrbRas3In
            ENDIF

            INQUIRE(FILE=TRIM(fich),EXIST=exists)
            IF (.NOT.(exists)) STOP "ERROR: File does not exist"
            OPEN(1,FILE=TRIM(fich),STATUS="old",IOSTAT=stat)
            IF (stat.ne.0) STOP "ERROR: Could not open file"
            READ(1,*) nCSF,nCAS
             
            this%nUniqueCSF=nCSF
            this%activeSpaceSize=nCAS
            
            ALLOCATE(CHARACTER(nCAS) :: csf)
           
            ALLOCATE(this%csf(nCSF))
            iCSF=0
            DO counter=1,this%nUniqueCSF     
                READ(1,*)  csf,nDet
                iCSF=iCSF+1
                CALL string2num(csf,this%csf(iCSF)%conf)
                ALLOCATE(CHARACTER(nCAS) :: det(nDet))
                ALLOCATE(zahler(nDet),nenner(nDet))
                ALLOCATE(this%csf(iCSF)%det(nDet))
                this%csf(iCSF)%nDet=nDet
                CALL string2num(csf,this%csf(iCSF)%conf)  
                READ(1,*) (zahler(iDet),nenner(iDet),det(iDet),iDet=1,nDet)
                DO iDet=1,nDet
                    CALL string2num(det(iDet),this%csf(iCsf)%det(iDet)%occ)
                    this%csf(iCSF)%det(iDet)%zahler=zahler(iDet)
                    this%csf(iCSF)%det(iDet)%nenner=nenner(iDet)
                ENDDO
                DEALLOCATE(det,zahler,nenner)
            ENDDO
            this%nUniqueCSF=nCSF
        END SUBROUTINE
        
        SUBROUTINE initOneIntegralCSFMap(this,CSF1,CSF2,intCSFMap)
            USE oneIntCSFMapIO
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(in) :: CSF1,CSF2
            TYPE(oneIntCSFMap_), INTENT(inout) :: intCSFMap
            INTEGER :: inactive,diff,occupied
            INTEGER :: iDet,jDet,iOrb,iSpin
            INTEGER :: orb(2,2),spin(2,2),nCAS,iCAS
            !diff=SUM(ABS(COUNT(this%csf(CSF1)%conf,2)-COUNT(this%csf(CSF2)%conf,2)))/2
            !IF (diff.gt.1) THEN
            !    RETURN
            !ENDIF
            CALL intCSFMap%getOccupied(occupied)
            nCAS=this%activeSpaceSize
            inactive=occupied-nCAS
            IF (occupied.lt.nCAS) THEN
                WRITE(6,'(A,I3,x,A,I3)') "ERROR: ncas=",nCAS,"occupied=",occupied
                STOP "Cannot have active space larger than total number of orbitals included"
            ENDIF                       
            DO iDet=1,this%csf(CSF1)%nDet
                DO jDet=1,this%csf(CSF2)%nDet
                    !diff=compareDeterminants(this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,nCAS)
                    diff=COUNT(this%csf(CSF1)%det(iDet)%occ.neqv.this%csf(CSF2)%det(jDet)%occ)/2
                    IF (diff.gt.1) THEN
                        CYCLE
                    ELSEIF (diff.eq.0) THEN
                        DO iOrb=1,inactive
                            CALL intCSFMap%incrementCSFSpace(iOrb,iOrb)
                        ENDDO          
                        DO iCAS=1,nCAS
                            DO iSpin=1,2
                                IF (this%csf(CSF1)%det(iDet)%occ(iCas,iSpin)) THEN
                                    CALL intCSFMap%incrementCSFSpace(iCAS+inactive,iCAS+inactive)
                                ENDIF
                            ENDDO
                        ENDDO            
                    ELSE
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin)
                        IF (spin(1,1).ne.spin(2,1)) CYCLE
                        CALL intCSFMap%incrementCSFSpace(orb(1,1)+inactive,orb(2,1)+inactive)  
                    ENDIF
                ENDDO
            ENDDO
        END SUBROUTINE
        
        SUBROUTINE createOneIntegralCSFMap(this,CSF1,CSF2,intCSFMap)
            USE oneIntCSFMapIO
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(in) :: CSF1,CSF2
            TYPE(oneIntCSFMap_), INTENT(inout) :: intCSFMap
            INTEGER :: inactive,diff,occupied
            INTEGER :: iDet,jDet,iOrb,iSpin
            INTEGER :: orb(2,2),spin(2,2),nCAS,iCAS
            INTEGER :: zahler,nenner
            INTEGER :: phase
            !diff=SUM(ABS(COUNT(this%csf(CSF1)%conf,2)-COUNT(this%csf(CSF2)%conf,2)))/2
            !IF (diff.gt.1) THEN
            !    RETURN
            !ENDIF
            CALL intCSFMap%getOccupied(occupied)
            nCAS=this%activeSpaceSize
            inactive=occupied-nCAS
            DO iDet=1,this%csf(CSF1)%nDet
                DO jDet=1,this%csf(CSF2)%nDet
                    !diff=compareDeterminants(this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,nCAS)
                    diff=COUNT(this%csf(CSF1)%det(iDet)%occ.neqv.this%csf(CSF2)%det(jDet)%occ)/2
                    IF (diff.gt.1) THEN
                        CYCLE
                    ELSEIF (diff.eq.0) THEN
                        zahler=this%csf(CSF1)%det(iDet)%zahler*this%csf(CSF2)%det(jDet)%zahler
                        nenner=this%csf(CSF1)%det(iDet)%nenner*this%csf(CSF2)%det(jDet)%nenner
                        DO iOrb=1,inactive
                            CALL intCSFMap%addCSF(iOrb,iOrb,CSF1,CSF2,4*zahler,nenner)
                        ENDDO          
                        DO iCAS=1,nCAS
                            DO iSpin=1,2
                                IF (this%csf(CSF1)%det(iDet)%occ(iCas,iSpin)) THEN
                                    CALL intCSFMap%addCSF(iCAS+inactive,iCAS+inactive,CSF1,CSF2,zahler,nenner)
                                ENDIF
                            ENDDO
                        ENDDO            
                    ELSE
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin,phase)
                        IF (spin(1,1).ne.spin(2,1)) CYCLE
                        zahler=this%csf(CSF1)%det(iDet)%zahler*this%csf(CSF2)%det(jDet)%zahler
                        nenner=this%csf(CSF1)%det(iDet)%nenner*this%csf(CSF2)%det(jDet)%nenner
                        CALL intCSFMap%addCSF(orb(1,1)+inactive,orb(2,1)+inactive,CSF1,CSF2,zahler*phase,nenner) 
                    ENDIF
                ENDDO
            ENDDO
        END SUBROUTINE
        
        FUNCTION CSFDiff(this,CSF1,CSF2) RESULT(diff)
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(in) :: CSF1,CSF2
            INTEGER :: diff
            diff=SUM(ABS(COUNT(this%csf(CSF1)%conf,2)-COUNT(this%csf(CSF2)%conf,2)))/2
        END FUNCTION

        SUBROUTINE initTwoIntegralCSFMap(this,CSF1,CSF2,intCSFMap)
            USE twoIntCSFMapIO
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(in) :: CSF1,CSF2
            TYPE(twoIntCSFMap_), INTENT(inout) :: intCSFMap
            INTEGER :: inactive,diff,occupied
            INTEGER :: iDet,jDet,iOrb,iSpin,jOrb,jSpin,kOrb,lOrb
            INTEGER :: mOrb,nOrb,pOrb,qOrb
            INTEGER :: orb(2,2),spin(2,2),nCAS,iCAS,jCAS
            LOGICAL :: delta1,delta2,delta3,delta4
            !diff=SUM(ABS(COUNT(this%csf(CSF1)%conf,2)-COUNT(this%csf(CSF2)%conf,2)))/2
            !IF (diff.gt.2) THEN
            !    RETURN
            !ENDIF

            CALL intCSFMap%getOccupied(occupied)
            nCAS=this%activeSpaceSize
            inactive=occupied-nCAS
           
            DO iDet=1,this%csf(CSF1)%nDet
                DO jDet=1,this%csf(CSF2)%nDet
                    diff=COUNT(this%csf(CSF1)%det(iDet)%occ.neqv.this%csf(CSF2)%det(jDet)%occ)/2
                    IF (diff.gt.2) THEN
                        CYCLE
                    ELSEIF (diff.eq.0) THEN
                        !CYCLE !DEBUG
                        DO iOrb=1,occupied
                            DO jOrb=1,occupied
                                DO iSpin=1,2
                                    DO jSpin=1,2
                                        delta1=kronecker(iSpin,jSpin)
                                        IF (iOrb.le.inactive.AND.jOrb.le.inactive) THEN
                                            CALL intCSFMap%incrementCSFSpace(iOrb,iOrb,jOrb,jOrb)
                                            IF (delta1) CALL intCSFMap%incrementCSFSpace(iOrb,jOrb,jOrb,iOrb)
                                        ELSEIF (iOrb.le.inactive.AND.jOrb.gt.inactive) THEN
                                            jCAS=jOrb-inactive
                                            IF (this%csf(CSF1)%det(iDet)%occ(jCas,jSpin)) THEN
                                                CALL intCSFMap%incrementCSFSpace(iOrb,iOrb,jOrb,jOrb)
                                                IF (delta1) CALL intCSFMap%incrementCSFSpace(iOrb,jOrb,jOrb,iOrb)
                                            ENDIF
                                        ELSEIF (iOrb.gt.inactive.AND.jOrb.le.inactive) THEN
                                            iCAS=iOrb-inactive
                                            IF (this%csf(CSF1)%det(iDet)%occ(iCas,iSpin)) THEN
                                                CALL intCSFMap%incrementCSFSpace(iOrb,iOrb,jOrb,jOrb)
                                                IF (delta1) CALL intCSFMap%incrementCSFSpace(iOrb,jOrb,jOrb,iOrb)
                                            ENDIF
                                        ELSE
                                            iCAS=iOrb-inactive
                                            jCAS=jOrb-inactive
                                            IF (    this%csf(CSF1)%det(iDet)%occ(iCas,iSpin).AND.&
                                                    this%csf(CSF1)%det(iDet)%occ(jCas,jSpin)) THEN
                                                    CALL intCSFMap%incrementCSFSpace(iOrb,iOrb,jOrb,jOrb)
                                                IF (delta1) CALL intCSFMap%incrementCSFSpace(iOrb,jOrb,jOrb,iOrb)
                                            ENDIF
                                        ENDIF
                                    ENDDO
                                ENDDO
                            ENDDO
                        ENDDO          
                    ELSEIF (diff.eq.1) THEN
                        !CYCLE !DEBUG 
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin)
                        DO iOrb=1,occupied
                            DO iSpin=1,2
                                delta1=kronecker(iSpin,spin(1,1))
                                delta2=kronecker(iSpin,spin(2,1))
                                delta3=kronecker(spin(1,1),spin(2,1))          
                                jOrb=inactive+orb(1,1)
                                kOrb=inactive+orb(2,1)
                                IF (iOrb.le.inactive) THEN
                                    IF (delta3) CALL intCSFMap%incrementCSFSpace(jOrb,kOrb,iOrb,iOrb)                    
                                    IF (delta1.AND.delta2) CALL intCSFMap%incrementCSFSpace(jOrb,iOrb,iOrb,kOrb)
                                ELSE
                                    iCAS=iOrb-inactive
                                    IF (    this%csf(CSF1)%det(iDet)%occ(iCas,iSpin).AND.&
                                        this%csf(CSF2)%det(jDet)%occ(iCas,iSpin)) THEN  
                                        IF (delta3) CALL intCSFMap%incrementCSFSpace(jOrb,kOrb,iOrb,iOrb)                    
                                        IF (delta1.AND.delta2) CALL intCSFMap%incrementCSFSpace(jOrb,iOrb,iOrb,kOrb)
                                    ENDIF  
                                ENDIF
                            ENDDO
                        ENDDO
                    ELSE
                        !CYCLE !DEBUG
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin)
                        iOrb=inactive+orb(1,1)
                        jOrb=inactive+orb(1,2)
                        kOrb=inactive+orb(2,1)
                        lOrb=inactive+orb(2,2)
                        delta1=kronecker(spin(1,1),spin(2,1))
                        delta2=kronecker(spin(1,2),spin(2,2))
                        delta3=kronecker(spin(1,1),spin(2,2))
                        delta4=kronecker(spin(1,2),spin(2,1))
                        IF (delta1.AND.delta2) THEN
                            CALL intCSFMap%incrementCSFSpace(iOrb,kOrb,jOrb,lOrb)
                        ENDIF
                        IF (delta3.AND.delta4) THEN
                            CALL intCSFMap%incrementCSFSpace(iOrb,lOrb,jOrb,kOrb)
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
        END SUBROUTINE
        
        SUBROUTINE createTwoIntegralCSFMap(this,CSF1,CSF2,intCSFMap)
            USE twoIntCSFMapIO
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(in) :: CSF1,CSF2
            TYPE(twoIntCSFMap_), INTENT(inout) :: intCSFMap
            INTEGER :: inactive,diff,occupied
            INTEGER :: iDet,jDet,iOrb,iSpin,jOrb,jSpin,kOrb,lOrb
            INTEGER :: orb(2,2),spin(2,2),nCAS,iCAS,jCAS
            INTEGER :: zahler,nenner
            LOGICAL :: delta1,delta2,delta3,delta4
            INTEGER :: phase
            !diff=SUM(ABS(COUNT(this%csf(CSF1)%conf,2)-COUNT(this%csf(CSF2)%conf,2)))/2
            !IF (diff.gt.2) THEN
            !    RETURN
            !ENDIF

            CALL intCSFMap%getOccupied(occupied)
            nCAS=this%activeSpaceSize
            inactive=occupied-nCAS
            DO iDet=1,this%csf(CSF1)%nDet
                DO jDet=1,this%csf(CSF2)%nDet
                    !diff=compareDeterminants(this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,nCAS)
                    diff=COUNT(this%csf(CSF1)%det(iDet)%occ.neqv.this%csf(CSF2)%det(jDet)%occ)/2
                    IF (diff.gt.2) THEN
                        CYCLE
                    ELSEIF (diff.eq.0) THEN
                        !CYCLE !DEBUG
                        zahler=this%csf(CSF1)%det(iDet)%zahler*this%csf(CSF2)%det(jDet)%zahler
                        nenner=4*this%csf(CSF1)%det(iDet)%nenner*this%csf(CSF2)%det(jDet)%nenner
                        DO iOrb=1,occupied
                            DO jOrb=1,occupied
                                DO iSpin=1,2
                                    DO jSpin=1,2
                                        delta1=kronecker(iSpin,jSpin)
                                        IF (iOrb.le.inactive.AND.jOrb.le.inactive) THEN
                                            CALL intCSFMap%addCSF(iOrb,iOrb,jOrb,jOrb,CSF1,CSF2,zahler,nenner)
                                            IF (delta1) THEN
                                                CALL intCSFMap%addCSF(iOrb,jOrb,jOrb,iOrb,CSF1,CSF2,-zahler,nenner)
                                            ENDIF
                                        ELSEIF (iOrb.le.inactive.AND.jOrb.gt.inactive) THEN
                                            jCAS=jOrb-inactive
                                            IF (this%csf(CSF1)%det(iDet)%occ(jCas,jSpin)) THEN
                                                CALL intCSFMap%addCSF(iOrb,iOrb,jOrb,jOrb,CSF1,CSF2,zahler,nenner)
                                                IF (delta1) THEN
                                                    CALL intCSFMap%addCSF(iOrb,jOrb,jOrb,iOrb,CSF1,CSF2,-zahler,nenner)
                                                ENDIF
                                            ENDIF
                                        ELSEIF (iOrb.gt.inactive.AND.jOrb.le.inactive) THEN
                                            iCAS=iOrb-inactive
                                            IF (this%csf(CSF1)%det(iDet)%occ(iCas,iSpin)) THEN
                                                CALL intCSFMap%addCSF(iOrb,iOrb,jOrb,jOrb,CSF1,CSF2,zahler,nenner)
                                                IF (delta1) THEN
                                                    CALL intCSFMap%addCSF(iOrb,jOrb,jOrb,iOrb,CSF1,CSF2,-zahler,nenner)
                                                ENDIF
                                            ENDIF
                                        ELSE
                                            iCAS=iOrb-inactive
                                            jCAS=jOrb-inactive
                                            IF (    this%csf(CSF1)%det(iDet)%occ(iCas,iSpin).AND.&
                                                this%csf(CSF1)%det(iDet)%occ(jCas,jSpin)) THEN
                                                CALL intCSFMap%addCSF(iOrb,iOrb,jOrb,jOrb,CSF1,CSF2,zahler,nenner)
                                                IF (delta1) THEN
                                                    CALL intCSFMap%addCSF(iOrb,jOrb,jOrb,iOrb,CSF1,CSF2,-zahler,nenner)
                                                ENDIF
                                            ENDIF
                                        ENDIF
                                    ENDDO
                                ENDDO
                            ENDDO
                        ENDDO          
                    ELSEIF (diff.eq.1) THEN
                        !CYCLE !DEBUG
                        zahler=this%csf(CSF1)%det(iDet)%zahler*this%csf(CSF2)%det(jDet)%zahler
                        nenner=this%csf(CSF1)%det(iDet)%nenner*this%csf(CSF2)%det(jDet)%nenner
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin,phase)
                        zahler=zahler*phase

                        DO iOrb=1,occupied
                            DO iSpin=1,2
                                delta1=kronecker(iSpin,spin(1,1))
                                delta2=kronecker(iSpin,spin(2,1))
                                delta3=kronecker(spin(1,1),spin(2,1))          
                                jOrb=inactive+orb(1,1)
                                kOrb=inactive+orb(2,1)
                                IF (iOrb.le.inactive) THEN
                                    IF (delta3) THEN
                                        CALL intCSFMap%addCSF(jOrb,kOrb,iOrb,iOrb,CSF1,CSF2,zahler,nenner)
                                    ENDIF
                                    IF (delta1.AND.delta2) THEN
                                        CALL intCSFMap%addCSF(jOrb,iOrb,iOrb,kOrb,CSF1,CSF2,-zahler,nenner)
                                    ENDIF
                                ELSE
                                    iCAS=iOrb-inactive
                                    IF (    this%csf(CSF1)%det(iDet)%occ(iCas,iSpin).AND.&
                                        this%csf(CSF2)%det(jDet)%occ(iCas,iSpin)) THEN  
                                        IF (delta3) THEN
                                            CALL intCSFMap%addCSF(jOrb,kOrb,iOrb,iOrb,CSF1,CSF2,zahler,nenner)
                                        ENDIF
                                        IF (delta1.AND.delta2) THEN
                                            CALL intCSFMap%addCSF(jOrb,iOrb,iOrb,kOrb,CSF1,CSF2,-zahler,nenner)
                                        ENDIF
                                    ENDIF  
                                ENDIF
                            ENDDO
                        ENDDO
                    ELSE
                        !CYCLE !DEBUG
                        zahler=this%csf(CSF1)%det(iDet)%zahler*this%csf(CSF2)%det(jDet)%zahler
                        nenner=this%csf(CSF1)%det(iDet)%nenner*this%csf(CSF2)%det(jDet)%nenner
                        CALL locateDeviantSpinOrbitals( this%csf(CSF1)%det(iDet)%occ,this%csf(CSF2)%det(jDet)%occ,&
                                                        nCas,diff,orb,spin,phase)
                        zahler=zahler*phase

                        iOrb=inactive+orb(1,1)
                        jOrb=inactive+orb(1,2)
                        kOrb=inactive+orb(2,1)
                        lOrb=inactive+orb(2,2)
                        delta1=kronecker(spin(1,1),spin(2,1))
                        delta2=kronecker(spin(1,2),spin(2,2))
                        delta3=kronecker(spin(1,1),spin(2,2))
                        delta4=kronecker(spin(1,2),spin(2,1))
                        IF (delta1.AND.delta2) THEN
                            CALL intCSFMap%addCSF(iOrb,kOrb,jOrb,lOrb,CSF1,CSF2,+zahler,nenner)
                        ENDIF
                        IF (delta3.AND.delta4) THEN
                            CALL intCSFMap%addCSF(iOrb,lOrb,jOrb,kOrb,CSF1,CSF2,-zahler,nenner)
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
        END SUBROUTINE

        SUBROUTINE getCSFMatrix(this,csfMatrix)
            IMPLICIT none
            CLASS(guga_) :: this
            LOGICAL*1, INTENT(inout), ALLOCATABLE :: csfMatrix(:,:,:)
            INTEGER :: iCSF
            IF(ALLOCATED(csfMatrix)) DEALLOCATE(csfMatrix)
            ALLOCATE(csfMatrix(this%nUniqueCSF,this%activeSpaceSize,2))
            DO iCSF=1,this%nUniqueCSF
                csfMatrix(iCSF,:,:)=this%csf(iCSF)%conf
            ENDDO
        END SUBROUTINE

        SUBROUTINE getNumberOfCSF(this,n)
            IMPLICIT none
            CLASS(guga_) :: this
            INTEGER, INTENT(out) :: n
            n=this%nUniqueCSF
        END SUBROUTINE
END MODULE
