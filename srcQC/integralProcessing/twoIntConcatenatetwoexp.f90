!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntConcatenatetwoexp

        IMPLICIT NONE
        LOGICAL              :: first
        INTEGER              :: stat, exp1, exp2, BufSize
        INTEGER              :: nbas, nind, m, IntCounter, i
        INTEGER, ALLOCATABLE :: BufIn(:)
        REAL*8, ALLOCATABLE  :: BufVal(:)
        CHARACTER*200        :: intFile, nfile, dataDir,input


        BufSize = 10000

        allocate(BufIn(BufSize),BufVal(BufSize))

        CALL GETARG(1,input) 
        OPEN(101,FILE=TRIM(input),STATUS="old")
        READ(101,*) dataDir
        READ(101,*) nfile
        intFile=trim(dataDir)//'/ham2E.compressed.int.'//trim(nfile)
        OPEN(103,FILE=intFile,STATUS='replace',FORM='unformatted')
        
        IntCounter = 0
        stat = 0
        first = .TRUE.
        READ(101,*) nind
        DO m=1,nind
            READ(101,*) exp1, exp2
            WRITE(IntFile,"(A,I0,A,I0)") 'ham2E.compressed.int.',exp1,'-',exp2
            OPEN(102,FILE=IntFile,STATUS='old',FORM='unformatted')
            READ(102,IOSTAT=stat) nbas
            IF (first) THEN
                WRITE(103) nbas,'key'
                first = .FALSE.
            ENDIF
            DO WHILE (stat .eq. 0)
                IntCounter = IntCounter + 1
                READ(102,IOSTAT=stat) BufIn(IntCounter), BufVal(IntCounter)
                IF (stat .ne. 0) THEN
                    DO i = 1, IntCounter - 1
                        WRITE(103) BufIn(i), BufVal(i)
                    ENDDO
                    IntCounter = 0
                ENDIF
                IF (IntCounter .eq. BufSize) THEN
                    DO i = 1, IntCounter
                        WRITE(103) BufIn(i), BufVal(i)
                    ENDDO
                    IntCounter = 0
                ENDIF
            ENDDO
            CLOSE(102)
        ENDDO
        CLOSE(103)

END PROGRAM
