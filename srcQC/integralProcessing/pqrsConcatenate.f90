!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM pqrsConcatenate
    IMPLICIT NONE

    INTEGER             :: occupied, local, iOrb, jOrb, kOrb, lOrb, stat, stat2, lLow
    LOGICAL             :: exists, first
    CHARACTER*200       :: orbitalFile, outputFile, inputFile, indexesFile
    CHARACTER*200       :: tmpFile, ind
    REAL*8, allocatable :: PQRS1(:,:,:,:), PQRS2(:,:,:,:)
    REAL*8              :: Val, thrCt
    integer counter

    NAMELIST /INPUT/   orbitalFile,&
        outputFile,&
        IndexesFile,&
        thrCt

    thrCt=1e-14 

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    OPEN(100,file=trim(IndexesFile),status='old')

    stat = 0
    first = .true.

    DO WHILE (stat .eq. 0)
        READ(100,*,IOSTAT=stat) ind
        IF (stat .ne. 0) exit
        tmpFile = trim(orbitalFile)//trim(ind)
        OPEN(101,file=tmpFile,form='unformatted',status='old')
        stat2 = 0
        READ(101,IOSTAT=stat2) iOrb, jOrb, kOrb, lOrb
        Write(*,*) iOrb, jOrb, kOrb, lOrb
        IF (first) THEN
            ALLOCATE(PQRS1(iOrb,jOrb,kOrb,lOrb))
            ALLOCATE(PQRS2(iOrb,kOrb,jOrb,lOrb))
            occupied = iOrb
            local = jOrb
            PQRS1 = 0.d0
            PQRS2 = 0.d0
            first = .false.
        ENDIF
        counter=0
        DO WHILE (stat2 .eq. 0)
            counter=counter+1
            READ(101,IOSTAT=stat2) iOrb, jOrb, kOrb, lOrb, val
            IF (stat2 .ne. 0) exit
            IF ((kOrb.le.local).OR.(lOrb.le.local)) THEN
                IF ((kOrb.le.local).OR.(lOrb.le.local)) THEN
                    PQRS1(jOrb,iOrb,lOrb,kOrb) = PQRS1(jOrb,iOrb,lOrb,kOrb) + val
                ELSE
                    PQRS1(jOrb,iOrb,kOrb,lOrb) = PQRS1(jOrb,iOrb,kOrb,lOrb) + val
                ENDIF
            ELSE
                PQRS2(kOrb,lOrb,iOrb,jOrb) = PQRS2(kOrb,lOrb,iOrb,jOrb) + val
            ENDIF
        ENDDO
        CLOSE(101)
        write(6,"(A,A,X,I0)") "Number of non-zero integrals in ",trim(tmpFile),counter
    ENDDO

    CLOSE(100)

    counter=0
    OPEN(100,file=outputFile,form='unformatted',status='replace')
    WRITE(100) occupied,local,occupied,local
    DO iOrb=1,local
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb) 
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied   
                    IF ((jOrb.gt.local).AND.(kOrb.gt.local).AND.(lOrb.gt.local)) CYCLE
                    IF ((kOrb.le.local).OR.(lOrb.le.local)) THEN
                        IF (kOrb.le.lOrb) THEN  
                            IF (ABS(pqrs1(jOrb,iOrb,lOrb,kOrb)).gt.thrCt) THEN
                                counter=counter+1
                                WRITE(100) iOrb,jOrb,kOrb,lOrb,PQRS1(jOrb,iOrb,lOrb,kOrb)
                            ENDIF
                        ELSE
                            IF (ABS(pqrs1(jOrb,iOrb,kOrb,lOrb)).gt.thrCt) THEN
                                counter=counter+1
                                WRITE(100) iOrb,jOrb,kOrb,lOrb,PQRS1(jOrb,iOrb,kOrb,lOrb)
                            ENDIF
                        ENDIF
                    ELSE
                        IF (ABS(pqrs2(kOrb,lOrb,iOrb,jOrb)).gt.thrCt) THEN
                            counter=counter+1
                            WRITE(100) iOrb,jOrb,kOrb,lOrb,PQRS2(kOrb,lOrb,iOrb,jOrb)
                        ENDIF
                    ENDIF
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    CLOSE(100)
    write(6,"(A,I0)") "Number of non-zero integrals in concatenated file ",counter
    WRITE(6,'(A)') "Happy"

    


END PROGRAM
