!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM compressTwoElectronIntegrals
!PROGRAM THAT READS TWO ELECTRON INTEGRALS FROM MOLCAS OUTPUT
!AND REWRITES THIS USING USEFUL INDECES. FURTHERMORE SKIPS
!ZERO INTEGRALS, INTEGRALS REDUNDANT DUE TO SYMMETRIES IN INDECES
!AND INTEGRALS NOT COMPLYING WITH LMAX AND KMAX WHICH CAN BE SPECIFIED BY USER.
!FURTHERMORE OUTPUTS A FILE THAT CONTAINS INFORMATION
!ABOUT MONOCENTRIC BLOCKS: SIZE,LABEL,QUANTUMNUMBERS L,M,K
        USE stopWatch
        IMPLICIT none
        TYPE integral_   
                REAL*8 :: I
                INTEGER :: ijkl(4)
                LOGICAL :: seen=.TRUE.
        END TYPE
        TYPE(stopWatch_) :: timer
        CHARACTER*200 :: inputFile
        CHARACTER*200 :: petiteBasisList
        CHARACTER*200 :: twoElectronIntegralsMolcas
        CHARACTER*200 :: twoElectronIntegralsCompressed
        CHARACTER*200 :: blockInfoFile
        INTEGER :: stat,iDump,pos
        LOGICAL :: exists
        
        INTEGER :: nBas,nShell
        INTEGER :: iShell,iTemp
        INTEGER :: mQ,lQ
        INTEGER :: center,tempCenter
        INTEGER :: pB,qB,rB,sB
        INTEGER :: iBas,jBas,kBas,lBas
        INTEGER :: A,B,C,D
        INTEGER :: AM,BM,CM,DM
        INTEGER :: iM,jM,kM,lM
        INTEGER :: maxMQ
        INTEGER :: nIntegrals,relevantIntegral
        INTEGER :: deleted,written
        INTEGER :: lMax,kMax
        INTEGER :: K,L,M
        INTEGER,ALLOCATABLE :: bK(:),bL(:)
        INTEGER,ALLOCATABLE :: bID(:,:),shellNBas(:),shellL(:),shellIndex(:)
        CHARACTER*16 :: label,mLabel,cTemp
        CHARACTER*16 :: atom
        CHARACTER*26 :: letters

        CHARACTER*16 :: tempLabel
        INTEGER :: iBlock,nBlock
        CHARACTER*16,ALLOCATABLE :: blockLabel(:)
        INTEGER, ALLOCATABLE :: blockSize(:)
        INTEGER, ALLOCATABLE :: blockQuantumNumbers(:,:) 
        CHARACTER*10,ALLOCATABLE :: orb(:)
        
        REAL*8,ALLOCATABLE :: vec(:,:,:,:)

        TYPE(integral_), ALLOCATABLE :: integral(:)
        INTEGER :: iInt,jInt,iInd

        LOGICAL :: symCheck
 
        NAMELIST /INPUT/        petiteBasisList,& 
                                twoElectronIntegralsMolcas,&
                                twoElectronIntegralsCompressed,&
                                blockInfoFile,&
                                lMax,&
                                kMax
        lMax=0
        kMax=0
        blockInfoFile="list.blockBasis"
        letters="abcdefghijklmnopqrstuvwxyz"

        WRITE(6,'(x,A)') "Start: compressTwoElectronIntegrals"
        WRITE(6,'(x,A)') "Looking for input file to be provided:"

        CALL GETARG(1,inputFile)
        IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
        WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
        INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
        IF (.NOT.exists) STOP "input file does not exist"
        OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
        IF (stat.ne.0) STOP "failure to open input file"
        READ(1,NML=INPUT,IOSTAT=stat)
        IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
        WRITE(UNIT=6,NML=INPUT)
        CLOSE(1)

        IF (kMax.lt.0) THEN
                WRITE(6,'(A)') "kMax=-1 was given. Set kMax=INT(LMax/2) to consider all blocks for specified l"
                kMax=lMax/2
        ENDIF

        OPEN(1,file=TRIM(petiteBasisList),status="old")
        OPEN(2,file=TRIM(twoElectronIntegralsMolcas),status="old",form="unformatted")
        !OPEN(2,file=TRIM(twoElectronIntegralsMolcas),status="old")

        OPEN(3,file=TRIM(twoElectronIntegralsCompressed),status="replace",form="unformatted")
        !OPEN(3,file=TRIM(twoElectronIntegralsCompressed),status="replace")
        
        tempLabel=" "
        nBlock=0
        DO 
                READ(1,*,IOSTAT=STAT) nBas,atom,label
                IF (IS_IOSTAT_END(stat)) EXIT
                IF (SCAN(atom,"X").gt.0) THEN
                        IF (TRIM(tempLabel).ne.TRIM(label)) THEN
                                tempLabel=label
                                nBlock=nBlock+1
                        ENDIF
                ENDIF
        ENDDO
        REWIND(1)
        ALLOCATE (bID(nBas,3))
        ALLOCATE (shellIndex(nBas))
        ALLOCATE (orb(nBas))
        ALLOCATE (bK(nBas),bL(nBas))

        ALLOCATE (blockLabel(nBlock))
        ALLOCATE (blockQuantumNumbers(nBlock,3))
        ALLOCATE (blockSize(nBlock))
        blockSize=0

        cTemp=" "
        iTemp=0
        nShell=0
        tempCenter=0
        tempLabel=" "
        iBlock=0
        CALL timer%initialize()
        DO pB=1,nBas
                READ(1,*,IOSTAT=STAT) iDump,atom,label,center
                orb(pB)=TRIM(label)
                pos=SCAN(label,letters)
                CALL getL(label,L)
                CALL getK(label,K)
                CALL getM(label,M)
                READ(label(1:pos-1),*) lQ
                IF ((lQ.ne.iTemp).OR.(center.ne.tempCenter)) THEN
                        nShell=nShell+1
                        iTemp=lQ
                        mQ=0
                ENDIF

                mLabel=label(pos:LEN(TRIM(label)))
                IF ((TRIM(mLabel).eq.TRIM(cTemp)).AND.(center.eq.tempCenter)) THEN
                        iBas=iBas+1
                ELSE
                        iBas=1
                        mQ=mQ+1
                        cTemp=mLabel
                        tempCenter=center
                ENDIF
                bID(pB,1)=nShell
                bID(pB,2)=mQ
                bID(pB,3)=iBas
                shellIndex(pB)=lQ
                IF (SCAN(atom,"X").gt.0) THEN
                        bK(pB)=K
                        bL(pB)=L
                        IF (TRIM(tempLabel).ne.TRIM(label)) THEN
                                tempLabel=label
                                iBlock=iBlock+1
                                blockSize(iBlock)=blockSize(iBlock)+1
                                blockLabel(iBlock)=TRIM(label)
                                blockQuantumNumbers(iBlock,1)=K
                                blockQuantumNumbers(iBlock,2)=L
                                blockQuantumNumbers(iBlock,3)=M
                        ELSE
                                blockSize(iBlock)=blockSize(iBlock)+1
                        ENDIF
                ELSE
                        bK(pB)=0
                        bL(pB)=0
                ENDIF
        ENDDO
        CLOSE(1)

        ALLOCATE(shellNBas(nShell))
        ALLOCATE(shellL(nShell))
        shellNBas=0
        DO pB=1,nBas
                shellNBas(bID(pB,1))=shellNBas(bID(pB,1))+1
                shellL(bID(pB,1))=shellIndex(pB)
        ENDDO
        DO iShell=1,nShell
                shellNBas(iShell)=shellNBas(iShell)/(shellL(iShell)*(shellL(iShell)+1)/2)
        ENDDO
        WRITE(6,'(x,A)') "Completed Processing of Petetis Basis List"
        WRITE(6,'(2x,A,x,I5)') "Number of shells: ",nShell
        WRITE(6,'(2x,A)') "Basis Functions per mL value in each:"
        WRITE(6,'(<nShell>(x,I5))') (shellNBas(iShell),iShell=1,nShell)
        WRITE(6,'(2x,A)') "Angular momentum of each:"
        WRITE(6,'(<nShell>(x,I5))') (shellL(iShell),iShell=1,nShell)
        WRITE(6,'(2x,A,x,I5)') "Number of basis functions: ",nBas
        WRITE(6,'(x,A)') "Table to identify each basis function:"
        WRITE(6,'(x,A16,2x,A7,2x,A5,2x,A5,2x,A10,2x,A15,2x,A11,2x,A11)') "petite basis fct","orbital","shell","l","mL+l+1","shell basis fct","quant num K","quant num L"
        DO pB=1,nBas
                WRITE(6,'(x,I16,2x,A7,2x,I5,2x,I5,2x,I10,2x,I15,2x,I11,2x,I11)') pB,TRIM(orb(pB)),bID(pB,1),shellL(bID(pB,1))-1,bID(pB,2),bID(pB,3),bK(pB),bL(pB)
        ENDDO
        
        IF (lMax.lt.0) lMax=MAXVAL(bL)
        IF (kMax.lt.0) kMax=MAXVAL(bK)

        WRITE(6,'(A,I3,2x,A,I3)') "lMax=",lMax,"kMax=",kMax

        OPEN(1,file=TRIM(blockInfoFile),STATUS="replace")
        DO iBlock=1,nBlock
                WRITE(1,'(I3,x,A7,x,I3,x,I3,x,I3)') blockSize(iBlock),TRIM(blockLabel(iBlock)),blockQuantumNumbers(iBlock,2),blockQuantumNumbers(iBlock,3),blockQuantumNumbers(iBlock,1)
        ENDDO
        CLOSE(1)
       
        maxMQ=MAXVAL(bID(:,2))

        WRITE(3),nBas
        !WRITE(3,'(x,I6)'),nBas

        ALLOCATE(vec(maxMQ,maxMQ,maxMQ,maxMQ))
        ALLOCATE(integral(1))
        deleted=0
        written=0
        DO 
          READ(2,IOSTAT=stat) A,B,C,D
          !READ(2,*,IOSTAT=stat) A,B,C,D
          AM=shellL(A)*2-1
          BM=shellL(B)*2-1
          CM=shellL(C)*2-1
          DM=shellL(D)*2-1
          nIntegrals=AM*BM*CM*DM*shellNBas(A)*shellNBas(B)*shellNBas(C)*shellNBas(D)
          DEALLOCATE(integral)
          ALLOCATE(integral(nIntegrals))
          relevantIntegral=0
          IF (stat.ne.0) EXIT
          DO lBas=1,shellNBas(D)
            DO kBas=1,shellNBas(C)
              DO jBas=1,shellNBas(B)
                DO iBas=1,shellNBas(A)
                  READ(2) ((((vec(lM,kM,jM,iM),iM=1,AM),jM=1,BM),kM=1,CM),lM=1,DM)    
                  !READ(2,*) ((((vec(lM,kM,jM,iM),iM=1,AM),jM=1,BM),kM=1,CM),lM=1,DM)    

                  DO iM=1,AM
                    DO jM=1,BM
                      DO kM=1,CM 
                        DO lM=1,DM
                          IF (ABS(vec(lM,kM,jM,iM)).lt.1e-18) THEN
                                deleted=deleted+1
                                CYCLE
                          ENDIF
                          DO pB=1,nBas
                                IF (ABS(bID(pB,1)-A)+ABS(bID(pB,2)-iM)+ABS(bID(pB,3)-iBas).eq.0) EXIT
                          ENDDO      
                          DO qB=1,nBas
                                IF (ABS(bID(qB,1)-B)+ABS(bID(qB,2)-jM)+ABS(bID(qB,3)-jBas).eq.0) EXIT
                          ENDDO      
                          DO rB=1,nBas
                                IF (ABS(bID(rB,1)-C)+ABS(bID(rB,2)-kM)+ABS(bID(rB,3)-kBas).eq.0) EXIT
                          ENDDO      
                          DO sB=1,nBas
                                IF (ABS(bID(sB,1)-D)+ABS(bID(sB,2)-lM)+ABS(bID(sB,3)-lBas).eq.0) EXIT
                          ENDDO      
                          IF ((pB.gt.nBas).OR.(qB.gt.nBas).OR.(rB.gt.nBas).OR.(sB.gt.nBas)) THEN
                                WRITE(6,'(4(x,I6))') A,B,C,D
                                STOP "Error: could not map to Basis fcts"
                          ENDIF
                          IF (bK(pB).gt.kMax.OR.bK(pB).gt.kMax.OR.bK(rB).gt.kMax.OR.bK(sB).gt.kMax) THEN
                                deleted=deleted+1       
                                CYCLE
                          ELSEIF (bL(pB).gt.lMax.OR.bL(pB).gt.lMax.OR.bL(rB).gt.lMax.OR.bL(sB).gt.lMax) THEN
                                deleted=deleted+1
                                CYCLE
                          ELSE
                                relevantIntegral=relevantIntegral+1
                                integral(relevantIntegral)%I=vec(lM,kM,jM,iM)
                                integral(relevantIntegral)%ijkl(1)=pB
                                integral(relevantIntegral)%ijkl(2)=qB
                                integral(relevantIntegral)%ijkl(3)=rB
                                integral(relevantIntegral)%ijkl(4)=sB
                                integral(relevantIntegral)%seen=.FALSE.
                          ENDIF
                        ENDDO
                      ENDDO    
                    ENDDO    
                  ENDDO

                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !THIS I CAN POTENTIALLY MAKE A FAIR BIT MORE EFFICIENT
          DO iInt=1,relevantIntegral-1
                IF (integral(iInt)%seen) CYCLE 
                DO jInt=iInt+1,relevantIntegral
                        IF (integral(jInt)%seen) CYCLE
                        integral(jInt)%seen=symCheck(integral(iInt)%ijkl,integral(jInt)%ijkl)                        
                ENDDO
          ENDDO
          DO iInt=1,relevantIntegral
                IF (integral(iInt)%seen) CYCLE
                written=written+1
                WRITE(3),(integral(iInt)%ijkl(iInd),iInd=1,4),integral(iInt)%I
                !WRITE(3,'(4(x,I8),x,E18.12)'),(integral(iInt)%ijkl(iInd),iInd=1,4),integral(iInt)%I
          ENDDO
          WRITE(6,'(A,x,I10,x,A,x,I10)') "Progress report: Integrals written:",written,"Integrals deleted:",deleted
        ENDDO   
        CALL timer%time("Integrals Compressed")        
        WRITE(6,'(A,x,I)') "Total number of integrals written to file:",written
        WRITE(6,'(A)') "Happy"
END PROGRAM

FUNCTION symCheck(ijkl,wxyz) RESULT (seen)
        INTEGER, INTENT(in) :: ijkl(4),wxyz(4)
        LOGICAL :: seen
        IF ((ijkl(1).eq.wxyz(1)).AND.(ijkl(2).eq.wxyz(2)).AND.(ijkl(3).eq.wxyz(4)).AND.(ijkl(4).eq.wxyz(3))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(2)).AND.(ijkl(2).eq.wxyz(1)).AND.(ijkl(3).eq.wxyz(3)).AND.(ijkl(4).eq.wxyz(4))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(2)).AND.(ijkl(2).eq.wxyz(1)).AND.(ijkl(3).eq.wxyz(4)).AND.(ijkl(4).eq.wxyz(3))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(3)).AND.(ijkl(2).eq.wxyz(4)).AND.(ijkl(3).eq.wxyz(1)).AND.(ijkl(4).eq.wxyz(2))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(3)).AND.(ijkl(2).eq.wxyz(4)).AND.(ijkl(3).eq.wxyz(2)).AND.(ijkl(4).eq.wxyz(1))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(4)).AND.(ijkl(2).eq.wxyz(3)).AND.(ijkl(3).eq.wxyz(1)).AND.(ijkl(4).eq.wxyz(2))) THEN
                seen=.TRUE.
        ELSEIF ((ijkl(1).eq.wxyz(4)).AND.(ijkl(2).eq.wxyz(3)).AND.(ijkl(3).eq.wxyz(2)).AND.(ijkl(4).eq.wxyz(1))) THEN
                seen=.TRUE.
        ELSE
                seen=.FALSE.
        ENDIF
END FUNCTION

SUBROUTINE getM(label,m)
        IMPLICIT none
        CHARACTER(len=*),INTENT(in) :: label    
        INTEGER, INTENT(out) :: m
        CHARACTER*1 :: labelList(10)
        CHARACTER*26 :: letters
        CHARACTER*1 :: lC
        INTEGER :: p,q

        letters="abcdefghijklmnopqrstuvwxyz"
        
        p=SCAN(label,letters)
        lC=label(p:p)
        IF (lc.eq."s") THEN
                m=0
        ELSEIF (label(p:p+1).eq."px") THEN
                m=1
        ELSEIF (label(p:p+1).eq."py") THEN
                m=-1
        ELSEIF (label(p:p+1).eq."pz") THEN
                m=0
        ELSE
                q=SCAN(label," ")
                IF (label(q-1:q).eq."0") THEN
                        m=0
                ELSEIF (label(q-1:q).eq."-") THEN
                        q=q-1
                        READ(label(p+1:q-1),*) m
                        m=m*-1
                ELSE
                        q=q-1
                        READ(label(p+1:q-1),*) m
                ENDIF
        ENDIF
END SUBROUTINE
SUBROUTINE getL(label,l)
        IMPLICIT none
        CHARACTER(len=*),INTENT(in) :: label    
        INTEGER, INTENT(out) :: l
        CHARACTER*1 :: labelList(10)
        CHARACTER*26 :: letters
        CHARACTER*1 :: lC
        INTEGER :: p,q

        letters="abcdefghijklmnopqrstuvwxyz"
        labelList(1)="s"
        labelList(2)="p"
        labelList(3)="d"
        labelList(4)="f"
        labelList(5)="g"
        labelList(6)="h"
        labelList(7)="i"
        labelList(8)="j"
        labelList(9)="k"
        labelList(10)="l"
        
        p=SCAN(label,letters)
        lC=label(p:p)
        DO q=1,10
                IF (lC.eq.labelList(q)) THEN
                        l=q-1
                        EXIT
                ENDIF
        ENDDO
END SUBROUTINE
SUBROUTINE getK(label,k)
        IMPLICIT none
        CHARACTER(len=*),INTENT(in) :: label    
        INTEGER, INTENT(out) :: k
        CHARACTER*1 :: labelList(10)
        CHARACTER*26 :: letters
        CHARACTER*1 :: lC
        CHARACTER*10 :: temp
        INTEGER :: p,q,l,shell

        letters="abcdefghijklmnopqrstuvwxyz"
        labelList(1)="s"
        labelList(2)="p"
        labelList(3)="d"
        labelList(4)="f"
        labelList(5)="g"
        labelList(6)="h"
        labelList(7)="i"
        labelList(8)="j"
        labelList(9)="k"
        labelList(10)="l"
        
        p=SCAN(label,letters)
        lC=label(p:p)
        DO q=1,10
                IF (lC.eq.labelList(q)) THEN
                        l=q
                        EXIT
                ENDIF
        ENDDO
        temp=label(1:p-1)
        READ(temp,*) shell
        k=(shell-l)/2
END SUBROUTINE
