!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE twoIntCSFMapIO
!CLASS FOR OBJECTS THAT LINK TWO ELECTRON INTEGRAL ijkl
!TO THE MATRIX ELEMENT <csf1|g|csf2> THAT DEPEND ON IT
        IMPLICIT none
        PRIVATE
        LOGICAL :: debug=.FALSE.
        PUBLIC twoIntCSFMap_
        !INTERNAL TYPES
        TYPE csfList_
                INTEGER :: nCSF=0
                INTEGER :: next=1
                INTEGER, ALLOCATABLE :: csf(:,:)
                INTEGEr, ALLOCATABLE :: zahler(:)
                INTEGEr, ALLOCATABLE :: nenner(:)
        END TYPE
        !CLASS STRUCTURE
        TYPE twoIntCSFMap_
                PRIVATE
                TYPE(csfList_), ALLOCATABLE :: csfList(:,:,:,:)
                INTEGER :: occupied=0
                INTEGER :: un=0
                LOGICAL :: alive=.FALSE.
                CONTAINS
                        PROCEDURE :: create
                        PROCEDURE :: incrementCSFSpace
                        PROCEDURE :: addCSF
                        PROCEDURE :: openTwoIntCSFMap
                        PROCEDURE :: writeTwoIntCSFMap
                        PROCEDURE :: getRecord
                        PROCEDURE :: getOccupied
                        PROCEDURE :: allocateCSF
                        PROCEDURE :: compress
        END TYPE
                
        CONTAINS
                !CLASS METHODS
                SUBROUTINE create(this,occupied)
                        IMPLICIT none
                        INTEGER, INTENT(in) :: occupied
                        CLASS(twoIntCSFMap_) :: this
                        this%occupied=occupied
                        ALLOCATE(this%csfList(occupied,occupied,occupied,occupied))
                END SUBROUTINE

                SUBROUTINE incrementCSFSpace(this,i,j,k,l)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER, INTENT(in) :: i,j,k,l
                        this%csfList(i,j,k,l)%nCSF=this%csfList(i,j,k,l)%nCSF+1
                        IF (debug) THEN
                                WRITE(6,'(A,x,4I4,A1,I4)') "Inc space for ijkl to",i,j,k,l,":",this%csfList(i,j,k,l)%nCSF
                        ENDIF
                END SUBROUTINE 
                        
                SUBROUTINE allocateCSF(this)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER :: nCSF,i,j,k,l,occ
                        occ=this%occupied
                        DO i=1,occ
                                DO j=1,occ
                                        DO k=1,occ
                                                DO l=1,occ
                                                        nCSF=this%csfList(i,j,k,l)%nCSF
                                                        ALLOCATE(this%csfList(i,j,k,l)%csf(nCSF,2))
                                                        ALLOCATE(this%csfList(i,j,k,l)%zahler(nCSF))
                                                        ALLOCATE(this%csfList(i,j,k,l)%nenner(nCSF))
                                                ENDDO
                                        ENDDO
                                ENDDO
                        ENDDO
                END SUBROUTINE

                SUBROUTINE addCSF(this,i,j,k,l,csf1,csf2,zahler,nenner)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER, INTENT(in) :: i,j,k,l,csf1,csf2,zahler,nenner
                        INTEGER :: nCSF,next
                        nCSF=this%csfList(i,j,k,l)%nCSF
                        next=this%csfList(i,j,k,l)%next
                        IF (debug) THEN
                                WRITE(6,'(A,x,4I4,A1,2I4,A1,I4)'),'Write to ijkl for the csf pair in position ',i,j,k,l,"<",csf1,csf2,">",next
                        ENDIF
                        IF (next.gt.nCSF) THEN
                                STOP "ERROR: csfSpace overflow"
                        ENDIF
                
                        this%csfList(i,j,k,l)%csf(next,1)=csf1
                        this%csfList(i,j,k,l)%csf(next,2)=csf2
                        this%csfList(i,j,k,l)%zahler(next)=zahler
                        this%csfList(i,j,k,l)%nenner(next)=nenner
                        next=next+1
                        this%csfList(i,j,k,l)%next=next
                END SUBROUTINE
                SUBROUTINE openTwoIntCSFMap(this,fich,un)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        CHARACTER(len=*), INTENT(in) :: fich
                        INTEGER, INTENT(in) :: un
                        this%un=un
                        OPEN(un,file=TRIM(fich),status="old",FORM="unformatted")
                        READ(un) this%occupied
                END SUBROUTINE
                SUBROUTINE getRecord(this,i,j,k,l,nCSF,csf,zahler,nenner,stat)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER,INTENT(out) :: i,j,k,l,nCSF,stat
                        INTEGER,ALLOCATABLE,INTENT(inout) :: csf(:,:),nenner(:),zahler(:)
                        INTEGER :: iCSF
                        READ (this%un) i,j,k,l,nCSF
                        IF (nCSF.eq.0) RETURN
                        IF (ALLOCATED(csf)) THEN
                                IF (size(csf,1).ne.nCSF.OR.size(csf,2).ne.2) THEN
                                        DEALLOCATE(csf)
                                        ALLOCATE(csf(nCSF,2))
                                ENDIF
                        ELSE
                                ALLOCATE(csf(nCSF,2))
                        ENDIF
                        IF (ALLOCATED(zahler)) THEN
                                IF (size(zahler,1).ne.nCSF) THEN
                                        DEALLOCATE(zahler)
                                        ALLOCATE(zahler(nCSF))
                                ENDIF
                        ELSE
                                ALLOCATE(zahler(nCSF))
                        ENDIF
                        IF (ALLOCATED(nenner)) THEN
                                IF (size(nenner,1).ne.nCSF) THEN
                                        DEALLOCATE(nenner)
                                        ALLOCATE(nenner(nCSF))
                                ENDIF
                        ELSE
                                ALLOCATE(nenner(nCSF))
                        ENDIF
                        READ(this%un,IOSTAT=stat)(      csf(iCSF,1),&
                                                        csf(iCSF,2),&
                                                        zahler(iCSF),&
                                                        nenner(iCSF),iCSF=1,nCSF)
                END SUBROUTINE

                SUBROUTINE writeTwoIntCSFMap(this,fich,sorte)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        CHARACTER(len=*), INTENT(in) :: fich
                        CHARACTER(len=*), INTENT(in),OPTIONAL :: sorte
                        
                        LOGICAL :: binary
                        INTEGER :: i,j,k,l
                        INTEGER :: occupied,stat,nCSF,iCSF
                        binary=.FALSE.
                        IF (PRESENT(sorte)) THEN
                                IF (sorte(1:1).eq.'b') binary=.TRUE.
                        ENDIF
                        IF (binary) OPEN(1,file=TRIM(fich),status="replace",FORM="unformatted",IOSTAT=stat)
                        IF (.NOT.binary) OPEN(1,file=TRIM(fich),status="replace",IOSTAT=stat)
                        occupied=this%occupied
                        IF (binary) WRITE(1) occupied
                        IF (.NOT.binary) WRITE(1,*) occupied
                        DO i=1,occupied
                                DO j=1,occupied
                                        DO k=1,occupied
                                                DO l=1,occupied
                                                        nCSF=this%csfList(i,j,k,l)%nCSF
                                                        IF (binary) WRITE(1) i,j,k,l,nCSF
                                                        IF (.NOT.binary) WRITE(1,'(5(x,I))',IOSTAT=stat) i,j,k,l,nCSF
                                                        IF (nCSF.gt.0) THEN
                                                                IF (.NOT.binary) THEN
                                                                        WRITE(1,'(<nCSF>(x,I5,x,I5,x,A1,x,I5,x,A1,x,I5,x,A1))',IOSTAT=stat) (this%csfList(i,j,k,l)%csf(iCSF,1),&
                                                                                        this%csfList(i,j,k,l)%csf(iCSF,2),":",&
                                                                                        this%csfList(i,j,k,l)%zahler(iCSF),"/",&
                                                                                        this%csfList(i,j,k,l)%nenner(iCSF),"|",iCSF=1,nCSF)
                                                                ELSE
                                                                        WRITE(1)        (this%csfList(i,j,k,l)%csf(iCSF,1),&
                                                                                        this%csfList(i,j,k,l)%csf(iCSF,2),&
                                                                                        this%csfList(i,j,k,l)%zahler(iCSF),&
                                                                                        this%csfList(i,j,k,l)%nenner(iCSF),iCSF=1,nCSF)
                                                                ENDIF
                                                        ENDIF
                                                ENDDO
                                        ENDDO
                                ENDDO
                        ENDDO
                        CLOSE(1)
                END SUBROUTINE

                SUBROUTINE compress(this)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER :: iOrb,jOrb,kOrb,lOrb,counter
                        INTEGER :: iCSF,nCSF,tCSF(2),csf(2)
                        INTEGER :: zahler,nenner,tZ,tN
                        INTEGER :: newNCSF,occupied
                        INTEGER, ALLOCATABLE :: newZahler(:),newNenner(:),newCSF(:,:)
                        occupied=this%occupied

                        DO iOrb=1,occupied
                         DO jOrb=1,occupied
                          DO kOrb=1,occupied
                           DO lOrb=1,occupied
                            nCSF=this%csfList(iOrb,jOrb,kOrb,lOrb)%nCSF
                            IF (nCSF.le.1) CYCLE
                            tCSF(1)=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(1,1)
                            tCSF(2)=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(1,2)
                            tZ=this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler(1)
                            tN=this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner(1)
                            !can increase mCSF every
                            !time a new block
                            !commences
                            newNCSF=0
                            counter=0 
                            DO iCSF=1,nCSF
                                    csf=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(iCSF,:)
                                    zahler=this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler(iCSF)
                                    nenner=this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner(iCSF)
                                    IF (csf(1).ne.tCsf(1).OR.csf(2).ne.tCsf(2).OR.ABS(tZ).ne.ABS(zahler).OR.tN.ne.nenner) THEN
                                            IF (abs(counter).gt.0) THEN
                                                    newNCSF=newNCSF+1 
                                            ENDIF
                                            tCSF=csf
                                            tZ=zahler
                                            tN=nenner
                                            counter=tZ/abs(tZ)
                                    ELSE
                                            counter=counter+zahler/ABS(zahler)
                                    ENDIF
                            ENDDO 
                            IF (abs(counter).gt.0) THEN
                                    newNCSF=newNCSF+1
                            ENDIF
                            ALLOCATE(newZahler(newNCSF))
                            ALLOCATE(newNenner(newNCSF))
                            ALLOCATE(newCSF(newNCSF,2))
                            tCSF(1)=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(1,1)
                            tCSF(2)=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(1,2)
                            tZ=this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler(1)  
                            tN=this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner(1)
                            !must wait until end to
                            !of block of equialent
                            !terms before storing >
                            !figure this out
                            newNCSF=0
                            counter=0
                            DO iCSF=1,nCSF
                                    csf=this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(iCSF,:)
                                    zahler=this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler(iCSF)
                                    nenner=this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner(iCSF)
                                    IF (csf(1).eq.tCsf(1).AND.csf(2).eq.tCsf(2).AND.ABS(tZ).eq.ABS(zahler).AND.tN.eq.nenner) THEN
                                            counter=counter+zahler/ABS(zahler)                                            
                                    ELSE
                                            IF (abs(counter).gt.0) THEN
                                                    newNCSF=newNCSF+1
                                                    newZahler(newNCSF)=abs(tZ)*counter**3/abs(counter)
                                                    newNenner(newNCSF)=abs(tN)
                                                    newCSF(newNCSF,:)=tCSF
                                            ENDIF
                                            tCSF=csf
                                            tZ=zahler
                                            tN=nenner
                                            counter=tZ/abs(tZ)
                                    ENDIF
                            ENDDO
                            IF (abs(counter).gt.0) THEN
                                    newNCSF=newNCSF+1
                                    newZahler(newNCSF)=abs(tZ)*counter**3/abs(counter)
                                    newNenner(newNCSF)=tN
                                    newCSF(newNCSF,:)=tCSF
                            ENDIF
                            this%csfList(iOrb,jOrb,kOrb,lOrb)%nCSF=newNCSF
                            DEALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%csf)
                            DEALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler)
                            DEALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner)
                            ALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%csf(newNCSF,2))
                            ALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler(newNCSF))
                            ALLOCATE(this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner(newNCSF))
                            this%csfList(iOrb,jOrb,kOrb,lOrb)%csf=newCSF
                            this%csfList(iOrb,jOrb,kOrb,lOrb)%nenner=newNenner
                            this%csfList(iOrb,jOrb,kOrb,lOrb)%zahler=newZahler
                            DEALLOCATE(newZahler)
                            DEALLOCATE(newNenner)
                            DEALLOCATE(newCSF)
                           ENDDO
                          ENDDO
                         ENDDO
                        ENDDO
                END SUBROUTINE
                
                SUBROUTINE getOccupied(this,n)
                        IMPLICIT none
                        CLASS(twoIntCSFMap_) :: this
                        INTEGER, INTENT(out) :: n
                        n=this%occupied
                END SUBROUTINE
END MODULE
