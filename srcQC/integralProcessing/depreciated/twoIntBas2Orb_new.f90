!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

!This needs to get a lot more efficient still
!either find way to eliminate more orbitals
!or make it faster somehow in loop part
        USE omp_lib
        USE twoIntIO
        USE orbitalIO
        USE stopWatch
        IMPLICIT none
        INTEGER :: stat
        LOGICAL :: exists
        TYPE(stopWatch_) :: timer
        TYPE(twoInt_) :: gBas
        TYPE(twoInt_) :: gOrb
        TYPE(orbital_) :: orb
        
        INTEGER ::occupied,local
        CHARACTER*200 :: inputFile
        CHARACTER*200 :: orbitalFile
        CHARACTER*200 :: twoElectronOperatorBasisFile
        CHARACTER*200 :: twoElectronOperatorOrbitalFile
        CHARACTER*200 :: token
        
        REAL*8 :: orb1,orb2,orb3,orb4
        REAL*8 :: twoElBas,twoElOrb,temp
        REAL*8, ALLOCATABLE :: oM(:,:)
        REAL*8, ALLOCATABLE :: tempVec(:)
        INTEGER :: iBas,jBas,kBas,lBas
        INTEGER :: iOrb,jOrb,kOrb,lOrb
        INTEGER :: nBas
	INTEGER :: pm(8,4),iPm,nPm
        INTEGER :: nAugElectrons,counter
        INTEGER :: nInd,iInd,jInd,tInd,startInd,endInd
        INTEGER :: lLow,nmono
        !INTEGER, ALLOCATABLE :: indeces(:,:)
        INTEGER, ALLOCATABLE :: IDs(:)
        !PARALLELIZATION VARIABLES        
        INTEGER :: nCPU,nrOfNodes,node
        
        
        NAMELIST /INPUT/        orbitalFile,&
                                occupied,&
                                twoElectronOperatorBasisFile,&
                                twoElectronOperatorOrbitalFile,&
                                local,&
                                nrOfNodes,&
                                node

        nrOfNodes=0
        node=0
        local=0                                

        CALL GETARG(1,inputFile)
        IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
        WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
        INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
        IF (.NOT.exists) STOP "input file does not exist"
        OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
        IF (stat.ne.0) STOP "failure to open input file"
        READ(1,NML=INPUT,IOSTAT=stat)
        IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
        WRITE(UNIT=6,NML=INPUT)

        IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       

        CALL orb%readOrbital(orbitalFile,"b") 
        CALL gBas%readTwoInt(twoElectronOperatorBasisFile,"b",1)
        CALL gBas%getNBas(nBas)
        ALLOCATE(oM(nBas,occupied))
        DO iOrb=1,occupied
                CALL orb%getOrbital(tempVec,iOrb)
                oM(:,iOrb)=tempVec
        ENDDO

        WRITE(6,'(I8,A)') nBas," basis functions in compressed integral file"

        nCPU=OMP_get_max_threads()
        WRITE(6,'(A,x,I4)') "number of available threads:",nCPU        
        IF (occupied**2.le.nCPU) THEN
                CALL omp_set_num_threads(occupied**2)
        ELSE
                CALL omp_set_num_threads(nCPU)
        ENDIF
        nCPU=OMP_get_max_threads()
        WRITE(6,'(A,x,I4)') "number of threads set to:",nCPU        

        WRITE(6,'(A)') "identify orbital indeces with one electron only in augmented orbitals"
        tInd=0
        DO iOrb=1,occupied
                DO jOrb=iOrb,occupied
                        DO kOrb=iOrb,occupied
                                IF (iOrb.eq.kOrb) THEN
                                        lLow=MAX(jOrb,kOrb)
                                ELSE
                                        lLow=kOrb
                                ENDIF
                                DO lOrb=lLow,occupied
                                        nmono=0
                                        IF (iOrb.gt.local) nmono=nmono+1
                                        IF (jOrb.gt.local) nmono=nmono+1
                                        IF (kOrb.gt.local) nmono=nmono+1
                                        IF (lOrb.gt.local) nmono=nmono+1
                                        IF (nmono.gt.3) CYCLE
                                        tInd=tInd+1
                                ENDDO
                        ENDDO
                ENDDO
        ENDDO
        WRITE(6,'(I10,A,I10,A)') tInd," of ",occupied**4," indeces must be calculated"
        IF (node.gt.nrOfNodes) THEN
                STOP "ERROR: node cannot be geater than number of nodes"
        ELSEIF (nrOfNodes.gt.0.AND.node.gt.0) THEN
                nInd=CEILING(REAL(tInd)/REAL(nrOfNodes))   
                startInd=nInd*(node-1)+1
                IF (node.lt.nrOfNodes) THEN
                        endInd=nInd*node
                ELSE
                        endInd=tInd
                        nInd=endInd-startInd+1
                ENDIF                
                WRITE(6,'(A,I4,A,I4,A)') "node ",node," of ",nrOfNodes," nodes "
                WRITE(6,'(A,I10,A,I10,A)') "Working of indeces ",startInd," to ",endInd," in this node"
        ELSE
                WRITE(6,'(A)') "no distribution to different nodes."
                nInd=tInd
                startInd=1
                endInd=nInd
        ENDIF

        CALL gOrb%initializeEmpty(occupied,nInd)

        iInd=0
        jInd=0
        DO iOrb=1,occupied
                DO jOrb=iOrb,occupied
                        DO kOrb=iOrb,occupied
                                IF (iOrb.eq.kOrb) THEN
                                        lLow=MAX(jOrb,kOrb)
                                ELSE
                                        lLow=kOrb
                                ENDIF
                                DO lOrb=lLow,occupied
                                        nmono=0
                                        IF (iOrb.gt.local) nmono=nmono+1
                                        IF (jOrb.gt.local) nmono=nmono+1
                                        IF (kOrb.gt.local) nmono=nmono+1
                                        IF (lOrb.gt.local) nmono=nmono+1
                                        IF (nmono.gt.3) CYCLE
                                        iInd=iInd+1
                                        IF (iInd.ge.startInd.AND.iInd.le.endInd) THEN
                                                jInd=jInd+1
                                                CALL gOrb%setIndex(jInd,iOrb,jOrb,kOrb,lOrb)
                                        ENDIF
                                ENDDO
                        ENDDO
                ENDDO
        ENDDO
        
        counter=0
        CALL timer%initialize()
        CALL timer%time("test stop watch dead time")
	DO
		CALL gBas%getVal(iBas,jBas,kBas,lBas,twoElBas,stat)
                counter=counter+1
         
                IF (iBas.gt.nBas.OR.jBas.gt.nBas.OR.kBas.gt.nBas.OR.lBas.gt.nBas) THEN
                        WRITE(6,'(A,4(x,I18))') "ERROR: invalid index values:",iBas,jBas,kBas,lBas
                        STOP "ERROR: an integral index was inconsisten with the number of basis functions"
                ENDIF
		IF (stat.ne.0) EXIT
		pm(1,1)=iBas;pm(1,2)=jBas;pm(1,3)=kBas;pm(1,4)=lBas
                nPm=1
                IF ((iBas.eq.jBas).AND.(kBas.eq.lBas).AND.(iBas.ne.kBas)) THEN
		        pm(2,1)=kBas;pm(2,2)=lBas;pm(2,3)=iBas;pm(2,4)=jBas
                        nPm=2
                ELSEIF ((iBas.ne.jBas).AND.(kbas.eq.lBas)) THEN
		        pm(2,1)=jBas;pm(2,2)=iBas;pm(2,3)=kBas;pm(2,4)=lBas
		        pm(3,1)=kBas;pm(3,2)=lBas;pm(3,3)=iBas;pm(3,4)=jBas
		        pm(4,1)=kBas;pm(4,2)=lBas;pm(4,3)=jBas;pm(4,4)=iBas
                        nPm=4
                ELSEIF ((iBas.eq.jBas).AND.(kbas.ne.lBas)) THEN
		        pm(2,1)=iBas;pm(2,2)=jBas;pm(2,3)=lBas;pm(2,4)=kBas
		        pm(3,1)=kBas;pm(3,2)=lBas;pm(3,3)=iBas;pm(3,4)=jBas
		        pm(4,1)=lBas;pm(4,2)=kBas;pm(4,3)=iBas;pm(4,4)=jBas
                        nPm=4
                ELSEIF ((iBas.ne.jBas).AND.(kBas.ne.lBas)) THEN
		        pm(2,1)=jBas;pm(2,2)=iBas;pm(2,3)=kBas;pm(2,4)=lBas
		        pm(3,1)=iBas;pm(3,2)=jBas;pm(3,3)=lBas;pm(3,4)=kBas
		        pm(4,1)=jBas;pm(4,2)=iBas;pm(4,3)=lBas;pm(4,4)=kBas
                        nPm=4
                        IF (.NOT.(((iBas.eq.kBas).AND.(jBas.eq.lBas)).OR.((iBas.eq.lBas).AND.(jBas.eq.kBas)))) THEN
		                pm(5,1)=kBas;pm(5,2)=lBas;pm(5,3)=iBas;pm(5,4)=jBas
		                pm(6,1)=kBas;pm(6,2)=lBas;pm(6,3)=jBas;pm(6,4)=iBas
		                pm(7,1)=lBas;pm(7,2)=kBas;pm(7,3)=iBas;pm(7,4)=jBas
		                pm(8,1)=lBas;pm(8,2)=kBas;pm(8,3)=jBas;pm(8,4)=iBas
                                nPm=8
                        ENDIF
                ENDIF

                !GET RID OF GORB OBJECT TO SEE HIW THAT EFFECTS PREFORMANCE

		!$OMP PARALLEL &
		!$OMP DEFAULT (none) &
		!$OMP SHARED (orb,gOrb,twoElBas,nInd,nPm,pm,oM) &
		!$OMP PRIVATE (iInd,orb1,orb2,orb3,orb4,iOrb,jOrb,kOrb,lOrb,twoElOrb,iPm,temp) 
		!$OMP DO
		DO iInd=1,nInd
                        CALL gOrb%getIndex(iInd,iOrb,jOrb,kOrb,lOrb)
                        temp=0.d0
		        DO iPm=1,nPm
		                !CALL orb%getOrbital(orb1,iOrb,pm(iPm,1))
			        !CALL orb%getOrbital(orb2,jOrb,pm(iPm,2))
			        !CALL orb%getOrbital(orb3,kOrb,pm(iPm,3))
			        !CALL orb%getOrbital(orb4,lOrb,pm(iPm,4))
                                !temp=temp+orb1*orb2*orb3*orb4
                                temp=temp+oM(pm(iPm,1),iOrb)*oM(pm(iPm,2),jOrb)*oM(pm(iPm,3),kOrb)*oM(pm(iPm,4),lOrb)
			        !twoElOrb=twoElBas*orb1*orb2*orb3*orb4
		                !CALL gOrb%incVal(iInd,twoElOrb)
		        ENDDO
                        twoElOrb=twoElBas*temp
		        CALL gOrb%incVal(iInd,twoElOrb)
		ENDDO
		!$OMP END DO NOWAIT
		!$OMP END PARALLEL
                IF (mod(counter,1000).eq.0) THEN
                        WRITE(token,'(A,I12,A)') "Basis integral record ",counter," processed"
                        CALL timer%time(TRIM(token))
                ENDIF
	ENDDO        
        CALL timer%time("Orbitals done")        

        CALL gOrb%writeTwoInt(TRIM(twoElectronOperatorOrbitalFile),"b")
        WRITE(6,'(A)') "Happy"
END PROGRAM
