!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM oneIntOrb2OperatorMatrix
!PROGRAM THAT FROM A ONE ELECTRON 
!INTEGRAL TABLE (oneInt_ ) AND A REDUCED DENSITY MATRIX (oneIntRDM_)
!CREATES THE MATRIX ELEMENTS
    USE RDMIO
    USE oneIntIO
    USE sortingAlgorithms
    USE searchInsert
    USE stopWatch

    IMPLICIT none
    !CONTROL VARS
    INTEGER :: stat
    LOGICAL :: exists,debug
    CHARACTER*200 :: inputFile,fmt

    !CMD LINE VARS 
    CHARACTER*200 :: scaleFactorCmdLine
    CHARACTER*200 :: hermitianCmdLine

    !INPUT VARS
    CHARACTER*200 :: oneIntRDMFile
    CHARACTER*200 :: oneIntFile
    CHARACTER*200 :: outputFile
    CHARACTER*200 :: outputFormat

    INTEGER :: nSource
    INTEGER :: nParent
    INTEGER :: nInactive
    LOGICAL :: hermitian

    REAL*8 :: scaleFactor

    !STRUCTURAL VARS
    TYPE(RDM_) :: oneIntRDM
    TYPE(oneInt_) :: orbInt
    TYPE(oneInt_) :: operatorInt

    REAL*8, ALLOCATABLE :: operatorMatrix(:,:)
    REAL*8, ALLOCATABLE :: rdm(:,:),rdmMat(:,:,:)

    INTEGER :: nLocalOrb
    INTEGER :: nStateTotal
    INTEGER :: nStateRDM
    INTEGER :: nXOrb
    INTEGER :: nSourceAndLocalAugState
    INTEGER :: nIntegral
    INTEGER :: occupied
    INTEGER :: nRDM
    INTEGER :: nOrbInRdm
    
    INTEGER, ALLOCATABLE :: order(:),rdmKey(:)
    INTEGER, ALLOCATABLE :: stateID1(:),stateID2(:)
    INTEGER, ALLOCATABLE :: augmentingOrb(:)

    !TEMPORAL VARS
    INTEGER :: ind1(2)
    INTEGER :: m1,m2,key,location
    INTEGER :: iState,jState,kState,lState
    INTEGER :: iIon,jIon
    INTEGER :: iOrb,jOrb
    INTEGER :: iRDM
    LOGICAL :: empty
    REAL*8 :: integral
    REAL*8, ALLOCATABLE :: tempRealArr2d(:,:)
    REAL*8, ALLOCATABLE :: tempRealArr3d(:,:,:)

    NAMELIST /INPUT/ oneIntRDMFile,&
      oneIntFile,&
      outputFile,&
      nInactive,&
      nSource,&
      nParent,&
      scaleFactor,&
      debug,&
      outputFormat,&
      hermitian

    hermitian=.TRUE.
    outputFormat="b"
    debug=.FALSE.
    scaleFactor=1.d0

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)

    CALL GETARG(2,scaleFactorCmdLine)
    IF (LEN(TRIM(scaleFactorCmdLine)).gt.0) THEN
        READ(scaleFactorCmdLine,*) scaleFactor
        WRITE(6,'(A)') "scaleFactor command line overwrite. Value = "
        WRITE(6,'(F12.6)') scaleFactor 
    ENDIF
    CALL GETARG(3,hermitianCmdLine)
    IF (LEN(TRIM(hermitianCmdLine)).gt.0) THEN
        READ(hermitianCmdLine,*) hermitian
        WRITE(6,'(A)') "hermitian command line overwrite. Value = "
        WRITE(6,'(L)') hermitian
    ENDIF

    CALL oneIntRDM%initializeOld(TRIM(oneIntRDMFile),"b",1)
    CALL oneIntRDM%getStates(nStateRDM)
    CALL oneIntRDM%getOccupied(nOrbInRdm)

    CALL orbInt%readOneInt(trim(oneIntFile),"b",2)
    CALL orbInt%getSymSize(occupied)
    
    nLocalOrb=(nStateRDM-nSource)/nParent+nInactive-2
    nXOrb=occupied-nLocalOrb
    nStateTotal=(occupied-nInactive)*nParent+nSource
    nSourceAndLocalAugState=nStateRDM-2*nParent

    WRITE(6,'(A)') "Orbitals in RDM (polycentric) : "
    WRITE(6,'(A,I0)') "     ",nLocalOrb
    WRITE(6,'(A)') "Diffuse Orbitals : "
    WRITE(6,'(A,I0)') "     ",nXOrb
    WRITE(6,'(A)') "Total number of states to construct operator matrix :"
    WRITE(6,'(A,I0)') "     ",nStateTotal
    WRITE(6,'(A)') "Number of states in the RDM :"
    WRITE(6,'(A,I0)') "     ",nStateRDM
    WRITE(6,'(A)') "Number of States not resulting from augmentation in diffuse orbitals:"
    WRITE(6,'(A,I0)') "     ",nSourceAndLocalAugState
    WRITE(6,'(A)') "Number of orbitals defining rdm file"
    WRITE(6,'(A,I0)') "     ",nOrbInRdm
    
    ALLOCATE(operatorMatrix(nStateTotal,nStateTotal))
    ALLOCATE(rdm(nStateRDM,nStateRDM))
    ALLOCATE(stateID1(nStateTotal))
    ALLOCATE(stateID2(nStateTotal))
    ALLOCATE(augmentingOrb(nStateTotal))
    operatorMatrix=0.d0
    
    DO iState=1,nSource
        augmentingOrb(iState)=0
    ENDDO
    jState=nSource
    DO iOrb=1,occupied-nInactive
        DO iIon=1,nParent
            jState=jState+1
            augmentingOrb(jState)=iOrb
        ENDDO
    ENDDO


    DO iOrb=1,nSourceAndLocalAugState
        stateID1(iOrb)=iOrb
        stateID2(iOrb)=iOrb
    ENDDO
    jOrb=nSourceAndLocalAugState
    DO iOrb=1,nXOrb
        DO iIon=1,nParent
            jOrb=jOrb+1
            stateID1(jOrb)=nSourceAndLocalAugState+iIon
            stateID2(jOrb)=nSourceAndLocalAugState+iIon+nParent
        ENDDO
    ENDDO

    WRITE(6,'(A)') "READ trd1"
   
    nRDM=0
    DO
        READ(1,iostat=stat) ind1(1),ind1(2),empty
        IF (stat.ne.0) EXIT
        IF (.NOT.empty) THEN
            nRDM=nRDM+1
            READ(1)
        ENDIF
    ENDDO
    WRITE(6,'(A,I0)') "number of relevant rdms = ",nRDM
    WRITE(6,'(A,E18.6)') "memory necessary in GB = ",REAL(nRDM*nStateRDM*(nStateRDM+1))/REAL(2.*1024.**3.)
    ALLOCATE(rdmMat(nRDM,nStateRDM,nStateRDM))
    ALLOCATE(rdmKey(nRDM))
    ALLOCATE(order(nRDM))
    REWIND(1)
    READ(1)
    nRDM=0
    DO 
        READ(1,iostat=stat) ind1(1),ind1(2),empty
        IF (stat.ne.0) EXIT
        IF (.NOT.empty) THEN
            nRDM=nRDM+1
            order(nRDM)=nRDM
            READ(1) ((rdmMat(nRDM,jState,iState),jState=1,iState),iState=1,nStateRDM)
            rdmKey(nRDM)=ind1(1)+ind1(2)*(nOrbInRdm+1)
        ENDIF
    ENDDO
    CLOSE(1)

    ALLOCATE(tempRealArr3d(nRDM,nStateRDM,nStateRDM)) 
    WRITE(6,'(A)') "Sort RDMs"
    CALL QSortInt(nRDM,rdmKey,order)
    tempRealArr3d=rdmMat

    DO iRDM=1,nRDM
        rdmMat(iRDM,:,:)=tempRealArr3d(order(iRDM),:,:)
    ENDDO
    DEALLOCATE(tempRealArr3d,order)

    WRITE(6,'(A)') "BEGIN COMPUTATION"
    nIntegral=0
    DO
        CALL orbInt%getRecord(ind1(1),ind1(2),integral,stat)
        IF (stat.ne.0) THEN
            WRITE(6,'(A,I8,A)') "End of integral file reached after reading ",nIntegral," integrals"
            EXIT
        ENDIF
        nIntegral=nIntegral+1

        CALL posMax2Val(ind1,nLocalOrb,m1,m2)
        key=ind1(1)+ind1(2)*(nOrbInRdm+1)
        CALL binarySearch(key,nRDM,rdmKey,location)
        IF (location.ge.0) THEN
            CYCLE
        ELSE
            rdm=rdmMat(ABS(location),:,:) 
        ENDIF
        IF (m1.le.nLocalOrb.AND.&
          m2.le.nLocalOrb) THEN
            DO iState=1,nStateTotal
                IF (iState.gt.nSourceAndLocalAugState) THEN
                    kState=stateID1(iState)
                ELSE
                    kState=iState
                ENDIF
                DO jState=iState,nStateTotal
                    IF (jState.gt.nSourceAndLocalAugState) THEN
                        IF (augmentingOrb(iState).eq.augmentingOrb(jState)) THEN
                            lState=stateID1(jState)
                        ELSE 
                            lState=stateID2(jState)
                        ENDIF
                    ELSE
                        lState=jState
                    ENDIF
                    operatorMatrix(iState,jState)=&
                      operatorMatrix(iState,jState)+&
                      rdm(kState,lState)*integral
                ENDDO
            ENDDO
        ELSEIF (m1.le.nLocalOrb.OR.&
          m2.le.nLocalOrb) THEN
            DO iState=1,nSourceAndLocalAugState
                kState=iState
                DO jState=nSourceAndLocalAugState+(m1-nLocalOrb-1)*nParent+1,&
                          nSourceAndLocalAugState+(m1-nLocalOrb)*nParent
                    lState=stateID1(jState)
                    operatorMatrix(iState,jState)=&
                      operatorMatrix(iState,jState)+&
                      rdm(kState,lState)*integral
                ENDDO
            ENDDO
        ELSE
            DO iState=nSourceAndLocalAugState+(m2-nLocalOrb-1)*nParent+1,&
                      nSourceAndLocalAugState+(m2-nLocalOrb)*nParent
                kState=stateID1(iState)

                IF (m1.eq.m2) THEN
                    DO jState=iState,nSourceAndLocalAugState+(m2-nLocalOrb)*nParent
                        IF (augmentingOrb(iState).eq.augmentingOrb(jState)) THEN
                            lState=stateID1(jState)
                        ELSE
                            lState=stateID2(jState)
                        ENDIF
                        operatorMatrix(iState,jState)=&
                          operatorMatrix(iState,jState)+&
                          rdm(kState,lState)*integral
                    ENDDO
                ELSE
                    DO jState=nSourceAndLocalAugState+(m1-nLocalOrb-1)*nParent+1,&
                              nSourceAndLocalAugState+(m1-nLocalOrb)*nParent
                        lState=stateID2(jState)
                        operatorMatrix(iState,jState)=&
                          operatorMatrix(iState,jState)+&
                          rdm(kState,lState)*integral
                    ENDDO
                ENDIF
            ENDDO
        ENDIF
    ENDDO

    WRITE(6,'(A)') "Ensure matrices (anti)hermiticity"

    DO iState=1,nStateTotal
        DO jState=iState+1,nStateTotal
            IF (hermitian) THEN
                operatorMatrix(jState,iState)=operatorMatrix(iState,jState)
            ELSE
                operatorMatrix(jState,iState)=operatorMatrix(iState,jState)
                operatorMatrix(iState,jState)=-1.d0*operatorMatrix(iState,jState)
            ENDIF
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Create new order"

    ALLOCATE(order(nStateTotal))
    DO iOrb=1,nSource
        stateID1(iOrb)=iOrb
        order(iOrb)=iOrb
    ENDDO
    jOrb=nSource
    DO iOrb=1,occupied-nInactive
        DO iIon=1,nParent
            jOrb=jOrb+1
            order(jOrb)=jOrb
            stateID1(jOrb)=(iIon-1)*occupied+iOrb+nSource
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Sort"

    ALLOCATE(tempRealArr2d(nStateTotal,nStateTotal))
    tempRealArr2d=operatorMatrix
    CALL QSortInt(nStateTotal,stateID1,order)

    WRITE(6,'(A)') "Apply new order"

    DO iState=1,nStateTotal
        DO jState=1,nStateTotal
            operatorMatrix(iState,jState)=tempRealArr2d(order(iState),order(jState))
        ENDDO
    ENDDO

    WRITE(6,'(A)') "Write to disk"

    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,file=trim(outputFile),status="replace")
        WRITE(1,'(I0)') 1
        WRITE(1,'(I0)') nStateTotal
        WRITE(fmt,'(A,I0,A)') "(",nStateTotal,"(x,E30.20))"
        DO iState=1,nStateTotal
            WRITE(1,trim(fmt)) (operatorMatrix(jState,iState)/scaleFactor,jState=1,nStateTotal)
        ENDDO
    ELSE
        OPEN(1,file=trim(outputFile),status="replace",form="unformatted")
        WRITE(1) 1
        WRITE(1) nStateTotal
        DO iState=1,nStateTotal
            WRITE(1) (operatorMatrix(jState,iState)/scaleFactor,jState=1,nStateTotal)
        ENDDO
    ENDIF
    CLOSE(1)
    WRITE(6,'(A)') "Happy"

END PROGRAM

SUBROUTINE posMax2Val(ind,local,m1,m2)
    IMPLICIT none
    INTEGER, INTENT(inout) :: ind(2)
    INTEGER, INTENT(in) :: local
    INTEGER :: p1(1),p2(1)
    INTEGER,INTENT(out) :: m1,m2
    INTEGER :: indT(2)

    indT=ind
    m1=MAXVAL(indT)
    p1=MINLOC(ABS(indT-m1))
    indT(p1(1))=0
    m2=MAXVAL(indT)
    p2=MINLOC(ABS(indT-m2))
    IF (m1.le.local) THEN
        RETURN
    ELSEIF (m2.le.local) THEN
        ind(p1(1))=local+1
    ELSE
        IF (m1.eq.m2) THEN
            ind(p1(1))=local+1
            ind(p2(1))=local+1
        ELSE
            ind(p1(1))=local+2
            ind(p2(1))=local+1
        ENDIF
    ENDIF
END SUBROUTINE





