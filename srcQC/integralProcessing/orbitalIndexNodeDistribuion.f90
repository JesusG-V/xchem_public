!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM twoIntBas2Orb
!CODE THAT TRANSFORMS two ELECTRON INTEGRAL FILES TEASED FROM MOLCAS
!AND, TRANSFORMS THESE INTO INTEGRALS IN TERMS OF ORBITAL USING 
!A USER DEFINED SET OF ORBITAL COEFFICIENTS.

!This needs to get a lot more efficient still
!either find way to eliminate more orbitals
!or make it faster somehow in loop part
    USE omp_lib
    USE twoIntIO
    USE orbitalIO
    USE stopWatch
    IMPLICIT none
    INTEGER :: stat
    LOGICAL :: exists
    TYPE(stopWatch_) :: timer
    TYPE(twoInt_) :: gBas
    TYPE(twoInt_) :: gOrb
    TYPE(orbital_) :: orb
    
    INTEGER ::occupied,local
    CHARACTER*200 :: inputFile
    CHARACTER*200 :: orbitalFile
    CHARACTER*200 :: twoElectronOperatorBasisFile
    CHARACTER*200 :: twoElectronOperatorOrbitalFile
    CHARACTER*200 :: token,fmt
    CHARACTER*200 :: outputFormat
    
    REAL*8 :: orb1,orb2,orb3,orb4
    REAL*8 :: twoElBas,twoElOrb,temp
    REAL*8, ALLOCATABLE :: oM(:,:)
    REAL*8, ALLOCATABLE :: oMt(:)
    REAL*8, ALLOCATABLE :: tempVec(:)
    INTEGER :: iBas,jBas,kBas,lBas
    INTEGER :: iOrb,jOrb,kOrb,lOrb
    INTEGER :: nBas
    INTEGER :: pm(4,8),iPm,nPm
    INTEGER :: nAugElectrons,counter,counter2
    INTEGER :: nInd,iInd,jInd,tInd,startInd,endInd
    INTEGER :: lLow,nmono
    INTEGER, ALLOCATABLE :: tempIntArr(:)
    INTEGER, ALLOCATABLE :: IDs(:)
    INTEGER, ALLOCATABLE :: idx(:,:)
    REAL*8, ALLOCATABLE :: integrals(:)
    
    LOGICAL, ALLOCATABLE :: zeroCoef(:)
    
    !PARALLELIZATION VARIABLES        
    INTEGER :: nCPU,nrOfNodes,node
    
    NAMELIST /INPUT/ occupied,&
      local,&
      nrOfNodes
    
    nrOfNodes=1
    local=0                                
    
    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    
    IF (local.eq.0) WRITE(6,'(A)')  "WARNING: local is set to 0. Are there no localized orbitals?"       
    
    WRITE(6,'(A)') "identify orbital indeces with one electron only in augmented orbitals"
    tInd=0
    DO iOrb=1,occupied
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb)
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied
                    nmono=0
                    IF (iOrb.gt.local) nmono=nmono+1
                    IF (jOrb.gt.local) nmono=nmono+1
                    IF (kOrb.gt.local) nmono=nmono+1
                    IF (lOrb.gt.local) nmono=nmono+1
                    IF (nmono.ge.3) CYCLE
                    tInd=tInd+1
                ENDDO
            ENDDO
        ENDDO
    ENDDO
    WRITE(6,'(I10,A,I15,A)') tInd," of ",occupied**4," indeces must be calculated"
    IF (node.gt.nrOfNodes) THEN
        STOP "ERROR: node cannot be geater than number of nodes"
    ELSEIF (nrOfNodes.gt.0.AND.node.gt.0) THEN
        nInd=CEILING(REAL(tInd)/REAL(nrOfNodes))   
        startInd=nInd*(node-1)+1
        IF (node.lt.nrOfNodes) THEN
            endInd=nInd*node
        ELSE
            endInd=tInd
            nInd=endInd-startInd+1
        ENDIF                
        WRITE(6,'(A,I4,A,I4,A)') "node ",node," of ",nrOfNodes," nodes "
        WRITE(6,'(A,I10,A,I10,A)') "Working of indeces ",startInd," to ",endInd," in this node"
    ELSE
        WRITE(6,'(A)') "no distribution to different nodes."
        nInd=tInd
        startInd=1
        endInd=nInd
    ENDIF
    
    ALLOCATE(idx(4,nInd))
    ALLOCATE(integrals(nInd))
    integrals=0.d0
    
    iInd=0
    jInd=0
    DO iOrb=1,occupied
        DO jOrb=iOrb,occupied
            DO kOrb=iOrb,occupied
                IF (iOrb.eq.kOrb) THEN
                    lLow=MAX(jOrb,kOrb)
                ELSE
                    lLow=kOrb
                ENDIF
                DO lOrb=lLow,occupied
                    nmono=0
                    IF (iOrb.gt.local) nmono=nmono+1
                    IF (jOrb.gt.local) nmono=nmono+1
                    IF (kOrb.gt.local) nmono=nmono+1
                    IF (lOrb.gt.local) nmono=nmono+1
                    IF (nmono.ge.3) CYCLE
                    iInd=iInd+1
                    IF (iInd.ge.startInd.AND.iInd.le.endInd) THEN
                        jInd=jInd+1     
                        idx(1,jInd)=iOrb
                        idx(2,jInd)=jOrb
                        idx(3,jInd)=kOrb
                        idx(4,jInd)=lOrb
                    ENDIF
                ENDDO
            ENDDO
        ENDDO
    ENDDO
END PROGRAM 
