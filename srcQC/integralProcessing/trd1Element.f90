!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE trd1ElementHelper
contains
SUBROUTINE locateDeviantSpinOrbitals(occ1,occ2,nCas,diff,orb,spin,phase) 
    IMPLICIT none
    LOGICAL*1,INTENT(in) :: occ1(nCAS,2),occ2(nCAS,2)
    INTEGER,INTENT(in) :: nCAS
    INTEGER,INTENT(in) :: diff
    INTEGER,INTENT(out) :: orb(2,2),spin(2,2)
    INTEGER,INTENT(out),OPTIONAL :: phase
    INTEGER :: iCas,iSpin
    INTEGER :: iDiff,jDiff
    INTEGER :: jump1,jump2
    LOGICAL :: done
    iDiff=0
    jDiff=0
    orb=0
    spin=0
    jump1=0
    jump2=0
    done=.FALSE.
    DO iCas=1,nCAS
        DO iSpin=1,2
            IF (occ1(iCas,iSpin).eqv.occ2(iCas,iSpin)) CYCLE
            IF (occ1(iCas,iSpin)) THEN
                iDiff=iDiff+1
                orb(1,iDiff)=iCas
                spin(1,iDiff)=iSpin
            ELSE
                jDiff=jDiff+1
                orb(2,jDiff)=iCas
                spin(2,jDiff)=iSpin
            ENDIF 
            IF (iDiff.eq.diff.AND.jDiff.eq.diff) THEN
                done=.TRUE.
                EXIT
            ENDIF
        ENDDO
        IF (done) EXIT
    ENDDO
    
    IF (.NOT.PRESENT(phase)) RETURN
    
    DO iDiff=1,diff
        done=.FALSE.
        DO iCas=1,nCas
            DO iSpin=1,2
                IF (iCAS.eq.orb(1,iDiff).AND.iSpin.eq.spin(1,iDiff)) THEN
                    done=.TRUE.           
                    EXIT   
                ENDIF
                IF (occ1(iCas,iSpin)) THEN
                    jump1=jump1+1
                ENDIF
            ENDDO
            IF (done) EXIT
        ENDDO
    ENDDO
    
    DO iDiff=1,diff
        done=.FALSE.
        DO iCas=1,nCas
            DO iSpin=1,2
                IF (iCAS.eq.orb(2,iDiff).AND.iSpin.eq.spin(2,iDiff)) THEN
                    done=.TRUE.           
                    EXIT   
                ENDIF
                IF (occ2(iCas,iSpin)) THEN
                    jump2=jump2+1
                ENDIF
            ENDDO
            IF (done) EXIT
        ENDDO
    ENDDO
    IF (mod(jump1,2).ne.mod(jump2,2)) THEN
        phase=-1
    ELSE
        phase=1
    ENDIF
END SUBROUTINE
end module

PROGRAM trd1Element
!PROGRAM THAT DIRECTLY CREATES 
!ONE ELECTRON DESNITY MATRIX FROM DETEREMINTS
    USE trd1ElementHelper
    USE OMP_lib
    USE detIO
    USE stopWatch
    USE sortingAlgorithms
    IMPLICIT none
!CONTROL VARIABLES
    INTEGER :: stat
    LOGICAL :: exists
    CHARACTER*200 :: inputFile
    TYPE(stopWatch_) :: timer

!INPUT VARIABLES
    CHARACTER*200 :: detFile
    CHARACTER*200 :: trd1File
    CHARACTER*100 :: outputFormat
    INTEGER :: occupied
    INTEGER :: state1
    INTEGER :: state2

!STRUCTURE VARIABLES
    TYPE(det_) :: det
    INTEGER :: active
    INTEGER :: nState
    INTEGER :: nDet
    INTEGER :: nRAS
    INTEGER :: inact
    INTEGER :: nVal
    INTEGER :: nUsedDet1
    INTEGER :: nUsedDet2
    LOGICAL*1, ALLOCATABLE :: detConf(:,:,:)
    LOGICAL*1, ALLOCATABLE :: usedDetConf1(:,:,:)
    LOGICAL*1, ALLOCATABLE :: usedDetConf2(:,:,:)
    REAL*8, ALLOCATABLE :: detCoef(:,:)
    REAL*8, ALLOCATABLE :: usedDetCoef1(:)
    REAL*8, ALLOCATABLE :: usedDetCoef2(:)
    REAL*8, ALLOCATABLE :: rdm(:,:) 

    INTEGER :: maxNDiff2Det
    INTEGER,ALLOCATABLE :: nDiff2Det(:)
    INTEGER, ALLOCATABLE :: diff2DetMat(:,:)
    INTEGER*1, ALLOCATABLE :: diff2Mat(:,:)

    INTEGER :: nRasKey
    INTEGER, ALLOCATABLE :: nRasKeyDet(:)
    INTEGER, ALLOCATABLE :: rasKey(:)
    INTEGER, ALLOCATABLE :: order(:)
    LOGICAL*1, ALLOCATABLE :: tempConf(:,:,:)
    REAL*8, ALLOCATABLE :: tempCoef(:)
    INTEGER :: key,ikey

!TEMP VARS
    INTEGER :: diff,diff2
    INTEGER :: iDet,jDet,kDet,lDet
    INTEGER :: iSpin,iRAS
    INTEGER :: iOrb,jOrb
    INTEGER :: iState,jState
    INTEGER :: phase,ind
    INTEGER :: orb(2,2),spin(2,2)
    CHARACTER*100 :: token,fmt
    REAL*8 :: tVal
    INTEGER :: counter
!PAR VARS
    INTEGER :: iCPU
    INTEGER*4 :: nCPU
    INTEGER :: numThread
    REAL*8, ALLOCATABLE :: rdmP(:,:,:)

!CODE
    NAMELIST /INPUT/ detFile,&
        occupied,&
        outputFormat,&
        trd1File,&
        state1,&
        state2

    detFile=""
    trd1File=""
    outputFormat="b"
    state1=0
    state2=0

    CALL GETARG(1,inputFile)
    IF (LEN(TRIM(inputFile)).eq.0) STOP "no input file was specified"
    WRITE(6,'(A,x,A)') TRIM(inputFile),"was specified as input"
    INQUIRE(FILE=TRIM(inputFile),EXIST=exists)
    IF (.NOT.exists) STOP "input file does not exist"
    OPEN(1,FILE=inputFile,STATUS="old",IOSTAT=stat)
    IF (stat.ne.0) STOP "failure to open input file"
    READ(1,NML=INPUT,IOSTAT=stat)
    IF (stat.ne.0) STOP "input file could not be read. Erroneous input?"
    WRITE(UNIT=6,NML=INPUT)
    CLOSE(1)      

    IF (state1.le.0) THEN
        STOP "ERROR need to specify at least one state"
    ENDIF

    CALL timer%initialize()

    CALL det%create(TRIM(detFile),state1)
    nRAS=det%getNumNRAS()
    nState=det%getNumState()
    nDet=det%getNumDet()
    ALLOCATE(detConf(nRas,2,nDet))
    ALLOCATE(detCoef(2,nDet))
    CALL det%getConf(detConf)
    CALL det%getCoef(detCoef(1,:))
    IF (state2.ge.1.AND.state2.ne.state1) THEN
        CALL det%create(TRIM(detFile),state2)
        CALL det%getCoef(detCoef(2,:))
    ELSE
        detCoef(2,:)=detCoef(1,:)
    ENDIF

    inact=occupied-nRAS
    WRITE(6,'(I0,A)') occupied," Inactive + Active orbitals"
    WRITE(6,'(I0,A)') inact," inactive orbitals"
    WRITE(6,'(I0,A)') nRAS," active space orbitals"

    WRITE(6,'(A)') "Analyse determinants:"
    nUsedDet1=0
    nUsedDet2=0
    DO iDet=1,nDet
        IF (ABS(detCoef(1,iDet)).gt.1e-14) THEN
            nUsedDet1=nUsedDet1+1
        ENDIF
        IF (ABS(detCoef(2,iDet)).gt.1e-14) THEN
            nUsedDet2=nUsedDet2+1
        ENDIF
    ENDDO
    WRITE(6,'(I0,A,I0,A)') nUsedDet1, " of ",nDet," determinants need to be processed in state1"
    WRITE(6,'(I0,A,I0,A)') nUsedDet2, " of ",nDet," determinants need to be processed in state2"
    IF (nUsedDet1*nUsedDet2.eq.0) THEN
        WRITE(6,'(A)') "Nothing to be done here"
        nVal=0
    ELSE
        ALLOCATE(usedDetConf1(nRas,2,nUsedDet1))
        ALLOCATE(usedDetCoef1(nUsedDet1))
        ALLOCATE(usedDetConf2(nRas,2,nUsedDet2))
        ALLOCATE(usedDetCoef2(nUsedDet2))
        jDet=0
        kDet=0
        DO iDet=1,nDet
            IF (ABS(detCoef(1,iDet)).gt.1e-14) THEN
                jDet=jDet+1
                usedDetConf1(:,:,jDet)=detConf(:,:,iDet)
                usedDetCoef1(jDet)=detCoef(1,iDet)
            ENDIF
            IF (ABS(detCoef(2,iDet)).gt.1e-14) THEN
                kDet=kDet+1
                usedDetConf2(:,:,kDet)=detConf(:,:,iDet)
                usedDetCoef2(kDet)=detCoef(2,iDet)
            ENDIF
        ENDDO

        DEALLOCATE(detConf)
        DEALLOCATE(detCoef)

        WRITE(6,'(A)') "Order determinants using key generated by upspins"
        ALLOCATE(rasKey(nUsedDet2))
        ALLOCATE(nRasKeyDet(nUsedDet2))
        ALLOCATE(order(nUsedDet2))
        DO iDet=1,nUsedDet2
            key=0
            jDet=0
            DO iOrb=1,nRas
                DO iSpin=1,1
                    IF (usedDetConf2(iOrb,iSpin,iDet)) THEN
                        key=key+2**(jDet)
                    ENDIF
                    jDet=jDet+1
                ENDDO
            ENDDO
            rasKey(iDet)=key
            order(iDet)=iDet
        ENDDO
        CALL QSortInt(nUsedDet2,rasKey,order) 
        ALLOCATE(tempConf(nRas,2,nUsedDet2))
        ALLOCATE(tempCoef(nUsedDet2))
        tempConf=usedDetConf2
        tempCoef=usedDetCoef2
        DO iDet=1,nUsedDet2
            usedDetConf2(:,:,iDet)=tempConf(:,:,order(iDet))
            usedDetCoef2(iDet)=tempCoef(order(iDet))
        ENDDO

        jDet=1 
        nRasKey=1
        nRasKeyDet=0
        IF (nUsedDet2.eq.1) THEN
            nRasKeyDet(1)=1
        ENDIF
        DO iDet=1,nUsedDet2-1
            IF (rasKey(iDet).eq.rasKey(iDet+1)) THEN
                jDet=jDet+1   
                IF (iDet.eq.nUsedDet2-1) THEN
                    nRasKeyDet(nRasKey)=jDet 
                ENDIF
            ELSE
                nRasKeyDet(nRasKey)=jDet
                jDet=1
                nRasKey=nRasKey+1
                IF (iDet.eq.nUsedDet2-1) THEN
                    nRasKeyDet(nRasKey)=1
                ENDIF
            ENDIF
        ENDDO

        WRITE(6,'(A,I0,A,F12.6,A)') "number of keys ",nRasKey," appearing on avg. ",&
          REAL(SUM(nRasKeyDet))/REAL(nRasKey)," times"

        nCPU=OMP_get_max_threads()
        ALLOCATE(nDiff2Det(nUsedDet1))
        nDiff2Det=0
        WRITE(6,'(A,I0)') "Number of CPUs: ",nCPU
        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP SHARED(usedDetConf1,nUsedDet1,usedDetConf2,nUsedDet2,nCPU) &
        !$OMP SHARED (nDiff2Det,nRasKeyDet,nRasKey,nRas) &
        !$OMP PRIVATE (iDet,jDet,diff,diff2,iKey,kDet)
        !$OMP DO 
        DO iDet=1,nUsedDet1
            kDet=1
            DO iKey=1,nRasKey 
                diff2=COUNT(usedDetConf1(:,1,iDet).neqv.usedDetConf2(:,1,kDet))/2
                IF (diff2.gt.1) THEN
                    kDet=kDet+nRasKeyDet(iKey)
                ELSE
                    DO jDet=kDet,kDet+nRasKeyDet(iKey)-1
                        diff=diff2+COUNT(usedDetConf1(:,2,iDet).neqv.usedDetConf2(:,2,jDet))/2
                        IF (diff.le.1) THEN
                            nDiff2Det(iDet)=nDiff2Det(iDet)+1
                        ENDIF
                    ENDDO
                    kDet=kDet+nRasKeyDet(iKey)
                ENDIF
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL
        CALL timer%time("number of tuples of determinants calculated with diff < 2")

        maxNDiff2Det=MAXVAL(nDiff2Det)
        WRITE(6,'(A,I0)') "maximum number of diff combs necessary ",maxNDiff2Det
        WRITE(6,'(A,F12.6,A)') "space needed for difference metric ",REAL(9*maxNDiff2Det*nUsedDet1)/REAL(1024.**2)," MB"
        ALLOCATE(diff2DetMat(maxNDiff2Det,nUsedDet1))
        ALLOCATE(diff2Mat(maxNDiff2Det,nUsedDet1))
        !$OMP PARALLEL &
        !$OMP DEFAULT(none) &
        !$OMP SHARED(nDiff2Det,usedDetConf1,nUsedDet1,usedDetConf2,nUsedDet2,nCPU) &
        !$OMP SHARED(diff2DetMat,diff2Mat,nRasKeyDet,nRasKey,nRas) &
        !$OMP PRIVATE (iDet,jDet,kDet,lDet,diff,diff2,iKey)
        !$OMP DO 
        DO iDet=1,nUsedDet1
            lDet=0
            kDet=1
            DO iKey=1,nRasKey
                diff2=COUNT(usedDetConf1(:,1,iDet).neqv.usedDetConf2(:,1,kDet))/2
                IF (diff2.gt.1) THEN
                    kDet=kDet+nRasKeyDet(iKey)
                ELSE
                    DO jDet=kDet,kDet+nRasKeyDet(iKey)-1
                        diff=diff2+COUNT(usedDetConf1(:,2,iDet).neqv.usedDetConf2(:,2,jDet))/2
                        IF (diff.le.1) THEN
                            lDet=lDet+1
                            diff2DetMat(lDet,iDet)=jDet
                            diff2Mat(lDet,iDet)=diff 
                        ENDIF
                    ENDDO
                    kDet=kDet+nRasKeyDet(iKey)
                ENDIF
            ENDDO
        ENDDO
        !$OMP END DO
        !$OMP END PARALLEL

        CALL timer%time("tuples of determinants calculated with diff < 2")

        CALL timer%time("Start calculating RDM")
        ALLOCATE(rdm(occupied,occupied))
        ALLOCATE(rdmP(nCPU,occupied,occupied))
        rdmP=0.d0
        rdm=0.d0
        tVal=0.d0



        !$OMP PARALLEL &
        !$OMP DEFAULT (none) &
        !$OMP SHARED (inact,nRAS,nState,nUsedDet1,usedDetConf1,usedDetConf2,usedDetCoef1,usedDetCoef2,rdmP) &
        !$OMP SHARED (nDiff2Det,diff2DetMat,diff2Mat) &
        !$OMP PRIVATE (numThread,jDet,kDet,iOrb,iSpin,diff,orb,spin,phase,tVal) 
        !$OMP DO
        DO iDet=1,nUsedDet1
            numThread=OMP_get_thread_num()+1
            DO kDet=1,nDiff2Det(iDet)
                jDet=diff2DetMat(kDet,iDet)
                diff=diff2Mat(kDet,iDet)
                tVal=usedDetCoef1(iDet)*usedDetCoef2(jDet)
                !WRITE(6,'(3(E18.12,x),2(I,x))') tVal,usedDetCoef1(iDet),usedDetCoef2(jDet),iDet,jDet
                !WRITE(6,'(2(<nRAS>(L1),x))') usedDetConf1(:,1,iDet),usedDetConf2(:,1,jDet)
                !WRITE(6,'(2(<nRAS>(L1),x))') usedDetConf1(:,2,iDet),usedDetConf2(:,2,jDet)
                IF(diff.eq.0) THEN
                    DO iOrb=1,inact
                        rdmP(numThread,iOrb,iOrb)=rdmP(numThread,iOrb,iOrb)+tVal*2
                    ENDDO
                    DO iRAS=1,nRAS
                        DO iSpin=1,2
                            IF (usedDetConf1(iRAS,iSpin,iDet)) THEN
                                rdmP(numThread,iRAS+inact,iRAS+inact)=rdmP(numThread,iRAS+inact,iRAS+inact)+tVal
                            ENDIF
                        ENDDO
                    ENDDO
                ELSEIF(diff.eq.1) THEN
                    CALL locateDeviantSpinOrbitals(usedDetConf1(:,:,iDet),usedDetConf2(:,:,jDet),nRAS,diff,orb,spin,phase) 
                    IF (spin(1,1).eq.spin(2,1)) THEN
                        rdmP(numThread,orb(1,1)+inact,orb(2,1)+inact)=rdmP(numThread,orb(1,1)+inact,orb(2,1)+inact)+tVal*phase
                    ENDIF
                ENDIF
            ENDDO
        ENDDO
        !$OMP END DO NOWAIT
        !$OMP END PARALLEL
        CALL timer%time("Done calculating RDM")

        DO iCPU=1,nCPU
            rdm=rdm+rdmP(iCPU,:,:)
        ENDDO
        nVal=COUNT(ABS(rdm).ge.1e-14)
    ENDIF
    
    WRITE(6,'(A,I0)') "Number of non zero Values in rdm for states in question= ",nVal
    IF (outputFormat(1:1).eq."f") THEN
        OPEN(1,FILE=TRIM(trd1File),STATUS="replace")
        WRITE(1,'(5(x,I0))') occupied,nState,state1,state2,nVal
        IF (nVal.gt.0) THEN
            DO iOrb=1,occupied
                DO jOrb=1,occupied
                    IF (ABS(rdm(iOrb,jOrb)).ge.1e-14) THEN
                        WRITE(1,'(2(x,I5),x,E18.12)') iOrb,jOrb,rdm(iOrb,jOrb)
                    ENDIF
                ENDDO
            ENDDO
        ENDIF
    ELSE
        OPEN(1,FILE=TRIM(trd1File),STATUS="replace",FORM="unformatted")
        WRITE(1) occupied,nState,state1,state2,nVal
        IF (nVal.gt.0) THEN
            DO iOrb=1,occupied
                DO jOrb=1,occupied
                    IF (ABS(rdm(iOrb,jOrb)).ge.1e-14) THEN
                        WRITE(1) iOrb,jOrb,rdm(iOrb,jOrb)
                    ENDIF
                ENDDO
            ENDDO
        ENDIF
    ENDIF
    CLOSE(1)

    WRITE(6,'(A)') "Happy"
END PROGRAM

