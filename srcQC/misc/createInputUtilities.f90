!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE createInputUtilities
    USE class_inputString
    CONTAINS
      
    SUBROUTINE  readInput(  fich,&
          inputGatewayFile,&
          inputRas1,&
          inputRas2,&
          inputRas3,&
          inputInactive,&
          inputSymmetryStatesParent,&
          inputNumberStatesParent,&
          inputSymmetryStatesSource,&
          inputNumberStatesSource,&
          inputCreateSourceFromParent,&
          inputSourceFiles,&
          inputOrbitalFileParent,&
          inputProject,&
          inputMonocentricBasisFile,&
          inputNHoleRas1,&
          inputNElecRas2,&
          inputNElecRas3,&
          inputLinearDependenceThr,&
          inputMultiplicityParent,&
          inputMultiplicityAugmented,&
          inputMaxMultipoleOrder,&
          inputLMonocentric,&
          inputKMonocentric,&
          rdmMethod,&
          multiGhost,&
          typeOrbital,&
          forceC1,&
          Rmin,&
          Rmax,&
          BSplineNodes,&
          CCF,&
          lkMonocentricFile)

        CHARACTER*100 :: gatewayFile
        CHARACTER*100 :: ras1
        CHARACTER*100 :: ras2
        CHARACTER*100 :: ras3
        CHARACTER*100 :: inactive
        CHARACTER*100 :: symmetryStatesParent
        CHARACTER*100 :: numberStatesParent
        CHARACTER*100 :: symmetryStatesSource
        CHARACTER*100 :: numberStatesSource
        CHARACTER*100 :: createSourceFromParent
        CHARACTER*100 :: sourceFiles
        CHARACTER*100 :: orbitalFileParent
        CHARACTER*100 :: project
        CHARACTER*100 :: monocentricBasis
        CHARACTER*100 :: spinAugmentedElectron
        CHARACTER*100 :: inputSpinAugmentedElectron
        CHARACTER*100 :: multiplicityParent
        CHARACTER*100 :: typeOrbital
        CHARACTER*100 :: CCF
        CHARACTER*100 :: lkMonocentricFile
        CHARACTER*100,INTENT(out) :: rdmMethod
        LOGICAL, INTENT(out) :: multiGhost,forceC1
        
        !INTEGER :: multiplicityParent
        INTEGER :: multiplicityAugmented
        INTEGER :: lMonocentric
        INTEGER :: kMonocentric
        INTEGER :: nHoleRas1
        INTEGER :: nElecRas2
        INTEGER :: nElecRas3
        INTEGER :: BSplineNodes
        INTEGER :: maxMultipoleOrder
        INTEGER :: iL, iK
        REAL*8 :: linearDependenceThr
        REAL*8 :: Rmin,Rmax
        
        INTEGER, INTENT(out) :: inputLMonocentric
        INTEGER, INTENT(out) :: inputNHoleRas1
        INTEGER, INTENT(out) :: inputNElecRas2
        INTEGER, INTENT(out) :: inputNElecRas3
        INTEGER, INTENT(out) :: inputMaxMultipoleOrder
        CHARACTER*100,INTENT(out) :: inputGatewayFile
        CHARACTER*100,INTENT(out) :: inputOrbitalFileParent
        CHARACTER*100,INTENT(out) :: inputProject
        CHARACTER*100,INTENT(out) :: inputMonocentricBasisFile
        REAL*8 :: inputLinearDependenceThr
        !INTEGER :: inputMultiplicityParent
        INTEGER :: inputMultiplicityAugmented
        TYPE(inputString),INTENT(out) :: inputMultiplicityParent
        TYPE(inputString),INTENT(out) :: inputRas1
        TYPE(inputString),INTENT(out) :: inputRas2
        TYPE(inputString),INTENT(out) :: inputRas3
        TYPE(inputString),INTENT(out) :: inputInactive
        TYPE(inputString),INTENT(out) :: inputSymmetryStatesParent
        TYPE(inputString),INTENT(out) :: inputNumberStatesParent
        TYPE(inputString),INTENT(out) :: inputSymmetryStatesSource
        TYPE(inputString),INTENT(out) :: inputNumberStatesSource
        TYPE(inputString),INTENT(out) :: inputCreateSourceFromParent
        TYPE(inputString),INTENT(out) :: inputSourceFiles
        CHARACTER*100,INTENT(in) :: fich
        CHARACTER*200 :: tempC      
        CHARACTER*100, ALLOCATABLE :: arrC1(:)
        INTEGER, ALLOCATABLE :: intArr(:)
        INTEGER :: length1,length2,i,stat
        LOGICAL :: exists
        NAMELIST /input/        project,&
          gatewayFile,&
          ras1,&
          ras2,&
          ras3,&
          inactive,&
          symmetryStatesParent,&
          numberStatesParent,&
          symmetryStatesSource,&
          numberStatesSource,&
          sourceFiles,&
          createSourceFromParent,&
          lMonocentric,&
          kMonocentric,&
          monocentricBasis,&
          nHoleRas1,&
          nElecRas2,&
          nElecRas3,&
          linearDependenceThr,&
          multiplicityParent,&
          multiplicityAugmented,&
          orbitalFileParent,&
          maxMultipoleOrder,&
          spinAugmentedElectron,&
          rdmMethod,&
          multiGhost,&
          typeOrbital,&
          forceC1,&
          Rmin,&
          Rmax,&
          BSplineNodes,&
          CCF,&
          lkMonocentricFile


        Rmin=-1.d0
        Rmax=-1.d0
        BSplineNodes=-1
        CCF=""
        rdmMethod="trd"
        multiGhost=.TRUE.
        forceC1=.TRUE.
        project                 =" "
        gatewayFile             =" "
        ras1                    =" "
        ras2                    =" "
        ras3                    =" "
        inactive                =" "
        symmetryStatesParent    =" "
        numberStatesParent      =" "
        symmetryStatesSource    =" "
        numberStatesSource      =" "
        sourceFiles             =" "
        createSourceFromParent  =" "
        lMonocentric            =-1
        kMonocentric            =-1
        monocentricBasis        =""
        nHoleRas1               =0
        nElecRas2               =-1
        nElecRas3               =0
        linearDependenceThr     =-1.0
        !multiplicityParent      =-1
        multiplicityParent      =" "
        multiplicityAugmented   =-1
        orbitalFileParent       =" "
        maxMultipoleOrder       =-1
        spinAugmentedElectron   =" "
        typeOrbital             ="MOLPRO"
        lkMonocentricFile       =" "
        
        OPEN(1,file=fich,status="old")
        READ(1,NML=input)
        !READ(1,NML=input,IOSTAT=stat)
        !IF (stat.ne.0) THEN
        !    WRITE(6,'(A)') "ERROR: input file could not be processed. Unknown keyword?"
        !    STOP
        !ENDIF
        !OPEN(1,file=fich,status="old")
        !READ(1,NML=input)
        
        CLOSE(1)
        IF (LEN(TRIM(project)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: project specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(gatewayFile)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: gatewayFile specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(ras2)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: ras2 specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(inactive)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: inactive specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(symmetryStatesParent)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: symmetryStatesParent specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(numberStatesParent)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: numberStatesParent specifier missing"
            STOP
        ENDIF
        IF (LEN(TRIM(monocentricBasis)).eq.0) THEN
            WRITE(6,'(A)') "No monocentric Basis specified. Default ghost basis will be used"
            multiGhost=.TRUE.
        ENDIF
        IF (LEN(TRIM(spinAugmentedElectron)).eq.0) THEN
            WRITE(6,'(A)') "ERROR: spinAugmentedElectron specifier missing"
            STOP
        ENDIF
        !IF ((TRIM(spinAugmentedElectron).ne."alpha").AND.(TRIM(spinAugmentedElectron).ne."beta")) THEN
        !    WRITE(6,'(A,x,A)') "ERROR: unknwon keyword for spinAugmentedElectron",TRIM(spinAugmentedElectron)
        !    STOP
        !ENDIF
        IF (LEN(TRIM(orbitalFileParent)).eq.0) THEN
            WRITE(6,'(A)') "No parent orbital file provided. Will be generated."
            orbitalFileParent=" "
        ENDIF
       
        IF (len(trim(lkMonocentricFile)).eq.0) THEN
            IF (lMonocentric .lt. 0) THEN
                WRITE(6,'(A)') "No or non-sensical value for lMonocentric. Set to default=0"
                lMonocentric=0
            ENDIF
            IF (kMonocentric .lt. 0) THEN
                    WRITE(6,'(A)') "No or non-sensical value for lMonocentric. Set to default=0"
                    kMonocentric=0
            ENDIF
        ELSE
            IF (lMonocentric .ge. 0) THEN
                WRITE(6,'(A)') "lkMonocentricFile specified. Value for lMonocentric will be ignored"
            ENDIF
            IF (kMonocentric .ge. 0) THEN
                WRITE(6,'(A)') "lkMonocentricFile specified. Value for kMonocentric will be ignored"
            ENDIF
        ENDIF

        IF (nElecRas2 .lt. 0) THEN
            WRITE(6,'(A)') "ERROR: no or non-sensical value for nElecRas2."
            STOP
        ENDIF
        IF (linearDependenceThr .lt. 0.0) THEN
            WRITE(6,'(A)') "No or non-sensical value for linearDependenceThr. Set to default=5e-4"
            linearDependenceThr=5.0e-4
        ENDIF
        !IF (multiplicityParent .lt. 0) THEN
        !    WRITE(6,'(A)') "ERROR: no or non-sensical value for multiplicityParent."
        !    STOP
        !ENDIF
        IF (multiplicityAugmented .lt. 0) THEN
            WRITE(6,'(A)') "ERROR: no or non-sensical value for multiplicityAugmented."
            STOP
        ENDIF
        IF (maxMultipoleOrder .lt. 0) THEN
            WRITE(6,'(A)') "no or non-sensical value for maxMultipoleOrder. Set to default=6"
            maxMultipoleOrder=6
        ENDIF
        
        inputLMonocentric=lMonocentric
        inputKMonocentric=kMonocentric
        inputNHoleRas1=nHoleRas1
        inputNElecRas2=nElecRas2
        inputNElecRas3=nElecRas3
        inputGatewayFile=gatewayFile
        inputOrbitalFileParent=orbitalFileParent
        inputMonocentricBasisFile=monocentricBasis
        inputProject=project
        inputLinearDependenceThr=linearDependenceThr
        !inputMultiplicityParent=multiplicityParent
        inputMultiplicityAugmented=multiplicityAugmented
        inputMaxMultipoleOrder=maxMultipoleOrder
        
        INQUIRE(file=gatewayFile,exist=exists)
        IF (.NOT.exists) THEN
            STOP "gateway file does not exist"
        ENDIF        
        
        IF (LEN(TRIM(monocentricBasis)).gt.0) THEN
            IF (multiGhost) THEN
                DO iL=0,lMonocentric
                    DO iK=0,kMonocentric
                        WRITE(tempC,'(A,I1,A,I1,A,A)') "l.",iL,"_k.",iK,".",TRIM(monocentricBasis)
                        INQUIRE(file=TRIM(tempC),exist=exists)
                        IF (.NOT.exists) THEN
                            PRINT*, "monocentric Basis File file does not exist: ",tempC
                            STOP "monocentric Basis File file does not exist"
                        ENDIF
                    ENDDO
                ENDDO
            ELSE
                INQUIRE(file=TRIM(monocentricBasis),exist=exists)
                IF (.NOT.exists) THEN
                    PRINT*, "monocentric Basis File file does not exist: ",TRIM(monocentricBasis)
                    STOP "monocentric Basis File file does not exist"
                ENDIF
            ENDIF        
        ENDIF

        CALL inputRas1%setString(ras1)
        CALL inputRas2%setString(ras2)
        CALL inputRas3%setString(ras3)
        CALL inputInactive%setString(inactive)
        CALL inputSymmetryStatesParent%setString(symmetryStatesParent)
        CALL inputNumberStatesParent%setString(numberStatesParent)
        CALL inputSymmetryStatesSource%setString(symmetryStatesSource)
        CALL inputNumberStatesSource%setString(numberStatesSource)
        CALL inputCreateSourceFromParent%setString(createSourceFromParent)
        CALL inputSourceFiles%setString(sourceFiles)
        CALL inputMultiplicityParent%setString(multiplicityParent) 
       
        CALL inputRas1%process("-")
        CALL inputRas2%process("-")
        CALL inputRas3%process("-")
        CALL inputInactive%process("-")
        CALL inputSymmetryStatesParent%process("-")
        CALL inputNumberStatesParent%process("-")
        CALL inputSymmetryStatesSource%process("-")
        CALL inputNumberStatesSource%process("-")
        CALL inputCreateSourceFromParent%process("-")
        CALL inputSourceFiles%process("-")
        CALL inputMultiplicityParent%process("-")

        CALL inputRas2%sizeString(length1)
        CALL inputInactive%sizeString(length2)
        IF (length1.ne.length2) THEN
            WRITE(6,'(A)') "ERROR: ras2 and inactive need to have same number of fields"
            STOP
        ENDIF
        ALLOCATE(intArr(length1))
        intArr=0
        CALL inputRas1%sizeString(length1)
        IF (length1.eq.0) THEN
            CALL inputRas1%setArray(intArr)  
        ELSEIF (length1.ne.length2) THEN
            WRITE(6,'(A)') "ERROR: ras1 and inactive need to have same number of fields"
            STOP
        ENDIF
        CALL inputRas3%sizeString(length1)
        IF (length1.eq.0) THEN
            CALL inputRas3%setArray(intArr)  
        ELSEIF (length1.ne.length2) THEN
            WRITE(6,'(A)') "ERROR: ras3 and inactive need to have same number of fields"
            STOP
        ENDIF

        CALL inputSymmetryStatesParent%sizeString(length1)
        CALL inputNumberStatesParent%sizeString(length2)
        IF (length1.ne.length2) THEN
            WRITE(6,'(A)') "ERROR: symmetryStatesParent and numberStatesParent need to have same number of fields"
            STOP
        ENDIF 
        !CALL inputSourceFiles%sizeString(length1)
        !CALL inputNumberStatesSource%sizeString(length2)
        !IF (length1.ne.length2) THEN
        !        WRITE(6,'(A)') "ERROR: sourceFiles and numberStatesSource need to have same number of fields"
        !        STOP
        !ENDIF 
        CALL inputSourceFiles%sizeString(length1)
        CALL inputCreateSourceFromParent%sizeString(length2)
        IF (length1.ne.length2.and.length2.ne.0) THEN
                WRITE(6,'(A)') "ERROR: sourceFiles and createSourceFromParent need to have same number of fields"
                STOP
        ENDIF 
        CALL inputCreateSourceFromParent%getCharArray(arrC1)
        length1=0
        DO i=1,size(arrC1)
            IF (TRIM(arrC1(i)).eq."T") THEN
                    length1=length1+1
            ENDIF
        ENDDO
        CALL inputSymmetryStatesSource%sizeString(length2)
        IF (length1.ne.length2.and.length1.ne.0) THEN
            print*, length1,length2
            WRITE(6,'(A)') "ERROR: numberStatesSource specifier needs to have the same &
              number of fields as fields of createSourceFromParent with value T"
            STOP
        ENDIF 
        WRITE(UNIT=6,NML=input)
    END SUBROUTINE
    
    SUBROUTINE writeRasscf(un,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2,nElecRas3,&
      symmetryStates,numberStates,sym,spin,lumorb,augElectron,ciOnly,fast,one,scaff)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,sym,spin
        LOGICAL, INTENT(in) :: lumorb,ciOnly,fast,augElectron,scaff
        INTEGER, INTENT(in) :: nHoleRas1
        INTEGER, INTENT(in) :: nElecRas2
        INTEGER, INTENT(in) :: nElecRas3
        TYPE(inputString), INTENT(in) :: ras1
        TYPE(inputString), INTENT(in) :: ras2
        TYPE(inputString), INTENT(in) :: ras3
        TYPE(inputString), INTENT(in) :: inactive
        TYPE(inputString), INTENT(in) :: symmetryStates
        TYPE(inputString), INTENT(in) :: numberStates
        LOGICAL :: one
        INTEGER :: i,nsym
        INTEGER, ALLOCATABLE :: arrI(:)
        CHARACTER*100, ALLOCATABLE :: arrC(:)
        CHARACTER*100 :: token,fmt
        CHARACTER*3 :: sorte
        CALL inactive%sizeString(nsym)
        
        WRITE(un,'(A)') "&RASSCF"
        IF (lumorb) THEN 
            WRITE(un,'(A)') "lumo"
        ENDIF
        IF (fast) THEN
            WRITE(un,'(A)') "tight"
            WRITE(un,'(I4,x,I4)') 1000,1000
            WRITE(un,'(A)') "cimx"
            WRITE(un,'(I1)') 1
        ELSE
            WRITE(un,'(A)') "thrs"
            WRITE(un,'(A)') "1e-22 1e-4 1e-4"
        ENDIF
        IF (ciOnly) THEN
            WRITE(un,'(A)') "cion"
        ENDIF
        WRITE(un,'(A)') "spin"
        WRITE(un,'(I1)') spin
        
        IF (sym.gt.0) THEN
            WRITE(un,'(A)') "symm"
            
            CALL symmetryStates%getSorte(sorte)
            IF (sorte.eq."str") THEN                                
                    CALL symmetryStates%getCharArray(arrC)
                    WRITE(un,'(A,A)') "##",TRIM(arrC(sym))
            ELSE
                    CALL symmetryStates%getIntArray(arrI)
                    WRITE(un,'(I2)') arrI(sym)
            ENDIF
            WRITE(un,'(A)') "inac"
            CALL inactive%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "froz"
            WRITE(fmt,'(A,I0,A)') "(",nsym,"(I1,x))"
            WRITE(un,TRIM(fmt)) (0,i=1,nsym)
            WRITE(un,'(A)') "ras1"
            CALL ras1%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "ras2"
            CALL ras2%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "ras3"
            CALL ras3%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "nact"
            WRITE(un,'(I2,x,I1,x,I1)') nElecRas2,nHoleRas1,nElecRas3
            WRITE(un,'(A)') "ciro"
            CALL numberStates%getIntArray(arrI)
            WRITE(un,'(I3,x,I3,x,I1)') arrI(sym),arrI(sym),1
        ELSE
            WRITE(un,'(A)') "inac"
            CALL inactive%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "froz"
            WRITE(un,'(I1)') 0
            WRITE(un,'(A)') "ras1"
            CALL ras1%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "ras2"
            CALL ras2%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "ras3"
            CALL ras3%sumString(i)
            IF (augElectron) THEN
                WRITE(un,'(I2)') i+2
            ELSE
                WRITE(un,'(I2)') i
            ENDIF
            WRITE(un,'(A)') "nact"
            IF (augElectron) THEN
                WRITE(un,'(I2,x,I1,x,I1)') nElecRas2+1,nHoleRas1,nElecRas3+1
            ELSE
                WRITE(un,'(I2,x,I1,x,I1)') nElecRas2,nHoleRas1,nElecRas3 
            ENDIF
            WRITE(un,'(A)') "ciro"
            IF (one) THEN
                WRITE(un,'(I3,x,I3,x,I1)') 1,1,1
            ELSE
                CALL numberStates%sumString(i)
                WRITE(un,'(I3,x,I3,x,I1)') i,i,1
            ENDIF
        ENDIF
        IF (scaff) THEN
            WRITE(un,'(A)') "prsd"
            WRITE(un,'(A)') "prwf"
            WRITE(un,'(A)') "0.0"
        ENDIF
        !WRITE(un,'(A)') "prwf"
        !WRITE(un,'(A)') "0.0"
    END SUBROUTINE  

    SUBROUTINE writeRasscf2(un,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas2,nElecRas3,&
      symmetryStates,numberStates,sym,spin,lumorb,augElectron,ciOnly,fast,one,scaff)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,sym!,spin
        LOGICAL, INTENT(in) :: lumorb,ciOnly,fast,augElectron,scaff
        INTEGER, INTENT(in) :: nHoleRas1
        INTEGER, INTENT(in) :: nElecRas2
        INTEGER, INTENT(in) :: nElecRas3
        TYPE(inputString), INTENT(in) :: ras1
        TYPE(inputString), INTENT(in) :: ras2
        TYPE(inputString), INTENT(in) :: ras3
        TYPE(inputString), INTENT(in) :: inactive
        TYPE(inputString), INTENT(in) :: symmetryStates
        TYPE(inputString), INTENT(in) :: numberStates
        TYPE(inputString), INTENT(in) :: spin
        LOGICAL :: one
        INTEGER :: i,nsym
        INTEGER, ALLOCATABLE :: arrI(:)
        CHARACTER*100, ALLOCATABLE :: arrC(:)
        CHARACTER*100 :: token,fmt
        CHARACTER*3 :: sorte
        CALL inactive%sizeString(nsym)
        
        WRITE(un,'(A)') "&RASSCF"
        IF (lumorb) THEN 
            WRITE(un,'(A)') "lumo"
        ENDIF
        IF (fast) THEN
            WRITE(un,'(A)') "tight"
            WRITE(un,'(I4,x,I4)') 1000,1000
            WRITE(un,'(A)') "cimx"
            WRITE(un,'(I1)') 1
        ELSE
            WRITE(un,'(A)') "thrs"
            WRITE(un,'(A)') "1e-22 1e-4 1e-4"
        ENDIF
        IF (ciOnly) THEN
            WRITE(un,'(A)') "cion"
        ENDIF
        WRITE(un,'(A)') "spin"
        CALL spin%getIntArray(arrI)
        IF (sym.ne.0) THEN
            WRITE(un,'(I1)') arrI(sym)
        ELSE
            WRITE(un,'(I1)') arrI(1)
        ENDIF
        
        IF (sym.gt.0) THEN
            WRITE(un,'(A)') "symm"
            
            CALL symmetryStates%getSorte(sorte)
            IF (sorte.eq."str") THEN                                
                    CALL symmetryStates%getCharArray(arrC)
                    WRITE(un,'(A,A)') "##",TRIM(arrC(sym))
            ELSE
                    CALL symmetryStates%getIntArray(arrI)
                    WRITE(un,'(I2)') arrI(sym)
            ENDIF
            WRITE(un,'(A)') "inac"
            CALL inactive%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "froz"
            WRITE(fmt,'(A,I0,A)') "(",nsym,"(I1,x))"
            WRITE(un,TRIM(fmt)) (0,i=1,nsym)
            WRITE(un,'(A)') "ras1"
            CALL ras1%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "ras2"
            CALL ras2%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "ras3"
            CALL ras3%writeString(token)
            WRITE(un,'(A)') token
            WRITE(un,'(A)') "nact"
            WRITE(un,'(I2,x,I1,x,I1)') nElecRas2,nHoleRas1,nElecRas3
            WRITE(un,'(A)') "ciro"
            CALL numberStates%getIntArray(arrI)
            WRITE(un,'(I3,x,I3,x,I1)') arrI(sym),arrI(sym),1
        ELSE
            WRITE(un,'(A)') "inac"
            CALL inactive%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "froz"
            WRITE(un,'(I1)') 0
            WRITE(un,'(A)') "ras1"
            CALL ras1%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "ras2"
            CALL ras2%sumString(i)
            WRITE(un,'(I2)') i
            WRITE(un,'(A)') "ras3"
            CALL ras3%sumString(i)
            IF (augElectron) THEN
                WRITE(un,'(I2)') i+2
            ELSE
                WRITE(un,'(I2)') i
            ENDIF
            WRITE(un,'(A)') "nact"
            IF (augElectron) THEN
                WRITE(un,'(I2,x,I1,x,I1)') nElecRas2+1,nHoleRas1,nElecRas3+1
            ELSE
                WRITE(un,'(I2,x,I1,x,I1)') nElecRas2,nHoleRas1,nElecRas3 
            ENDIF
            WRITE(un,'(A)') "ciro"
            IF (one) THEN
                WRITE(un,'(I3,x,I3,x,I1)') 1,1,1
            ELSE
                CALL numberStates%sumString(i)
                WRITE(un,'(I3,x,I3,x,I1)') i,i,1
            ENDIF
        ENDIF
        
        IF (scaff) THEN
            WRITE(un,'(A)') "prsd"
            WRITE(un,'(A)') "prwf"
            WRITE(un,'(A)') "0.0"
        ENDIF
        !WRITE(un,'(A)') "prwf"
        !WRITE(un,'(A)') "0.0"
    END SUBROUTINE  
    
    !SUBROUTINE writeRHF(un)
    !    IMPLICIT none
    !    INTEGER, INTENT(in) :: un
    !    WRITE(un,'(A)') "{rhf"
    !    WRITE(un,'(A)') "}"
    !END SUBROUTINE

    !SUBROUTINE writeUHF(un)
    !    IMPLICIT none
    !    INTEGER, INTENT(in) :: un
    !    WRITE(un,'(A)') "{uhf"
    !    WRITE(un,'(A)') "}"
    !END SUBROUTINE

    SUBROUTINE writeMulti(un,casOccupied,inactive,ras1,ras2,ras3,nHoleRas1,nElecRas3,&
      symmetryStates,numberStates,e,inputOrb,multiplicity)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,e,nHoleRas1,nElecRas3
        TYPE(inputString), INTENT(in) :: casOccupied
        TYPE(inputString), INTENT(in) :: inactive
        TYPE(inputString), INTENT(in) :: ras1
        TYPE(inputString), INTENT(in) :: ras2
        TYPE(inputString), INTENT(in) :: ras3
        TYPE(inputString), INTENT(in) :: symmetryStates
        TYPE(inputString), INTENT(in) :: numberStates
        TYPE(inputString), INTENT(in) :: multiplicity
        CHARACTER(len=*) :: inputOrb
        INTEGER, ALLOCATABLE :: states(:)
        CHARACTER*100, ALLOCATABLE :: symC(:)
        INTEGER, ALLOCATABLE :: symI(:)
        INTEGER, ALLOCATABLE :: spinI(:)
        INTEGER, ALLOCATABLE :: inacArr(:)
        INTEGER, ALLOCATABLE :: ras1Arr(:)
        INTEGER, ALLOCATABLE :: ras2Arr(:)
        INTEGER, ALLOCATABLE :: ras3Arr(:)
        INTEGER :: i,iOrb,iSym
        INTEGER :: nSym
        INTEGER :: nInac
        INTEGER :: nRas1
        INTEGER :: nRas2
        INTEGER :: nRas3
    
        CHARACTER*200 :: fmt
        CHARACTER*100 :: token
        CHARACTER*3 :: sorte
        LOGICAL :: readOrbFromFile
      
        CALL inactive%sumString(nInac)
        CALL ras1%sumString(nRas1)
        CALL ras2%sumString(nRas2)
        CALL ras3%sumString(nRas3)
        CALL inactive%getArray(inacArr)
        CALL ras1%getArray(ras1Arr)
        CALL ras2%getArray(ras2Arr)
        CALL ras3%getArray(ras3Arr)
        INQUIRE(file=inputOrb,exist=readOrbFromFile)
        
        CALL symmetryStates%sizeString(nSym)
        IF(.NOT.readOrbFromFile) THEN
            WRITE(un,'(A)') "{hf}"
        ENDIF
        WRITE(un,'(A)') "{multi"
        IF (readOrbFromFile) THEN
            !WRITE(un,'(A)') "start 2140.3"
            WRITE(un,'(A)') "dont orbital" 
        ENDIF
        WRITE(un,'(A)') "orbital 2140.2"
        CALL casOccupied%writeString(token)
        WRITE(un,'(A,A)') "occ",token
        CALL inactive%writeString(token)
        WRITE(un,'(A,A)') "closed",token
        CALL symmetryStates%getSorte(sorte)
        CALL multiplicity%getIntArray(spinI)
        IF (sorte.eq."str") THEN
            CALL symmetryStates%getCharArray(symC)
            CALL numberStates%getIntArray(states)
            WRITE(fmt,'(A,I0,A)') "(A,I0,x,I0,x,",nRas1,"(I0,A,I0,x))"
            WRITE(un,TRIM(fmt)) "restrict ",nRas1*2-nHoleRas1,nRas1*2,&
              ((iOrb+nInac,".",iSym,iOrb=1,ras1Arr(iSym)),iSym=1,SIZE(ras2Arr))
            WRITE(fmt,'(A,I0,A)') "(A,I0,x,I0,x,",nRas3,"(I0,A,I0,x))"
            WRITE(un,TRIM(fmt)) "restrict ",0,nElecRas3,&
              ((iOrb+nInac+nRas1+nRas2,".",iSym,iOrb=1,ras3Arr(iSym)),iSym=1,SIZE(ras2Arr))
            DO i=1,nSym
                WRITE(un,'(A,x,I2,x,A,A,x,I1)')  "wf",e,"##",TRIM(symC(i)),spinI(i)-1
                WRITE(un,'(A,x,I2)') "state",states(i)
            ENDDO
        ELSE
            CALL symmetryStates%getIntArray(symI)
            CALL numberStates%getIntArray(states)
            WRITE(fmt,'(A,I0,A)') "(A,I0,x,I0,x,",nRas1,"(I0,A,I0,x))"
            WRITE(un,TRIM(fmt)) "restrict ",nRas1*2-nHoleRas1,nRas1*2,&
              ((iOrb+inacArr(iSym),".",iSym,iOrb=1,ras1Arr(iSym)),iSym=1,SIZE(ras2Arr))
            WRITE(fmt,'(A,I0,A)') "(A,I0,x,I0,x,",nRas3,"(I0,A,I0,x))"
            WRITE(un,TRIM(fmt)) "restrict ",0,nElecRas3,&
              ((iOrb+inacArr(iSym)+ras1Arr(iSym)+ras2Arr(iSym),".",iSym,iOrb=1,ras3Arr(iSym)),iSym=1,SIZE(ras2Arr))
            DO i=1,nSym
                WRITE(un,'(A,x,I2,x,I2,x,I1)') "wf",e,symI(i),spinI(i)-1
                WRITE(un,'(A,x,I2)') "state",states(i)
            ENDDO
        ENDIF
        WRITE(un,'(A)') "}"
    END SUBROUTINE  
    
    SUBROUTINE writeMatrop(un)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)') "{matrop"
        WRITE(un,'(A)') "load S"
        WRITE(un,'(A)') "load CurrOrb orb 2140.2"
        WRITE(un,'(A)') "write CurrOrb orb.pro new"
        WRITE(un,'(A)') "tran S_mo S CurrOrb"
        WRITE(un,'(A)') "write S_mo ov.orb.pro new"
        WRITE(un,'(A)')  "}"
        WRITE(un,'(A)')  "put,molden,pro.molden"
    END SUBROUTINE 
    
    SUBROUTINE writeRassi(un,prop,vel)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un,prop
        INTEGER :: iCom,nCom
        LOGICAL, INTENT(in), optional :: vel
        WRITE(un,'(A)') "&RASSI"
        IF (prop.gt.0) THEN
            nCom=(prop+1)*(prop+2)/2
            WRITE(un,'(A)') "onel"
            WRITE(un,'(A)') "mein"
            WRITE(un,'(A)') "prop"
            WRITE(un,'(I2)') nCom
            DO iCom=1,nCom
                IF (.not.present(vel)) THEN
                    WRITE(un,'(A,x,I2,A,x,I2)') "'Mltpl",prop,"'",iCom
                ELSE
                    IF (vel) THEN
                        WRITE(un,'(A,x,I2,A,x,I2)') "'Velocity",prop,"'",iCom
                    ELSE
                        WRITE(un,'(A,x,I2,A,x,I2)') "'Mltpl",prop,"'",iCom
                    ENDIF
                ENDIF
            ENDDO
        ELSE
            WRITE(un,'(A)') "cipr"
            WRITE(un,'(A)') "orbi"
        ENDIF
    END SUBROUTINE
    
    SUBROUTINE writeSeward(un,oneEl,maxMultipoleOrder)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un,maxMultipoleOrder
        CHARACTER(len=*) , INTENT(in) :: oneEl
        WRITE(un,'(A)') "&SEWARD"
        IF (oneEl.eq."oneEl") THEN
            WRITE(un,'(A)') "mult"
            WRITE(un,'(I1)') maxMultipoleOrder         
            WRITE(un,'(A)') "oneo"
        !ELSE
        !    WRITE(un,'(A)') "sdip"         
        ENDIF
    END SUBROUTINE
    
    SUBROUTINE writeCaspt2(un)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)')  "&CASPT2"
        WRITE(un,'(A)')  "PRWF"
        WRITE(un,'(A)')  "0.0"
        WRITE(un,'(A)')  "imag"
        WRITE(un,'(A)')  "0.2"
        WRITE(un,'(A)')  "multi"
        WRITE(un,'(A)')  "1 1"
    END SUBROUTINE 
    SUBROUTINE writeDesy(un)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)')  "&EXPBAS"
        WRITE(un,'(A)')  "noex"
        WRITE(un,'(A)')  "desy"
    END SUBROUTINE 
    
    SUBROUTINE writeLinDepKill(un,lMonocentric,kMonocentric,&
      nLocOrb,linearDependenceThr,multiGhost,lkMonocentricFile)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        INTEGER, INTENT(in) :: lMonocentric
        INTEGER, INTENT(in) :: kMonocentric
        INTEGER, INTENT(in) :: nLocOrb
        LOGICAL, INTENT(in) :: multiGhost
        REAL*8, INTENT(in) :: linearDependenceThr
        CHARACTER*100, INTENT(in) :: lkMonocentricFile
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "ovFile=ov.int"
        WRITE(un,'(A)') "locOrbFile=loc.ion.bin"
        WRITE(un,'(A,E18.12)') "linDepThr=",linearDependenceThr
        WRITE(un,'(A,I0)') "nOrbLoc=",nLocOrb
        WRITE(un,'(A)') "linIndepOrbFile=ion.homemade"
        WRITE(un,'(A)') "blockInfoFile=blockBasis"
        WRITE(un,'(A,I0)') "lMax=",lMonocentric
        WRITE(un,'(A,I0)') "kMax=",kMonocentric
        WRITE(un,'(A,L)') "multiGhost=",multiGhost
        WRITE(un,'(A)') "orbSymFile=orbs.sym"
        IF (LEN(TRIM(lkMonocentricFile)).gt.0) THEN
            WRITE(un,'(A)') "lkMonocentricFile=lk.dat"
        ENDIF
        WRITE(un,'(A)') "&END"
    END SUBROUTINE 
    SUBROUTINE writeCreateZeroOrbByBlock(un,lMonocentric,kMonocentric,&
      nLocOrb,linearDependenceThr1,linearDependenceThr2)
        IMPLICIT none 
        INTEGER, INTENT(in) :: un,lMonocentric,kMonocentric,nLocOrb
        REAL*8, INTENT(in) :: linearDependenceThr1,linearDependenceThr2
     
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "ovFile=ov.int"
        WRITE(un,'(A)') "locOrbFile=loc.ion.bin"
        WRITE(un,'(A,E12.5)') "epsilon1=",linearDependenceThr1
        WRITE(un,'(A,E12.5)') "epsilon2=",linearDependenceThr2
        WRITE(un,'(A,I0)') "lMax=",lMonocentric
        WRITE(un,'(A,I0)') "kMax=",kMonocentric
        WRITE(un,'(A,I0)') "nOrb0=",nLocOrb
        WRITE(un,'(A)') "blockInfoFile=blockBasis"
        WRITE(un,'(A)') "monocentricOrbFile=augmented"
        WRITE(un,'(A)') "monopolyOrbFile=ion.homemade"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE 
    SUBROUTINE writeOrthoNormalize(un)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=symOrb.bin"
        WRITE(un,'(A)') "ovFile=ov.loc.int"
        WRITE(un,'(A)') "linIndepOrbitalFile=symOrb.GS"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE 
    SUBROUTINE writeSeveral(un,nLocOrb,sourceFiles)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,nLocOrb
        TYPE(inputString), INTENT(in) :: sourceFiles
        CHARACTER*100, ALLOCATABLE :: arrC(:)
        INTEGER :: i
        
        CALL sourceFiles%getCharArray(arrC)
        WRITE(un,'(A)') "basoverlap.bin"
        WRITE(un,'(A)') "ion.homemade.bin"
        WRITE(un,'(I1)') SIZE(arrC)
        WRITE(un,'(A,x,I5)') "##nLocBas",nLocOrb
        DO i=1,SIZE(arrC)
            WRITE(un,'(A,A)') TRIM(arrC(i)),".bin"
            WRITE(un,'(A,A)') TRIM(arrC(i)),".homemade.Orb"
            WRITE(un,'(A,A)') TRIM(arrC(i)),".homemade.bin"
        ENDDO
    END SUBROUTINE    
    
    SUBROUTINE writeCreateLauncher(un,numberStatesSource,numberStatesParent,ras1,ras2,ras3,inactive,project)
        IMPLICIT none
        INTEGER :: i
        INTEGER, INTENT(in) :: un
        INTEGER :: nSource,nParent,nLocOcc,firstRotate,nInac
        INTEGER :: nRas1,nRas2,nRas3
        CHARACTER*100 :: fmt
        CHARACTER(len=*), INTENT(in) :: project
        TYPE(inputString), INTENT(in) :: numberStatesSource,numberStatesParent,ras1,ras2,ras3,inactive
        CALL numberStatesSource%sumString(nSource)
        CALL numberStatesParent%sumString(nParent)
        CALL ras1%sumString(nRas1)
        CALL ras2%sumString(nRas2)
        CALL ras3%sumString(nRas3)
        nLocOcc=nRas1+nRas2+nRas3
        CALL inactive%sumString(nInac)
        firstRotate=nLocOcc+1+nInac
        WRITE(un,'(A,A)')  "#!/bin/bash"
        WRITE(un,'(A,A)')  "totorb=","##nLinIndep"
        IF (firstRotate.lt.10) THEN
            WRITE(un,'(A,I1)')  "initorb=",firstRotate
        ELSEIF (firstRotate.lt.100) THEN
            WRITE(un,'(A,I2)')  "initorb=",firstRotate
        ELSE
            WRITE(un,'(A,I3)')  "initorb=",firstRotate
        ENDIF
        WRITE(un,'(A,A)')  "totbas=","##nMonPoBasis"
        
        WRITE(fmt,'(A,I0,A)') "(A,",nParent,"(I3,x),A)"
        WRITE(un,TRIM(fmt)) 'laststatesLocAug="',(i,i=nSource+nParent*nLocOcc+1,nSource+nParent*(nLocOcc+1)),'"'
        WRITE(fmt,'(A,I0,A)') "(A,",2*nParent,"(I3,x),A)"
        WRITE(un,TRIM(fmt)) 'laststatesAugAug="',(i,i=nSource+nParent*nLocOcc+1,nSource+nParent*(nLocOcc+2)),'"'
        
        WRITE(un,'(A,A,A)')  'template="',TRIM(project),'_LocAug"'
        WRITE(un,'(A)') 'for ((i=$totorb; i>=$initorb ; i-- )); do'
        WRITE(un,'(A)') 'sed "s/actorb/$i/" template | sed "s/totorb/$totorb/" | sed "s/##sqr/0/" |\'
        WRITE(un,'(A)') 'sed "s/initorb/$initorb/" | sed "s/maxcpus/17/" |\'
        WRITE(un,'(A)') 'sed "s/totbas/$totbas/" | sed "s/@laststates/$laststatesLocAug/" > ${template}${i}.sh'
        !WRITE(un,'(A)') 'echo "sbatch $molcasOutput/${template}${i}.sh" >> temp_run.sh'
        WRITE(un,'(A)') 'echo "sbatch $orbTempDir/${template}${i}.sh" >> temp_run.sh'
        WRITE(un,'(A)') 'echo "sleep 0.1" >> temp_run.sh'
        WRITE(un,'(A)') 'done'
        WRITE(un,'(A)') 'tac temp_run.sh > temp_runA.sh' 
        WRITE(un,'(A)') 'rm temp_run.sh'
        
        WRITE(un,'(A,A,A)')  'template="',TRIM(project),'_AugAug"'
        WRITE(un,'(A)') 'for ((i=$totorb-1; i>=$initorb ; i-- )); do'
        WRITE(un,'(A)') 'sed "s/actorb/$i/" template | sed "s/totorb/$totorb/" | sed "s/##sqr/1/" |\'
        WRITE(un,'(A)') 'sed "s/initorb/$initorb/" | sed "s/maxcpus/17/" |\'
        WRITE(un,'(A)') 'sed "s/totbas/$totbas/" | sed "s/@laststates/$laststatesAugAug/" > ${template}${i}.sh'
        !WRITE(un,'(A)') 'echo "sbatch $molcasOutput/${template}${i}.sh" >> temp_run.sh'
        WRITE(un,'(A)') 'echo "sbatch $orbTempDir/${template}${i}.sh" >> temp_run.sh'
        WRITE(un,'(A)') 'echo "sleep 0.1" >> temp_run.sh'
        WRITE(un,'(A)') 'done'
        WRITE(un,'(A)') 'tac temp_run.sh > temp_runB.sh' 
        WRITE(un,'(A)') 'rm temp_run.sh'
        !WRITE(un,'(A,A,A)') 'cat temp_runA.sh temp_runB.sh > LAUNCH_',TRIM(project),'.sh' 
        WRITE(un,'(A)') 'cat temp_runA.sh temp_runB.sh > orbSubmit2.sh' 
        WRITE(un,'(A)') 'rm temp_runA.sh temp_runB.sh'
    END SUBROUTINE
    
    SUBROUTINE writeMix(un,numberStatesSource,numberStatesParent,ras1,ras2,ras3,casOccupied)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        INTEGER :: nParent,nSource,nRas1,nRas2,nRas3,nOcc
        TYPE(inputString), INTENT(in) :: numberStatesSource,numberStatesParent,ras1,ras2,ras3,casOccupied
        CALL numberStatesSource%sumString(nSource)
        CALL numberStatesParent%sumString(nParent)
        CALL ras1%sumString(nRas1)
        CALL ras2%sumString(nRas2)
        CALL ras3%sumString(nRas3)
        CALL casOccupied%sumString(nOcc)
        WRITE(un,'(A)') "##output"
        WRITE(un,'(A)') "##props"
        WRITE(un,'(I3)') nSource
        WRITE(un,'(I3)') nParent
        WRITE(un,'(I3)') nRas1+nRas2+nRas3
        WRITE(un,'(I3,x,A)') nOcc+1,"##nLinIndep"
        WRITE(un,'(A)') "free"
        WRITE(un,'(A)') "orbital 2"
        WRITE(un,'(A)') "yes"
        WRITE(un,'(I3)') 8
        WRITE(un,'(A)') "ham1E .TRUE."
        WRITE(un,'(A)') "dip1 .FALSE."
        WRITE(un,'(A)') "dip2 .FALSE."
        WRITE(un,'(A)') "dip3 .FALSE."
        WRITE(un,'(A)') "vel1 .FALSE."
        WRITE(un,'(A)') "vel2 .FALSE."
        WRITE(un,'(A)') "vel3 .FALSE."
        WRITE(un,'(A)') "kin .FALSE."
    END SUBROUTINE
    
    SUBROUTINE writeGrid_It(un,nOrb)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,nOrb
        INTEGER :: iOrb
        WRITE(un,'(A)') "&GRID_IT"
        WRITE(un,'(A)') "GAP"
        WRITE(un,'(A)') "6.0"
        WRITE(un,'(A)') "ASCII"
        WRITE(un,'(A)') "NPOI"
        WRITE(un,'(A)') "32 32 40"
        WRITE(un,'(A)') "ORBI"
        WRITE(un,'(I4)') nOrb
        DO iOrb=1,nOrb
            WRITE(un,'(I1,x,I4)') 1,iOrb 
        ENDDO
    END SUBROUTINE
    
    SUBROUTINE writeGABS_Interface(un,multiplicitySource,inactive,ras1,ras2,ras3,kMax,lMax,multiGhost)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,multiplicitySource
        INTEGER :: nLocOrb,nRas1,nRas2,nRas3,nInactive
        INTEGER :: lMax,kMax
        LOGICAL, INTENT(in) :: multiGhost
        INTEGER, ALLOCATABLE :: arrI1(:)
        TYPE(inputString), INTENT(in) :: ras1,ras2,ras3,inactive
        CALL ras1%sumString(nRas1)
        CALL ras2%sumString(nRas2)
        CALL ras3%sumString(nRas3)
        CALL inactive%sumString(nInactive)
        nLocOrb=nRas1+nRas2+nRas3+nInactive
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A,I0)') "kMax=",kMax
        WRITE(un,'(A,I0)') "lMax=",lMax
        WRITE(un,'(A)') "basisFile=bas"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A,I0)') "blockInfoFile=blockBasis"
        WRITE(un,'(A,I0)') "multiplicity=",multiplicitySource
        WRITE(un,'(A,x,I3)') "nLocOrb=",nLocOrb 
        WRITE(un,'(A,x,I3)') "nInactive=",nInactive
        WRITE(un,'(A)') "nLocBas=##nLocBas"
        WRITE(un,'(A)') "nOrb=##nOrb"
        WRITE(un,'(A,L)') "multiGhost=",multiGhost
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeGABS_Interface_sym(un,multiplicitySource,inactive,ras1,ras2,ras3,kMax,lMax,multiGhost,forceC1,lkMonocentricFile)
        IMPLICIT none
        INTEGER, INTENT(in) :: un,multiplicitySource
        INTEGER :: nLocOrb,nRas1,nRas2,nRas3,nInactive
        INTEGER :: lMax,kMax
        LOGICAL, INTENT(in) :: multiGhost,forceC1
        CHARACTER*100, intent(in) :: lkMonocentricFile
        INTEGER, ALLOCATABLE :: arrI1(:)
        TYPE(inputString), INTENT(in) :: ras1,ras2,ras3,inactive
        CALL ras1%sumString(nRas1)
        CALL ras2%sumString(nRas2)
        CALL ras3%sumString(nRas3)
        CALL inactive%sumString(nInactive)
        nLocOrb=nRas1+nRas2+nRas3+nInactive
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A,I0)') "kMax=",kMax
        WRITE(un,'(A,I0)') "lMax=",lMax
        WRITE(un,'(A)') "basisFile=bas"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A,I0)') "blockInfoFile=blockBasis"
        WRITE(un,'(A,I0)') "multiplicity=",multiplicitySource
        WRITE(un,'(A,x,I3)') "nLocOrb=",nLocOrb 
        WRITE(un,'(A,x,I3)') "nInactive=",nInactive
        WRITE(un,'(A)') "nLocBas=##nLocBas"
        WRITE(un,'(A,L)') "multiGhost=",multiGhost
        WRITE(un,'(A,L)') "forceC1=",forceC1
        WRITE(un,'(A)') "orbSymFile=orbs.sym"   
        IF (LEN(TRIM(lkMonocentricFile)).gt.0) THEN
            WRITE(un,'(A)') "lkMonocentricFile=lk.dat"
        ENDIF
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    
    SUBROUTINE writeCompressTwoElectronIntegrals(un,lMax,kMax,lkMonocentricFile,dontCompress)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: lMax,kMax
        CHARACTER*100, INTENT(in) :: lkMonocentricFile
        LOGICAL, OPTIONAL, INTENT(in) :: dontCompress
        LOGICAL :: dontCompressLocal
        dontCompressLocal = merge(dontCompress,.FALSE.,present(dontCompress))
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "petiteBasisList=petiteBasis"
        WRITE(un,'(A)') "twoElectronIntegralsMolcas=ham2E.int"
        WRITE(un,'(A)') "twoElectronIntegralsCompressed=ham2E.compressed.int"
        WRITE(un,'(A)') "blockInfoFile=rawBlockBasis"
        WRITE(un,'(A,I2)') "lMax=",lMax
        WRITE(un,'(A,I2)') "kMax=",kMax
        IF (LEN(TRIM(lkMonocentricFile)).gt.0) THEN
            WRITE(un,'(A)') "lkMonocentricFile=lk.dat"
        ENDIF
        WRITE(un,'(A,L)') "dontCompress=",dontCompressLocal
        IF (dontCompressLocal) THEN
            WRITE(un,'(A,L)') "printBasInfo=T"
        ELSE
            WRITE(un,'(A,L)') "printBasInfo=F"
        ENDIF
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeOneIntBas2Orb(un)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A)') "oneElectronOperatorBasisFiles=ham1E.int-ov.int-dip1.int-dip2.int-dip3.int-&
                                                        vel1.int-vel2.int-vel3.int-kin.int"
        WRITE(un,'(A)') "oneElectronOperatorOrbitalFiles=ham1E.orb.int-ov.orb.int-&
                                                        dip1.orb.int-dip2.orb.int-dip3.orb.int-&
                                                        vel1.orb.int-vel2.orb.int-vel3.orb.int-kin.orb.int"
        WRITE(un,'(A)') "occupied=##nLinIndep"
        WRITE(un,'(A)') "hermitian=h-h-h-h-h-a-a-a-h"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTwoIntBas2Orb(un,local,nodes)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: nodes
        INTEGER,INTENT(in) :: local
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A)') "twoElectronOperatorBasisFile=ham2E.compressed.int"
        WRITE(un,'(A)') "twoElectronOperatorOrbitalFile=ham2E.orb.int"
        WRITE(un,'(A,I3)') "local=",local
        WRITE(un,'(A)') "occupied=##nLinIndep"
        WRITE(un,'(A)') "node=##node"
        WRITE(un,'(A,I0)') "nrOfNodes=",nodes
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeIjkl2ijrs(un,local)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: local
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A)') "integralFile=ham2E.compressed.int"
        WRITE(un,'(A)') "outegralFile=ijrs.int"
        WRITE(un,'(A)') "occupied=##nLinIndep"
        WRITE(un,'(A,I0)') "local=",local
        WRITE(un,'(A)') "suffixFile=ijkl.suffix"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeIjkl2iqks(un,local)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: local
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A)') "integralFile=ham2E.compressed.int"
        WRITE(un,'(A)') "outegralFile=iqks.int"
        WRITE(un,'(A)') "occupied=##nLinIndep"
        WRITE(un,'(A,I0)') "local=",local
        WRITE(un,'(A)') "suffixFile=ijkl.suffix"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeIqks2pqrs(un,local)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: local
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbitalFile=ion.homemade.bin"
        WRITE(un,'(A)') "iqksFile=iqks.int"
        WRITE(un,'(A)') "ijrsFile=ijrs.int"
        WRITE(un,'(A)') "outegralFile=pqrs.int"
        WRITE(un,'(A)') "occupied=##nLinIndep"
        WRITE(un,'(A,I0)') "local=",local
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeOneIntOrb2OperatorMatrix(un,nElectron)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        INTEGER,INTENT(in) :: nElectron
        WRITE(un,'(A)') "&INPUT"
        !WRITE(un,'(A)') "oneIntRDMFile=oneIntRDM"
        WRITE(un,'(A)') "oneIntRDMFile=trd1"
        WRITE(un,'(A)') "oneIntFiles=ham1E.orb.int-ov.orb.int-&
                                    dip1.orb.int-dip2.orb.int-dip3.orb.int-&
                                    vel1.orb.int-vel2.orb.int-vel3.orb.int-kin.orb.int"
        WRITE(un,'(A)') "outputFiles=ham1E.state.dat-ov.state.dat-&
                                    dip1.state.dat-dip2.state.dat-dip3.state.dat-&
                                    vel1.state.dat-vel2.state.dat-vel3.state.dat-kin.state.dat"
        WRITE(un,'(A)') "printMode=tri-tri-rch-rch-rch-rca-rca-rca-rch"
        WRITE(un,'(A,I2.2,A)') "scaleFactor=1-",nElectron,"-1-1-1-1-1-1-1"
        WRITE(un,'(A)') "activeOrbitals=##active"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTwoIntOrb2OperatorMatrix(un)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "twoIntRDMFile=trd2"
        !WRITE(un,'(A)') "twoIntRDMFile=twoIntRDM"
        WRITE(un,'(A)') "twoIntFile=ham2E.orb.int"
        WRITE(un,'(A)') "outputFile=ham2E.state.dat"
        WRITE(un,'(A)') "activeOrbitals=##active"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeOneIntOrb2OperatorMatrix_new(un,nInactive,nSource,nParent)
        IMPLICIT none
        INTEGER,INTENT(in) :: un,nInactive,nSource,nParent
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "oneIntRDMFile=trd1"
        WRITE(un,'(A)') "oneIntFile=op1E.orb.int"
        WRITE(un,'(A)') "outputFile=op1E.state.int"
        WRITE(un,'(A,I0)') "nInactive=",nInactive
        WRITE(un,'(A,I0)') "nSource=",nSource
        WRITE(un,'(A,I0)') "nParent=",nParent
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTwoIntOrb2OperatorMatrix_new(un,nInactive,nSource,nParent)
        IMPLICIT none
        INTEGER,INTENT(in) :: un,nInactive,nSource,nParent
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "twoIntRDMFile=trd2"
        WRITE(un,'(A)') "twoIntFile=ham2E.orb.int"
        WRITE(un,'(A)') "outputFile=ham2E.state.int"
        WRITE(un,'(A,I0)') "nInactive=",nInactive
        WRITE(un,'(A,I0)') "nSource=",nSource
        WRITE(un,'(A,I0)') "nParent=",nParent
        WRITE(un,'(A)') "&END"
    END SUBROUTINE




    SUBROUTINE writeCreateIntegral2CSFMap(un,occupied,nRas3)
        IMPLICIT none
        INTEGER,INTENT(in) :: un,nRas3
        INTEGER,INTENT(in) :: occupied

        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "gugaFile=local.source.ras3.guga"
        WRITE(un,'(A)') "oneIntCSFMapFile=oneIntCSFMap"
        WRITE(un,'(A)') "twoIntCSFMapFile=twoIntCSFMap"
        WRITE(un,'(A,I3)') "occupied=",occupied
        WRITE(un,'(A,I3)') "nOrbRas3=",nRas3
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeSumOneIntTwoIntNuclear(un)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "oneElFile=ham1E.state.dat"
        WRITE(un,'(A)') "twoElFile=ham2E.state.dat"
        WRITE(un,'(A)') "ovFile=ov.state.dat" 
        WRITE(un,'(A)') "nucRepFile=nuclearRepulsion"
        WRITE(un,'(A)') "totHamFile=totalHam.state.dat"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeOneIntRDMCreate(un)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "ciVectorFile=CI.dat"
        WRITE(un,'(A)') "oneIntCSFMapFile=oneIntCSFMap"
        WRITE(un,'(A)') "oneIntRDMFile=trd1"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTwoIntRDMCreate(un)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "ciVectorFile=CI.dat"
        WRITE(un,'(A)') "twoIntCSFMapFile=twoIntCSFMap"
        WRITE(un,'(A)') "twoIntRDMFile=trd2"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeSeward2(un,oneo,sdip)
        IMPLICIT none
        INTEGER,INTENT(in) :: un
        LOGICAL,INTENT(in) :: oneo,sdip
        WRITE(un,'(A)') "&SEWARD" 
        WRITE(un,'(A)') "noguess"
        IF (oneo) THEN
            WRITE(un,'(A)') "oneo"
        ENDIF
        !IF (sdip.ge.0) THEN
        !    WRITE(un,'(A)') "sdip"
        !ENDIF 
    END SUBROUTINE
    SUBROUTINE writeTrd1(un,occ)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        INTEGER, INTENT(in) :: occ
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "trd1File=trd1"
        WRITE(un,'(A)') "detFile=source-aug.det"
        WRITE(un,'(A)') "state1=##state1"
        WRITE(un,'(A)') "state2=##state2"
        WRITE(un,'(A,I0)') "occupied=",occ
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTrd1Build(un)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "trd1ElementFileList=trd1ElementFileList"
        WRITE(un,'(A)') "trd1File=trd1"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTrd2(un,occ)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        INTEGER, INTENT(in) :: occ
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "trd2File=trd2"
        WRITE(un,'(A)') "detFile=source-aug.det"
        WRITE(un,'(A,I0)') "occupied=",occ
        WRITE(un,'(A)') "state1=##state1"
        WRITE(un,'(A)') "state2=##state2"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE
    SUBROUTINE writeTrd2Build(un)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "trd2ElementFileList=trd2ElementFileList"
        WRITE(un,'(A)') "trd2File=trd2"
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

    SUBROUTINE writeSymSplit(un,inactive,multiplicityAugmented,&
      parentSyms,nParentStatesBySym,&
      sourceSyms,nSourceStatesBySym,&
      multiplicityParent,forceC1)
        IMPLICIT none
        INTEGER, INTENT(in) :: un
        INTEGER, INTENT(in) :: inactive,multiplicityAugmented
        LOGICAL, INTENT(in) :: forceC1
        TYPE(inputString), INTENT(in) :: parentSyms,nParentStatesBySym
        TYPE(inputString), INTENT(in) :: sourceSyms,nSourceStatesBySym
        TYPE(inputString), INTENT(in) :: multiplicityParent 
        WRITE(un,'(A)') "&INPUT"
        WRITE(un,'(A)') "orbSymFile=orbs.sym"   
        WRITE(un,'(A,A)') "parentSyms=",trim(parentSyms%getString())
        WRITE(un,'(A,A)') "nParentStatesBySym=",trim(nParentStatesBySym%getString())
        WRITE(un,'(A,A)') "sourceSyms=",trim(sourceSyms%getString()) 
        WRITE(un,'(A,A)') "nSourceStatesBySym=",trim(nSourceStatesBySym%getString())
        WRITE(un,'(A,I0)') "inactive=",inactive
        WRITE(un,'(A)') "blockInfoFile=blockBasis"   
        WRITE(un,'(A,I0)') "multiplicityAugmented=",multiplicityAugmented
        WRITE(un,'(A,A)') "multiplicityParent=",trim(multiplicityParent%getString())
        WRITE(un,'(A,L)') "forceC1=",forceC1   
        WRITE(un,'(A)') "&END"
    END SUBROUTINE

END MODULE
