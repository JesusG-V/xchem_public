MODULE sortingAlgorithms
IMPLICIT none

CONTAINS
RECURSIVE SUBROUTINE QSortInt(nA,A,O)
INTEGER, INTENT(in) :: nA
INTEGER, INTENT(inout) :: A(nA),O(nA)

INTEGER :: left,right
INTEGER :: temp
INTEGER :: marker
INTEGER :: pivot
REAL*8 :: random

IF (nA.gt.1) THEN
    CALL random_number(random)
    pivot=A(int(random*real(nA-1))+1) !random pivot
    left=0
    right=nA+1
    DO WHILE (left<right)
        right=right-1
        DO WHILE (A(right)>pivot)
            right=right-1 
        ENDDO
        left=left+1
        DO WHILE (A(left)<pivot)
            left=left+1
        ENDDO
        IF (left<right) THEN
            temp=A(left)
            A(left)=A(right)
            A(right)=temp
            temp=O(left)
            O(left)=O(right)
            O(right)=temp
        ENDIF
    ENDDO
    IF (left.eq.right) THEN 
        marker=left+1
    ELSE
        marker=left
    ENDIF
    CALL QSortInt(marker-1,A(:marker-1),O(:marker-1))
    CALL QSortInt(nA-marker+1,A(marker:),O(marker:))
ENDIF
END SUBROUTINE
END MODULE
