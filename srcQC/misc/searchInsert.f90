!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE searchInsert
!SUBROUTINES FOR:
!BINARY SEARCH: SEARCH ONE DIMENSIONAL SORTED INTEGER ARRAY FOR A GIVEN ELEMENT
!IF ELEMENT IS NOT PRESENT THE POSITION WHERE IT SHOULD BE INSERTED IS RETURNED
!ELSE THE POSITION WHERE IT WAS FOUND IS RETURNED AS A NEGATIVE VALUE
!
!INSERT: INSERTS KEY INTO SPECIFIED POSITION IN ONE DIMENSIONAL ARRAY
!
!INSTERKEYVAL : INSERTES KEY AND VALUE INTO SPECIFIED POSITION OF TWO
!DIMENSIONAL ARRAY. (FIRST VALUE KEY SECOND VALUE VALUE)
CONTAINS
    SUBROUTINE binarySearch(key,n,array,insertionPoint,minInitial)
        IMPLICIT none
        INTEGER, INTENT(in) :: key,n
        INTEGER, INTENT(in) :: array(n)
        INTEGER, INTENT(in), OPTIONAL :: minInitial
        INTEGER, INTENT(out) :: insertionPoint
        INTEGER :: iMax,iMin,iMid
        iMax=n
        iMin=1
        IF (PRESENT(minInitial)) THEN
            iMin=minInitial
        ENDIF
        DO WHILE (iMin.le.iMax)
            iMid=(iMin+iMax)/2
            IF (array(iMid).eq.key) THEN
                insertionPoint=-iMid
                RETURN
            ELSEIF (key.lt.array(iMid)) THEN
                iMax=iMid-1
            ELSE  
                iMin=iMid+1
            ENDIF
        ENDDO
        insertionPoint=iMax
    END SUBROUTINE
    
    SUBROUTINE insert(key,after,nRel,nTot,arr)
        IMPLICIT none
        INTEGER, INTENT(in) :: key,after,nRel,nTot
        INTEGER, INTENT(inout) :: arr(nTot)
        INTEGER :: i
        IF (nRel.ge.nTot) THEN
            STOP "ERROR: no space in array to insert further key"
        ENDIF
       
        arr(after+1:nRel+1)=cshift(arr(after+1:nRel+1),-1)

        !DO i=nRel,after+1,-1
        !    arr(i+1)=arr(i) 
        !ENDDO
        
        arr(after+1)=key
    END SUBROUTINE

    SUBROUTINE insertKeyValue(key,val,after,nRel,nTot,arr)
        IMPLICIT none
        INTEGER, INTENT(in) :: key,val,after,nRel,nTot
        INTEGER, INTENT(inout) :: arr(nTot,2)
        INTEGER :: i
        IF (nRel.ge.nTot) THEN
            STOP "ERROR: no space in array to insert further value"
        ENDIF
        
        DO i=nRel,after+1,-1
            arr(i+1,:)=arr(i,:) 
        ENDDO
        !DO i=nRel,after+1,-1
        !    arr(i+1,1)=arr(i,1) 
        !ENDDO
        !DO i=nRel,after+1,-1
        !    arr(i+1,2)=arr(i,2) 
        !ENDDO
        arr(after+1,1)=key
        arr(after+1,2)=val
    END SUBROUTINE
    
    SUBROUTINE key2ind(key,nBas,i,j,k,l)
        IMPLICIT none
        INTEGER,INTENT(in) :: key,nBas
        INTEGER,INTENT(out) :: i,j,k,l
        i=key/((nBas+1)**3)
        j=mod(key,(nBas+1)**3)/((nBas+1)**2)
        k=mod(key,(nBas+1)**2)/(nBas+1)
        l=mod(key,(nBas+1))
    END SUBROUTINE

    SUBROUTINE ind2key(i,j,k,l,nBas,key)
        IMPLICIT none
        INTEGER,INTENT(in) :: i,j,k,l,nBas
        INTEGER,INTENT(out) :: key
        key=i*(nbas+1)**3+j*(nbas+1)**2+k*(nbas+1)+l
    END SUBROUTINE
END MODULE
