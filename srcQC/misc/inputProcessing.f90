!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

MODULE class_inputString
!CLASS THAT TREATS A GENERAL STRING AS A LIST OF INTGERD
!OR SUBSTRINGS. A SEPARATOR CAN BE DEFINED AND THE ELEMENTS
!IN THE STRING CAN BE, DEPENDING ON DATA STORED IN STRING,
!COUNTED,SUMMED,ADDED ETC.
    IMPLICIT none
    PRIVATE
    PUBLIC :: to_lower
    PUBLIC :: to_upper
    PUBLIC :: to_upperSelective
    PUBLIC :: inputString
    PUBLIC :: OPERATOR(+)
    TYPE inputString
        PRIVATE
        CHARACTER*3 :: sorte
        CHARACTER*300 :: string=""
        !gfortran4.8 does not support deffered length string in derived types
        !CHARACTER(len=:), ALLOCATABLE :: string
        INTEGER, ALLOCATABLE :: arrayInt(:)
        CHARACTER*100, ALLOCATABLE :: arrayChar(:)
        LOGICAL :: empty
        CONTAINS
            PROCEDURE setString
            PROCEDURE getString
            PROCEDURE process
            PROCEDURE writeString
            PROCEDURE sizeString 
            PROCEDURE sumString 
            PROCEDURE getSorte
            PROCEDURE getIntArray
            PROCEDURE setIntArray      
            PROCEDURE getCharArray
            PROCEDURE setCharArray      
            GENERIC :: getArray => getIntArray,getCharArray
            GENERIC :: setArray => setIntArray,setCharArray
    END TYPE
    INTERFACE OPERATOR (+)
        MODULE PROCEDURE add
    END INTERFACE OPERATOR (+)
    CONTAINS
        SUBROUTINE setString(this,string)
            CLASS(inputString) :: this
            CHARACTER(LEN=*), INTENT(in) :: string
            !gfortran4.8 does not support defered length string in derived types
            !IF (ALLOCATED(this%string)) DEALLOCATE(this%string)
            !ALLOCATE(CHARACTER(LEN(string)) :: this%string)
            IF (LEN(TRIM(string)).gt.300) THEN
                STOP "Max string length currently 300 for inputStrings (gfort 4.8 :( )"
            ENDIF
            this%string = string
        END SUBROUTINE
    
        FUNCTION getString(this)
            IMPLICIT none
            CLASS(inputString) :: this
            CHARACTER*300 :: getString
            getString=this%string
        END FUNCTION

        SUBROUTINE process(this,inSeparator)
            CLASS(inputString) :: this
            CHARACTER*1, OPTIONAL :: inSeparator
            CHARACTER*1 :: separator 
            INTEGER :: i,j,iField,start,finish,nFields,counter,stat
            CHARACTER*300 :: token
            REAL*8 :: r
            token=" "
            IF (.NOT.(present(inSeparator))) THEN
                separator=' '
            ELSE
                separator=inSeparator
            ENDIF
            i=1
            counter=0
            nFields=0
            DO  WHILE (i.le.LEN(TRIM(this%string)))
                IF (this%string(i:i).ne.separator) THEN
                    counter=counter+1
                    token(counter:counter)=this%string(i:i)
                    nFields=nFields+1    
                    DO j=i+1,LEN(TRIM(this%string))
                        IF (this%string(j:j).ne.separator) THEN
                            counter=counter+1
                            i=i+1          
                            token(counter:counter)=this%string(i:i)
                        ELSE
                            EXIT
                        ENDIF
                    ENDDO
                ENDIF
                i=i+1
            ENDDO
           
            IF (ALLOCATED(this%arrayInt)) DEALLOCATE(this%arrayInt)
            IF (ALLOCATED(this%arrayChar)) DEALLOCATE(this%arrayChar)
            IF (nFields.eq.0) THEN
                ALLOCATE(this%arrayInt(0))
                ALLOCATE(this%arrayChar(0))
                this%empty=.TRUE.
                WRITE(6,'(A,A,A,A,A)') "String empty"
                RETURN
            ENDIF
            this%empty=.FALSE.


            !PRINT*, "debug",counter
            READ(token(1:counter),*,IOSTAT=stat) r
            IF (stat.eq.0) THEN
                IF (SCAN(token,".").eq.0) THEN
                    ALLOCATE(this%arrayInt(nFields))
                    this%sorte="int"
                ELSE
                    ALLOCATE(this%arrayChar(nFields))
                    this%sorte="str"
                ENDIF
            ELSE
                ALLOCATE(this%arrayChar(nFields))
                this%sorte="str"
            ENDIF
            
            !WRITE(6,'(A,A,A,A,A,I0,A)') "String ",TRIM(this%string)," identified as ",TRIM(this%sorte),&
            !  " string with ",nFields," fields"
            
            IF (TRIM(this%sorte).eq."str") THEN
                finish=1
                start=1
                DO iField=1,nFields
                    DO WHILE (finish.le.LEN(TRIM(this%string)))
                        IF (this%string(finish:finish).ne.separator) THEN 
                            finish=finish+1
                        ELSE 
                            this%arrayChar(iField)=this%string(start:finish-1)
                            finish=finish+1
                            start=finish
                            EXIT
                        ENDIF
                        IF (finish.eq.(LEN(TRIM(this%string))+1)) THEN
                            this%arrayChar(iField)=this%string(start:finish-1)
                        ENDIF
                    ENDDO
                ENDDO
            ELSE
                finish=1
                start=1
                DO iField=1,nFields
                    DO WHILE (finish.le.LEN(TRIM(this%string)))
                        IF (this%string(finish:finish).ne.separator) THEN 
                            finish=finish+1
                        ELSE
                            token=this%string(start:finish-1)
                            !print*,"DEBUG ",TRIM(token)
                            READ(token,*) this%arrayInt(iField) 
                            finish=finish+1
                            start=finish 
                            EXIT
                        ENDIF
                        IF (finish.eq.(LEN(TRIM(this%string))+1)) THEN
                            token=this%string(start:finish-1)
                            READ(token,*) this%arrayInt(iField) 
                        ENDIF
                    ENDDO
                ENDDO
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE getIntArray(this,arr)
            CLASS(inputString) :: this
            INTEGER, ALLOCATABLE,INTENT(inout) :: arr(:)
            IF (ALLOCATED(arr)) DEALLOCATE(arr)
            IF (this%empty) THEN
                ALLOCATE(arr(0))
                RETURN
            ENDIF
            IF (TRIM(this%sorte).eq. "str") STOP "ERROR: Trying to get Character Input string with Integer array" 
            ALLOCATE(arr(size(this%arrayInt)))
            arr=this%arrayInt
        END SUBROUTINE
        
        SUBROUTINE setIntArray(this,arr)
            CLASS(inputString) :: this
            INTEGER, INTENT(in),ALLOCATABLE :: arr(:)
            IF (.NOT.ALLOCATED(arr)) STOP "ERROR: must pass allocated array to setter"
            IF (ALLOCATED(this%arrayInt)) DEALLOCATE(this%arrayInt)
            ALLOCATE(this%arrayInt(SIZE(arr,1)))
            this%arrayInt=arr
            this%sorte="int"
            this%empty=.FALSE.
        END SUBROUTINE
        
        SUBROUTINE getCharArray(this,arr)
            CLASS(inputString) :: this
            CHARACTER(LEN=*), ALLOCATABLE,INTENT(inout) :: arr(:)
            IF (ALLOCATED(arr)) DEALLOCATE(arr)
            IF (this%empty) THEN
                ALLOCATE(arr(0))
                RETURN
            ENDIF
            IF (TRIM(this%sorte).eq. "int") STOP "ERROR: Trying to get Integer Input string with Character array" 
            ALLOCATE(arr(size(this%arrayChar)))
            arr=this%arrayChar
        END SUBROUTINE
        
        SUBROUTINE setCharArray(this,arr)
            CLASS(inputString) :: this
            CHARACTER(len=*), INTENT(in),ALLOCATABLE :: arr(:)
            IF (.NOT.ALLOCATED(arr)) STOP "ERROR: must pass allocated array to setter"
            IF (ALLOCATED(this%arrayInt)) DEALLOCATE(this%arrayInt)
            ALLOCATE(this%arrayChar(SIZE(arr,1)))
            this%arrayChar=arr
            this%sorte="str"
        END SUBROUTINE
        
        SUBROUTINE sumString(this,s)    
            CLASS(inputString) :: this
            INTEGER,INTENT(out) :: s
            IF (this%sorte.eq."char") STOP "cannot sum up character array"
            IF(size(this%arrayInt).eq.0)THEN
                s=0
            ELSE 
                s=SUM(this%arrayInt)
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE writeString(this,un)
            CLASS(inputString) :: this
            CHARACTER(LEN=*), intent(out) :: un                                                                               
            CHARACTER*100, F
            INTEGER :: i
            
            IF (this%sorte.eq."char") THEN
                WRITE(F,'(A1,I1,A)') "(",size(this%arrayChar),"(x,A))"  
                WRITE(un,F) (TRIM(this%arrayChar(i)),i=1,Size(this%arrayChar))
            ELSE
                WRITE(F,'(A1,I1,A)') "(",size(this%arrayInt),"(x,I2))"  
                WRITE(un,F) (this%arrayInt(i),i=1,Size(this%arrayInt))                             
            ENDIF
        END SUBROUTINE
        
        SUBROUTINE sizeString(this,s)
            CLASS(inputString) :: this
            INTEGER,INTENT(out) :: s
            IF (this%sorte.eq."str") THEN
                s=SIZE(this%arrayChar)
            ELSE
                s=SIZE(this%arrayInt)
            ENDIF 
        END SUBROUTINE
        
        SUBROUTINE getSorte(this,ch)
            CLASS(inputString) :: this
            CHARACTER(LEN=*),INTENT(out) :: ch
            ch=this%sorte
        END SUBROUTINE
        
        FUNCTION add(a,b) RESULT(c)
            TYPE(inputString),INTENT(in) :: a,b      
            TYPE(inputString) :: c
            INTEGER, ALLOCATABLE :: ta(:),tb(:),tc(:)
            CALL a%getIntArray(ta)
            CALL b%getIntArray(tb)
            ALLOCATE(tc(SIZE(ta)))
            tc=ta+tb
            CALL c%setIntArray(tc)
        END FUNCTION

        SUBROUTINE to_lower(s)
            INTEGER :: i
            CHARACTER(LEN=*), INTENT(inout) :: s
            DO i=1,len(s)
                SELECT CASE(s(i:i))
                    CASE("A":"Z")
                        s(i:i)=ACHAR(IACHAR(s(i:i))+32)
                END SELECT
            ENDDO
        END SUBROUTINE

        SUBROUTINE to_upper(s)
            INTEGER :: i
            CHARACTER(LEN=*), INTENT(inout) :: s
            DO i=1,len(s)
                SELECT CASE(s(i:i))
                    CASE("a":"z")
                        s(i:i)=ACHAR(IACHAR(s(i:i))-32)
                END SELECT
            ENDDO
        END SUBROUTINE
        SUBROUTINE to_upperSelective(s,subSet)
            INTEGER :: i
            CHARACTER(LEN=*), INTENT(inout) :: s
            CHARACTER(LEN=*), INTENT(in) :: subSet
            DO i=1,len(s) 
                IF (SCAN(s(i:i),subSet).ne.0) THEN
                    s(i:i)=ACHAR(IACHAR(s(i:i))-32)
                ENDIF
            ENDDO
        END SUBROUTINE

END MODULE
