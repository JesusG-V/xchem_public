!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

module matmat
    contains
    subroutine dmatmat(A,B,transA,transB,C,alpha,beta)
        implicit none
        real*8, intent(in), allocatable :: A(:,:), B(:,:)
        real*8, intent(inout), allocatable :: C(:,:)
        character*1, intent(in) :: transA,transB
        integer :: k, m, n, lda, ldb, ldc 
        real*8, intent(in), optional :: alpha, beta
        real*8 :: alphaLoc, betaLoc
        
        alphaLoc = merge(alpha, 1.d0, present(alpha))
        betaLoc = merge(beta, 0.d0, present(beta))

        if (transA .eq. "n" .or. transA .eq. "N") then
            m = size(A,1)
            k = size(A,2)
        elseif (transA .eq. "t" .or. transA .eq. "T") then
            m = size(A,2)
            k = size(A,1)
        endif
        if (transB .eq. "n" .or. transB .eq. "N") then
            n = size(B,2)
        elseif (transB .eq. "t" .or. transB .eq. "T") then
            n = size(B,1)
        endif
        lda = size(A,1)
        ldb = size(B,1)
        ldc = size(C,1)

        call dgemm( transa,    & !! 1
                    transb,    & !! 2
                    m,         & !! 3
                    n,         & !! 4
                    k,         & !! 5
                    alphaLoc,  & !! 6
                    A,         & !! 7
                    lda,       & !! 8
                    B,         & !! 9
                    ldb,       & !! 10
                    betaLoc,   & !! 11
                    C,         & !! 12
                    ldc)         !! 13
    end subroutine
end module
