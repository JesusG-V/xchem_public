!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM pro2cas
!CONDE THAT TRANSFORMS A MOLPRO ORBITAL INTO A
!MOLCAS ORBITAL FILE.
    USE orbitalIO
    IMPLICIT none
    TYPE(orbital_) :: orbital
    INTEGER :: nSym,iSym
    INTEGER :: nOrb,iOrb,jOrb,kOrb,nFound,last
    INTEGER, ALLOCATABLE :: symSize(:)
    INTEGER :: i,j
    INTEGER :: counter
    REAL*8 :: renorm
    CHARACTER*10, ALLOCATABLE :: orbitalPro(:)
    CHARACTER*10, ALLOCATABLE :: orbitalCas(:)
    REAL*8, ALLOCATABLE :: orbitals(:,:)
    REAL*8, ALLOCATABLE :: orbitalsNew(:,:)
    INTEGER, ALLOCATABLE :: atomPro(:)
    INTEGER, ALLOCATABLE :: atomCas(:)
    INTEGER, ALLOCATABLE :: reorder(:)
    CHARACTER*200 :: fich,ch1,outFile,fmt
    LOGICAL, ALLOCATABLE :: free(:)
    LOGICAL :: found
    CHARACTER*200 :: inputFile        
    !This code transferes molpro with symmetry to molcas orbtials
    !to molcas orbitals with symmtry. Written by Markus Klinker
    
    renorm=1.d0
    
    CALL GETARG(1,inputFile)
    OPEN(10,file=TRIM(inputFile)) 
    
    WRITE(6,'(A)') "MOLPRO Input Orbital File"
    READ(10,*) fich
    OPEN(1,file=fich,STATUS="old")
    READ(1,*); READ(1,*)
    WRITE(6,'(A)') TRIM(fich)        
    WRITE(6,'(A)') "MOLCAS Output Orbital File:" 
    READ(10,*) outFile
    WRITE(6,'(A)') TRIM(outFile)        
    
    WRITE(6,'(A)') "Number of Symmetries:"
    READ(10,*) nSym
    WRITE(6,'(I2)') nSym
    
    ALLOCATE(symSize(nSym))
    
    DO iSym=1,nSym
            READ(10,*) symSize(iSym)
            counter=CEILING(REAL(symSize(iSym))/REAL(10))
            IF (symSize(iSym).gt.0) THEN
                    DO iOrb=1,symSize(iSym)+counter
                            READ(10,*)
                    ENDDO
            ENDIF
    ENDDO
    WRITE(fmt,'(A,I0,A)') "(A,",nSym,"(x,I4))"
    WRITE(6,TRIM(fmt)) "with sizes:",(symSize(iSym),iSym=1,nSym)
    
    REWIND(10)
    READ(10,*)
    READ(10,*)
    READ(10,*)
    
    CALL orbital%initializeEmpty(symSize)       
    
    counter=0
    DO iSym=1,nSym
            WRITE(6,'(A,x,I4)') "Processing Symmetry:",iSym
            WRITE(6,'(A,x,I4,x,A)') "Number of Orbitals in Symmetry",iSym,":"
            READ(10,*) nOrb
            IF (nOrb.eq.0) THEN     
                    CYCLE
            ENDIF
            WRITE(6,'(I4,x,A)') nOrb,"Orbitals"
            ALLOCATE(orbitalPro(nOrb))
            ALLOCATE(orbitalCas(nOrb))
            ALLOCATE(atomPro(nOrb))
            ALLOCATE(atomCas(nOrb))
            ALLOCATE(orbitals(nOrb,nOrb))
            ALLOCATE(orbitalsNew(nOrb,nOrb))
            ALLOCATE(free(nOrb))
            ALLOCATE(reorder(nOrb))
            orbitalsNew=0.d0
            free=.TRUE.
            WRITE(6,'(A)') "Reading order of obitals according to MOLPRO"
            READ(10,*) (atomPro(iOrb),orbitalPro(iOrb),iOrb=1,nOrb)
            WRITE(6,'(A)') "Reading order of obitals according to MOLCAS"
!                READ(5,*) (atomCas(iOrb),orbitalCas(iOrb),iOrb=1,nOrb)

!!! Change by jgv Reading directly from MOLCAS seward
        last=0
        i=0
        do iOrb=1,nOrb
            READ(10,*) jOrb,ch1,orbitalCas(iOrb),j
            IF (iOrb.eq.1) THEN
                i=j
                last=i
            ELSE
                IF (j.ne.last) THEN
                    last=j
                    i=i+1
                ENDIF
            ENDIF
            atomCas(iOrb)=i
        enddo
!!!!    !!!!!!!!
        DO iOrb=1,nOrb
            READ(1,*) (orbitals(jOrb,iOrb),jOrb=1,nOrb)
        ENDDO
        WRITE(6,'(A)') "Reorganize orbitals to match MOLCAS format"
        found=.FALSE.
        nFound=0
        DO iOrb=1,nOrb
            DO jOrb=1,nOrb
                IF ((orbitalCas(iOrb).eq.orbitalPro(jOrb)).AND.&
                  (atomCas(iOrb).eq.atomPro(jOrb)).AND.&
                  free(jOrb)) THEN
                    nFound=nFound+1
                    free(jOrb)=.FALSE.
                    reorder(iOrb)=jOrb
                    found=.TRUE.
                    EXIT
                ENDIF
            ENDDO
            IF (.NOT.found) THEN
                STOP "A MOLPRO ORBITAL COULD NOT BE MATCHED TO A MOLCAS ONE. CHECK INPUT"
            ENDIF   
        ENDDO
        IF (nFound.ne.nOrb) THEN
            WRITE(6,'(A,x,I3,x,A,x,I3)') "INFO number of Orbitals:", nOrb ,"in Sym",iSym
            WRITE(6,'(A,x,I3,x,A,x,I3)') "INFO number of Orbitals found:", nFound,"in Sym",iSym
            WRITE(6,'(A)') "ERROR: Mismatch between number of read and number of printed orbitals.& 
              Different order of atoms in wavefunction file and gateway file?"
            STOP "MISMATCH BETWEEN NUMBER OF READ AND NUMBER OF PRINTED ORBITALS.&
              DIFFERENT ORDER OF ATOMS IN WAVEFUNCTION AND MONOPOLY INPUT?"
        ENDIF
        

        DO iOrb=1,nOrb
            DO kOrb=1,nOrb
                orbitalsNew(kOrb,iOrb)=orbitals(iOrb,reorder(kOrb))/sqrt(renorm) 
            ENDDO
        ENDDO

        CALL orbital%setSym(iSym,orbitalsNew)
        !DO iOrb=1,nOrb
        !        counter=counter+1
        !        WRITE(2,'(A,x,I4,x,I4,x,I4)') "* ORBITAL",iSym,iOrb,counter
        !        WRITE(2,'(4(E18.12e2))') (orbitals(iOrb,reorder(kOrb))/sqrt(renorm),kOrb=1,nOrb)
        !ENDDO
                
        DEALLOCATE(orbitalPro)
        DEALLOCATE(orbitalCas)
        DEALLOCATE(atomPro)
        DEALLOCATE(atomCas)
        DEALLOCATE(orbitals)
        DEALLOCATE(orbitalsNew)
        DEALLOCATE(free)
        DEALLOCATE(reorder)
        ENDDO
        READ(1,*) ch1
        IF (ch1.ne."END_DATA") THEN
            WRITE(6,'(A)') "MOLPRO ORBITAL FILE DID NOT TERMINATE AS EXPECTED"
            STOP "MOLPRO ORBITAL FILE DID NOT TERMINATE AS EXPECTED"
        ENDIF
        CALL orbital%writeOrbital(TRIM(outFile)//".bin","b")
        CALL orbital%writeOrbital(TRIM(outFile)//".RasOrb","f")
        CLOSE(1)
        CLOSE(2)
        CLOSE(10)
        PRINT*, "Happy"
END PROGRAM
