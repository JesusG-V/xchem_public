!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM autoAugment
!CODE THAT CREATES ALL THE INPUT FILES NECESSARY
!TO AUTOMATICALLY RUN MOMOPOLY SCRIPTS
    USE class_Gateway
    USE class_inputString
    USE createInputUtilities
    IMPLICIT none
    INTEGER :: i,nSym,iSym,electronsParent,nRas3,nOrb
    INTEGER :: lMonocentric,kMonocentric,totalLMonocentric
    INTEGER :: maxMultipoleOrder
    !INTEGER :: multiplicityParent
    INTEGER :: multiplicityAugmented
    INTEGER :: occupiedNoSym
    INTEGER :: nHoleRas1
    INTEGER :: nElecRas2
    INTEGER :: nElecRas3
    INTEGER :: BSplineNodes
    INTEGER :: inactiveNoSym
    INTEGER :: nParent
    INTEGER :: nSource
    LOGICAL :: multiGhost,forceC1
    INTEGER, ALLOCATABLE :: arrI1(:)
    INTEGER, ALLOCATABLE :: arrI2(:)
    CHARACTER*100, ALLOCATABLE :: arrC1(:)
    CHARACTER*100, ALLOCATABLE :: arrC2(:)
    CHARACTER*100, ALLOCATABLE :: arrC3(:)
    CHARACTER*100 :: fich,token
    CHARACTER*100 :: gatewayFile
    CHARACTER*100 :: orbitalFileParent
    CHARACTER*100 :: monocentricBasisFile
    CHARACTER*100 :: project
    CHARACTER*100 :: rdmMethod
    CHARACTER*100 :: typeOrbital
    CHARACTER*100 :: CCF
    CHARACTER*100 :: lkMonocentricFile
    CHARACTER*200 :: dir
    REAL*8 :: linearDependenceThr,Rmin,Rmax
    TYPE(inputString) :: ras1
    TYPE(inputString) :: ras2
    TYPE(inputString) :: ras3
    TYPE(inputString) :: inactive
    TYPE(inputString) :: casOccupied
    TYPE(inputString) :: symmetryStatesParent
    TYPE(inputString) :: numberStatesParent
    TYPE(inputString) :: symmetryStatesSource
    TYPE(inputString) :: numberStatesSource
    TYPE(inputString) :: createSourceFromParent
    TYPE(inputString) :: sourceFiles
    TYPE(inputString) :: multiplicityParent
    TYPE(gateway) :: molcasGate
    
    fich=""
    dir=""
    CALL GETARG(1,dir)
    CALL GETARG(2,fich)
    
    IF (LEN(TRIM(fich)).eq.0) THEN
        STOP "ERROR no input defined"
    ENDIF
    IF (LEN(TRIM(dir)).eq.0) THEN
        STOP "ERROR no output directory defined"
    ENDIF           
    WRITE(6,'(A16,x,A10)') "Input file:",fich   
    CALL readInput( fich,&
      gatewayFile,&
      ras1,&
      ras2,&
      ras3,&
      inactive,&
      symmetryStatesParent,&
      numberStatesParent,&
      symmetryStatesSource,&
      numberStatesSource,&
      createSourceFromParent,&
      sourceFiles,&
      orbitalFileParent,&
      project,&
      monocentricBasisFile,&
      nHoleRas1,&
      nElecRas2,&
      nElecRas3,&
      linearDependenceThr,&
      multiplicityParent,&
      multiplicityAugmented,&
      maxMultipoleOrder,&
      lMonocentric,&
      kMonocentric,&
      rdmMethod,&
      multiGhost,&
      typeOrbital,&
      forceC1,&
      Rmin,&
      Rmax,&
      BSplineNodes,&
      CCF,&
      lkMonocentricFile)

    CALL molcasGate%setProjectName(project)
    CALL molcasGate%setMonocentricBasisFile(monocentricBasisFile)
    CALL molcasGate%readGateway(gatewayFile)
    
    casOccupied=inactive+ras1
    casOccupied=casOccupied+ras2
    casOccupied=casOccupied+ras3
    CALL casOccupied%sumString(occupiedNoSym)
    
    electronsParent=nElecRas2
  
    CALL inactive%sumString(inactiveNoSym)
    electronsParent=electronsParent+2*inactiveNoSym
    write(6,'(A,I0)') "Number of electrons in parent ions: ",electronsParent
    
    CALL numberStatesParent%sumString(nParent)
    CALL numberStatesSource%sumString(nSource)
    
    CALL symmetryStatesParent%sizeString(nSym)
    CALL symmetryStatesParent%writeString(token)

    OPEN(2,file=TRIM(dir)//"/GABS_interface_sym.xchem",STATUS="replace")
    CALL writeGABS_Interface_sym(2,multiplicityAugmented,inactive,ras1,ras2,ras3,&
      kMonocentric,lMonocentric,multiGhost,forceC1,lkMonocentricFile)
    CLOSE(2)
    
    CALL numberStatesParent%getArray(arrI1)
    CALL ras2%sumString(nRas3)
    nRas3=nRas3+2
    arrI1=arrI1*nRas3
    CALL numberStatesParent%setArray(arrI1)
    
    CALL casOccupied%sumString(nOrb)
    
    CALL sourceFiles%getArray(arrC1)
    CALL createSourceFromParent%getArray(arrC2)
    CALL numberStatesSource%getArray(arrI1)
    CALL numberStatesSource%getArray(arrI2)
    iSym=0
    
    OPEN(2,file=TRIM(dir)//"/oneIntBas2Orb.xchem",STATUS="replace")
    CALL writeOneIntBas2Orb(2)
    CLOSE(2)
    
    OPEN(2,file=TRIM(dir)//"/ijkl2ijrs.xchem",STATUS="replace")
    CALL writeIjkl2ijrs(2,occupiedNoSym)
    CLOSE(2)
    OPEN(2,file=TRIM(dir)//"/ijkl2iqks.xchem",STATUS="replace")
    CALL writeIjkl2iqks(2,occupiedNoSym)
    CLOSE(2)
    OPEN(2,file=TRIM(dir)//"/iqks2pqrs.xchem",STATUS="replace")
    CALL writeIqks2pqrs(2,occupiedNoSym)
    CLOSE(2)
    
    OPEN(2,file=TRIM(dir)//"/oneIntOrb2OperatorMatrix_new.xchem",STATUS="replace")
    CALL writeOneIntOrb2OperatorMatrix_new(2,inactiveNoSym,nSource,nParent)
    CLOSE(2)
    
    OPEN(2,file=TRIM(dir)//"/twoIntOrb2OperatorMatrix_new.xchem",STATUS="replace")
    CALL writeTwoIntOrb2OperatorMatrix_new(2,inactiveNoSym,nSource,nParent)
    CLOSE(2)

    OPEN(2,file=TRIM(dir)//"/sumOneIntTwoIntNuclear.xchem",STATUS="replace")
    CALL writeSumOneIntTwoIntNuclear(2)
    CLOSE(2)
    
    OPEN(2,file=TRIM(dir)//"/linDepKill.xchem",STATUS="replace")
    CALL writeLinDepKill(2,lMonocentric,kMonocentric,nOrb,linearDependenceThr,multiGhost,lkMonocentricFile)
    CLOSE(2)
    
    OPEN(2,FILE=TRIM(dir)//"/trd2Build.xchem",STATUS="replace")
    CALL writeTrd2Build(2)
    CLOSE(2)
    OPEN(2,FILE=TRIM(dir)//"/trd1Build.xchem",STATUS="replace")
    CALL writeTrd1Build(2)
    CLOSE(2)

    OPEN(2,FILE=TRIM(dir)//"/symSplit.xchem",STATUS="replace")
    CALL writeSymSplit(2,inactiveNoSym,multiplicityAugmented,&
      symmetryStatesParent,numberStatesParent,&
      symmetryStatesSource,numberStatesSource,&
      multiplicityParent,forceC1)
    CLOSE(2)

    WRITE(6,*) "Happy"
END PROGRAM
