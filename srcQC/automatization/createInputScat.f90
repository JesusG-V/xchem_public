!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

PROGRAM autoAugment
    USE class_inputString
    USE createInputUtilities
    implicit none
    INTEGER :: i,nSym,iSym,electronsParent,nRas3,nOrb
    INTEGER :: lMonocentric,kMonocentric,totalLMonocentric
    INTEGER :: maxMultipoleOrder
    !INTEGER :: multiplicityParent
    INTEGER :: multiplicityAugmented
    INTEGER :: occupiedNoSym
    INTEGER :: nHoleRas1
    INTEGER :: nElecRas2
    INTEGER :: nElecRas3
    INTEGER :: nodes
    INTEGER :: inactiveNoSym
    INTEGER :: nParent
    INTEGER :: nSource
    LOGICAL :: multiGhost,forceC1
    INTEGER, ALLOCATABLE :: arrI1(:)
    INTEGER, ALLOCATABLE :: arrI2(:)
    CHARACTER*100, ALLOCATABLE :: arrC1(:)
    CHARACTER*100, ALLOCATABLE :: arrC2(:)
    CHARACTER*100, ALLOCATABLE :: arrC3(:)
    CHARACTER*200 :: dir,fich,token
    CHARACTER*200 :: gatewayFile
    CHARACTER*200 :: orbitalFileParent
    CHARACTER*200 :: monocentricBasisFile
    CHARACTER*100 :: project
    CHARACTER*200 :: rdmMethod
    CHARACTER*100 :: typeOrbital
    REAL*8 :: linearDependenceThr
    TYPE(inputString) :: ras1
    TYPE(inputString) :: ras2
    TYPE(inputString) :: ras3
    TYPE(inputString) :: inactive
    TYPE(inputString) :: casOccupied
    TYPE(inputString) :: symmetryStatesParent
    TYPE(inputString) :: numberStatesParent
    TYPE(inputString) :: symmetryStatesSource
    TYPE(inputString) :: numberStatesSource
    TYPE(inputString) :: createSourceFromParent
    TYPE(inputString) :: sourceFiles
    TYPE(inputString) :: multiplicityParent

    fich=""
    dir=""
    CALL GETARG(1,dir)
    CALL GETARG(2,fich)

    IF (LEN(TRIM(fich)).eq.0) THEN
        STOP "ERROR no input defined"
    ENDIF
    IF (LEN(TRIM(dir)).eq.0) THEN
        STOP "ERROR no output directory defined"
    ENDIF           
    CALL readInput( fich,&
      gatewayFile,&
      ras1,&
      ras2,&
      ras3,&
      inactive,&
      symmetryStatesParent,&
      numberStatesParent,&
      symmetryStatesSource,&
      numberStatesSource,&
      createSourceFromParent,&
      sourceFiles,&
      orbitalFileParent,&
      project,&
      monocentricBasisFile,&
      nHoleRas1,&
      nElecRas2,&
      nElecRas3,&
      linearDependenceThr,&
      multiplicityParent,&
      multiplicityAugmented,&
      maxMultipoleOrder,&
      lMonocentric,&
      kMonocentric,&
      nodes,&
      rdmMethod,&
      multiGhost,&
      typeOrbital,&
      forceC1,&
      CCF,&
      BSplineRMin,&
      BSplineRMax,&
      BSplineNodes)

    fich=""
    dir=""
    CALL GETARG(1,dir)
    CALL GETARG(2,fich)

    IF (LEN(TRIM(fich)).eq.0) THEN
        STOP "ERROR no input defined"
    ENDIF
    IF (LEN(TRIM(dir)).eq.0) THEN
        STOP "ERROR no output directory defined"
    ENDIF           
    WRITE(6,'(A16,x,A10)'), "Input file:",fich   
    CALL readInput( fich,&
      gatewayFile,&
      ras1,&
      ras2,&
      ras3,&
      inactive,&
      symmetryStatesParent,&
      numberStatesParent,&
      symmetryStatesSource,&
      numberStatesSource,&
      createSourceFromParent,&
      sourceFiles,&
      orbitalFileParent,&
      project,&
      monocentricBasisFile,&
      nHoleRas1,&
      nElecRas2,&
      nElecRas3,&
      linearDependenceThr,&
      multiplicityParent,&
      multiplicityAugmented,&
      maxMultipoleOrder,&
      lMonocentric,&
      kMonocentric,&
      nodes,&
      rdmMethod,&
      multiGhost,&
      typeOrbital,&
      forceC1,&
      CCF,&
      BSplineRMin,&
      BSplineRMax,&
      BSplineNode)

    OPEN(2,file=trim(dir)//"/BSplineBasis")
    CALL writeBSplineBasis(2,BSplineRMin,BSplineRMax,BSplineNodes)  

END PROGRAM
