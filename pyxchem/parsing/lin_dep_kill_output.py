#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from xchem_error import *

class LinDepKillOutputParser:
	def __init__(self):
		self._number_diffuse_orbitals = None

	def __call__(self, line):
		if 'Total orbitals' in line:
			self._number_diffuse_orbitals = int(line.split()[-1])

	@property
	def number_diffuse_orbitals( self ):
		if self._number_diffuse_orbitals:
			return( self._number_diffuse_orbitals ) 
		else:
			raise XchemError('No set of linearly independent diffues orbitals could be produced. Increase threshold?')
