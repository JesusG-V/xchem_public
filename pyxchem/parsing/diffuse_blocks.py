#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

from collections import OrderedDict

class DiffusenBlockParser:
	alphabeticL = 'spdfghiklmnoqrtu'
	def __init__(self, file_name):
		self.file_name = file_name
		self.num_basis_fcts = None
		self.found_petite_basis_list = False
		self.finished = False
		self.blocks = OrderedDict()

	def __call__(self, line):
		if self.finished:
			return
		if self.found_petite_basis_list:
			try:	
				basis_id = int(line.split()[0])
			except:
				return
			self.process_petite_line( line )
			if basis_id == self.num_basis_fcts:
				self.write()
				self.finished = True
		if 'Number of basis functions' in line:
			self.num_basis_fcts = int(line.split()[-1])	
		if 'Petite list Basis Function' in line:
			self.found_petite_basis_list = True

	def process_petite_line(self, line):
		basis_id, atom, label, center = line.split()
		if 'X' not in atom:
			return
		ghostId = DiffusenBlockParser.getGhostId(atom)
		l,m,k = DiffusenBlockParser.getLmk(label)
		block = (label, ghostId)
		if block in self.blocks:
			self.blocks[(label, ghostId)]['nfcts'] += 1
		else:
			self.blocks[(label, ghostId)]  = {'nfcts':1,'k':k,'l':l,'m':m}

	def write(self):
		with open(self.file_name, 'w') as f:
			for (label,ghostId),info in self.blocks.items():
				nfcts = info['nfcts']
				l = info['l']
				m = info['m']
				k = info['k']
				f.write('{} {} {} {} {} {}\n'.format(nfcts,label,l,m,k,ghostId))
		
	@staticmethod
	def getGhostId(atom):
		gid = 0
		if len(atom)>1:
			gid = int(atom[1:])
		return(gid)

	@staticmethod
	def getLmk(label):
		l_letter = label.translate( {ord(s) : None for s in '1234567890+-xyz'} )
		l = DiffusenBlockParser.alphabeticL.index( l_letter )
		l_index = label.index(l_letter)
		raw_k = int(label[:l_index])
		k = int((raw_k-l)/2)
		if l==0:
			m=0
		else:
			m_string = label[l_index+1:]
			if m_string == 'x':	
				m = 1
			elif m_string == 'y':
				m = -1
			elif m_string == 'z':
				m = 0
			else:
				if m_string[-1] == '0':
					m = 0
				else:
					m = int(m_string[:-1])
					if m_string[-1] == '-':
						m *= -1
		return (l,m,k)
