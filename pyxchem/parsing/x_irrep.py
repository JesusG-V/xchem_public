#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

class XIrrepParser:
	def __init__(self):
		self._irrep = None
		self._on = False
		self._seen = []
		self._result = []
		self.result = {}
		
	def __call__(self, line):
		if 'Irreducible representation :' in line:
			self._irrep = line.split()[-1]
			self._on = True
		if ' X' in line and self._on: 
			label, type = line.split()[1:3]
			if '{}{}'.format(label,type) not in self._seen:
				self._result.append((type,self._irrep))
				self._seen.append('{}{}'.format(label,type))
		if 'Symmetry species  ' in line and self._on:
			self._on = False
			symm_lst = line.split()[2:]
			self.result = { label: symm_lst.index(type) + 1 for label, type in self._result }
