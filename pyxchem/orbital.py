#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import numpy as np
import os
class Orbital:
	def __init__(self, file_name = None):
		self._nsym = None
		self._orbitals_of_sym = None
		self._raw = []
		self._orbs = []
		self._occ = []
		self._one = []
		self._index = []
		if file_name:
			self.read(file_name)

	def set_occ(self, occ_val):
		self._occ = [ np.ones(shape=occ.shape) * occ_val for occ in self._occ ]

	def reorder(self, input_dict):
		inac = input_dict['inac']
		ras1 = input_dict['ras1']
		ras2 = input_dict['ras2']
		ras3 = input_dict['ras3']
		energy = 1.0
		total = [ 0  for i in range(self._nsym) ]
		if (inac): 
			for isym,norb in enumerate(inac):
				for iorb in range(norb):
					self._one[isym][iorb] = energy
					energy += 1.0
			total = inac

		if (ras1):
			for isym,norb in enumerate(ras1):
				for iorb in range(norb):
					self._one[isym][iorb + total[isym] ] = energy
					energy += 1.0
			total = [ i+j for i,j in zip(total,ras1) ] 

		if (ras2):
			for isym,norb in enumerate(ras2):
				for iorb in range(norb):
					self._one[isym][iorb + total[isym] ] = energy
					energy += 1.0
			total = [ i+j for i,j in zip(total,ras2) ] 

		if (ras3):
			for isym,norb in enumerate(ras3):
				for iorb in range(norb):
					self._one[isym][iorb + total[isym] ] = energy
					energy += 1.0
			total = [ i+j for i,j in zip(total,ras3) ] 

		for isym,norb in enumerate(self._orbitals_of_sym):
			for iorb in range(total[isym], norb):
				self._one[isym][iorb] = energy
				energy += 1.0

	def write(self, file_name, fmt = 'INPORB 2.2'):
		with open( file_name, 'w') as f:
			f.write('#' + fmt + '\n' )
			f.write('#INFO\n')
			f.write('* Symetry Specfications\n')
			f.write('0   %i   0 ' % self._nsym + '\n')
			f.write( ' '.join( list(map(str,self._orbitals_of_sym))) + '\n')
			f.write( ' '.join( list(map(str,self._orbitals_of_sym))) + '\n')
			f.write('* Written by pyxchem\n')
			f.write('#ORB\n')
			#PRINT ORBITALS
			orb_count = 0
			for i,(orb,norb) in enumerate(zip(self._orbs, self._orbitals_of_sym)):
				for iorb in range(norb):
					orb_count += 1
					f.write( '* ORBITAL{:5d}{:5d}{:5d}'.format(i+1, iorb+1, orb_count) + '\n' )
					f.write(Orbital.array_to_fmt_string(orb[iorb,:]))

			f.write('#OCC\n')
			f.write('* OCCUPATION NUMBERS\n')
			for occ in self._occ:
				f.write(Orbital.array_to_fmt_string(occ))

			f.write('#OCHR\n')
			f.write('* OCCUPATION NUMBERS\n')
			for occ in self._occ:
				f.write(Orbital.array_to_fmt_string(occ, ncol=10, fmt=' {:7.4f}'))

			f.write('#ONE\n')
			f.write('* ONE ELECTRON ENERGIES\n')
			for one in self._one:
				f.write(Orbital.array_to_fmt_string(one, ncol=10, fmt=' {: 11.4e}'))

			f.write('#INDEX\n')
			for index in self._index:
				f.write('* 1234567890\n')
				if len(index) == 0: continue
				start, end = 0,0
### JGV checking if the number is a multiple of 10
				if len(index)%10 == 0:
					nrows=int(len(index)/10)
				else:
					nrows=int(len(index)/10)+1
				#for l in range(int(len(index)/10)+1):
				for l in range(nrows):
					end += min(10, len(index))
					f.write('{:1d} {:s}\n'.format(l,index[start:end]))
					start = end

	def array_to_fmt_string( arr , ncol = 5, fmt = ' {: 21.14e}'):
		str = ""
		length = arr.shape[0]
		if length == 0: return(str)
		start, end = 0,0
		for line in range(int(length/ncol)+1):
			end = min(end+ncol,length)
			for i in range(end-start):
				str += fmt.format(arr[start + i])
			start = end 
			if line < int(length/ncol):
				str += '\n'
		if length % ncol != 0:
			str += '\n'
		return(str)


	def read(self,file_name ):
		with open( file_name, 'r' )  as f:
			for line in f:
				line = line.strip()
				if str.startswith( line,'*') or not line: continue
				self._raw.append(line.split())
		info_index = self._raw.index(['#INFO'])
		self._nsym =int( self._raw[info_index + 1][1] )
		self._orbitals_of_sym = list(map(int,self._raw[info_index + 2]))

		orb_index = self._raw.index(['#ORB'])
		occ_index = self._raw.index(['#OCC'])
		ochr_index = self._raw.index(['#OCHR'])
		one_index = self._raw.index(['#ONE'])
		index_index = self._raw.index(['#INDEX'])

		ncoef = sum( list( map( lambda x:x**2, self._orbitals_of_sym )))

		orb = [ float(item) for sublist in self._raw[orb_index + 1 : occ_index ] for item in sublist]
		occ = [float(item) for sublist in self._raw[occ_index + 1 : ochr_index ] for item in sublist]
		one = [float(item) for sublist in self._raw[one_index + 1 : index_index ] for item in sublist]
		index = ''.join([item[1] for item in self._raw[index_index + 1: ]] )

		start1, end1 = 0,0
		start2, end2 = 0,0
		for norb in self._orbitals_of_sym:
			end1 += norb
			end2 += norb ** 2
			orb_arr = np.array( orb[start2:end2] ).reshape((norb,norb))
			one_arr = np.array( one[start1:end1] )  
			occ_arr = np.array( occ[start1:end1] )
			index_str = index[start1:end1]
			self._orbs.append(orb_arr)
			self._one.append(one_arr)
			self._occ.append(occ_arr)
			self._index.append(index_str)
			start1 = end1
			start2 = end2
