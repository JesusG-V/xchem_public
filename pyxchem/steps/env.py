#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os
import xchem_abstract
import subprocess
import osutils
from xchem_error import *

class Env(xchem_abstract.XchemModule):
	def __init__( self, input_dict, info = False ):
		super().__init__(input_dict, 'env', info = info)
		# Add the acceptable parameters for this module.
		# MOLCAS
		molcas = xchem_abstract.XchemKeyword( name = 'molcas',
										      optional = True, 
											  data_type = 'str'
											  )
		description ='''If given sets the environment variable MOLCAS.'''
		molcas.description = description
		
		# MOLCAS_MEM
		molcas_mem = xchem_abstract.XchemKeyword( name = 'molcas_mem',
										          optional = True, 
											      data_type = 'int'
											      )
		description ='''If given sets the environment variable MOLCAS_MEM.'''
		molcas_mem.description = description
		
		# OMP_NUM_THREADS
		omp_num_threads = xchem_abstract.XchemKeyword( name = 'omp_num_threads',
										               optional = True, 
											           data_type = 'str'
											           )
		description ='''If given sets the environment variable OMP_NUM_TRHEADS.'''
		omp_num_threads.description = description
		
		#RMLOCK 
		rmLock = xchem_abstract.XchemKeyword( name = 'rmLock',
										               optional = True, 
											           data_type = 'bool',
                                                                                                   default = True
											           )
		description ='''If TRUE removes the possible remaining *.lock files (reading/writing HDF5 files) from previous jobs. It is TRUE by default.'''
		rmLock.description = description
		
		# XCHEM_PRINT
		xchem_print = xchem_abstract.XchemKeyword( name = 'xchem_print',
										           optional = True, 
											       data_type = 'int',
												   default = 1
											       )
		description ='''Sets the verbosity of the output to be printed:
		0: Minimal (default)
		1: Verbose '''
		xchem_print.description = description

		#XCHEM_DATA 
		xchem_data_dir = xchem_abstract.XchemKeyword( name = 'xchem_data_dir',
													  optional = True,
													  data_type = 'str'
													  )
		description = '''Directory storing the data produced at various steps of the \
		                 calculation.'''
		xchem_data_dir.description = description
		xchem_data_dir.default = os.path.join( os.getcwd(), "xchem_data" )

		#XCHEM_NUM_NODES
		xchem_max_num_nodes = xchem_abstract.XchemKeyword( name = 'xchem_max_num_nodes',
														   optional = True,
														   data_type = 'int',
														   default = 10
														   )
		description = 'Set the maximum number of jobs to run simultaneously in submitting either rdm or integrals'
		xchem_max_num_nodes.description = description

		#XCHEM
		xchem = xchem_abstract.XchemKeyword( name = 'xchem',
											 optional = True,
											 data_type = 'str')

		#XCHEM_DEBUG
		xchem_debug = xchem_abstract.XchemKeyword( name = 'xchem_debug',
													  optional = True,
													  data_type = 'bool',
													  default = False
													  )
		description = '''If true use debug binaries.'''
		xchem_debug.description = description
	
		# Associate keywords with this model
		self.add_keyword( molcas )
		self.add_keyword( molcas_mem )
		self.add_keyword( omp_num_threads )
		self.add_keyword( rmLock )
		self.add_keyword( xchem_print )
		self.add_keyword( xchem_data_dir )
		self.add_keyword( xchem ) 
		self.add_keyword( xchem_debug ) 
		self.add_keyword( xchem_max_num_nodes )

		self._check_keywords()
	
	@xchem_abstract.prolog_and_epilog
	def run( self, uid = 0 ):
		# Organize environment variables.
		if  self.input_dict['molcas']:
			os.environ['MOLCAS'] = self.input_dict['molcas']
		else:
			if 'MOLCAS' not in os.environ:
				raise XchemError('MOLCAS environment variable not defined')
		#
		if self.input_dict['molcas_mem']:
			os.environ['MOLCAS_MEM'] = str(self.input_dict['molcas_mem'])
		else:
			if 'MOLCAS_MEM' not in os.environ:
				os.environ['MOLCAS_MEM'] = '400'
		#
		if self.input_dict['omp_num_threads']:
			os.environ['OMP_NUM_THREADS'] = self.input_dict['omp_num_threads']
		else:
			if 'OMP_NUM_THREADS' not in os.environ:
				os.environ['OMP_NUM_THREADS'] = '1'
		#
		if self.input_dict['rmLock']:
			os.environ['rmLock'] = '1'
		else:
			os.environ['rmLock'] = '0'
		#
		if self.input_dict['xchem_print']:
			os.environ['XCHEM_PRINT'] = str(self.input_dict['xchem_print'])
		#
		if self.input_dict['xchem_data_dir']:
			if 'XCHEM_DATA_DIR' not in os.environ:
				os.environ['XCHEM_DATA_DIR'] = self.input_dict['xchem_data_dir']
		#
		if self.input_dict['xchem']:
			os.environ['XCHEM'] = self.input_dict['xchem']
		#
		if self.input_dict['xchem_max_num_nodes']:
			os.environ['XCHEM_MAX_NUM_NODES'] = str(self.input_dict['xchem_max_num_nodes'])
		#
		if self.input_dict['xchem_debug']:
			os.environ['XCHEM_DEBUG'] = '1'
		else:
			os.environ['XCHEM_DEBUG'] = '0'
		os.environ['XCHEM_SUBMIT'] = os.getcwd()

		# Print the environment variables.
		print('The environment is setup as follows')
		print('MOLCAS=' + os.environ['MOLCAS'] ) 
		print('MOLCAS_MEM=' + os.environ['MOLCAS_MEM'] )
		print('XCHEM=' + os.environ['XCHEM'] )
		print('XCHEM_DATA_DIR=' + os.environ['XCHEM_DATA_DIR'] )

		if not os.path.isfile( os.path.join(os.environ['XCHEM'],'pyxchem','pyxchem.py') ):
			raise XchemError('Could not find an xchem installation at the path provided.')

		# Look for and test installation of pymolcas and molcas.
		try:
			p = subprocess.Popen(['pymolcas','--banner'])
			rc = p.wait()
		except FileNotFoundError:
			raise XchemMolcasError('pymolcas could not be run.')
		if rc != 0:
			raise XchemMolcasError('molcas could not be run.')	
		else:
			print('Molcas installation works correctly')

