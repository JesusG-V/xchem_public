#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os, shutil,sys
import xchem_abstract
import subprocess
import xchem_keywords
from xchem_error import *
from molcas_input_generators import *
from xchem_input_generators import *
from molcas_manager import MolcasManager
from xchem_manager import XchemManager
import osutils
import h5_extractors
import itertools
import submission_tools

class Integrals(xchem_abstract.XchemModule):
	def __init__( self, input_dict, info = False ):
		super().__init__(input_dict, 'integrals', info = info )
		# Add the acceptable parameters for this module.
		self.add_keyword( xchem_keywords.defer )
		self.add_keyword( xchem_keywords.gateway_desym )
		self.add_keyword( xchem_keywords.pqrs_temporal )

		self._check_keywords()
		self.MOLCAS = MolcasManager( WorkDir= self.WorkDir, 
									 MOLCAS_OUTPUT = self.WorkDir)
		self.xchem = XchemManager()

	def ijks( self, expslist=None, ntask=None,exp1=None,exp2=None,intfile=None,readprev=True ):
		if  self.input_dict['defer']:
			if self.input_dict['saveTwoElInt']:
				conc_input = twoexpconc_input(self.DataDir, ntask,  expslist) 
				task = self.xchem.task('twoIntConcatenatetwoexp', xchem_file_input = conc_input)
				self.job_list.add_task( task )
			else:
				data_dirs = self.load_previous_meta_data(['basis','orbitals'])
				suf=str(exp1)+'-'+str(exp2)
				self.log_message('Transform two electron basis integrals to orbital integrals '+str(suf))
				if self.input_dict['pqrs_temporal']:
					ijkl2pqrs = ijkl2pqrs_input( self.input_dict, self.meta_dict, suf, intfile, ntask )
				else:
					ijkl2pqrs = ijkl2pqrs_input( self.input_dict, self.meta_dict, suf, intfile, ntask, readprev=readprev, path=self.WorkDir )
				task=self.xchem.task('ijkl2pqrs', ijkl2pqrs)
				self.job_list.add_task(task)

### OLD
#				ij_suffix = open( os.path.join(self.WorkDir, 'ij.suffix'+str(ntask)) ,'w')
#				for i in range(len(expslist)):
#					ij_suffix.write(expslist[i]+'\n')
#				ij_suffix.close()	
#				
#				self.log_message('Transform two electron basis integrals to orbital integrals')
#				self.log_message('compute [iq||ks] from [ij||kl] ',priority=2)
#				ijkl2iqks = ijkl2iqks_input( self.input_dict, self.meta_dict, ntask)
#				task = self.xchem.task('ijkl2iqks', ijkl2iqks )
#				self.job_list.add_task(task)
#				#
#				self.log_message('compute [ij||rs] from [ij||kl] ',priority=2)
#				ijkl2ijrs = ijkl2ijrs_input( self.input_dict, self.meta_dict, ntask )
#				task = self.xchem.task('ijkl2ijrs', ijkl2ijrs )
#				self.job_list.add_task(task)
#				#
#				self.log_message('compute [pq||rs] from [iq||ks] and [ij||rs]',priority=2)
#				iqks2pqrs = iqks2pqrs_input( self.input_dict, self.meta_dict, ntask ) 
#				task = self.xchem.task('iqks2pqrs', iqks2pqrs)
#				self.job_list.add_task(task)
		else:
			if self.input_dict['saveTwoElInt']:
				shutil.move(intfile, os.path.join(self.DataDir,intfile))
				return
			else:
				data_dirs = self.load_previous_meta_data(['basis','orbitals'])
				ij_suffix = open( os.path.join(self.WorkDir, 'ij.suffix') ,'w')
				suf=str(exp1)+'-'+str(exp2)
				ij_suffix.write(suf)
				ij_suffix.close()	
				self.log_message('Transform two electron basis integrals to orbital integrals '+str(suf))
				if suf == "0-0":
					if os.path.isfile("pqrs.int"):
						os.remove("pqrs.int")
				ijkl2pqrs = ijkl2pqrs_input( self.input_dict, self.meta_dict, suf, intfile )
				self.xchem.run('ijkl2pqrs', ijkl2pqrs, in_files = {data_dirs['orbitals'] : [r'mpcg\.bin']})
 

	@xchem_abstract.prolog_and_epilog
	def run( self, uid = 0 ):
		osutils.mkdir( self.WorkDir )
		osutils.mkdir( self.DataDir )
		os.chdir(self.WorkDir)

		data_dirs = self.load_previous_meta_data(['basis'])
		if not self.input_dict['saveTwoElInt']:
			data_dirs = self.load_previous_meta_data(['basis','orbitals'])

		seward = seward_input( oneo = False)
		lastshell = h5_extractors.lastshell( os.path.join( data_dirs['basis'], self.DesymProject + '.guessorb.h5' )) 
		self.meta_dict['lastshell'] = lastshell
		self.log_message('Number of local shells = {}'.format(lastshell) )

		self.log_message('Run Molcas to compute and compress two electron integrals for PCG and MCG bases')
#VICENT MODIFICATION
		nexp = self.input_dict['nexp']
		lMonocentric = self.input_dict['lMonocentric']
		kMonocentric = self.input_dict['kMonocentric']
		LMax = lMonocentric[0] + 2*kMonocentric[0]

		if self.input_dict['twoexp']:
			ntask = 1
			nofjob = 0
			expslist=[]
			if self.input_dict['defer']:
				new_job = submission_tools.need_new_job( int((nexp*(nexp+1))/2+1), int(os.environ['XCHEM_MAX_NUM_NODES']))
			self.log_message('Consider just the polycentric basis')
			exp1 = exp2 = 0
			gateway = gateway_input( os.path.join( data_dirs['basis'], self.SymProject + '.guessorb.h5'), 
						 sdip = True, 
						 monocentric = False ,
						 input_dict = self.input_dict)
			ij_suffix_handle = open( os.path.join(self.DataDir, 'ij.suffix') ,'w')
			exp1 = exp2 = 0
			shell_limit = {
					'lastshell'    : lastshell,
					'exp1'         : exp1,
					'exp2'         : exp2,
					'nexp'         : nexp,
					'lMonocentric' : lMonocentric,
					'kMonocentric' : kMonocentric
						}
			if self.input_dict['defer']:
				if new_job[nofjob]:
					readprev=False
					if self.input_dict['saveTwoElInt']:
						self.job_list.add_job( in_files = {self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'.int.*'] } )
					else:
						self.job_list.add_job( in_files = {data_dirs['orbitals'] : ['mpcg\.bin'], self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'pqrs\.int.*'] } )
## First job, not read the prev pqrs.int
					expslist=[]
				task = self.MOLCAS.task( molcas_input = gateway, Project = self.DesymProject ,  shell_limit = shell_limit )
				self.job_list.add_task( task )
			else:
				self.MOLCAS.run( gateway, Project = self.DesymProject, shell_limit = shell_limit )

			intfile = 'ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)

			if self.input_dict['defer']:
				task = self.MOLCAS.task( seward,
						Project = self.DesymProject,
	    					 dump_integrals = False,
	    					 shell_limit = shell_limit )
				self.job_list.add_task( task )
				expslist.append(str(exp1)+'-'+str(exp2))
				self.ijks(exp1=exp1,exp2=exp2,intfile=intfile,ntask=ntask,readprev=readprev,expslist=expslist)
				readprev=True
				nofjob = nofjob + 1
			else:
#				if not self.input_dict['saveTwoElInt']:
#					intfile = '/dev/shm/ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)+"."+str(os.getpid())
#					kk=open(os.path.join(self.WorkDir,"intmem"),"w")
#					kk.write('"'+intfile+'"\n')
#					kk.close()
				self.MOLCAS.run(  seward,
						Project = self.DesymProject,
	    					 dump_integrals = False,
	    					 shell_limit = shell_limit )
				ij_suffix_handle.write('{}-{}\n'.format(exp1,exp2))
				self.ijks(exp1=exp1,exp2=exp2,intfile=intfile)

			if self.input_dict['defer']:
				if nofjob == len(new_job) or new_job[nofjob]:
					#self.ijks(expslist=expslist, ntask=ntask )
					ntask += 1

			for exp1 in range(1,nexp+1):
				exp2 = 0
				self.log_message('Consider just one monocentric exponent: i={}'.format(exp1), priority=2)
				gateway = gateway_input( os.path.join( data_dirs['basis'], self.SymProject + '.guessorb.h5'), 
							sdip = True, 
							monocentric = True,
							input_dict = self.input_dict,
							exp1 = exp1,
							exp2 = exp2 )
				shell_limit = {
						'lastshell' : lastshell,
						'exp1'      : exp1,
						'exp2'      : exp2,
						'nexp'      : nexp,  
						'lMonocentric' : lMonocentric,
						'kMonocentric' : kMonocentric
								}
				if self.input_dict['defer']:
					if new_job[nofjob]:
						readprev=False
						if self.input_dict['saveTwoElInt']:
							self.job_list.add_job( in_files = {self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'.int.*'] } )
						else:
							self.job_list.add_job( in_files = {data_dirs['orbitals'] : ['mpcg\.bin'], self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'pqrs\.int.*'] } )
						expslist=[]
					task = self.MOLCAS.task( molcas_input = gateway, Project = self.DesymProject ,  shell_limit = shell_limit )
					self.job_list.add_task( task )
				else:
					self.MOLCAS.run( gateway, Project = self.DesymProject, shell_limit = shell_limit )

				intfile = 'ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)
				if self.input_dict['defer']:
					task = self.MOLCAS.task( seward,
							Project = self.DesymProject,
	    						 dump_integrals = False,
	    						 shell_limit = shell_limit )
					self.job_list.add_task( task )
					expslist.append(str(exp1)+'-'+str(exp2))
					self.ijks(exp1=exp1,exp2=exp2,intfile=intfile,ntask=ntask,readprev=readprev,expslist=expslist)
					readprev=True
					nofjob = nofjob + 1
				else:
#					if not self.input_dict['saveTwoElInt']:
#						intfile = '/dev/shm/ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)+"."+str(os.getpid())
#						kk=open(os.path.join(self.WorkDir,"intmem"),"w")
#						kk.write('"'+intfile+'"\n')
#						kk.close()
					self.MOLCAS.run(  seward,
							Project = self.DesymProject,
	    						 dump_integrals = False,
	    						 shell_limit = shell_limit )
					ij_suffix_handle.write('{}-{}\n'.format(exp1, exp2))
					self.ijks(exp1=exp1,exp2=exp2,intfile=intfile)
				if self.input_dict['defer']:
					if nofjob == len(new_job) or new_job[nofjob]:
						#self.ijks(expslist=expslist, ntask=ntask)
						ntask += 1
				for exp2 in range(exp1,nexp+1):
					if exp1 == exp2:
						self.log_message('Consider monocentric exponents: i={} and j>{}'.format(exp1,exp2), priority=2)
					else:
						gateway = gateway_input( os.path.join( data_dirs['basis'], self.SymProject + '.guessorb.h5'), 
										sdip = True, 
										monocentric = True ,
										input_dict = self.input_dict,
										exp1 = exp1,
										exp2 = exp2 )
						shell_limit = {
								'lastshell' : lastshell,
								'exp1'      : exp1,
								'exp2'      : exp2,
								'nexp'      : nexp,  
								'lMonocentric' : lMonocentric,
								'kMonocentric' : kMonocentric
									}
						if self.input_dict['defer']:
							if new_job[nofjob]:
								readprev=False
								if self.input_dict['saveTwoElInt']:
									self.job_list.add_job( in_files = {self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'.int.*'] } )
								else:
									self.job_list.add_job( in_files = {data_dirs['orbitals'] : ['mpcg\.bin'], self.WorkDir : ['ij\.suffix\.*'] }, out_files = {self.WorkDir : [r'pqrs\.int.*'] } )
								expslist=[]
							task = self.MOLCAS.task( molcas_input = gateway, Project = self.DesymProject ,  shell_limit = shell_limit )
							self.job_list.add_task( task )
						else:
							self.MOLCAS.run( gateway, Project = self.DesymProject, shell_limit = shell_limit )

						intfile = 'ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)

						if self.input_dict['defer']:
							task = self.MOLCAS.task( seward,
									Project = self.DesymProject,
	    								 dump_integrals = False,
	    								 shell_limit = shell_limit )
							self.job_list.add_task( task )
							expslist.append(str(exp1)+'-'+str(exp2))
							self.ijks(exp1=exp1,exp2=exp2,intfile=intfile,ntask=ntask,readprev=readprev,expslist=expslist)
							readprev=True
							nofjob = nofjob + 1
						else:
#							if not self.input_dict['saveTwoElInt']:
#								intfile = '/dev/shm/ham2E.compressed.int.'+str(exp1)+'-'+str(exp2)+"."+str(os.getpid())
#								kk=open(os.path.join(self.WorkDir,"intmem"),"w")
#								kk.write('"'+intfile+'"\n')
#								kk.close()
							self.MOLCAS.run(  seward,
									Project = self.DesymProject,
	    								 dump_integrals = False,
	    								 shell_limit = shell_limit )
							ij_suffix_handle.write('{}-{}\n'.format(exp1, exp2))
							self.ijks(exp1=exp1,exp2=exp2,intfile=intfile)
						if self.input_dict['defer']:
							if nofjob == len(new_job) or new_job[nofjob]:
								#self.ijks(expslist=expslist, ntask=ntask)
								ntask += 1
			if self.input_dict['defer']:
				i_index = open( os.path.join(self.WorkDir, 'i.index'),'w')
				if nofjob <= int(os.environ['XCHEM_MAX_NUM_NODES']): suffix = nofjob
				if nofjob > int(os.environ['XCHEM_MAX_NUM_NODES']): suffix = int(os.environ['XCHEM_MAX_NUM_NODES'])
				for i in range(suffix):
					if self.input_dict['saveTwoElInt']:
						ij_suffix_handle.write('{}\n'.format(i+1))
					else:
						i_index.write('{}\n'.format(i+1))
				i_index.close()
				self.job_list.next_block()
				if not self.input_dict['saveTwoElInt']:
					self.job_list.add_job( in_files = { self.WorkDir : [r'pqrs\.int.*', r'i\.index'] }, out_files = {self.DataDir : [r'pqrs\.int$'] } )
					pqrsConcatenate = pqrsConcatenate_input()
					task = self.xchem.task('pqrsConcatenate', pqrsConcatenate )
					self.job_list.add_task(task)
			else:
				if not self.input_dict['saveTwoElInt']:
					shutil.move(os.path.join(self.WorkDir,'pqrs.int'),os.path.join(self.DataDir,'pqrs.int'),)	

		else:
			gateway = gateway_input( os.path.join( data_dirs['basis'], self.SymProject + '.guessorb.h5'), 
									 sdip = True, 
									 monocentric = True ,
									 input_dict = self.input_dict)
			ij_suffix_handle = open( os.path.join(self.DataDir, 'ij.suffix') ,'w')
			# Main Loop for integrals
			osutils.symlink( os.path.join( data_dirs['basis'], 'petiteBasis'), 'petiteBasis' )
			if self.input_dict['defer']:
				new_job = submission_tools.need_new_job( int(lastshell * ( lastshell+1 ) / 2), int(os.environ['XCHEM_MAX_NUM_NODES']))
			# Main Loop for integrals
			for ijob,(jshell, ishell) in enumerate(itertools.combinations_with_replacement( range(1,lastshell+1), r=2 )):
				ij_suffix_handle.write('{}-{}\n'.format(ishell, jshell))
				shell_limit = {
						'lastshell' : lastshell,
						'shell1'      : ishell,
						'shell2'      : jshell,
									}
				if jshell == ishell:
					self.log_message('Consider local shells: i={} and j>={}'.format(ishell,ishell), priority=2)
	
				compressed_file = 'ham2E.compressed.int.{}-{}'.format(ishell,jshell)
				if self.input_dict['defer']:
					if new_job[ijob]:
						self.job_list.add_job(in_files = {self.WorkDir : [r'petiteBasis',r'lk.dat']}, out_files = {self.DataDir : [r'ham2E\.compressed\.int\.\d*-\d*$']} )
					task = self.MOLCAS.task( molcas_input = gateway, Project = self.DesymProject )
					self.job_list.add_task( task )
				else:
					self.MOLCAS.run( gateway, Project = self.DesymProject )
	
				if self.input_dict['defer']:
					task = self.MOLCAS.task( seward,
								             Project = self.DesymProject,
								             dump_integrals = True,
								             shell_limit = shell_limit )
					self.job_list.add_task( task ) 
				else:
					self.MOLCAS.run( seward,
									 Project = self.DesymProject,
									 dump_integrals = True,
									 shell_limit = shell_limit )
				#
				if self.input_dict['defer']:
					compress = compressTwoElectronIntegrals_input( self.input_dict, molcas_integral_file = 'ham2E.[ijkl.suffix].int',
																   compressed_integral_file='{}.[ijkl.suffix]'.format(compressed_file))
					task = self.xchem.task( 'compressTwoElectronIntegrals', compress , iterate_over = 'ijkl.suffix', parallel = True)
					self.job_list.add_task( task )
					task = self.xchem.task( 'twoIntConcatenate',xchem_cmd_line_input='{} ijkl.suffix'.format(compressed_file))
					self.job_list.add_task( task )
				else:
					with open('ijkl.suffix') as f:
						for ijkl in f:
							self.xchem.input_cache = '.xchem.' + ijkl.strip()
							molcas_integral_file = 'ham2E.{}.int'.format(ijkl.strip())
							compress = compressTwoElectronIntegrals_input( self.input_dict, molcas_integral_file = molcas_integral_file,
																		   compressed_integral_file='{}.{}'.format(compressed_file,ijkl.strip()))
							self.proc_list.waitForFreeProc( os.environ['OMP_NUM_THREADS'] ) 
							p = self.xchem.run('compressTwoElectronIntegrals',compress, wait = False )
							self.proc_list.add( p )
						self.proc_list.waitForAllProcs()
						self.xchem.run('twoIntConcatenate',xchem_cmd_line_input='{} ijkl.suffix'.format(compressed_file))
						shutil.move(compressed_file, os.path.join(self.DataDir,compressed_file))
				#
			if not self.input_dict['defer']:
				with open('ijkl.suffix') as f:
					for ijkl in f:
						osutils.rm( '{}.{}'.format(compressed_file,ijkl.strip()) )	
		
		ij_suffix_handle.close()	
		if self.input_dict['defer']: self.job_list.dump( os.path.join( self.DataDir, 'integrals.submission.json'), uid = uid )
		self.dump_meta_data()
	
