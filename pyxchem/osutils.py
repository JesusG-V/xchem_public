#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

import os
import re
import errno
import json
import shutil
from xchem_error import *

def mkdir( path ):
	try: 
		os.makedirs( path )
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise

def echo( file_name, content = None ):
	handle = open( file_name, mode='w')
	handle.write(content)
	handle.close()

def rm( file_name ):
	try:
		os.remove( file_name )
	except FileNotFoundError:
		pass

def rmdir( path ):
	if os.path.isdir(path):
		shutil.rmtree ( path )

def symlink( src, dst ):
	try:
		os.unlink( dst )
	except FileNotFoundError:
		pass
	os.symlink( src, dst )

def concatenate( file_list, output_file = None, parse_fct = None, rm_src = False):
	if output_file:
		with open(output_file,'w')  as fout:
			for fin in file_list:
				fout.write( open(fin,'r').read() )
				if rm_src: 
					rm( fin )
	elif parse_fct:
		for fin in file_list:
			with open(fin,'r') as f:
				for line in f:
					parse_fct(line.strip())
				if rm_src:
					rm( fin )

def write_data_to_json( file_name, dictionary):
	j_data = json.dumps(dictionary, indent = 2)
	with open( file_name, mode='w') as handle:
		handle.write(j_data)

def read_data_from_json( file_name ):
	with open( file_name ) as handle:
		j_raw = handle.read()
		data = json.loads(j_raw)
		return( data )

def read_meta_dict( step ):
	try:
		handle = open( os.path.join(os.environ['XCHEM_DATA_DIR'], '{}.json'.format(step)), mode='r')
	except FileNotFoundError:
		return False
	j_raw = handle.read()
	handle.close()
	input_dict = json.loads(j_raw)
	return(input_dict)

def regexls(path, pattern):
	res = [ f for f in os.listdir(path) if re.search(pattern, f) ] 
	return res

def lnregexls( origin, destination, pattern ):
	for file_name in regexls( origin, pattern ):
		symlink( os.path.join(origin, file_name), os.path.join(destination, file_name) )

def mvregexls( origin, destination, pattern ):
	for file_name in regexls( origin, pattern ):
		shutil.move( os.path.join(origin, file_name), os.path.join(destination, file_name) )

def cpregexls( origin, destination, pattern ):
	for file_name in regexls( origin, pattern ):
		shutil.copy( os.path.join(origin, file_name), os.path.join(destination, file_name) )

def getFilePath( file_name ):	
	if os.path.isfile( os.path.join(os.environ['XCHEM_SUBMIT'], file_name)):
		path = os.path.join(os.environ['XCHEM_SUBMIT'], file_name)
	elif os.path.isfile( file_name ):
		path = file_name
	else:
		raise XchemError('Could not find file: {}'.format(file_name))
	return path

def getDirPath( path_name ):	
	if os.path.isdir( os.path.join(os.environ['XCHEM_SUBMIT'], path_name)):
		path = os.path.join(os.environ['XCHEM_SUBMIT'], path_name)
	elif os.path.isdir( path_name ):
		path = path_name
	else:
		raise XchemError('Could not find directory: {}'.format(path_name))
	return path
