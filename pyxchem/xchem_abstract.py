#***********************************************************************
# This file is part of XChem.                                          *
#                                                                      *
# XChem is free software; you can redistribute it and/or modify        *
# it under the terms of the GNU Lesser General Public License, v. 2.1. *
# XChem is distributed in the hope that it will be useful, but it      *
# is provided "as is" and without any express or implied warranties.   *
# For more details see the full text of the license in the file        *
# LICENSE or in <http://www.gnu.org/licenses/>.                        *
#                                                                      *
# Copyright (C) 2023, XChem Authors                                    *
#***********************************************************************

'''Abstract Classes to define XCHEM modules.

Theses classes are used to define XCHEM modules by defining
their keywords and a run method.
'''
import os, osutils, time, shutil, sys
from xchem_error import *
from submission_tools import *
from glob import glob
from uuid import uuid4
from textwrap import wrap

class XchemKeyword:
	'''Keywords for XCHEM modules
	'''
	def __init__(self, name, optional = False, description = '', data_type = '', is_file = False, default = None, array = False ):
		self._name = name
		self._optional = optional
		self._description = description 
		self._data_type = data_type
		self._is_file = is_file
		self._default = default
		self._array = array

	def print_info( self ):
		print('Keyword: ',self._name)
		print('  Description:')
		wrapped_description=wrap(self.description,width=130)
		for line in wrapped_description:
			print( '    ' + line)
		print('  Data_type: ',self._data_type)
		print('  Optional: ',self._optional)
		print('  Default: ',self._default)
	
	@property 
	def name( self ):
		return self._name

	@name.setter
	def name( self, value ):
		self._name = value

	@property 
	def optional( self ):
		return self._optional

	@optional.setter
	def optional( self, value ):
		self._optional = value

	@property 
	def description( self ):
		return self._description

	@description.setter
	def description( self, value ):
		self._description = value
 	 
	@property 
	def data_type( self ):
		return self._data_type

	@data_type.setter
	def data_type( self, value ):
		self._data_type = value

	@property 
	def is_file( self ):
		return self._is_file

	@is_file.setter
	def is_file( self, value ):
		self._is_file = value

	@property
	def default( self ):
		return self._default

	@default.setter
	def default( self, value ):
		self._default = value
	
	@property 
	def array( self ):
		return self._array

	@array.setter
	def array(self, value):
		self._array = value


def prolog_and_epilog( fct ):
	def wrapped_fct( self, *args, **kwargs ):
		self._prolog()
		return fct(self, *args, **kwargs)
		self._epilog()
	return wrapped_fct

class XchemModule:
	def __init__( self, input_dict, name, data_dir = None, info = False):
		if name: self.name = name
		self.info = info
		self._keywords = {}
		if self.info:
			return
		if name != 'env':
			existing_data = osutils.read_meta_dict( name )	
		else:
			existing_data = False
		self.recycle = False
		if input_dict:
			self.input_dict = input_dict
			self.meta_dict = {}
		else:
			if existing_data:
				self.recycle = True
				self.input_dict = existing_data.pop('input')
				self.meta_dict = existing_data
			else:
				self.input_dict = {}
				self.meta_dict = {}
		self._given_keywords = list(input_dict.keys())
		self.SymProject = 'sym'
		self.DesymProject = 'desym'
		self.job_list = JobList()
		self._out_files = []
		self.proc_list = ProcList()
		if name != 'env' : 
			if data_dir:
				self.DataDir = os.path.join( os.environ['XCHEM_DATA_DIR'],data_dir) 
			else:
				self.DataDir = os.path.join( os.environ['XCHEM_DATA_DIR'],name) 

			if 'XCHEM_TMP_DIR' in os.environ:
				self.WorkDir = os.path.join( os.environ['XCHEM_TMP_DIR'],name,'tmp')
			else:
				self.WorkDir = os.path.join( self.DataDir, 'tmp')

	
	@prolog_and_epilog
	def submit(self, xchem_remote_withId, SubmissionFlags, uid, depends_on_prev_ids = [], only_inputs=False ):
		self.job_list._load( os.path.join( self.DataDir, '{}.submission.json.{}'.format(self.name, uid)))
		if '$' not in xchem_remote_withId:
			raise XchemError('remote_dir most contain some job dependent environment variable. Consider using $XCHEM_JOBID, and make sure to put it in single quotes, to avoid premature expansion.')
		jobids = []
		depends_on_jobids = []
		order = 0
#### Let's try to have different uid even when there are more than one job in a simple job_list
		ntasks=0
		for i,job in enumerate(self.job_list.jobs):
			if order != job.order:	
				depends_on_jobids = jobids.copy()
				jobids = []
				order = job.order
			xchem_remote = xchem_remote_withId.replace('$XCHEM_JOBID', '{}'.format(self.generate_xchemjobid()))
			job_string = self.create_script(job, xchem_remote )
			if order == 0:
				kk=depends_on_prev_ids.copy()
				if only_inputs:
					kk=[0]
				jobid = launch( job_string, flags = SubmissionFlags, depends_on_jobids = kk, script_file_name = '.{}.xchem.{}'.format(self.name, ntasks+uid), only_inputs=only_inputs )
			else: 
				kk=depends_on_jobids.copy()
				if only_inputs:
					kk=[1]
				jobid = launch( job_string, flags = SubmissionFlags, depends_on_jobids = kk, script_file_name = '.{}.xchem.{}'.format(self.name, ntasks+uid), only_inputs=only_inputs )
			jobids.append( jobid )
			time.sleep(0.1)
			ntasks+=1
		return( jobids )

	def _module_specific_checks(self):
		pass
	
	def _check_keywords( self ):
		if self.info:
			self.print_info()
			sys.exit()

		# No need to check keywords if they were automatically read
		# from previous run.
		if self.recycle:
			return

		# Check if keywords are valid
		for kw_name in self.input_dict:
			if not self.valid_keyword( kw_name ):
				raise XchemInputError( 'Invalid keyword: ' + kw_name )

		# Check if non-optional keywords are provided
		for kw_name, kw in self._keywords.items():
			if kw.optional: continue
			if not kw.name in self.input_dict:
				raise XchemInputError( 'Missing required keyword: ' + kw.name )

		# Check if input data typeas are as they are meant to be and cast accordingly
		for kw_name, kw_value in self.input_dict.items():
			if (self._keywords[kw_name].data_type == 'str'):
				try:
					kw_value_new = [ str(v) for v in kw_value ] 
				except ValueError:	
					raise XchemInputError('Input of unpexpected datatype for keyword ' + kw_name )
				self.input_dict[kw_name] = kw_value_new
			elif (self._keywords[kw_name].data_type == 'int'):
				try:
					kw_value_new = [ int(v) for v in kw_value ] 
				except ValueError:	
					raise XchemInputError('Input of unpexpected datatype for keyword ' + kw_name )
				self.input_dict[kw_name] = kw_value_new
			elif (self._keywords[kw_name].data_type == 'float'):
				try:
					kw_value_new = [ float(v) for v in kw_value ] 
				except ValueError:	
					raise XchemInputError('Input of unpexpected datatype for keyword ' + kw_name )
				self.input_dict[kw_name] = kw_value_new
			elif (self._keywords[kw_name].data_type == 'bool'):
				for v in kw_value:
					if v.lower() not in ['t','f','1','0']:
						raise XchemInputError('Input of unpexpected datatype for keyword ' + kw_name )
				kw_value_new = [ True if v.lower() in ['t','1'] else False for v in kw_value ]
				self.input_dict[kw_name] = kw_value_new
			elif (self._keywords[kw_name].data_type == 'custom'):
				kw_value_new = ' '.join( kw_value )
				self.input_dict[kw_name] = kw_value_new

		# Set optional keywords that are not present to their default values (if one exists)
		# Treat present keywords according to wether or not the input allows multiple fields
		for kw_name, kw in self._keywords.items():	
			if kw_name in self.input_dict:
				if not kw.array:
					if len (self.input_dict[kw_name]) > 1: raise XchemInputError('Only one field can be given for ' + kw_name )
					self.input_dict[kw_name] = self.input_dict[kw_name][0]
			else:
				if kw.optional:
					self.input_dict[kw_name] = kw.default

		# Check if file keywords refer to files that exist
		for kw_name, kw, in self._keywords.items():	
			if kw_name in self.input_dict:
				if (kw.is_file and self.input_dict[kw_name]):
					in_file = os.path.join(os.environ['XCHEM_SUBMIT'],self.input_dict[kw_name]) 
					if not os.path.isfile( in_file ):
						raise XchemFileError("Input file {} could not be found".format(in_file))
		
		# Apply module specific checks if _module_specific_checks is overridden.
		self._module_specific_checks()

	def load_previous_meta_data(self, step_list, necessary = True, overwrite = True ):
		if overwrite:
			data_dirs = {}
		for step in step_list:
			if step in ['basis','integrals','orbitals','rdm','qci']:
				data_dir = os.path.join( os.environ['XCHEM_DATA_DIR'],step)
			else:
				data_dir = os.path.join( os.environ['XCHEM_DATA_DIR'],'ccm')
			data_dirs[step] = data_dir
			step_dict = osutils.read_meta_dict( step ) 
			if necessary:
				if not step_dict:
					raise XchemError('Missing data. Check if module "{}" has been run, before running "{}"'.format(step, self.name))
				#else:
				#	continue
			# Update input dict.
			temp_dict = self.input_dict.copy()
			self.input_dict.update( step_dict.pop('input') ) 
			for kw in self._given_keywords:
				self.input_dict[kw] = temp_dict[kw]
			# Update meta dict.
			temp_dict = self.meta_dict.copy()
			self.meta_dict.update( step_dict )
		return( data_dirs )

	@property
	def out_files( self ):
		return self._out_files

	@out_files.setter
	def out_files( self, value ):
		if type(value) is list:
			self._out_files = value
		else:
			raise XchemInternalError('List of output files needs to be a list.')

	def add_out_files( self, out_file ):
		self._out_files.append( out_file )

	def mv_out_files( self ):
		for out_files in self._out_files:
			for f in glob( out_files ):
				shutil.move(f,os.path.join(self.DataDir,f))

	def valid_keyword( self, keyword_name ):
		if keyword_name in self._keywords:
			return True
		else:
			return False

	def has_keyword( self, keyword ):
		if keyword.name in self._keywords:
			return True
		else:
			return False

	def add_keyword( self, keyword ):
		if self.has_keyword( keyword ):
			raise XchemInternalError( 'Doubly defined keyword {}. Please report bug. This is most likely harmless'.format( keyword.name ) )
		else:
			self._keywords[ keyword.name ] =  keyword

	def print_info( self ):
		print( 'Module: ',self.name )
		print( 'Valid Keywords: ')
		for kw_name, kw in self._keywords.items():
			kw.print_info()
			print()

	def dump_meta_data(self):
		meta_dict = self.meta_dict
		meta_dict['input'] = self.input_dict
		osutils.write_data_to_json( os.path.join(os.environ['XCHEM_DATA_DIR'],'{}.json'.format(self.name)), meta_dict )

	def log_message(self, message, priority = 1 ):
		print( ' '*2*(priority-1) +  message)

	def create_script(self, job, XchemRemote):
		job_script =  '#!{}\n'.format( sys.executable )
		job_script += 'import sys, os, subprocess,time, resource\n'
		job_script += 'from glob import glob\n'
		job_script += 'resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY) )\n'
		job_script += 'os.environ["XCHEM"] = \'{}\'\n'.format(os.environ['XCHEM'])
		job_script += 'os.environ["XCHEM_PRINT"] = \'{}\'\n'.format(os.environ['XCHEM_PRINT'])
		job_script += 'os.environ["MOLCAS"] = \'{}\'\n'.format(os.environ['MOLCAS'])
		job_script += 'os.environ["MOLCAS_MEM"] = \'{}\'\n'.format(os.environ['MOLCAS_MEM'])
		job_script += 'os.environ["OMP_NUM_THREADS"] = \'{}\'\n'.format(os.environ['OMP_NUM_THREADS'])
		job_script += 'sys.path.append(os.path.join(os.environ["XCHEM"],"pyxchem"))\n'
		job_script += 'from molcas_manager import MolcasManager\n'
		job_script += 'from xchem_manager import XchemManager\n'
		job_script += 'import osutils\n'
		job_script += 'import submission_tools\n'
		job_script += 'WorkDir = \'{}\'\n'.format(self.WorkDir)
		job_script += 'DataDir = \'{}\'\n'.format(self.DataDir)
		if job.work_in_place:
			job_script += 'os.chdir( DataDir )\n'
			job_script += 'MOLCAS = MolcasManager( WorkDir = DataDir, MOLCAS_OUTPUT = "WorkDir" )\n'
		else:
			job_script += 'XchemRemote=os.path.expandvars(\'{}\')\n'.format(XchemRemote) 
			job_script += 'osutils.mkdir( XchemRemote )\n'
			job_script += 'os.chdir( XchemRemote )\n'
			job_script += 'MOLCAS = MolcasManager( WorkDir = XchemRemote, MOLCAS_OUTPUT = "WorkDir" )\n'
		job_script += 'xchem = XchemManager()\n'
		if not job.work_in_place:
			for directory in job.in_files:
				for pattern in job.in_files[directory]:
					job_script += 'osutils.lnregexls( \'{}\', \'.\', \'{}\' )\n'.format( directory, pattern)
		for task in job.tasks:
			if task['parallel']:
				job_script += 'proc_list = submission_tools.ProcList()\n'
			if task['package'] == 'molcas':
				job_script += 'MOLCAS.run(\'{}\',Project=\'{}\',dump_integrals={},shell_limit={})\n'.format(task['molcas_input'].replace('\n','\\n'), task['Project'], task['dump_integrals'], task['shell_limit'] )
			elif task['package'] == 'xchem':
				if type(task['iterate_over']) is str:
					job_script += 'with open(\'{}\',"r") as f:\n'.format(task['iterate_over'])
					job_script += '    for line in f:\n'
					if task['xchem_file_input']:
						job_script += '        xchem_file_input = \'{}\'.replace(\'{}\',line)\n'.format(task['xchem_file_input'].replace('\n','\\n'), '[{}]'.format(task['iterate_over']))
					else:
						job_script += '        xchem_file_input = None\n'
					if task['xchem_cmd_line_input']:
						job_script += '        xchem_cmd_line_input = \'{}\'.replace(\'{}\',line)\n'.format(task['xchem_cmd_line_input'].replace('\n','\\n'), '[{}]'.format(task['iterate_over']))
					else:
						job_script += '        xchem_cmd_line_input = None\n'
					if task['parallel']:
						job_script += '        xchem.input_cache = \'.xchem.\' + line.strip()\n'
						job_script += '        proc_list.waitForFreeProc( os.environ[\'OMP_NUM_THREADS\'] )\n'
						job_script += '        p = xchem.run( program = \'{}\', xchem_file_input = xchem_file_input, xchem_cmd_line_input = xchem_cmd_line_input, parsefct = {}, xchem_file_input_name = {}, wait=False )\n'.format(
								   task['program'],
								   task['parsefct'],
								   task['xchem_file_input_name'])
						job_script += '        proc_list.add( p )\n'
						job_script += 'proc_list.waitForAllProcs( )\n'
						
					else:
						job_script += '        xchem.run( program = \'{}\', xchem_file_input = xchem_file_input, xchem_cmd_line_input = xchem_cmd_line_input, parsefct = {}, xchem_file_input_name = {})\n'.format(
								   task['program'],
								   task['parsefct'],
								   task['xchem_file_input_name'])
				elif type(task['iterate_over']) is list:
					for i in list:
						if task['xchem_file_input']:
							job_script += 'xchem_file_input = \'{}\'\n'.format( task['xchem_file_input'].replace('\n','\\n').replace('[{}]'.format(task['iterate_over']), i))
						else:
							job_script += 'xchem_file_input = None\n'
						if task['xchem_cmd_line_input']:
							job_script += 'xchem_cmd_line_input = \'{}\'\n'.format( task['xchem_cmd_line_input'].replace('\n','\\n').replace('[{}]'.format(task['iterate_over']), i))
						else:
							job_script += 'xchem_cmd_line_input = None\n'
						job_script += 'xchem.run( program = \'{}\', xchem_file_input = xchem_file_input, xchem_cmd_line_input = xchem_cmd_line_input, parsefct = {}, xchem_file_input_name = {})\n'.format(
									   task['program'],
									   task['parsefct'],
									   task['xchem_file_input_name'])
				else:
					if task['xchem_file_input']:
						if type( task['xchem_file_input'] )  is str:
							job_script += 'xchem_file_input = \'{}\'\n'.format( task['xchem_file_input'].replace('\n','\\n'))
						elif type( task['xchem_file_input'] ) is list:
							job_script += 'xchem_file_input = {}\n'.format( [ f for f in task['xchem_file_input'] ] ) 
						else:
							raise XchemInternalError()
					else:
						job_script += 'xchem_file_input = None\n'
					if task['xchem_cmd_line_input']:
						job_script += 'xchem_cmd_line_input = \'{}\'\n'.format( task['xchem_cmd_line_input'].replace('\n','\\n'))
					else:
						job_script += 'xchem_cmd_line_input = None\n'
					job_script += 'xchem.run( program = \'{}\', xchem_file_input = xchem_file_input, xchem_cmd_line_input = xchem_cmd_line_input, parsefct = {}, xchem_file_input_name = {})\n'.format(
								   task['program'],
								   task['parsefct'],
								   task['xchem_file_input_name'])
			else: 
				raise XchemInternalError()
		if not job.work_in_place:
			for directory in job.out_files:
				for pattern in job.out_files[directory]:
					job_script += 'osutils.mvregexls( \'.\', \'{}\', \'{}\' )\n'.format( directory, pattern)
			job_script += 'osutils.rmdir( XchemRemote )\n'
		return(job_script)

	def generate_xchemjobid(self):
		return(uuid4())

	def _prolog(self):
		self._start_time = time.time()
		statement = 'PYXCHEM: Starting module {} at time {}'.format(self.name, time.asctime())
		print( '-'*len(statement))
		print( '-'*len(statement))
		print( statement )
		print( '-'*len(statement))
		print( '-'*len(statement))
		print( ' ' )

	def _epilog(self):
		self._end_time = time.time()
		statement = 'PYXCHEM: Module {} finished at time {}'.format(self.name, time.asctime())
		print( '-'*len(statement))
		print( '-'*len(statement))
		print( statement )
		print( 'Time elapsed= {:10.5f} seconds'.format(self._end_time-self._start_time)) 
		print( '-'*len(statement))
		print( '-'*len(statement))
		print( ' ' )
