.. _ccm:

****
&ccm
****

This module computes the matrix elements between the source states and all possible augmented states (i.e. all states obtained by augmenting the parent ions with another electron of angular momenta included in the close coupling expansion, in either Gaussian or B-spline orbitals). 

Parameters
##########

* **closeCouplingChannels** Identical to the same keyword in the module *bsplines*; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, as the ccm module may become quite costly for large close coupling expansions. 
* **operators** Specify which matrix elements to compute. Valid choices are H (for the Hamiltonian), S (for the Overlap), CAP (for the CAPs, if included) and Dipole. Default: H S Dipole
* **axes** If the dipoles are required, select which components should be calculated. Valid choices are x, y, and z. Default: x y z.
* **gauges** If the dipoles are requested, select which gauges to compute them in. Valid choices are v (for velocity) and l (for length). Default: l v.
* **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
* **brasym**
* **cond**
* **ketsym**

