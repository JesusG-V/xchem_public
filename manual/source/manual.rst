.. _manual:

******
Manual
******

This chapter outlines the modules and their parameters that may be included in the input file for an XCHEM calculation. The structure of the input file was outlined in :ref:`input_file`. Figure :numref:`flow` shows the dependency of the different modules on each other. 

.. _flow:
.. figure:: pics/flow.png

Flow chart showing the dependencies among the different modules making up XCHEM. The dependency of *scatteringstates* on *boxstates* (dotted arrow) may be ignored if the parameters *cm* in *scatteringstates* is set to *hsle*. The *plot* module will produce plots, as long as at least one of the dependencies indicated by the blue arrows is fulfilled.

:ref:`basis`

:ref:`orbitals`

:ref:`integrals`

:ref:`rdm`

:ref:`qci`

:ref:`bsplines`

:ref:`ccm`

:ref:`boxstates`

:ref:`scatteringstates`

:ref:`dta`

:ref:`plot`

:ref:`preptdse`
