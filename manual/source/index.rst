.. Xchem documentation master file, created by
   sphinx-quickstart on Mon Sep 14 10:43:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. figure:: pics/xchem.png

Welcome to Xchem's documentation!
===================================

.. toctree::
   :maxdepth: 2

   overview
   theory
   installation
   howto
   manual
   basis
   orbitals
   integrals
   rdm
   qci
   bsplines
   ccm
   boxstates
   scatstates
   dta
   plot
   preptdse
   guide
   references
