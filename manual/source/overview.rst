********
Overview
********

The \XCHEM code comprises a set of tools allowing the computation of scattering states of complex molecules, with the goal of studying photoionization processes ejecting no more than one electron from such systems. Currently *complex molecules* refers to multi electronic diatomic systems (for instance N\ :sub:`2` and O\ :sub:`2`), and small poly-atomic systems (for instance Water). Though future releases of this code will increase these limitations on the size of the molecules, which are due primarily to requirements made on computational resources. 

Core Ideas
##########

The philosophy of the \XCHEM code may be thought of as the combination of a few of the core aspects used to generate said scattering states. To understand these aspects we recall that, using the close coupling formalism of scattering theory, we may express a scattering state :math:`\Psi_{\alpha E}` of a molecular system (with N\ :sub:`e` electrons) as (for details see :cite:`Marante2014,Marante2017,Marante2017_2` and references therein)

.. math::
   :label: xchemCore

	\Psi^{-}_{\alpha E}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e})&=&\sum_i c_{i,\alpha E}\aleph_{i}+\sum_{\beta,i}N_{\alpha i}\hat{\mathcal{A}}\Upsilon_{\beta}c_{\beta i,\alpha E}\quad\mathrm{where}\\

.. math::
   :label: channelFct
   
        \Upsilon_{\alpha}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1};\hat{\mathbf{r}}_{N_e},\zeta_{N_e})&=&^{2S+1}\left[\Phi_{a}(\mathbf{x}_1,\cdots,\mathbf{x}_{N_e-1})\otimes\chi(\zeta_{N_e})\right]_{\Sigma}X_{l,m}(\hat{\mathbf{r}}_{N_e})\\

where :math:`\aleph_i` denote *short range states* with electrons in bound orbitals, :math:`\Upsilon_{\alpha}` denote the *channel functions* built from cationic states coupled to the spin and orbital angular momentum of the ejected electron, :math:`\phi_i` denotes the radial component of the ejected electron and :math:`\alpha` is the channel index specifying the cationic state :math:`\Phi_{a}`, the quantum numbers :math:`l` and :math:`m` of the ejected electron and the total and projected spin angular momenta :math:`S` and :math:`\Sigma`. Of the terms in equations :eq:`xchemCore` and :eq:`channelFct`, :math:`\phi_{i}` is the only term inherently dependent on the oscillatory behaviour of the continuum electron. 

The short range states :math:`\aleph_i`, as well as the cationic state :math:`\Phi_{\alpha}`, contained in the definition of the channel functions, are described exclusively in terms of electrons in bound orbitals. Therefore they may be computed with the standard tools of quantum chemistry (QC), computationally implemented in quantum chemistry packages such as Molcas :cite:`aquilante2016molcas` and Molpro :cite:`MOLPRO_brief`. These (and others) have in common, that the electronic wavefunction is expressed in terms of orbitals expanded in sets of Gaussian basis functions centred at the atomic sites of the molecular system; the so called polycentric Gaussian (PCG) basis sets. 

In contrast to that, a similar approach is poorly suited for the :math:`\phi_i(r_{N_e})`-term of equation :eq:`xchemCore` due to problems associated with PCGs in expressing the long range behaviour of continuum electrons :cite:`Marante2014,Marante2017`. Consequently a hybrid set of B-Spline basis functions and an auxiliary set of monocentric Gaussian (MCG) basis functions, centred at the centre of mass of the the molecule, is used for the description of the continuum electron. This basis set is referred to as a GABS Basis :cite:`Marante2014`.

This approach yields a key advantage inherent in the design of the GABS and PCG basis sets, namely a vanishing overlap between B-Splines and PCGs. This significantly simplifies the evaluation of integrals allowing for the application of the \XCHEM code to rather complex systems. This combination of using PCG-based QCPs (for the evaluation of :math:`\aleph_i` and :math:`\Phi_{\alpha}`) and interfacing them with a GABS based approach (for the representation of :math:`\phi_{i}`) to construct physically admissible scattering states [1]_ is the approach defining the \XCHEM code. More details about the components comprising the \XCHEM code may be found in the next chapter.

.. [1] obtained as solutions to the generalized eigenvalue problem :math:`(\mathbf{H}-\mathbf{S}E)\mathbf{c}=0` (where :math:`\mathbf{H}` and :math:`\mathbf{S}` are the Hamiltonian and overlap matrices states obtained by augmenting the :math:`\Phi_a` of equation :eq:`channelFct` with an additional electron), with physicality of the solution being ensured by enforcing correct asymptotic behaviour :cite:`Marante2017`

