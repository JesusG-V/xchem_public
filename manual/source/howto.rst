***********
Using XCHEM
***********

Executing XCHEM
###############
To run XCHEM in the most straightforward way requires only execution of the following command:

::

    <PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE>
 
The following three sections outline the general structure of the input file, the structure of the results, as well as the modifications that need to be made to the workflow if working on a cluster.

.. _input_file:

Structure of the input file
###########################

The input file defines all steps that will be carried out by an XCHEM calculation. The input file requests different modules by including 

::	

    &<MODULE_NAME>
    param1=val
    param2=val1 val2 
    # lines starting with #,* or ! are treated as comments.

where the parameters following the module name, modify the behaviour of the given module. For a detailed list of available modules and their parameters, see chapter :ref:`manual`. 

Meta modules
************

The modules  

::

       &exit

and 

::

       &nowait

may be used to modify the flow of program. If the former is encountered at any point, the calculation halts. The latter may be used to specify modules to be run in parallel if working on a cluster. For more details see section :ref:`on_clusters`.

.. _output:

Structure of the produced data
##############################

By default all data produced by the XCHEM calculation is saved in the directory:

::

    $PWD/XCHEM_data/*

In it the modules save those results that are relevant for subsequent calculations in different sub-directories. Data produced during the execution, that is not relevant for subsequent modules is saved in 

::

    $PWD/XCHEM_data/<MODULE_DIR>/tmp

These directories may be deleted upon completion of each individual module. It is recommendable to do so, as these may become quite large. 

.. _on_clusters:

Working on a cluster
####################

While it is possible to treat small systems, with a limited range of angular momenta on a single node, it is likely that larger calculations using XCHEM need to be run on a cluster. **The current distribution assumes availability of the slurm workload manager.** XCHEM facilitates this by allowing expensive calculations to be executed in two steps: a first cheap step (referred to as setup, see section :ref:`setup` run locally, and a second more expensive step (referred to as submission, see section :ref:`submission`) distributed over several nodes. The parameter **defer**, available to all but the basis module is used to control which parts of the calculation to submit.

The "defer" parameter
*********************

If defer is set to true in some module, the expensive parts of that module and all subsequent modules are not run immediately in their entirety, but rather are prepared for later submission. **Note**: Mixing defer=T and defer=F should be done only with great care, as a module run with defer=F, assumes that all modules whose data it depends on have finished in their entirety.

.. _setup:

Setup
*****

The execution of the setup step is identical to a simple, local run of XCHEM:
::

	<PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE>

Afterwards, the directory structured described in section :ref:`output` will have already been created. 

.. _submission:

Submission
**********

To submit one or all modules, marked as deferred execute:

::

	<PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE> --submit <MODULE> --remote_dir '<REMOTE_PATH>' --submission_flags '<FLAGS>'

This will trigger the submission of a series of jobs with dependencies among them set to ensure that they run in the correct order. **NOTE: The input file must not be modified between the setup and the submission step**. The command line parameters have the following functions:

#. :math:`<` **MODULE** :math:`>` Chose which module to submit. "all" may be used to submit all modules marked for submission.
#. :math:`<` **REMOTE_PATH** :math:`>` On most clusters each node has a local scratch space that should be used for read and write operations during the execution of some jobs. This flag may be used to specify this directory. **Note:** the path for each job should be unique, for this purpose the path should be given in single inverted comas to avoid premature expansion of environment variables such as $SLURM_JOBID.
#. :math:`<` **FLAGS** :math:`>` Flags to be passed to sbatch, to specify number of processors, quality if services, requested computational time etc.

Only_inputs
***********

It writes all the inputs of the module specified and a script to execute them. This adds more flexibility to run the different jobs.

::

        <PATH_TO_XCHEM_INSTALLATION>/pyxchem/pyxchem.py <INPUT_FILE> --submit <MODULE> --only_inputs


&nowait
*******
The &nowait meta module may be used to permit the concurrent execution of several modules. If placed between two modules the jobs corresponding to the second module will not wait for completion of those associated with the first module. This is primarily useful to request simultaneous execution of the ccm module with different close coupling expansions or simultaneous execution of the scatteringstates module with different energy ranges (see chapter :ref:`manual` for more details on the modules).
