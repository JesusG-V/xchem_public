.. _rdm:

****
&rdm
****

The *rdm* (reduced density matrix) module computes the one and two electron density matrices between all source states and states resulting from augmentation of parent ions with an additional electron, in either an active polycentric Gaussian orbital or a diffuse monocentric Gaussian orbital.

Parameters
##########

*  **nact** Define the number of electrons in the system. The keyword accepts up to three numbers. The first specifies the number of electrons in the ras2 space. The second and third specify the number of holes and electrons in the ras1 and ras3 spaces, respectively. 
*  **symmParentStates** Define groups of parent ions by specifying their symmetry. The same symmetry may appear multiple times to specify, for example, two sets of states in :math:`B_{1u}` symmetry with different multiplicities.
*  **spinParentStates** Define the multiplicity for each group defined in symmParentStates.

*  **ciroParentStates** Specify which or how many states to include for each group of states. If an integer :math:`i` is provided, Molcas will be used to create the CI vectors corresponding to the :math:`i` energetically lowest states of given symmetry and multiplicity. It will do so without re-optimising the orbitals. If a string :math:`s` is given the code will try to read the CI vectors (expressed in determinants) from the file :math:`s`. 

*  **coreHoleParentIon** Use only in combination with providing integers to ciroParentStates. For each group specify if in calculating the CI vectors Molcas' hexs keyword should be applied. Useful for the treatment of core hole states (see Molcas documentation for more details). Default: False
*  **spinAugmentingElectron** Specify the spin of the electron to be augmented with as either "alpha" or "beta".
*  **spinAugmentedStates** Specify the multiplicity of ALL source states. Note that as currently treatment of spin orbit coupling is not included, there is no need to specify source states of different multiplicity as they have no chance of coupling. 

* **nameSourceStates** Specify the name of the all source states.
*  **symmSourceStates** Equivalent to symmParentStates, but used to specify short range states with one more electron compared to the parent ion states. Default: None
*  **ciroSourceStates** Equivalent to ciroParentStates, but used to specify short range states with one more electron compared to the parent ion states. Default: None
*  **coreHoleSource** Equivalent to coreHoleParentIon, but used to specify short range states with one more electron compared to the parent ion states. Default: False

*  **gugaDir** The computation of guga tables may be expensive for active spaces that contain in excess of :math:`10^5` configuration state functions. As the structure of the guga table only depends on the active space, and not on the geometry, orbitals, or choices of states, the name of a directory may be specified, containing the relevant files of guga tables. Typically this would be used if treating the same system for multiple geometries. In that case the file `*.guga` should be save from the rdm/tmp directory and the path be provided to this keyword. Default: None
*  **forceC1** Treat all augmented states to be of C1 symmetry. This does not mean the states orbitals lose their symmetry, only that in calculating matrix elements, their symmetry is not considered. At present it is not recommended to change this keyword from its default value. Default: True
*  **maxMultipoleOrder** Define the maximum multiple order to be computed for later use in the multipole expansion carried out for the computation of the Hamiltonian matrix including B-spline orbitals. Default: 6.
*  **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.

