*******************
Guide to Parameters
*******************

Choosing the Bases
******************

As pointed out previously the basis may be seen as composed of polycentric Gaussians, monocentric Gaussians and B-splines. Making good choices for these basis sets is crucial for obtaining good results in acceptable computational time. The following three paragraphs outline the considerations one should make for each of these basis sets.

.. _choosepcg:

Polycentric Gaussians
#####################

The polycentric Gaussians take care of the description of the short range structure of parent ion states and source states. As such a basis set should be chosen that is flexible enough to give a good description of those states. Care should be taken not to include very diffuse polycentric Gaussians, as that would require increasing :math:`R_0` which in turn requires the use of a large monocentric Gaussian basis. 

As a method to check the viability of the PCGs used, it is recommendable to plot or otherwise inspect the radial behaviour of the active space orbitals constructed from PCGs, as exemplified in figures :numref:`rad1` and :numref:`rad2`.

Monocentric Gaussians
*********************

For most purposes the default values that define the exponents used in the monocentric Gaussian basis, need not be modified. The key parameters in choosing the monocentric Gaussian basis are *lMonocentric and *kMonocentric*. In general smaller values of :math:`l` require larger values of :math:`k` to ensure the same radial flexibility. Therefore for applications requiring a small maximum value for :math:`l\leq3` (e.g. small molecules with high symmetry), it is usually sufficient to choose the same maximum value for :math:`k` for every :math:`l` (see :ref:`basis` for details). For more complex systems it may be useful to choose large values of :math:`k` (e.g. :math:`3`) for small :math:`l` (e.g. :math:`\leq2`) and smaller values for :math:`k` (e.g. :math:`0`) for larger l :math:`l\geq 4`. Furthermore the need for higher angular momenta increases for scattering states of larger energies; in figure :numref:`artifacts` the unphysical slow oscillations around the correct cross sections are a consequence of insufficient *lMonocentric* at the affected energies. 

B-splines
*********

The key parameters to set up the B-spline basis are *rmin*, *rmax* and *numberNodes*. 

rmin
####

Finding a good choice for *rmin* follows essentially the path outlined in section :ref:`choosepcg`. Negligible overlap between the B-splines and active space orbitals, used to express the source and parent ion states, must be ensured. Figure :numref:`rad2` and :numref:`rad1` show how a choice for *rmin*, guaranteing neglible overlap, may be made.

rmax
####

*rmax* must be chose large enough to avoid the following complication: computing scattering states just above an ionisation threshold corresponds to ejection of electrons with a very long wavelength. For an accurate representation of the scattering states of a given energy it must be ensured that at least a few oscillations of the electronic wave function fit into the box demarcated by *rmax*. Furthermore, if using a complex absober, *rmax* may need to be increased to accomodate it.

numberNodes
###########

The number of nodes determines the density of B-splines. It should be ensured that the chose B-spline basis is capable of representing the oscillations of an electron ejected at the maximum kinetic energy included in the calculation; in figure :numref:`artifacts` the unphysical fast oscillations around the correct cross sections are a consequence of insufficient *numberNodes* at the affected energies.

.. _rad1:
.. figure:: pics/rad1.png

   Radial behaviour of active space orbitals of a calculation for NO:math:`_2`. In choosing the PCGs, the active space, and :math:`R_0` a compromise should be aimed for that allows choosing :math:`R_0`, so that no significant part of the active PCG orbitals is allowed to overlap with B-Splines. In this example :math:`R_0=7` would be a reasonable choice, given that more than 99.9\% of each of the active space orbitals is contained to within a radius of :math:`7` atomic units.

.. _rad2:
.. figure:: pics/orbs_pyrazine.png

   ISO surface of an active space orbital of pyrazine cropped to different box sizes. The interior of the manifold contains 99.9\% of the depicted orbital. Left: the manifold cropped to a box of 8 a.u.; it is fully contained in the box. Right: the manifold cropped to a box of 7 a.u.; protruding sections are cut away. A similar analysis of all active space orbitals, would allow choosing an appropriate value for :math:`R_0`.

.. _artifacts:
.. figure:: pics/h.jpg

    Comparison of photoionization cross section of atomic hydrogen obtained analytically (black) and with XCHEM (coloured). The effects of poorly chosen parameters for the basis sets manifest themselves in different ways: In sufficient angular momentum in the monocentric Gaussian basis leads to slow unphysical oscillations around the correct solution, it can be seen clearly that increasing :math:`l` remedies this behaviour. Insufficient density of B-splines leads to jagged, fast oscillation before incorrectly predicting zero cross section. Increasing the B-spline density would yield smooth curves.

