.. _integrals:

**********
&integrals
**********

This module calculates the relevant two electron integrals for monocentric and polycentric Gaussian basis functions. In many cases this module will account for a considerable percentage of total computational time. Its cost grows rather quickly for increasing values of kMonocentric and lMonocentric. Running the calculation on a single node it is not recommendable to have values larger than :math:`3` and :math:`2` for kMonocentric and lMonocentric, respectively. Across multiple nodes value of lMonocentric:math:`=6` and kMonocentric:math:`=1` are possible but may take several days even if run on architectures with :math:`10` nodes of :math:`16` processors each. 

Parameters
##########
*  **defer** Defer as much of the calculation as possible, to be executed later, distributing the workload among multiple nodes.

