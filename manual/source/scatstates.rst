.. _scatteringstates:

*****************
&scatteringstates
*****************

This module computes scattering states of the system by fitting to the asymptotically correct solution. It also compute the relative scattering phases by diagonalising the S matrix. 

Parameters
##########

* **closeCouplingChannels** Identical to the same keyword in the module bsplines; if not given the close coupling expansion defined in that module will be used. It may be useful to work with a subset of what was defines in the bsplines module, to investigate how the coupling of different channels is reflected in the scattering states. 
* **emin** Choose the lowest absolute energy in atomic units, of the scattering states to be computed.
* **emax** Choose the highest absolute energy in atomic units, of the scattering states to be computed. **WARNING:** emin and emax **may not** lie on either side of an ionisation threshold, i.e. an energy at which some channels change from closed to open. The module should be requested multiple times with different values for emin and emax for the analysis of scattering states between different pairs of ionisation thresholds.
* **ne** Number of scattering states to compute between emin and emax.
* **cm** Method used to compute the scattering states, i.e. finding :math:`M` non-trivial solutions to the equation :math:`(\mathbf{H}-E\mathbf{S})\Psi = 0`, where :math:`M` is the number of open channels at energy :math:`E`. Valid choices are: hsle (solve homogeneous system using LU decomoposition :math:`\mathbf{H}-E\mathbf{S}=\mathbf{L}\mathbf{U}`), invm (write :math:`(\mathbf{H}-E\mathbf{S})\Psi = 0` as :math:`[\mathbf{A}|\mathbf{B}]=\left[\frac{\tilde{\Psi}}{-\mathbf{I}}\right]` and invert :math:`\mathbf{A}` to obtain :math:`\Psi = \mathbf{A}^{-1}\mathbf{B}`), and spec (invert :math:`A` of invm method more efficiently by writing :math:`\mathbf{A} = \mathbf{\Phi}(\mathbf{E}-E)\mathbf{\Phi}^{\dagger}`), where :math:`\mathbf{\Phi}` and :math:`\mathbf{E}` are the matrix of box eigenstates and diagonal matrix of box eigenvalues, respectively. Default: hsle. 

* **scattConditionNumber** Conditioning number used to remove linear dependencies in calculating scattering states. Default= :math:`10^{-10}`
* **loadLocStates** See module boxstates.
* **defer** Defer as much of the calculation as possible to be executed later, distributing the workload among multiple nodes.
* **cond**
* **numBsDropBeginning**
* **conditionBsThreshold**
* **conditionBsMethod**
* **numBsDropEnding**
* **conditionNumber**
* **brasym**
