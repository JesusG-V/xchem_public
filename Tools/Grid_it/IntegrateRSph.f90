!***********************************************************************
! This file is part of XChem.                                          *
!                                                                      *
! XChem is free software; you can redistribute it and/or modify        *
! it under the terms of the GNU Lesser General Public License, v. 2.1. *
! XChem is distributed in the hope that it will be useful, but it      *
! is provided "as is" and without any express or implied warranties.   *
! For more details see the full text of the license in the file        *
! LICENSE or in <http://www.gnu.org/licenses/>.                        *
!                                                                      *
! Copyright (C) 2023, XChem Authors                                    *
!***********************************************************************

program IntegrateRSph

implicit none

real*8                  rMin,rMax,rDif
real*8, allocatable  :: Density(:,:),val(:)
integer                 nDist,nOrbs,iOrb,iDist
character*400        :: OutputFname,kkStr
character(len=:), allocatable :: Fname,InputFName

write(*,*) 'Name of the input file'
read(*,*) kkStr
allocate(InputFname, source = trim(kkStr))
write(*,*) 'Number of R distances, min and max'
read(*,*) nDist,rMin,rMax
write(*,*) 'Number of orbitals'
read(*,*) nOrbs
write(*,*) 'Name of the output file'
read(*,*) OutputFname
allocate(Density(nOrbs,nDist),val(nOrbs))
rDif = (rMax-rMin)/(nDist-1)
do iDist = 1, nDist
  if (allocated(Fname)) deallocate(Fname)
  write(kkStr,*) iDist-1
  allocate(Fname, source = trim(InputFname)//trim(adjustl(adjustr(kkStr))))
  open(1,file=trim(Fname),status='old',form='formatted')
  do iOrb = 1, nOrbs
    read(1,*)
    read(1,*) Density(iOrb,iDist) 
  enddo
  close(1)
enddo

open(1,file=trim(OutputFname),status='replace',form='formatted')

do iDist=1,nDist
  if (iDist .eq. 1 ) then
    do iOrb=1,nOrbs
      val(iOrb) = Density(iOrb,iDist)*(1.0/3.0)*( (rDif*(iDist-0.5))**3 )
    enddo
    write(1,*) rDif*(iDist-0.5),(1.0-val(iOrb),iOrb=1,nOrbs)
  else
    do iOrb=1,nOrbs
      val(iOrb) = val(iOrb) + (Density(iOrb,iDist)*(1.0/3.0)*( (rDif*(iDist-0.5))**3 - (rDif*(iDist-1.5))**3 ))
    enddo
    write(1,*) rDif*(iDist-0.5),(1.0-val(iOrb),iOrb=1,nOrbs)
  endif
enddo

end program
