# XCHEM EXAMPLE

## Hydrogen - Small

The input here provided, shows a slighly larger calculation than hydrogen\_tiny. It should take around 15 min on 1 processor. It includes a larger Gaussian basis
( higher maximum angular momenta, more exponents ), more parent ions, more B-splines and two additional close coupling channels. The increase in computational
time is primarily due to the larger Gaussian basis.

Details of changes with respect to hydrogen\_tiny

* Includes angular values for k,l=(0 0, 1 0) and uses default exponents (nexp = 22). Multighost indicates the use of different gaussian basis sets for different l, with
an optimized set of contraction coefficents:
	* lMonocentric = 1
	* kMonocentric = 0
	* multighost = t

* Include two parent ions
	* ciroParentStates = 2

* Include more B-splines (reaching further)
	* rmax = 100
	* numberNodes=200

* Include more close coupling channels
	* closeCouplingChannels=1 (0 0, 1 0); 2 (0 0)
