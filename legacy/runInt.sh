#!/bin/bash

n=$(echo -n "$SLURM_JOB_ID" | wc -c )
if [ $n -gt 0 ]; then
	SLRM_CMD=$(scontrol show jobid=$SLURM_JOB_ID | awk '/Command=/' | sed 's/Command=//g' | awk '{print $1}')
	LINK=$(readlink -f $SLRM_CMD)
else
	LINK=$(readlink -f $BASH_SOURCE)
fi
SCRIPT=${LINK##*/}
BASE=${SCRIPT%.*}
INSTALLPATH=${LINK%/*}
. $INSTALLPATH/config.sh

OVERWRITE=0
BREAK=0
log=$CurrDir/log
if [ -e $log ]; then
	for (( i=1 ; i<=9999 ; i++ )); do
		if [ ! -e ${log}_$i ]; then	
			log=${log}_$i
			break
		fi 
	done
fi
ulimit -s unlimited
while getopts ":i:rphl:" opt; do
        case $opt in
		h) 	one="$INSTALLPATH/man/$BASE"
		   	two="$INSTALLPATH/man/general"
		   	cat $one $two | less
		   	exit ;; \
                i) 	IN=$OPTARG ;; \
		:) 	echo "missing argument for (i)nput"
			exit ;; \
		r) 	OVERWRITE=1 ;; \
		p) 	BREAK=1 ;; \
		l) 	log=$CurrDir/$OPTARG ;; \
		:) 	echo "missing argument for (l)og"
			exit ;; \
		[?]) 	echo "unknwon option -$OPTARG"
			exit 1 ;; \
        esac
done
export log=$log
export OVERWRITE=$OVERWRITE
export IN=$IN

if [ -e $log ]; then
	rm -f $log
fi
echo "------------------" > $log
echo "runMonopoly log file" >> $log
echo "------------------"  >> $log
if [ -z $IN ]; then
	echo "no (i)nput file provided. STOP" >> $log
	exit
fi
if [ ! -e $IN ]; then
	echo "(i)nput file does not exist. STOP" >> $log
fi

if [ $OVERWRITE -eq 1 ]; then 
	echo "ove(r)writing previous output" >> $log
else
	echo "keeping previous output" >> $log
fi
. $INSTALLPATH/processInput.sh 
. $INSTALLPATH/setupTwo.sh

MOLCAS_MEM_TEMP=$MOLCAS_MEM
export MOLCAS_MEM=2000

#CREATE INPUT FILES
if [ ! -e $intDir/.cp001 ]; then
	echo "Creating Input Files" >> $log
	if [ ! -z $lkMonocentricFile ]; then
		cp $lkMonocentricFile $intTempDir/lk.dat
	fi
	$xchemSrc/createInputInt.exe $intInputDir $IN >> $intTempDir/createInputInt.out
	check $intTempDir/createInputInt.out
	echo "input files created" > $intDir/.cp001
else
	echo "input files already exist" >> $log
fi
if [ $BREAK -eq 1 ]; then	
	echo "runLocal.sh executed with -p. Script stopped to allow user to verify input files." >> $log
	echo "To resume run runLocal.sh without -p and -r options." >> $log
	exit
fi


#MONPO GATEWAY
cd $intTempDir
echo "Extract information about monpo basis for later calculations" >> $log
export Project="monopoly"
export WorkDir="$PWD/TMP-$Project"
if [ ! -d $WorkDir ]; then
	mkdir $WorkDir
fi
echo "" >> $log

next=$(skip gateway.monpo.cas.out)
echo "RUNNING gateway.monpo.sym.cas to obtain" >> $log
echo "  -integrals one electron for monocentric and polycentric basis functions" >> $log

if [ $next -ne 0 ]; then
	echo "one electron seward already done -> skip" >> $log	
else
    #Work around until EMIL gets updated
	if [ ! -z $lkMonocentricFile ]; then
		lMonocentric=$(awk 'BEGIN{l=-1}NR>1{if($1>l){l=$1}}END{print l}' lk.dat )
		kMonocentric=$(awk 'BEGIN{k=-1}NR>1{if($2>k){k=$2}}END{print k}' lk.dat )
	fi
	for (( l=0; l<= $lMonocentric; l++)); do
		for (( k=0; k<= $kMonocentric; k++)); do
			export lll=$l
			export kkk=$k
			awk -v pat="l.${l}_k.${k}.bas" '$0 ~ pat{system("cat $ghostLibrary/l.${lll}_k.${kkk}.bas"); next}1' $intInputDir/gateway.monpo.sym.cas > .tmp
			mv .tmp $intInputDir/gateway.monpo.sym.cas
			awk -v pat="l.${l}_k.${k}.bas" '$0 ~ pat{system("cat $ghostLibrary/l.${lll}_k.${kkk}.bas"); next}1' $intInputDir/gateway.monpo.cas > .tmp
			mv .tmp $intInputDir/gateway.monpo.cas
		done
	done
	awk -v pat="zero.bas" '$0 ~ pat{system("cat $ghostLibrary/zero.bas"); next}1' $intInputDir/gateway.monpo.sym.cas > .tmp
	mv .tmp $intInputDir/gateway.monpo.sym.cas
	awk -v pat="zero.bas" '$0 ~ pat{system("cat $ghostLibrary/zero.bas"); next}1' $intInputDir/gateway.monpo.cas > .tmp
	mv .tmp $intInputDir/gateway.monpo.cas
	#
	OMP_NUM_THREADS_TEMP=$OMP_NUM_THREADS
	export OMP_NUM_THREADS=1
	pymolcas $intInputDir/gateway.monpo.sym.cas > gateway.monpo.sym.cas.out
	check gateway.monpo.sym.cas.out
	awk -f $parseDir/ext_basisSymmetryInfo.awk gateway.monpo.sym.cas.out > $intDir/blockSymmetryInfo

	awk '/Center of mass/{getline;getline;print "X ",$1,$2,$3," Angstrom"}' gateway.monpo.sym.cas.out  > "CoM"
	CoMx=$(awk '{print $2}' CoM)
	CoMy=$(awk '{print $3}' CoM)
	CoMz=$(awk '{print $4}' CoM)
	awk -v x=$CoMx -v y=$CoMy -v z=$CoMz -f $parseDir/ext_geometry.awk gateway.monpo.sym.cas.out
	geomFiles=$(ls geometry*)
	nAtom=0
	if [ -e totalGeometry ]; then
		rm totalGeometry
	fi
	touch totalGeometry
	for g in $geomFiles; do
		count=$(wc -l $g | awk '{print $1}')
		nAtom=$(($nAtom+$count))
		l="##${g}#"
		sed -e "/$l/ Ir $g" -e "/$l/Id" "$intInputDir/gateway.monpo.cas" > .tmp 
			mv .tmp "$intInputDir/gateway.monpo.cas"
	done
	
	pymolcas $intInputDir/gateway.monpo.cas > gateway.monpo.cas.out
	check gateway.monpo.cas.out
	awk -f $parseDir/ext_petiteBasis.awk gateway.monpo.cas.out > petiteBasis
	awk '!/X/{c++;s=substr($3,1,2);if(c==1){p=s;q=1};if(p!=s){q++};p=s}END{print q}' petiteBasis > $intTempDir/lastshell.dat
	cp $intTempDir/lastshell.dat $intDir/lastshell.dat
	$xchemSrc/compressTwoElectronIntegrals.exe $intInputDir/compressTwoElectronIntegrals.dontCompress.xchem > compressTwoElectronIntegrals.dontCompress.out
	cp $intTempDir/rawBlockBasis $intDir/rawBlockBasis
	sed -e '1,/X\.\.\.\./d' -e '/contaminant all/,$d' gateway.monpo.cas.out > $intDir/aug.basis	
fi

sed "s%##INSTALLPATH%$INSTALLPATH%g ; s%##IN%$IN%g" $templateDir/intSubmit.template > $CurrDir/intSubmit.sh	

#SEWARD
if [ -e seward.oneo.cas.out ]; then
	next=$(grep -c "Happy" seward.oneo.cas.out)
else
	next=0
fi
echo "RUNNING seward (intDir/seward.oneo.cas.out) to obtain" >> $log
echo "  -one electron integrals monocentric and polycentric basis functions" >> $log
if [ $next -ne 0 ]; then
	echo "seward already done -> skip" >> $log	
else
	echo "1" > $WorkDir/prIntegral
	export MOLCAS_MEM=$MOLCAS_MEM_TEMP
	pymolcas $intInputDir/seward.oneo.cas > seward.oneo.cas.out
	export MOLCAS_MEM=2000
	check seward.oneo.cas.out
	mv $WorkDir/dip*.int $intDir/
	mv $WorkDir/vel*.int $intDir/
	mv $WorkDir/ov.int $intDir/
	mv $WorkDir/kin.int $intDir/
	mv $WorkDir/ham1E.int $intDir/
fi

echo "DONE" >> $log
