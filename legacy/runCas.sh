#!/bin/bash

n=$(echo -n "$SLURM_JOB_ID" | wc -c )
if [ $n -gt 0 ]; then
	SLRM_CMD=$(scontrol show jobid=$SLURM_JOB_ID | awk '/Command=/' | sed 's/Command=//g' | awk '{print $1}')
	LINK=$(readlink -f $SLRM_CMD)
else
	LINK=$(readlink -f $BASH_SOURCE)
fi
SCRIPT=${LINK##*/}
BASE=${SCRIPT%.*}
INSTALLPATH=${LINK%/*}
source $INSTALLPATH/config.sh

OVERWRITE=0
BREAK=0
log=$CurrDir/log
if [ -e $log ]; then
	for (( i=1 ; i<=9999 ; i++ )); do
		if [ ! -e ${log}_$i ]; then	
			log=${log}_$i
			break
		fi 
	done
fi 
ulimit -s unlimited
while getopts ":i:rphl:" opt; do
        case $opt in
		h) 	one="$INSTALLPATH/man/$BASE"
		   	two="$INSTALLPATH/man/general"
		   	cat $one $two | less
		   	exit ;; \
                i) 	IN=$OPTARG ;; \
		:) 	echo "missing argument for (i)nput"
			exit ;; \
		r) 	OVERWRITE=1 ;; \
		p) 	BREAK=1 ;; \
		l) 	log=$CurrDir/$OPTARG ;; \
		:) 	echo "missing argument for (l)og"
			exit ;; \
		[?]) 	echo "unknwon option -$OPTARG"
			exit 1 ;; \
        esac
done
export log=$log
export OVERWRITE=$OVERWRITE
export IN=$IN

if [ -e $log ]; then
	rm -f $log
fi
echo "------------------"> $log
echo "runLocal log file" >> $log
echo "------------------">> $log
if [ -z $IN ]; then
	echo "no (i)nput file provided. STOP" >> $log
	exit
fi

if [ ! -e $IN ]; then
	echo "(i)nput files does not exist. STOP" >> $log
	exit
fi

if [ $OVERWRITE -eq 1 ]; then 
	echo "ove(r)writing previous output" >> $log
else
	echo "keeping previous output" >> $log
fi
. $INSTALLPATH/processInput.sh 
. $INSTALLPATH/setupOne.sh

#CREATE INPUT FILES
if [ ! -e $casDir/.cp001 ]; then
	echo "Creating Input Files" >> $log
	$xchemSrc/createInputCas.exe $casInputDir $IN >> $casTempMolcasDir/createInputCas.out
	check $casTempMolcasDir/createInputCas.out
	echo "input files created" > $casDir/.cp001
else
	echo "input files already exist" >> $log
fi
if [ $BREAK -eq 1 ]; then	
	echo "runLocal.sh executed with -p. Script stopped to allow user to verify input files." >> $log
	echo "To resume run runLocal.sh without -p and -r options." >> $log
	exit
fi

#GATEWAY
cd $casTempMolcasDir
export Project="sym"
export WorkDir="$PWD/TMP-$Project"

echo "RUNNING gateway.local.sym.cas (molcasOutput/gateway.local.sym.out) to obtain:"  >> $log
echo "  -geometry without symmetry -> saved in molcasOutput/"  >> $log
echo "  -information on active space -> saved in molcasOutput/casInfo"  >> $log
next=$(skip gateway.local.sym.out)
if [ $next -eq 0 ]; then
	echo " " >> $log	
	mkdir $WorkDir
	pymolcas "$casInputDir/gateway.local.sym.cas" > "gateway.local.sym.out"
	check "gateway.local.sym.out"
else
	echo " -> skip" >> $log
fi

#CENTER OF MASS AND GEOMETRY
awk '/Center of mass/{getline;getline;print "X ",$1,$2,$3," Angstrom"}' "gateway.local.sym.out" > "CoM"
CoMx=$(awk '{print $2}' CoM)
CoMy=$(awk '{print $3}' CoM)
CoMz=$(awk '{print $4}' CoM)
echo "Center of Mass=($CoMx,$CoMy,$CoMz) a.u."  >> $log
echo "It is recommended the CoM be at the origin from the beginning" >> $log
awk -v x=$CoMx -v y=$CoMy -v z=$CoMz -f $parseDir/ext_geometry.awk gateway.local.sym.out
geomFiles=$(ls geometry*)
nAtom=0
if [ -e totalGeometry ]; then
	rm totalGeometry
fi
touch totalGeometry
for g in $geomFiles; do
	count=$(wc -l $g | awk '{print $1}')
	nAtom=$(($nAtom+$count))
	l="##${g}#"
	sed -e "/$l/ Ir $g" -e "/$l/Id" "$casInputDir/gateway.local.noSym.cas" > .tmp 
		mv .tmp "$casInputDir/gateway.local.noSym.cas"
	cat $g >> totalGeometry
done
sed "/##geometry/ r totalGeometry" $casInputDir/pro > .tmp && mv .tmp $casInputDir/pro
sed -i "/##geometry/a Title" $casInputDir/pro
sed -i "s/##geometry/$nAtom/g" $casInputDir/pro

#CAS INFO
awk '/Character Table for /{print tolower($NF)}' gateway.local.sym.out > casInfo
awk '/Symmetry species/{for (i=3;i<=NF;i++){printf "%s ", $i};print " ";exit}' gateway.local.sym.out >> casInfo
awk '/Basis functions    /{for (i=3;i<=NF;i++){printf "%s ", $i};print " ";exit}' gateway.local.sym.out >> casInfo
for nOrb in $ras2; do
	ras0="$ras0 0"
done
if [ -z "$ras1" ]; then
	ras1=$ras0
fi
if [ -z "$ras3" ]; then
	ras3=$ras0
fi
echo "$inactive" >> casInfo
echo "$ras1" >> casInfo
echo "$ras2" >> casInfo
echo "$ras3" >> casInfo
cp casInfo $casDir/casInfo
nRas1=$(echo $ras1 | awk '{for(i=1;i<=NF;i++){sum=sum+$i}}END{print sum}')
nRas2=$(echo $ras2 | awk '{for(i=1;i<=NF;i++){sum=sum+$i}}END{print sum}')
nRas3=$(echo $ras3 | awk '{for(i=1;i<=NF;i++){sum=sum+$i}}END{print sum}')
nRas3=$(($nRas3+$nRas2+$nRas1+2))
awk 'BEGIN{c=0}{if (NR==3){for (i=1;i<=NF;i++){for (j=1;j<=$i;j++){c++;print c}}}}' casInfo > energies
awk 'BEGIN{c=0}{if (NR==3){for (i=1;i<=NF;i++){for (j=1;j<=$i;j++){c++;print 0}}}}' casInfo > occupations

#SEWARD
echo "RUNNING seward (molcasOutput/gateway.local.sym.out) to obtain:"  >> $log
#echo "  -overlap matrix of local basis -> stored in ov.loc.int"  >> $log

next=$(skip "$casTempMolproDir/gateway.local.sym.out" )
if [ $next -eq 0 ]; then
	pymolcas "$casInputDir/seward.cas" >> "gateway.local.sym.out"
	check gateway.local.sym.out
else
	echo " -> skip" >> $log
fi

#MOLPRO
echo " " >> $log
next=$(skip $casDir/loc.ion.bin)
if [ $next -eq 0 ]; then
	case $typeOrbital in 
		MOLPRO)
			echo "RUNNING pro (molproOutput/pro.out) to obtain" >> $log
			echo "  -SA-CASSCF orbitlas for states of different symmetries specefied by user" >> $log
			cd $casTempMolproDir
			if [ -e pro.out ]; then
				echo "File pro.out exists, skipping" >> $log
 			else
				W=$PWD
				I=$PWD
				d=$PWD
				cp $casInputDir/pro .
				cp $CurrDir/$orbitalFileParent in.wf
				$molpro -W$W -I$I -d$d -s pro 
				check pro.out
				mv pro.molden $casDir/
			fi
			grep "MCSCF STATE.*Energy" pro.out | awk '{print $NF}' > $casDir/E.pro.dat
			echo " " >> $log
			echo "extract orbital info from molcas and molpro output and use this to transfor molpro to molcas orbitals"  >> $log
			$xchemSrc/readSewardProCas.exe "pro.out" "$casTempMolcasDir/gateway.local.sym.out" "$casInputDir/pro2cas.in" > readSewardProCas.out
			check readSewardProCas.out
			$xchemSrc/pro2cas.exe "$casInputDir/pro2cas.in" > pro2cas.out
			check pro2cas.out
			mv symOrb.bin $casDir/symOrb.bin
			mv symOrb.RasOrb $casDir/symOrb.RasOrb
			;;
		INPORB)
			echo "GETTING orbitals from INPORB file" >> $log
			cd $casTempMolcasDir
			cp $CurrDir/$orbitalFileParent $casDir/symOrb.RasOrb	
	esac

	#DESYMMETRIZE
	cd $casTempMolcasDir
	isSym=$(grep -c "symmetry" $casInputDir/gateway.local.sym.cas)
	if [[ $isSym -ne 0 ]]; then
		echo "RUNNING desy.cas (molcasOutput/desy.local.cas.out) to obtain:" >> $log
		echo "  -desymetrize converterted molpro orbitals" >> $log
		cp $casDir/symOrb.RasOrb $WorkDir/INPORB
		echo " " >> $log
		pymolcas $casInputDir/desy.cas > "desy.local.cas.out"
		check "desy.local.cas.out"
		echo " " >> $log
		echo "RUNNING reOrderSym2NoSym (output in lof file) to obtain"  >> $log
		echo "  -desymmetrized orbitlas in correct order" >> $log
		$xchemSrc/reOrderSym2NoSym.exe casInfo "$WorkDir/${Project}.DeSymOrb" "$casDir/symOrb.RasOrb" loc.ion.bin loc.ion.RasOrb>> $log
		cp loc.ion.bin $casDir/
		cp loc.ion.RasOrb $casDir/
	else	
		echo "No desymmetrization necessary" >> $log
		cp $casDir/symOrb.bin $WorkDir/loc.ion.bin
		cp $casDir/symOrb.RasOrb $WorkDir/loc.ion.RasOrb
	fi
else
	echo " -> skip" >> $log
fi

#GUGA tables
if [ -e $casDir/GUGA -o -d $casDir/GUGA ]; then
 cd $casTempMolcasDir
 ln -s $casDir/GUGA/* .
fi

#GUGA RAS3
export Project="noSym"
export WorkDir="$PWD/TMP-$Project"
echo " " >> $log
echo "RUNNING gateway.local.noSym.cas (molcasOutput/gateway.local.noSym.out) to obtain"  >> $log
echo " -integrals for localized basis"  >> $log
pymolcas "$casInputDir/gateway.local.noSym.cas" > "gateway.local.noSym.out" 
check "gateway.local.noSym.out"
pymolcas "$casInputDir/seward.cas" >> "gateway.local.noSym.out"
awk '/Nuclear Potential Energy/{print $(NF-1);exit}' "gateway.local.noSym.out" > $casDir/nuclearRepulsion
echo " " >> $log

echo "RUNNING rasscf.local.source.noSym.ras3.cas (molcasOutput/rasscf.local.source.noSym.ras3.cas.out) to obtain" >> $log
echo "  -scaffolding for jobiph without symmetry, with RAS3 with augmenting electron" >> $log
echo "  -nuclear contribution to dipole (in file permanentDip)" >> $log
next=$(skip local.source.ras3.guga)
if [ $next -eq 0 ]; then
	echo "1" > $WorkDir/gugaOnly
	cp loc.ion.RasOrb $WorkDir/INPORB

	echo "1" > $WorkDir/gugaOnly
	pymolcas "$casInputDir/rasscf.local.source.noSym.ras3.cas" > "rasscf.local.source.noSym.ras3.cas.out"
	rm $WorkDir/gugaOnly
	check "rasscf.local.source.noSym.ras3.cas.out"
	$pyParseDir/gugaParse.py rasscf.local.source.noSym.ras3.cas.out > local.source.ras3.guga

else
	echo " -> skip " >> $log
fi

#GUGA
echo " "  >> $log
echo "RUNNING rasscf.local.parent.noSym.cas (molcasOutput/rasscf.local.parent.noSym.cas.out) to obtain" >> $log
echo "  -scaffolding for jobiph without symmetry,without RAS3 and without augmenting electron" >> $log
next=$(skip local.parent.noSym.guga)
if [ $next -eq 0 ]; then
	echo "1" > $WorkDir/gugaOnly
	pymolcas "$casInputDir/rasscf.local.parent.noSym.cas" > "rasscf.local.parent.noSym.cas.out"
	rm $WorkDir/gugaOnly
	check "rasscf.local.parent.noSym.cas.out"
	cp $WorkDir/$Project.JobIph  ion.scaffolding.JobIph

	test=0
	if [ $OVERWRITE -ne 1 ]; then
		if [ -e local.parent.noSym.guga ]; then
			test=1
		else
			test=0
		fi
	fi
	if [ $test -eq 1 ]; then
		echo "already done -> skip" >> $log	
	else
		$pyParseDir/gugaParse.py rasscf.local.parent.noSym.cas.out > local.parent.noSym.guga
	fi
else
	echo " -> skip" >> $log
fi

cd $casInputDir
	symFiles=$(ls -1 rasscf.local.parent.sym-*.cas)
cd $casTempMolcasDir
if [ ! -d jobiphFactory ]; then
	mkdir jobiphFactory
fi

#exit 1

#AUGMENT
export Project="sym"
export WorkDir="$PWD/TMP-$Project"
nSym=$(echo "$symFiles" | wc -l)
echo "$nSym" > $casInputDir/ion.csfs2csf.in
echo "$nSym * $nRas3" | bc > $casInputDir/aug.csfs2csf.bySym.in
counter=0
for f in $symFiles; do	
	counter=$(($counter+1))
	echo "" >> $log
	echo "RUNNING $f (molcasOutput/$f.out) to obtain:" >> $log
	echo "  -symmetric ci vector from $f" >> $log
	echo "  -guga table" >> $log
		
	next=$(skip $f.CI.dat)
	if [ $next -eq 0 ]; then
		cp $casDir/symOrb.RasOrb $WorkDir/INPORB
		if [ ! -e $f.guga ]; then
			echo "prsd" >> $casInputDir/$f
			echo "prwf" >> $casInputDir/$f
			echo "0.0" >> $casInputDir/$f
		fi
		pymolcas $casInputDir/$f > $f.out	
		check $f.out
		if [ -e $f.guga ]; then
			echo "skip guga" >> $log
		else
			$pyParseDir/gugaParse.py $f.out > $f.guga
		fi
		echo "Test extractCIfromHDF5.py" >> $log
		$pyParseDir/extractCIfromHDF5.py $WorkDir/${Project}.rasscf.h5 > $casTempMolcasDir/$f.CI.dat
		cp $WorkDir/$Project.JobIph $casTempMolcasDir/${f}.JobIph
	else
		echo " -> skip" >> $log
	fi

	echo " " >> $log
	echo "RUNNING CSF2Det,Det2CSF,augment and CSFs2CSF (output in log file) to obtain:" >> $log
	echo "  -symmetric (non) augmented CI vector"  >> $log
	#spin=$(grep -m 1 "spinAugmentedElectron" $CurrDir/$IN | sed 's/spinAugmentedElectron=//g' | sed 's/-/ /g' | awk '{print $1}')
	spin=$(echo "$spinAugmentedElectron" | awk -v c=$counter '{print $c}')

	echo "	->turn into determinants" >> $log
	next=$(skip $f.CI.desym.dat)
	if [ $next -eq 0 ]; then
		echo "	\"$f.CI.dat\" ascii
			\"$f.guga\"
			\"$f.det\" ascii" | $xchemSrc/CSF2Det.exe >> $log  
		echo "	->turn into nonSymmetric CSFs" >> $log
		echo "	\"local.parent.noSym.guga\"
			\"$f.det\"
			\"$f.CI.desym.dat\" ascii" | $xchemSrc/Det2CSF.exe >> $log
	else
		echo " -> skip " >> $log
	fi
	echo "\"$f.CI.desym.dat\"" >> $casInputDir/ion.csfs2csf.in	
	
	echo "	->augment" >> $log
	echo "	\"$f.det\"
		\"$spin\"
		$nRas3" | $xchemSrc/augment.exe >> $log
	echo "	->turn into nonSymmetric CSFs" >> $log
	S=${spin:0:1}
	njobs=0
	for (( i=1 ; i <= $nRas3 ; i++ )); do
		n=$(echo $i | awk '{printf "%2.2i",$1}')
		next=$(skip $f.augment.$S-$n.CI)
		if [ $next -eq 0 ]; then
			njobs=$(($njobs+1))	
			echo "	\"local.source.ras3.guga\"
				\"augment.$S-$n.det\"
				\"$f.augment.$S-$n.CI\" ascii" > "Det2CSF.${n}.in"
			$xchemSrc/Det2CSF.exe < Det2CSF.${n}.in >> $log & 
			if [ $njobs -eq $OMP_NUM_THREADS ]; then
				wait
				njobs=0
			fi
		else
			echo "$f.augment.$S-$n.CI exists -> skip" >> $log	
		fi
		echo "\"$f.augment.$S-$n.CI\"" >> $casInputDir/aug.csfs2csf.bySym.in 
	done  
	wait
done 

awk '/RASSCF root/{c=c+1;if(c<10){fname="En_00"c".out"}else{fname="En_00"c".out"}; print $NF > fname}' rasscf.local.parent.sym-*.cas.out
mv En_0*.out $casDir/

sed -e 's/a-/a- /g' -e 's/b-/b- /g' -e 's/.CI/ .CI/g' $casInputDir/aug.csfs2csf.bySym.in | sort -k 2,2 | sed 's/ //g' > $casInputDir/aug.csfs2csf.byAug.in 
echo "aug.CI.dat" >> $casInputDir/aug.csfs2csf.bySym.in
echo "aug.CI.dat" >> $casInputDir/aug.csfs2csf.byAug.in
echo "ion.CI.dat" >> $casInputDir/ion.csfs2csf.in

awk '/Total nuclear/{if (NF==5){print $(NF-2),$(NF-1),$NF;exit}}' rasscf.local.parent.sym-1.cas.out > $casDir/permanentDip

if [ -e ion.CI.dat ]; then
 echo "File ion.CI.dat exists, skipping" >> $log
else
 $xchemSrc/CSFs2CSF.exe < $casInputDir/ion.csfs2csf.in >> $log
fi

if [ -e aug.CI.dat ]; then
 echo "File aug.CI.dat exists, skipping" >> $log
else
 $xchemSrc/CSFs2CSF.exe < $casInputDir/aug.csfs2csf.byAug.in >> $log
fi

cd $casTempMolcasDir
pymolcas "$casInputDir/seward.local.mltpl.cas" > "seward.local.mltpl.cas.out"
check seward.local.mltpl.cas.out
counter=0
for f in $symFiles; do
	counter=$(($counter+1))
	if [ $counter -le 9 ]; then
		cp ${f}.JobIph $WorkDir/JOB00$counter
	else
		cp ${f}.JobIph $WorkDir/JOB0$counter
	fi
done

cd $casInputDir
	mltplFiles=$(ls -1 rassi.local.mltpl-*.cas)
cd $casTempMolcasDir
order=0
if [ -e multipoles.raw ]; then
	rm multipoles.raw
fi
for f in $mltplFiles; do
	order=$(($order+1))
	rm -f $WorkDir/prop*.out
	echo "" >> $log
	echo "RUNNING $f (molcasOutput/$f.out) to obtain" >> $log
	echo "  -multipole of order $order of parent ion" >> $log
	pymolcas $casInputDir/$f > $f.out
	check $f.out
	cat $WorkDir/prop*.out >> multipoles.raw 
done
awk -f $parseDir/multipoles.awk multipoles.raw > $casDir/multipoles.dat
rm multipoles.raw

cd $casInputDir
	mltplFiles=$(ls -1 rassi.local.velocity-*.cas)
cd $casTempMolcasDir
order=0
if [ -e velocities.raw ]; then
	rm velocities.raw
fi
for f in $mltplFiles; do
	order=$(($order+1))
	rm -f $WorkDir/prop*.out
	echo "" >> $log
	echo "RUNNING $f (molcasOutput/$f.out) to obtain" >> $log
	echo "  -velocity of order $order of parent ion" >> $log
	pymolcas $casInputDir/$f > $f.out
	check $f.out
	cat $WorkDir/prop*.out >> velocities.raw 
done
awk -f $parseDir/multipoles.awk velocities.raw > $casDir/velocities.dat
rm velocities.raw


#SOURCE
echo "calculate source states and turn source and augmented states into determinatns" >> $log
nSource=0
if [ -z $createSourceFromParent ]; then
	for s in $sourceFiles; do
		nSource=$(($nSource+1))
		fromParent[$nSource]='T'
	done
else
	for s in $createSourceFromParent; do
		nSource=$(($nSource+1))
		fromParent[$nSource]=$s
	done
fi
echo "nSource=$nSource" >> $log
echo "generate? ${fromParent[*]}" >> $log
nTotal=0
if [ ! $nSource -eq 0 ]; then
	nRun=0
	nCopy=0
	for s in $sourceFiles; do
		nTotal=$(($nTotal+1))
		sourceAll[$nTotal]=$s
		if [ "${fromParent[$nTotal]}" == "T" ]; then
			nRun=$(($nRun+1))
			sourceRun[$nRun]=$s
		elif [ "${fromParent[$nTotal]}" == "F" ]; then
			nCopy=$(($nCopy+1))
			sourceCopy[$nCopy]=$s
		fi
	done

	export Project="sym"
	export WorkDir="$PWD/TMP-$Project"

	for (( i=1 ; i <= $nRun ; i++ )); do
		f="rasscf.monpo.source-${sourceRun[$i]}.sym.cas"
		echo "creating local part for source $f" >> $log
		cp $casDir/symOrb.RasOrb $WorkDir/INPORB
		echo "" >> $log
		echo "RUNNING $f (molcasOutput/$f) to obtain" >> $log
		echo "  -CI vector and guga table for source: ${sourceRun[$i]}" >> $log
		pymolcas $casInputDir/$f > $f.out
		if [ -e $f.guga ]; then
			echo "Skip guga" >> $log
		else
			$pyParseDir/gugaParse.py $f.out > $f.guga
		fi
		check $f.out

		$pyParseDir/extractCIfromHDF5.py $WorkDir/${Project}.rasscf.h5 > $casTempMolcasDir/$f.CI.dat
		echo "" >> $log
		echo "RUNNING CSF2Det and Det2CSF (output in log file) to obtain" >> $log
		echo "	-non symmetric,ras3 CI-vectors for source: ${sourceRun[$i]}"  >> $log
		echo "	\"$f.CI.dat\" ascii
			\"$f.guga\"
			\"$f.det\" ascii" | $xchemSrc/CSF2Det.exe >> $log  
		echo "	->turn into nonSymmetric CSFs" >> $log
		echo "	\"local.source.ras3.guga\"
			\"$f.det\"
			\"$f.CI.desym.dat\" ascii" | $xchemSrc/Det2CSF.exe >> $log
		cp $f.CI.desym.dat source-${sourceRun[$i]}.CI.dat
	done
	for (( i=1 ; i<=$nCopy ; i++ )); do
		echo "" >> $log
		echo "LOOKING for ${sourceCopy[$i]}.JobIph" >> $log
		if [ ! -e $CurrDir/${sourceCopy[$i]}.JobIph ]; then	
			echo "ERROR could not find ${sourceCopy[$i]}.JobIph, which was specefied by user to serve as source state." >> $log
			exit
		else
			echo "FOUND ${sourceCopy[$i]}.JobIph" >> $log
		fi
		echo "RUNNING job2asc to gut ${sourceCopy[$i]}.JobIph." >> $log
		echo "NOTE this is expected to have desymetrized Orbitals and an (albeit empty) RAS3 active space." >> $log
		echo "A version is pending which automatizes this but will require user to provide guga table" >> $log

		$pyParseDir/extractCIfromHDF5.py $WorkDir/${Project}.rasscf.h5 > $casTempMolcasDir/rasscf.monpo.source-${sourceCopy[$i]}.sym.cas.CI.desym.dat
	done
else
	echo "no Source States. Skip first step" >> $log
fi


if [ $nSource -eq 0 ]; then
	ln -s  aug.CI.dat source-aug.CI.dat
else
	if [ -e source-aug.CI.dat ]; then
		 echo "source-aug.CI.dat exists, skipping" >> $log
	else
		echo "$(($nSource+1))" > $casInputDir/"source-aug.csfs2csf.in"
		ls -1 source-*.CI.dat >> $casInputDir/"source-aug.csfs2csf.in"
		echo "aug.CI.dat" >> $casInputDir/"source-aug.csfs2csf.in"
		echo "source-aug.CI.dat" >> $casInputDir/"source-aug.csfs2csf.in" 
		$xchemSrc/CSFs2CSF.exe < $casInputDir/"source-aug.csfs2csf.in" >> $log
	fi
fi

nState=$(awk 'NR==1{printf"%i", $1}' source-aug.CI.dat)
echo "nState=$nState" >> $log

echo "source-aug.CI.dat ascii
	local.source.ras3.guga
	source-aug.det binary" | $xchemSrc/CSF2Det.exe >> $log  
$xchemSrc/detSplit.exe source-aug.det >> $log
rm source-aug.det 

#TRD/CSFMAP
if [[ $rdmMethod -eq "trd" ]]; then
	sed -e "s%##INSTALLPATH%$INSTALLPATH%g" -e "s%##IN%$IN%g" $templateDir/casSubmit.template > $CurrDir/casSubmit.sh	
	sed -e "s/##nState/$nState/g" $templateDir/submitTrd2Element.template > submitTrd2Element.temp.sh
else
	echo "createCSF needs to be written to script here" >> $log
fi
