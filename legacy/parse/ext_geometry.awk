/  Label   Cartesian Coordinates /{
	getline
	getline
	c=c+1
	label[$1]=c
}
/Center  Label                x/{
	getline
	i=1
	while($1==i){
		atomRaw=$2
		atom=gensub(/[0-9]+/,"","g",atomRaw)
		l=label[atomRaw]
		a[atom]++
		if (l<10) {
			name="geometry00"l
		} else if (l<100){
			name="geometry0"l
		} else {
			name="geometry"l
		}
		if (a[atom]==1){
			printf"%s%s %18.14f %18.14f %18.14f %s\n",atom,a[atom],$3,$4,$5,"Bohr" > name
		}else{
			printf"%s%s %18.14f %18.14f %18.14f %s\n",atom,a[atom],$3,$4,$5,"Bohr" >> name
		}
		i++
		getline
	}
}
