#!/bin/bash
dir=$1
characterTable=$(awk '{if (NR==2){print $0}}' casInfo)
files=$(ls -1 ${dir}/*)
for f in $files; do
	count=0
	for c in $characterTable; do
		count=$(($count+1))
		string="##$c"
		sed "s/$string/$count/g" $f > $dir/tmp
		mv $dir/tmp $f
	done
done


