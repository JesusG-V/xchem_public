#!/bin/env python3

import h5py
import sys

filename = sys.argv[1]
f = h5py.File(filename, 'r')

data = f["CI_VECTORS"]
nRoot = data.shape[0]
nCSF = data.shape[1]

print("%05d %010d" % (nRoot,nCSF))
for iRoot in range(nRoot):
	for iCSF in range(nCSF):  
		print("%.20E " % data[iRoot,iCSF],end='')
	print("")
