#!/bin/bash
if [[ $WorkDir = *'$'* ]]; then
	WorkDir=$(eval echo "$WorkDir")	
fi
export WorkDir=$WorkDir
echo "WorkDir= $WorkDir"
if [ ! -d $WorkDir ]; then
    mkdir -p $WorkDir
fi

cd $orbTempDir

log=$WorkDir/log

if [ -e ijrs.int ]; then
	echo " File with partially transformed integrals ijrs.int already exists -> skip"
else
	echo "jobid=${SLURM_JOB_ID}" > $log
	echo "Copying orbtials and basis two electron integrals to node" >> $log
	cp $orbInputDir/ijkl2ijrs.xchem $WorkDir/ijkl2ijrs.xchem
	if [ -e $intDir/ham2E.compressed.int ]; then
		ln -s $intDir/ham2E.compressed.int $WorkDir/
	elif [ -e $intDir/ham2E.compressed.int.1-1 ]; then
		ln -s $intDir/ham2E.compressed.int.* $WorkDir/
		lastshell=$(cat $intDir/lastshell.dat)
		echo "" > $WorkDir/ijkl.suffix
		for (( i=1; i<=$lastshell; i++)); do
			for (( j=1; j<=$i; j++)); do
				echo "${i}-${j}" >> $WorkDir/ijkl.suffix
			done
		done
	elif [ -e $intTempDir/ham2E.compressed.int.1-1-1-1 ]; then
		cp $intTempDir/ijkl.suffix $WorkDir/ijkl.suffix
		ln -s $intDir/TMP $WorkDir/ints
	else
		echo "ERROR: no integrals or TMP directory found"
		exit 1
	fi

	cp $orbTempDir/ion.homemade.bin $WorkDir/ion.homemade.bin
	
	cd $WorkDir

	$xchemSrc/ijkl2ijrs.exe $WorkDir/ijkl2ijrs.xchem >> $log
	test=$(grep -c "Happy" $log)
	if [ $test -eq 1 ];then
		echo "Partial integral transformation (ijkl -> ijrs) done" >> $log
		bzip2 < $log > $orbTempDir/ijkl2ijrs.log.bz2
		mv $WorkDir/ijrs.int $orbTempDir/ijrs.int
	else
		echo "Partial integral transformation (ijkl -> ijrs) FAILED" >> $log
		bzip2 < $log > $orbTempDir/ijkl2ijrs.log.fail.bz2
	fi
fi	
