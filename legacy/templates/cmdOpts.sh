#!/bin/bash

for i in "$@"; do
	case $i in
		-h*|--help*)
		echo 'orbSubmit command line options:'
		echo ' -h --help : displays this message' 
		echo ' -q --qos : define quality of service of submitted jobs'
		exit
		shift
		;;
		-q*|--qos*)
		QOS="${i#*=}"
		shift
		;;
		*)
		echo "ERROR unknown command line option"
		exit
		;;
	esac
done
