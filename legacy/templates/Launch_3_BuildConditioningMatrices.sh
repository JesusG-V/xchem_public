#!/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatBCM.sh
else
	bash calcs/submitScatBCM.sh
fi
