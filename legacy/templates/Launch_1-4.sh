#!/bin/bash

cond=0
cap=0
axes="xyz"
gauge="lv"
qos="fast"

source SCAT_PARAMS
source calcs/submitScatHeader1.sh
cd $scatInputDir
$pyParseDir/ccParse.py $CCF
mv .nchannel $scatSubmitDir/
cd $scatSubmitDir

while [[ $# -gt 0 ]]; do
	key=$1
	case $key in
		-gauge)
		if [ -z $2 ]; then	
			echo "ERROR -gauge needs an argument"
			exit 1
		fi
		if [[ ! "lv vl"=*$2* ]]; then
			echo "ERROR invalid argument for -gauge"
			exit 1
		fi
		shift
		shift
		;;
		-axes)
		if [ -z $2 ]; then	
			echo "ERROR -axes needs an argument"
			exit 1
		fi
		if [[ ! "xyz"=*$2* ]]; then
			echo "ERROR invalid argument for -axes"
			exit 1
		fi
		shift
		shift
		;;
	esac
done

nChannel=$(cat .nchannel)
### - 1 - ###
idBME=$(sbatch --job-name=BME calcs/submitScatBME.sh | awk '{print $NF}')
### - 2 - ###
idCQCI=$(sbatch --job-name=CQCI --dependency=afterok:$idBME calcs/submitScatCQCI.sh | awk '{print $NF}')
### - 3 - ###
if [ $cond -eq 1 ]; then
	idBCM=$(sbatch --job-name=BCM --dependency=afterok:$idBME:$idCQCI calcs/submitScatBCM.sh | awk '{print $NF}')
fi

idBSES_D=""
for (( iC = 0 ; iC < $nChannel ; iC++ )); do
    sed "s/##c1/$iC/g ; s/##c2/$iC/g" calcs/submitScatBSES.sh > OpMat_${iC}_${iC}.sh
    id=$(sbatch --dependency=afterok:$idBME:$idCQCI OpMat_${iC}_${iC}.sh | awk '{print $NF}')
	if [ $iC -eq 0 ]; then
		idBSES_D="$id"
	else
		idBSES_D="${idBSES_D}:${id}"
	fi
    rm OpMat_${iC}_${iC}.sh
done

### - 4 - ###
ids="${idBME}:${idCQCI}:${idBCM}:${idBSES_D}"
ids=$(sed "s/[:]\{2,\}/:/g ; s/:$//g" <<< $ids)
idBSES_OD=""
for (( iC = 0 ; iC < $nChannel ; iC++ )); do
    for (( jC = $iC+1 ; jC < $nChannel ; jC++ )); do
        sed "s/##c1/$iC/g ; s/##c2/$jC/g" calcs/submitScatBSES.sh > OpMat_${iC}_${jC}.sh
        sbatch --dependency=afterok:$ids OpMat_${iC}_${jC}.sh 
        rm OpMat_${iC}_${jC}.sh
    done
done
