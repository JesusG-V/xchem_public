#!/bin/bash
export nCPUS=$OMP_NUM_THREADS
export OMP_NUM_THREADS=1
export Project="xchemInt"
if [[ $WorkDir = *'$'* ]]; then
	WorkDir=$(eval echo "$WorkDir")	
fi
echo "WorkDir= $WorkDir"


export CurrDir=$WorkDir
export WorkDir=$CurrDir/tmp
if [ ! -d $WorkDir ]; then
    mkdir -p $WorkDir
fi

i=##i
j=##j

cp $intInputDir/gateway.monpo.cas  $CurrDir/molcas
cat $intInputDir/seward.cas >> $CurrDir/molcas
cp $intInputDir/compressTwoElectronIntegrals.xchem $CurrDir/compressTwoElectronIntegrals.xchem

cd $CurrDir
echo "1" > $WorkDir/prIntegral
cp $intTempDir/petiteBasis $WorkDir/
cp $intTempDir/lastshell.dat $WorkDir/
if [ -f $intTempDir/lk.dat ]; then
	cp $intTempDir/lk.dat $WorkDir/lk.dat
fi
echo "$i $j" >> $WorkDir/lastshell.dat

log=$CurrDir/basisTwoElectronIntegrals.${i}-${j}.log
echo "nCPUS=$nCPUS" > $log
pymolcas molcas >> $log

check=$(grep -c "Happy" $log)

echo "check $check"

if [ $check -eq 0 ]; then
	echo "ERROR Molcas failed. No integrals produced" >> $log
	mv $log $intTempDir/basisTwoElectronIntegrals.${i}-${j}.fail.log
	exit 1
fi

echo "Molcas done. Run integral compression in parallel." >> $log

cd $WorkDir

function cmpr {
	local suffix=$1
	mkdir $suffix
	cd $suffix
	mv ../ham2E.${line}.int ham2E.int
	ln -s ../petiteBasis .
	ln -s ../lk.dat .
	$xchemSrc/compressTwoElectronIntegrals.exe $CurrDir/compressTwoElectronIntegrals.xchem >> $WorkDir/compress.${suffix}.log
	mv ham2E.compressed.int ../ham2E.compressed.int.${line}
	cd $WorkDir 
	rm -rf $suffix
	rm running.$suffix
}

cpus=0
while read -r line; do
	echo "compress block $line" >> $log
	touch running.$line
	cpus=$(ls -1 running.* 2>/dev/null | wc -l )
	cmpr $line &
	while [ $cpus -ge $nCPUS ]; do
		sleep 5
		cpus=$(ls -1 running.* 2>/dev/null | wc -l )
		#echo "$cpus busy" >> $log
	done
done < ijkl.suffix
wait

while read -r line; do
	check=$(grep -c "Happy" compress.${line}.log) 
	if [ $check -gt 0 ]; then
		cat compress.${line}.log >> $log	
	else
		echo "ERROR in compression of ${line} block" >> $log
		exit 1
	fi
done < ijkl.suffix

#concatenate if made parallel
$xchemSrc/twoIntConcatenate.exe "ham2E.compressed.int" "ijkl.suffix" > concatenate.log

check=$(grep -c "Happy" concatenate.log)
if [ $check -gt 0 ]; then
	cat concatenate.log >> $log
	bzip2 < $log > $intTempDir/basisTwoElectronIntegrals.${i}-${j}.log.bz2
	mv ham2E.compressed.int $intDir/ham2E.compressed.int.${i}-${j}
else
	cat concatenate.log >> $log
	echo "ERROR concatenation failed" >> $log
	bzip2 < $log > $intTempDir/basisTwoElectronIntegrals.${i}-${j}.fail.log.bz2
fi

