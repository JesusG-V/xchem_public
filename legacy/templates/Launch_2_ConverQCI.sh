#!/bin/bash
source SCAT_PARAMS
source calcs/submitScatHeader1.sh
cd $scatInputDir
$pyParseDir/ccParse.py $CCF
mv .nchannel $scatSubmitDir/
cd $scatSubmitDir

if [ $cap -eq 1 ]; then
	echo "ATTENTION: cap is set to 1. Make sure valide absorbers are included inf scat_*/input/AbsorptionPotential"
	echo "           IF you change this now you must rerun BasisMatrixElements (Launch_1*) first!"
fi
if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatCQCI.sh
else
	bash calcs/submitScatCQCI.sh
fi
