#/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatBCSES.sh
else
	bash calcs/submitScatBCSES.sh
fi
