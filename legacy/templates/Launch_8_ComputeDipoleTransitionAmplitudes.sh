#!/bin/bash
source SCAT_PARAMS

if [[ $gauge = *"l"* ]]; then
	if [[ $axes = *"x"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_LenX.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_LenX.sh
		fi
	fi
	if [[ $axes = *"y"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_LenY.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_LenY.sh
		fi
	fi
	if [[ $axes = *"z"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_LenZ.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_LenZ.sh
		fi
	fi
fi
if [[ $gauge = *"v"* ]]; then
	if [[ $axes = *"x"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_VelX.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_VelX.sh
		fi
	fi
	if [[ $axes = *"y"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_VelY.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_VelY.sh
		fi
	fi
	if [[ $axes = *"z"* ]]; then
		if [ $submit -eq 1 ]; then 
			sbatch calcs/submitScatCDTA_VelZ.sh
			sleep 0.5
		else
			bash calcs/submitScatCDTA_VelZ.sh
		fi
	fi
fi
