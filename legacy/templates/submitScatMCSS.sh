#!/bin/bash
source calcs/submitScatHeader1.sh
source calcs/submitScatHeader2.sh

cd $WorkDir

if [ -L $qcSpec ]; then
	unlink $qcSpec
fi 
if [ -L scatDir ]; then
	unlink scatDir
fi 

ln -s $orbDir $qcSpec
ln -s $scatDir scatDir

cp $scatInputDir/SAE_GeneralInputFile .
cp $scatInputDir/XchemInputFile .
cp $scatInputDir/Basis_Mixed .
cp $scatInputDir/AbsorptionPotential .
cp $scatInputDir/GaussianBSplineBasis .
cp $scatInputDir/GaussianBasis .
cp $scatInputDir/BSplineBasis .

cp $scatInputDir/$CCF .

log=$scatLogDir/ComputeMultichannelScatteringStates.out_Emin${Emin}_Emax${Emax}_cm${cm}

if [[ -z $brasym && -z $ketsym ]]; then
	echo "ERROR: either bra or ketsym need to be defined" > $scatLogDir/ComputeMultichannelScatteringStates.out
	exit 1
elif [[ -z $ketsym ]]; then 
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -brasym $brasym -Emin $Emin -Emax $Emax -cm $cm -NE $NE > $log
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -brasym $brasym -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log
elif [[ -z $brasym ]]; then
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -ketsym $ketsym -Emin $Emin -Emax $Emax -cm $cm -NE $NE > $log
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -ketsym $ketsym -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log
else
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -brasym $brasym -ketsym $ketsym -Emin $Emin -Emax $Emax -cm $cm -NE $NE > $log
	$xchemSrc/ComputeMultichannelScatteringStates -gif SAE_GeneralInputFile -pif XchemInputFile -mult $multiplicityAugmented -ccfile $CCF -esdir $qcSpec -op S -brasym $brasym -ketsym $ketsym -Emin $Emin -Emax $Emax -cm $cm -concatenate >> $log
fi
