#!/bin/bash
source SCAT_PARAMS

if [ $submit -eq 1 ]; then 
	sbatch calcs/submitScatCSESE.sh
else
	bash calcs/submitScatCSESE.sh
fi
