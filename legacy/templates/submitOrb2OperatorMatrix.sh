#!/bin/bash
if [[ $WorkDir = *'$'* ]]; then
	WorkDir=$(eval echo "$WorkDir")	
fi
export WorkDir=$WorkDir
echo "WorkDir= $WorkDir"
if [ ! -d $WorkDir ]; then
    mkdir -p $WorkDir
fi

export log=$WorkDir/log
cd $WorkDir

echo "Create two electron hamiltonian" > $log
echo "Copy files to node" >> $log

#### TWO ELECTRON HAMILTONAIN #####

if [ ! -e $orbDir/ham2E.state.int ]; then
	ln -s $casDir/trd2 $WorkDir/trd2
	cp $orbInputDir/twoIntOrb2OperatorMatrix_new.xchem $WorkDir/twoIntOrb2OperatorMatrix.xchem
	ln -s $orbTempDir/ham2E.orb.int $WorkDir/ham2E.orb.int
	$xchemSrc/twoIntOrb2OperatorMatrix_new.exe twoIntOrb2OperatorMatrix.xchem >> $log
	happy=$(grep -c "Happy" $log )
	if [[ $happy -gt 0 ]]; then
		echo "Calculation complete" >> $log 
		mv $log $orbTempDir/twoIntOrb2OperatorMatrix.log
	else
		echo "ERROR: Calculation failed" >> $log
		mv $log $orbTempDir/twoIntOrb2OperatorMatrix.fail
	fi
else
	echo "ham2E.state.int already exists" >> $log
	echo " -> skip" >> $log
	mv $log $orbTempDir/twoIntOrb2OperatorMatrix.log
	if [ ! -e $orbDir/Ham.state.int ]; then
		ln -s $orbDir/ham2E.state.int .
	fi 
fi

#### ONE ELECTRON HAMILTONAIN #####

echo "Create one electron hamiltonian" > $log
echo "Copy files to node" >> $log
cp $casDir/trd1 $WorkDir/trd1
function oneIntSend {
	local orbIntFile=$1
	LocalWorkDir="$WorkDir/$orbIntFile"
	mkdir $LocalWorkDir
	cd $LocalWorkDir
	ln -s $WorkDir/trd1 $LocalWorkDir/trd1
	cp $orbTempDir/$orbIntFile op1E.orb.int
	cp $orbInputDir/oneIntOrb2OperatorMatrix_new.xchem oneIntOrb2OperatorMatrix.xchem
	if [[ $count -eq 1 ]]; then
		$xchemSrc/oneIntOrb2OperatorMatrix_new.exe oneIntOrb2OperatorMatrix.xchem $nele ${hermitian[$count]} > log
	else
		$xchemSrc/oneIntOrb2OperatorMatrix_new.exe oneIntOrb2OperatorMatrix.xchem 1.0 ${hermitian[$count]} > log
	fi
	happy=$(grep -c "Happy" log)
	if [[ $happy -eq 0 ]]; then
		echo "fail" >> log
	fi
}

oneIntFiles="ov.orb.int ham1E.orb.int kin.orb.int" 
oneIntFiles="$oneIntFiles dip1.orb.int dip2.orb.int dip3.orb.int"
oneIntFiles="$oneIntFiles vel1.orb.int vel2.orb.int vel3.orb.int"
hermitian[1]="T"
hermitian[2]="T"
hermitian[3]="T"
hermitian[4]="T"
hermitian[5]="T"
hermitian[6]="T"
hermitian[7]="F"
hermitian[8]="F"
hermitian[9]="F"

miscFile="nuclearRepulsion permanentDip casInfo"
for MF in $miscFile; do
	cp $orbTempDir/$MF .
done

nele=$(awk '/Number of electrons in parent ions/{print $NF}' $orbTempDir/createInputOrb.out)
nele=$(($nele + 1))

count=0
for OIF in $oneIntFiles; do
	count=$(($count+1))
	if [ ! -e $orbDir/${OIF%%.*}.state.int ]; then
		oneIntSend $OIF &
	else
		echo "${OIF%%.*}.state.int already exists" >> $log
		echo " -> skip" >> $log
		ln -s $orbDir/${OIF%%.*}.state.int .
	fi
done
wait

for OIF in $oneIntFiles; do
	if [ -e $WorkDir/$OIF/log ]; then
		cat $WorkDir/$OIF/log >> $log
		mv $WorkDir/$OIF/op1E.state.int $WorkDir/${OIF%%.*}.state.int
	fi
done

fail=$(grep -c "fail" $log)
if [[ $fail -eq 0 ]]; then
	echo "Calculation complete" >> $log 
	mv $log $orbTempDir/oneIntOrb2OperatorMatrix.log
else
	echo "ERROR: Calculation failed" >> $log
	mv $log $orbTempDir/oneIntOrb2OperatorMatrix.fail
fi

#### INCLUDE NUCLEAR CONTRIBUTION #####

cd $WorkDir
echo "House Keeping:" > $log
echo "	- Account for permanent dipole" >> $log
echo "	- Sum up total Hamiltonian" >> $log

dipx=$(awk '{print $1}' permanentDip)
dipy=$(awk '{print $2}' permanentDip)
dipz=$(awk '{print $3}' permanentDip)

if [ ! -e $orbDir/dip1.state.int ]; then
	echo "&INPUT
		oneIntFile=dip1.state.int
		ovFile=ov.state.int
		diagonal=$dipx
		&END" > input
	$xchemSrc/subtractDiagonal.exe input >> $log
fi
if [ ! -e $orbDir/dip2.state.int ]; then
	echo "&INPUT
		oneIntFile=dip2.state.int
		ovFile=ov.state.int
		diagonal=$dipy
		&END" > input
	$xchemSrc/subtractDiagonal.exe input >> $log
fi
if [ ! -e $orbDir/dip3.state.int ]; then
	echo "&INPUT
		oneIntFile=dip3.state.int
		ovFile=ov.state.int
		diagonal=$dipz
		&END" > input
	$xchemSrc/subtractDiagonal.exe input >> $log
fi

if [ ! -e $orbDir/Ham.state.int ]; then
	if [ -e $orbDir/ham1E.state.int ]; then
		ln -s $orbDir/ham1E.state.int .
	fi
	echo "&INPUT
		oneElFile=ham1E.state.int
		twoElFile=ham2E.state.int
		ovFile=ov.state.int
		nucRepFile=nuclearRepulsion
		totHamFile=Ham.state.int
		&END" > input
	$xchemSrc/sumOneIntTwoIntNuclear.exe input >> $log
fi

mv $log $orbTempDir/miscOrb2OperatorMatrix.log

#### SPLIT BY SYMMETRY #####

cp $orbInputDir/symSplit.xchem .
cp $orbTempDir/blockBasis .
cp $orbTempDir/orbs.sym .
echo "splitting matrice according to symmetry and creating interface" > $log 

fs="Ham.state.int"
fs="$fs dip1.state.int dip2.state.int dip3.state.int"
fs="$fs vel1.state.int vel2.state.int vel3.state.int"

for f in $fs; do
	if [ ! -e $f ]; then
		ln -s $orbDir/$f .
	fi
	$xchemSrc/symSplit.exe symSplit.xchem $f >> $log
done
if [ ! -e ov.state.int ]; then
	ln -s $orbDir/ov.state.int .
fi
$xchemSrc/symSplit.exe symSplit.xchem ov.state.int "T" >> $log 

### CREATE INTERFACE(S) ####


echo "create interface(s) to scattering code" >> $log

forceC1=$(grep -c "Enforcing c1 symmetry" $log)
nIrrep=$(awk 'NR==2{print NF}' casInfo)
cp $orbInputDir/GABS_interface_sym.xchem .
cp $orbTempDir/lk.dat lk.dat
cp $orbDir/diffuseOrb.bin ion.homemade.bin
ln -s $ghostLibrary/* .

if [[ $forceC1 -eq 0 ]]; then
	for (( iIrrep=1 ; iIrrep<=$nIrrep ; iIrrep++ )); do
		$xchemSrc/GABS_interface_sym.exe GABS_interface_sym.xchem $iIrrep >> $log
	done
else
	$xchemSrc/GABS_interface_sym.exe GABS_interface_sym.xchem 1  >> $log
fi

### DIAGONALIZE HAMILTONIAN ####

echo "Compute Eigenvalues" >> $log

echo "&INPUT
	opFile=Ham.state.int
	ovFile=ov.state.int
	eValFile=ham.eVals
	&END" > input
$xchemSrc/computeEvals.exe input >> $log

### MOVE DATA FROM NODE ####

fs="Ham.state ham1E.state ham2E.state kin.state ov.state"
fs="$fs dip1.state dip2.state dip3.state"
fs="$fs vel1.state vel2.state vel3.state"
cpfs="symmetry ham.eVals"

for f in $fs; do 
	$xchemSrc/bin2fmt.exe ${f}.int ${f}.fmt
	cpfs="$cpfs  ${f}.int ${f}.fmt"
done


for f in $cpfs; do 
	if [ -e $f ]; then
		if [ ! -e $orbDir/$f ]; then
			mv $f $orbDir/$f
		fi
	else
		if [ ! -e $orbDir/$f ]; then
			echo "ERROR $f doesnt exist and wasnt created" >> $log
			echo "ERROR $f doesnt exist and wasnt created" 
		fi
	fi
done

fs=$(ls -1 *.out)
for f in $fs; do
	mv $f $orbDir/$f
done

fs=$(ls -1 interface*)
for f in $fs; do
	mv $f $orbDir/$f
done
mv $log $orbTempDir/interface.log
