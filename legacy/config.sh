#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Specify maximum memory in molcas.
if [[ -z $MOLCAS_MEM ]]; then
 export MOLCAS_MEM=8000
fi

# Add necessary libraries
export LD_LIBRARY_PATH=/home/jgv/programs/hdf5/lib/:$LD_LIBRARY_PATH

# Define molpro path if molpro *.wf format is used for orbitals
#export molpro=/home/jgv/bin/molpros

# Number of processors to work with.
if [[ -z $OMP_NUM_THREADS ]]; then
	export OMP_NUM_THREADS=16
fi

#Working Directory
if [[ -z $WorkDir ]]; then
    export WorkDir=$PWD
fi

export xchemSrc="$DIR/bin"
if [[ $DEBUG == 1 ]]; then
	export xchemSrc="$DIR/bin_d"
fi
export parseDir="$DIR/parse"
export pyParseDir="$DIR/pyparse"
export templateDir="$DIR/templates"
export ScriptDir="$DIR/scripts"
export ghostLibrary="$DIR/ghostLibrary"
export CurrDir="$PWD"
export MOLCAS_PRINT=3
