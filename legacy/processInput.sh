#!/bin/bash
while read line; do	
	counter=$(echo "$line" | grep -c '=')
	if [ $counter -eq 1 ]; then
		line=$(echo "$line" | sed -e "s/[\t, ]//g" -e 's/=/ /g')
		fields=$(echo $line | wc -w)
		if [ $fields -ne  2 ]; then
			echo "ERROR parsing inputFile for script" >> $log 
			exit
		fi
		a=0
		for f in $line; do
			a=$(($a+1))
			if [ $a -eq 1 ]; then
				fs[$a]=$f
			else
				fs[$a]=$(echo "$f" | sed 's/-/ /g')
			fi 
		done
		eval "export ${fs[1]}=\"${fs[2]}\""
	fi
done < $IN

# setting up defaults
if [ -z $nodes ]; then
	export nodes=20
fi
if [ -z $trdMethod ]; then
	export rdmMethod="trd"
fi
if [ -z $typeOrbital ]; then
	export typeOrbital="INPORB"
fi

#CAS PATHS
tempPath=$(echo "cas_i.${inactive}_r1.${ras1}_r2.${ras2}_r3.${ras3}_e.${nElecRas1}-${nElecRas2}-${nElecRas3}" | sed 's/ /-/g')
export casDir="$CurrDir/$tempPath"
export casTempMolcasDir="$casDir/TMP_molcas"
export casTempMolproDir="$casDir/TMP_molpro"
export casInputDir="$casDir/input"

#INT PATHS
if [ -z $lkMonocentricFile ]; then
	export intDir="$CurrDir/int_l.${lMonocentric}_k.${kMonocentric}"
else
	export intDir="$CurrDir/int_lk.${lkMonocentricFile}"
fi
export intTempDir="$intDir/TMP"
export intInputDir="$intDir/input"

#ORB PATHS
if [ -z $lkMonocentricFile ]; then
	tempPath=$(echo "orb_i.${inactive}_r1.${ras1}_r2.${ras2}_r3.${ras3}_e.${nElecRas1}-${nElecRas2}-${nElecRas3}_l.${lMonocentric}_k.${kMonocentric}" | sed 's/ /-/g')
else
	tempPath=$(echo "orb_i.${inactive}_r1.${ras1}_r2.${ras2}_r3.${ras3}_e.${nElecRas1}-${nElecRas2}-${nElecRas3}_lk.${lkMonocentricFile}" | sed 's/ /-/g')
fi
export orbDir="$CurrDir/$tempPath"
export orbTempDir="$orbDir/TMP"
export orbPropsDir="$orbDir/props"
export orbInputDir="$orbDir/input"

#SCATT PATHS
tempPath=$(echo "scat_cc.${CCF}_Rmin.${Rmin}_Rmax.${Rmax}_NBS.${BSplineNodes}" | sed 's/ /-/g')
export scatDir="$CurrDir/$tempPath"
export scatLogDir="$scatDir/Log"
export scatSAEDir="$scatDir/SAE"
export scatStorageDir="$scatDir/Storage"
export scatInputDir="$scatDir/input"
export scatSubmitDir="$scatDir/submission"

function check {
	local f=$1
	count=$(grep -c 'Happy\|Variable memory released\|Convergence problem\|_RC_ALL_IS_WELL_' $f)
	if [ $count -lt 1 ]; then
		echo "Error in $f". Last 10 lines of output: >> $log
		tail -10 $f >> $log
		exit 1
	else
		echo "${f%.*} terminated succesfully" >> $log
	fi
}

function skip {
	local f=$1
	local s=0
	if [ $OVERWRITE -ne 1 ]; then
		if [ -e $f ]; then
			s=1
		fi
	fi
	echo $s
}

export -f check
export -f skip
