#!/bin/bash
if [ $OVERWRITE -eq 1 ]; then
	if [ -d $intDir ]; then
		rm -rf $intDir
	fi
	mkdir $intDir
	mkdir $intTempDir
	mkdir $intInputDir
else
	if [ ! -d $intDir ]; then
		mkdir $intDir
		mkdir $intTempDir
		mkdir $intInputDir
	fi
fi
