#!/bin/bash

n=$(echo -n "$SLURM_JOB_ID" | wc -c )
if [ $n -gt 0 ]; then
	SLRM_CMD=$(scontrol show jobid=$SLURM_JOB_ID | awk '/Command=/' | sed 's/Command=//g' | awk '{print $1}')
	LINK=$(readlink -f $SLRM_CMD)
else
	LINK=$(readlink -f $BASH_SOURCE)
fi
SCRIPT=${LINK##*/}
BASE=${SCRIPT%.*}
INSTALLPATH=${LINK%/*}
. $INSTALLPATH/config.sh

OVERWRITE=0
BREAK=0
log=$CurrDir/log
if [ -e $log ]; then
	for (( i=1 ; i<=9999 ; i++ )); do
		if [ ! -e ${log}_$i ]; then	
			log=${log}_$i
			break
		fi 
	done
fi
ulimit -s unlimited
while getopts ":i:rphl:" opt; do
        case $opt in
		h) 	one="$INSTALLPATH/man/$BASE"
		   	two="$INSTALLPATH/man/general"
		   	cat $one $two | less
		   	exit ;; \
                i) 	IN=$OPTARG ;; \
		:) 	echo "missing argument for (i)nput"
			exit ;; \
		r) 	OVERWRITE=1 ;; \
		p) 	BREAK=1 ;; \
		l) 	log=$CurrDir/$OPTARG ;; \
		:) 	echo "missing argument for (l)og"
			exit ;; \
		[?]) 	echo "unknwon option -$OPTARG"
			exit 1 ;; \
        esac
done
export log=$log
export OVERWRITE=$OVERWRITE
export IN=$IN

if [ -e $log ]; then
	rm -f $log
fi
echo "------------------" > $log
echo "runMonopoly log file" >> $log
echo "------------------"  >> $log
if [ -z $IN ]; then
	echo "no (i)nput file provided. STOP" >> $log
	exit
fi
if [ ! -e $IN ]; then
	echo "(i)nput file does not exist. STOP" >> $log
fi

if [ $OVERWRITE -eq 1 ]; then 
	echo "ove(r)writing previous output" >> $log
else
	echo "keeping previous output" >> $log
fi
. $INSTALLPATH/processInput.sh 
. $INSTALLPATH/setupThree.sh

#CREATE INPUT FILES
if [ ! -e $orbDir/.cp001 ]; then
	echo "Creating Input Files" >> $log
	if [ ! -z $lkMonocentricFile ]; then
		cp $lkMonocentricFile $orbTempDir/lk.dat
	fi
	$xchemSrc/createInputOrb.exe $orbInputDir $IN >> $orbTempDir/createInputOrb.out
	check $orbTempDir/createInputOrb.out
	echo "input files created" > $orbDir/.cp001
else
	echo "input files already exist" >> $log
fi
if [ $BREAK -eq 1 ]; then	
	echo "runOrb.sh executed with -p. Script stopped to allow user to verify input files." >> $log
	echo "To resume run runLocal.sh without -p and -r options." >> $log
	exit
fi

#CHECK DEPENDECIES, CREATE LINKS AND CONCATENATE
echo "Check if necessary CAS data exists" >> $log
casFiles="loc.ion.bin loc.ion.RasOrb permanentDip nuclearRepulsion" 
for file in $casFiles; do
	if [ -e $casDir/$file ]; then
		ln -s $casDir/$file $orbTempDir/$file
	else
		echo "ERROR $file missing in cas directory" >> $log
		exit 1
	fi
done
intFiles="ham1E.int ov.int dip1.int dip2.int dip3.int vel1.int vel2.int vel3.int kin.int"
for file in $intFiles; do
	if [ -e $intDir/$file ]; then
		ln -s $intDir/$file $orbTempDir/$file
	else
		echo "ERROR $file missing in int directory" >> $log
		exit 1
	fi
done

if [[ -e $intDir/rawBlockBasis && $intDir/blockSymmetryInfo && $casDir/casInfo ]]; then
	echo "$intDir/rawBlockBasis
	  $intDir/blockSymmetryInfo
	  $casDir/casInfo" | $pyParseDir/blockSymAssign.py > $orbTempDir/blockBasis
	  cp $casDir/casInfo $orbTempDir/casInfo
elif [[ -e $intDir/blockBasis ]]; then
	echo "WARNING: using deprecated blockBasis file $intDir/blockBasis" >> $log
	echo "	if working with multiple active spaces ensure its veracity" >> $log
	echo "" >> $log
	cp $intDir/blockBasis $orbTempDir/blockBasis
	if [[ -e $casDir/casInfo ]]; then
		cp $casDir/casInfo $orbTempDir/casInfo
	elif [[ -e $intDir/casInfo ]]; then
		cp $intDir/casInfo $orbTempDir/casInfo
	else 
		echo "ERROR casInfo file not found" >> $log
		exit 1
	fi
else 
	echo "ERROR blockBasis file not found" >> $log
	exit 1
fi

nRas2=$(echo $ras2 | awk '{if (NF>0) {for(i=1;i<=NF;i++){sum=sum+$i}}else{sum=0}}END{print sum}')
nRas3=$(echo $ras3 | awk '{if (NF>0) {for(i=1;i<=NF;i++){sum=sum+$i}}else{sum=0}}END{print sum}')
nPI=$(echo $numberStatesParent | awk '{if (NF>0) {for(i=1;i<=NF;i++){sum=sum+$i}}else{sum=0}}END{print sum}')
nSS=$(echo $numberStatesSource | awk '{if (NF>0) {for(i=1;i<=NF;i++){sum=sum+$i}}else{sum=0}}END{print sum}')
nState=$(( ($nRas2+$nRas3 + 2) * $nPI + $nSS ))
nTRDSoll=$(($nState*($nState+1)/2))
if [ ! -e $casDir/trd1 ]; then
	cd $casTempMolcasDir
	echo "Build trd1" >> $log
	ls -1 *.trd1 > trd1ElementFileList
	nTRDIst=$(wc -l trd1ElementFileList | awk '{print $1}')
	if [ $nTRDSoll -ne $nTRDIst ]; then
		echo "ERROR: some trd1 files are missing" >> $log
		exit 1
	fi
	$xchemSrc/trd1Build.exe $orbInputDir/trd1Build.xchem > trd1Build.out
	mv trd1 $casDir/
fi 
if [ ! -e $casDir/trd2 ]; then
	cd $casTempMolcasDir
	echo "Build trd2" >> $log
	ls -1 *.trd2 > trd2ElementFileList
	nTRDIst=$(wc -l trd2ElementFileList | awk '{print $1}')
	if [ $nTRDSoll -ne $nTRDIst ]; then
		echo "ERROR: some trd2 files are missing"
		exit 1
	fi
	$xchemSrc/trd2Build.exe $orbInputDir/trd2Build.xchem > trd2Build.out
	mv trd2 $casDir/
fi 

if [ -e $casDir/trd1 ]; then
	ln -s $casDir/trd1 $orbTempDir/trd1
else
	echo "WARNING: trd1 could not be built" >> $log
	exit "make sure all trd1 element calculations are finished and rerun this script before executing submitOrb2.sh"
fi
if [ -e $casDir/trd2 ]; then
	ln -s $casDir/trd2 $orbTempDir/trd2	
else
	echo "WARNING: trd2 could not be built" >> $log
	exit "make sure all trd2 element calculations are finished and rerun this script before executing submitOrb2.sh"
fi

cd $orbDir
cp $casDir/multipoles.dat $orbDir/multipoles.dat
cp $casDir/velocities.dat $orbDir/velocities.dat
ln -s $casDir/En_0*.out $orbDir/

cd $orbTempDir

#LINDEPKILL
echo "RUNNING linDepKill.xchem (orbTempDir/createZeroOrbByBlock.monpo.xchem.out) to obtain"  >> $log
echo "  -hybrid basis with linear depndencies removed while mainting symmtries as far as possible" >> $log
$xchemSrc/linDepKill.exe $orbInputDir/linDepKill.xchem > linDepKill.out

linDepError=$(grep -c "Error in overlap exceed" linDepKill.out)
if [ $linDepError -ne 0 ]; then
	echo "Error: could not remove lin deps. Threshold too high?" >> $log
	exit
fi

cp $orbTempDir/ion.homemade.bin $orbDir/diffuseOrb.bin
cp $orbTempDir/ion.homemade.RasOrb $orbDir/diffuseOrb.RasOrb

nLinIndep=$(awk '/Total orbitals/{print $4}' linDepKill.out)
nMonPoBasis=$(awk '/Total orbitals/{print $3}' linDepKill.out)
nLocBas=$(awk '/local Basis Fcts/{print $NF}' linDepKill.out)

sed -i "s/##nLocBas/$nLocBas/g" $orbInputDir/GABS_interface_sym.xchem
sed -i "s/##nLinIndep/$nLinIndep/g" $orbInputDir/oneIntBas2Orb.xchem

sed -i "s/##nLinIndep/$nLinIndep/g" $orbInputDir/ijkl2iqks.xchem
sed -i "s/##nLinIndep/$nLinIndep/g" $orbInputDir/ijkl2ijrs.xchem
sed -i "s/##nLinIndep/$nLinIndep/g" $orbInputDir/iqks2pqrs.xchem
cp $templateDir/submitIjkl2ijrs.sh $orbTempDir/submitIjkl2ijrs.sh
cp $templateDir/submitIjkl2iqks.sh $orbTempDir/submitIjkl2iqks.sh
cp $templateDir/submitIqks2pqrs.sh $orbTempDir/submitIqks2pqrs.sh
cp $templateDir/submitOrb2OperatorMatrix.sh $orbTempDir/submitOrb2OperatorMatrix.sh
sed -e "s%##INSTALLPATH%$INSTALLPATH%g" -e "s%##IN%$IN%g" $templateDir/orbSubmit.template > $CurrDir/orbSubmit.sh

#ONEINTBASTWOORB
$xchemSrc/oneIntBas2Orb_new.exe $orbInputDir/oneIntBas2Orb.xchem > oneIntBas2Orb.out

cd $CurrDir
