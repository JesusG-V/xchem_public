#!/bin/bash

n=$(echo -n "$SLURM_JOB_ID" | wc -c )
if [ $n -gt 0 ]; then
    SLRM_CMD=$(scontrol show jobid=$SLURM_JOB_ID | awk '/Command=/' | sed 's/Command=//g' | awk '{print $1}')
    LINK=$(readlink -f $SLRM_CMD)
else
    LINK=$(readlink -f $BASH_SOURCE)
fi
SCRIPT=${LINK##*/}
BASE=${SCRIPT%.*}
INSTALLPATH=${LINK%/*}
source $INSTALLPATH/config.sh

OVERWRITE=0
BREAK=0
QOS="normal"
log=$CurrDir/log
if [ -e $log ]; then
    for (( i=1 ; i<=9999 ; i++ )); do
        if [ ! -e ${log}_$i ]; then
            log=${log}_$i
            break
        fi
    done
fi
ulimit -s unlimited

while [[ $# -gt 0 ]]; do
	key=$1
	case $key in
	    -h|--help)
		one="$INSTALLPATH/man/$BASE"
		two="$INSTALLPATH/man/general"
		cat $one $two | less
	    exit
	    shift
	    ;;
	    -q|--qos)
	    QOS="$2"
		if [ -z "$2" ]; then
			echo "missing argument for (q)os"
			exit 1
		fi
	    shift
	    shift
	    ;;
		-i|--input)
		IN="$2"
		if [ -z "$2" ]; then
			echo "missing argument for (i)nput"
			exit 1
		fi
		if [ ! -e $IN ]; then
			echo "input file does not exist"
			exit 1
		fi
		shift
		shift
		;;
		-r|--overwrite)
		OVERWRITE=1
	    shift
	    ;;
		-p|--pause)
		BREAK=1
	    shift
	    ;;
		-l|--log)
		log="$CurrDir/$2"
		if [ -z "$2" ]; then
			echo "missing argument for (l)og"
			exit 1
		fi
	    shift
	    shift
	    ;;
	    *)
	    echo "ERROR unknown command line option"
	    exit
	    ;;
	esac
done

export log=$log
export OVERWRITE=$OVERWRITE
export IN=$IN

if [ -e $log ]; then
    rm -f $log
fi
echo "------------------"> $log
echo "runScattering log file" >> $log
echo "------------------"  >> $log
if [ -z $IN ]; then
	echo "no (i)nput file provided. STOP" >> $log
	exit
fi
if [ ! -e $IN ]; then
	echo "(i)nput file does not exist. STOP" >> $log
fi

if [ $OVERWRITE -eq 1 ]; then 
	echo "ove(r)writing previous output" >> $log
else
	echo "keeping previous output" >> $log
fi
. $INSTALLPATH/processInput.sh 
. $INSTALLPATH/setupFour.sh

if [ -z $lkMonocentricFile ]; then
	MaxAngularMomentum=$(($lMonocentric + 2*$kMonocentric))
else
	MaxAngularMomentum=$(awk 'BEGIN{LMax=-1}NR>1{t=$1+2*$2; if (t>LMax){LMax=t}}END{print LMax}' $lkMonocentricFile)
fi 

#PREPARE INPUT FILES
if [ ! -e $scatDir/.cp001 ]; then
	if [[ -e $CurrDir/$CCF ]]; then
		cp $CurrDir/$CCF $scatInputDir/$CCF
	else
		echo "ERROR CCF file not found" >> $log
		exit 1
	fi
	sed "s%##StoreDirectory%$scatSAEDir%g" $templateDir/SAE_GeneralInputFile.template > $scatInputDir/SAE_GeneralInputFile
	sed "s/##NumberNodes/$BSplineNodes/g ; s/##Rmin/$Rmin/g ; s/##Rmax/$Rmax/g" $templateDir/BSplineBasis.template > $scatInputDir/BSplineBasis
	sed "s/##MaxAngularMomentum/$MaxAngularMomentum/g" $templateDir/GaussianBasis.template > $scatInputDir/GaussianBasis
	cp $templateDir/XchemInputFile.template $scatInputDir/XchemInputFile
	cp $templateDir/Basis_Mixed.template $scatInputDir/Basis_Mixed
	cp $templateDir/GaussianBSplineBasis.template $scatInputDir/GaussianBSplineBasis
	cp $templateDir/AbsorptionPotential.template $scatInputDir/AbsorptionPotential
	
	cp $templateDir/SCAT_PARAMS $scatSubmitDir/SCAT_PARAMS

	#cd $scatInputDir
	#$pyParseDir/ccParse.py $CCF
	#cd $CurrDir
	#nChannel=$(cat $scatInputDir/${CCF}.info)

	mkdir $scatSubmitDir/calcs
	cp $IN $scatDir/$IN
	cp $templateDir/submitScat*.sh $scatSubmitDir/calcs/
	sed "s%##INSTALLPATH%$INSTALLPATH%g ; s%##IN%$scatDir/$IN%g ; s%##CurrDir%$CurrDir%g" $templateDir/submitScatHeader1.sh > $scatSubmitDir/calcs/submitScatHeader1.sh

	fs=$(ls $templateDir/Launch_*.sh | xargs -n 1 basename)
	for f in $fs; do 
		sed "s%##nChannel%$nChannel%g" $templateDir/$f > $scatSubmitDir/$f
	done	
	
else
    echo "input files already exist" >> $log
fi
