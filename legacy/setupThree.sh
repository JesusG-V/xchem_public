#!/bin/bash
if [ $OVERWRITE -eq 1 ]; then
	if [ -d $orbDir ]; then
		rm -rf $orbDir
	fi
	mkdir $orbDir
	mkdir $orbTempDir
	mkdir $orbInputDir
else
	if [ ! -d $orbDir ]; then
		mkdir $orbDir
		mkdir $orbTempDir
		mkdir $orbInputDir
	fi
fi
